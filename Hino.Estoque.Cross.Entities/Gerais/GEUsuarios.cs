using Hino.Estoque.Infra.Cross.Entities.Estoque;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Estoque.Infra.Cross.Entities.Gerais
{
    [Table("GEUSUARIOS")]
    public class GEUsuarios : BaseEntity
    {
        public GEUsuarios()
        {
        }

        [Key]
        [Column(Order = 3)]
        public string codusuario { get; set; }
        public string nome { get; set; }
        public bool status { get; set; }
        public byte tipusuario { get; set; }
        public string senha { get; set; }
        public string controle { get; set; }
        public string email { get; set; }
        public bool visualizasc { get; set; }
        public decimal vervalprogrec { get; set; }
        public string vervalromaneio { get; set; }
        public bool admincalendario { get; set; }
        public bool alteracomissao { get; set; }
        public bool alteratblprvig { get; set; }
        public bool visvlrultcmpsc { get; set; }
        public bool visvlrreqest { get; set; }
        public bool naopaginagrid { get; set; }
        public bool viscomprarestrito { get; set; }
        public bool permiteestoqneg { get; set; }
        
    }
}
