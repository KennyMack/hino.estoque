﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Estoque.Infra.Cross.Entities.Gerais
{
    [Table("CZINTEGREMPRESA")]
    public class CZIntegrEmpresa : BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long IdIntegracao { get; set; }
        public long ID { get; set; }
        public string CNPJCPF { get; set; }
        public bool NContribuinte { get; set; }
        public string Inscricao { get; set; }
        public string GrupoEIP { get; set; }
        public string Site { get; set; }
        public string EMailNFe { get; set; }
        public int CodEstado { get; set; }
        public string Fone { get; set; }
        public string Fax { get; set; }
        public string Tipo { get; set; }
        public string EnderecoEntrega { get; set; }
        public string BairroEntrega { get; set; }
        public string FoneEntrega { get; set; }
        public string CidadeEntrega { get; set; }
        public string EstadoEntrega { get; set; }
        public string CepEntrega { get; set; }
        public string EnderecoCobranca { get; set; }
        public string BairroCobranca { get; set; }
        public string FoneCobranca { get; set; }
        public string CidadeCobranca { get; set; }
        public string EstadoCobranca { get; set; }
        public string CepCobranca { get; set; }
        public bool Apagar { get; set; }

        public long? CodEmpresa { get; set; }
        public DateTime? DataIntegra { get; set; }
        public bool EmpIntegra { get; set; }
    }
}
