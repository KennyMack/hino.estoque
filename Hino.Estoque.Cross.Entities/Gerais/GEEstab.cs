using Hino.Estoque.Infra.Cross.Entities.Estoque;
using Hino.Estoque.Infra.Cross.Entities.Fiscal;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Estoque.Infra.Cross.Entities.Gerais
{
    [Table("GEESTAB")]
    public class GEEstab : BaseEntity
    {
        public GEEstab()
        {
            this.ESInventario = new HashSet<ESInventario>();
            this.ESKardex = new HashSet<ESKardex>();
            this.ESLote = new HashSet<ESLote>();
            this.ESTransferencia = new HashSet<ESTransferencia>();
            this.FSProdutoparamEstab = new HashSet<FSProdutoparamEstab>();
            this.FSProdutoPcp = new HashSet<FSProdutoPcp>();
            this.GEFuncionarios = new HashSet<GEFuncionarios>();
            this.ESEnderecamento = new HashSet<ESEnderecamento>();
        }
        [Key]
        [Column(Order = 1)]
        public short codestab { get; set; }
        public string razaosocial { get; set; }
        public string nomefantasia { get; set; }
        public int? codendereco { get; set; }
        public byte codmoeda { get; set; }
        public decimal ativo { get; set; }
        public bool cdquantidade { get; set; }
        public byte cdvlunitario { get; set; }
        public bool cdvltotal { get; set; }
        public short cdpercentual { get; set; }
        public short? codnatopercompra { get; set; }
        public short? codnatopervenda { get; set; }
        public bool? gerarcontaempresa { get; set; }
        public string caminhoimagens { get; set; }
        public decimal cdtamanhocubico { get; set; }
        public short? codnatopercf { get; set; }
        public bool consestoque { get; set; }
        public string certificado { get; set; }
        public short? codgrupoestab { get; set; }
        public bool usaunnegocio { get; set; }
        public string tokenbuscacnpj { get; set; }
        
        public virtual ICollection<ESInventario> ESInventario { get; set; }
        public virtual ICollection<ESKardex> ESKardex { get; set; }
        public virtual ICollection<ESLote> ESLote { get; set; }
        public virtual ICollection<ESTransferencia> ESTransferencia { get; set; }
        public virtual ICollection<FSProdutoparamEstab> FSProdutoparamEstab { get; set; }
        public virtual ICollection<FSProdutoPcp> FSProdutoPcp { get; set; }
        public virtual ICollection<GEFuncionarios> GEFuncionarios { get; set; }
        public virtual ICollection<ESEnderecamento> ESEnderecamento { get; set; }
    }
}
