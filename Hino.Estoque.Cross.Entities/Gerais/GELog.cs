using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Estoque.Infra.Cross.Entities.Gerais
{
    [Table("GELOG")]
    public class GELog : BaseEntity
    {
        public GELog()
        {
            this.GELogDet = new HashSet<GELogDet>();
        }
        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public decimal codlog { get; set; }
        public DateTime datalog { get; set; }
        public string tabela { get; set; }
        public string chavereg { get; set; }
        //[ForeignKey("GEUsuarios")]
        public string codusuario { get; set; }
        public string estacao { get; set; }
        public string acao { get; set; }
        
        public virtual ICollection<GELogDet> GELogDet { get; set; }
        public virtual GEUsuarios GEUsuarios { get; set; }
    }
}
