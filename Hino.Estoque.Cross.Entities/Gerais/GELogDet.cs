using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Estoque.Infra.Cross.Entities.Gerais
{
    [Table("GELOGDET")]
    public class GELogDet : BaseEntity
    {
        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        //[ForeignKey("GELog")]
        public decimal codlog { get; set; }
        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string campo { get; set; }
        public string de { get; set; }
        public string para { get; set; }

        public virtual GELog gelog { get; set; }
    }
}
