﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Estoque.Infra.Cross.Entities.Gerais
{
    [Table("CZINTEGRACAOREVISAO")]
    public class CZIntegracaoRevisao : BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long IdIntegracao { get; set; }
        public string Desenho { get; set; }
        public string Revisao { get; set; }
        public DateTime? DataIntegracao { get; set; }
        public bool Integrado { get; set; }
    }
}
