﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Infra.Cross.Entities.Vendas
{
    [Table("CZINTEGPEDITENS")]
    public class CZIntegPedItens: BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long IdItemIntegr { get; set; }
        [ForeignKey("czintegrapedidos")]
        public long IdIntegracao { get; set; }
        public int OrderIDItem { get; set; }
        public string Pedido { get; set; }
        public string Codigo { get; set; }
        public string Descricao { get; set; }
        public string ItemCliente { get; set; }
        public string ExtraInfo { get; set; }
        public int Item { get; set; }
        public int ItemII { get; set; }
        public string Tipo { get; set; }
        public double Qtd { get; set; }
        public double QtdII { get; set; }
        public double IPI { get; set; }
        public double IPIII { get; set; }
        public double Preco { get; set; }
        public double PrecoII { get; set; }
        public double Desconto { get; set; }
        public string Modificado { get; set; }
        public double DescPed { get; set; }
        public double MargemIVA { get; set; }
        public string PedClie { get; set; }
        public string ItemClie { get; set; }
        public double Importado { get; set; }
        public string CodHino { get; set; }
        public bool Integrado { get; set; }

        public virtual CZIntegraPedidos czintegrapedidos { get; set; }

    }
}
