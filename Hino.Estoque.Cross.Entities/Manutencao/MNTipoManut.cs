﻿using Hino.Estoque.Infra.Cross.Utils.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Infra.Cross.Entities.Manutencao
{
    [Table("MNTIPOMANUT")]
    public class MNTipoManut : BaseEntity
    {
        [Key]
        [Column(Order = 1)]
        public string codtipomanut { get; set; }
        public string descricao { get; set; }
        public EManutPrioridade prioridade { get; set; }
        public bool enviaemail { get; set; }
        public string email { get; set; }
        public string codusuario { get; set; }
        public short? codmotivo { get; set; }
    }
}
