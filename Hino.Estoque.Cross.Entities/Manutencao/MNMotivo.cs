﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Infra.Cross.Entities.Manutencao
{
    [Table("MNMOTIVO")]
    public class MNMotivo: BaseEntity
    {
        [Key]
        [Column(Order = 1)]
        public short codmotivo { get; set; }
        public string descricao { get; set; }
    }
}
