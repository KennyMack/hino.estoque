﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Infra.Cross.Entities.Manutencao
{
    [Table("MNEQUIPAMENTO")]
    public class MNEquipamento : BaseEntity
    {
        [Key]
        [Column(Order = 1)]
        public short codestab { get; set; }
        [Key]
        [Column(Order = 2)]
        public string codequipamento { get; set; }
        public string descricao { get; set; }
        public short status { get; set; }
    }
}
