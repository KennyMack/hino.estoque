﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Infra.Cross.Entities.Manutencao
{

    [Table("MNSOLICMANUT")]
    public class MNSolicManut : BaseEntity
    {
        [Key]
        [Column(Order = 1)]
        public short codestab { get; set; }
        [Key]
        [Column(Order = 2)]
        public long codsolic { get; set; }
        public string codequipamento { get; set; }
        public DateTime datasolic { get; set; }
        public string status { get; set; }
        public short codmotivo { get; set; }
        public string observacao { get; set; }
        public string solicitante { get; set; }
        public string aprovador { get; set; }
        public string prioridade { get; set; }
        [NotMapped]
        public string descprioridade
        {
            get
            {
                switch (prioridade)
                {
                    case "A":
                        return "Alta";
                    case "M":
                        return "Média";
                    default:
                        return "Baixa";

                }
            }
        }
        public string codtipomanut { get; set; }

        public virtual MNEquipamento Equipamento { get; set; }
        public virtual MNTipoManut TipoManut { get; set; }
        public virtual MNMotivo Motivo { get; set; }

    }
}
