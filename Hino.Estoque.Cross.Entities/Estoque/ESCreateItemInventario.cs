﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Infra.Cross.Entities.Estoque
{
    public class ESCreateItemInventario
    {
        public short codestab { get; set; }
        public long codinv { get; set; }
        public int contagem { get; set; }
        public string codproduto { get; set; }
        public string codestoque { get; set; }
    }
}
