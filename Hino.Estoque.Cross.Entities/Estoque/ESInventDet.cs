using Hino.Estoque.Infra.Cross.Entities.Fiscal;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Estoque.Infra.Cross.Entities.Estoque
{
    [Table("ESINVENTDET")]
    public class ESInventDet : BaseEntity
    {
        public ESInventDet()
        {
            this.ESInventLote = new HashSet<ESInventLote>();
            this.ESInventTerc = new HashSet<ESInventTerc>();
        }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        //[ForeignKey("ESInventario")]
        public short codestab { get; set; }
        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        //[ForeignKey("ESInventario")]
        public decimal codinv { get; set; }
        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public decimal contagem { get; set; }
        [Key]
        [Column(Order = 4)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        //[ForeignKey("FSProduto")]
        public string codproduto { get; set; }
        [Key]
        [Column(Order = 5)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        //[ForeignKey("FSLocalEstoque")]
        public string codestoque { get; set; }
        public decimal saldofisico { get; set; }
        public decimal? saldoinvent { get; set; }
        public DateTime? datacontagem { get; set; }
        public string codusuario { get; set; }

        public virtual ESInventario ESInventario { get; set; }
        public virtual ICollection<ESInventLote> ESInventLote { get; set; }
        public virtual ICollection<ESInventTerc> ESInventTerc { get; set; }
        public virtual FSLocalEstoque FSLocalEstoque { get; set; }
        public virtual FSProduto FSProduto { get; set; }
    }
}
