﻿using Hino.Estoque.Infra.Cross.Entities.Fiscal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Infra.Cross.Entities.Estoque
{
    [Table("ESPRODENDEREC")]
    public class ESProdEnderec : BaseEntity
    {
        [Key]
        [Column(Order = 1)]
        public short codestab { get; set; }
        [Key]
        [Column(Order = 2)]
        public int codendestoque { get; set; }
        [Key]
        [Column(Order = 3)]
        public string codproduto { get; set; }

        public virtual ESEnderecamento ESEnderecamento { get; set; }
        public virtual FSProdutoparamEstab FSProdutoparamEstab { get; set; }
    }
}
