﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.ViewModels.Estoque
{
    public class ESCreateProdTransferencia
    {
        public int CodEstab { get; set; }
        public string CodProduto { get; set; }
        public string CodEstoqueOri { get; set; }
        public string CodEstoqueDest { get; set; }
        public decimal Quantidade { get; set; }
        public string CodUsuario { get; set; }
        public long Lote { get; set; }
    }
}
