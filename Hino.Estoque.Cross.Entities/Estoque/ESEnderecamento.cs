﻿using Hino.Estoque.Infra.Cross.Entities.Gerais;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Infra.Cross.Entities.Estoque
{
    [Table("ESENDERECAMENTO")]
    public class ESEnderecamento : BaseEntity
    {
        public ESEnderecamento()
        {
            this.ESProdEnderec = new HashSet<ESProdEnderec>();
        }

        [Key]
        [Column(Order = 1)]
        public short codestab { get; set; }
        [Key]
        [Column(Order = 2)]
        public int codendestoque { get; set; }
        public string descricao { get; set; }
        public byte tipo { get; set; }
        public string sigla { get; set; }
        public string nivel { get; set; }
        public string posicao { get; set; }
        public string rua { get; set; }
        public decimal capacidadem3 { get; set; }
        public decimal altura { get; set; }
        public decimal largura { get; set; }
        public decimal comprimento { get; set; }
        
        public virtual ICollection<ESProdEnderec> ESProdEnderec { get; set; }
        public virtual GEEstab GEEstab { get; set; }
    }
}
