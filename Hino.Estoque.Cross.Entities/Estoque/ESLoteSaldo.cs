using Hino.Estoque.Infra.Cross.Entities.Fiscal;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Estoque.Infra.Cross.Entities.Estoque
{
    [Table("ESLOTESALDO")]        
    public class ESLoteSaldo : BaseEntity
    {
        public ESLoteSaldo()
        {
            this.ESTransfLote = new HashSet<ESTransfLote>();
        }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short codestab { get; set; }
        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        //[ForeignKey("ESLote")]
        public decimal lote { get; set; }
        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        //[ForeignKey("FSLocalEstoque")]
        public string codestoque { get; set; }
        public double qtdentrada { get; set; }
        public double qtdsaida { get; set; }

        [NotMapped]
        public double saldoLote
        {
            get => qtdentrada - qtdsaida;
        }

        public virtual ESLote ESLote { get; set; }
        public virtual ICollection<ESTransfLote> ESTransfLote { get; set; }
        public virtual FSLocalEstoque FSLocalEstoque { get; set; }
    }
}
