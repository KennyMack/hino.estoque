using Hino.Estoque.Infra.Cross.Entities.Fiscal;
using Hino.Estoque.Infra.Cross.Entities.Gerais;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Estoque.Infra.Cross.Entities.Estoque
{
    [Table("ESINVENTARIO")]
    public class ESInventario : BaseEntity
    {
        public ESInventario()
        {
            this.ESInventDet = new HashSet<ESInventDet>();
            this.ESKardex = new HashSet<ESKardex>();
        }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        //[ForeignKey("GEEstab")]
        public short codestab { get; set; }
        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public decimal codinv { get; set; }
        //[ForeignKey("FSProduto")]
        public string codproduto { get; set; }
        //[ForeignKey("FSLocalEstoque")]
        public string codestoque { get; set; }
        public string status { get; set; }
        public DateTime datainv { get; set; }
        [NotMapped]
        public decimal contagem { get; set; }
        public decimal qtdecontag { get; set; }
        public DateTime? dataefet { get; set; }
        public decimal? contagefet { get; set; }

        [NotMapped]
        public string descstatus
        {
            get
            {
                switch (status)
                {
                    case "E":
                        return "EFETIVADA";
                    case "C":
                        return "CANCELADA";
                    default:
                        return "PENDENTE";
                }
            }
        }

        public virtual ICollection<ESInventDet> ESInventDet { get; set; }
        public virtual ICollection<ESKardex> ESKardex { get; set; }
        public virtual FSLocalEstoque FSLocalEstoque { get; set; }
        public virtual FSProduto FSProduto { get; set; }
        public virtual GEEstab GEEstab { get; set; }
    }
}
