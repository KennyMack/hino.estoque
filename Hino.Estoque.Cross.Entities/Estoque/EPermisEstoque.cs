﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Infra.Cross.Entities.Estoque
{
    public enum EPermisEstoque
    {
        Sucesso = 0,
        SemPermissao = 1,
        DataInvalida = 2
    }
}
