﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Infra.Cross.Entities.Estoque
{
    public class ESCreateProdEndTransf
    {
        public ESCreateProdEndTransf()
        {
            Items = new HashSet<ESProdEndTransfItem>();
        }

        public string CodEstoqueOri { get; set; }
        public int CodEstab { get; set; }
        public string CodProduto { get; set; }
        public string CodUsuario { get; set; }
        public IEnumerable<ESProdEndTransfItem> Items { get; set; }
    }

    public class ESProdEndTransfItem
    {
        public string CodEstoqueDest { get; set; }
        public decimal Quantidade { get; set; }
    }
}
