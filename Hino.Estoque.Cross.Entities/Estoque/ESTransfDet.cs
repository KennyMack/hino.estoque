using Hino.Estoque.Infra.Cross.Entities.Fiscal;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Estoque.Infra.Cross.Entities.Estoque
{
    [Table("ESTRANSFDET")]
    public class ESTransfDet : BaseEntity
    {
        public ESTransfDet()
        {
            this.ESTransfLote = new HashSet<ESTransfLote>();
        }
        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short codestab { get; set; }
        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        //[ForeignKey("ESTransferencia")]
        public decimal codtransf { get; set; }
        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public decimal seqtransf { get; set; }
        //[ForeignKey("FSProduto")]
        public string codproduto { get; set; }
        public DateTime data { get; set; }
        public string locdestino { get; set; }
        public decimal necessidade { get; set; }
        public string locorigem { get; set; }
        public decimal qtdetransf { get; set; }
        
        public virtual ICollection<ESTransfLote> ESTransfLote { get; set; }
        public virtual ESTransferencia ESTransferencia { get; set; }
        public virtual FSProduto FSProduto { get; set; }
    }
}
