﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Hino.Estoque.Infra.Cross.Entities.Fiscal;

namespace Hino.Estoque.Infra.Cross.Entities.Estoque
{
    [Table("ESREQUISICAO")]
    public class ESRequisicao : BaseEntity
    {
        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int codrequisicao { get; set; }
        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [ForeignKey("FSProdutoparamEstab")]
        public short codestab { get; set; }
        [ForeignKey("FSProdutoparamEstab")]
        [Column(Order = 3)]
        public string codproduto { get; set; }
        public long? lote { get; set; }
        public string codestoque { get; set; }
        public string codccusto { get; set; }
        public DateTime data { get; set; }
        public int codfuncionario { get; set; }
        public decimal quantidade { get; set; }
        public decimal precomedio { get; set; }
        public decimal valortotal { get; set; }
        public int? codos { get; set; }
        [NotMapped]
        public decimal saldoatual { get; set; }
        public string codusuaprov { get; set; }
        public short status { get; set; }
        public string observacao { get; set; }

        public virtual FSProduto FSProduto { get; set; }
        public virtual FSProdutoparamEstab FSProdutoparamEstab { get; set; }
    }
}
