using Hino.Estoque.Infra.Cross.Entities.Gerais;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Estoque.Infra.Cross.Entities.Estoque
{
    [Table("ESTRANSFERENCIA")]        
    public class ESTransferencia : BaseEntity
    {
        public ESTransferencia()
        {
        }

        [Key]
        [Column(Order = 1)]
        public short codestab { get; set; }
        [Key]
        [Column(Order = 2)]
        public decimal codtransf { get; set; }
        public DateTime data { get; set; }
        public string tipo { get; set; }
        public decimal? codordprod { get; set; }
        public string nivelordprod { get; set; }
        public decimal? codestrutura { get; set; }
        public int? codpedvenda { get; set; }
        public string status { get; set; }
        public string solicitante { get; set; }
        public string responsavel { get; set; }
        public DateTime? dtstatus { get; set; }
        
    }
}
