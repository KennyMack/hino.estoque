using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Estoque.Infra.Cross.Entities.Estoque
{
    [Table("ESTRANSFLOTE")]
    public class ESTransfLote : BaseEntity
    {
        [Key]
        [Column(Order = 1)]
        public int codtransflote { get; set; }
       // [ForeignKey("ESTransfDet,ESLoteSaldo")]
        public short codestab { get; set; }
       // [ForeignKey("ESTransfDet")]
        public decimal codtransf { get; set; }
        //[ForeignKey("ESTransfDet")]
        public decimal seqtransf { get; set; }
        //[ForeignKey("ESLoteSaldo")]
        public decimal lote { get; set; }
        //[ForeignKey("ESLoteSaldo")]
        public string codestoque { get; set; }
        public decimal quantidade { get; set; }

        public virtual ESLoteSaldo ESLoteSaldo { get; set; }
        public virtual ESTransfDet ESTransfDet { get; set; }
    }
}
