using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Estoque.Infra.Cross.Entities.Estoque
{
    [Table("ESINVENTTERC")]
    public class ESInventTerc : BaseEntity
    {
        [Key]
        [Column(Order = 1)]
        public short codestab { get; set; }
        [Key]
        [Column(Order = 2)]
        public decimal codinv { get; set; }
        [Key]
        [Column(Order = 3)]
        public decimal contagem { get; set; }
        [Key]
        [Column(Order = 4)]
        public string codproduto { get; set; }
        [Key]
        [Column(Order = 5)]
        public string codestoque { get; set; }
        [Key]
        [Column(Order = 6)]
        public decimal indiceitem { get; set; }
        [Key]
        [Column(Order = 7)]
        public byte sequencia { get; set; }
        public decimal saldofiscal { get; set; }
        public decimal? saldoinvent { get; set; }

        public virtual ESInventDet ESInventDet { get; set; }
    }
}
