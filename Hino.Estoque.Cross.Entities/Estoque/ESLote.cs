using Hino.Estoque.Infra.Cross.Entities.Fiscal;
using Hino.Estoque.Infra.Cross.Entities.Gerais;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Estoque.Infra.Cross.Entities.Estoque
{
    [Table("ESLOTE")]        
    public class ESLote : BaseEntity
    {
        public ESLote()
        {
            this.ESInventLote = new HashSet<ESInventLote>();
            this.ESLoteSaldo = new HashSet<ESLoteSaldo>();
        }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [ForeignKey("FSProdutoparamEstab")]
        public short codestab { get; set; }
        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public decimal lote { get; set; }
        public bool origem { get; set; }
        public string status { get; set; }
        public decimal? indiceitemnf { get; set; }
        public byte? seqitemnf { get; set; }
        public decimal? codlancamento { get; set; }
        public decimal? loteorigem { get; set; }
        public DateTime datalote { get; set; }
        public string aprovador { get; set; }
        [ForeignKey("FSProdutoparamEstab")]
        [Column(Order = 3)]
        public string codproduto { get; set; }

        public int? codunnegocio { get; set; }

        public virtual ICollection<ESInventLote> ESInventLote { get; set; }
        public virtual ICollection<ESLoteSaldo> ESLoteSaldo { get; set; }
        public virtual FSProdutoparamEstab FSProdutoparamEstab { get; set; }
    }
}
