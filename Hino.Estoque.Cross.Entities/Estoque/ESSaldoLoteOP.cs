﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Infra.Cross.Entities.Estoque
{
    public class ESSaldoLoteOP : BaseEntity
    {
        public string CODCOMPONENTE { get; set; }
        public long LOTE { get; set; }
        public decimal QTDENECESSARIA { get; set; }
        public string CODESTOQUEORI { get; set; }
        public double SALDOESTOQUEPADRAO { get; set; }
        public string CODESTOQUEDEST { get; set; }
        public decimal QUANTIDADETRANSF { get; set; }
        public decimal SALDOTRANSF { get; set; }
        public int MESMOLOCAL { get; set; }
        public long CODORDPROD { get; set; }
        public string NIVELORDPROD { get; set; }
        public long CODESTRUTURA { get; set; }
        public int CODROTEIRO { get; set; }
        public int OPERACAO { get; set; }
        public long CODOPLOTE { get; set; }
        public long CODOPLOTEORIGINAL { get; set; }
        public short LOTELIVRE { get; set; }
        public DateTime DATALOTE { get; set; }
        public short ORIGEMLOTE { get; set; }
        public long? SEQLOTE { get; set; }
        public double QTDETRANSFATUAL { get; set; }
    }
}
