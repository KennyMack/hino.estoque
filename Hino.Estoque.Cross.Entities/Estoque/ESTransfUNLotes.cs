﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Infra.Cross.Entities.Estoque
{
    public class esTransfUNLotes
    {
        public long CodLote { get; set; }
        public string CodProduto { get; set; }
        public string CodEstoque { get; set; }
        public int? CodUnNegocio { get; set; }
        public int SeqLote { get; set; }
        public decimal Saldo { get => Entrada - Saida; }
        public decimal Entrada { get; set; }
        public decimal Saida { get; set; }
        public decimal QtdSeparada { get; set; }
    }
}
