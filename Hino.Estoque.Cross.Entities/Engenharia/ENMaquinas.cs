﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Estoque.Infra.Cross.Entities.Engenharia
{
    [Table("ENMAQUINAS")]
    public class ENMaquinas : BaseEntity
    {

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short codestab { get; set; }
        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string codmaquina { get; set; }
        public string descricao { get; set; }
        public short codcelula { get; set; }
        public short? codprocesso { get; set; }
        public bool status { get; set; }
    }
}
