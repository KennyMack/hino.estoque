﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Infra.Cross.Entities.Producao
{
    [Table("PDMOTIVOS")]
    public class PDMotivos: BaseEntity
    {
        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int codmotivo { get; set; }
        public string descricao { get; set; }
        /// <summary>
        /// 0 'PARADA'
        /// 1 'REFUGO'
        /// 2 'RETRABALHO'
        /// 3 'PARADA PROGRAMADA'
        /// </summary>
        public short tipo { get; set; }
        public bool status { get; set; }
    }
}
