using Hino.Estoque.Infra.Cross.Entities;
using Hino.Estoque.Infra.Cross.Entities.Fiscal;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Estoque.Infra.Cross.Entities.Producao
{
    [Table("PDORDEMPRODCOMP")]
    public class PDOrdemProdComp : BaseEntity
    {
        public PDOrdemProdComp()
        {
            pdoptransf = new HashSet<PDOPTransf>();
            OrdemProdCompEstqEnd = new List<PDOrdemProdCompEstqEnd>();
        }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short codestab { get; set; }
        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public decimal codordprod { get; set; }
        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string nivelordprod { get; set; }
        [Key]
        [Column(Order = 4)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public decimal codestrutura { get; set; }
        [Key]
        [Column(Order = 5)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int codroteiro { get; set; }
        [Key]
        [Column(Order = 6)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int operacao { get; set; }
        [Key]
        [Column(Order = 7)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [ForeignKey("fsproduto")]
        public string codcomponente { get; set; }
        public string codestoque { get; set; }
        public double quantidade { get; set; }
        // public decimal qtdconsumida { get; set; }
        public bool baixaapto { get; set; }
        // public decimal variacao { get; set; }
        // public decimal perda { get; set; }
        // public bool reaproveita { get; set; }
        // public decimal altmanual { get; set; }
        // public decimal fator { get; set; }
        // public decimal geraconsudif { get; set; }
        // public decimal sequencia { get; set; }
        // public decimal camadas { get; set; }
        // public decimal passadas { get; set; }
        // public byte nivel { get; set; }
        // public short? codtecido { get; set; }
        // public decimal percagrup { get; set; }
        public string codunidade { get; set; }
        // public decimal qtdeeng { get; set; }
        public bool separacao { get; set; }
        [NotMapped]
        public double saldoorigem { get; set; }
        [NotMapped]
        public double saldodestino { get; set; }
        [NotMapped]
        public bool loadqtd { get; set; }
        public virtual FSLocalEstoque fslocalestoque { get; set; }
        public virtual FSProduto fsproduto { get; set; }
        public virtual ICollection<PDOPTransf> pdoptransf { get; set; }
        public virtual PDOrdemProd pdordemprod { get; set; }
        public virtual PDOrdemProdRotinas pdordemprodrotinas { get; set; }

        public List<PDOrdemProdCompEstqEnd> OrdemProdCompEstqEnd { get; set; }
    }
}
