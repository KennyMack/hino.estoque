﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Infra.Cross.Entities.Producao.OEE
{
    public class PDOEEMensalResultado
    {
        public string codmaq { get; set; }
        public string enmaquinasdescricao { get; set; }
        public decimal meta { get; set; }
        public decimal producao { get; set; }
        public decimal refugo { get; set; }
        public string horastrab { get; set; }
        public string horasparada { get; set; }
        public decimal oee { get; set; }
        public decimal disponibilidade { get; set; }
        public decimal produtividade { get; set; }
        public decimal qualidade { get; set; }
    }
}
