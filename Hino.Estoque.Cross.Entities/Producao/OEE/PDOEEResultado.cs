﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Infra.Cross.Entities.Producao.OEE
{
    public class PDOEEResultado
    {
        public int CodSeq { get; set; }
        public string Campo { get; set; }
        public string C_01 { get; set; }
        public string C_02 { get; set; }
        public string C_03 { get; set; }
        public string C_04 { get; set; }
        public string C_05 { get; set; }
        public string C_06 { get; set; }
        public string C_07 { get; set; }
        public string C_08 { get; set; }
        public string C_09 { get; set; }
        public string C_10 { get; set; }
        public string C_11 { get; set; }
        public string C_12 { get; set; }
        public string C_13 { get; set; }
        public string C_14 { get; set; }
        public string C_15 { get; set; }
        public string C_16 { get; set; }
        public string C_17 { get; set; }
        public string C_18 { get; set; }
        public string C_19 { get; set; }
        public string C_20 { get; set; }
        public string C_21 { get; set; }
        public string C_22 { get; set; }
        public string C_23 { get; set; }
        public string C_24 { get; set; }
        public string C_25 { get; set; }
        public string C_26 { get; set; }
        public string C_27 { get; set; }
        public string C_28 { get; set; }
        public string C_29 { get; set; }
        public string C_30 { get; set; }
        public string C_31 { get; set; }
    }
}
