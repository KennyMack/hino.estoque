﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Estoque.Infra.Cross.Entities.Producao.OEE
{
    [Table("PDOEEJUSTIFICA")]
    public class PDOEEJustifica : BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int codjustifica { get; set; }
        public short codestab { get; set; }
        public string codmaquina { get; set; }
        public short tipo { get; set; }
        public DateTime dataoee { get; set; }
        public DateTime datajustificativa { get; set; }
        public string justificativa { get; set; }
    }
}
