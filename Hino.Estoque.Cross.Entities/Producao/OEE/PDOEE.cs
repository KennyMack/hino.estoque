﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Infra.Cross.Entities.Producao.OEE
{
    public class PDOEE : BaseEntity
    {
        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short codestab { get; set; }
        public string codusuario { get; set; }
        public DateTime dataini { get; set; }
        public DateTime datafim { get; set; }
        public short? codprocesso { get; set; }
        public string codmaquina { get; set; }
        public int? codagrup { get; set; }
        public string turno { get; set; }
        public int gerardet { get; set; }
    }
}
