using Hino.Estoque.Infra.Cross.Entities;
using Hino.Estoque.Infra.Cross.Entities.Estoque;
using Hino.Estoque.Infra.Cross.Entities.Fiscal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Estoque.Infra.Cross.Entities.Producao
{
    [Table("PDORDEMPROD")]
    public class PDOrdemProd : BaseEntity
    {
        public PDOrdemProd()
        {
            this.estransferencia = new HashSet<ESTransferencia>();
            this.pdordemprodrotinas = new HashSet<PDOrdemProdRotinas>();
            this.pdordemprodcomp = new HashSet<PDOrdemProdComp>();
        }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short codestab { get; set; }
        public decimal codprograma { get; set; }
        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public decimal codordprod { get; set; }
        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string nivelordprod { get; set; }
        public decimal codordprodpai { get; set; }
        public string nivelordprodpai { get; set; }
        public int codroteiro { get; set; }
        public decimal codestrutura { get; set; }
        [ForeignKey("fsproduto")]
        public string codproduto { get; set; }
        public DateTime dtinicio { get; set; }
        public DateTime dttermino { get; set; }
        public DateTime? dtencerramento { get; set; }
        public decimal programado { get; set; }
        public decimal realizado { get; set; }
        public decimal refugado { get; set; }
        public string status { get; set; }
        public decimal ultimasequencia { get; set; }
        public decimal fator { get; set; }
        public decimal qtdinicial { get; set; }
        public decimal custoinicial { get; set; }
        public decimal custoreproc { get; set; }
        public int seqbarras { get; set; }
        public int? codagrupfoto { get; set; }
        public bool liberado { get; set; }
        public DateTime dtemissao { get; set; }
        public bool usaopestoque { get; set; }
        public bool usasaldoestoque { get; set; }

        [NotMapped]
        public bool ShowResultOP { get; set; }

        public virtual ICollection<ESTransferencia> estransferencia { get; set; }
        public virtual FSProduto fsproduto { get; set; }
        public virtual ICollection<PDOrdemProdRotinas> pdordemprodrotinas { get; set; }
        public virtual ICollection<PDOrdemProdComp> pdordemprodcomp { get; set; }
    }
}
