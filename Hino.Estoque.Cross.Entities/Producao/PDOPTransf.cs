using Hino.Estoque.Infra.Cross.Entities;
using Hino.Estoque.Infra.Cross.Entities.Fiscal;
using Hino.Estoque.Infra.Cross.Entities.Gerais;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Estoque.Infra.Cross.Entities.Producao
{
    [Table("PDOPTRANSF")]
    public class PDOPTransf : BaseEntity
    {
        public PDOPTransf()
        {
            TransfLote = new List<PDOPTransfLote>();
        }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short codestab { get; set; }
        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int codtransf { get; set; }
        public decimal codordprod { get; set; }
        public string nivelordprod { get; set; }
        public decimal codestrutura { get; set; }
        public int codroteiro { get; set; }
        public int operacao { get; set; }
        public string codcomponente { get; set; }
        public string codestoqueori { get; set; }
        public string codestoquedest { get; set; }
        public string codusuario { get; set; }
        public System.DateTime datatransf { get; set; }
        public double quantidade { get; set; }
        public short status { get; set; }
        public decimal? codtransfest { get; set; }
        
        [NotMapped]
        public List<PDOPTransfLote> TransfLote { get; set; }
    }
}
