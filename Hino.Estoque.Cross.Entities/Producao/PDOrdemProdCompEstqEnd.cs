﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Infra.Cross.Entities.Producao
{
    public class PDOrdemProdCompEstqEnd
    {
        [Key]
        [Column(Order = 1)]
        public short codestab { get; set; }
        [Key]
        [Column(Order = 2)]
        public string codestoque { get; set; }
        [Key]
        [Column(Order = 3)]
        public string codproduto { get; set; }
        public decimal codordprod { get; set; }
        public string nivelordprod { get; set; }
        public string descricao { get; set; }
        public decimal saldodisponivel { get; set; }
    }
}
