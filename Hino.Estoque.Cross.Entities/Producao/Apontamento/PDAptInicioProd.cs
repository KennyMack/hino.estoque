﻿using Hino.Estoque.Infra.Cross.Entities.Gerais;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Infra.Cross.Entities.Producao.Apontamento
{
    [Table("PDAPTINICIOPROD")]
    public class PDAptInicioProd : BaseEntity
    {
        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long codiniapt { get; set; }
        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short codestab { get; set; }
        public decimal codordprod { get; set; }
        public string nivelordprod { get; set; }
        public decimal codestrutura { get; set; }
        public short codroteiro { get; set; }
        public short operacao { get; set; }
        public decimal codfuncionario { get; set; }
        public string codusuario { get; set; }
        public DateTime dtinicio { get; set; }
        public string codmaquina { get; set; }
        /// <summary>
        /// 1 - Inicio Preparação
        /// 2 - Fim Preparação
        /// 3 - Inicio de Produção
        /// 4 - Fim de Produção
        /// 5 - Inicio Troca de Serviço
        /// 6 - Fim Troca de Serviço
        /// 7 - Inicio de Manutenção
        /// 8 - Fim de Manutenção
        /// 9 - Inicio Consulta Desenho
        /// 10 - Fim Consulta Desenho
        /// 11 - Início Retrabalho
        /// 12 - Fim Retrabalho
        /// 13 - Termino turno
        /// </summary>
        public short tipo { get; set; }
        public int? codmotivo { get; set; }
    }
}
