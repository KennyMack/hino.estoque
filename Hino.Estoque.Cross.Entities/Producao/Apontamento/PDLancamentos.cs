using Hino.Estoque.Infra.Cross.Entities;
using Hino.Estoque.Infra.Cross.Entities.Estoque;
using Hino.Estoque.Infra.Cross.Entities.Gerais;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Estoque.Infra.Cross.Entities.Producao.Apontamento
{
    [Table("PDLANCAMENTOS")]
    public class PDLancamentos : BaseEntity
    {
        public PDLancamentos()
        {
            this.eskardex = new HashSet<ESKardex>();
            this.eslote = new HashSet<ESLote>();
        }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short codestab { get; set; }
        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long codlancamento { get; set; }
        public decimal codordprod { get; set; }
        public string nivelordprod { get; set; }
        public int codroteiro { get; set; }
        public int operacao { get; set; }
        public System.DateTime dtinicio { get; set; }
        public System.DateTime dttermino { get; set; }
        public decimal codfuncionario { get; set; }
        public decimal quantidade { get; set; }
        public decimal codestrutura { get; set; }
        public decimal? codlancpai { get; set; }
        public string turno { get; set; }
        public string codmaquina { get; set; }
        public decimal? codcor { get; set; }
        public string lotemanual { get; set; }
        public bool reginsp { get; set; }
        public short? codmotivo { get; set; }
        public int totalmin { get; set; }
        public int totalminesp { get; set; }
        public int produtividade { get; set; }
        [NotMapped]
        public string codusuario { get; set; }
        [NotMapped]
        public int terminoturno { get; set; }

        public virtual ICollection<ESKardex> eskardex { get; set; }
        public virtual ICollection<ESLote> eslote { get; set; }
        public virtual GEEstab geestab { get; set; }
        public virtual GEFuncionarios gefuncionarios { get; set; }
        public virtual PDOrdemProdRotinas pdordemprodrotinas { get; set; }
    }
}
