﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Infra.Cross.Entities.Producao.Apontamento
{
    [Table("PDSUCATA")]
    public class PDSucata: BaseEntity
    {
        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short codestab { get; set; }
        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long codsucata { get; set; }
        public DateTime datasuc { get; set; }
        public long? lote { get; set; }
        public string codproduto { get; set; }
        public string codestoque { get; set; }
        public decimal qtdeproduto { get; set; }
        public string codprdsuc { get; set; }
        public string codestsuc { get; set; }
        public decimal qtdesucata { get; set; }
        public int codmotivo { get; set; }
        public decimal codfuncionario { get; set; }
        public decimal codordprod { get; set; }
        public string nivelordprod { get; set; }
        public decimal codestrutura { get; set; }
        public long? codlancamento { get; set; }
    }
}
