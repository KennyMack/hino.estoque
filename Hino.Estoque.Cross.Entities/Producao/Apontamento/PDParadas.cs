﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Infra.Cross.Entities.Producao.Apontamento
{
    [Table("PDPARADAS")]
    public class PDParadas: BaseEntity
    {
        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long codparada { get; set; }
        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short codestab { get; set; }
        public decimal codordprod { get; set; }
        public string nivelordprod { get; set; }
        public DateTime inicio { get; set; }
        public DateTime termino { get; set; }
        public int codfuncionario { get; set; }
        public short codmotivo { get; set; }
        public string observacao { get; set; }
        public decimal codestrutura { get; set; }
        public string turno { get; set; }
        public string codmaquina { get; set; }
    }
}
