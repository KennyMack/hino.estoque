﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Infra.Cross.Entities.Producao.Apontamento
{
    [Table("PDAPTTERMINOPROD")]
    public class PDAptTerminoProd: BaseEntity
    {
        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long codfimapt { get; set; }
        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short codestab { get; set; }
        public long codiniapt { get; set; }
        public string codusuario { get; set; }
        public int codfuncionario { get; set; }
        public DateTime dttermino { get; set; }
        /// <summary>
        /// 1 - Inicio Preparação
        /// 2 - Fim Preparação
        /// 3 - Inicio de Produção
        /// 4 - Fim de Produção
        /// 5 - Inicio Troca de Serviço
        /// 6 - Fim Troca de Serviço
        /// 7 - Inicio de Manutenção
        /// 8 - Fim de Manutenção
        /// 9 - Inicio Consulta Desenho
        /// 10 - Fim Consulta Desenho
        /// 11 - Início Retrabalho
        /// 12 - Fim Retrabalho
        /// 13 - Termino turno
        /// </summary>
        public short tipo { get; set; }
        public decimal quantidade { get; set; }
        public short? codmotivo { get; set; }
        public decimal qtdrefugo { get; set; }
        public decimal qtdretrabalho { get; set; }
        public string observacao { get; set; }
        [NotMapped]
        public bool askstartnew { get; set; }
    }
}
