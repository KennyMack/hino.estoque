﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Infra.Cross.Entities.Producao.Apontamento
{
    public class PDLancMotivoRefugo
    {
        public short codmotivo { get; set; }
        public decimal quantidade { get; set; }
        public decimal qtdreaproveitada { get; set; }
        public decimal qtdsucata { get; set; }
    }
}
