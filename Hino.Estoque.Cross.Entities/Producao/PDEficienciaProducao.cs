﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Infra.Cross.Entities.Producao
{
    public class PDEficienciaProducao
    {
        [Column("ESTABELECIMENTO")]
        public short CodEstab { get; set; }
        [Column("PROCESSO")]
        public string Processo { get; set; }
        [Column("OP")]
        public string OP { get; set; }
        [Column("PRODUTO")]
        public string Produto { get; set; }
        [Column("TOTALPROGRAMADO")]
        public decimal TotalProgramado { get; set; }
        [Column("TURNO")]
        public string Turno { get; set; }
        [Column("METADIA")]
        public decimal MetaDia { get; set; }
        [Column("QTDEPROD")]
        public decimal QtdeProd { get; set; }
        [Column("QTDEREF")]
        public decimal QtdeRef { get; set; }
        [Column("EFICIENCIA")]
        public decimal Eficiencia { get; set; }
        [Column("CRITICA")]
        public string Critica { get; set; }
        [Column("MAQUINA")]
        public string Maquina { get; set; }
        [Column("FUNCIONARIO")]
        public string Funcionario { get; set; }
        [Column("EFICTOT")]
        public decimal EficTotal { get; set; }
        [Column("QTDMAQ")]
        public decimal QtdMaquinas { get; set; }
        [Column("PERCREFUGO")]
        public decimal PercRefugo { get; set; }
    }
}
