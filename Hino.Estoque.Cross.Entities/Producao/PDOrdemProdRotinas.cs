using Hino.Estoque.Infra.Cross.Entities;
using Hino.Estoque.Infra.Cross.Entities.Producao.Apontamento;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Estoque.Infra.Cross.Entities.Producao
{
    [Table("PDORDEMPRODROTINAS")]
    public class PDOrdemProdRotinas : BaseEntity
    {
        public PDOrdemProdRotinas()
        {
            this.pdlancamentos = new HashSet<PDLancamentos>();
            this.pdordemprodcomp = new HashSet<PDOrdemProdComp>();
        }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short codestab { get; set; }
        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public decimal codordprod { get; set; }
        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string nivelordprod { get; set; }
        [Key]
        [Column(Order = 4)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public decimal codestrutura { get; set; }
        [Key]
        [Column(Order = 5)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int codroteiro { get; set; }
        [Key]
        [Column(Order = 6)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int operacao { get; set; }
        public short codrotina { get; set; }
        public string descricao { get; set; }
        public string codmaquina { get; set; }
        public decimal prodhoraria { get; set; }
        public decimal operador { get; set; }
        public bool aptoprocesso { get; set; }
        public bool aptoacabado { get; set; }
        public decimal qtdeproducao { get; set; }
        public decimal qtderefugo { get; set; }
        // public DateTime dtinicio { get; set; }
        // public DateTime dttermino { get; set; }
        // public decimal alteradomanual { get; set; }

        public virtual ICollection<PDLancamentos> pdlancamentos { get; set; }
        public virtual PDOrdemProd pdordemprod { get; set; }
        public virtual ICollection<PDOrdemProdComp> pdordemprodcomp { get; set; }
    }
}
