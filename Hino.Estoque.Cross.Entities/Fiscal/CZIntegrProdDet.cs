﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Estoque.Infra.Cross.Entities.Fiscal
{
    [Table("CZINTEGRPRODDET")]
    public class CZIntegrProdDet : BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long IdIntegDet { get; set; }
        // [ForeignKey("CZIntegProdutos")]
        public long IdIntegracao { get; set; }
        public string PartNumber { get; set; }
        public decimal? UnitPrice { get; set; }
        public decimal? UnitCost { get; set; }
        public string FiscalClass { get; set; }
        public string DiscountClass { get; set; }
        public string LeadtimeClass { get; set; }
        public decimal? NetWeight { get; set; }
        public decimal? GrossWeight { get; set; }
        public decimal? Stock { get; set; }
        public decimal? StockLeast { get; set; }
        public decimal? Reserve { get; set; }
        public string Prevision { get; set; }
        public DateTime? DateDet { get; set; }
        public bool Apagar { get; set; }

        public virtual CZIntegProdutos CZIntegProdutos { get; set; }
    }
}
