﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Infra.Cross.Entities.Fiscal
{
    [Table("CZINTEGPRODUTOS")]
    public class CZIntegProdutos: BaseEntity
    {
        public CZIntegProdutos()
        {
            this.CZIntegrProdDet = new HashSet<CZIntegrProdDet>();
        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long IdIntegracao { get; set; }
        public string PartNumber { get; set; }
        public string PartNumberOther { get; set; }
        public string PartDescription { get; set; }
        public string Faturamento { get; set; }
        public string ProductLineID { get; set; }
        public string DepartmentID { get; set; }
        public string UnitID { get; set; }
        public string FinishID1 { get; set; }
        public string FinishID2 { get; set; }
        public string FinishID3 { get; set; }
        public string NomHeight { get; set; }
        public string NomWidth { get; set; }
        public string NomDepth { get; set; }
        public string Dimensions { get; set; }
        public DateTime? DateInclusion { get; set; }
        public DateTime? DateRelease { get; set; }
        public DateTime? DateTermination { get; set; }
        public DateTime? ModificationDate { get; set; }
        public string ModificationID { get; set; }
        public decimal? EstHolec { get; set; }
        public decimal? MinHolec { get; set; }
        public decimal? MaxHolec { get; set; }
        public decimal? EstProHolec { get; set; }
        public decimal? EstSiemens { get; set; }
        public decimal? MinSiemens { get; set; }
        public decimal? MaxSiemens { get; set; }
        public decimal? EstProSiemens { get; set; }
        public string Classificacao { get; set; }
        public bool Critico { get; set; }
        public decimal Importado { get; set; }
        public bool Desativar { get; set; }
        public string CodWohner { get; set; }
        public string IDSped { get; set; }
        public bool ListaRepr { get; set; }
        public bool Apagar { get; set; }
        
        public DateTime? DataIntegra { get; set; }
        public bool ProdIntegra { get; set; }

        public virtual ICollection<CZIntegrProdDet> CZIntegrProdDet { get; set; }
    }
}
