using Hino.Estoque.Infra.Cross.Entities.Estoque;
using Hino.Estoque.Infra.Cross.Entities.Gerais;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Hino.Estoque.Infra.Cross.Entities.Fiscal
{
    [Table("FSPRODUTOPARAMESTAB")]
    public class FSProdutoparamEstab : BaseEntity
    {
        public FSProdutoparamEstab()
        {
            ESRequisicao = new HashSet<ESRequisicao>();
            this.ESLote = new HashSet<ESLote>();
            this.FSSaldoEstoque = new HashSet<FSSaldoEstoque>();
            this.FSProdutoPcp = new HashSet<FSProdutoPcp>();
            this.ESProdEnderec = new HashSet<ESProdEnderec>();
        }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        // [ForeignKey("GEEstab")]
        public short codestab { get; set; }
        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        // [ForeignKey("FSProduto")]
        public string codproduto { get; set; }
        public string codfamilia { get; set; }
        public string codtipo { get; set; }
        // [ForeignKey("FSLocalEstoque")]
        public string codestoque { get; set; }
        public string codunidade { get; set; }
        public string ncm { get; set; }
        public short status { get; set; }
        public bool contestoque { get; set; }
        public decimal? codaplicentrada { get; set; }
        public decimal? codaplicsaida { get; set; }
        public string codserv { get; set; }
        public bool? codorigmerc { get; set; }
        public string codcest { get; set; }
        public decimal perclucro { get; set; }
        public bool atualizapreco { get; set; }
        public bool? tipoatualizacao { get; set; }
        public bool? mrpplanjcomp { get; set; }
        public decimal perperdasped { get; set; }
        public string codanp { get; set; }
        public bool enviagtinxml { get; set; }
        public decimal fatconvunidex { get; set; }
        public DateTime? dtultcompra { get; set; }
        public decimal vlrultcompra { get; set; }
        public bool imobilizado { get; set; }
        public int? codsubfamilia { get; set; }
        public DateTime datamodificacao { get; set; }
        public string uniquekey { get; set; }
        public int? idapi { get; set; }
        public decimal vlrultcompraimp { get; set; }

        [NotMapped]
        public FSSaldoEstoque SaldoEstoque
        {
            get
            {
                try
                {
                    return this.FSSaldoEstoque.ToArray()[0];
                }
                catch (Exception)
                {
                }

                return null;
            }
            set => this.FSSaldoEstoque = new HashSet<FSSaldoEstoque>() { value };
        }

        public virtual ICollection<ESRequisicao> ESRequisicao { get; set; }
        public virtual ICollection<ESLote> ESLote { get; set; }
        public virtual FSLocalEstoque FSLocalEstoque { get; set; }
        public virtual FSProduto FSProduto { get; set; }
        public virtual ICollection<FSSaldoEstoque> FSSaldoEstoque { get; set; }
        public virtual ICollection<FSProdutoPcp> FSProdutoPcp { get; set; }
        public virtual GEEstab GEEstab { get; set; }
        public virtual ICollection<ESProdEnderec> ESProdEnderec { get; set; }
    }
}
