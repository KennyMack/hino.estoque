using Hino.Estoque.Infra.Cross.Entities.Estoque;
using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Estoque.Infra.Cross.Entities.Fiscal
{
    [Table("FSPRODUTO")]
    public class FSProduto : BaseEntity
    {
        public FSProduto()
        {
            this.ESInventario = new HashSet<ESInventario>();
            this.ESInventDet = new HashSet<ESInventDet>();
            this.ESKardex = new HashSet<ESKardex>();
            this.ESTransfDet = new HashSet<ESTransfDet>();
            this.FSProdutoparamEstab = new HashSet<FSProdutoparamEstab>();
            this.FSProdutoPcp = new HashSet<FSProdutoPcp>();
        }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string codproduto { get; set; }
        public string descricao { get; set; }
        public string detalhamento { get; set; }
        public string infadicfiscal { get; set; }

        [NotMapped]
        public FSProdutoparamEstab ProdutoParamEstab
        {
            get
            {
                try
                {
                    return this.FSProdutoparamEstab.ToArray()[0];
                }
                catch (Exception)
                {
                }

                return null;
            }
            set => this.FSProdutoparamEstab = new HashSet<FSProdutoparamEstab>() { value };
        }

        [NotMapped]
        public FSProdutoPcp ProdutoPCP
        {
            get
            {
                try
                {
                    return this.FSProdutoPcp.ToArray()[0];
                }
                catch (Exception)
                {
                }

                return null;
            }
            set => this.FSProdutoPcp = new HashSet<FSProdutoPcp>() { value };
        }

        public virtual ICollection<ESInventario> ESInventario { get; set; }
        public virtual ICollection<ESInventDet> ESInventDet { get; set; }
        public virtual ICollection<ESKardex> ESKardex { get; set; }
        public virtual ICollection<ESTransfDet> ESTransfDet { get; set; }
        public virtual ICollection<FSProdutoparamEstab> FSProdutoparamEstab { get; set; }
        public virtual ICollection<FSProdutoPcp> FSProdutoPcp { get; set; }
    }
}
