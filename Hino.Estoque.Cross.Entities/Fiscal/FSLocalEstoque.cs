using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Estoque.Infra.Cross.Entities.Fiscal
{
    [Table("FSLOCALESTOQUE")]
    public class FSLocalEstoque : BaseEntity
    {
        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string codestoque { get; set; }
        public string descricao { get; set; }
        public string classificacao { get; set; }
        public string codgrupoestoq { get; set; }
        public int? codempresa { get; set; }
    }
}
