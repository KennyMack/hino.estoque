using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Estoque.Infra.Cross.Entities.Fiscal
{
    [Table("FSSALDOESTOQUE")]
    public class FSSaldoEstoque : BaseEntity
    {
        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        //[ForeignKey("FSProdutoparamEstab")]
        public string codproduto { get; set; }
        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short codestab { get; set; }
        //[ForeignKey("FSLocalEstoque")]
        public string codestoque { get; set; }
        public bool status { get; set; }
        public decimal anterior { get; set; }
        public decimal entrada { get; set; }
        public decimal saida { get; set; }
        public decimal capacidade { get; set; }
        public decimal capacidadedisp { get; set; }

        [NotMapped]
        public decimal saldoEstoque
        {
            get => (anterior + entrada) - saida;
        }

        public virtual FSLocalEstoque FSLocalEstoque { get; set; }
        public virtual FSParamLocEstoqEstab FSParamLocEstoqEstab { get; set; }
        public virtual FSProdutoparamEstab FSProdutoparamEstab { get; set; }
    }
}
