﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Infra.Cross.Entities.Fiscal
{
    [Table("FSPARAMLOCESTOQESTAB")]
    public class FSParamLocEstoqEstab : BaseEntity
    {

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short codestab { get; set; }
        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string codestoque { get; set; }
        public short? calcmrp { get; set; }
        public short? negativo { get; set; }
        public short? status { get; set; }
        public int? codendclassif { get; set; }
        public string rua { get; set; }
        public string andar { get; set; }
        public string predio { get; set; }
        public string apartamento { get; set; }
        public decimal? capacidade { get; set; }
        public decimal? peso { get; set; }
        public decimal? altura { get; set; }
        public decimal? profundidade { get; set; }
        public int? codunnegocio { get; set; }
        public string sigla { get; set; }
        public short tipoend { get; set; }
    }
}
