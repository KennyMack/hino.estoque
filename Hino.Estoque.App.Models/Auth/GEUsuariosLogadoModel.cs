﻿using Hino.Estoque.App.Models.Gerais;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Estoque.App.Models.Auth
{
    public class GEUsuariosLogadoModel : BaseEntity
    {
        public GEUsuariosLogadoModel()
        {
            GESettings = new GEEstabSettingsModel();
        }
        public bool success { get; set; }
        public string error { get; set; }
        public GEUsuariosModel GEUsuarios { get; set; }
        public GEFuncionariosModel GEFuncionarios { get; set; }
        public GEEstabSettingsModel GESettings { get; set; }
    }
}
