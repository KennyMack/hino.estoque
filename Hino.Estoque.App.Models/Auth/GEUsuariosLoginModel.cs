﻿using Hino.Estoque.App.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Estoque.App.Models.Auth
{
    [EndPoint("Auth/Mordor")]
    public class GEUsuariosLoginModel : BaseEntity
    {
        [RequiredField]
        [Max10LengthField]
        [DisplayField]
        public string identifier { get; set; }

        [RequiredField]
        [DisplayField]
        [DecimalRangeField(0, 999)]
        public short? codestab { get; set; }
        
        [DisplayField]
        public string senha { get; set; }
    }
}
