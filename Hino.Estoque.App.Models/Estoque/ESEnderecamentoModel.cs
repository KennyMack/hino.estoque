﻿using Hino.Estoque.App.Models.Gerais;
using Hino.Estoque.App.Utils.Attributes;
using System.Collections.Generic;

namespace Hino.Estoque.App.Models.Estoque
{
    [EndPoint("Estoque/Enderecamento/Estab/{pCodEstab}")]
    public class ESEnderecamentoModel : BaseEntity
    {
        public ESEnderecamentoModel()
        {
            this.ESProdEnderec = new HashSet<ESProdEnderecModel>();
        }
        
        public short codestab { get; set; }
        public int codendestoque { get; set; }
        public string descricao { get; set; }
        public byte tipo { get; set; }
        public string sigla { get; set; }
        public string nivel { get; set; }
        public string posicao { get; set; }
        public string rua { get; set; }
        public decimal capacidadem3 { get; set; }
        public decimal altura { get; set; }
        public decimal largura { get; set; }
        public decimal comprimento { get; set; }
        
        public virtual ICollection<ESProdEnderecModel> ESProdEnderec { get; set; }
        public virtual GEEstabModel GEEstab { get; set; }
    }
}
