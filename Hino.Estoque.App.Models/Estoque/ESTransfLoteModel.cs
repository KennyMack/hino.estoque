using System;
using System.Collections.Generic;
using Hino.Estoque.App.Models.Fiscal;
using Hino.Estoque.App.Utils.Attributes;
namespace Hino.Estoque.App.Models.Estoque
{
    [EndPoint("Estoque/Inventario/Estab/{pCodEstab}")]
    public class ESTransfLoteModel : BaseEntity
    {
        public int codtransflote { get; set; }
        public short codestab { get; set; }
        public decimal codtransf { get; set; }
        public decimal seqtransf { get; set; }
        public decimal lote { get; set; }
        public string codestoque { get; set; }
        public decimal quantidade { get; set; }

        public virtual ESLoteSaldoModel ESLoteSaldo { get; set; }
        public virtual ESTransfDetModel ESTransfDet { get; set; }
    }
}
