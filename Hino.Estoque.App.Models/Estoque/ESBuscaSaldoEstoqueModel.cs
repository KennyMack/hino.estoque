﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Estoque.App.Models.Estoque
{
    public class ESBuscaSaldoEstoqueModel
    {
        public int CodEstab { get; set; }
        public string CodProduto { get; set; }
        public string CodEstoque { get; set; }
        public decimal SaldoAtual { get; set; }
    }
}
