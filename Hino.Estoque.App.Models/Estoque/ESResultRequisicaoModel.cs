﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Estoque.App.Models.Estoque
{
    public class ESResultRequisicaoModel
    {
        public string CodProduto { get; set; }
        public string CodEstoque { get; set; }
        public bool Sucesso { get; set; }
        public decimal Quantidade { get; set; }
        public List<ESLoteSaldoModel> Lotes = new List<ESLoteSaldoModel>();
    }
}
