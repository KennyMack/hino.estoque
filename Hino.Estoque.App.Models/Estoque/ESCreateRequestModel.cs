﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Estoque.App.Models.Estoque
{
    public class ESCreateRequestModel
    {
        public string codproduto { get; set; }
        public short codestab { get; set; }
        public string codestoque { get; set; }
        public decimal quantidade { get; set; }
        public int codfuncionario { get; set; }
    }
}
