using System;
using System.Collections.Generic;
using Hino.Estoque.App.Models.Fiscal;
using Hino.Estoque.App.Utils.Attributes;
namespace Hino.Estoque.App.Models.Estoque
{
    [EndPoint("Estoque/Inventario/Estab/{pCodEstab}")]
    public class ESLoteSaldoModel : BaseEntity
    {
        public ESLoteSaldoModel()
        {
            this.ESTransfLote = new HashSet<ESTransfLoteModel>();
        }

        public short codestab { get; set; }
        public decimal lote { get; set; }
        public string codestoque { get; set; }
        public decimal qtdentrada { get; set; }
        public decimal qtdsaida { get; set; }

        public decimal saldoLote
        {
            get => qtdentrada - qtdsaida;
        }

        public virtual ESLoteModel ESLote { get; set; }
        public virtual ICollection<ESTransfLoteModel> ESTransfLote { get; set; }
        public virtual FSLocalEstoqueModel FSLocalEstoque { get; set; }
    }
}
