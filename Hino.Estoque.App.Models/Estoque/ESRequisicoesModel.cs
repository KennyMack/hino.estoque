﻿using Hino.Estoque.App.Models.Fiscal;
using Hino.Estoque.App.Utils.Attributes;
using Hino.Estoque.App.Utils.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Hino.Estoque.App.Models.Estoque
{
    [EndPoint("Estoque/Requisicoes/Estab/{pCodEstab}")]
    public class ESRequisicoesModel : BaseEntity
    {
        public int codrequisicao { get; set; }
        public short codestab { get; set; }
        public string codproduto { get; set; }
        public long? lote { get; set; }
        public string codestoque { get; set; }
        public string codccusto { get; set; }
        public DateTime data { get; set; }
        public int codfuncionario { get; set; }
        public decimal quantidade { get; set; }
        public decimal precomedio { get; set; }
        public decimal valortotal { get; set; }
        public int? codos { get; set; }
        [NotMapped]
        public decimal saldoatual { get; set; }
        public string codusuaprov { get; set; }
        public short status { get; set; }
        public string observacao { get; set; }

        [NotMapped]
        public string DetailDataReq => data.ToString("dd/MM/yy");
        [NotMapped]
        public string Detail => $"{codproduto} - {FSProduto.descricao.SubStrTitle()}";
        [NotMapped]
        public string SubDetail => $"{codestoque}   Qtd: {quantidade:n4}   {DetailDataReq}";

        public virtual FSProdutoModel FSProduto { get; set; }
        public virtual FSProdutoparamEstabModel FSProdutoparamEstab { get; set; }
    }
}
