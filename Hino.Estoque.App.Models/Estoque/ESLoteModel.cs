using System;
using System.Collections.Generic;
using Hino.Estoque.App.Models.Fiscal;
using Hino.Estoque.App.Utils.Attributes;
namespace Hino.Estoque.App.Models.Estoque
{
    [EndPoint("Estoque/Inventario/Estab/{pCodEstab}")]
    public class ESLoteModel : BaseEntity
    {
        public ESLoteModel()
        {
            this.ESInventLote = new HashSet<ESInventLoteModel>();
            this.ESLoteSaldo = new HashSet<ESLoteSaldoModel>();
        }

        public short codestab { get; set; }
        public decimal lote { get; set; }
        public bool origem { get; set; }
        public string status { get; set; }
        public decimal? indiceitemnf { get; set; }
        public byte? seqitemnf { get; set; }
        public decimal? codlancamento { get; set; }
        public decimal? loteorigem { get; set; }
        public DateTime datalote { get; set; }
        public string aprovador { get; set; }
        public string codproduto { get; set; }

        public virtual ICollection<ESInventLoteModel> ESInventLote { get; set; }
        public virtual ICollection<ESLoteSaldoModel> ESLoteSaldo { get; set; }
        public virtual FSProdutoparamEstabModel FSProdutoparamEstab { get; set; }
    }
}
