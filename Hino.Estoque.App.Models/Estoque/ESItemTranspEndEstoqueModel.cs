﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Estoque.App.Models.Estoque
{
    public class ESItemTranspEndEstoqueModel
    {
        public string CodProduto { get; set; }
        public string LocalOrigem { get; set; }
        public string LocalDestino { get; set; }
        public double Quantidade { get; set; }
    }
}
