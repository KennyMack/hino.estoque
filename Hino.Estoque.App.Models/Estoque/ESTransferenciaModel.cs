using System;
using Hino.Estoque.App.Utils.Attributes;
namespace Hino.Estoque.App.Models.Estoque
{
    [EndPoint("Estoque/Inventario/Estab/{pCodEstab}")]
    public class ESTransferenciaModel : BaseEntity
    {
        public ESTransferenciaModel()
        {
        }

        public short codestab { get; set; }
        public decimal codtransf { get; set; }
        public DateTime data { get; set; }
        public string tipo { get; set; }
        public decimal? codordprod { get; set; }
        public string nivelordprod { get; set; }
        public decimal? codestrutura { get; set; }
        public int? codpedvenda { get; set; }
        public string status { get; set; }
        public string solicitante { get; set; }
        public string responsavel { get; set; }
        public DateTime? dtstatus { get; set; }
    }
}
