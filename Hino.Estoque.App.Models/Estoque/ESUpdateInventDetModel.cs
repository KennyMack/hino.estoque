﻿using Hino.Estoque.App.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Estoque.App.Models.Estoque
{
    [EndPoint("Estoque/Inventario/Estab/{pCodEstab}")]
    public class ESUpdateInventDetModel : BaseEntity
    {
        [DisplayField]
        [RequiredField]
        public short codestab { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal codinv { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal contagem { get; set; }
        [DisplayField]
        [RequiredField]
        public string codproduto { get; set; }
        [DisplayField]
        [RequiredField]
        public string codestoque { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal saldoinvent { get; set; }
        [DisplayField]
        [RequiredField]
        public string codusuario { get; set; }
        [DisplayField]
        [RequiredField]
        public DateTime? datacontagem { get; set; }

        public string codlote { get; set; }
    }
}
