using System;
using Hino.Estoque.App.Models.Fiscal;
using Hino.Estoque.App.Utils.Attributes;
namespace Hino.Estoque.App.Models.Estoque
{
    [EndPoint("Estoque/Inventario/Estab/{pCodEstab}")]
    public class ESInventLoteModel : BaseEntity
    {
        public short codestab { get; set; }
        public decimal codinv { get; set; }
        public decimal contagem { get; set; }
        public string codproduto { get; set; }
        public string codestoque { get; set; }
        public decimal lote { get; set; }
        public decimal saldolote { get; set; }
        public decimal? saldoinvent { get; set; }

        public virtual ESInventDetModel esinventdet { get; set; }
        public virtual ESLoteModel eslote { get; set; }
        public virtual FSLocalEstoqueModel fslocalestoque { get; set; }
    }
}
