using System;
using Hino.Estoque.App.Utils.Attributes;
namespace Hino.Estoque.App.Models.Estoque
{
    [EndPoint("Estoque/Inventario/Estab/{pCodEstab}")]
    public class ESInventTercModel : BaseEntity
    {
        public short codestab { get; set; }
        public decimal codinv { get; set; }
        public decimal contagem { get; set; }
        public string codproduto { get; set; }
        public string codestoque { get; set; }
        public decimal indiceitem { get; set; }
        public byte sequencia { get; set; }
        public decimal saldofiscal { get; set; }
        public decimal? saldoinvent { get; set; }

        public virtual ESInventDetModel ESInventDet { get; set; }
    }
}
