using System;
using System.Collections.Generic;
using Hino.Estoque.App.Models.Fiscal;
using Hino.Estoque.App.Utils.Attributes;
namespace Hino.Estoque.App.Models.Estoque
{
    [EndPoint("Estoque/Inventario/Estab/{pCodEstab}")]
    public class ESTransfDetModel : BaseEntity
    {
        public ESTransfDetModel()
        {
            this.ESTransfLote = new HashSet<ESTransfLoteModel>();
        }
        public short codestab { get; set; }
        public decimal codtransf { get; set; }
        public decimal seqtransf { get; set; }
        public string codproduto { get; set; }
        public DateTime data { get; set; }
        public string locdestino { get; set; }
        public decimal necessidade { get; set; }
        public string locorigem { get; set; }
        public decimal qtdetransf { get; set; }

        public virtual ICollection<ESTransfLoteModel> ESTransfLote { get; set; }
        public virtual ESTransferenciaModel ESTransferencia { get; set; }
        public virtual FSProdutoModel FSProduto { get; set; }
    }
}
