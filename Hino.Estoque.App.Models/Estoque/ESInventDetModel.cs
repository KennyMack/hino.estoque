using System;
using System.Collections.Generic;
using System.ComponentModel;
using Hino.Estoque.App.Models.Fiscal;
using Hino.Estoque.App.Utils.Attributes;
using SQLite;

namespace Hino.Estoque.App.Models.Estoque
{
    [EndPoint("Estoque/Inventario/Estab/{pCodEstab}")]
    [Table("ESInventDetModel")]
    public class ESInventDetModel : BaseEntity
    {
        public ESInventDetModel()
        {
            this.ESInventLote = new HashSet<ESInventLoteModel>();
            this.ESInventTerc = new HashSet<ESInventTercModel>();
        }

        public short codestab { get; set; }
        public decimal codinv { get; set; }
        public decimal contagem { get; set; }
        public string codproduto { get; set; }
        public string codestoque { get; set; }
        public decimal saldofisico { get; set; }
        public bool alterado { get; set; }
        public decimal saldooriginal { get; set; }
        public DateTime? datacontagem { get; set; }
        public string codusuario { get; set; }
        decimal _quantitySelected = 0;
        public decimal saldoinvent
        {
            get { return _quantitySelected; }
            set { SetProperty(ref _quantitySelected, value); }
        }

        public string FSProduto_descricao { get; set; }

        public int? ESEnderecamento_codenderecamento { get; set; }
        public string ESEnderecamento_descricao { get; set; }
        public string ESEnderecamento_sigla { get; set; }
        public string ESEnderecamento_rua { get; set; }
        public string ESEnderecamento_nivel { get; set; }
        public string ESEnderecamento_posicao { get; set; }
        public string FSProduto_GTIN { get; set; }
        public string FSProduto_GTINEmbalagem { get; set; }

        [Ignore]
        public string ProductDescription { get => $"{codproduto} - {FSProduto_descricao}"; }

        [Ignore]
        public string Detail { get => $"Inventário: {codinv}/{contagem}"; }

        [Ignore]
        public string Detail2 { get => $"Local: {codestoque}"; }

        [Ignore]
        public string Detail3
        {
            get
            {
                if (!string.IsNullOrEmpty(ESEnderecamento_descricao))
                    return $@"{ESEnderecamento_descricao} - {ESEnderecamento_sigla} (R:{ESEnderecamento_rua} N:{ESEnderecamento_nivel} P:{ESEnderecamento_posicao})";

                return "";
            }
        }

        [Ignore]
        public virtual ESInventarioModel ESInventario { get; set; }
        [Ignore]
        public virtual ICollection<ESInventLoteModel> ESInventLote { get; set; }
        [Ignore]
        public virtual ICollection<ESInventTercModel> ESInventTerc { get; set; }
        [Ignore]
        public virtual FSLocalEstoqueModel FSLocalEstoque { get; set; }
        [Ignore]
        public virtual FSProdutoModel FSProduto { get; set; }
    }
}
