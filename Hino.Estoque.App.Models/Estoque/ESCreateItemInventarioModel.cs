﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Estoque.App.Models.Estoque
{
    public class ESCreateItemInventarioModel
    {
        public short codestab { get; set; }
        public long codinv { get; set; }
        public int contagem { get; set; }
        public string codproduto { get; set; }
        public string codestoque { get; set; }
        public decimal quantidade { get; set; }
        public string codusuario { get; set; }
        public DateTime datacontagem { get; set; }
        public string Result { get; set; }
    }
}
