﻿using Hino.Estoque.App.Models.Fiscal;
using Hino.Estoque.App.Utils.Attributes;

namespace Hino.Estoque.App.Models.Estoque
{
    [EndPoint("Estoque/Enderecamento/Produto/Estab/{pCodEstab}")]
    public class ESProdEnderecModel : BaseEntity
    {
        public short codestab { get; set; }
        public int codendestoque { get; set; }
        public string codproduto { get; set; }

        public virtual ESEnderecamentoModel ESEnderecamento { get; set; }
        public virtual FSProdutoparamEstabModel FSProdutoparamEstab { get; set; }
    }
}
