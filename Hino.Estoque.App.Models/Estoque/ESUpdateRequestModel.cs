﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Estoque.App.Models.Estoque
{
    public class ESUpdateRequestModel
    {
        public int codrequisicao { get; set; }
        public short codestab { get; set; }
        public int codfuncionario { get; set; }
    }
}
