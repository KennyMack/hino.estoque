﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Estoque.App.Models.Estoque
{
    public class ESCreateProdEndTransfModel
    {
        public ESCreateProdEndTransfModel()
        {
            Items = new List<ESProdEndTransfItemModel>();
        }

        public string CodEstoqueOri { get; set; }
        public int CodEstab { get; set; }
        public string CodProduto { get; set; }
        public string CodUsuario { get; set; }
        public List<ESProdEndTransfItemModel> Items { get; set; }
    }

    public class ESProdEndTransfItemModel
    {
        public string CodEstoqueDest { get; set; }
        public double Quantidade { get; set; }
    }
}
