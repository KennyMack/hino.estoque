﻿using Hino.Estoque.App.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Estoque.App.Models.Estoque
{
    public class ESCreateProdTransferenciaModel
    {
        [DisplayField]
        [RequiredField]
        public int CodEstab { get; set; }

        [DisplayField]
        [RequiredField]
        public string CodProduto { get; set; }

        [DisplayField]
        [RequiredField]
        public string CodEstoqueOri { get; set; }

        [DisplayField]
        [RequiredField]
        public string CodEstoqueDest { get; set; }

        [DisplayField]
        [RequiredField]
        public decimal Quantidade { get; set; }

        [DisplayField]
        [RequiredField]
        public string CodUsuario { get; set; }

        [DisplayField]
        public long Lote { get; set; }
    }
}
