using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Hino.Estoque.App.Models.Fiscal;
using Hino.Estoque.App.Models.Gerais;
using Hino.Estoque.App.Utils.Attributes;

namespace Hino.Estoque.App.Models.Estoque
{
    [EndPoint("Estoque/Inventario/Estab/{pCodEstab}")]
    public class ESInventarioModel : BaseEntity
    {
        public ESInventarioModel()
        {
            this.ESInventDet = new HashSet<ESInventDetModel>();
            this.ESKardex = new HashSet<ESKardexModel>();
        }

        public short codestab { get; set; }
        public decimal codinv { get; set; }
        public string codproduto { get; set; }
        public string codestoque { get; set; }
        public string status { get; set; }
        public DateTime datainv { get; set; }
        [NotMapped]
        public decimal contagem { get; set; }
        public decimal qtdecontag { get; set; }
        public DateTime? dataefet { get; set; }
        public decimal? contagefet { get; set; }
        [NotMapped]
        public string DetailDataInv { get => datainv.ToString("dd/MM/yy");  }

        [NotMapped]
        public string Detail { get => $"Inventário: {DetailDataInv}"; }
        [NotMapped]
        public string SubDetail { get => $"Contagem: {contagem}/{qtdecontag}"; }

        public virtual ICollection<ESInventDetModel> ESInventDet { get; set; }
        public virtual ICollection<ESKardexModel> ESKardex { get; set; }
        public virtual FSLocalEstoqueModel FSLocalEstoque { get; set; }
        public virtual FSProdutoModel FSProduto { get; set; }
        public virtual GEEstabModel GEEstab { get; set; }
    }
}
