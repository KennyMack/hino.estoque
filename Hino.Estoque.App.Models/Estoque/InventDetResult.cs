﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Estoque.App.Models.Estoque
{
    public class InventDetResult
    {
        public long IdMobile { get; set; }
        public int codinv { get; set; }
        public decimal contagem { get; set; }
        public string codproduto { get; set; }
        public decimal saldoinvent { get; set; }
    }
}
