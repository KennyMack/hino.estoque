using System;
using Hino.Estoque.App.Utils.Attributes;
namespace Hino.Estoque.App.Models.Gerais
{
    [EndPoint("General")]
    public class GEUsuariosModel : BaseEntity
    {
        public GEUsuariosModel()
        {
        }

        public string codusuario { get; set; }
        public string nome { get; set; }
        public bool status { get; set; }
        public byte tipusuario { get; set; }
        public string senha { get; set; }
        public string controle { get; set; }
        public string email { get; set; }
        public bool visualizasc { get; set; }
        public decimal vervalprogrec { get; set; }
        public string vervalromaneio { get; set; }
        public bool admincalendario { get; set; }
        public bool alteracomissao { get; set; }
        public bool alteratblprvig { get; set; }
        public bool visvlrultcmpsc { get; set; }
        public bool visvlrreqest { get; set; }
        public bool naopaginagrid { get; set; }
        public bool viscomprarestrito { get; set; }
        public bool permiteestoqneg { get; set; }
    }
}
