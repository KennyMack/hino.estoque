using System;
using System.Collections.Generic;
using Hino.Estoque.App.Utils.Attributes;
namespace Hino.Estoque.App.Models.Gerais
{
    [EndPoint("General")]
    public class GELogModel : BaseEntity
    {
        public GELogModel()
        {
            this.GELogDet = new HashSet<GELogDetModel>();
        }

        public decimal codlog { get; set; }
        public DateTime datalog { get; set; }
        public string tabela { get; set; }
        public string chavereg { get; set; }
        public string codusuario { get; set; }
        public string estacao { get; set; }
        public string acao { get; set; }

        public virtual ICollection<GELogDetModel> GELogDet { get; set; }
        public virtual GEUsuariosModel GEUsuarios { get; set; }
    }
}
