using System;
using System.Collections.Generic;
using Hino.Estoque.App.Models.Estoque;
using Hino.Estoque.App.Models.Fiscal;
using Hino.Estoque.App.Utils.Attributes;
namespace Hino.Estoque.App.Models.Gerais
{
    [EndPoint("Gerais/Estabelecimento")]
    public class GEEstabModel : BaseEntity
    {
        public GEEstabModel()
        {
            this.ESInventario = new HashSet<ESInventarioModel>();
            this.ESKardex = new HashSet<ESKardexModel>();
            this.ESLote = new HashSet<ESLoteModel>();
            this.ESTransferencia = new HashSet<ESTransferenciaModel>();
            this.FSProdutoparamEstab = new HashSet<FSProdutoparamEstabModel>();
            this.FSProdutoPcp = new HashSet<FSProdutoPcpModel>();
            this.GEFuncionarios = new HashSet<GEFuncionariosModel>();
            this.ESEnderecamento = new HashSet<ESEnderecamentoModel>();
        }

        public short codestab { get; set; }
        public string razaosocial { get; set; }
        public string nomefantasia { get; set; }
        public int? codendereco { get; set; }
        public byte codmoeda { get; set; }
        public decimal ativo { get; set; }
        public bool cdquantidade { get; set; }
        public byte cdvlunitario { get; set; }
        public bool cdvltotal { get; set; }
        public short cdpercentual { get; set; }
        public short? codnatopercompra { get; set; }
        public short? codnatopervenda { get; set; }
        public bool? gerarcontaempresa { get; set; }
        public string caminhoimagens { get; set; }
        public decimal cdtamanhocubico { get; set; }
        public short? codnatopercf { get; set; }
        public bool consestoque { get; set; }
        public string certificado { get; set; }
        public short? codgrupoestab { get; set; }
        public bool usaunnegocio { get; set; }
        public string tokenbuscacnpj { get; set; }

        public virtual ICollection<ESInventarioModel> ESInventario { get; set; }
        public virtual ICollection<ESKardexModel> ESKardex { get; set; }
        public virtual ICollection<ESLoteModel> ESLote { get; set; }
        public virtual ICollection<ESTransferenciaModel> ESTransferencia { get; set; }
        public virtual ICollection<FSProdutoparamEstabModel> FSProdutoparamEstab { get; set; }
        public virtual ICollection<FSProdutoPcpModel> FSProdutoPcp { get; set; }
        public virtual ICollection<GEFuncionariosModel> GEFuncionarios { get; set; }
        public virtual ICollection<ESEnderecamentoModel> ESEnderecamento { get; set; }
    }
}
