﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Estoque.App.Models.Gerais
{
    public class GEEstabSettingsModel
    {
        public GEEstabSettingsModel()
        {
            Settings = new GESettingsModel();
            Estab = new GEEstabModel();
        }
        public GEEstabModel Estab { get; set; }
        public GESettingsModel Settings { get; set; }
    }
}
