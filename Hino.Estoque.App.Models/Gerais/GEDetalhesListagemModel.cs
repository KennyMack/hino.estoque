﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Estoque.App.Models.Gerais
{
    public class GEDetalhesListagemModel
    {
        public string Description { get; set; }
        public string Data { get; set; }
    }
}
