using System;
using Hino.Estoque.App.Utils.Attributes;
namespace Hino.Estoque.App.Models.Gerais
{
    [EndPoint("General")]
    public class GELogDetModel : BaseEntity
    {
        public decimal codlog { get; set; }
        public string campo { get; set; }
        public string de { get; set; }
        public string para { get; set; }

        public virtual GELogModel gelog { get; set; }
    }
}
