﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Hino.Estoque.App.Models.Engenharia
{
    public class ENProcAgrupModel
    {
        public int codestab { get; set; }
        public int codgrupo { get; set; }
        public string descricao { get; set; }
        [NotMapped]
        public string Detail => $"{codgrupo} - {descricao}";
    }
}
