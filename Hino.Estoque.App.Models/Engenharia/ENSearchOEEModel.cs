﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Estoque.App.Models.Engenharia
{
    public class ENSearchOEEModel
    {
        public bool Loaded { get; set; }
        public ENMaquinasModel[] Maquinas { get; set; }
        public ENProcessosModel[] Processos { get; set; }
        public ENProcAgrupModel[] Agrupamentos { get; set; }
    }

    public enum PageOrigem
    {
        OEE = 0,
        OEEMENSAL = 1
    }
}
