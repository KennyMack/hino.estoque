﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Hino.Estoque.App.Models.Engenharia
{
    public class ENMaquinasModel
    {
        public short codestab { get; set; }
        public string codmaquina { get; set; }
        public string descricao { get; set; }
        public short codcelula { get; set; }
        public short? codprocesso { get; set; }

        [NotMapped]
        public string Detail => $"{codmaquina} - {descricao}";
    }
}
