﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Hino.Estoque.App.Models.Engenharia
{
    public class ENProcessosModel
    {
        public short codestab { get; set; }
        public short codprocesso { get; set; }
        public string descricao { get; set; }

        [NotMapped]
        public string Detail => $"{codprocesso} - {descricao}";
    }
}
