﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Estoque.App.Models.Manutencao
{
    public class MNCriarSolicManutModel
    {
        public short codestab { get; set; }
        public string codequipamento { get; set; }
        public DateTime datasolic { get; set; }
        public short codmotivo { get; set; }
        public string observacao { get; set; }
        public int codfuncionario { get; set; }
        public string prioridade { get; set; }
        public string codtipomanut { get; set; }
    }
}
