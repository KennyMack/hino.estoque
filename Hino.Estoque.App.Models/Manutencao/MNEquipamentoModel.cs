﻿using Hino.Estoque.App.Utils.Attributes;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Estoque.App.Models.Manutencao
{
    [EndPoint("Manutencao/Equipamentos/{pCodEstab}")]
    public class MNEquipamentoModel: BaseEntity
    {
        public short codestab { get; set; }
        public string codequipamento { get; set; }
        public string descricao { get; set; }
        public short status { get; set; }
        [Ignore]
        public string Detail => $"{codequipamento} - {descricao}";
    }
}
