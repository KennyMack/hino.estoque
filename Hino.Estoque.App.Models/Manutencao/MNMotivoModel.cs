﻿using Hino.Estoque.App.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Hino.Estoque.App.Models.Manutencao
{
    [EndPoint("Manutencao/Motivos/{pCodEstab}")]
    public class MNMotivoModel : BaseEntity
    {
        public short codmotivo { get; set; }
        public string descricao { get; set; }

        [NotMapped]
        public string Detail => $"{codmotivo} - {descricao}";
    }
}
