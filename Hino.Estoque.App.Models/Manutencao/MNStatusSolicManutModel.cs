﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Estoque.App.Models.Manutencao
{
    public class MNStatusSolicManutModel
    {
        public long codsolic { get; set; }
        public short codestab { get; set; }
        public int codfuncionario { get; set; }
        public int status { get; set; }
    }
}
