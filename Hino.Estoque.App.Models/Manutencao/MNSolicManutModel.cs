﻿using Hino.Estoque.App.Utils.Attributes;
using Hino.Estoque.App.Utils.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Hino.Estoque.App.Models.Manutencao
{
    [EndPoint("Manutencao/Solicitar/{pCodEstab}")]
    public class MNSolicManutModel : BaseEntity
    {
        public short codestab { get; set; }
        public long codsolic { get; set; }
        public string codequipamento { get; set; }
        public DateTime datasolic { get; set; }
        public string status { get; set; }
        public short codmotivo { get; set; }
        public string observacao { get; set; }
        public string solicitante { get; set; }
        public string aprovador { get; set; }
        public string prioridade { get; set; }
        [NotMapped]
        public string descstatus
        {
            get
            {
                switch (status)
                {
                    case "A":
                        return "Aprovada";
                    case "R":
                        return "Reprovada";
                    case "E":
                        return "Encerrada";
                    default:
                        return "Pendente";

                }
            }
        }
        [NotMapped]
        public string descprioridade
        {
            get
            {
                switch (prioridade)
                {
                    case "A":
                        return "Alta";
                    case "M":
                        return "Média";
                    default:
                        return "Baixa";

                }
            }
        }
        public string codtipomanut { get; set; }

        [NotMapped]
        public string DetailDataReq => datasolic.ToString("dd/MM/yy");
        [NotMapped]
        public string Detail => $"{codequipamento} - {Equipamento.descricao.SubStrTitle()}";
        [NotMapped]
        public string SubDetail => $"{descprioridade}   {descstatus}   {DetailDataReq}";

        [NotMapped]
        public bool HasNote => !observacao.IsEmpty();

        [NotMapped]
        public bool ShowButtons => status == "P";

        [NotMapped]
        public bool IsBaixa => prioridade == "B";

        [NotMapped]
        public bool IsMedia => prioridade == "M";

        [NotMapped]
        public bool IsAlta => prioridade == "A";

        public virtual MNEquipamentoModel Equipamento { get; set; }
        public virtual MNTipoManutModel TipoManut { get; set; }
        public virtual MNMotivoModel Motivo { get; set; }
    }
}
