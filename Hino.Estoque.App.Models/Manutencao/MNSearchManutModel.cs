﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Estoque.App.Models.Manutencao
{
    public class MNSearchManutModel
    {
        public bool Loaded { get; set; }
        public MNMotivoModel[] Motivos { get; set; }
        public MNEquipamentoModel[] Equipamentos { get; set; }
        public MNTipoManutModel[] TipoManut { get; set; }
    }
}
