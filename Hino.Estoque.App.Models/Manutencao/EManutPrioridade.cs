﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Estoque.App.Models.Manutencao
{
    public enum EManutPrioridade
    {
        BAIXA = 0,
        MEDIA = 1,
        ALTA = 2
    }
}
