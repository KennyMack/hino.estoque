﻿using Hino.Estoque.App.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Hino.Estoque.App.Models.Manutencao
{
    [EndPoint("Manutencao/Tipos/{pCodEstab}")]
    public class MNTipoManutModel : BaseEntity
    {
        public string codtipomanut { get; set; }
        public string descricao { get; set; }
        public EManutPrioridade prioridade { get; set; }
        public bool enviaemail { get; set; }
        public string email { get; set; }
        public string codusuario { get; set; }
        public short? codmotivo { get; set; }

        [NotMapped]
        public string Detail => $"{codtipomanut} - {descricao}";
    }
}
