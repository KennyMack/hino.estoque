﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Estoque.App.Models.Manutencao
{
    public class MNSolicForUserModel
    {
        public short codestab { get; set; }
        public int codfuncionario { get; set; }
        public string codusuario { get; set; }
    }
}
