﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Estoque.App.Models.Producao
{
    public class PDOPTransfResultModel
    {
        public PDOPTransfResultModel()
        {
            OrdemProd = new PDOrdemProdModel();
            Transferencias = new List<PDOPTransfItemResultModel>();
        }

        public PDOrdemProdModel OrdemProd { get; set; }
        public List<PDOPTransfItemResultModel> Transferencias { get; set; }
    }

    public class PDOPTransfItemResultModel
    {
        public PDOPTransfModel Transferencia { get; set; }
        public bool Rastreabilidade { get; set; }
        public bool Sucesso { get; set; }
        public string Motivo { get; set; }
        public bool Validado { get; set; }
        public decimal QtdSeparada { get; set; }
        public decimal StockBalance { get; set; }
    }
}
