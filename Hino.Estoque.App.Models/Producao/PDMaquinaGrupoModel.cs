﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;

namespace Hino.Estoque.App.Models.Producao
{
    public class PDMaquinaGrupoModel: ObservableCollection<PDMaquinaEficDetalheModel>, INotifyPropertyChanged
    {

        private bool _Expanded;
        public bool Expanded
        {
            get => _Expanded;
            set
            {
                if (_Expanded != value)
                {
                    _Expanded = value;

                    OnPropertyChanged(new PropertyChangedEventArgs("Expanded"));
                    OnPropertyChanged(new PropertyChangedEventArgs("StateIcon"));
                }
            }
        }

        public string StateIcon
        {
            get => Expanded ? "X" : "Y";
        }

        public string Maquina { get; set; }
        public string Processo { get; set; }
        public string Turno { get; set; }
        public decimal QtdMaquinas { get; set; }
        public decimal EficTotal { get; set; }

        public PDMaquinaEficDetalheModel[] Detalhes { get; set; }
    }
}
