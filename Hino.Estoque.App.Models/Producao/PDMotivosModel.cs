﻿using Hino.Estoque.App.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Estoque.App.Models.Producao
{
    [EndPoint("Producao/Motivos/{pCodEstab}")]
    public class PDMotivosModel : BaseEntity
    {
        public int codmotivo { get; set; }
        public string descricao { get; set; }
        /// <summary>
        /// 0 'PARADA'
        /// 1 'REFUGO'
        /// 2 'RETRABALHO'
        /// 3 'PARADA PROGRAMADA'
        /// </summary>
        public short tipo { get; set; }
        public bool status { get; set; }
    }
}
