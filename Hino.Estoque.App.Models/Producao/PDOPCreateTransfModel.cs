﻿using Hino.Estoque.App.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Estoque.App.Models.Producao
{
    [EndPoint("Estoque/Transferencias/Estab/{pCodEstab}")]
    public class PDOPCreateTransfModel : BaseEntity
    {
        public PDOPCreateTransfModel()
        {
        }

        [DisplayField]
        [RequiredField]
        [IntegerRangeField(0, 999)]
        public short codestab { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal codordprod { get; set; }
        [DisplayField]
        [RequiredField]
        public string nivelordprod { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal codestrutura { get; set; }
        [DisplayField]
        [RequiredField]
        [IntegerRangeField(0, 999)]
        public int codroteiro { get; set; }
        [DisplayField]
        [RequiredField]
        [IntegerRangeField(0, 999)]
        public int operacao { get; set; }
        [DisplayField]
        [RequiredField]
        public string codcomponente { get; set; }
        [DisplayField]
        [RequiredField]
        public string codestoqueori { get; set; }
        [DisplayField]
        [RequiredField]
        public string codestoquedest { get; set; }
        [DisplayField]
        [RequiredField]
        public string codusuario { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal quantidade { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal qtdseparada { get; set; }

        public bool complete { get; set; }

        public string observacao { get; set; }

        public string Identificador { get; set; }

        public DateTime datatransf { get; set; }
    }
}
