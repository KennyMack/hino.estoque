﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Estoque.App.Models.Producao.Apontamento
{
    public class PDAptLancRefugoModel : BaseEntity
    {
        public int CodMotivo { get; set; }
        public string Descricao { get; set; }
        public string Motivo { get => $"{CodMotivo} - {Descricao}"; }
        public decimal Quantidade { get; set; }
        public decimal QtdSucata { get; set; }
        public decimal QtdReaproveitada { get; set; }
        public bool ApontarPeso { get; set; }
    }
}
