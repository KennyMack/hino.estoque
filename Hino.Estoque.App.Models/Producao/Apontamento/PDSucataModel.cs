﻿using Hino.Estoque.App.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Estoque.App.Models.Producao.Apontamento
{
    [EndPoint("Producao/Lancamentos/{pCodEstab}")]
    public class PDSucataModel : BaseEntity
    {
        public short codestab { get; set; }
        public long codsucata { get; set; }
        public DateTime datasuc { get; set; }
        public long? lote { get; set; }
        public string codproduto { get; set; }
        public string codestoque { get; set; }
        public decimal qtdeproduto { get; set; }
        public string codprdsuc { get; set; }
        public string codestsuc { get; set; }
        public decimal qtdesucata { get; set; }
        public int codmotivo { get; set; }
        public decimal codfuncionario { get; set; }
        public decimal codordprod { get; set; }
        public string nivelordprod { get; set; }
        public decimal codestrutura { get; set; }
        public long? codlancamento { get; set; }
    }
    public class PDSucataCreateVM 
    {
        public short codestab { get; set; }
        public long? lote { get; set; }
        public string codproduto { get; set; }
        public string codestoque { get; set; }
        public decimal qtdeproduto { get; set; }
        public string codprdsuc { get; set; }
        public string codestsuc { get; set; }
        public decimal qtdesucata { get; set; }
        public int codmotivo { get; set; }
        public decimal codfuncionario { get; set; }
        public decimal codordprod { get; set; }
        public string nivelordprod { get; set; }
        public decimal codestrutura { get; set; }
        public long? codlancamento { get; set; }
    }
}
