﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Hino.Estoque.App.Models.Producao.Apontamento
{
    public class PDAptLancamentosModel : BaseEntity
    {
        public PDAptLancamentosModel()
        {
            Refugos = new HashSet<PDAptLancRefugoModel>();
        }

        public long CodAptInicio { get; set; }
        public long? CodAptTermino { get; set; }
        public long? CodLancamento { get; set; }
        public string Status => DtTermino == null ? "Andamento" : "Finalizado";
        public string StatusColor => Status == "Andamento" ? "#ffd700" : "#05a05a";
        public short Tipo { get; set; }
        public string TipoLancamento
        {
            get
            {
                switch (Tipo)
                {
                    case 1:
                        return "Preparação";
                    case 2:
                        return "Preparação";
                    case 3:
                        return "Produção";
                    case 4:
                        return "Produção";
                    case 5:
                        return "Troca de serviço";
                    case 6:
                        return "Troca de serviço";
                    case 7:
                        return "Manutenção";
                    case 8:
                        return "Manutenção";
                    case 9:
                        return "Consulta desenho";
                    case 10:
                        return "Consulta desenho";
                    case 11:
                        return "Retrabalho";
                    case 12:
                        return "Retrabalho";
                    case 13:
                        return "Produção";
                    default:
                        return "";
                }
            }
        }
        public DateTime DtInicio { get; set; }
        public DateTime? DtTermino { get; set; }
        public long CodOrdProd { get; set; }
        public string NivelOrdProd { get; set; }
        public string OrdemProd => CodLancamento > 0 ? $"OP: {CodOrdProd} - {NivelOrdProd} - Lanc: {CodLancamento}"  : $"OP: {CodOrdProd} - {NivelOrdProd}";
        public int CodRoteiro { get; set; }
        public int Operacao { get; set; }
        public string DescOperacao { get; set; }
        public string OperacaoItem { get => $"{Operacao} - {DescOperacao}"; }
        public string Turno { get; set; }
        public string CodMaquina { get; set; }
        public string CodProduto { get; set; }
        public string Descricao { get; set; }
        public string Produto { get => $"{CodProduto} - {Descricao}"; }
        public long CodEstrutura { get; set; }

        public decimal Quantidade { get; set; }
        public decimal QtdRefugo { get; set; }
        public int CodFuncionario { get; set; }
        public string NomeFuncionario { get; set; }

        public string Funcionario { get => $"{CodFuncionario} - {NomeFuncionario}"; }
        public string CodUsuario { get; set; }
        public bool ShowProdutivity
        {
            get => Tipo == 3 || Tipo == 4 || Tipo == 13;
        }

        public int TotalMin { get; set; }
        public string DescTotalMin { get; set; }
        public int TotalMinEsp { get; set; }
        public string DescTotalMinEsp { get; set; }
        public int Produtividade { get; set; }

        public ICollection<PDAptLancRefugoModel> Refugos { get; set; }
    }
}
