﻿using Hino.Estoque.App.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Estoque.App.Models.Producao.Apontamento
{
    [EndPoint("Producao/Lancamentos/{pCodEstab}")]
    public class PDAptInicioProdModel : BaseEntity
    {
        public long codiniapt { get; set; }
        public int codestab { get; set; }
        public decimal codordprod { get; set; }
        public string nivelordprod { get; set; }
        public decimal codestrutura { get; set; }
        public int codroteiro { get; set; }
        public int operacao { get; set; }
        public decimal codfuncionario { get; set; }
        public string codusuario { get; set; }
        public DateTime dtinicio { get; set; }
        public string codmaquina { get; set; }
        /// <summary>
        /// 1 - Inicio Preparação
        /// 2 - Fim Preparação
        /// 3 - Inicio de Produção
        /// 4 - Fim de Produção
        /// 5 - Inicio Troca de Serviço
        /// 6 - Fim Troca de Serviço
        /// 7 - Inicio de Manutenção
        /// 8 - Fim de Manutenção
        /// 9 - Inicio Consulta Desenho
        /// 10 - Fim Consulta Desenho
        /// 11 - Início Retrabalho
        /// 12 - Fim Retrabalho
        /// 13 - Fim turno
        /// </summary>
        public short tipo { get; set; }
        public int? codmotivo { get; set; }
    }
}
