﻿using Hino.Estoque.App.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Estoque.App.Models.Producao.Apontamento
{
    [EndPoint("Producao/Lancamentos/{pCodEstab}")]
    public class PDAptTerminoProdModel : BaseEntity
    {
        public PDAptTerminoProdModel()
        {
            Refugos = new HashSet<PDAptLancRefugoModel>();
        }

        public long codfimapt { get; set; }
        public short codestab { get; set; }
        public long codiniapt { get; set; }
        public string codusuario { get; set; }
        public int codfuncionario { get; set; }
        public DateTime dttermino { get; set; }
        /// <summary>
        /// 1 - Inicio Preparação
        /// 2 - Fim Preparação
        /// 3 - Inicio de Produção
        /// 4 - Fim de Produção
        /// 5 - Inicio Troca de Serviço
        /// 6 - Fim Troca de Serviço
        /// 7 - Inicio de Manutenção
        /// 8 - Fim de Manutenção
        /// 9 - Inicio Consulta Desenho
        /// 10 - Fim Consulta Desenho
        /// 11 - Início Retrabalho
        /// 12 - Fim Retrabalho
        /// 13 - Termino turno
        /// </summary>
        public short tipo { get; set; }
        public decimal quantidade { get; set; }
        public int? codmotivo { get; set; }
        public decimal qtdrefugo { get; set; }
        public decimal qtdretrabalho { get; set; }
        public string observacao { get; set; }
        public bool askstartnew { get; set; }

        public ICollection<PDAptLancRefugoModel> Refugos { get; set; }
    }
}
