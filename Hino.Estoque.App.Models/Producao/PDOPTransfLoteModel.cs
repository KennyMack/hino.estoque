using System;
using Hino.Estoque.App.Models.Estoque;
using Hino.Estoque.App.Utils.Attributes;
namespace Hino.Estoque.App.Models.Producao
{
    [EndPoint("Producao")]
    public class PDOPTransfLoteModel : BaseEntity
    {
        public short codestab { get; set; }
        public int codtransf { get; set; }
        public decimal lote { get; set; }
        public decimal quantidade { get; set; }

        public string Detail { get => $"Lote: {lote} - Qtd: {quantidade:n4}"; }

        public virtual ESLoteModel eslote { get; set; }
        public virtual PDOPTransfModel pdoptransf { get; set; }
    }
}
