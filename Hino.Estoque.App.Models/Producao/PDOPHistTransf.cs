﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Hino.Estoque.App.Models.Producao
{
    public class PDOPHistTransf
    {
        public long IdMobile { get; set; }
        public DateTime DataTransf { get; set; }
        public string OP { get; set; }
        public string CodProduto { get; set; }
        public string CodEstoqueOri { get; set; }
        public string CodEstoqueDest { get; set; }
        public decimal Quantidade { get; set; }
        public decimal QtdSeparada { get; set; }
        public bool Complete { get; set; }
        public string Observacao { get; set; }
        public Color Color { get; set; }
    }
}
