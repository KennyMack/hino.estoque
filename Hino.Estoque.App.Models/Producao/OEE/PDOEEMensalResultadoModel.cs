﻿using Hino.Estoque.App.Utils.Extensions;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Hino.Estoque.App.Models.Producao.OEE
{
    public class PDOEEMensalResultadoModel
    {
        public string codmaq { get; set; }
        public string enmaquinasdescricao { get; set; }
        public string maquinadetalhe => $"{codmaq} - {enmaquinasdescricao}".SubStrTitle(40);
        public decimal meta { get; set; }
        public decimal producao { get; set; }
        public decimal refugo { get; set; }
        public string horastrab { get; set; }
        public string horasparada { get; set; }
        public decimal oee { get; set; }
        public Color oeeColor { get; set; }
        public decimal disponibilidade { get; set; }
        public decimal produtividade { get; set; }
        public decimal qualidade { get; set; }
    }
}
