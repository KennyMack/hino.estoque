﻿using System;
using System.Drawing;

namespace Hino.Estoque.App.Models.Producao.OEE
{
    public class PDOEEResultadoModel
    {
        public DateTime Date { get; set; }
        public decimal Meta { get; set; }
        public decimal Producao { get; set; }
        public decimal Refugo { get; set; }
        public string HorasTotais { get; set; }
        public string HorasDisponiveis { get; set; }
        public string HorasTrabalhadas { get; set; }
        public string HorasParadas { get; set; }
        public decimal OEE { get; set; }
        public Color OEEColor { get; set; }
        public decimal EficProducao { get; set; }
        public Color EficProducaoColor { get; set; }
        public decimal Disponibilidade { get; set; }
        public decimal Qualidade { get; set; }
        public bool TemJustificativa { get; set; }
        public Color JustificativaColor => TemJustificativa ? Color.FromArgb(38, 122, 252) : Color.FromArgb(150, 150, 150);
    }
}
