﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Hino.Estoque.App.Models.Producao.OEE
{
    public class PDOEEResultModel
    {
        public PDOEEResultModel()
        {
            Results = new List<PDOEEListagemModel>();
            Colors = new List<PDOEEParam>();
            Justificativas = new List<PDOEEJustificativa>();
        }

        public List<PDOEEListagemModel> Results { get; set; }
        public List<PDOEEParam> Colors { get; set; }
        public List<PDOEEJustificativa> Justificativas { get; set; }
    }

    public class PDOEEParam
    {
        public short codestab { get; set; }
        public short tipo { get; set; }
        public short cor { get; set; }

        public Color CorEscolhida
        {
            get
            {
                switch (cor)
                {
                    // vermelha
                    case 0:
                        return Color.FromArgb(184, 0, 0);
                    // amarela
                    case 1:
                        return Color.FromArgb(255, 215, 0);
                    // verde
                    case 2:
                        return Color.FromArgb(5, 160, 90);
                    default:
                        return Color.Black;
                }
            }
        }
        public decimal inicio { get; set; }
        public decimal termino { get; set; }
    }

    public class PDOEEJustificativa
    {
        public short codestab { get; set; }
        public int codjustifica { get; set; }
        public string codmaquina { get; set; }
        public short tipo { get; set; }
        public DateTime dataoee { get; set; }
        public DateTime datajustificativa { get; set; }
        public string justificativa { get; set; }
    }

    public class PDOEEListagemModel
    {
        public int CodSeq { get; set; }
        public string Campo { get; set; }
        public string C_01 { get; set; }
        public string C_02 { get; set; }
        public string C_03 { get; set; }
        public string C_04 { get; set; }
        public string C_05 { get; set; }
        public string C_06 { get; set; }
        public string C_07 { get; set; }
        public string C_08 { get; set; }
        public string C_09 { get; set; }
        public string C_10 { get; set; }
        public string C_11 { get; set; }
        public string C_12 { get; set; }
        public string C_13 { get; set; }
        public string C_14 { get; set; }
        public string C_15 { get; set; }
        public string C_16 { get; set; }
        public string C_17 { get; set; }
        public string C_18 { get; set; }
        public string C_19 { get; set; }
        public string C_20 { get; set; }
        public string C_21 { get; set; }
        public string C_22 { get; set; }
        public string C_23 { get; set; }
        public string C_24 { get; set; }
        public string C_25 { get; set; }
        public string C_26 { get; set; }
        public string C_27 { get; set; }
        public string C_28 { get; set; }
        public string C_29 { get; set; }
        public string C_30 { get; set; }
        public string C_31 { get; set; }
    }
}
