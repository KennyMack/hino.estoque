﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Estoque.App.Models.Producao.OEE
{
    public class PDOEEMensalResultModel
    {
        public PDOEEMensalResultModel()
        {
            Results = new List<PDOEEMensalResultadoModel>();
            Colors = new List<PDOEEParam>();
        }

        public List<PDOEEMensalResultadoModel> Results { get; set; }
        public List<PDOEEParam> Colors { get; set; }
    }
}
