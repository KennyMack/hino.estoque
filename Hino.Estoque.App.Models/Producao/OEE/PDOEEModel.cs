﻿using Hino.Estoque.App.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Estoque.App.Models.Producao.OEE
{
    [EndPoint("Producao/OEE/{pCodEstab}")]
    public class PDOEEModel : BaseEntity
    {
    }

    public class PDGerarOEEModel
    {
        [RequiredField]
        public short codestab { get; set; }
        [RequiredField]
        public string codusuario { get; set; }
        [RequiredField]
        public DateTime dataini { get; set; }
        [RequiredField]
        public DateTime datafim { get; set; }
        [RequiredField]
        public short? codprocesso { get; set; }
        [RequiredField]
        public string codmaquina { get; set; }
        public int? codagrup { get; set; }
        [RequiredField]
        public string turno { get; set; }
        [RequiredField]
        public int gerardet { get; set; }

    }
}
