﻿using System;
using System.Collections.Generic;
using System.Text;
using Hino.Estoque.App.Utils.Attributes;
using SQLite;

namespace Hino.Estoque.App.Models.Producao
{
    [EndPoint("Producao")]
    [Table("PDOrdemProdCompModel")]
    public class PDOrdemProdCompEstqEndModel : BaseEntity
    {
        public short codestab { get; set; }
        public decimal codordprod { get; set; }
        public string nivelordprod { get; set; }
        public string codestoque { get; set; }
        public string codproduto { get; set; }
        public string descricao { get; set; }
        public decimal saldodisponivel { get; set; }
    }
}
