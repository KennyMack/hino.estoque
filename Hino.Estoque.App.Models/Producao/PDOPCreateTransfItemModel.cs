﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace Hino.Estoque.App.Models.Producao
{
    [Table("PDOPCreateTransfItemModel")]
    public class PDOPCreateTransfItemModel : BaseEntity
    {
        public short codestab { get; set; }
        public decimal codordprod { get; set; }
        public string nivelordprod { get; set; }
        public decimal codestrutura { get; set; }
        public int codroteiro { get; set; }
        public int operacao { get; set; }
        public string codcomponente { get; set; }
        public string codestoqueori { get; set; }
        public string codestoquedest { get; set; }
        public decimal qtdseparada { get; set; }
        public DateTime datatransf { get; set; }
        public string Identificador { get; set; }
    }
}
