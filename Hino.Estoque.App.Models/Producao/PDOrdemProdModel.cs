using System;
using System.Collections.Generic;
using Hino.Estoque.App.Models.Estoque;
using Hino.Estoque.App.Models.Fiscal;
using Hino.Estoque.App.Utils.Attributes;
using SQLite;

namespace Hino.Estoque.App.Models.Producao
{
    [EndPoint("Producao/Ordem/Producao/{pCodEstab}")]
    public class PDOrdemProdModel : BaseEntity
    {
        public PDOrdemProdModel()
        {
            this.estransferencia = new HashSet<ESTransferenciaModel>();
            this.pdordemprodrotinas = new HashSet<PDOrdemProdRotinasModel>();
            this.pdordemprodcomp = new HashSet<PDOrdemProdCompModel>();
        }

        public short codestab { get; set; }
        public decimal codprograma { get; set; }
        public decimal codordprod { get; set; }
        public string nivelordprod { get; set; }
        public decimal codordprodpai { get; set; }
        public string nivelordprodpai { get; set; }
        public int codroteiro { get; set; }
        public decimal codestrutura { get; set; }
        public string codproduto { get; set; }
        public DateTime dtinicio { get; set; }
        public DateTime dttermino { get; set; }
        public DateTime? dtencerramento { get; set; }
        public decimal programado { get; set; }
        public decimal realizado { get; set; }
        public decimal refugado { get; set; }
        public string status { get; set; }
        public decimal ultimasequencia { get; set; }
        public decimal fator { get; set; }
        public decimal qtdinicial { get; set; }
        public decimal custoinicial { get; set; }
        public decimal custoreproc { get; set; }
        public int seqbarras { get; set; }
        public int? codagrupfoto { get; set; }
        public bool liberado { get; set; }
        public DateTime dtemissao { get; set; }
        public bool usaopestoque { get; set; }
        public bool usasaldoestoque { get; set; }
        public bool ShowResultOP { get; set; }
        public virtual ICollection<ESTransferenciaModel> estransferencia { get; set; }
        public virtual FSProdutoModel fsproduto { get; set; }
        public virtual ICollection<PDOrdemProdRotinasModel> pdordemprodrotinas { get; set; }
        public virtual ICollection<PDOrdemProdCompModel> pdordemprodcomp { get; set; }
        [Ignore]
        public string OPLancDescription => $"{codordprod} - {nivelordprod}";
        [Ignore]
        public string OPLancProduct => $"{codproduto} - {fsproduto.descricao}";
    }
}
