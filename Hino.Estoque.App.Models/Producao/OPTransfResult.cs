﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Estoque.App.Models.Producao
{
    public class OPTransfResult
    {
        public string Result { get; set; }
        public bool Success { get; set; }
        public PDOPTransfResultModel TransfResults { get; set; }
    }
}
