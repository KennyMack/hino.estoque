using System;
using Hino.Estoque.App.Models.Gerais;
using Hino.Estoque.App.Utils.Attributes;
namespace Hino.Estoque.App.Models.Producao
{
    [EndPoint("Producao")]
    public class PDParamEstabModel : BaseEntity
    {
        public short codestab { get; set; }
        public bool calculandomps { get; set; }
        public bool calculandomrp { get; set; }
        public string leadtimeop { get; set; }
        public bool exigemaqapto { get; set; }
        public bool exigeturnoapto { get; set; }
        public string aceitaprgmaior { get; set; }
        public string encerraautomatico { get; set; }
        public string geraopfilhos { get; set; }
        public bool recalcopaltcomp { get; set; }
        public decimal regramaqaponta { get; set; }
        public decimal contapontproc { get; set; }
        public decimal consumolote { get; set; }
        public decimal fifolotesconsumo { get; set; }
        public bool inputsensor { get; set; }
        public bool apontalogin { get; set; }
        public bool custoapont { get; set; }
        public bool taxascustoapt { get; set; }
        public decimal materiaiscustoapt { get; set; }
        public bool contestoqref { get; set; }
        public bool validadtconsumo { get; set; }
        public bool validadtsucata { get; set; }
        public decimal validadtapont { get; set; }
        public decimal minutosapont { get; set; }
        public bool transfmodo { get; set; }
        public bool tempovalapont { get; set; }
        public int? codmotsuc { get; set; }
        public bool horamanualapt { get; set; }
        public bool calcoee { get; set; }
        public bool imprimelanc { get; set; }
        public int numseqfotolito { get; set; }
        public int? codrotencomenda { get; set; }
        public int? codmotrefugo { get; set; }
        public int? codmotparada { get; set; }
        public int? codmotretrab { get; set; }
        public int? codmotsetup { get; set; }
        public int? codmotdesenho { get; set; }
        public int? codmottrserv { get; set; }
        public int? codmotmanut { get; set; }
        public string turnopadrao { get; set; }
        public string codmaqpadrao { get; set; }
        public bool procsequencial { get; set; }
        public decimal situacaoop { get; set; }
        public bool aptproducao { get; set; }
        public bool aptsetup { get; set; }
        public bool apttrocaserv { get; set; }
        public bool aptmanut { get; set; }
        public bool aptconsdesenho { get; set; }
        public bool aptretrab { get; set; }
        public bool exigetransfini { get; set; }
        public bool validacompoper { get; set; }
        public bool valaptoperanterior { get; set; }
        public bool usaempenho { get; set; }
        public bool reapautomatico { get; set; }
        public bool validaopsaldo { get; set; }
        public bool usasaldoestoque { get; set; }

        public virtual GEEstabModel geestab { get; set; }
    }
}
