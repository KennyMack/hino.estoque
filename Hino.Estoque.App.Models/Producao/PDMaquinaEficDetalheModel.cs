﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Estoque.App.Models.Producao
{
    public class PDMaquinaEficDetalheModel
    {
        public string OP { get; set; }
        public string Maquina { get; set; }
        public string Produto { get; set; }
        public decimal Eficiencia { get; set; }
        public decimal MetaDia { get; set; }
        public decimal TotalProgramado { get; set; }
        public decimal QtdeProd { get; set; }
        public decimal QtdeRef { get; set; }
        public decimal PercRefugo { get; set; }

        public string DetalheProducao
        {
            get => $"{TotalProgramado:n4} / {QtdeProd:n4} / {QtdeRef:n4}";
        }
    }
}
