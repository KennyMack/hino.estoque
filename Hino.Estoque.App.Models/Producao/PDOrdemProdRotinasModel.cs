using System;
using System.Collections.Generic;
using Hino.Estoque.App.Utils.Attributes;
using SQLite;

namespace Hino.Estoque.App.Models.Producao
{
    [EndPoint("Producao")]
    public class PDOrdemProdRotinasModel : BaseEntity
    {
        public PDOrdemProdRotinasModel()
        {
            this.pdlancamentos = new HashSet<PDLancamentosModel>();
            this.pdordemprodcomp = new HashSet<PDOrdemProdCompModel>();
        }

        public short codestab { get; set; }
        public decimal codordprod { get; set; }
        public string nivelordprod { get; set; }
        public decimal codestrutura { get; set; }
        public int codroteiro { get; set; }
        public int operacao { get; set; }
        public short codrotina { get; set; }
        [Ignore]
        public string DetailOperacao
        {
            get => $"{operacao} - {descricao}";
        }
        public string descricao { get; set; }
        public string codmaquina { get; set; }
        public decimal prodhoraria { get; set; }
        public decimal operador { get; set; }
        public bool aptoprocesso { get; set; }
        public bool aptoacabado { get; set; }
        public decimal qtdeproducao { get; set; }
        public decimal qtderefugo { get; set; }
        // public DateTime dtinicio { get; set; }
        // public DateTime dttermino { get; set; }
        // public decimal alteradomanual { get; set; }

        public virtual ICollection<PDLancamentosModel> pdlancamentos { get; set; }
        public virtual PDOrdemProdModel pdordemprod { get; set; }
        public virtual ICollection<PDOrdemProdCompModel> pdordemprodcomp { get; set; }
    }
}
