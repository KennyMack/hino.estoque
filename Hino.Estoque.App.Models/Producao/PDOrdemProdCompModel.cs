using System;
using System.Collections.Generic;
using System.Linq;
using Hino.Estoque.App.Models.Fiscal;
using Hino.Estoque.App.Utils.Attributes;
using Hino.Estoque.App.Utils.Extensions;
using SQLite;

namespace Hino.Estoque.App.Models.Producao
{
    [EndPoint("Producao")]
    [Table("PDOrdemProdCompModel")]
    public class PDOrdemProdCompModel : BaseEntity
    {
        public PDOrdemProdCompModel()
        {
            this.pdoptransf = new HashSet<PDOPTransfModel>();
        }

        public short codestab { get; set; }
        public decimal codordprod { get; set; }
        public string nivelordprod { get; set; }
        public decimal codestrutura { get; set; }
        public int codroteiro { get; set; }
        public int operacao { get; set; }
        public string codcomponente { get; set; }
        public string codestoque { get; set; }
        public decimal quantidade { get; set; }
        public decimal qtdconsumida { get; set; }
        public bool baixaapto { get; set; }
        public decimal variacao { get; set; }
        public decimal perda { get; set; }
        public bool reaproveita { get; set; }
        public decimal altmanual { get; set; }
        public decimal fator { get; set; }
        public decimal geraconsudif { get; set; }
        public decimal sequencia { get; set; }
        public decimal camadas { get; set; }
        public decimal passadas { get; set; }
        public byte nivel { get; set; }
        public short? codtecido { get; set; }
        public decimal percagrup { get; set; }
        public string codunidade { get; set; }
        public decimal qtdeeng { get; set; }
        public bool separacao { get; set; }

        public decimal PDOPTransfQtdSep { get; set; }

        decimal _qtdSeparada = 0;
        public decimal qtdSeparada
        {
            get { return _qtdSeparada; }
            set { SetProperty(ref _qtdSeparada, value); }
        }

        decimal _QtdSeparadaTotal = 0;
        public decimal QtdSeparadaTotal
        {
            get { return _QtdSeparadaTotal; }
            set { SetProperty(ref _QtdSeparadaTotal, value); }
        }

        string _Readouts = "";
        public string Readouts
        {
            get { return _Readouts; }
            set { SetProperty(ref _Readouts, value); }
        }

        public string FSProduto_GTIN { get; set; }
        public string FSProduto_GTINEmbalagem { get; set; }
        public string FSProduto_descricao { get; set; }
        public decimal FSProduto_separacaomaximo { get; set; }
        public int? ESEnderecamento_codenderecamento { get; set; }
        public string ESEnderecamento_descricao { get; set; }
        
        public string ESEnderecamento_sigla { get; set; }
        public string ESEnderecamento_rua { get; set; }
        public string ESEnderecamento_nivel { get; set; }
        public string ESEnderecamento_posicao { get; set; }
        public string Estoque_Enderecos { get; set; }
        public string codestoqueori { get; set; }
        public decimal saldoorigem { get; set; }
        public string codestoquedest { get; set; }
        public decimal saldodestino { get; set; }
        public bool separado { get; set; }
        public bool loadqtd { get; set; }

        [Ignore]
        public string ProductDescription { get => $"{codcomponente} - {FSProduto_descricao}"; }

        [Ignore]
        public string Detail { get => $"OP: {codordprod}/{nivelordprod}"; }

        [Ignore]
        public string DetailOrigin
        {
            get => 
                $"Ori.: {codestoqueori} / {saldoorigem:n4}";
        }

        [Ignore]
        public string DetailDest { get => $"Des.: {codestoquedest} / {saldodestino:n4}"; }

        [Ignore]
        public string Detail3
        {
            get
            {
                if (!string.IsNullOrEmpty(ESEnderecamento_descricao))
                    return $@"{ESEnderecamento_descricao} - {ESEnderecamento_sigla} (R:{ESEnderecamento_rua} N:{ESEnderecamento_nivel} P:{ESEnderecamento_posicao})";

                return "";
            }
        }

        [Ignore]
        public string DetailQuantity { get => FSProduto_separacaomaximo > 0 ?
                $"Qtd. Nec.: {quantidade:n4} - Lim.: {FSProduto_separacaomaximo:n4} / Sep.:{PDOPTransfQtdSep:n4}" :
                $"Qtd. Nec.: {quantidade:n4} / Sep.:{PDOPTransfQtdSep:n4}"; }


        [Ignore]
        public string DetailQuantityStock
        {
            get
            {
                var QtdTaken = PDOPTransfQtdSep;
                if (QtdSeparadaTotal > 0)
                    QtdTaken = QtdSeparadaTotal;

               return FSProduto_separacaomaximo > 0 ?
                    $"Nec.: {quantidade:n4} - Lim.: {FSProduto_separacaomaximo:n4} / Sep.:{QtdTaken:n4}" :
                    $"Nec.: {quantidade:n4} / Sep.:{QtdTaken:n4}";
            }
        }

        [Ignore]
        public virtual FSLocalEstoqueModel fslocalestoque { get; set; }
        [Ignore]
        public virtual FSProdutoModel fsproduto { get; set; }
        [Ignore]
        public virtual ICollection<PDOPTransfModel> pdoptransf { get; set; }
        [Ignore]
        public virtual PDOrdemProdModel pdordemprod { get; set; }
        [Ignore]
        public virtual PDOrdemProdRotinasModel pdordemprodrotinas { get; set; }
        [Ignore]
        public List<PDOrdemProdCompEstqEndModel> ordemprodcompestqend { get; set; }
        [Ignore]
        public string OriginLocations
        {
            get
            {
                if (ordemprodcompestqend != null && ordemprodcompestqend.Any())
                    return string.Join("\r\n", ordemprodcompestqend
                        .Select(s => $"Ori.: {s.codestoque} / {s.saldodisponivel:n4}")
                        .ToArray());
                else
                    return DetailOrigin;
            }
        }

        [Ignore]
        public string TakenOriginLocations
        {
            get
            {
                if (Readouts.IsEmpty() && ordemprodcompestqend != null && ordemprodcompestqend.Any())
                {
                    var result = new List<PDOrdemProdCompEstqEndModel>();

                    var QtdNec = quantidade - PDOPTransfQtdSep;
                    var QtdAcum = 0M;

                    foreach (var Linha in ordemprodcompestqend)
                    {
                        var NewLine = new PDOrdemProdCompEstqEndModel
                        {
                            codestoque = Linha.codestoque,
                            codproduto = Linha.codproduto,
                            descricao = Linha.descricao,
                            saldodisponivel = 0,
                        };
                        if (Linha.saldodisponivel >= (QtdNec - QtdAcum))
                        {
                            NewLine.saldodisponivel = QtdNec - QtdAcum;
                            QtdAcum = QtdNec;
                        }
                        else
                        {
                            NewLine.saldodisponivel = Linha.saldodisponivel;
                            QtdAcum += Linha.saldodisponivel;
                        }

                        result.Add(NewLine);

                        if (QtdAcum >= QtdNec)
                            break;
                    }

                    return string.Join("\r\n", result
                        .Select(s => $"Ori.: {s.codestoque} / {s.saldodisponivel:n4}")
                        .ToArray());
                }
                else if (!Readouts.IsEmpty())
                    return Readouts;
                else
                    return DetailOrigin;
            }
        }

        [Ignore]
        public string SugestedOriginLocation
        {
            get
            {
                if (ordemprodcompestqend != null && ordemprodcompestqend.Any())
                {
                    var result = new List<PDOrdemProdCompEstqEndModel>();

                    var QtdNec = quantidade - PDOPTransfQtdSep;
                    var QtdAcum = 0M;

                    foreach (var Linha in ordemprodcompestqend)
                    {
                        var NewLine = new PDOrdemProdCompEstqEndModel
                        {
                            codestoque = Linha.codestoque,
                            codproduto = Linha.codproduto,
                            descricao = Linha.descricao,
                            saldodisponivel = 0,
                        };
                        if (Linha.saldodisponivel >= (QtdNec - QtdAcum))
                        {
                            NewLine.saldodisponivel = QtdNec - QtdAcum;
                            QtdAcum = QtdNec;
                        }
                        else
                        {
                            NewLine.saldodisponivel = Linha.saldodisponivel;
                            QtdAcum += Linha.saldodisponivel;
                        }

                        result.Add(NewLine);

                        if (QtdAcum >= QtdNec)
                            break;
                    }

                    return string.Join("\r\n", result
                        .Select(s => $"Ori.: {s.codestoque} / {s.saldodisponivel:n4}")
                        .ToArray());
                }
                else
                    return DetailOrigin;

            }
        }

    }
}
