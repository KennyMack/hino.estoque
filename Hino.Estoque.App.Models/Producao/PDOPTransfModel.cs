using System;
using System.Collections.Generic;
using System.Drawing;
using Hino.Estoque.App.Models.Fiscal;
using Hino.Estoque.App.Models.Gerais;
using Hino.Estoque.App.Utils.Attributes;
using SQLite;

namespace Hino.Estoque.App.Models.Producao
{
    [EndPoint("Producao")]
    public class PDOPTransfModel : BaseEntity
    {
        public PDOPTransfModel()
        {
            this.TransfLote = new HashSet<PDOPTransfLoteModel>();
        }

        public short codestab { get; set; }
        public int codtransf { get; set; }
        public decimal codordprod { get; set; }
        public string nivelordprod { get; set; }
        public decimal codestrutura { get; set; }
        public int codroteiro { get; set; }
        public int operacao { get; set; }
        public string codcomponente { get; set; }
        public string codestoqueori { get; set; }
        public string codestoquedest { get; set; }
        public string codusuario { get; set; }
        public System.DateTime datatransf { get; set; }
        public decimal quantidade { get; set; }
        public byte status { get; set; }
        public decimal? codtransfest { get; set; }

        [Ignore]
        public string ProductDescription { get; set; }

        [Ignore]
        public decimal SaldoOrigin { get; set; }
        [Ignore]
        public decimal QtdTotalNec { get; set; }
        [Ignore]
        public decimal QtdTransferida { get; set; }
        [Ignore]
        public string DetailOrigin { get; set; }
        [Ignore]
        public string DetailDest { get; set; }
        [Ignore]
        public string Detail3 { get; set; }

        [Ignore]
        public string Message { get; set; }

        [Ignore]
        public bool Success { get; set; }

        [Ignore]
        public Color ColorResult { get; set; }

        [Ignore]
        public string DetailQtdTransf { get => $"Qtd. Transf.: {QtdTransferida:n4}"; }

        [Ignore]
        public int Sort => Success ? 1 : 0;

        public virtual FSLocalEstoqueModel fslocalestoque { get; set; }
        public virtual GEUsuariosModel geusuarios { get; set; }
        public virtual ICollection<PDOPTransfLoteModel> TransfLote { get; set; }
        public virtual PDOrdemProdCompModel pdordemprodcomp { get; set; }
    }
}
