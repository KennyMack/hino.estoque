using System;
using System.Collections.Generic;
using Hino.Estoque.App.Models.Estoque;
using Hino.Estoque.App.Models.Gerais;
using Hino.Estoque.App.Utils.Attributes;
namespace Hino.Estoque.App.Models.Producao
{
    [EndPoint("Producao")]
    public class PDLancamentosModel : BaseEntity
    {
        public PDLancamentosModel()
        {
            this.eskardex = new HashSet<ESKardexModel>();
            this.eslote = new HashSet<ESLoteModel>();
        }

        public short codestab { get; set; }
        public decimal codlancamento { get; set; }
        public decimal codordprod { get; set; }
        public string nivelordprod { get; set; }
        public int codroteiro { get; set; }
        public int operacao { get; set; }
        public System.DateTime dtinicio { get; set; }
        public System.DateTime dttermino { get; set; }
        public decimal codfuncionario { get; set; }
        public decimal quantidade { get; set; }
        public decimal codestrutura { get; set; }
        public decimal? codlancpai { get; set; }
        public string turno { get; set; }
        public string codmaquina { get; set; }
        public decimal? codcor { get; set; }
        public string lotemanual { get; set; }
        public bool reginsp { get; set; }
        public short? codmotivo { get; set; }

        public virtual ICollection<ESKardexModel> eskardex { get; set; }
        public virtual ICollection<ESLoteModel> eslote { get; set; }
        public virtual GEEstabModel geestab { get; set; }
        public virtual GEFuncionariosModel gefuncionarios { get; set; }
        public virtual PDOrdemProdRotinasModel pdordemprodrotinas { get; set; }
    }
}
