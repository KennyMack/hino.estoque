﻿using Hino.Estoque.App.Utils.Exceptions;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Estoque.App.Models
{
    public class DefaultResultModel
    {
        public DefaultResultModel()
        {
            error = new List<ModelException>();
        }

        public int status { get; set; }
        public bool success { get; set; }
        public JContainer data { get; set; }
        public List<ModelException> error { get; set; }
    }
}
