using System;
using System.Linq;
using System.Collections.Generic;
using Hino.Estoque.App.Models.Estoque;
using Hino.Estoque.App.Utils.Attributes;
using SQLite;

namespace Hino.Estoque.App.Models.Fiscal
{
    [EndPoint("Fiscal/Produtos/Estab/{pCodEstab}")]
    public class FSProdutoModel : BaseEntity
    {
        public FSProdutoModel()
        {
            this.ESInventario = new HashSet<ESInventarioModel>();
            this.ESInventDet = new HashSet<ESInventDetModel>();
            this.ESKardex = new HashSet<ESKardexModel>();
            this.ESTransfDet = new HashSet<ESTransfDetModel>();
            this.FSProdutoparamEstab = new HashSet<FSProdutoparamEstabModel>();
            this.FSProdutoPcp = new HashSet<FSProdutoPcpModel>();
        }

        public string codproduto { get; set; }
        public string descricao { get; set; }
        public string detalhamento { get; set; }
        public string infadicfiscal { get; set; }

        [Ignore]
        public string estoquedestino { get; set; }

        [Ignore]
        public FSProdutoparamEstabModel ProdutoParamEstab
        {
            get
            {
                
                var produto = FSProdutoparamEstab?.FirstOrDefault();

                if (produto != null)
                    return produto;

                return null;
            }
        }

        [Ignore]
        public FSProdutoPcpModel ProdutoPcp
        {
            get
            {
                var produto = FSProdutoPcp?.FirstOrDefault();

                if (produto != null)
                    return produto;

                return null;
            }
        }

        public virtual ICollection<ESInventarioModel> ESInventario { get; set; }
        public virtual ICollection<ESInventDetModel> ESInventDet { get; set; }
        public virtual ICollection<ESKardexModel> ESKardex { get; set; }
        public virtual ICollection<ESTransfDetModel> ESTransfDet { get; set; }
        public virtual ICollection<FSProdutoparamEstabModel> FSProdutoparamEstab { get; set; }
        public virtual ICollection<FSProdutoPcpModel> FSProdutoPcp { get; set; }
    }
}
