using System;
using Hino.Estoque.App.Utils.Attributes;
using SQLite;

namespace Hino.Estoque.App.Models.Fiscal
{
    [EndPoint("Fiscal")]
    public class FSSaldoEstoqueModel : BaseEntity
    {
        public string codproduto { get; set; }
        public short codestab { get; set; }
        public string codestoque { get; set; }
        public bool status { get; set; }
        public decimal anterior { get; set; }
        public decimal entrada { get; set; }
        public decimal saida { get; set; }
        public decimal capacidade { get; set; }
        public decimal capacidadedisp { get; set; }

        [Ignore]
        public string Enderecamento =>
            FSParamLocEstoqEstab != null ?
            $"End: { FSParamLocEstoqEstab.codestoque } Saldo: {saldoEstoque:n4}"
            : "";

        [Ignore]
        public string CapacidadeDescricao => $"{capacidade:n4} / {capacidadedisp:n4}";
        public decimal saldoEstoque
        {
            get => anterior + entrada - saida;
        }

        public virtual FSParamLocEstoqEstabModel FSParamLocEstoqEstab { get; set; }
        public virtual FSLocalEstoqueModel FSLocalEstoque { get; set; }
        public virtual FSProdutoparamEstabModel FSProdutoparamEstab { get; set; }
    }
}
