using System;
using Hino.Estoque.App.Models.Gerais;
using Hino.Estoque.App.Utils.Attributes;
namespace Hino.Estoque.App.Models.Fiscal
{
    [EndPoint("Fiscal")]
    public class FSProdutoPcpModel : BaseEntity
    {
        public string codproduto { get; set; }
        public short codestab { get; set; }
        public decimal estoqueminimo { get; set; }
        public decimal estoquemaximo { get; set; }
        public decimal pesoliquido { get; set; }
        public decimal pesobruto { get; set; }
        public decimal tamanhocubico { get; set; }
        public decimal qtdeporemb { get; set; }
        public decimal qtddeembalagem { get; set; }
        public decimal loteminimo { get; set; }
        public decimal lotemaximo { get; set; }
        public decimal lotemultiplo { get; set; }
        public decimal lotedia { get; set; }
        public bool rastreabilidade { get; set; }
        public decimal leadtime { get; set; }
        public short? rotpadrao { get; set; }
        public string codsucata { get; set; }
        public bool encomenda { get; set; }
        public decimal ltentrega { get; set; }
        public decimal calcestminauto { get; set; }
        public string codgtin { get; set; }
        public decimal perctolcompra { get; set; }
        public decimal compagregado { get; set; }
        public string codgtinemb { get; set; }
        public bool arqoriginal { get; set; }
        public decimal fatorimpressao { get; set; }
        public bool importado { get; set; }
        public int? codempresa { get; set; }
        public decimal comprimento { get; set; }
        public decimal largura { get; set; }
        public decimal espessura { get; set; }
        public int curvaproduto { get; set; }
        public decimal separacaomaximo { get; set; }

        public virtual FSProdutoModel FSProduto { get; set; }
        public virtual FSProdutoparamEstabModel FSProdutoparamEstab { get; set; }
        public virtual GEEstabModel GEEstab { get; set; }
    }
}
