﻿using Hino.Estoque.App.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Estoque.App.Models.Fiscal
{
    [EndPoint("Fiscal")]
    public class FSParamLocEstoqEstabModel : BaseEntity
    {
        public string codestoque { get; set; }
        public short? calcmrp { get; set; }
        public short? negativo { get; set; }
        public short? status { get; set; }
        public int? codendclassif { get; set; }
        public string rua { get; set; }
        public string andar { get; set; }
        public string predio { get; set; }
        public string apartamento { get; set; }
        public decimal? capacidade { get; set; }
        public decimal? peso { get; set; }
        public decimal? altura { get; set; }
        public decimal? profundidade { get; set; }
        public int? codunnegocio { get; set; }
        public string sigla { get; set; }
        public short tipoend { get; set; }
    }
}
