using System;
using System.Linq;
using System.Collections.Generic;
using Hino.Estoque.App.Models.Estoque;
using Hino.Estoque.App.Models.Gerais;
using Hino.Estoque.App.Utils.Attributes;
using SQLite;

namespace Hino.Estoque.App.Models.Fiscal
{
    [EndPoint("Fiscal")]
    public class FSProdutoparamEstabModel : BaseEntity
    {
        public FSProdutoparamEstabModel()
        {
            this.ESLote = new HashSet<ESLoteModel>();
            this.FSSaldoEstoque = new HashSet<FSSaldoEstoqueModel>();
            this.FSProdutoPcp = new HashSet<FSProdutoPcpModel>();
            this.ESProdEnderec = new HashSet<ESProdEnderecModel>();
        }

        public string codproduto { get; set; }
        public short codestab { get; set; }
        public string codfamilia { get; set; }
        public string codtipo { get; set; }
        public string codestoque { get; set; }
        public string codunidade { get; set; }
        public string ncm { get; set; }
        public bool status { get; set; }
        public bool contestoque { get; set; }
        public decimal? codaplicentrada { get; set; }
        public decimal? codaplicsaida { get; set; }
        public string codserv { get; set; }
        public bool? codorigmerc { get; set; }
        public string codcest { get; set; }
        public decimal perclucro { get; set; }
        public bool atualizapreco { get; set; }
        public bool? tipoatualizacao { get; set; }
        public bool? mrpplanjcomp { get; set; }
        public decimal perperdasped { get; set; }
        public string codanp { get; set; }
        public bool enviagtinxml { get; set; }
        public decimal fatconvunidex { get; set; }
        public DateTime? dtultcompra { get; set; }
        public decimal vlrultcompra { get; set; }
        public int? codsubfamilia { get; set; }
        public DateTime datamodificacao { get; set; }
        public string uniquekey { get; set; }
        public int? idapi { get; set; }
        public decimal vlrultcompraimp { get; set; }

        public virtual ICollection<ESLoteModel> ESLote { get; set; }
        public virtual FSLocalEstoqueModel FSLocalEstoque { get; set; }
        public virtual FSProdutoModel FSProduto { get; set; }
        public virtual ICollection<FSSaldoEstoqueModel> FSSaldoEstoque { get; set; }
        public virtual ICollection<FSProdutoPcpModel> FSProdutoPcp { get; set; }
        public virtual GEEstabModel GEEstab { get; set; }
        public virtual ICollection<ESProdEnderecModel> ESProdEnderec { get; set; }

        [Ignore]
        public ESEnderecamentoModel Enderecamento
        {
            get
            {
                var Enderec = ESProdEnderec?.FirstOrDefault();
                if (Enderec != null)
                    return Enderec.ESEnderecamento;

                return null;
            }
        }
    }
}
