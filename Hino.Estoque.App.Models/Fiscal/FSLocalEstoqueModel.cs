using System;
using Hino.Estoque.App.Utils.Attributes;
namespace Hino.Estoque.App.Models.Fiscal
{
    [EndPoint("Fiscal")]
    public class FSLocalEstoqueModel : BaseEntity
    {
        public string codestoque { get; set; }
        public string descricao { get; set; }
        public string classificacao { get; set; }
        public string codgrupoestoq { get; set; }
        public int? codempresa { get; set; }
    }
}
