﻿using System;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Hino.Estoque.App.Views;
using System.Globalization;
using Hino.Estoque.App.Services;
using Xamarin.Essentials;
using Hino.Estoque.App.Utils.Interfaces;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Hino.Estoque.App.Views.Estoque;
using Hino.Estoque.App.Services.ViewModels.Auth;
using Hino.Estoque.App.Views.Auth;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace Hino.Estoque.App
{
    public partial class App : Application
    {
        public static event EventHandler IsConnectedChanged;
        private readonly LoginVM _LoginVM;

        private bool _IsNotConnected;
        public bool IsNotConnected 
        {
            get => _IsNotConnected;
            set
            {
                _IsNotConnected = value;
                IsConnectedChanged?.Invoke(_IsNotConnected, new EventArgs());
            }
        }

        public App()
        {
            InitializeComponent();

            CultureInfo.DefaultThreadCurrentCulture = new CultureInfo("pt-BR");
            XF.Material.Forms.Material.Init(this, "Material.Configuration");

            DInjection.Initialize();

            _LoginVM = new LoginVM();
            _LoginVM.GoToLogin += _LoginVM_GoToLogin;

            CurrentApp.ClearPreferences();
            MainPage = new NavigationPage(new Views.Auth.LoginPage());
        }

        private void _LoginVM_GoToLogin(object sender, EventArgs e) =>
            MainPage = new NavigationPage(new LoginPage());

        private void Connectivity_ConnectivityChanged(object sender, ConnectivityChangedEventArgs e)
        {
            IsNotConnected = !CurrentApp.IsWIFI();// !e.ConnectionProfiles.ToList().Any(r => r == ConnectionProfile.WiFi);// e.NetworkAccess != NetworkAccess.Internet;
            var instance = DependencyService.Get<IMessage>();
            instance.ShowShortMessage(IsNotConnected ?
                Estoque.App.Resources.ErrorMessagesResource.NoInternetConnection : 
                Estoque.App.Resources.ErrorMessagesResource.InternetConection);
        }

        protected async override void OnStart()
        {
            // Handle when your app starts
            CultureInfo.DefaultThreadCurrentCulture = new CultureInfo("pt-BR");
            Connectivity.ConnectivityChanged += Connectivity_ConnectivityChanged;
            IsNotConnected = !CurrentApp.IsWIFI();// Connectivity.NetworkAccess != NetworkAccess.Internet;

            #region Permissions
            try
            {
                var status = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Camera);
                if (status != Plugin.Permissions.Abstractions.PermissionStatus.Granted)
                {
                    if (await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.Camera))
                    {
                        DependencyService.Get<IMessage>().ShowShortMessage("Permissão para a câmera é obrigatória.");
                    }

                    var results = await CrossPermissions.Current.RequestPermissionsAsync(Permission.Camera);
                    //Best practice to always check that the key exists
                    if (results.ContainsKey(Permission.Camera))
                        status = results[Permission.Camera];
                }

                status = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Storage);
                if (status != Plugin.Permissions.Abstractions.PermissionStatus.Granted)
                {
                    if (await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.Storage))
                    {
                        DependencyService.Get<IMessage>().ShowShortMessage("Permissão para acesso ao armazenamento é obrigatória.");
                    }

                    var results = await CrossPermissions.Current.RequestPermissionsAsync(Permission.Storage);
                    //Best practice to always check that the key exists
                    if (results.ContainsKey(Permission.Storage))
                        status = results[Permission.Storage];
                }
            }
            catch (Exception)
            {

            }
            #endregion
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
            Connectivity.ConnectivityChanged -= Connectivity_ConnectivityChanged;
        }

        protected override void OnResume()
        {
            base.OnResume();
            // Handle when your app resumes
            CultureInfo.DefaultThreadCurrentCulture = new CultureInfo("pt-BR");
            Connectivity.ConnectivityChanged += Connectivity_ConnectivityChanged;
            IsNotConnected = !CurrentApp.IsWIFI();// Connectivity.NetworkAccess != NetworkAccess.Internet;
        }
    }
}
