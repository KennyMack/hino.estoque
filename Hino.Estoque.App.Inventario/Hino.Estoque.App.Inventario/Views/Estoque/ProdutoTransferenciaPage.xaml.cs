﻿using Hino.Estoque.App.Services;
using Hino.Estoque.App.Services.ViewModels.Fiscal;
using Hino.Estoque.App.Utils.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XF.Material.Forms.UI;

namespace Hino.Estoque.App.Views.Estoque
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProdutoTransferenciaPage : ContentPage
    {
        private string LastFocused;
        private readonly ProdutosVM _ProdutosVM;
        public ProdutoTransferenciaPage()
        {
            InitializeComponent ();
            BindingContext = _ProdutosVM = new ProdutosVM();
            _ProdutosVM.ShowBarCode = true;
            _ProdutosVM.evtTextFocused += _ProdutosVM_evtTextFocused;
            _ProdutosVM.GoToMain += _ProdutosVM_GoToMain;
            _ProdutosVM.MessageConfirmRaise += _ProdutosVM_MessageConfirmRaise;

            LastFocused = "PRODUCT";
        }

        private async void _ProdutosVM_MessageConfirmRaise(object sender, Utils.Interfaces.IMessageRaise e)
        {
            if (await DisplayAlert(e.Title, e.Text, "Sim", "Não"))
            {
                if (e.Title == Hino.Estoque.App.Resources.ValidationMessagesResource.ConfirmTransf)
                    await _ProdutosVM.TransfConfirm();
            }
        }

        private async void App_IsConnectedChanged(object sender, EventArgs e)
        {
            var isWIFI = CurrentApp.IsWIFI();
            if (!isWIFI)
                await DisplayAlert(Hino.Estoque.App.Resources.FieldsNameResource.Connection, Hino.Estoque.App.Resources.ErrorMessagesResource.NoInternetConnection, "OK");

            btnTransferir.IsEnabled = isWIFI;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            App.IsConnectedChanged += App_IsConnectedChanged;
            App_IsConnectedChanged(null, new EventArgs());

            if (!CurrentApp.LastStockOrigin.IsEmpty())
            {
                txtStockOrigin.Text = CurrentApp.LastStockOrigin;
                _ProdutosVM.STOCKORIGIN = CurrentApp.LastStockOrigin;
            }
            if (!CurrentApp.LastStockDestiny.IsEmpty())
            {
                txtStockDestiny.Text = CurrentApp.LastStockDestiny;
                _ProdutosVM.STOCKDESTINY = CurrentApp.LastStockDestiny;
            }
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            App.IsConnectedChanged -= App_IsConnectedChanged;
        }

        private async void _ProdutosVM_GoToMain(object sender, EventArgs e)
        {
            _ProdutosVM.PRODUCT = "";
            _ProdutosVM.STOCKORIGINSALDO = "";
            _ProdutosVM.STOCKORIGIN = "";
            _ProdutosVM.STOCKDESTINY = "";
            _ProdutosVM.STOCKDESTINYSALDO = "";
            _ProdutosVM.QUANTITY = 0;
            _ProdutosVM.LOTE = "";
            _ProdutosVM.HASRAST = false;
            txtProduct.Text = "";
            txtStockOrigin.Text = "";
            txtStockDestiny.Text = "";
            txtQuantity.Text = "0";
            txtLote.Text = "";
            await Navigation.PopToRootAsync();
        }

        private void _ProdutosVM_evtTextFocused(object sender, EventArgs e)
        {
            if (sender is string)
            {
                switch (sender.ToString())
                {
                    case "PRODUCT": 
                        txtProduct.Focus();
                        break;
                    case "STOCKORIGIN":
                        txtStockOrigin.Focus();
                        break;
                    case "STOCKDESTINY":
                        txtStockDestiny.Focus();
                        break;
                    case "QUANTITY":
                        txtQuantity.Focus();
                        break;
                    case "LOTE":
                        txtLote.Focus();
                        break;
                }
            }
        }

        private void ViewBarcodeVCTpl_BarcodeReadResult(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (!(sender is ZXing.Result barcodeResult))
                    return;

                if (LastFocused == "PRODUCT")
                {
                    _ProdutosVM.PRODUCT = barcodeResult.Text.Trim();
                    await _ProdutosVM.GetProductByBarCodeAsync();
                }
                if (LastFocused == "STOCKORIGIN")
                {
                    _ProdutosVM.STOCKORIGIN = barcodeResult.Text.Trim();
                    txtStockDestiny.Focus();
                }
                if (LastFocused == "STOCKDESTINY")
                {
                    _ProdutosVM.STOCKDESTINY = barcodeResult.Text.Trim();
                    txtQuantity.Focus();
                }
                if (LastFocused == "LOTE")
                {
                    _ProdutosVM.LOTE = barcodeResult.Text.Trim();
                    txtQuantity.Focus();
                }
            });
        }

        private async Task ProductCompleted()
        {
            await _ProdutosVM.GetProductByBarCodeAsync();

            if (!_ProdutosVM.HASPRODUCTERROR && !_ProdutosVM.PRODUCT.IsEmpty() && btnTransferir.IsEnabled)
            {
                if (!_ProdutosVM.STOCKORIGIN.IsEmpty())
                    await _ProdutosVM.BuscaSaldoEstoqueAsync(_ProdutosVM.STOCKORIGIN, true);

                if (!_ProdutosVM.STOCKDESTINY.IsEmpty())
                    await _ProdutosVM.BuscaSaldoEstoqueAsync(_ProdutosVM.STOCKDESTINY, false);
            }
        }

        private async void TxtProduct_Unfocused(object sender, FocusEventArgs e) =>
            await ProductCompleted();

        private async void TxtProduct_Completed(object sender, EventArgs e) =>
            await ProductCompleted();

        private void TxtQuantity_Completed(object sender, EventArgs e) =>
            _ProdutosVM.CmdTransfItemClick.Execute(null);

        private void TxtProduct_Focused(object sender, FocusEventArgs e)
        {
            LastFocused = "PRODUCT";
        }


        private void TxtStockOrigin_Focused(object sender, FocusEventArgs e)
        {
            LastFocused = "STOCKORIGIN";
        }

        private void TxtStockDestiny_Focused(object sender, FocusEventArgs e)
        {
            LastFocused = "STOCKDESTINY";
        }
        private void txtLote_Focused(object sender, FocusEventArgs e)
        {
            LastFocused = "LOTE";
        }

        private void Txt_TextChanged(object sender, TextChangedEventArgs e)
        {
            (sender as MaterialTextField).Text = e.NewTextValue.ToUpperInvariant();
        }

        private void txtLote_Completed(object sender, EventArgs e)
        {

        }

        private async void TxtStockOrigin_Completed(object sender, EventArgs e) =>
            await _ProdutosVM.BuscaSaldoEstoqueAsync(_ProdutosVM.STOCKORIGIN, true);

        private async void TxtStockDestiny_Completed(object sender, EventArgs e) =>
            await _ProdutosVM.BuscaSaldoEstoqueAsync(_ProdutosVM.STOCKDESTINY, false);

        protected override bool OnBackButtonPressed()
        {
            Device.BeginInvokeOnMainThread(async () => {
                var result = await this.DisplayAlert(Hino.Estoque.App.Resources.ValidationMessagesResource.Confirm,
                    Hino.Estoque.App.Resources.ValidationMessagesResource.BackConfirmMessage,
                    Hino.Estoque.App.Resources.ValidationMessagesResource.ConfirmBack,
                    Hino.Estoque.App.Resources.ValidationMessagesResource.Stay);
                if (result) await this.Navigation.PopAsync();
            });

            return true;
        }

        private void btnShowAddress_Clicked(object sender, EventArgs e)
        {

        }
    }
}