﻿using Hino.Estoque.App.Models.Estoque;
using Hino.Estoque.App.Services;
using Hino.Estoque.App.Services.ViewModels.Estoque;
using Hino.Estoque.App.Templates;
using Hino.Estoque.App.Utils.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XF.Material.Forms.UI;

namespace Hino.Estoque.App.Views.Estoque
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SearchInventariosPage : BaseContentPage
    {
        private bool _BuscandoBarras;
        private string LastFocused;
        private readonly SearchInventarioVM _SearchInventarioVM;
        public SearchInventariosPage(ESInventarioModel pESInventarioModel)
        {
            InitializeComponent();
            BindingContext = _SearchInventarioVM = new SearchInventarioVM(pESInventarioModel);
            _SearchInventarioVM.Title = $"Inventário: {pESInventarioModel.codinv}/{pESInventarioModel.contagem}";
            _SearchInventarioVM.ShowBarCode = true;
            _SearchInventarioVM.SaveSuccess += SearchInventarioVM_GoToMain;
            _SearchInventarioVM.evtTextFocused += _ProdutosVM_evtTextFocused;
            _SearchInventarioVM.MessageRaise += _SearchInventarioVM_MessageRaise;
            viewBarcodeVCTpl.BarcodeClosed += (s, e) => _BuscandoBarras = false;
            LastFocused = "PRODUCT";
        }

        private void _SearchInventarioVM_MessageRaise(object sender, Utils.Interfaces.IMessageRaise e)
        {
            DisplayAlert(e.Title, e.Text, "OK");
        }

        private void SearchInventarioVM_GoToMain(object sender, EventArgs e)
        {
            LastFocused = "PRODUCT";
            _SearchInventarioVM.ClearInstance();
        }

        private async void App_IsConnectedChanged(object sender, EventArgs e)
        {
            var isWIFI = CurrentApp.IsWIFI();
            if (!isWIFI)
                await DisplayAlert(Hino.Estoque.App.Resources.FieldsNameResource.Connection, Hino.Estoque.App.Resources.ErrorMessagesResource.NoInternetConnection, "OK");

            btnSaveCount.IsEnabled = isWIFI;
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            App.IsConnectedChanged -= App_IsConnectedChanged;
            MessagingCenter.Unsubscribe<App, string>(this, "btnLerBarrasTapped");
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            App.IsConnectedChanged += App_IsConnectedChanged;
            App_IsConnectedChanged(null, new EventArgs());

            MessagingCenter.Subscribe<App, string>(this, "btnLerBarrasTapped", async (sender, args) =>
            {
                if (!_BuscandoBarras)
                {
                    _BuscandoBarras = true;
                    await viewBarcodeVCTpl.ShowBarCodeView();
                }
            });
        }

        private void _ProdutosVM_evtTextFocused(object sender, EventArgs e)
        {
            if (sender is string)
            {
                switch (sender.ToString())
                {
                    case "PRODUCT":
                        txtProduct.Focus();
                        break;
                    case "STOCK":
                        txtStock.Focus();
                        break;
                    case "QUANTITY":
                        txtQuantity.Focus();
                        break;
                }
            }
        }

        private void TxtProduct_Completed(object sender, EventArgs e) =>
            txtProduct_Unfocused(sender, new FocusEventArgs(txtProduct, true));

        private void ViewBarcodeVCTpl_BarcodeReadResult(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                _BuscandoBarras = false;
                if (!(sender is ZXing.Result barcodeResult))
                    return;

                if (LastFocused == "STOCK")
                {
                    _SearchInventarioVM.STOCK = barcodeResult.Text;
                    if (!string.IsNullOrEmpty(txtProduct.Text) && !string.IsNullOrEmpty(txtProduct.Text))
                    {
                        _SearchInventarioVM.PRODUCT = txtProduct.Text;
                        await _SearchInventarioVM.GetStockByBarCodeAsync();

                        if (!_SearchInventarioVM.MANUALQUANTITY &&
                            !_SearchInventarioVM.STOCK.IsEmpty())
                            LastFocused = "QUANTITY";
                    }
                    else
                        txtProduct.Focus();
                }
                else if(LastFocused == "PRODUCT")
                {
                    _SearchInventarioVM.PRODUCT = barcodeResult.Text;
                    await _SearchInventarioVM.GetProductByBarCodeAsync();
                }
                else if (LastFocused == "QUANTITY")
                {
                    try
                    {
                        _SearchInventarioVM.PRODUCTBARCODE = barcodeResult.Text.Trim();
                        _SearchInventarioVM.CountQuantity();
                    }
                    catch (Exception)
                    {

                    }
                }
            });
        }


        private void TxtProduct_Focused(object sender, FocusEventArgs e)
        {
            LastFocused = "PRODUCT";
        }

        private void TxtProduct_Completed(object sender, FocusEventArgs e)
        {

        }

        private void txtProduct_Unfocused(object sender, FocusEventArgs e)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                await _SearchInventarioVM.GetProductByBarCodeAsync();
            });
        }

        private void TxtStock_Completed(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (!txtProduct.Text.IsEmpty())
                {
                    _SearchInventarioVM.PRODUCT = txtProduct.Text;
                    await _SearchInventarioVM.GetStockByBarCodeAsync();

                    if (!_SearchInventarioVM.MANUALQUANTITY &&
                        !_SearchInventarioVM.STOCK.IsEmpty())
                        LastFocused = "QUANTITY";
                }
            });
        }

        private void TxtStock_Focused(object sender, FocusEventArgs e)
        {
            LastFocused = "STOCK";
        }

        private void TxtStock_Unfocused(object sender, FocusEventArgs e) =>
            TxtStock_Completed(sender, e);

        private void txtQuantity_Focused(object sender, FocusEventArgs e)
        {
            LastFocused = "QUANTITY";
        }

        private void TxtQuantity_Completed(object sender, EventArgs e) =>
            _SearchInventarioVM.CmdSaveCountClick.Execute(null);

        private void txtPackageQuantity_Completed(object sender, EventArgs e) =>
            btnConfirmPackage_Clicked(sender, e);

        private void btnInformPackage_Clicked(object sender, EventArgs e)
        {
            _SearchInventarioVM.INFORMPACKAGES = true;
            _SearchInventarioVM.INFORMEDPACKAGES = false;
            _SearchInventarioVM.PACKAGESQUANTITY = 1;
            txtPackageQuantity.Focus();
        }

        private void btnConfirmPackage_Clicked(object sender, EventArgs e)
        {
            // _SearchInventarioVM.PACKAGESQUANTITY = _SearchInventarioVM.PACKAGESQUANTITY <= 0 ? 1 : 0;
            _SearchInventarioVM.INFORMEDPACKAGES = true;
            _SearchInventarioVM.INFORMPACKAGES = false;
        }
    }
}