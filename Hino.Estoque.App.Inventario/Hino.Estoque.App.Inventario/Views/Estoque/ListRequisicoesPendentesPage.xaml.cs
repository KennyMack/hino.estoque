﻿using Hino.Estoque.App.Services.ViewModels.Estoque;
using Hino.Estoque.App.Templates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Estoque.App.Views.Estoque
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ListRequisicoesPendentesPage : BaseContentPage
    {
        private readonly RequisicoesPendenteVM _RequisicoesPendenteVM;
        public ListRequisicoesPendentesPage()
        {
            InitializeComponent();
            BindingContext = _RequisicoesPendenteVM = new RequisicoesPendenteVM();
        }
        
        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (_RequisicoesPendenteVM.Items.Count == 0)
                _RequisicoesPendenteVM.RefreshDataCommand.Execute(null);
        }

        private void lvRequisicoesPendentes_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {

        }

        private async void CreateRequest_Clicked(object sender, EventArgs e) =>
            await Navigation.PushAsync(new RequisicaoProdutoPage());

        private async void RequisicoesItemVCTpl_btnAprovarClicked(object sender, EventArgs e)
        {
            if (await DisplayAlert("Alterar status", $"Aprovar a requisição: {sender} ?", "APROVAR", "MANTER PENDENTE"))
                await _RequisicoesPendenteVM.AproveRequest(Convert.ToInt32(sender));
        }

        private async void RequisicoesItemVCTpl_btnReprovarClicked(object sender, EventArgs e)
        {
            if (await DisplayAlert("Alterar status", $"Reprovar a requisição: {sender} ?", "REPROVAR", "MANTER PENDENTE"))
                await _RequisicoesPendenteVM.ReproveRequest(Convert.ToInt32(sender));
        }
    }
}