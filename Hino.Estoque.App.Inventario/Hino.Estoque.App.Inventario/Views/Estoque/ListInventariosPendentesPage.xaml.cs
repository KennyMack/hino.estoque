﻿using Hino.Estoque.App.Models.Estoque;
using Hino.Estoque.App.Services;
using Hino.Estoque.App.Services.ViewModels.Auth;
using Hino.Estoque.App.Services.ViewModels.Estoque;
using Hino.Estoque.App.Templates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Estoque.App.Views.Estoque
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ListInventariosPendentesPage : BaseContentPage
    {
        private readonly LoginVM _LoginVM;
        private readonly InventariosPendentesVM _InventariosPendentesVM;
        public ListInventariosPendentesPage()
        {
            InitializeComponent();
            BindingContext = _InventariosPendentesVM = new InventariosPendentesVM();
            _LoginVM = new LoginVM();
            _LoginVM.MessageConfirmRaise += _LoginVM_MessageConfirmRaise;
            _LoginVM.GoToLogin += _LoginVM_GoToLogin;
        }

        private void _LoginVM_GoToLogin(object sender, EventArgs e) =>
            App.Current.MainPage = new NavigationPage(new Auth.LoginPage());

        private async void _LoginVM_MessageConfirmRaise(object sender, Utils.Interfaces.IMessageRaise e)
        {
            if (await DisplayAlert(e.Title, e.Text, "Sim", "Não"))
            {
                _LoginVM.LogOut();
            }
        }

        private async void App_IsConnectedChanged(object sender, EventArgs e)
        {
            var isWIFI = CurrentApp.IsWIFI();
            if (!isWIFI)
                await DisplayAlert(Hino.Estoque.App.Resources.FieldsNameResource.Connection, Hino.Estoque.App.Resources.ErrorMessagesResource.NoInternetConnection, "OK");
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            App.IsConnectedChanged += App_IsConnectedChanged;
            App_IsConnectedChanged(null, new EventArgs());

            if (_InventariosPendentesVM.Items.Count == 0)
                _InventariosPendentesVM.RefreshDataCommand.Execute(null);
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            App.IsConnectedChanged -= App_IsConnectedChanged;
        }

        private async void LvInventariosPendentes_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (!(e.SelectedItem is ESInventarioModel inventario))
                return;

            await Navigation.PushAsync(new SearchInventariosPage(inventario));

            // Manually deselect item.
            lvInventariosPendentes.SelectedItem = null;
        }

        private void TiExit_Clicked(object sender, EventArgs e) =>
            _LoginVM.ConfirmExit();
    }
}