﻿using Hino.Estoque.App.Models.Estoque;
using Hino.Estoque.App.Services;
using Hino.Estoque.App.Services.ViewModels.Estoque;
using Hino.Estoque.App.Templates;
using Hino.Estoque.App.Templates.Utils;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XF.Material.Forms.UI.Dialogs;

namespace Hino.Estoque.App.Views.Estoque
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ListInventItensPendentesPage : BaseContentPage
    {
        private readonly InventarioDetVM _InventarioDetVM;
        public ListInventItensPendentesPage(ESInventarioModel pESInventarioModel)
        {
            InitializeComponent ();
            BindingContext = _InventarioDetVM = new InventarioDetVM(pESInventarioModel);
            _InventarioDetVM.InfiniteListView(lvInvenItemsPendentes);
            _InventarioDetVM.MessageConfirmRaise += _InventarioDetVM_MessageConfirmRaise;
            _InventarioDetVM.SearchEvent(txtProductsSearchBar);
            _InventarioDetVM.GoToList += _InventarioDetVM_GoToList;
        }

        private async void _InventarioDetVM_GoToList(object sender, EventArgs e) =>
            await Navigation.PopAsync();

        private async void _InventarioDetVM_MessageConfirmRaise(object sender, Utils.Interfaces.IMessageRaise e)
        {
            if (await DisplayAlert(e.Title, e.Text, "Sim", "Não"))
            {
                if (e.Title == Hino.Estoque.App.Resources.ValidationMessagesResource.ConfirmTyping)
                    await _InventarioDetVM.SyncValueAsync(true);
                else
                    await _InventarioDetVM.ReloadData();
            }
        }

        private async void App_IsConnectedChanged(object sender, EventArgs e)
        {
            var isWIFI = CurrentApp.IsWIFI();
            if (!isWIFI)
                await DisplayAlert(Hino.Estoque.App.Resources.FieldsNameResource.Connection, Hino.Estoque.App.Resources.ErrorMessagesResource.NoInternetConnection, "OK");

            UpdateItem.IsEnabled = isWIFI;
            ConfirmItem.IsEnabled = isWIFI;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            App.IsConnectedChanged += App_IsConnectedChanged;
            App_IsConnectedChanged(null, new EventArgs());

            if (_InventarioDetVM.Items.Count() == 0)
                _InventarioDetVM.RefreshDataCommand.Execute(null);
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            App.IsConnectedChanged -= App_IsConnectedChanged;
        }

        private void LvInvenItemsPendentes_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            lvInvenItemsPendentes.SelectedItem = null;
        }

        private async void TxtQuantity_Completed(object sender, EventArgs e) =>
            await _InventarioDetVM.SaveValueAsync((InventDetResult)sender);

        private void SearchItem_Clicked(object sender, EventArgs e)
        {
            _InventarioDetVM.ShowSearch = !_InventarioDetVM.ShowSearch;

            if (_InventarioDetVM.ShowSearch)
                txtProductsSearchBar.Focus();
        }

        private void ConfirmItem_Clicked(object sender, EventArgs e) =>
            _InventarioDetVM.ConfirmSave();

        private void SearchBarCode_Clicked(object sender, EventArgs e)
        {
            _InventarioDetVM.ShowBarCode = !_InventarioDetVM.ShowBarCode;
            viewBarcodeVCTpl.HeightRequest = _InventarioDetVM.ShowBarCode ? 200 : 0;
        }

        private void UpdateItem_Clicked(object sender, EventArgs e) =>
            _InventarioDetVM.ConfirmReload();

        public void BarcodeReadResult_Result(object sender, EventArgs e)
        {
            SearchBarCode_Clicked(sender, e);

            if (!(sender is ZXing.Result barcodeResult))
                return;
            _InventarioDetVM.ShowSearch = true;
            _InventarioDetVM.SEARCHDATA = barcodeResult.Text;
        }

        protected override bool OnBackButtonPressed()
        {
            Device.BeginInvokeOnMainThread(async () => {
                var result = await this.DisplayAlert(Hino.Estoque.App.Resources.ValidationMessagesResource.Confirm,
                    Hino.Estoque.App.Resources.ValidationMessagesResource.BackConfirmMessage,
                    Hino.Estoque.App.Resources.ValidationMessagesResource.ConfirmBack,
                    Hino.Estoque.App.Resources.ValidationMessagesResource.Stay);
                if (result) await this.Navigation.PopAsync();
            });

            return true;
        }
    }
}