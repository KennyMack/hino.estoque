﻿using Hino.Estoque.App.Services;
using Hino.Estoque.App.Services.ViewModels.Fiscal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XF.Material.Forms.UI;

namespace Hino.Estoque.App.Views.Estoque
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProdutoDetalhesPage : ContentPage
    {
        private readonly ProdutosVM _ProdutosVM;
        public ProdutoDetalhesPage()
        {
            InitializeComponent();
            BindingContext = _ProdutosVM = new ProdutosVM("Detalhar produto");
            _ProdutosVM.ShowBarCode = true;
            _ProdutosVM.GoToMain += _ProdutosVM_GoToMain;
        }

        private async void App_IsConnectedChanged(object sender, EventArgs e)
        {
            var isWIFI = CurrentApp.IsWIFI();
            if (!isWIFI)
                await DisplayAlert(Hino.Estoque.App.Resources.FieldsNameResource.Connection, Hino.Estoque.App.Resources.ErrorMessagesResource.NoInternetConnection, "OK");

            btnConsultar.IsEnabled = isWIFI;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            App.IsConnectedChanged += App_IsConnectedChanged;
            App_IsConnectedChanged(null, new EventArgs());
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            App.IsConnectedChanged -= App_IsConnectedChanged;
        }

        private async void _ProdutosVM_GoToMain(object sender, EventArgs e) =>
            await Navigation.PopToRootAsync();

        private void ViewBarcodeVCTpl_BarcodeReadResult(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (!(sender is ZXing.Result barcodeResult))
                    return;

                _ProdutosVM.PRODUCT = barcodeResult.Text.Trim();
                await _ProdutosVM.GetDetailProductByBarCodeAsync();
            });
        }

        private void TxtProduct_Completed(object sender, EventArgs e) =>
            _ProdutosVM.CmdDetalharClick.Execute(null);

        private void Txt_TextChanged(object sender, TextChangedEventArgs e)
        {
            (sender as MaterialTextField).Text = e.NewTextValue.ToUpperInvariant();
        }

        private void btnConsultarOutro_Clicked(object sender, EventArgs e) =>
            _ProdutosVM.DETAILLOADED = false;

        private void lvSaldos_ItemSelected(object sender, SelectedItemChangedEventArgs e) =>
            this.lvSaldos.SelectedItem = null;
    }
}