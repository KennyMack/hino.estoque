﻿using Hino.Estoque.App.Services;
using Hino.Estoque.App.Services.ViewModels.Estoque;
using Hino.Estoque.App.Utils.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XF.Material.Forms.UI;

namespace Hino.Estoque.App.Views.Estoque
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RequisicaoProdutoPage : ContentPage
    {
        private string LastFocused;
        private readonly RequisicaoNovaVM _RequisicaoNovaVM;
        public RequisicaoProdutoPage()
        {
            InitializeComponent();
            BindingContext = _RequisicaoNovaVM = new RequisicaoNovaVM();
            _RequisicaoNovaVM.ShowBarCode = true;
            _RequisicaoNovaVM.evtTextFocused += _RequsicaoNovaVM_evtTextFocused; ;
            _RequisicaoNovaVM.GoToMain += _RequsicaoNovaVM_GoToMain; ;
            LastFocused = "PRODUCT";
        }

        private async void App_IsConnectedChanged(object sender, EventArgs e)
        {
            bool isWIFI = CurrentApp.IsWIFI();
            if (!isWIFI)
                await DisplayAlert(Hino.Estoque.App.Resources.FieldsNameResource.Connection, Hino.Estoque.App.Resources.ErrorMessagesResource.NoInternetConnection, "OK");

            btnSaveRequest.IsEnabled = isWIFI;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            App.IsConnectedChanged += App_IsConnectedChanged;
            App_IsConnectedChanged(null, new EventArgs());
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            App.IsConnectedChanged -= App_IsConnectedChanged;
        }

        private async void _RequsicaoNovaVM_GoToMain(object sender, EventArgs e) =>
            await Navigation.PopToRootAsync();

        private void viewBarcodeVCTpl_BarcodeReadResult(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (!(sender is ZXing.Result barcodeResult))
                    return;

                if (LastFocused == "PRODUCT")
                {
                    _RequisicaoNovaVM.PRODUCT = barcodeResult.Text.Trim();
                    await _RequisicaoNovaVM.GetProductByBarCodeAsync();
                }
                if (LastFocused == "STOCKORIGIN")
                {
                    _RequisicaoNovaVM.STOCKORIGIN = barcodeResult.Text.Trim();
                    txtQuantity.Focus();
                }
            });
        }

        private void _RequsicaoNovaVM_evtTextFocused(object sender, EventArgs e)
        {
            if (sender is string)
            {
                switch (sender.ToString())
                {
                    case "PRODUCT":
                        txtProduct.Focus();
                        break;
                    case "STOCKORIGIN":
                        txtStockOrigin.Focus();
                        break;
                    case "QUANTITY":
                        txtQuantity.Focus();
                        break;
                }
            }
        }

        private async void txtProduct_Completed(object sender, EventArgs e)
        {
            await _RequisicaoNovaVM.GetProductByBarCodeAsync();

            if (!_RequisicaoNovaVM.HASPRODUCTERROR && !_RequisicaoNovaVM.PRODUCT.IsEmpty()
                && btnSaveRequest.IsEnabled)
            {
                if (!_RequisicaoNovaVM.STOCKORIGIN.IsEmpty())
                    await _RequisicaoNovaVM.BuscaSaldoEstoqueAsync(_RequisicaoNovaVM.STOCKORIGIN);
            }
        }

        private void Txt_TextChanged(object sender, TextChangedEventArgs e)
        {
            (sender as MaterialTextField).Text = e.NewTextValue.ToUpperInvariant();
        }

        private void txtProduct_Focused(object sender, FocusEventArgs e)
        {
            LastFocused = "PRODUCT";
        }

        private async void txtStockOrigin_Completed(object sender, EventArgs e) =>
            await _RequisicaoNovaVM.BuscaSaldoEstoqueAsync(_RequisicaoNovaVM.STOCKORIGIN);

        private void txtStockOrigin_Focused(object sender, FocusEventArgs e)
        {
            LastFocused = "STOCKORIGIN";
        }

        private void txtQuantity_Completed(object sender, EventArgs e) =>
            _RequisicaoNovaVM.CmdRequestItemClick.Execute(null);


        protected override bool OnBackButtonPressed()
        {
            Device.BeginInvokeOnMainThread(async () => {
                var result = await this.DisplayAlert(Hino.Estoque.App.Resources.ValidationMessagesResource.Confirm,
                    Hino.Estoque.App.Resources.ValidationMessagesResource.BackConfirmMessage,
                    Hino.Estoque.App.Resources.ValidationMessagesResource.ConfirmBack,
                    Hino.Estoque.App.Resources.ValidationMessagesResource.Stay);
                if (result) await this.Navigation.PopAsync();
            });

            return true;
        }

        private void txtProduct_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}