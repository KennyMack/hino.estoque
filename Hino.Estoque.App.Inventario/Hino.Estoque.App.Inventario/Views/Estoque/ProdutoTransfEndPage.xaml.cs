﻿using Hino.Estoque.App.Services;
using Hino.Estoque.App.Services.ViewModels.Estoque;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XF.Material.Forms.UI;

namespace Hino.Estoque.App.Views.Estoque
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProdutoTransfEndPage : ContentPage
    {
        private string LastFocused;
        private string LastFocusedAddress;
        private readonly EstoqueEndVM _EstoqueEndVM;
        public ProdutoTransfEndPage()
        {
            InitializeComponent();
            BindingContext = _EstoqueEndVM = new EstoqueEndVM();
            _EstoqueEndVM.ShowBarCode = true;
            _EstoqueEndVM.evtTextFocused += _ProdutosVM_evtTextFocused;
            _EstoqueEndVM.GoToMain += _ProdutosVM_GoToMain;
            _EstoqueEndVM.MessageRaise += async (evt, e) => await DisplayAlert(e.Title, e.Text, "OK");
            _EstoqueEndVM.MessageConfirmRaise += _EstoqueEndVM_MessageConfirmRaise;
            LastFocused = "PRODUCT";
            LastFocusedAddress = "ADDRESS";
        }

        private async void _EstoqueEndVM_MessageConfirmRaise(object sender, Utils.Interfaces.IMessageRaise e)
        {
            if (await DisplayAlert(e.Title, e.Text, "Sim", "Não"))
            {
                if (e.Title == Hino.Estoque.App.Resources.ValidationMessagesResource.ConfirmTyping)
                    await _EstoqueEndVM.ConfirmarTransfAsync();
            }
        }

        private async void App_IsConnectedChanged(object sender, EventArgs e)
        {
            var isWIFI = CurrentApp.IsWIFI();
            if (!isWIFI)
                await DisplayAlert(Hino.Estoque.App.Resources.FieldsNameResource.Connection, Hino.Estoque.App.Resources.ErrorMessagesResource.NoInternetConnection, "OK");

            btnCarregarEnd.IsEnabled = isWIFI;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            App.IsConnectedChanged += App_IsConnectedChanged;
            App_IsConnectedChanged(null, new EventArgs());
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            App.IsConnectedChanged -= App_IsConnectedChanged;
        }

        private async void _ProdutosVM_GoToMain(object sender, EventArgs e) =>
            await Navigation.PopToRootAsync();

        private void _ProdutosVM_evtTextFocused(object sender, EventArgs e)
        {
            if (sender is string)
            {
                switch (sender.ToString())
                {
                    case "PRODUCT":
                        txtProduct.Focus();
                        break;
                    case "STOCKORIGIN":
                        txtStockOrigin.Focus();
                        break;
                }
            }
        }

        private void ViewBarcodeVCTpl_BarcodeReadResult(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (!(sender is ZXing.Result barcodeResult))
                    return;

                if (LastFocused == "PRODUCT")
                {
                    _EstoqueEndVM.PRODUCT = barcodeResult.Text.Trim();
                    await _EstoqueEndVM.GetProductByBarCodeAsync();
                }
                if (LastFocused == "STOCKORIGIN")
                {
                    _EstoqueEndVM.STOCKORIGIN = barcodeResult.Text.Trim();
                    _EstoqueEndVM.OnCarregarEndClick();
                }
            });
        }

        private async void TxtProduct_Completed(object sender, EventArgs e) =>
            await _EstoqueEndVM.GetProductByBarCodeAsync();

        private void TxtProduct_Focused(object sender, FocusEventArgs e)
        {
            LastFocused = "PRODUCT";
        }

        private async void txtProduct_Unfocused(object sender, FocusEventArgs e)
        {
            _EstoqueEndVM.PRODUCT = txtProduct.Text?.Trim();
            await _EstoqueEndVM.GetProductByBarCodeAsync();
        }

        private void TxtStockOrigin_Focused(object sender, FocusEventArgs e)
        {
            LastFocused = "STOCKORIGIN";
        }

        private void Txt_TextChanged(object sender, TextChangedEventArgs e)
        {
            (sender as MaterialTextField).Text = e.NewTextValue.ToUpperInvariant();
        }

        private async void TxtStockOrigin_Completed(object sender, EventArgs e) =>
            await _EstoqueEndVM.BuscaSaldoEstoqueAsync(_EstoqueEndVM.STOCKORIGIN);

        private void viewBarcodeVCTplEndereco_BarcodeReadResult(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (!(sender is ZXing.Result barcodeResult))
                    return;

                if (LastFocusedAddress == "ADDRESS")
                {
                    _EstoqueEndVM.STOCKDESTINY = barcodeResult.Text.Trim();
                    await _EstoqueEndVM.GetAddresDestinyInfoAsync();
                }
                if (LastFocusedAddress == "QUANTITY")
                {
                    // _EstoqueEndVM.QUANTITY = barcodeResult.Text.Trim();
                    // _EstoqueEndVM.OnCarregarEndClick();
                }
            });
        }

        private async void txtEnderecoEstoque_Unfocused(object sender, FocusEventArgs e) =>
            await _EstoqueEndVM.GetAddresDestinyInfoAsync();

        private async void txtEnderecoEstoque_Completed(object sender, EventArgs e) =>
            await _EstoqueEndVM.GetAddresDestinyInfoAsync();

        private void txtEnderecoEstoque_Focused(object sender, FocusEventArgs e) =>
            LastFocusedAddress = "ADDRESS";

        private void txtQuantity_Focused(object sender, FocusEventArgs e) =>
            LastFocusedAddress = "QUANTITY";

        private void txtQuantity_Completed(object sender, EventArgs e) =>
            _EstoqueEndVM.CmdAddEnderecoClick.Execute(null);

        private void lvEnderecos_ItemSelected(object sender, SelectedItemChangedEventArgs e) =>
            this.lvEnderecos.SelectedItem = null;

        private void ConfirmItem_Clicked(object sender, EventArgs e) =>
           _EstoqueEndVM.ConfirmSave();

        protected override bool OnBackButtonPressed()
        {
            Device.BeginInvokeOnMainThread(async () => {
                var result = await this.DisplayAlert(Hino.Estoque.App.Resources.ValidationMessagesResource.Confirm,
                    Hino.Estoque.App.Resources.ValidationMessagesResource.BackConfirmMessage,
                    Hino.Estoque.App.Resources.ValidationMessagesResource.ConfirmBack,
                    Hino.Estoque.App.Resources.ValidationMessagesResource.Stay);
                if (result) await this.Navigation.PopAsync();
            });

            return true;
        }
    }
}