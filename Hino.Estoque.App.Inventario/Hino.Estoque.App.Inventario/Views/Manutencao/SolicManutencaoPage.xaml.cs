﻿using Hino.Estoque.App.Services;
using Hino.Estoque.App.Services.ViewModels.Manutencao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XF.Material.Forms.UI;

namespace Hino.Estoque.App.Views.Manutencao
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SolicManutencaoPage : ContentPage
    {
        private string LastFocused;
        private readonly CriarSolicManutencaoVM _CriarSolicManutencaoVM;
        public SolicManutencaoPage()
        {
            InitializeComponent();
            BindingContext = _CriarSolicManutencaoVM = new CriarSolicManutencaoVM();
            _CriarSolicManutencaoVM.ShowBarCode = true;
            _CriarSolicManutencaoVM.evtTextFocused += CriarSolicManutencaoVM_evtTextFocused;
            _CriarSolicManutencaoVM.GoToMain += CriarSolicManutencaoVM_GoToMain;
            LastFocused = "EQUIPMENT";

        }

        private async void App_IsConnectedChanged(object sender, EventArgs e)
        {
            bool isWIFI = CurrentApp.IsWIFI();
            if (!isWIFI)
                await DisplayAlert(Hino.Estoque.App.Resources.FieldsNameResource.Connection, Hino.Estoque.App.Resources.ErrorMessagesResource.NoInternetConnection, "OK");

            btnSaveRequest.IsEnabled = isWIFI;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            App.IsConnectedChanged += App_IsConnectedChanged;
            App_IsConnectedChanged(null, new EventArgs());
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            App.IsConnectedChanged -= App_IsConnectedChanged;
        }

        protected override bool OnBackButtonPressed()
        {
            Device.BeginInvokeOnMainThread(async () => {
                var result = await this.DisplayAlert(Hino.Estoque.App.Resources.ValidationMessagesResource.Confirm,
                    Hino.Estoque.App.Resources.ValidationMessagesResource.BackConfirmMessage,
                    Hino.Estoque.App.Resources.ValidationMessagesResource.ConfirmBack,
                    Hino.Estoque.App.Resources.ValidationMessagesResource.Stay);
                if (result) await this.Navigation.PopAsync();
            });

            return true;
        }

        private async void CriarSolicManutencaoVM_GoToMain(object sender, EventArgs e) =>
            await Navigation.PopToRootAsync();

        private void viewBarcodeVCTpl_BarcodeReadResult(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (!(sender is ZXing.Result barcodeResult))
                    return;

                if (LastFocused == "EQUIPMENT")
                {
                    _CriarSolicManutencaoVM.EQUIPMENT = barcodeResult.Text.Trim();
                    await _CriarSolicManutencaoVM.GetEquipmentByBarCodeAsync();
                    txtTipoManut.Focus();
                }
                if (LastFocused == "TYPE")
                {
                    _CriarSolicManutencaoVM.TIPOMANUT = barcodeResult.Text.Trim();
                    await _CriarSolicManutencaoVM.GetTipoByBarCodeAsync();
                    txtMotivoManut.Focus();
                }
                if (LastFocused == "REASON")
                {
                    _CriarSolicManutencaoVM.MOTIVOMANUT = barcodeResult.Text.Trim();
                    await _CriarSolicManutencaoVM.GetMotivoByBarCodeAsync();
                }
            });
        }

        private void CriarSolicManutencaoVM_evtTextFocused(object sender, EventArgs e)
        {
            if (sender is string)
            {
                switch (sender.ToString())
                {
                    case "EQUIPMENT":
                        txtEquipment.Focus();
                        break;
                    case "TYPE":
                        txtTipoManut.Focus();
                        break;
                    case "REASON":
                        txtMotivoManut.Focus();
                        break;
                }
            }
        }

        private void Txt_TextChanged(object sender, TextChangedEventArgs e)
        {
            (sender as MaterialTextField).Text = e.NewTextValue.ToUpperInvariant();
        }

        private void txtEquipment_Completed(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(async () =>
                await _CriarSolicManutencaoVM.GetEquipmentByBarCodeAsync());
        }

        private void txtTipoManut_Completed(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(async () =>
                await _CriarSolicManutencaoVM.GetTipoByBarCodeAsync());
        }

        private void txtMotivoManut_Completed(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(async () =>
                await _CriarSolicManutencaoVM.GetMotivoByBarCodeAsync());
        }

        #region Ao alterar o foco dos campos
        private void txtEquipment_Focused(object sender, FocusEventArgs e)
        {
            LastFocused = "EQUIPMENT";
        }

        private void txtTipoManut_Focused(object sender, FocusEventArgs e)
        {
            LastFocused = "TYPE";
        }

        private void txtMotivoManut_Focused(object sender, FocusEventArgs e)
        {
            LastFocused = "REASON";
        }

        private void txtMotivoManut_Unfocused(object sender, FocusEventArgs e)
        {
            Device.BeginInvokeOnMainThread(async () =>
                await _CriarSolicManutencaoVM.GetMotivoByBarCodeAsync());
        }

        private void txtTipoManut_Unfocused(object sender, FocusEventArgs e)
        {
            Device.BeginInvokeOnMainThread(async () =>
                await _CriarSolicManutencaoVM.GetTipoByBarCodeAsync());
        }

        private void txtEquipment_Unfocused(object sender, FocusEventArgs e)
        {
            Device.BeginInvokeOnMainThread(async () =>
                await _CriarSolicManutencaoVM.GetEquipmentByBarCodeAsync());
        }
        #endregion

        private void btnSearchEquipment_Tapped(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (!_CriarSolicManutencaoVM._SearchManut.Loaded && btnSaveRequest.IsEnabled)
                    await _CriarSolicManutencaoVM.LoadSearchManutSolicAsync();
            });
        }
    }
}