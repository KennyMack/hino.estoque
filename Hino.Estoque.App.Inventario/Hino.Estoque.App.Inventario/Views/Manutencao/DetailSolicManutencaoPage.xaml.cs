﻿using Hino.Estoque.App.Models.Manutencao;
using Hino.Estoque.App.Services;
using Hino.Estoque.App.Services.ViewModels.Manutencao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Estoque.App.Views.Manutencao
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DetailSolicManutencaoPage : ContentPage
    {
        private readonly DetailSolicManutencaoVM _DetailSolicManutencaoVM;
        public DetailSolicManutencaoPage(MNSolicManutModel solic)
        {
            InitializeComponent();
            BindingContext = _DetailSolicManutencaoVM = new DetailSolicManutencaoVM(solic);
            _DetailSolicManutencaoVM.GoToMain += _DetailSolicManutencaoVM_GoToMain; ;
            _DetailSolicManutencaoVM.MessageRaise += async (evt, e) => await DisplayAlert(e.Title, e.Text, "OK");
            _DetailSolicManutencaoVM.AprovarSolicRaise += _DetailSolicManutencaoVM_AprovarSolicRaise;
            _DetailSolicManutencaoVM.ReprovarSolicRaise += _DetailSolicManutencaoVM_ReprovarSolicRaise;
        }

        private async void _DetailSolicManutencaoVM_AprovarSolicRaise(object sender, Utils.Interfaces.IMessageRaise e)
        {
            if (await DisplayAlert(e.Title, e.Text, e.Ok, e.Cancel))
                await _DetailSolicManutencaoVM.AproveSolicAsync();
        }

        private async void _DetailSolicManutencaoVM_ReprovarSolicRaise(object sender, Utils.Interfaces.IMessageRaise e)
        {
            if (await DisplayAlert(e.Title, e.Text, e.Ok, e.Cancel))
                await _DetailSolicManutencaoVM.ReproveSolicAsync();
        }

        private async void _DetailSolicManutencaoVM_GoToMain(object sender, EventArgs e) =>
            await Navigation.PopToRootAsync();

        private async void App_IsConnectedChanged(object sender, EventArgs e)
        {
            var isWIFI = CurrentApp.IsWIFI();
            if (!isWIFI)
                await DisplayAlert(Hino.Estoque.App.Resources.FieldsNameResource.Connection, Hino.Estoque.App.Resources.ErrorMessagesResource.NoInternetConnection, "OK");

            btnAprovar.IsEnabled = isWIFI;
            btnReprovar.IsEnabled = isWIFI;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            App.IsConnectedChanged += App_IsConnectedChanged;
            App_IsConnectedChanged(null, new EventArgs());
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            App.IsConnectedChanged -= App_IsConnectedChanged;
        }

        private void _DetailSolicManutencaoVM_MessageConfirmRaise(object sender, Utils.Interfaces.IMessageRaise e)
        {
        }
    }
}