﻿using Hino.Estoque.App.Models.Manutencao;
using Hino.Estoque.App.Services.ViewModels.Manutencao;
using Hino.Estoque.App.Templates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Estoque.App.Views.Manutencao
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ListSolicManutencaoPage : BaseContentPage
    {
        private readonly SolicitacoesManutencaoVM _SolicitacoesManutencaoVM;
        public ListSolicManutencaoPage()
        {
            InitializeComponent();
            BindingContext = _SolicitacoesManutencaoVM = new SolicitacoesManutencaoVM();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (_SolicitacoesManutencaoVM.Items.Count == 0)
                _SolicitacoesManutencaoVM.RefreshDataCommand.Execute(null);
        }

        private async void lvDados_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (!(e.SelectedItem is MNSolicManutModel solic))
                return;

            await Navigation.PushAsync(new DetailSolicManutencaoPage(solic));

            // Manually deselect item.
            lvDados.SelectedItem = null;
        }

        private async void CreateRequest_Clicked(object sender, EventArgs e) =>
            await Navigation.PushAsync(new SolicManutencaoPage());

    }
}