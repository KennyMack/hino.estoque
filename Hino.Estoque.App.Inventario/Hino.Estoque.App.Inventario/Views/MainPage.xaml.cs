﻿using Hino.Estoque.App.Models.Engenharia;
using Hino.Estoque.App.Services;
using Hino.Estoque.App.Services.ViewModels.Auth;
using Hino.Estoque.App.Templates;
using Hino.Estoque.App.Views.Auth;
using Hino.Estoque.App.Views.Estoque;
using Hino.Estoque.App.Views.Manutencao;
using Hino.Estoque.App.Views.Producao;
using Hino.Estoque.App.Views.Producao.Lancamento;
using Hino.Estoque.App.Views.Producao.OEE;
using Hino.Estoque.App.Views.Producao.Relatorio;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Estoque.App.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : BaseContentPage
    {
        private readonly LoginVM _LoginVM;
        public string TitlePage { get; set; }

        public MainPage()
        {
            InitializeComponent();
            this.BindingContext = _LoginVM = new LoginVM();
            _LoginVM.MessageConfirmRaise += LoginVM_MessageConfirmRaise;
            _LoginVM.GoToLogin += LoginVM_GoToLogin;
            _LoginVM.USERLOGGED = CurrentApp.UserLogged?.GEUsuarios?.codusuario ?? "";
        }

        private void LoginVM_GoToLogin(object sender, EventArgs e) =>
            App.Current.MainPage = new NavigationPage(new LoginPage());

        private async void LoginVM_MessageConfirmRaise(object sender, Utils.Interfaces.IMessageRaise e)
        {
            if (await DisplayAlert(e.Title, e.Text, "Sim", "Não"))
                _LoginVM.LogOut();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            CurrentApp.LoadPermissions();
            // await _LoginVM.LoadPendentesAync();
            if (CurrentApp.Resume)
            {
                CurrentApp.ClearPreferences();
                CurrentApp.Resume = false;

                App.Current.MainPage = new NavigationPage(new LoginPage());
            }
        }

        private void TiExit_Clicked(object sender, EventArgs e) =>
            _LoginVM.ConfirmExit();

        private async void BtnSeparate_Clicked(object sender, EventArgs e)
        {
            if (CurrentApp.ValidatePermission(TelaOpcao.Estoque))
                await Navigation.PushAsync(new SearchOrdemProdPage(OrigemSearch.Separacao));
        }

        private async void BtnInventario_Clicked(object sender, EventArgs e)
        {
            if (CurrentApp.ValidatePermission(TelaOpcao.Estoque))
                await Navigation.PushAsync(new ListInventariosPendentesPage());
        }

        private async void BtnTransfEstoque_Clicked(object sender, EventArgs e)
        {
            if (CurrentApp.ValidatePermission(TelaOpcao.Estoque))
                await Navigation.PushAsync(new ProdutoTransferenciaPage());
        }

        private async void btnHistTransf_Clicked(object sender, EventArgs e)
        {
            if (CurrentApp.ValidatePermission(TelaOpcao.Producao))
                await Navigation.PushAsync(new ListHistOrdemProdPage());
        }

        private async void BtnConsultaSaldo_Clicked(object sender, EventArgs e)
        {
            if (CurrentApp.ValidatePermission(TelaOpcao.Estoque))
                await Navigation.PushAsync(new ProdutoConsultaSaldoPage());
        }

        private async void BtnProductDetail_Clicked(object sender, EventArgs e) =>
            await Navigation.PushAsync(new ProdutoDetalhesPage());

        private async void btnTransfEndEstoque_Clicked(object sender, EventArgs e)
        {
            if (CurrentApp.ValidatePermission(TelaOpcao.Estoque))
                await Navigation.PushAsync(new ProdutoTransfEndPage());
        }

        private async void btnApontamentos_Clicked(object sender, EventArgs e)
        {
            if (CurrentApp.ValidatePermission(TelaOpcao.Producao))
                await Navigation.PushAsync(new ListApontamentosPage());
        }

        private async void btnSolicManut_Clicked(object sender, EventArgs e) =>
            await Navigation.PushAsync(new ListSolicManutencaoPage());

        private async void btnRequisicao_Clicked(object sender, EventArgs e)
        {
            if (CurrentApp.ValidatePermission(TelaOpcao.Estoque))
                await Navigation.PushAsync(new ListRequisicoesPendentesPage());
        }

        private async void btnRelatorioEficProducao_Clicked(object sender, EventArgs e)
        {
            if (CurrentApp.ValidatePermission(TelaOpcao.Producao))
                await Navigation.PushAsync(new RelatorioEficienciaProducaoPage());
        }

        private async void btnOEE_Clicked(object sender, EventArgs e)
        {
            if (CurrentApp.ValidatePermission(TelaOpcao.Producao))
                await Navigation.PushAsync(new SearchOEEPage(PageOrigem.OEE));
        }

        private async void btnOEEMensal_Clicked(object sender, EventArgs e)
        {
            if (CurrentApp.ValidatePermission(TelaOpcao.Producao))
                await Navigation.PushAsync(new SearchOEEPage(PageOrigem.OEEMENSAL));

        }
    }
}