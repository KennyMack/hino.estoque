﻿using Hino.Estoque.App.Models.Producao;
using Hino.Estoque.App.Services;
using Hino.Estoque.App.Services.ViewModels.Producao;
using Hino.Estoque.App.Templates;
using Hino.Estoque.App.Utils.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Estoque.App.Views.Producao
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ItemOrdemProdEstockPage : BaseContentPage
    {
        private bool loaded { get; set; }
        private bool _BuscandoBarras;
        private string LastFocused;
        readonly PDOrdemProdCompModel _PDOrdemProdCompModel;
        readonly ItemOPStockVM _ItemOPStockVM;
        public ItemOrdemProdEstockPage(PDOrdemProdCompModel pProdCompModel)
        {
            InitializeComponent();
            _PDOrdemProdCompModel = pProdCompModel;
            BindingContext = _ItemOPStockVM = new ItemOPStockVM(pProdCompModel);
            _ItemOPStockVM.ShowBarCode = false;
            _ItemOPStockVM.SaveSuccess += _ItemOPStockVM_SaveSuccess;
            _ItemOPStockVM.evtTextFocused += _ItemOPStockVM_evtTextFocused;
            _ItemOPStockVM.MessageRaise += _ItemOPStockVM_MessageRaise;
            viewBarcodeVCTpl.BarcodeClosed += (s, e) => _BuscandoBarras = false;
            LastFocused = "STOCK";
            loaded = false;
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();

            txtOPLancProduct.Text = $"{_PDOrdemProdCompModel.codcomponente} - {_PDOrdemProdCompModel.FSProduto_descricao}";

            _ItemOPStockVM.PRODUCT = _PDOrdemProdCompModel.codcomponente;
            await _ItemOPStockVM.GetProductByBarCodeAsync();
            App.IsConnectedChanged += App_IsConnectedChanged;
            App_IsConnectedChanged(null, new EventArgs());

            MessagingCenter.Subscribe<App, string>(this, "btnLerBarrasTapped", async (sender, args) =>
            {
                if (!_BuscandoBarras)
                {
                    _BuscandoBarras = true;
                    await viewBarcodeVCTpl.ShowBarCodeView();
                }
            });


            if (!loaded)
            {
                loaded = true;
                _ItemOPStockVM.MANUALQUANTITY = CurrentApp.UserLogged.GESettings.Settings.QuantityManual;
                if (CurrentApp.UserLogged.GESettings.Settings.UseStockOriginSuggested)
                {
                    _ItemOPStockVM.STOCK = _PDOrdemProdCompModel.codestoqueori;
                    txtStock_Completed(this, new EventArgs());
                    LastFocused = "QUANTITY";
                    txtQuantity.Focus();
                }
            }
        }

        protected async override void OnDisappearing()
        {
            base.OnDisappearing();
            App.IsConnectedChanged -= App_IsConnectedChanged;
            MessagingCenter.Unsubscribe<App, string>(this, "btnLerBarrasTapped");

            await _ItemOPStockVM.UpdateTotalQuantityAsync();
        }

        private async void App_IsConnectedChanged(object sender, EventArgs e)
        {
            var isWIFI = CurrentApp.IsWIFI();
            if (!isWIFI)
                await DisplayAlert(Hino.Estoque.App.Resources.FieldsNameResource.Connection, Hino.Estoque.App.Resources.ErrorMessagesResource.NoInternetConnection, "OK");

            btnSaveCount.IsEnabled = isWIFI;
        }

        private void _ItemOPStockVM_MessageRaise(object sender, Utils.Interfaces.IMessageRaise e) =>
            DisplayAlert(e.Title, e.Text, "OK");

        private void _ItemOPStockVM_evtTextFocused(object sender, EventArgs e)
        {
            if (sender is string)
            {
                switch (sender.ToString())
                {
                    case "STOCK":
                        txtStock.Focus();
                        break;
                    case "QUANTITY":
                        txtQuantity.Focus();
                        break;
                }
            }
        }

        private void _ItemOPStockVM_SaveSuccess(object sender, EventArgs e)
        {
            LastFocused = "STOCK";
            _ItemOPStockVM.ClearInstance();
            _ItemOPStockVM.MANUALQUANTITY = CurrentApp.UserLogged.GESettings.Settings.QuantityManual;
            if (CurrentApp.UserLogged.GESettings.Settings.UseStockOriginSuggested)
            {
                _ItemOPStockVM.STOCK = _PDOrdemProdCompModel.codestoqueori;
                txtStock_Completed(this, new EventArgs());
                LastFocused = "QUANTITY";
                txtQuantity.Focus();
            }
        }

        private void txtStock_Completed(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                _ItemOPStockVM.PRODUCT = _PDOrdemProdCompModel.codcomponente;
                await _ItemOPStockVM.GetStockByBarCodeAsync();

                if (!_ItemOPStockVM.MANUALQUANTITY &&
                    !_ItemOPStockVM.STOCK.IsEmpty())
                    LastFocused = "QUANTITY";
            });
        }

        private void txtStock_Focused(object sender, FocusEventArgs e) =>
            LastFocused = "STOCK";

        private void txtStock_Unfocused(object sender, FocusEventArgs e) =>
            txtStock_Completed(sender, e);

        private void btnInformPackage_Clicked(object sender, EventArgs e)
        {
            _ItemOPStockVM.INFORMPACKAGES = true;
            _ItemOPStockVM.INFORMEDPACKAGES = false;
            _ItemOPStockVM.PACKAGESQUANTITY = 1;
            txtPackageQuantity.Focus();
        }

        private void btnConfirmPackage_Clicked(object sender, EventArgs e)
        {
            _ItemOPStockVM.INFORMEDPACKAGES = true;
            _ItemOPStockVM.INFORMPACKAGES = false;
        }

        private void txtPackageQuantity_Completed(object sender, EventArgs e) =>
            btnConfirmPackage_Clicked(sender, e);

        private void txtQuantity_Completed(object sender, EventArgs e)
        {

        }

        private void txtQuantity_Focused(object sender, FocusEventArgs e)
        {
            LastFocused = "QUANTITY";
        }

        private void viewBarcodeVCTpl_BarcodeReadResult(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                _BuscandoBarras = false;
                if (!(sender is ZXing.Result barcodeResult))
                    return;

                if (LastFocused == "STOCK")
                {
                    _ItemOPStockVM.STOCK = barcodeResult.Text;
                    _ItemOPStockVM.PRODUCT = _PDOrdemProdCompModel.codcomponente;
                    await _ItemOPStockVM.GetStockByBarCodeAsync();

                    if (!_ItemOPStockVM.MANUALQUANTITY &&
                        !_ItemOPStockVM.STOCK.IsEmpty())
                        LastFocused = "QUANTITY";
                }
                else if (LastFocused == "QUANTITY")
                {
                    try
                    {
                        _ItemOPStockVM.PRODUCTBARCODE = barcodeResult.Text.Trim();
                        _ItemOPStockVM.CountQuantity();
                    }
                    catch (Exception)
                    {

                    }
                }
            });
        }

        private void SearchBarCode_Clicked(object sender, EventArgs e)
        {
            _ItemOPStockVM.ShowBarCode = !_ItemOPStockVM.ShowBarCode;
        }
    }
}