﻿using Hino.Estoque.App.Services.ViewModels.Producao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Estoque.App.Views.Producao
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ListHistOrdemProdPage : ContentPage
    {
        private readonly HistOrdemProdTransfVM _HistOrdemProdTransfVM;

        public ListHistOrdemProdPage()
        {
            InitializeComponent();
            BindingContext = _HistOrdemProdTransfVM = new HistOrdemProdTransfVM();
            _HistOrdemProdTransfVM.InfiniteListView(lvHistorico);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (_HistOrdemProdTransfVM.Items.Count == 0)
                _HistOrdemProdTransfVM.RefreshDataCommand.Execute(null);
        }
    }
}