﻿using Hino.Estoque.App.Models.Producao;
using Hino.Estoque.App.Services.ViewModels.Producao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Estoque.App.Views.Producao
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ListItemsOPTransfResultPage : ContentPage
    {
        public event EventHandler evtPageClosed;
        private readonly OrdemProdTransfVM _OrdemProdTransfVM;
        public ListItemsOPTransfResultPage(PDOrdemProdModel pPDOrdemProdModel, OPTransfResult pListTransf)
        {
            InitializeComponent();
            BindingContext = _OrdemProdTransfVM = new OrdemProdTransfVM(pPDOrdemProdModel, pListTransf);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (_OrdemProdTransfVM.Items.Count() == 0)
                _OrdemProdTransfVM.RefreshDataCommand.Execute(null);
        }

        private void MaterialButton_Clicked(object sender, EventArgs e) =>
            Device.BeginInvokeOnMainThread(async () => {
                await Navigation.PopModalAsync();
                evtPageClosed?.Invoke(this, e);
           });

        private void lvOPTranference_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            lvOPTranference.SelectedItem = null;
        }
    }
}