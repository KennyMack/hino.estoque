﻿using Hino.Estoque.App.Models.Producao;
using Hino.Estoque.App.Services;
using Hino.Estoque.App.Services.ViewModels.Producao;
using Hino.Estoque.App.Views.Producao.Lancamento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Estoque.App.Views.Producao
{
    public enum OrigemSearch
    {
        Separacao = 0,
        Apontamento = 1
    }
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SearchOrdemProdPage : ContentPage
    {
        private readonly SearchOrdemProdVM _SearchOrdemProdVM;
        public SearchOrdemProdPage (OrigemSearch pOrigem)
        {
            InitializeComponent ();
            BindingContext = _SearchOrdemProdVM = new SearchOrdemProdVM();
            if (pOrigem == OrigemSearch.Separacao)
                _SearchOrdemProdVM.GoToItemsOP += SearchOrdemProdVM_GoToItemsOPAsync;
            else
                _SearchOrdemProdVM.GoToItemsOP += _SearchOrdemProdVM_GoToApontamentoOPAsync;
            _SearchOrdemProdVM.ShowBarCode = true;
        }

        private async void App_IsConnectedChanged(object sender, EventArgs e)
        {
            var isWIFI = CurrentApp.IsWIFI();
            if (!isWIFI)
                await DisplayAlert(Hino.Estoque.App.Resources.FieldsNameResource.Connection, Hino.Estoque.App.Resources.ErrorMessagesResource.NoInternetConnection, "OK");

            btnLoadItems.IsEnabled = isWIFI;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            App.IsConnectedChanged += App_IsConnectedChanged;
            App_IsConnectedChanged(null, new EventArgs());
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            App.IsConnectedChanged -= App_IsConnectedChanged;
        }

        private async void SearchOrdemProdVM_GoToItemsOPAsync(object sender, EventArgs e)
        {
            if (!CurrentApp.UserLogged.GESettings.Settings.AllowChangeStockOrigin)
                await Navigation.PushAsync(new ListItemsOrdemProdPage((PDOrdemProdModel)sender));
            else
                await Navigation.PushAsync(new ListItemsOrdemProdEstockPage((PDOrdemProdModel)sender));

        }

        private async void _SearchOrdemProdVM_GoToApontamentoOPAsync(object sender, EventArgs e) =>
            await Navigation.PushAsync(new AppointmentsMainPage((PDOrdemProdModel)sender));

        private void ViewBarcodeVCTpl_BarcodeReadResult(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                if (!(sender is ZXing.Result barcodeResult))
                    return;

                _SearchOrdemProdVM.OPIDENTIFIER = barcodeResult.Text;
                _SearchOrdemProdVM.CmdGetOPItemsClick.Execute(null);
            });
        }

        private void TxtOpIdentifier_Completed(object sender, EventArgs e)
        {
            if (btnLoadItems.IsEnabled)
                _SearchOrdemProdVM.CmdGetOPItemsClick.Execute(null);
        }
    }
}