﻿using Hino.Estoque.App.Models.Producao;
using Hino.Estoque.App.Services;
using Hino.Estoque.App.Services.ViewModels.Producao.Apontamento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Estoque.App.Views.Producao.Lancamento
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AppointmentsMainPage : ContentPage
    {
        public AppointmentsMainPage(PDOrdemProdModel pOrdemProd)
        {
            InitializeComponent();
            viewOrdemProdDescription.OrdemProd = pOrdemProd;
            viewAppointmentsOptions.OrdemProd = pOrdemProd;
            App.IsConnectedChanged += App_IsConnectedChanged;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            App_IsConnectedChanged(null, new EventArgs());
        }

        private async void App_IsConnectedChanged(object sender, EventArgs e)
        {
            var isWIFI = CurrentApp.IsWIFI();
            if (!isWIFI)
                await DisplayAlert(Hino.Estoque.App.Resources.FieldsNameResource.Connection, Hino.Estoque.App.Resources.ErrorMessagesResource.NoInternetConnection, "OK");

            viewAppointmentsOptions.StatusButtons(isWIFI);
        }
    }
}