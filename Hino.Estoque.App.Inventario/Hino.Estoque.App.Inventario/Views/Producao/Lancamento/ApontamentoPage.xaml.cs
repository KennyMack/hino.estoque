﻿using Hino.Estoque.App.Models.Producao;
using Hino.Estoque.App.Services;
using Hino.Estoque.App.Services.ViewModels.Producao.Apontamento;
using Hino.Estoque.App.Utils.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XF.Material.Forms.UI;

namespace Hino.Estoque.App.Views.Producao.Lancamento
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ApontamentoPage : ContentPage
    {
        private AptInicioProdVM _AptInicioProdVM;
        private AptTerminoProdVM _AptTerminoProdVM;
        private readonly ApontamentoProdVM _ApontamentoProdVM;
        private readonly PDOrdemProdModel _PDOrdemProdModel;
        public ApontamentoPage(PDOrdemProdModel pOrdemProd)
        {
            InitializeComponent();
            BindingContext = _ApontamentoProdVM = new ApontamentoProdVM(pOrdemProd);
            _PDOrdemProdModel = pOrdemProd;
        }

        private async void App_IsConnectedChanged(object sender, EventArgs e)
        {
            var isWIFI = CurrentApp.IsWIFI();
            if (!isWIFI)
                await DisplayAlert(Hino.Estoque.App.Resources.FieldsNameResource.Connection, Hino.Estoque.App.Resources.ErrorMessagesResource.NoInternetConnection, "OK");

            btnStartProduction.IsEnabled = isWIFI;
            btnFinishProduction.IsEnabled = isWIFI;
            btnFinishTurnProduction.IsEnabled = isWIFI;
            btnFinishParada.IsEnabled = isWIFI;
            btnStartParada.IsEnabled = isWIFI;

            btnConfirmAppointmentFinish.IsEnabled = isWIFI;
            btnCancelAppointmentFinish.IsEnabled = isWIFI;

            btnCancelAppointment.IsEnabled = isWIFI;
            btnConfirmAppointmentStart.IsEnabled = isWIFI;
            
            btnCancelAppointmentStart.IsEnabled = isWIFI;
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();

            txtOPLancDescription.Text = $"{_PDOrdemProdModel.codordprod} - {_PDOrdemProdModel.nivelordprod}";
            txtOPLancProduct.Text = $"{_PDOrdemProdModel.codproduto} - {_PDOrdemProdModel.fsproduto.descricao}";

            _ApontamentoProdVM.ShowHeader = true;
            _ApontamentoProdVM.ShowOptions = true;
            await _ApontamentoProdVM.GetMotivosAsync();
            await _ApontamentoProdVM.GetStopMotivosAsync();

            App.IsConnectedChanged += App_IsConnectedChanged;
            App_IsConnectedChanged(null, new EventArgs());
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            App.IsConnectedChanged -= App_IsConnectedChanged;
        }

        private void CreateOperationOptions()
        {
            _ApontamentoProdVM.SelectedOperation = -1;
            _ApontamentoProdVM.QtdBoas = 0;
            _ApontamentoProdVM.QtdRefugo = 0;
            _ApontamentoProdVM.MotivosNome.Clear();
            _ApontamentoProdVM.ParadaMotivosNome.Clear();
            slOperations.Children.Clear();
            foreach (var item in _ApontamentoProdVM.Rotinas)
            {
                var button = new RadioButton
                {
                    GroupName = "Operation",
                    Value = item.operacao,
                    Margin = new Thickness(8, 8),
                    Content = item.DetailOperacao
                };
                button.CheckedChanged += Button_CheckedChanged;

                slOperations.Children.Add(button);
            }

            foreach (var item in _ApontamentoProdVM.Motivos.OrderBy(r => r.codmotivo))
                _ApontamentoProdVM.MotivosNome.Add($"{item.codmotivo} - {item.descricao}");

            foreach (var item in _ApontamentoProdVM.ParadaMotivos.OrderBy(r => r.codmotivo))
                _ApontamentoProdVM.ParadaMotivosNome.Add($"{item.codmotivo} - {item.descricao}");
        }

        private void Button_CheckedChanged(object sender, CheckedChangedEventArgs e)
        {
            _ApontamentoProdVM.SelectedOperation = -1;
            if (e.Value)
                _ApontamentoProdVM.SelectedOperation = Convert.ToInt32(((RadioButton)sender).Value);
            if (_ApontamentoProdVM.IsStart && _ApontamentoProdVM.SelectedOperation > -1)
            {
                _ApontamentoProdVM.ShowHeader = false;
                _ApontamentoProdVM.ShowOperations = false;
                _ApontamentoProdVM.ShowCancelation = false;
                _ApontamentoProdVM.ShowConfirmation = true;

                _AptInicioProdVM = new AptInicioProdVM(_PDOrdemProdModel, _ApontamentoProdVM.SelectedOperation);
                txtOPDescription.Text = _AptInicioProdVM.OPDescription;
                txtOPProduct.Text = _AptInicioProdVM.OPProduct;
                txtUsuario.Text = _AptInicioProdVM.Usuario;
                txtOPOperacao.Text = _AptInicioProdVM.Operacao;
                txtOPDtInicio.Text = _AptInicioProdVM.OPDTInicio.ToString("dd/MM/yyyy HH:mm");

                _AptInicioProdVM.MessageRaise -= _AptTerminoProdVM_MessageRaise;
                _AptInicioProdVM.MessageRaise += _AptTerminoProdVM_MessageRaise;
                _AptInicioProdVM.GoToStart -= _AptInicioProdVM_GoToStart;
                _AptInicioProdVM.GoToStart += _AptInicioProdVM_GoToStart;
            }
            else if (_ApontamentoProdVM.IsTurnFinish && _ApontamentoProdVM.SelectedOperation > -1)
            {
                _ApontamentoProdVM.ShowHeader = false;
                _ApontamentoProdVM.ShowOperations = false;
                _ApontamentoProdVM.ShowCancelation = false;
                _ApontamentoProdVM.ShowConfirmation = true;

                _AptTerminoProdVM = new AptTerminoProdVM(_PDOrdemProdModel,
                   _ApontamentoProdVM.SelectedOperation,
                   0,
                   0,
                   "");
                txtOPDescription.Text = _AptTerminoProdVM.OPDescription;
                txtOPProduct.Text = _AptTerminoProdVM.OPProduct;
                txtUsuario.Text = _AptTerminoProdVM.Usuario;
                txtOPOperacao.Text = _AptTerminoProdVM.Operacao;
                txtOPDtInicio.Text = _AptTerminoProdVM.OPDTTermino.ToString("dd/MM/yyyy HH:mm");
                _AptTerminoProdVM.GoToStart -= _AptInicioProdVM_GoToStart;
                _AptTerminoProdVM.GoToStart += _AptInicioProdVM_GoToStart;
                _AptTerminoProdVM.MessageRaise -= _AptTerminoProdVM_MessageRaise;
                _AptTerminoProdVM.MessageRaise += _AptTerminoProdVM_MessageRaise;
            }
            else if (_ApontamentoProdVM.IsStartParada && _ApontamentoProdVM.SelectedOperation > -1)
            {
                _ApontamentoProdVM.ShowHeader = false;
                _ApontamentoProdVM.ShowOperations = false;
                _ApontamentoProdVM.ShowCancelation = false;
                _ApontamentoProdVM.ShowConfirmation = true;
                _ApontamentoProdVM.IsContinueFinishParada = false;

                _AptInicioProdVM = new AptInicioProdVM(_PDOrdemProdModel, _ApontamentoProdVM.SelectedOperation);
                txtOPDescription.Text = _AptInicioProdVM.OPDescription;
                txtOPProduct.Text = _AptInicioProdVM.OPProduct;
                txtUsuario.Text = _AptInicioProdVM.Usuario;
                txtOPOperacao.Text = _AptInicioProdVM.Operacao;
                txtOPDtInicio.Text = _AptInicioProdVM.OPDTInicio.ToString("dd/MM/yyyy HH:mm");

                _AptInicioProdVM.MessageRaise -= _AptTerminoProdVM_MessageRaise;
                _AptInicioProdVM.MessageRaise += _AptTerminoProdVM_MessageRaise;
                _AptInicioProdVM.GoToStart -= _AptInicioProdVM_GoToStart;
                _AptInicioProdVM.GoToStart += _AptInicioProdVM_GoToStart;
            }
            else if (_ApontamentoProdVM.IsFinishParada && _ApontamentoProdVM.SelectedOperation > -1)
            {
                csMotivo.SelectedChoice = null;
                csMotivoParada.SelectedChoice = null;
                csMotivo.Text = null;
                csMotivoParada.Text = null;
                txtParadaObservacao.Text = "";

                _AptTerminoProdVM = new AptTerminoProdVM(_PDOrdemProdModel,
                   _ApontamentoProdVM.SelectedOperation,
                   0,
                   0,
                   "");
                txtOPDescription.Text = _AptTerminoProdVM.OPDescription;
                txtOPProduct.Text = _AptTerminoProdVM.OPProduct;
                txtUsuario.Text = _AptTerminoProdVM.Usuario;
                txtOPOperacao.Text = _AptTerminoProdVM.Operacao;
                txtOPDtInicio.Text = _AptTerminoProdVM.OPDTTermino.ToString("dd/MM/yyyy HH:mm");
                _AptTerminoProdVM.GoToStart -= _AptInicioProdVM_GoToStart;
                _AptTerminoProdVM.GoToStart += _AptInicioProdVM_GoToStart;
                _AptTerminoProdVM.MessageRaise -= _AptTerminoProdVM_MessageRaise;
                _AptTerminoProdVM.MessageRaise += _AptTerminoProdVM_MessageRaise;
            }

        }

        private void _AptInicioProdVM_GoToStart(object sender, EventArgs e)
        {
            _ApontamentoProdVM.ShowHeader = true;
            _ApontamentoProdVM.ShowOptions = true;
            _ApontamentoProdVM.ShowOperations = false;
            _ApontamentoProdVM.ShowQtd = false;
            _ApontamentoProdVM.ShowRefugo = false;
            _ApontamentoProdVM.ShowConfirmation = false;
            _ApontamentoProdVM.IsFinish = false;
            _ApontamentoProdVM.IsTurnFinish = false;
            _ApontamentoProdVM.IsStart = false;
            _ApontamentoProdVM.IsStartParada = false;
            _ApontamentoProdVM.IsFinishParada = false;
            _ApontamentoProdVM.ShowCancelation = false;
            _ApontamentoProdVM.IsContinueFinishParada = false;
            CreateOperationOptions();
        }

        private void btnStartProduction_Clicked(object sender, EventArgs e)
        {
            _ApontamentoProdVM.ShowHeader = true;
            _ApontamentoProdVM.ShowOptions = false;
            _ApontamentoProdVM.ShowOperations = true;
            _ApontamentoProdVM.ShowQtd = false;
            _ApontamentoProdVM.ShowRefugo = false;
            _ApontamentoProdVM.ShowConfirmation = false;
            _ApontamentoProdVM.IsFinish = false;
            _ApontamentoProdVM.IsTurnFinish = false;
            _ApontamentoProdVM.IsStart = true;
            _ApontamentoProdVM.IsStartParada = false;
            _ApontamentoProdVM.IsFinishParada = false;
            _ApontamentoProdVM.ShowCancelation = true;
            _ApontamentoProdVM.IsContinueFinishParada = false;
            CreateOperationOptions();
        }

        private void btnFinishProduction_Clicked(object sender, EventArgs e)
        {
            _ApontamentoProdVM.ShowHeader = true;
            _ApontamentoProdVM.ShowOptions = false;
            _ApontamentoProdVM.ShowOperations = true;
            _ApontamentoProdVM.ShowQtd = true;
            _ApontamentoProdVM.ShowRefugo = false;
            _ApontamentoProdVM.ShowConfirmation = false;
            _ApontamentoProdVM.IsFinish = true;
            _ApontamentoProdVM.IsTurnFinish = false;
            _ApontamentoProdVM.IsStart = false;
            _ApontamentoProdVM.IsStartParada = false;
            _ApontamentoProdVM.IsFinishParada = false;
            _ApontamentoProdVM.ShowCancelation = true;
            _ApontamentoProdVM.IsContinueFinishParada = false;
            CreateOperationOptions();
        }

        private void btnFinishTurnProduction_Clicked(object sender, EventArgs e)
        {
            _ApontamentoProdVM.ShowHeader = true;
            _ApontamentoProdVM.ShowOptions = false;
            _ApontamentoProdVM.ShowOperations = true;
            _ApontamentoProdVM.ShowQtd = false;
            _ApontamentoProdVM.ShowRefugo = false;
            _ApontamentoProdVM.ShowConfirmation = false;
            _ApontamentoProdVM.IsFinish = false;
            _ApontamentoProdVM.IsTurnFinish = true;
            _ApontamentoProdVM.IsStart = false;
            _ApontamentoProdVM.IsStartParada = false;
            _ApontamentoProdVM.IsFinishParada = false;
            _ApontamentoProdVM.ShowCancelation = true;
            _ApontamentoProdVM.IsContinueFinishParada = false;
            CreateOperationOptions();
        }

        private void btnCancelAppointment_Clicked(object sender, EventArgs e)
        {
            _ApontamentoProdVM.ShowHeader = true;
            _ApontamentoProdVM.ShowOptions = true;
            _ApontamentoProdVM.ShowOperations = false;
            _ApontamentoProdVM.ShowQtd = false;
            _ApontamentoProdVM.ShowRefugo = false;
            _ApontamentoProdVM.ShowConfirmation = false;
            _ApontamentoProdVM.IsFinish = false;
            _ApontamentoProdVM.IsTurnFinish = false;
            _ApontamentoProdVM.IsStart = false;
            _ApontamentoProdVM.IsStartParada = false;
            _ApontamentoProdVM.IsFinishParada = false;
            _ApontamentoProdVM.ShowCancelation = false;
            _ApontamentoProdVM.IsContinueFinishParada = false;
            CreateOperationOptions();
        }

        private async void btnConfirmAppointmentStart_Clicked(object sender, EventArgs e) =>
            await _AptInicioProdVM.Iniciar();

        private void btnCancelAppointmentStart_Clicked(object sender, EventArgs e)
        {
            _ApontamentoProdVM.ShowHeader = true;
            _ApontamentoProdVM.ShowOperations = true;
            _ApontamentoProdVM.ShowCancelation = true;
            _ApontamentoProdVM.ShowConfirmation = false;
            _ApontamentoProdVM.IsContinueFinishParada = false;
            CreateOperationOptions();
        }

        private void TxtQuantity_Completed(object sender, EventArgs e)
        {
        }

        private void TxtQuantity_Unfocused(object sender, FocusEventArgs e)
        {
            TxtQuantity_Completed(sender, null);
        }

        private void btnCancelAppointmentFinish_Clicked(object sender, EventArgs e)
        {
            _ApontamentoProdVM.ShowHeader = true;
            _ApontamentoProdVM.ShowOptions = false;
            _ApontamentoProdVM.ShowOperations = true;
            _ApontamentoProdVM.ShowQtd = true;
            _ApontamentoProdVM.ShowRefugo = false;
            _ApontamentoProdVM.ShowConfirmation = false;
            _ApontamentoProdVM.IsFinish = true;
            _ApontamentoProdVM.IsTurnFinish = false;
            _ApontamentoProdVM.IsStart = false;
            _ApontamentoProdVM.IsStartParada = false;
            _ApontamentoProdVM.IsFinishParada = false;
            _ApontamentoProdVM.ShowCancelation = true;
            _ApontamentoProdVM.IsContinueFinishParada = false;
            CreateOperationOptions();
            _AptTerminoProdVM.RefugoMotivo.Clear();
        }

        private void btnCancelAppointmentTurnFinish_Clicked(object sender, EventArgs e)
        {
            btnCancelAppointment_Clicked(sender, e);
            CreateOperationOptions();
            _AptTerminoProdVM.RefugoMotivo.Clear();
        }

        private async void btnConfirmAppointmentTurnFinish_Clicked(object sender, EventArgs e) =>
            await _AptTerminoProdVM.FinalizarTurnoAsync();

        private async void btnConfirmAppointmentFinish_Clicked(object sender, EventArgs e) =>
            await _AptTerminoProdVM.Finalizar();

        private void btnContinueFinish_Clicked(object sender, EventArgs e)
        {
            decimal.TryParse(txtQuantity.Text, out decimal qtd);
            _ApontamentoProdVM.QtdBoas = qtd;
            decimal.TryParse(txtDigQtdRefugo.Text, out decimal qtdrefugo);
            _ApontamentoProdVM.QtdRefugo = qtdrefugo;

            if (_ApontamentoProdVM.ValidarFinishContinue())
            {
                _AptTerminoProdVM = new AptTerminoProdVM(_PDOrdemProdModel,
                    _ApontamentoProdVM.SelectedOperation,
                    _ApontamentoProdVM.QtdBoas,
                    _ApontamentoProdVM.QtdRefugo,
                    _ApontamentoProdVM.Observacao);
                txtOPDescription.Text = _AptTerminoProdVM.OPDescription;
                txtOPProduct.Text = _AptTerminoProdVM.OPProduct;
                txtUsuario.Text = _AptTerminoProdVM.Usuario;
                txtOPOperacao.Text = _AptTerminoProdVM.Operacao;
                txtQtdBoas.Text = $"Boas: {_ApontamentoProdVM.QtdBoas}";
                txtQtdRefugo.Text = $"Refugo: {_ApontamentoProdVM.QtdRefugo}";
                txtOPDtInicio.Text = _AptTerminoProdVM.OPDTTermino.ToString("dd/MM/yyyy HH:mm");
                _AptTerminoProdVM.GoToStart -= _AptInicioProdVM_GoToStart;
                _AptTerminoProdVM.GoToStart += _AptInicioProdVM_GoToStart;
                _AptTerminoProdVM.MessageRaise -= _AptTerminoProdVM_MessageRaise;
                _AptTerminoProdVM.MessageRaise += _AptTerminoProdVM_MessageRaise;
                _AptTerminoProdVM.RefugoMotivo.CollectionChanged -= RefugoMotivo_CollectionChanged;
                _AptTerminoProdVM.RefugoMotivo.CollectionChanged += RefugoMotivo_CollectionChanged;

                if (_AptTerminoProdVM.QtdRefugo <= 0)
                {
                    _ApontamentoProdVM.ShowHeader = false;
                    _ApontamentoProdVM.ShowOperations = false;
                    _ApontamentoProdVM.ShowQtd = false;
                    _ApontamentoProdVM.ShowRefugo = false;
                    _ApontamentoProdVM.ShowConfirmation = true;
                    _ApontamentoProdVM.ShowCancelation = false;
                }
                else
                {
                    _ApontamentoProdVM.ShowOperations = false;
                    _ApontamentoProdVM.ShowQtd = false;
                    _ApontamentoProdVM.ShowRefugo = true;
                    _ApontamentoProdVM.ShowConfirmation = false;
                    _ApontamentoProdVM.ShowCancelation = true;
                    LoadRefugoList();
                    RecalculaRefugoRestante();
                }
            }
        }

        private void _AptTerminoProdVM_MessageRaise(object sender, Utils.Interfaces.IMessageRaise e)
        {
            DisplayAlert(e.Title, e.Text, "OK");
        }

        private void btnContinueRefugoFinish_Clicked(object sender, EventArgs e)
        {
            if (_AptTerminoProdVM.ValidarRefugos())
            {
                _ApontamentoProdVM.ShowHeader = false;
                _ApontamentoProdVM.ShowOperations = false;
                _ApontamentoProdVM.ShowQtd = false;
                _ApontamentoProdVM.ShowRefugo = false;
                _ApontamentoProdVM.ShowConfirmation = true;
                _ApontamentoProdVM.ShowCancelation = false;
                _ApontamentoProdVM.IsContinueFinishParada = false;
            }
        }

        private void RecalculaRefugoRestante()
        {
            try
            {
                var qtdRefugo = _AptTerminoProdVM.QtdRefugo;
                txtQtdRestante.Text = $"Refugo: {_AptTerminoProdVM.RefugoMotivo.Sum(r => r.Quantidade):n4} de {qtdRefugo:n4}";
            }
            catch (Exception)
            {
            }
        }

        private void LoadRefugoList()
        {
            try
            {
                ItensRefugo.Choices?.Clear();
                ItensRefugo.Choices = null;
                var items = new List<string>();
                foreach (var item in _AptTerminoProdVM.RefugoMotivo.OrderBy(r => r.CodMotivo))
                    items.Add($"{item.CodMotivo} - {item.Descricao} ({item.Quantidade:n4})");
                if (!items.Any())
                    items.Add("Nenhum refugo informado");

                ItensRefugo.Choices = items;
                ItensRefugo.ForceLayout();
                
            }
            catch (Exception)
            {

            }
        }

        private void RefugoMotivo_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            LoadRefugoList();
        }

        private void txtQtdRefugo_Completed(object sender, EventArgs e)
        {
            btnContinueFinish_Clicked(sender, e);
        }

        private void txtQtdRefugo_Unfocused(object sender, FocusEventArgs e)
        {
            txtQtdRefugo_Completed(sender, null);
        }

        private void csMotivo_ChoiceSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (_AptTerminoProdVM != null)
                _AptTerminoProdVM.MotivoSelecionado = null;//  e.SelectedItem.ToString();
        }

        private void txtQtdRefugoMot_Unfocused(object sender, FocusEventArgs e)
        {

        }

        private void txtQtdRefugoMot_Completed(object sender, EventArgs e)
        {
            btnAdicionarRefugo_Clicked(sender, e);
        }

        private void btnAdicionarRefugo_Clicked(object sender, EventArgs e)
        {
            if (_AptTerminoProdVM != null /*&& !(_AptTerminoProdVM?.MotivoSelecionado ?? "").IsEmpty()*/)
            {
                decimal.TryParse(txtQtdRefugoMot.Text, out decimal qtdrefugo);

                if (qtdrefugo > 0)
                {
                    var motivo = "";// _AptTerminoProdVM.MotivoSelecionado.Split('-');
                    var codmotivo = Convert.ToInt32(motivo[0]);
                    var descmotivo = motivo[1];

                    var itemExists = _AptTerminoProdVM.RefugoMotivo.Where(r => r.CodMotivo == codmotivo).FirstOrDefault();

                    if (itemExists != null)
                    {
                        itemExists.Quantidade += _ApontamentoProdVM.QtdItemRefugo;
                        LoadRefugoList();
                    }
                    else
                    {
                        _AptTerminoProdVM.RefugoMotivo.Add(new Models.Producao.Apontamento.PDAptLancRefugoModel
                        {
                            CodMotivo = codmotivo,
                            Descricao = "descmotivo",
                            Quantidade = _ApontamentoProdVM.QtdItemRefugo
                        });
                    }
                    _AptTerminoProdVM.MotivoSelecionado = null;
                    _ApontamentoProdVM.QtdItemRefugo = 0;
                    csMotivo.SelectedChoice = null;
                    csMotivo.Text = null;
                }
            }
            RecalculaRefugoRestante();
        }

        private async void btnRemoverSelecionados_Clicked(object sender, EventArgs e)
        {
            var resp = await DisplayAlert("Confirma", "Remover itens selecionados?", "REMOVER", "MANTER");

            if (resp)
            {
                try
                {
                    var items = ItensRefugo.SelectedIndices;
                    foreach (var item in items)
                        _AptTerminoProdVM.RefugoMotivo.RemoveAt(item);
                }
                catch (Exception)
                {

                }
                LoadRefugoList();
                RecalculaRefugoRestante();
            }
        }

        private void btnStartParada_Clicked(object sender, EventArgs e)
        {
            _ApontamentoProdVM.ShowHeader = true;
            _ApontamentoProdVM.ShowOptions = false;
            _ApontamentoProdVM.ShowOperations = true;
            _ApontamentoProdVM.ShowQtd = false;
            _ApontamentoProdVM.ShowRefugo = false;
            _ApontamentoProdVM.ShowConfirmation = false;
            _ApontamentoProdVM.IsFinish = false;
            _ApontamentoProdVM.IsTurnFinish = false;
            _ApontamentoProdVM.IsStart = false;
            _ApontamentoProdVM.IsStartParada = true;
            _ApontamentoProdVM.IsFinishParada = false;
            _ApontamentoProdVM.ShowCancelation = true;
            CreateOperationOptions();
        }

        private void btnFinishParada_Clicked(object sender, EventArgs e)
        {
            _ApontamentoProdVM.ShowHeader = true;
            _ApontamentoProdVM.ShowOptions = false;
            _ApontamentoProdVM.ShowOperations = true;
            _ApontamentoProdVM.ShowQtd = false;
            _ApontamentoProdVM.ShowRefugo = false;
            _ApontamentoProdVM.ShowConfirmation = false;
            _ApontamentoProdVM.IsFinish = false;
            _ApontamentoProdVM.IsTurnFinish = false;
            _ApontamentoProdVM.IsStart = false;
            _ApontamentoProdVM.IsStartParada = false;
            _ApontamentoProdVM.IsFinishParada = true;
            _ApontamentoProdVM.ShowCancelation = true;
            _ApontamentoProdVM.IsContinueFinishParada = false;
            CreateOperationOptions();
        }

        private void btnCancelAppointmentStartParada_Clicked(object sender, EventArgs e)
        {
            _ApontamentoProdVM.IsStartParada = false;
            _ApontamentoProdVM.IsFinishParada = false;
            _ApontamentoProdVM.IsContinueFinishParada = false;
            btnCancelAppointment_Clicked(sender, e);
        }

        private async void btnConfirmAppointmentStartParada_Clicked(object sender, EventArgs e) =>
            await _AptInicioProdVM.IniciarParadaAsync();

        private void btnCancelAppointmentFinishParada_Clicked(object sender, EventArgs e)
        {
            _ApontamentoProdVM.IsStartParada = false;
            _ApontamentoProdVM.IsFinishParada = false;
            _ApontamentoProdVM.IsContinueFinishParada = false;
            btnCancelAppointment_Clicked(sender, e);
        }

        private async void btnConfirmAppointmentFinishParada_Clicked(object sender, EventArgs e)
        {
            _AptTerminoProdVM.Observacao = txtParadaObservacao.Text;
            await _AptTerminoProdVM.FinalizarParadaAsync();
        }

        private void csMotivoParada_ChoiceSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (_AptTerminoProdVM != null)
                _AptTerminoProdVM.MotivoSelecionado = null; //e.SelectedItem.ToString();
        }

        private void btnContinueFinishParada_Clicked(object sender, EventArgs e)
        {
            if (!_ApontamentoProdVM.ValidarFinishParadaContinue())
                return;

            if (_AptTerminoProdVM.ValidaMotivoParada())
            {
                _ApontamentoProdVM.ShowHeader = false;
                _ApontamentoProdVM.ShowOperations = false;
                _ApontamentoProdVM.ShowQtd = false;
                _ApontamentoProdVM.ShowRefugo = false;
                _ApontamentoProdVM.IsFinishParada = false;
                _ApontamentoProdVM.ShowConfirmation = true;
                _ApontamentoProdVM.ShowCancelation = false;
                _ApontamentoProdVM.IsContinueFinishParada = true;
            }
        }

        protected override bool OnBackButtonPressed()
        {
            Device.BeginInvokeOnMainThread(async () => {
                var result = await this.DisplayAlert(Hino.Estoque.App.Resources.ValidationMessagesResource.Confirm,
                    Hino.Estoque.App.Resources.ValidationMessagesResource.BackConfirmMessage,
                    Hino.Estoque.App.Resources.ValidationMessagesResource.ConfirmBack,
                    Hino.Estoque.App.Resources.ValidationMessagesResource.Stay);
                if (result) await this.Navigation.PopAsync();
            });

            return true;
        }
    }
}