﻿using Hino.Estoque.App.Services.ViewModels.Producao.Apontamento;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Estoque.App.Views.Producao.Lancamento
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ListApontamentosPage : ContentPage
    {
        private readonly ApontamentoProdVM _ApontamentoProdVM;
        public ListApontamentosPage()
        {
            InitializeComponent();
            BindingContext = _ApontamentoProdVM = new ApontamentoProdVM();
        }

        private async void CreateAppointment_Clicked(object sender, EventArgs e) =>
            await Navigation.PushAsync(new SearchOrdemProdPage(OrigemSearch.Apontamento));

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (_ApontamentoProdVM.Items.Count == 0)
                _ApontamentoProdVM.RefreshDataCommand.Execute(null);
        }
    }
}
