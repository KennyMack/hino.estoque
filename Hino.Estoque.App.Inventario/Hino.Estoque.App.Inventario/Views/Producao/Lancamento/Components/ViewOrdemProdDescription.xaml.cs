﻿using Hino.Estoque.App.Models.Producao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Estoque.App.Views.Producao.Lancamento.Components
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ViewOrdemProdDescription : ContentView
    {
        public PDOrdemProdModel OrdemProd
        {
            get { return (PDOrdemProdModel)GetValue(OrdemProdProperty); }
            set { SetValue(OrdemProdProperty, value); }
        }

        public static readonly BindableProperty OrdemProdProperty =
            BindableProperty.Create(nameof(OrdemProd), typeof(PDOrdemProdModel), typeof(ViewOrdemProdDescription));

        public ViewOrdemProdDescription()
        {
            InitializeComponent();
            _content.BindingContext = this;
        }
    }
}