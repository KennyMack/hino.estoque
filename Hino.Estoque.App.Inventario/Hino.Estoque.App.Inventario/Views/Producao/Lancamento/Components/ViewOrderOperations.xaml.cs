﻿using Hino.Estoque.App.Models.Producao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Estoque.App.Views.Producao.Lancamento.Components
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ViewOrderOperations : ContentView
    {
        public event EventHandler OperationSelected;

        public PDOrdemProdModel OrdemProd
        {
            get { return (PDOrdemProdModel)GetValue(OrdemProdProperty); }
            set
            {
                SetValue(OrdemProdProperty, value);
                SettingsValueChanged(value);

            }
        }

        public static readonly BindableProperty OrdemProdProperty =
            BindableProperty.Create(nameof(OrdemProd), typeof(PDOrdemProdModel), typeof(ViewOrdemProdDescription));

        public void SettingsValueChanged(PDOrdemProdModel newValue)
        {
            Operations.Children.Clear();
            foreach (var item in newValue.pdordemprodrotinas.OrderBy(r => r.operacao))
            {
                if (item.aptoprocesso || item.aptoacabado)
                {
                    var button = new RadioButton
                    {
                        GroupName = "Operation",
                        Value = item.operacao,
                        Margin = new Thickness(8, 8),
                        Content = item.DetailOperacao
                    };
                    button.CheckedChanged += Button_CheckedChanged;

                    Operations.Children.Add(button);
                }
            }
        }

        private void Button_CheckedChanged(object sender, CheckedChangedEventArgs e) =>
            OperationSelected?.Invoke(sender, e);

        public ViewOrderOperations()
        {
            InitializeComponent();
            _content.BindingContext = this;
        }
    }
}