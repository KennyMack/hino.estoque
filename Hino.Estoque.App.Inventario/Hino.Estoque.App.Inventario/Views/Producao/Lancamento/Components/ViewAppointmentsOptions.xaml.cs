﻿using Hino.Estoque.App.Models.Producao;
using Hino.Estoque.App.Services;
using Hino.Estoque.App.Views.Producao.Sucata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Estoque.App.Views.Producao.Lancamento.Components
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ViewAppointmentsOptions : ContentView
    {
        public PDOrdemProdModel OrdemProd
        {
            get { return (PDOrdemProdModel)GetValue(OrdemProdProperty); }
            set { SetValue(OrdemProdProperty, value); }
        }

        public static readonly BindableProperty OrdemProdProperty =
            BindableProperty.Create(nameof(OrdemProd), typeof(PDOrdemProdModel), typeof(ViewOrdemProdDescription));

        public ViewAppointmentsOptions ()
        {
            InitializeComponent ();
            _content.BindingContext = this;
        }

        public void StatusButtons(bool pBtn)
        {
            btnStartProduction.IsEnabled = pBtn;
            btnFinishProduction.IsEnabled = pBtn;
            btnFinishTurnProduction.IsEnabled = pBtn;
            btnStartParada.IsEnabled = pBtn;
            btnFinishParada.IsEnabled = pBtn;
            btnApontSucata.IsEnabled = pBtn;
        }

        private async void btnStartProduction_Clicked(object sender, EventArgs e)
        {
            if (CurrentApp.ValidatePermission(TelaOpcao.Producao))
                await Navigation.PushAsync(new AppointmentsMainTabPage(OperationAppointment.StartProduction, OrdemProd));
        }

        private async void btnFinishProduction_Clicked(object sender, EventArgs e)
        {
            if (CurrentApp.ValidatePermission(TelaOpcao.Producao))
                await Navigation.PushAsync(new AppointmentsMainTabPage(OperationAppointment.EndProduction, OrdemProd));
        }

        private async void btnFinishTurnProduction_Clicked(object sender, EventArgs e)
        {
            if (CurrentApp.ValidatePermission(TelaOpcao.FimTurno))
                await Navigation.PushAsync(new AppointmentsMainTabPage(OperationAppointment.EndTurnProduction, OrdemProd));
        }

        private async void btnStartParada_Clicked(object sender, EventArgs e)
        {
            if (CurrentApp.ValidatePermission(TelaOpcao.Producao))
                await Navigation.PushAsync(new AppointmentsMainTabPage(OperationAppointment.StartEvent, OrdemProd));
        }

        private async void btnFinishParada_Clicked(object sender, EventArgs e)
        {
            if (CurrentApp.ValidatePermission(TelaOpcao.Producao))
                await Navigation.PushAsync(new AppointmentsMainTabPage(OperationAppointment.EndEvent, OrdemProd));
        }

        private async void btnApontSucata_Clicked(object sender, EventArgs e)
        {
            if (CurrentApp.ValidatePermission(TelaOpcao.Producao))
                await Navigation.PushAsync(new ApontSucataPage(OrdemProd));
        }

        private void btnApontRefugo_Clicked(object sender, EventArgs e)
        {

        }
    }
}