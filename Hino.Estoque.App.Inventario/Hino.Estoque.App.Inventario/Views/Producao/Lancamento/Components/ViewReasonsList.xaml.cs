﻿using Hino.Estoque.App.Services.ViewModels.Producao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Estoque.App.Views.Producao.Lancamento.Components
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ViewReasonsList : ContentView
    {
        public event EventHandler ReasonSelected;
        private readonly MotivosVM _MotivosVM;

        public string LBLReasonPlaceholder
        {
            get { return (string)GetValue(LBLReasonPlaceholderProperty); }
            set { SetValue(LBLReasonPlaceholderProperty, value); }
        }

        public static readonly BindableProperty LBLReasonPlaceholderProperty =
            BindableProperty.Create(nameof(LBLReasonPlaceholder), typeof(string), typeof(ViewReasonsList));

        public ViewReasonsList()
        {
            InitializeComponent();
            _content.BindingContext = _MotivosVM = new MotivosVM();
        }

        public async Task OnAppearing(bool isEvent)
        {
            _MotivosVM.IsEvent = isEvent;
            csMotivo.Placeholder = LBLReasonPlaceholder;
            if (!_MotivosVM.IsEvent)
            {
                await _MotivosVM.GetMotivosAsync();
                csMotivo.Choices = _MotivosVM.Motivos.OrderBy(r => r.codmotivo).Select(r => $"{r.codmotivo} - {r.descricao}").ToArray();
            }
            else
            {
                await _MotivosVM.GetStopMotivosAsync();
                csMotivo.Choices = _MotivosVM.ParadaMotivos.OrderBy(r => r.codmotivo).Select(r => $"{r.codmotivo} - {r.descricao}").ToArray();
            }
        }

        private void csMotivo_ChoiceSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var Reasons = !_MotivosVM.IsEvent ? _MotivosVM.Motivos : _MotivosVM.ParadaMotivos;

            ReasonSelected?.Invoke(
                Reasons.First(r => r.codmotivo.ToString() == e.SelectedItem.ToString().Split('-')[0].Trim())
                , e);
        }

        public void ClearSelection()
        {
            csMotivo.SelectedChoice = null;
            csMotivo.Text = null;
        }
    }
}