﻿using Hino.Estoque.App.Models.Producao;
using Hino.Estoque.App.Services;
using Hino.Estoque.App.Services.ViewModels.Producao.Apontamento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Estoque.App.Views.Producao.Lancamento.Components.Events
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class StartEventPage : ContentPage
    {
        public event EventHandler ConfirmEvent;

        private readonly StartEventVM _StartEventVM;

        public StartEventPage (PDOrdemProdModel pOp)
        {
            InitializeComponent ();
            viewOrdemProdDescription.OrdemProd = pOp;
            viewOrderOperations.OrdemProd = pOp;
            viewConfirmation.OrdemProd = pOp;
            viewReasonsList.ReasonSelected += ViewReasonsList_ReasonSelected;
            this.BindingContext = _StartEventVM = new StartEventVM(pOp);
        }

        private async void App_IsConnectedChanged(object sender, EventArgs e)
        {
            var isWIFI = CurrentApp.IsWIFI();
            if (!isWIFI)
                await DisplayAlert(Hino.Estoque.App.Resources.FieldsNameResource.Connection, Hino.Estoque.App.Resources.ErrorMessagesResource.NoInternetConnection, "OK");

            viewConfirmation.StatusButtons(isWIFI);
        }

        private void ViewReasonsList_ReasonSelected(object sender, EventArgs e) =>
            _StartEventVM.SelectedReason = (PDMotivosModel)sender;

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            App.IsConnectedChanged += App_IsConnectedChanged;
            App_IsConnectedChanged(null, new EventArgs());
            await viewReasonsList.OnAppearing(true);
        }

        private void viewOrderOperations_OperationSelected(object sender, EventArgs e) =>
            _StartEventVM.SelectedOperation = Convert.ToInt32(((RadioButton)sender).Value);

        private void viewConfirmation_ConfirmEvent(object sender, EventArgs e) =>
            ConfirmEvent?.Invoke(_StartEventVM, e);

        private async void viewConfirmation_CancelEvent(object sender, EventArgs e) =>
            await Navigation.PopAsync();

        protected override bool OnBackButtonPressed()
        {
            Device.BeginInvokeOnMainThread(async () => {
                var result = await this.DisplayAlert(Hino.Estoque.App.Resources.ValidationMessagesResource.Confirm,
                    Hino.Estoque.App.Resources.ValidationMessagesResource.BackConfirmMessage,
                    Hino.Estoque.App.Resources.ValidationMessagesResource.ConfirmBack,
                    Hino.Estoque.App.Resources.ValidationMessagesResource.Stay);
                if (result) await this.Navigation.PopAsync();
            });

            return true;
        }
    }
}