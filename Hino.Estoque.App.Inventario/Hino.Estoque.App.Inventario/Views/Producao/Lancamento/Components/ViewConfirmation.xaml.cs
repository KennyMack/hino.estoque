﻿using Hino.Estoque.App.Models.Producao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Estoque.App.Views.Producao.Lancamento.Components
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ViewConfirmation : ContentView
    {
        public event EventHandler ConfirmEvent;
        public event EventHandler CancelEvent;

        public PDOrdemProdModel OrdemProd
        {
            get { return (PDOrdemProdModel)GetValue(OrdemProdProperty); }
            set { SetValue(OrdemProdProperty, value); }
        }

        public static readonly BindableProperty OrdemProdProperty =
            BindableProperty.Create(nameof(OrdemProd), typeof(PDOrdemProdModel), typeof(ViewOrdemProdDescription));

        public ViewConfirmation ()
		{
			InitializeComponent ();
            _content.BindingContext = this;
        }

        private void btnConfirmAppointmentFinishParada_Clicked(object sender, EventArgs e) =>
            ConfirmEvent?.Invoke(sender, e);

        private void btnCancelAppointmentFinishParada_Clicked(object sender, EventArgs e) =>
            CancelEvent?.Invoke(sender, e);

        public void StatusButtons(bool pBtn)
        {
            btnConfirmAppointmentFinishParada.IsEnabled = pBtn;
        }
    }
}