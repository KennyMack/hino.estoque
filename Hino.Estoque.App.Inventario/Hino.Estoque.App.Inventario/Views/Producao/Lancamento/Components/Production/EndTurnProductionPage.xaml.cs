﻿using Hino.Estoque.App.Models.Producao;
using Hino.Estoque.App.Services;
using Hino.Estoque.App.Services.ViewModels.Producao.Apontamento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Estoque.App.Views.Producao.Lancamento.Components.Production
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EndTurnProductionPage : ContentPage
    {
        public event EventHandler ConfirmEvent;
        private readonly EndTurnProductionVM _EndTurnProductionVM;

        public EndTurnProductionPage(PDOrdemProdModel pOp)
        {
            InitializeComponent();
            viewOrdemProdDescription.OrdemProd = pOp;
            viewOrderOperations.OrdemProd = pOp;
            viewConfirmation.OrdemProd = pOp;
            this.BindingContext = _EndTurnProductionVM = new EndTurnProductionVM(pOp);
        }

        private async void App_IsConnectedChanged(object sender, EventArgs e)
        {
            var isWIFI = CurrentApp.IsWIFI();
            if (!isWIFI)
                await DisplayAlert(Hino.Estoque.App.Resources.FieldsNameResource.Connection, Hino.Estoque.App.Resources.ErrorMessagesResource.NoInternetConnection, "OK");

            viewConfirmation.StatusButtons(isWIFI);
        }

        protected override void OnAppearing()
        {
            App.IsConnectedChanged += App_IsConnectedChanged;
            App_IsConnectedChanged(null, new EventArgs());
        }

        private void viewOrderOperations_OperationSelected(object sender, EventArgs e) =>
            _EndTurnProductionVM.SelectedOperation = Convert.ToInt32(((RadioButton)sender).Value);

        private void viewConfirmation_ConfirmEvent(object sender, EventArgs e) =>
            ConfirmEvent?.Invoke(_EndTurnProductionVM, e);

        private async void viewConfirmation_CancelEvent(object sender, EventArgs e) =>
            await Navigation.PopAsync();

        protected override bool OnBackButtonPressed()
        {
            Device.BeginInvokeOnMainThread(async () => {
                var result = await this.DisplayAlert(Hino.Estoque.App.Resources.ValidationMessagesResource.Confirm,
                    Hino.Estoque.App.Resources.ValidationMessagesResource.BackConfirmMessage,
                    Hino.Estoque.App.Resources.ValidationMessagesResource.ConfirmBack,
                    Hino.Estoque.App.Resources.ValidationMessagesResource.Stay);
                if (result) await this.Navigation.PopAsync();
            });

            return true;
        }
    }
}