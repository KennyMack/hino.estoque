﻿using Hino.Estoque.App.Models.Producao;
using Hino.Estoque.App.Services;
using Hino.Estoque.App.Services.ViewModels.Producao.Apontamento;
using System;
using System.Collections.Generic;
using System.Linq;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Estoque.App.Views.Producao.Lancamento.Components.Production
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EndProductionPage : ContentPage
    {
        public event EventHandler ConfirmEvent;

        private readonly EndProductionVM _EndProductionVM;

        public EndProductionPage(PDOrdemProdModel pOp)
        {
            InitializeComponent();
            viewOrdemProdDescription.OrdemProd = pOp;
            viewOrderOperations.OrdemProd = pOp;
            viewConfirmation.OrdemProd = pOp;
            viewReasonsList.ReasonSelected += ViewReasonsList_ReasonSelected;
            this.BindingContext = _EndProductionVM = new EndProductionVM(pOp);
            _EndProductionVM.ReasonListChanged -= EndProductionVM_ReasonListChanged;
            _EndProductionVM.ReasonListChanged += EndProductionVM_ReasonListChanged;
        }

        private void EndProductionVM_ReasonListChanged(object sender, EventArgs e) =>
            LoadRefugoList();

        private void LoadRefugoList()
        {
            try
            {
                ItensRefugo.Choices?.Clear();
                ItensRefugo.Choices = null;
                var items = new List<string>();
                foreach (var item in _EndProductionVM.RefugoMotivo.OrderBy(r => r.CodMotivo))
                    items.Add($"{item.CodMotivo} - {item.Descricao} ({item.Quantidade:n4})");
                if (!items.Any())
                    items.Add("Nenhum refugo informado");

                ItensRefugo.Choices = items;
                ItensRefugo.ForceLayout();
            }
            catch (Exception)
            {

            }
        }

        private async void App_IsConnectedChanged(object sender, EventArgs e)
        {
            var isWIFI = CurrentApp.IsWIFI();
            if (!isWIFI)
                await DisplayAlert(Hino.Estoque.App.Resources.FieldsNameResource.Connection, Hino.Estoque.App.Resources.ErrorMessagesResource.NoInternetConnection, "OK");

            viewConfirmation.StatusButtons(isWIFI);
        }

        protected async override void OnAppearing()
        {
            await viewReasonsList.OnAppearing(false);
            App.IsConnectedChanged += App_IsConnectedChanged;
            App_IsConnectedChanged(null, new EventArgs());

            txtNote.Focused += Txt_Focused;
            txtNote.Unfocused += Txt_Unfocused;
            txtDigQtdRefugo.Focused += Txt_Focused;
            txtDigQtdRefugo.Unfocused += Txt_Unfocused;
            txtQuantity.Focused += Txt_Focused;
            txtQuantity.Unfocused += Txt_Unfocused;
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            txtNote.Focused -= Txt_Focused;
            txtNote.Unfocused -= Txt_Unfocused;
            txtDigQtdRefugo.Focused -= Txt_Focused;
            txtDigQtdRefugo.Unfocused -= Txt_Unfocused;
            txtQuantity.Focused -= Txt_Focused;
            txtQuantity.Unfocused -= Txt_Unfocused;
        }

        private void Txt_Unfocused(object sender, FocusEventArgs e)
        {
            Content.LayoutTo(new Rectangle(0, 0, Content.Bounds.Width, Content.Bounds.Height));
        }

        private void Txt_Focused(object sender, FocusEventArgs e)
        {
            var valor = 0;
            switch (e.VisualElement.ClassId)
            {
                case "0":
                    valor = 160;
                    break;
                case "1":
                    valor = 220;
                    break;
                case "2":
                    valor = 280;
                    break;
            }

            Content.LayoutTo(new Rectangle(0, valor * -1, Content.Bounds.Width, Content.Bounds.Height));
        }

        private void ViewReasonsList_ReasonSelected(object sender, EventArgs e) =>
            _EndProductionVM.SelectedReason = (PDMotivosModel)sender;

        private void viewOrderOperations_OperationSelected(object sender, EventArgs e) =>
            _EndProductionVM.SelectedOperation = Convert.ToInt32(((RadioButton)sender).Value);

        private void viewConfirmation_ConfirmEvent(object sender, EventArgs e) =>
            ConfirmEvent?.Invoke(_EndProductionVM, e);

        private async void viewConfirmation_CancelEvent(object sender, EventArgs e) =>
            await Navigation.PopAsync();

        private void txtQuantity_Completed(object sender, EventArgs e)
        {

        }

        private void txtQuantity_Unfocused(object sender, FocusEventArgs e)
        {

        }

        private void txtDigQtdRefugo_Unfocused(object sender, FocusEventArgs e)
        {

        }

        private void txtDigQtdRefugo_Completed(object sender, EventArgs e)
        {

        }

        private void RefreshResultDescription()
        {
            var operation = _EndProductionVM._PDOrdemProdModel.pdordemprodrotinas.FirstOrDefault(r => r.operacao == _EndProductionVM.SelectedOperation);
            if (operation != null)
            {
                txtOPOperacao.Text = $"Operação: {operation.DetailOperacao}";
                txtQtdBoas.Text = $"Boas: {_EndProductionVM.QtdBoas:n4}";
                txtQtdRefugo.Text = $"Refugo: {_EndProductionVM.QtdRefugo:n4}";
            }
        }

        private void btnContinueFinish_Clicked(object sender, EventArgs e)
        {
            if (_EndProductionVM.ValidateContinue())
            {
                _EndProductionVM.ShowMainAppointment = false;
                _EndProductionVM.ShowFinalConfirm = _EndProductionVM.QtdRefugo <= 0;
                _EndProductionVM.ShowRefugoDetail = _EndProductionVM.QtdRefugo > 0;
                _EndProductionVM.RecalculaRefugoRestante();
                RefreshResultDescription();
            }
        }

        private void btnContinueRefugoFinish_Clicked(object sender, EventArgs e)
        {
            if (_EndProductionVM.ValidateContinue())
            {
                _EndProductionVM.ShowMainAppointment = false;
                _EndProductionVM.ShowRefugoDetail = false;
                _EndProductionVM.ShowFinalConfirm = true;
                _EndProductionVM.RecalculaRefugoRestante();
                RefreshResultDescription();
            }
        }

        private void txtQtdRefugoMot_Unfocused(object sender, FocusEventArgs e)
        {

        }

        private void txtQtdRefugoMot_Completed(object sender, EventArgs e) =>
            btnAdicionarRefugo_Clicked(sender, e);


        private void btnAdicionarRefugo_Clicked(object sender, EventArgs e)
        {
            decimal.TryParse(txtQtdRefugoMot.Text, out decimal qtdrefugo);
            _EndProductionVM.AddToReasonList(qtdrefugo);
            viewReasonsList.ClearSelection();
        }

        private async void btnRemoverSelecionados_Clicked(object sender, EventArgs e)
        {
            var resp = await DisplayAlert("Confirma", "Remover itens selecionados?", "REMOVER", "MANTER");

            if (resp)
            {
                var items = ItensRefugo.SelectedIndices;
                foreach (var item in items)
                {
                    try
                    {
                        _EndProductionVM.RefugoMotivo.RemoveAt(item);
                    }
                    catch (Exception)
                    {

                    }
                }
            }
        }

        protected override bool OnBackButtonPressed()
        {
            Device.BeginInvokeOnMainThread(async () => {
                var result = await this.DisplayAlert(Hino.Estoque.App.Resources.ValidationMessagesResource.Confirm,
                    Hino.Estoque.App.Resources.ValidationMessagesResource.BackConfirmMessage,
                    Hino.Estoque.App.Resources.ValidationMessagesResource.ConfirmBack,
                    Hino.Estoque.App.Resources.ValidationMessagesResource.Stay);
                if (result) await this.Navigation.PopAsync();
            });

            return true;
        }
    }
}