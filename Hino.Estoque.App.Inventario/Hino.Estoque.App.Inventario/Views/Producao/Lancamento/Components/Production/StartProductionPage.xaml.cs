﻿using Hino.Estoque.App.Models.Producao;
using Hino.Estoque.App.Services;
using Hino.Estoque.App.Services.ViewModels.Producao.Apontamento;
using Hino.Estoque.App.Views.Producao.Lancamento.Components.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XF.Material.Forms.UI;

namespace Hino.Estoque.App.Views.Producao.Lancamento.Components.Production
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class StartProductionPage : ContentPage
    {
        public event EventHandler ConfirmEvent;
        public event EventHandler ConfirmEndLastEvent;
        private readonly StartProductionVM _StartProductionVM;
        public StartProductionPage(PDOrdemProdModel pOp)
        {
            InitializeComponent();
            viewOrdemProdDescription.OrdemProd = pOp;
            viewOrderOperations.OrdemProd = pOp;
            viewConfirmation.OrdemProd = pOp;
            this.BindingContext = _StartProductionVM = new StartProductionVM(pOp);

            _StartProductionVM.MessageConfirmRaise += _StartProductionVM_MessageConfirmRaise;
        }

        private async void _StartProductionVM_MessageConfirmRaise(object sender, Utils.Interfaces.IMessageRaise e)
        {
            var resp = await DisplayAlert(e.Title, e.Text, e.Ok, e.Cancel);
            if (resp)
            {
                var _EndEventPage = new EndEventPage(viewOrdemProdDescription.OrdemProd)
                {
                    Title = "Terminar parada"
                };
                _EndEventPage.ConfirmEvent += EndEventPage_ConfirmEvent;
                await Navigation.PushAsync(_EndEventPage);
            }
            else
                ConfirmEvent?.Invoke(_StartProductionVM, new EventArgs());
        }

        private void EndEventPage_ConfirmEvent(object sender, EventArgs e)
        {
            ConfirmEndLastEvent?.Invoke((EndEventVM)sender, e);
        }

        private async void App_IsConnectedChanged(object sender, EventArgs e)
        {
            var isWIFI = CurrentApp.IsWIFI();
            if (!isWIFI)
                await DisplayAlert(Hino.Estoque.App.Resources.FieldsNameResource.Connection, Hino.Estoque.App.Resources.ErrorMessagesResource.NoInternetConnection, "OK");

            viewConfirmation.StatusButtons(isWIFI);
        }

        protected override void OnAppearing()
        {
            App.IsConnectedChanged += App_IsConnectedChanged;
            App_IsConnectedChanged(null, new EventArgs());
        }

        private void viewOrderOperations_OperationSelected(object sender, EventArgs e) =>
            _StartProductionVM.SelectedOperation = Convert.ToInt32(((RadioButton)sender).Value);

        private async void viewConfirmation_ConfirmEvent(object sender, EventArgs e)
        {
            var result = await _StartProductionVM.HasLastEventOpenned();

            if (result is null) return;

            if (result ?? false)
            {
                /*await Navigation.PushAsync(new EndEventPage(viewOrdemProdDescription.OrdemProd)
                {
                    Title = "Terminar parada"
                });*/
                return;
            }

            ConfirmEvent?.Invoke(_StartProductionVM, e);
        }
            

        private async void viewConfirmation_CancelEvent(object sender, EventArgs e) =>
            await Navigation.PopAsync();

        protected override bool OnBackButtonPressed()
        {
            Device.BeginInvokeOnMainThread(async () => {
                var result = await this.DisplayAlert(Hino.Estoque.App.Resources.ValidationMessagesResource.Confirm,
                    Hino.Estoque.App.Resources.ValidationMessagesResource.BackConfirmMessage,
                    Hino.Estoque.App.Resources.ValidationMessagesResource.ConfirmBack,
                    Hino.Estoque.App.Resources.ValidationMessagesResource.Stay);
                if (result) await this.Navigation.PopAsync();
            });

            return true;
        }


        private void txtMaquina_Completed(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(async () =>
                await _StartProductionVM.GetMaquinaAsync());
        }

        private void txtMaquina_Unfocused(object sender, FocusEventArgs e)
        {
            Device.BeginInvokeOnMainThread(async () =>
                await _StartProductionVM.GetMaquinaAsync());
        }

        private void txt_TextChanged(object sender, TextChangedEventArgs e)
        {
            (sender as MaterialTextField).Text = e.NewTextValue.ToUpperInvariant();
        }
    }
}