using Hino.Estoque.App.Models.Producao;
using Hino.Estoque.App.Services;
using Hino.Estoque.App.Services.ViewModels.Producao.Apontamento;
using Hino.Estoque.App.Views.Producao.Lancamento.Components.Events;
using Hino.Estoque.App.Views.Producao.Lancamento.Components.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Estoque.App.Views.Producao.Lancamento
{
    public enum OperationAppointment
    {
        StartProduction = 0,
        EndProduction = 1,
        EndTurnProduction = 2,
        StartEvent = 3,
        EndEvent = 4
    }

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AppointmentsMainTabPage : TabbedPage
    {
        private readonly AppointmentProdVM _AppointmentProdVM;
        public AppointmentsMainTabPage (OperationAppointment pOperation, PDOrdemProdModel pOp)
        {
            InitializeComponent();
            this.BindingContext = _AppointmentProdVM = new AppointmentProdVM(pOp);
            _AppointmentProdVM.GoToStart += AppointmentProdVM_GoToStart;
            _AppointmentProdVM.MessageRaise += AppointmentProdVM_MessageRaise;
            _AppointmentProdVM.StartNewOperation += Appointment_StartNewOperation;
            string TitlePage = "";
            switch (pOperation)
            {
                case OperationAppointment.StartProduction:
                    TitlePage = "Iniciar produção";
                    var _StartProductionPage = new StartProductionPage(pOp)
                    {
                        Title = TitlePage
                    };
                    _StartProductionPage.ConfirmEvent += StartProductionPage_ConfirmEvent;
                    _StartProductionPage.ConfirmEndLastEvent += EndEventPage_ConfirmEvent;
                    Children.Add(_StartProductionPage);
                    break;
                case OperationAppointment.EndProduction:
                    TitlePage = "Terminar produção";
                    var _EndProductionPage = new EndProductionPage(pOp)
                    {
                        Title = TitlePage
                    };
                    _EndProductionPage.ConfirmEvent += EndProductionPage_ConfirmEvent;
                    Children.Add(_EndProductionPage);
                    break;
                case OperationAppointment.EndTurnProduction:
                    TitlePage = "Terminar turno";
                    var _EndTurnProductionPage = new EndTurnProductionPage(pOp)
                    {
                        Title = TitlePage
                    };
                    _EndTurnProductionPage.ConfirmEvent += EndTurnProductionPage_ConfirmEvent; ;
                    Children.Add(_EndTurnProductionPage);
                    break;
                case OperationAppointment.StartEvent:
                    TitlePage = "Iniciar parada";
                    var _StartEventPage = new StartEventPage(pOp)
                    {
                        Title = TitlePage
                    };
                    _StartEventPage.ConfirmEvent += StartEventPage_ConfirmEvent;
                    Children.Add(_StartEventPage);
                    break;
                case OperationAppointment.EndEvent:
                    TitlePage = "Terminar parada";
                    var _EndEventPage = new EndEventPage(pOp)
                    {
                        Title = TitlePage
                    };
                    _EndEventPage.ConfirmEvent += EndEventPage_ConfirmEvent;
                    Children.Add(_EndEventPage);
                    break;
            }
            this.Title = TitlePage;
            this.CurrentPage = this.Children.First();
        }

        private async void Appointment_StartNewOperation(object sender, EventArgs e)
        {
            var obj = (ConfirmStartNewProduction)sender;
            if (await DisplayAlert(obj.Title, obj.Text, "INICIAR", "NÃO INICIAR"))
                await _AppointmentProdVM.StartProductionAsync(obj.startProductionVM);
            else
                await Navigation.PopAsync();
        }

        private void AppointmentProdVM_MessageRaise(object sender, Utils.Interfaces.IMessageRaise e) =>
            DisplayAlert(e.Title, e.Text, "OK");

        private async void AppointmentProdVM_GoToStart(object sender, EventArgs e) =>
            await Navigation.PopAsync();

        private async void StartProductionPage_ConfirmEvent(object sender, EventArgs e) =>
            await _AppointmentProdVM.StartProductionAsync((StartProductionVM)sender);

        private async void EndProductionPage_ConfirmEvent(object sender, EventArgs e) =>
            await _AppointmentProdVM.EndProductionAsync((EndProductionVM)sender);

        private async void EndTurnProductionPage_ConfirmEvent(object sender, EventArgs e) =>
            await _AppointmentProdVM.EndTurnProductionAsync((EndTurnProductionVM)sender);

        private async void StartEventPage_ConfirmEvent(object sender, EventArgs e) =>
            await _AppointmentProdVM.StartEventAsync((StartEventVM)sender);

        private async void EndEventPage_ConfirmEvent(object sender, EventArgs e) =>
            await _AppointmentProdVM.EndEventAsync((EndEventVM)sender);

        protected override bool OnBackButtonPressed()
        {
            Device.BeginInvokeOnMainThread(async () => {
                var result = await this.DisplayAlert(Hino.Estoque.App.Resources.ValidationMessagesResource.Confirm,
                    Hino.Estoque.App.Resources.ValidationMessagesResource.BackConfirmMessage,
                    Hino.Estoque.App.Resources.ValidationMessagesResource.ConfirmBack,
                    Hino.Estoque.App.Resources.ValidationMessagesResource.Stay);
                if (result) await this.Navigation.PopAsync();
            });

            return true;
        }
    }
}