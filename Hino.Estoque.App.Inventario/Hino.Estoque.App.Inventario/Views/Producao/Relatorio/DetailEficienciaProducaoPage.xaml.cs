﻿using Hino.Estoque.App.Models.Producao;
using Hino.Estoque.App.Services.ViewModels.Producao;
using Hino.Estoque.App.Templates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Estoque.App.Views.Producao.Relatorio
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DetailEficienciaProducaoPage : BaseContentPage
    {
        private readonly DetailRelatorioEficienciaProdVM _DetailRelatorioEficienciaProdVM;
        public DetailEficienciaProducaoPage(PDMaquinaGrupoModel pMaquinaGrupo)
        {
            InitializeComponent();
            BindingContext = _DetailRelatorioEficienciaProdVM = new DetailRelatorioEficienciaProdVM(pMaquinaGrupo);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (_DetailRelatorioEficienciaProdVM.Items.Count == 0)
                _DetailRelatorioEficienciaProdVM.RefreshDataCommand.Execute(null);
        }

        private void lvDados_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            // if (!(e.SelectedItem is MNSolicManutModel solic))
            //     return;
            // 
            // await Navigation.PushAsync(new DetailSolicManutencaoPage(solic));

            // Manually deselect item.
            lvDados.SelectedItem = null;
        }
    }
}