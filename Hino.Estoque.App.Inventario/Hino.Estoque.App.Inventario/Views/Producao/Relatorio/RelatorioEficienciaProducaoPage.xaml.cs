﻿using Hino.Estoque.App.Models.Producao;
using Hino.Estoque.App.Services.ViewModels.Producao;
using Hino.Estoque.App.Templates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Estoque.App.Views.Producao.Relatorio
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RelatorioEficienciaProducaoPage : BaseContentPage
    {
        private readonly RelatorioEficienciaProducaoVM _RelatorioEficienciaProducaoVM;

        public RelatorioEficienciaProducaoPage()
        {
            InitializeComponent();
            BindingContext = _RelatorioEficienciaProducaoVM = new RelatorioEficienciaProducaoVM();
        }

        private async void lvDados_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        { 
            if (!(e.SelectedItem is PDMaquinaGrupoModel maqGrupo))
                return;
            
            await Navigation.PushAsync(new DetailEficienciaProducaoPage(maqGrupo));

            lvDados.SelectedItem = null;
        }
    }
}