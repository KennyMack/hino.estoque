﻿using Hino.Estoque.App.Models.Engenharia;
using Hino.Estoque.App.Services;
using Hino.Estoque.App.Services.ViewModels.Producao.OEE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XF.Material.Forms.UI;

namespace Hino.Estoque.App.Views.Producao.OEE
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SearchOEEPage : ContentPage
    {
        private readonly SearchOEEVM _SearchOEEVM;
        public SearchOEEPage(PageOrigem pageOrigem)
        {
            InitializeComponent();
            BindingContext = _SearchOEEVM = new SearchOEEVM(pageOrigem);
            _SearchOEEVM.GoToResultOEE += SearchOEEVM_GoToResultOEE;
            _SearchOEEVM.GoToResultOEEMensal += SearchOEEVM_GoToResultOEEMensal;

            boxProcesso.IsVisible = pageOrigem == PageOrigem.OEE;
            boxMaquina.IsVisible = pageOrigem == PageOrigem.OEE;
            txtTurno.IsVisible = pageOrigem == PageOrigem.OEE;

            boxAgrupProcesso.IsVisible = pageOrigem == PageOrigem.OEEMENSAL;
        }

        private async void App_IsConnectedChanged(object sender, EventArgs e)
        {
            bool isWIFI = CurrentApp.IsWIFI();
            if (!isWIFI)
                await DisplayAlert(Hino.Estoque.App.Resources.FieldsNameResource.Connection, Hino.Estoque.App.Resources.ErrorMessagesResource.NoInternetConnection, "OK");

            btnGerarOEE.IsEnabled = isWIFI;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            App.IsConnectedChanged += App_IsConnectedChanged;
            App_IsConnectedChanged(null, new EventArgs());
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            App.IsConnectedChanged -= App_IsConnectedChanged;
        }

        protected override bool OnBackButtonPressed()
        {
            Device.BeginInvokeOnMainThread(async () => {
                var result = await this.DisplayAlert(Hino.Estoque.App.Resources.ValidationMessagesResource.Confirm,
                    Hino.Estoque.App.Resources.ValidationMessagesResource.BackConfirmMessage,
                    Hino.Estoque.App.Resources.ValidationMessagesResource.ConfirmBack,
                    Hino.Estoque.App.Resources.ValidationMessagesResource.Stay);
                if (result) await this.Navigation.PopAsync();
            });

            return true;
        }

        private void txtMaquina_Completed(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(async () =>
                await _SearchOEEVM.GetMaquinaAsync());
        }

        private void txtMaquina_Focused(object sender, FocusEventArgs e)
        {

        }

        private void txtMaquina_Unfocused(object sender, FocusEventArgs e)
        {
            Device.BeginInvokeOnMainThread(async () =>
                await _SearchOEEVM.GetMaquinaAsync());
        }

        private void txtProcesso_Completed(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(async () =>
                await _SearchOEEVM.GetProcessoAsync());
        }

        private void txtProcesso_Focused(object sender, FocusEventArgs e)
        {

        }

        private void txtProcesso_Unfocused(object sender, FocusEventArgs e)
        {
            Device.BeginInvokeOnMainThread(async () =>
                await _SearchOEEVM.GetProcessoAsync());
        }

        private void txt_TextChanged(object sender, TextChangedEventArgs e)
        {
            (sender as MaterialTextField).Text = e.NewTextValue.ToUpperInvariant();
        }

        private void cbGerarDetalhes_CheckedChanged(object sender, CheckedChangedEventArgs e)
        {
            _SearchOEEVM.GERARDETALHES = e.Value;
        }

        async void SearchOEEVM_GoToResultOEE(object sender, EventArgs e) =>
            await Navigation.PushAsync(new ListOEEPage(_SearchOEEVM.DTINICIO, _SearchOEEVM.DTTERMINO, _SearchOEEVM.MAQUINA));

        async void SearchOEEVM_GoToResultOEEMensal(object sender, EventArgs e) =>
            await Navigation.PushAsync(new ListOEEMensalPage(_SearchOEEVM.DTINICIO, _SearchOEEVM.DTTERMINO));

        private void txtAgrupProcesso_Unfocused(object sender, FocusEventArgs e)
        {
            Device.BeginInvokeOnMainThread(async () =>
                await _SearchOEEVM.GetAgrupProcessoAsync());
        }

        private void txtAgrupProcesso_Focused(object sender, FocusEventArgs e)
        {

        }

        private void txtAgrupProcesso_Completed(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(async () =>
                await _SearchOEEVM.GetAgrupProcessoAsync());

        }
    }
}