﻿using Hino.Estoque.App.Models.Producao.OEE;
using Hino.Estoque.App.Services.ViewModels.Producao.OEE;
using Hino.Estoque.App.Templates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Estoque.App.Views.Producao.OEE
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ListOEEPage : BaseContentPage
    {
        readonly ListOEEVM _ListOEEVM;
        public ListOEEPage(DateTime pInicio, DateTime pTermino, string pCodMaquina)
        {
            InitializeComponent();
            BindingContext = _ListOEEVM = new ListOEEVM(pInicio, pTermino, pCodMaquina);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (_ListOEEVM.Items.Count() == 0)
                _ListOEEVM.RefreshDataCommand.Execute(null);
        }

        private async void lvOEEResultado_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (!(e.SelectedItem is PDOEEResultadoModel resultado))
                return;
            if (resultado.TemJustificativa)
            {
                var message = _ListOEEVM.GetJustificativa(resultado.Date);

                await DisplayAlert("Justificativas", message, "OK");

            }

            // await Navigation.PushAsync(new DetailSolicManutencaoPage(solic));

            // Manually deselect item.
            lvOEEResultado.SelectedItem = null;
        }
    }
}