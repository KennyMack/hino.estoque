﻿using Hino.Estoque.App.Services.ViewModels.Producao.OEE;
using Hino.Estoque.App.Templates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Estoque.App.Views.Producao.OEE
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ListOEEMensalPage : BaseContentPage
    {
        readonly ListOEEMensalVM _ListOEEMensalVM;
        public ListOEEMensalPage(DateTime pInicio, DateTime pTermino)
        {
            InitializeComponent();
            BindingContext = _ListOEEMensalVM = new ListOEEMensalVM(pInicio, pTermino);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (_ListOEEMensalVM.Items.Count() == 0)
                _ListOEEMensalVM.RefreshDataCommand.Execute(null);
        }
    }
}