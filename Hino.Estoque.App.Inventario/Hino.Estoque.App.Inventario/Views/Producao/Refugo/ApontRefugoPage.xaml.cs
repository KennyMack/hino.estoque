﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Estoque.App.Views.Producao.Refugo
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ApontRefugoPage : ContentPage
    {
        public ApontRefugoPage()
        {
            InitializeComponent();
        }
    }
}