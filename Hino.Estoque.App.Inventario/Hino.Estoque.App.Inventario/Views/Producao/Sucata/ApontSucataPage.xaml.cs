﻿using Hino.Estoque.App.Models.Producao;
using Hino.Estoque.App.Services;
using Hino.Estoque.App.Services.ViewModels.Producao.Sucata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XF.Material.Forms.UI;

namespace Hino.Estoque.App.Views.Producao.Sucata
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ApontSucataPage : ContentPage
    {
        private string LastFocused;
        private readonly SucataVM _SucataVM;

        public ApontSucataPage(PDOrdemProdModel pOp)
        {
            InitializeComponent();
            viewOrdemProdDescription.OrdemProd = pOp;
            viewConfirmation.OrdemProd = pOp;
            viewReasonsList.ReasonSelected += ViewReasonsList_ReasonSelected;
            this.BindingContext = _SucataVM = new SucataVM(pOp);
            _SucataVM.MessageRaise += SucataVM_MessageRaise;
            _SucataVM.GoToStart += AppointmentProdVM_GoToStart;
            _SucataVM.evtTextFocused += _ProdutosVM_evtTextFocused;
            LastFocused = "PRODUCT";
        }

        private async void AppointmentProdVM_GoToStart(object sender, EventArgs e) =>
            await Navigation.PopAsync();

        private void SucataVM_MessageRaise(object sender, Utils.Interfaces.IMessageRaise e) =>
            DisplayAlert(e.Title, e.Text, "OK");

        private void _ProdutosVM_evtTextFocused(object sender, EventArgs e)
        {
            if (sender is string)
            {
                switch (sender.ToString())
                {
                    case "PRODUCT":
                        txtProduct.Focus();
                        break;
                    case "STOCKORIGIN":
                        txtStockOrigin.Focus();
                        break;
                    case "QUANTITY":
                        txtQuantity.Focus();
                        break;
                }
            }
        }

        private void ViewReasonsList_ReasonSelected(object sender, EventArgs e) =>
            _SucataVM.SelectedReason = (PDMotivosModel)sender;


        private async void App_IsConnectedChanged(object sender, EventArgs e)
        {
            var isWIFI = CurrentApp.IsWIFI();
            if (!isWIFI)
                await DisplayAlert(Hino.Estoque.App.Resources.FieldsNameResource.Connection, Hino.Estoque.App.Resources.ErrorMessagesResource.NoInternetConnection, "OK");

            viewConfirmation.StatusButtons(isWIFI);
        }

        protected async override void OnAppearing()
        {
            await _SucataVM.LoadComponentsAsync();
            await viewReasonsList.OnAppearing(false);
            base.OnAppearing();
            App.IsConnectedChanged += App_IsConnectedChanged;
            App_IsConnectedChanged(null, new EventArgs());
        }

        private async void viewConfirmation_ConfirmEvent(object sender, EventArgs e) =>
            await _SucataVM.SalvarSucataAsync();

        private async void viewConfirmation_CancelEvent(object sender, EventArgs e) =>
            await Navigation.PopAsync();

        protected override bool OnBackButtonPressed()
        {
            Device.BeginInvokeOnMainThread(async () => {
                var result = await this.DisplayAlert(Hino.Estoque.App.Resources.ValidationMessagesResource.Confirm,
                    Hino.Estoque.App.Resources.ValidationMessagesResource.BackConfirmMessage,
                    Hino.Estoque.App.Resources.ValidationMessagesResource.ConfirmBack,
                    Hino.Estoque.App.Resources.ValidationMessagesResource.Stay);
                if (result) await this.Navigation.PopAsync();
            });

            return true;
        }

        private async void txtProduct_Completed(object sender, EventArgs e) =>
            await _SucataVM.GetProductByBarCodeAsync();

        private async void txtProduct_Unfocused(object sender, FocusEventArgs e) =>
            await _SucataVM.GetProductByBarCodeAsync();

        private void txtStockOrigin_Completed(object sender, EventArgs e)
        {

        }

        private void txtProduct_TextChanged(object sender, TextChangedEventArgs e) =>
            (sender as MaterialTextField).Text = e.NewTextValue.ToUpperInvariant();

        private void viewBarcodeVCTpl_BarcodeReadResult(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (!(sender is ZXing.Result barcodeResult))
                    return;

                if (LastFocused == "PRODUCT")
                {
                    _SucataVM.PRODUCT = barcodeResult.Text.Trim();
                    await _SucataVM.GetProductByBarCodeAsync();
                }
                if (LastFocused == "STOCKORIGIN")
                {
                    _SucataVM.STOCKORIGIN = barcodeResult.Text.Trim();
                    txtStockOrigin.Focus();
                }
            });
        }

        private void txtProduct_Focused(object sender, FocusEventArgs e)
        {
            LastFocused = "PRODUCT";
        }

        private void txtStockOrigin_Focused(object sender, FocusEventArgs e)
        {
            LastFocused = "STOCKORIGIN";
        }

        private void txtQuantity_Completed(object sender, EventArgs e)
        {

        }
    }
}