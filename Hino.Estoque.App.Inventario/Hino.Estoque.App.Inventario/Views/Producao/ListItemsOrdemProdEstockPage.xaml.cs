﻿using Hino.Estoque.App.Templates;
using Hino.Estoque.App.Models.Producao;
using Hino.Estoque.App.Services;
using Hino.Estoque.App.Services.ViewModels.Producao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Estoque.App.Views.Producao
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ListItemsOrdemProdEstockPage : BaseContentPage
    {
        private readonly OrdemProdVM _OrdemProdVM;
        private bool _ItemShowed = false;
        public ListItemsOrdemProdEstockPage(PDOrdemProdModel pPDOrdemProdModel)
        {
            InitializeComponent();
            BindingContext = _OrdemProdVM = new OrdemProdVM(pPDOrdemProdModel);
            _OrdemProdVM.ShowResultTransf = pPDOrdemProdModel.ShowResultOP;
            _OrdemProdVM.InfiniteListView(lvOPComponents);
            _OrdemProdVM.MessageConfirmRaise += _OrdemProdVM_MessageConfirmRaise;
            _OrdemProdVM.SearchEvent(txtProductsSearchBar);
            _OrdemProdVM.GoToTransfResult += _OrdemProdVM_GoToTransfResult;
        }

        private void _OrdemProdVM_GoToTransfResult(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                var page = new ListItemsOPTransfResultPage(
                    _OrdemProdVM._PDOrdemProdModel,
                    _OrdemProdVM._ResultTransf);
                page.evtPageClosed += Page_evtPageClosed;
                await Navigation.PushModalAsync(page);
            });
        }

        private void Page_evtPageClosed(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(async () =>
                await Navigation.PopToRootAsync());
        }

        private async void _OrdemProdVM_MessageConfirmRaise(object sender, Utils.Interfaces.IMessageRaise e)
        {
            if (await DisplayAlert(e.Title, e.Text, "Sim", "Não"))
            {
                if (e.Title == Hino.Estoque.App.Resources.ValidationMessagesResource.ConfirmTyping)
                    await _OrdemProdVM.SyncValueItemsAsync();
            }
        }

        private async void App_IsConnectedChanged(object sender, EventArgs e)
        {
            var isWIFI = CurrentApp.IsWIFI();
            if (!isWIFI)
                await DisplayAlert(Hino.Estoque.App.Resources.FieldsNameResource.Connection, Hino.Estoque.App.Resources.ErrorMessagesResource.NoInternetConnection, "OK");

            SearchItem.IsEnabled = isWIFI;
            ConfirmItem.IsEnabled = isWIFI;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            App.IsConnectedChanged += App_IsConnectedChanged;
            App_IsConnectedChanged(null, new EventArgs());

            if (_OrdemProdVM.Items.Count() == 0)
                _OrdemProdVM.RefreshDataCommand.Execute(null);
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            App.IsConnectedChanged -= App_IsConnectedChanged;
        }

        private void SearchBarCode_Clicked(object sender, EventArgs e)
        {
            _OrdemProdVM.ShowBarCode = !_OrdemProdVM.ShowBarCode;
            viewBarcodeVCTpl.HeightRequest = _OrdemProdVM.ShowBarCode ? 200 : 0;
        }

        private void SearchItem_Clicked(object sender, EventArgs e)
        {
            _OrdemProdVM.ShowSearch = !_OrdemProdVM.ShowSearch;

            if (_OrdemProdVM.ShowSearch)
                txtProductsSearchBar.Focus();
        }

        private void ConfirmItem_Clicked(object sender, EventArgs e) =>
            _OrdemProdVM.ConfirmSave();

        private async void ItensVCTpl_Quantity_Completed(object sender, EventArgs e) =>
            await _OrdemProdVM.SaveValueAsync((OPComponentsResult)sender);

        private async void LvInvenItemsPendentes_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (lvOPComponents.SelectedItem is PDOrdemProdCompModel)
            {
                var page = new ItemOrdemProdEstockPage((PDOrdemProdCompModel)e.SelectedItem);
                page.Disappearing += Page_Disappearing;
                await Navigation.PushAsync(page);
                _ItemShowed = true;
                /*
                if (((PDOrdemProdCompModel)e.SelectedItem).qtdSeparada <= 0)
                {
                    var qtdSep = ((PDOrdemProdCompModel)e.SelectedItem).quantidade;
                    if (((PDOrdemProdCompModel)e.SelectedItem).FSProduto_separacaomaximo > 0 &&
                        ((PDOrdemProdCompModel)e.SelectedItem).FSProduto_separacaomaximo < qtdSep)
                        qtdSep = ((PDOrdemProdCompModel)e.SelectedItem).FSProduto_separacaomaximo;

                    await _OrdemProdVM.SaveValueAsync(new OPComponentsResult
                    {
                        IdMobile = ((PDOrdemProdCompModel)e.SelectedItem).IdMobile,
                        qtdseparada = qtdSep
                    });
                    ((PDOrdemProdCompModel)e.SelectedItem).qtdSeparada = qtdSep;
                }
                */
            }
            lvOPComponents.SelectedItem = null;
        }

        private void Page_Disappearing(object sender, EventArgs e)
        {
            if (_ItemShowed)
            {
                _ItemShowed = false;
                _OrdemProdVM.RefreshDataCommand.Execute(null);
            }
        }

        private void ViewBarcodeVCTpl_BarcodeReadResult(object sender, EventArgs e)
        {
            SearchBarCode_Clicked(sender, e);

            if (!(sender is ZXing.Result barcodeResult))
                return;
            _OrdemProdVM.ShowSearch = true;
            _OrdemProdVM.SEARCHDATA = barcodeResult.Text;
        }

        protected override bool OnBackButtonPressed()
        {
            Device.BeginInvokeOnMainThread(async () => {
                var result = await this.DisplayAlert(Hino.Estoque.App.Resources.ValidationMessagesResource.Confirm,
                    Hino.Estoque.App.Resources.ValidationMessagesResource.BackConfirmMessage,
                    Hino.Estoque.App.Resources.ValidationMessagesResource.ConfirmBack,
                    Hino.Estoque.App.Resources.ValidationMessagesResource.Stay);
                if (result) await this.Navigation.PopAsync();
            });

            return true;
        }
    }
}