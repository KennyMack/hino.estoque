﻿using Hino.Estoque.App.Services.ViewModels.Auth;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XF.Material.Forms.UI;
using XF.Material.Forms.UI.Dialogs;

namespace Hino.Estoque.App.Views.Auth
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        private readonly LoginVM _LoginVM;
        public LoginPage()
        {
            InitializeComponent();

            BindingContext = _LoginVM = new LoginVM();

            _LoginVM.GoToMainPage += _LoginVM_GoToMainPage;
        }

        protected async override void OnAppearing() =>
            await _LoginVM.LoadEstabs();

        private async void BtnSelectEstab_Clicked(object sender, EventArgs e) =>
            await _LoginVM.ShowEstabs();

        private void _LoginVM_GoToMainPage(object sender, EventArgs e) => 
            App.Current.MainPage = new NavigationPage(new MainPage());

        private async void BtnServidor_Clicked(object sender, EventArgs e) =>
            await Navigation.PushAsync(new ServerPage());

        private void BtnEnter_Clicked(object sender, EventArgs e) =>
            _LoginVM.CmdLoginClick.Execute(null);

        private void UserLogin_Completed(object sender, EventArgs e) =>
            _LoginVM.CmdLoginClick.Execute(null);
    }
}