﻿using Hino.Estoque.App.Services.ViewModels.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XF.Material.Forms.UI.Dialogs;

namespace Hino.Estoque.App.Views.Auth
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ServerPage : ContentPage
	{
        private readonly ServerVM _ServerVM;
        public ServerPage ()
        {
            InitializeComponent ();
            this.BindingContext = _ServerVM = new ServerVM();

            _ServerVM.SaveComplete += _ServerVM_SaveComplete;
            _ServerVM.BackPage += _ServerVM_BackPage;
        }

        private async void _ServerVM_BackPage(object sender, EventArgs e) =>
            await Navigation.PopToRootAsync();

        private async void _ServerVM_SaveComplete(object sender, EventArgs e) =>
            await Navigation.PopToRootAsync();

        private void BtnBack_Clicked(object sender, EventArgs e) =>
            _ServerVM.CmdBackClick.Execute(null);
        
        private void BtnSave_Clicked(object sender, EventArgs e) =>
            _ServerVM.CmdSaveClick.Execute(null);

        private void ServerAddress_Completed(object sender, EventArgs e) =>
            _ServerVM.CmdSaveClick.Execute(null);
    }
}