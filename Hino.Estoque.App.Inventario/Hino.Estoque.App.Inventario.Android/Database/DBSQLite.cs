﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Hino.Estoque.App.Services.DB;

namespace Hino.Estoque.App.Droid.Database
{
    public class DBSQLite : IDBSQLite
    {
        public string DataBasePath => Path.Combine(
                    System.Environment.GetFolderPath(
                    System.Environment.SpecialFolder.Personal),
                    "HINOERP.db3"
                );
    }
}