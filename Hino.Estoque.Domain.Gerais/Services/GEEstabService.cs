using Hino.Estoque.Domain.Gerais.Interfaces.Repositories;
using Hino.Estoque.Domain.Gerais.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Gerais;
using Hino.Estoque.Domain.Base.Services;
using System.Threading.Tasks;

namespace Hino.Estoque.Domain.Gerais.Services
{
    public class GEEstabService : BaseService<GEEstab>, IGEEstabService
    {
        private readonly IGEEstabRepository _IGEEstabRepository;

        public GEEstabService(IGEEstabRepository pIGEEstabRepository) : 
             base(pIGEEstabRepository)
        {
            _IGEEstabRepository = pIGEEstabRepository;
        }

        public async Task<GEEstab> GetEstabById(int pCodEstab) =>
            await _IGEEstabRepository.GetEstabById(pCodEstab);
    }
}
