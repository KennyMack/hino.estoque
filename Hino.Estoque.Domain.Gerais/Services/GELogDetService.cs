using Hino.Estoque.Domain.Gerais.Interfaces.Repositories;
using Hino.Estoque.Domain.Gerais.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Gerais;
using Hino.Estoque.Domain.Base.Services;

namespace Hino.Estoque.Domain.Gerais.Services
{
    public class GELogDetService : BaseService<GELogDet>, IGELogDetService
    {
        private readonly IGELogDetRepository _IGELogDetRepository;

        public GELogDetService(IGELogDetRepository pIGELogDetRepository) : 
             base(pIGELogDetRepository)
        {
            _IGELogDetRepository = pIGELogDetRepository;
        }
    }
}
