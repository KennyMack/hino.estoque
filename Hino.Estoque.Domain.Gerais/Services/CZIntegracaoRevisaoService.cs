﻿using Hino.Estoque.Domain.Base.Services;
using Hino.Estoque.Domain.Gerais.Interfaces.Repositories;
using Hino.Estoque.Domain.Gerais.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Gerais;

namespace Hino.Estoque.Domain.Gerais.Services
{
    public class CZIntegracaoRevisaoService : BaseService<CZIntegracaoRevisao>, ICZIntegracaoRevisaoService
    {
        private readonly ICZIntegracaoRevisaoRepository _ICZIntegracaoRevisaoRepository;

        public CZIntegracaoRevisaoService(ICZIntegracaoRevisaoRepository pCZIntegracaoRevisaoRepository) :
             base(pCZIntegracaoRevisaoRepository)
        {
            _ICZIntegracaoRevisaoRepository = pCZIntegracaoRevisaoRepository;
        }
    }
}
