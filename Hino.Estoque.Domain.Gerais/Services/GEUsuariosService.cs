using Hino.Estoque.Domain.Gerais.Interfaces.Repositories;
using Hino.Estoque.Domain.Gerais.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Gerais;
using Hino.Estoque.Domain.Base.Services;
using System.Threading.Tasks;
using System.Linq;

namespace Hino.Estoque.Domain.Gerais.Services
{
    public class GEUsuariosService : BaseService<GEUsuarios>, IGEUsuariosService
    {
        private readonly IGEUsuariosRepository _IGEUsuariosRepository;

        public GEUsuariosService(IGEUsuariosRepository pIGEUsuariosRepository) : 
             base(pIGEUsuariosRepository)
        {
            _IGEUsuariosRepository = pIGEUsuariosRepository;
        }

        public async Task<GEUsuarios> GetUserByUserName(string pUserName) =>
            (await _IGEUsuariosRepository.QueryAsync(r => r.codusuario == pUserName)).FirstOrDefault();
    }
}
