using Hino.Estoque.Domain.Gerais.Interfaces.Repositories;
using Hino.Estoque.Domain.Gerais.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Gerais;
using Hino.Estoque.Domain.Base.Services;
using System.Threading.Tasks;
using System.Linq;

namespace Hino.Estoque.Domain.Gerais.Services
{
    public class GEFuncionariosService : BaseService<GEFuncionarios>, IGEFuncionariosService
    {
        private readonly IGEFuncionariosRepository _IGEFuncionariosRepository;

        public GEFuncionariosService(IGEFuncionariosRepository pIGEFuncionariosRepository) : 
             base(pIGEFuncionariosRepository)
        {
            _IGEFuncionariosRepository = pIGEFuncionariosRepository;
        }

        public async Task<GEFuncionarios> GetByIdentifier(short pCodEstab, string pIdentifier)
        {
            return (await _IGEFuncionariosRepository.QueryAsync(r => r.codestab == pCodEstab && r.identificacao == pIdentifier)).FirstOrDefault();
        }
    }
}
