using Hino.Estoque.Domain.Gerais.Interfaces.Repositories;
using Hino.Estoque.Domain.Gerais.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Gerais;
using Hino.Estoque.Domain.Base.Services;

namespace Hino.Estoque.Domain.Gerais.Services
{
    public class GELogService : BaseService<GELog>, IGELogService
    {
        private readonly IGELogRepository _IGELogRepository;

        public GELogService(IGELogRepository pIGELogRepository) : 
             base(pIGELogRepository)
        {
            _IGELogRepository = pIGELogRepository;
        }
    }
}
