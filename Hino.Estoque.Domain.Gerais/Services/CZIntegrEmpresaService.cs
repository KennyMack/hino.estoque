﻿using Hino.Estoque.Domain.Base.Interfaces.Repositories;
using Hino.Estoque.Domain.Base.Interfaces.Services;
using Hino.Estoque.Domain.Base.Services;
using Hino.Estoque.Domain.Gerais.Interfaces.Repositories;
using Hino.Estoque.Domain.Gerais.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Gerais;

namespace Hino.Estoque.Domain.Gerais.Services
{
    public class CZIntegrEmpresaService : BaseService<CZIntegrEmpresa>, ICZIntegrEmpresaService
    {
        private readonly ICZIntegrEmpresaRepository _ICZIntegrEmpresaRepository;

        public CZIntegrEmpresaService(ICZIntegrEmpresaRepository pCZIntegrEmpresaRepository) :
             base(pCZIntegrEmpresaRepository)
        {
            _ICZIntegrEmpresaRepository = pCZIntegrEmpresaRepository;
        }
    }
}
