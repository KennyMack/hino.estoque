using Hino.Estoque.Infra.Cross.Entities.Gerais;
using Hino.Estoque.Domain.Base.Interfaces.Services;
using System.Threading.Tasks;

namespace Hino.Estoque.Domain.Gerais.Interfaces.Services
{
    public interface IGEEstabService : IBaseService<GEEstab>
    {
        Task<GEEstab> GetEstabById(int pCodEstab);
    }
}
