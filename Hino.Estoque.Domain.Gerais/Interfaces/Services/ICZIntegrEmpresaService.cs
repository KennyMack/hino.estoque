﻿using Hino.Estoque.Domain.Base.Interfaces.Repositories;
using Hino.Estoque.Domain.Base.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Gerais;

namespace Hino.Estoque.Domain.Gerais.Interfaces.Services
{
    public interface ICZIntegrEmpresaService : IBaseService<CZIntegrEmpresa>
    {
    }
}
