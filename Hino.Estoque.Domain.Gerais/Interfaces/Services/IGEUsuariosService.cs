using Hino.Estoque.Infra.Cross.Entities.Gerais;
using Hino.Estoque.Domain.Base.Interfaces.Services;
using System.Threading.Tasks;

namespace Hino.Estoque.Domain.Gerais.Interfaces.Services
{
    public interface IGEUsuariosService : IBaseService<GEUsuarios>
    {
        Task<GEUsuarios> GetUserByUserName(string pUserName);
    }
}
