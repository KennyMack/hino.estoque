﻿using Hino.Estoque.Domain.Base.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Gerais;

namespace Hino.Estoque.Domain.Gerais.Interfaces.Services
{
    public interface ICZIntegracaoRevisaoService : IBaseService<CZIntegracaoRevisao>
    {
    }
}
