using Hino.Estoque.Infra.Cross.Entities.Gerais;
using Hino.Estoque.Domain.Base.Interfaces.Services;

namespace Hino.Estoque.Domain.Gerais.Interfaces.Services
{
    public interface IGELogDetService : IBaseService<GELogDet>
    {
    }
}
