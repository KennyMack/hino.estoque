using Hino.Estoque.Infra.Cross.Entities.Gerais;
using Hino.Estoque.Domain.Base.Interfaces.Repositories;

namespace Hino.Estoque.Domain.Gerais.Interfaces.Repositories
{
    public interface IGEFuncionariosRepository : IBaseRepository<GEFuncionarios>
    {
    }
}
