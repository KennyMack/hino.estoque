using Hino.Estoque.Infra.Cross.Entities.Gerais;
using Hino.Estoque.Domain.Base.Interfaces.Repositories;
using System.Threading.Tasks;

namespace Hino.Estoque.Domain.Gerais.Interfaces.Repositories
{
    public interface IGEEstabRepository : IBaseRepository<GEEstab>
    {
        Task<GEEstab> GetEstabById(int pCodEstab);
    }
}
