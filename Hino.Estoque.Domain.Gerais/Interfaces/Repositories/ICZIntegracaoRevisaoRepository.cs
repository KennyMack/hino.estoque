﻿using Hino.Estoque.Domain.Base.Interfaces.Repositories;
using Hino.Estoque.Infra.Cross.Entities.Gerais;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Domain.Gerais.Interfaces.Repositories
{
    public interface ICZIntegracaoRevisaoRepository : IBaseRepository<CZIntegracaoRevisao>
    {
    }
}
