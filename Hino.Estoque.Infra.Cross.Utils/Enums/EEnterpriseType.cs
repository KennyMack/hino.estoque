﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Infra.Cross.Utils.Enums
{
    public enum EEnterpriseType
    {
        PessoaFisica = 0,
        PessoaJuridica = 1
    }
}
