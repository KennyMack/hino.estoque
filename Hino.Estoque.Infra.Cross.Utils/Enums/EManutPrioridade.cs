﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Infra.Cross.Utils.Enums
{
    public enum EManutPrioridade
    {
        BAIXA = 0,
        MEDIA = 1,
        ALTA = 2
    }
}
