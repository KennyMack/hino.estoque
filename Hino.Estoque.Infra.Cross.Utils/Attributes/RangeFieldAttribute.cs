﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Hino.Estoque.Infra.Cross.Utils.Attributes
{
    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class IntegerRangeFieldAttribute: RangeAttribute
    {
        public IntegerRangeFieldAttribute(int minimum, int maximum):
            base(minimum, maximum)
        {

            ErrorMessageResourceName = "RangeOnlyInteger";
            ErrorMessageResourceType = typeof(Resources.MessagesResource);
        }
    }

    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class DecimalRangeFieldAttribute : RangeAttribute
    {
        public DecimalRangeFieldAttribute(double minimum, double maximum) :
            base(minimum, maximum)
        {

            ErrorMessageResourceName = "RangeOnlyDecimal";
            ErrorMessageResourceType = typeof(Resources.MessagesResource);
        }
    }
}
