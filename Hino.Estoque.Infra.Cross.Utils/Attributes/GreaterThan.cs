﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Hino.Estoque.Infra.Cross.Utils.Attributes
{
    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class LongGreaterThan : RangeAttribute
    {
        private long _minimum;
        public LongGreaterThan(long minimum) :
            base(minimum + 1, long.MaxValue)
        {
            _minimum = minimum;
            ErrorMessageResourceName = "GreaterThan";
            ErrorMessageResourceType = typeof(Resources.MessagesResource);
        }

        public override string FormatErrorMessage(string name) =>
            string.Format(base.ErrorMessageString, name, _minimum);
    }

    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class IntegerGreaterThan : RangeAttribute
    {
        private int _minimum;
        public IntegerGreaterThan(int minimum) :
            base(minimum + 1, int.MaxValue)
        {
            _minimum = minimum;
            ErrorMessageResourceName = "GreaterThan";
            ErrorMessageResourceType = typeof(Resources.MessagesResource);
        }

        public override string FormatErrorMessage(string name) =>
            string.Format(base.ErrorMessageString, name, _minimum);
    }

    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class SbyteGreaterThan : RangeAttribute
    {
        private sbyte _minimum;
        public SbyteGreaterThan(sbyte minimum) :
            base(minimum + 1, sbyte.MaxValue)
        {
            _minimum = minimum;
            ErrorMessageResourceName = "GreaterThan";
            ErrorMessageResourceType = typeof(Resources.MessagesResource);
        }

        public override string FormatErrorMessage(string name) =>
            string.Format(base.ErrorMessageString, name, _minimum);
    }

    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class ShortGreaterThan : RangeAttribute
    {
        private short _minimum;
        public ShortGreaterThan(short minimum) :
            base(minimum + 1, short.MaxValue)
        {
            _minimum = minimum;
            ErrorMessageResourceName = "GreaterThan";
            ErrorMessageResourceType = typeof(Resources.MessagesResource);
        }

        public override string FormatErrorMessage(string name) =>
            string.Format(base.ErrorMessageString, name, _minimum);
    }

    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class DecimalGreaterThan : RangeAttribute
    {
        private short _minimum;
        public DecimalGreaterThan(short minimum) :
            base(minimum + 1, short.MaxValue)
        {
            _minimum = minimum;
            ErrorMessageResourceName = "GreaterThan";
            ErrorMessageResourceType = typeof(Resources.MessagesResource);
        }

        public override string FormatErrorMessage(string name) =>
            string.Format(base.ErrorMessageString, name, _minimum);
    }
}
