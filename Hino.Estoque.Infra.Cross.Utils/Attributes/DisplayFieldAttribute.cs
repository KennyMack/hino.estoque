﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Hino.Estoque.Infra.Cross.Utils.Attributes
{
    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class DisplayFieldAttribute : DisplayNameAttribute
    {
        public DisplayFieldAttribute([CallerMemberName]string propertyName = "")
        {
            var a = new System.Resources.ResourceManager(typeof(Resources.Fields))
            {
                IgnoreCase = true
            };
            var result = a.GetString(propertyName);

            DisplayNameValue = result.IsEmpty() ? propertyName : result;
        }
    }
}
