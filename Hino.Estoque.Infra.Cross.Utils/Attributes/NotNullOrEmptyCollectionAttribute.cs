﻿using System;
using System.Collections;
using System.ComponentModel.DataAnnotations;

namespace Hino.Estoque.Infra.Cross.Utils.Attributes
{
    [AttributeUsage(AttributeTargets.Property, Inherited = true, AllowMultiple = false)]
    public class NotNullOrEmptyCollectionAttribute : ValidationAttribute
    {
        public NotNullOrEmptyCollectionAttribute()
        {
            ErrorMessageResourceName = "MustBeAtLeastOneItem";
            ErrorMessageResourceType = typeof(Resources.MessagesResource);
        }

        public override bool IsValid(object value)
        {
            if (value is ICollection collection)
            {
                return collection.Count != 0;
            }

            return value is IEnumerable enumerable && enumerable.GetEnumerator().MoveNext();
        }
    }
}
