﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Infra.Cross.Utils
{
    public static class StringEx
    {
        public static string SubStr(this string valor, int start, int count)
        {
            if (valor.Length > count && count > 0)
                return valor.Substring(start, count);

            return valor;
        }

        public static String FmtTelefone(this string valor)
        {
            if (string.IsNullOrEmpty(valor))
                return null;

            return valor.Replace("-", "")
                .Replace("(", "")
                .Replace(")", "")
                .Replace(" ", "")
                .Replace("-", "")
                .SubStr(0, 14);
        }

        public static string SubStrTitle(this string pString)
        {
            if (pString.Length > 20)
                return pString.Substring(0, 20) + "...";

            return pString;
        }

        /// <summary>
        /// Verifica se a string esta vazia
        /// </summary>
        /// <param name="value">Valor</param>
        /// <returns>System.bool.</returns>
        public static bool IsEmpty(this string value)
        {
            return string.IsNullOrEmpty(value) || string.IsNullOrWhiteSpace(value);
        }

        public static string LimpaEspacosIniFim(this string valor)
        {
            var retorno = valor.Trim();
            if (!string.IsNullOrEmpty(retorno) ||
                !string.IsNullOrWhiteSpace(retorno))
            {
                if (retorno[0].ToString() == " ")
                    retorno = retorno.LimpaEspacosIniFim();

                if (retorno[retorno.Length - 1].ToString() == " ")
                    retorno = retorno.LimpaEspacosIniFim();
            }
            return retorno;
        }

        public static string LimpaMensagemOracle(this string pMensagem)
        {
            var TemErro = pMensagem.SubStr(0, 4) == "ORA-";

            if (TemErro)
            {
                var mensagemPartes = pMensagem.Split(
                    new[] { "\r\n", "\r", "\n" },
                    StringSplitOptions.None);

                var temErroCustom = mensagemPartes.Any(r => Convert.ToInt32(r.SubStr(4, 5)).IsBetweenII(20000, 20999));

                if (temErroCustom)
                {
                    StringBuilder resultado = new StringBuilder();
                    foreach (var message in mensagemPartes)
                    {
                        if (!message.IsEmpty() && message.StartsWith("ORA") && Convert.ToInt32(message.SubStr(4, 5)).IsBetweenII(20000, 20999))
                            resultado.AppendLine(message.Substring(10).LimpaEspacosIniFim());
                        else if (!message.IsEmpty() && !message.StartsWith("ORA"))
                            resultado.AppendLine(message);
                    }
                    return resultado.ToString();
                }
            }

            return pMensagem;
        }
    }
}
