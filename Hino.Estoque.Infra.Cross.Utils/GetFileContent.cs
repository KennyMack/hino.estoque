﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Infra.Cross.Utils
{
    public static class GetFileContent
    {
        public static string[] GetFile(string pFile)
        {
            try
            {
                var path = System.Web.Hosting.HostingEnvironment.MapPath("~/bin");

                return File.ReadAllLines(string.Concat(path, "\\", pFile));
            }
            catch (Exception)
            {
                
            }

            return null;
        }


        public static string BasePath()
        {
            return System.Web.Hosting.HostingEnvironment.MapPath("~/bin");
        }
    }
}
