﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hino.Estoque.App.Utils.Extensions
{
    /// <summary>
    /// Classe para verificação In
    /// </summary>
    public static class InReferencia
    {
        #region Verifica se os valores estão dentro da faixa
        /// <summary>
        /// Verifica se os valores estão dentro da faixa
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="valor">Valor para verificar</param>
        /// <param name="referencia">Valores</param>
        /// <returns>bool</returns>
        public static bool In<T>(this T valor, params T[] referencia) where T : IComparable
        {

            return referencia.Any(r => r != null && r.Equals(valor));
        }
        #endregion

    }
}
