﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Estoque.App.Utils.Extensions
{
    public static class DateEx
    {
        #region Numero da semana
        public static int MonthWeekNumber(this DateTime date)
        {
            var numSemana = 1;
            var numDias = DateTime.DaysInMonth(date.Year, date.Month);
            var diaSemana = (int)date.DayOfWeek;

            for (int i = 0; i < numDias; i++)
            {
                if (i == date.Day)
                {
                    break;
                }

                diaSemana++;

                if (diaSemana > 7)
                {
                    numSemana++;
                    diaSemana = 1;
                }
            }

            return numSemana;
        }
        #endregion

        #region Primeiro dia do mes
        /// <summary>
        /// Retorna o primeiro dia do mês
        /// </summary>
        /// <param name="date">data</param>
        /// <returns>DateTime</returns>
        public static DateTime FirstDay(this DateTime date)
        {
            return new DateTime(date.Year, date.Month, 1, date.Hour, date.Minute, date.Second, date.Millisecond);
        }
        #endregion

        #region Ultimo dia do mes
        /// <summary>
        /// Retorna o ultimo dia do mês
        /// </summary>
        /// <param name="date">data</param>
        /// <returns>DateTime</returns>
        public static DateTime LastDay(this DateTime date)
        {
            return new DateTime(date.Year, date.Month,
                        DateTime.DaysInMonth(date.Year, date.Month),
                       date.Hour, date.Minute, date.Second, date.Millisecond);
        }
        #endregion

        #region Ultimo hora do dia
        /// <summary>
        /// Retorna a ultima hora dia
        /// </summary>
        /// <param name="date">data</param>
        /// <returns>DateTime</returns>
        public static DateTime LastHour(this DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day,
                       23, 59, 59, 59);
        }
        #endregion

        #region Primeira hora do dia
        /// <summary>
        /// Retorna a primeira hora dia
        /// </summary>
        /// <param name="date">data</param>
        /// <returns>DateTime</returns>
        public static DateTime FirstHour(this DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day,
                        0, 0, 0, 0);
        }
        #endregion

        #region Traduz dia da semana
        public static string TraduzDiaSemana(this DayOfWeek pDiaSemana, bool abreviado = false)
        {
            switch (pDiaSemana)
            {
                case DayOfWeek.Sunday:
                    return abreviado ? "Dom" : "Domingo";
                case DayOfWeek.Monday:
                    return abreviado ? "Seg" : "Segunda-feira";
                case DayOfWeek.Tuesday:
                    return abreviado ? "Ter" : "Terça-feira";
                case DayOfWeek.Wednesday:
                    return abreviado ? "Qua" : "Quarta-feira";
                case DayOfWeek.Thursday:
                    return abreviado ? "Qui" : "Quinta-feira";
                case DayOfWeek.Friday:
                    return abreviado ? "Sex" : "Sexta-feira";
                case DayOfWeek.Saturday:
                    return abreviado ? "Sab" : "Sábado";
                default:
                    return "";
            }
        }
        #endregion

        #region fim de semana
        public static bool FimDeSemana(this DateTime pDate)
        {
            switch (pDate.DayOfWeek)
            {
                case DayOfWeek.Saturday:
                case DayOfWeek.Sunday:
                    return true;
                case DayOfWeek.Monday:
                case DayOfWeek.Tuesday:
                case DayOfWeek.Wednesday:
                case DayOfWeek.Thursday:
                case DayOfWeek.Friday:
                    return false;
                default:
                    return false;
            }
        }
        #endregion

        /// <summary>
        /// Pega o próximo dia útil da data informada.
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static DateTime GetProximoDiaUtil(DateTime dateTime)
        {
            try
            {
                while (true)
                {
                    if (dateTime.DayOfWeek == DayOfWeek.Saturday)
                        dateTime = dateTime.AddDays(2);
                    else if (dateTime.DayOfWeek == DayOfWeek.Sunday)
                        dateTime = dateTime.AddDays(1);

                    return dateTime;
                }
            }
            catch (Exception E)
            {
                throw new Exception("Buscar próximo dia útil", E);
            }
        }
    }
}
