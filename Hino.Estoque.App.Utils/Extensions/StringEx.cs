﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Estoque.App.Utils.Extensions
{
    public static class StringEx
    {
        public static string SubStrTitle(this string pString)
        {
            if (pString.Length > 20)
                return pString.Substring(0, 20) + "...";

            return pString;
        }
        public static string SubStrTitle(this string pString, int size)
        {
            if (pString.Length > size)
                return pString.Substring(0, size) + "...";

            return pString;
        }
        /// <summary>
        /// Verifica se a string esta vazia
        /// </summary>
        /// <param name="value">Valor</param>
        /// <returns>System.bool.</returns>
        public static bool IsEmpty(this string value)
        {
            return string.IsNullOrEmpty(value) || string.IsNullOrWhiteSpace(value);
        }
    }
}
