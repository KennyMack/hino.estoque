﻿using Hino.Estoque.App.Utils.Interfaces;
using System;

namespace Hino.Estoque.App.Utils.Messages
{
    public class MessageRaiseEvent: EventArgs, IMessageRaise
    {
        public MessageRaiseEvent(string title, string text)
        {
            Text = text;
            Title = title;
        }

        public MessageRaiseEvent(string title, string text, object tag)
        {
            Text = text;
            Title = title;
            Tag = tag;
        }

        public MessageRaiseEvent(string title, string text, string ok, string cancel)
        {
            Text = text;
            Title = title;
            Ok = ok;
            Cancel = cancel;
        }

        public MessageRaiseEvent(string title, string text, string ok, string cancel, object tag)
        {
            Text = text;
            Title = title;
            Ok = ok;
            Cancel = cancel;
            Tag = tag;
        }

        public string Text { get; private set; }
        public string Title { get; private set; }
        public object Tag { get; private set; }
        public string Ok { get; private set; }
        public string Cancel { get; private set; }
    }
}
