﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Estoque.App.Utils.Interfaces
{
    public interface ILoadComplete
    {
        object Sender { get; }
    }
}
