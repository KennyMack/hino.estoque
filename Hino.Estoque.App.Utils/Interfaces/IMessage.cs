﻿using System;

namespace Hino.Estoque.App.Utils.Interfaces
{
    public interface IMessage
    {
        void ShowShortMessage(String text, bool Short = true);
    }
}
