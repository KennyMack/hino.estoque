﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Estoque.App.Utils.Interfaces
{
    public interface IMessageRaise
    {
        string Title { get; }
        string Text { get; }
        string Ok { get; }
        string Cancel { get; }
        object Tag { get; }
    }
}
