﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Estoque.App.Utils.Attributes
{
    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class RequiredFieldAttribute: RequiredAttribute
    {
        public RequiredFieldAttribute()
        {
            AllowEmptyStrings = false;
            ErrorMessageResourceName = "RequiredDefault";
            ErrorMessageResourceType = typeof(Resources.ValidationMessagesResource); 
        }
    }
}
