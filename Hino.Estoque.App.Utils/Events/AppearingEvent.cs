﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Estoque.App.Utils.Events
{
    public class AppearingEvent : EventArgs
    {
        public string Title { get; private set; }
        public int Quantidade { get; private set; }

        public AppearingEvent(string pTitle, int pQuantidade)
        {
            this.Title = pTitle;
            this.Quantidade = pQuantidade;
        }
    }
}
