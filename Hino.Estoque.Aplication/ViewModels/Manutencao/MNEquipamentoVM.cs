﻿using Hino.Estoque.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.ViewModels.Manutencao
{
    public class MNEquipamentoVM : BaseVM
    {
        [DisplayField]
        public short codestab { get; set; }
        [DisplayField]
        public string codequipamento { get; set; }
        [DisplayField]
        public string descricao { get; set; }
        [DisplayField]
        public sbyte status { get; set; }
    }
}
