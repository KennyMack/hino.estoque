﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.ViewModels.Manutencao
{
    public class MNSolicForUserVM
    {
        public short codestab { get; set; }
        public int codfuncionario { get; set; }
        public string codusuario { get; set; }
    }
}
