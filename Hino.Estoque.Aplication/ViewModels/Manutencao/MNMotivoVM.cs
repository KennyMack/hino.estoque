﻿using Hino.Estoque.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.ViewModels.Manutencao
{
    public class MNMotivoVM: BaseVM
    {
        [DisplayField]
        public short codmotivo { get; set; }
        [DisplayField]
        public string descricao { get; set; }
    }

    public class MNSearchMotivoVM
    {
        [DisplayField]
        [RequiredField]
        public short codmotivo { get; set; }
    }
}
