﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.ViewModels.Manutencao
{
    public class MNSearchManutVM
    {
        public MNMotivoVM[] Motivos { get; set; }
        public MNEquipamentoVM[] Equipamentos { get; set; }
        public MNTipoManutVM[] TipoManut { get; set; }
    }
}
