﻿using Hino.Estoque.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.ViewModels.Manutencao
{
    public class MNSolicManutVM : BaseVM
    {
        [DisplayField]
        [RequiredField]
        [IntegerGreaterThan(0)]
        public short codestab { get; set; }
        [DisplayField]
        public long codsolic { get; set; }
        [DisplayField]
        [RequiredField]
        public string codequipamento { get; set; }
        [DisplayField]
        [RequiredField]
        public DateTime datasolic { get; set; }
        [DisplayField]
        public string status { get; set; }
        [DisplayField]
        [RequiredField]
        public short codmotivo { get; set; }
        [DisplayField]
        public string observacao { get; set; }
        [DisplayField]
        [RequiredField]
        public string solicitante { get; set; }
        [DisplayField]
        public string aprovador { get; set; }
        [DisplayField]
        [RequiredField]
        public string prioridade { get; set; }
        [NotMapped]
        public string descprioridade
        {
            get
            {
                switch (prioridade)
                {
                    case "A":
                        return "Alta";
                    case "M":
                        return "Média";
                    default:
                        return "Baixa";

                }
            }
        }
        [DisplayField]
        [RequiredField]
        public string codtipomanut { get; set; }

        public MNEquipamentoVM Equipamento { get; set; }
        public MNTipoManutVM TipoManut { get; set; }
        public MNMotivoVM Motivo { get; set; }

    }
}
