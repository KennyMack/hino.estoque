﻿using Hino.Estoque.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.ViewModels.Manutencao
{
    public class MNStatusSolicManutVM
    {
        [DisplayField]
        [RequiredField]
        [IntegerGreaterThan(0)]
        public long codsolic { get; set; }
        [DisplayField]
        [RequiredField]
        [IntegerGreaterThan(0)]
        public short codestab { get; set; }
        [DisplayField]
        [RequiredField]
        [IntegerGreaterThan(0)]
        public int codfuncionario { get; set; }
        [DisplayField]
        [RequiredField]
        [IntegerRangeField(1, 2)]
        public int status { get; set; }
    }
}
