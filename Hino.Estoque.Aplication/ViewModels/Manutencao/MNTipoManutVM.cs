﻿using Hino.Estoque.Infra.Cross.Utils.Attributes;
using Hino.Estoque.Infra.Cross.Utils.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.ViewModels.Manutencao
{
    public class MNTipoManutVM : BaseVM
    {
        public string codtipomanut { get; set; }
        public string descricao { get; set; }
        public EManutPrioridade prioridade { get; set; }
        public bool enviaemail { get; set; }
        public string email { get; set; }
        public string codusuario { get; set; }
        public short? codmotivo { get; set; }
    }

    public class MNSearchTipoManutVM 
    {
        [DisplayField]
        [RequiredField]
        public string codtipomanut { get; set; }
    }
}
