﻿using Hino.Estoque.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.ViewModels.Manutencao
{
    public class MNCreateSolicManutVM : BaseVM
    {
        [DisplayField]
        [RequiredField]
        [IntegerGreaterThan(0)]
        public short codestab { get; set; }
        [DisplayField]
        [RequiredField]
        public string codequipamento { get; set; }
        [DisplayField]
        [RequiredField]
        public DateTime datasolic { get; set; }
        [DisplayField]
        [RequiredField]
        [IntegerGreaterThan(0)]
        public short codmotivo { get; set; }
        [DisplayField]
        public string observacao { get; set; }
        [DisplayField]
        [RequiredField]
        [IntegerGreaterThan(0)]
        public int codfuncionario { get; set; }
        [DisplayField]
        [RequiredField]
        public string prioridade { get; set; }
        [DisplayField]
        [RequiredField]
        public string codtipomanut { get; set; }
    }
}
