﻿using Hino.Estoque.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.ViewModels.Manutencao
{
    public class MNSearchEquipamentoVM
    {
        [DisplayField]
        [RequiredField]
        public int codestab { get; set; }
        [DisplayField]
        [RequiredField]
        public string equipamento { get; set; }
    }
}
