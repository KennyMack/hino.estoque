using Hino.Estoque.Aplication.ViewModels;
using Hino.Estoque.Aplication.ViewModels.Fiscal;
using Hino.Estoque.Infra.Cross.Entities.Fiscal;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Estoque.Aplication.ViewModels.Estoque
{
    public class ESInventLoteVM : BaseVM
    {
        public short codestab { get; set; }
        public decimal codinv { get; set; }
        public decimal contagem { get; set; }
        public string codproduto { get; set; }
        public string codestoque { get; set; }
        public decimal lote { get; set; }
        public decimal saldolote { get; set; }
        public decimal? saldoinvent { get; set; }

        public virtual ESInventDetVM esinventdet { get; set; }
        public virtual ESLoteVM eslote { get; set; }
        public virtual FSLocalEstoqueVM fslocalestoque { get; set; }
    }
}
