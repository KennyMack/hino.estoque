﻿using Hino.Estoque.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.ViewModels.Estoque
{
    public class ESBuscaSaldoEstoqueVM
    {
        [DisplayField]
        [RequiredField]
        public int CodEstab { get; set; }
        [DisplayField]
        [RequiredField]
        public string CodProduto { get; set; }
        [DisplayField]
        [RequiredField]
        public string CodEstoque { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal SaldoAtual { get; set; }
    }
}
