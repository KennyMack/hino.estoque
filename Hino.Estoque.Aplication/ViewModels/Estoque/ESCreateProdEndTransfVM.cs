﻿using Hino.Estoque.Infra.Cross.Utils.Attributes;
using System.Collections.Generic;

namespace Hino.Estoque.Aplication.ViewModels.Estoque
{
    public class ESCreateProdEndTransfVM
    {
        public ESCreateProdEndTransfVM()
        {
            Items = new HashSet<ESProdEndTransfItemVM>();
        }

        [DisplayField]
        [RequiredField]
        public string CodEstoqueOri { get; set; }
        [DisplayField]
        [RequiredField]
        public int CodEstab { get; set; }
        [DisplayField]
        [RequiredField]
        public string CodProduto { get; set; }
        [DisplayField]
        [RequiredField]
        public string CodUsuario { get; set; }
        public IEnumerable<ESProdEndTransfItemVM> Items { get; set; }
    }

    public class ESProdEndTransfItemVM
    {
        [DisplayField]
        [RequiredField]
        public string CodEstoqueDest { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal Quantidade { get; set; }
    }
}
