using Hino.Estoque.Aplication.ViewModels;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Estoque.Aplication.ViewModels.Estoque
{
    public class ESTransfLoteVM : BaseVM
    {
        public int codtransflote { get; set; }
        public short codestab { get; set; }
        public decimal codtransf { get; set; }
        public decimal seqtransf { get; set; }
        public decimal lote { get; set; }
        public string codestoque { get; set; }
        public decimal quantidade { get; set; }

        public virtual ESLoteSaldoVM ESLoteSaldo { get; set; }
        public virtual ESTransfDetVM ESTransfDet { get; set; }
    }
}
