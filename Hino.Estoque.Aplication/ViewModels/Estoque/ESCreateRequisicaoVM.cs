﻿using Hino.Estoque.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.ViewModels.Estoque
{
    public class ESCreateRequisicaoVM
    {
        [DisplayField]
        [RequiredField]
        public string codproduto { get; set; }
        [DisplayField]
        [RequiredField]
        [IntegerGreaterThan(0)]
        public short codestab { get; set; }
        [DisplayField]
        [RequiredField]
        public string codestoque { get; set; }
        [DisplayField]
        [RequiredField]
        [DecimalGreaterThan(0)]
        public decimal quantidade { get; set; }
        [DisplayField]
        [RequiredField]
        [IntegerGreaterThan(0)]
        public int codfuncionario { get; set; }
        [DisplayField]
        public string observacao { get; set; }
    }

    public class ESUpdateRequisicaoVM
    {
        [DisplayField]
        [RequiredField]
        [IntegerGreaterThan(0)]
        public int codrequisicao { get; set; }
        [DisplayField]
        [RequiredField]
        [IntegerGreaterThan(0)]
        public short codestab { get; set; }
        [DisplayField]
        [RequiredField]
        [IntegerGreaterThan(0)]
        public int codfuncionario { get; set; }
    }
}
