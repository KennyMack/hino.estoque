﻿using Hino.Estoque.Aplication.ViewModels.Fiscal;
using Hino.Estoque.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.ViewModels.Estoque
{
    public class ESRequisicaoVM : BaseVM
    {
        [DisplayField]
        [RequiredField]
        public int codrequisicao { get; set; }
        [DisplayField]
        [RequiredField]
        public short codestab { get; set; }
        [DisplayField]
        [RequiredField]
        public string codproduto { get; set; }
        [DisplayField]
        public long? lote { get; set; }
        [DisplayField]
        [RequiredField]
        public string codestoque { get; set; }
        [DisplayField]
        [RequiredField]
        public string codccusto { get; set; }
        [DisplayField]
        [RequiredField]
        public DateTime data { get; set; }
        [DisplayField]
        [RequiredField]
        public int codfuncionario { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal quantidade { get; set; }
        [DisplayField]
        public decimal precomedio { get; set; }
        [DisplayField]
        public decimal valortotal { get; set; }
        [DisplayField]
        public int? codos { get; set; }
        [DisplayField]
        public decimal saldoatual { get; set; }
        public string codusuaprov { get; set; }
        public short status { get; set; }
        public string observacao { get; set; }
        
        public FSProdutoVM FSProduto { get; set; }
        public FSProdutoparamEstabVM FSProdutoparamEstab { get; set; }
    }
}
