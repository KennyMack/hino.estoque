using Hino.Estoque.Aplication.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Estoque.Aplication.ViewModels.Estoque
{
    public class ESInventTercVM : BaseVM
    {
        public short codestab { get; set; }
        public decimal codinv { get; set; }
        public decimal contagem { get; set; }
        public string codproduto { get; set; }
        public string codestoque { get; set; }
        public decimal indiceitem { get; set; }
        public byte sequencia { get; set; }
        public decimal saldofiscal { get; set; }
        public decimal? saldoinvent { get; set; }

        public virtual ESInventDetVM ESInventDet { get; set; }
    }
}
