using Hino.Estoque.Aplication.ViewModels;
using Hino.Estoque.Aplication.ViewModels.Fiscal;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Estoque.Aplication.ViewModels.Estoque
{
    public class ESLoteVM : BaseVM
    {
        public ESLoteVM()
        {
            this.ESInventLote = new HashSet<ESInventLoteVM>();
            this.ESLoteSaldo = new HashSet<ESLoteSaldoVM>();
        }
        
        public short codestab { get; set; }
        public decimal lote { get; set; }
        public bool origem { get; set; }
        public string status { get; set; }
        public decimal? indiceitemnf { get; set; }
        public byte? seqitemnf { get; set; }
        public decimal? codlancamento { get; set; }
        public decimal? loteorigem { get; set; }
        public DateTime datalote { get; set; }
        public string aprovador { get; set; }
        public string codproduto { get; set; }
        
        public virtual ICollection<ESInventLoteVM> ESInventLote { get; set; }
        public virtual ICollection<ESLoteSaldoVM> ESLoteSaldo { get; set; }
        public virtual FSProdutoparamEstabVM FSProdutoparamEstab { get; set; }
    }
}
