using Hino.Estoque.Aplication.ViewModels;
using Hino.Estoque.Aplication.ViewModels.Fiscal;
using Hino.Estoque.Infra.Cross.Entities.Fiscal;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Estoque.Aplication.ViewModels.Estoque
{
    public class ESTransfDetVM : BaseVM
    {
        public ESTransfDetVM()
        {
            this.ESTransfLote = new HashSet<ESTransfLoteVM>();
        }
        public short codestab { get; set; }
        public decimal codtransf { get; set; }
        public decimal seqtransf { get; set; }
        public string codproduto { get; set; }
        public DateTime data { get; set; }
        public string locdestino { get; set; }
        public decimal necessidade { get; set; }
        public string locorigem { get; set; }
        public decimal qtdetransf { get; set; }
        
        public virtual ICollection<ESTransfLoteVM> ESTransfLote { get; set; }
        public virtual ESTransferenciaVM ESTransferencia { get; set; }
        public virtual FSProdutoVM FSProduto { get; set; }
    }
}
