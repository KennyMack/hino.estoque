using Hino.Estoque.Aplication.ViewModels;
using Hino.Estoque.Aplication.ViewModels.Fiscal;
using Hino.Estoque.Infra.Cross.Entities.Fiscal;
using Hino.Estoque.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Estoque.Aplication.ViewModels.Estoque
{
    public class ESInventDetItemVM
    {
        [DisplayField]
        [RequiredField]
        public short codestab { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal codinv { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal contagem { get; set; }
        [DisplayField]
        [RequiredField]
        public string codproduto { get; set; }
        [DisplayField]
        [RequiredField]
        public string codestoque { get; set; }
    }

    public class ESUpdateInventDetVM : BaseVM
    {
        [DisplayField]
        [RequiredField]
        public short codestab { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal codinv { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal contagem { get; set; }
        [DisplayField]
        [RequiredField]
        public string codproduto { get; set; }
        [DisplayField]
        [RequiredField]
        public string codestoque { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal saldoinvent { get; set; }
        [DisplayField]
        [RequiredField]
        public string codusuario { get; set; }
        [DisplayField]
        [RequiredField]
        public DateTime datacontagem { get; set; }
        public string codlote { get; set; }
    }

    public class ESInventDetVM : BaseVM
    {
        public ESInventDetVM()
        {
            this.ESInventLote = new HashSet<ESInventLoteVM>();
            this.ESInventTerc = new HashSet<ESInventTercVM>();
        }
        
        public short codestab { get; set; }
        public decimal codinv { get; set; }
        public decimal contagem { get; set; }
        public string codproduto { get; set; }
        public string codestoque { get; set; }
        public decimal saldofisico { get; set; }
        public decimal? saldoinvent { get; set; }
        public DateTime? datacontagem { get; set; }
        public string codusuario { get; set; }

        public virtual ESInventarioVM ESInventario { get; set; }
        public virtual ICollection<ESInventLoteVM> ESInventLote { get; set; }
        public virtual ICollection<ESInventTercVM> ESInventTerc { get; set; }
        public virtual FSLocalEstoqueVM FSLocalEstoque { get; set; }
        public virtual FSProdutoVM FSProduto { get; set; }
    }
}
