﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.ViewModels.Estoque
{
    public class ESResultRequisicaoVM
    {
        public string CodProduto { get; set; }
        public string CodEstoque { get; set; }
        public bool Sucesso { get; set; }
        public decimal Quantidade { get; set; }
        public List<ESResultRequisicaoLoteVM> Lotes = new List<ESResultRequisicaoLoteVM>();
    }

    public class ESResultRequisicaoLoteVM
    {
        public long Lote { get; set; }
        public string CodEstoque { get; set; }
        public decimal Quantidade { get; set; }
    }
}
