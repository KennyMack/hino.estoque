using Hino.Estoque.Aplication.ViewModels;
using Hino.Estoque.Aplication.ViewModels.Fiscal;
using Hino.Estoque.Aplication.ViewModels.Gerais;
using Hino.Estoque.Infra.Cross.Entities.Fiscal;
using Hino.Estoque.Infra.Cross.Entities.Gerais;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Estoque.Aplication.ViewModels.Estoque
{
    public class ESInventarioVM : BaseVM
    {
        public ESInventarioVM()
        {
            this.ESInventDet = new HashSet<ESInventDetVM>();
            this.ESKardex = new HashSet<ESKardexVM>();
        }
        
        public short codestab { get; set; }
        public decimal codinv { get; set; }
        public string codproduto { get; set; }
        public string codestoque { get; set; }
        public string status { get; set; }
        public DateTime datainv { get; set; }
        [NotMapped]
        public decimal contagem { get; set; }
        public decimal qtdecontag { get; set; }
        public DateTime? dataefet { get; set; }
        public decimal? contagefet { get; set; }
        
        public virtual ICollection<ESInventDetVM> ESInventDet { get; set; }
        public virtual ICollection<ESKardexVM> ESKardex { get; set; }
        public virtual FSLocalEstoqueVM FSLocalEstoque { get; set; }
        public virtual FSProdutoVM FSProduto { get; set; }
        public virtual GEEstabVM GEEstab { get; set; }
    }
}
