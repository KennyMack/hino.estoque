using Hino.Estoque.Aplication.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Estoque.Aplication.ViewModels.Estoque
{
    public class ESKardexVM : BaseVM
    {
        public short codestab { get; set; }
        public decimal codkardex { get; set; }
        public decimal? indiceinftem { get; set; }
        public byte? seqnfitem { get; set; }
        public decimal? codlancamento { get; set; }
        public decimal? codconsumo { get; set; }
        public decimal? codrefugo { get; set; }
        public DateTime data { get; set; }
        public string codtipomov { get; set; }
        public string codproduto { get; set; }
        public string codunidade { get; set; }
        public string codestoque { get; set; }
        public decimal quantidade { get; set; }
        public decimal valortotal { get; set; }
        public decimal precomedio { get; set; }
        public decimal? lote { get; set; }
        public string observacao { get; set; }
        public int? codajuste { get; set; }
        public int? codrequisicao { get; set; }
        public int? codromaneiosep { get; set; }
        public decimal? codsucata { get; set; }
        public string codusuario { get; set; }
        public int? codinsprecebe { get; set; }
        public decimal? codtransf { get; set; }
        public int? codsolic { get; set; }
        public decimal? codinv { get; set; }
        public int? codsobra { get; set; }
        public int? codromaneiosepdet { get; set; }
        public int? codreserva { get; set; }
        public int? codempenho { get; set; }
        public int? indiceromitem { get; set; }
    }
}
