﻿using Hino.Estoque.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.ViewModels.Estoque
{
    public class ESCreateProdTransferenciaVM
    {
        [DisplayField]
        [RequiredField]
        public int CodEstab { get; set; }

        [DisplayField]
        [RequiredField]
        public string CodProduto { get; set; }

        [DisplayField]
        [RequiredField]
        public string CodEstoqueOri { get; set; }

        [DisplayField]
        [RequiredField]
        public string CodEstoqueDest { get; set; }

        [DisplayField]
        [RequiredField]
        public double Quantidade { get; set; }

        [DisplayField]
        [RequiredField]
        public string CodUsuario { get; set; }

        [DisplayField]
        public long Lote { get; set; }

        public long? CodOrdProd { get; set; }
        public string NivelOrdProd { get; set; }
    }
}
