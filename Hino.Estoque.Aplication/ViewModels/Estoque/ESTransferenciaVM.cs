using Hino.Estoque.Aplication.ViewModels;
using Hino.Estoque.Infra.Cross.Entities.Gerais;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Estoque.Aplication.ViewModels.Estoque
{
    public class ESTransferenciaVM : BaseVM
    {
        public ESTransferenciaVM()
        {
        }
        
        public short codestab { get; set; }
        public decimal codtransf { get; set; }
        public DateTime data { get; set; }
        public string tipo { get; set; }
        public decimal? codordprod { get; set; }
        public string nivelordprod { get; set; }
        public decimal? codestrutura { get; set; }
        public int? codpedvenda { get; set; }
        public string status { get; set; }
        public string solicitante { get; set; }
        public string responsavel { get; set; }
        public DateTime? dtstatus { get; set; }
        
    }
}
