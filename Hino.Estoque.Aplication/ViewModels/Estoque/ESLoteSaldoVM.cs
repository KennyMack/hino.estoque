using Hino.Estoque.Aplication.ViewModels;
using Hino.Estoque.Aplication.ViewModels.Fiscal;
using Hino.Estoque.Infra.Cross.Entities.Fiscal;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Estoque.Aplication.ViewModels.Estoque
{
    public class ESLoteSaldoVM : BaseVM
    {
        public ESLoteSaldoVM()
        {
            this.ESTransfLote = new HashSet<ESTransfLoteVM>();
        }

        public short codestab { get; set; }
        public decimal lote { get; set; }
        public string codestoque { get; set; }
        public decimal qtdentrada { get; set; }
        public decimal qtdsaida { get; set; }
        
        public decimal saldoLote
        {
            get => qtdentrada - qtdsaida;
        }

        public virtual ESLoteVM ESLote { get; set; }
        public virtual ICollection<ESTransfLoteVM> ESTransfLote { get; set; }
        public virtual FSLocalEstoqueVM FSLocalEstoque { get; set; }
    }
}
