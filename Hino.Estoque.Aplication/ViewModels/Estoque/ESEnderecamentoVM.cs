﻿using Hino.Estoque.Aplication.ViewModels.Gerais;
using Hino.Estoque.Infra.Cross.Utils.Attributes;
using System.Collections.Generic;

namespace Hino.Estoque.Aplication.ViewModels.Estoque
{
    public class ESEnderecamentoVM : BaseVM
    {
        public ESEnderecamentoVM()
        {
            this.ESProdEnderec = new HashSet<ESProdEnderecVM>();
        }

        [DisplayField]
        [RequiredField]
        public short codestab { get; set; }
        [DisplayField]
        [RequiredField]
        public int codendestoque { get; set; }
        [DisplayField]
        [RequiredField]
        public string descricao { get; set; }
        [DisplayField]
        [RequiredField]
        public byte tipo { get; set; }
        [DisplayField]
        [RequiredField]
        public string sigla { get; set; }
        [DisplayField]
        [RequiredField]
        public string nivel { get; set; }
        [DisplayField]
        [RequiredField]
        public string posicao { get; set; }
        [DisplayField]
        [RequiredField]
        public string rua { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal capacidadem3 { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal altura { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal largura { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal comprimento { get; set; }
        
        public virtual ICollection<ESProdEnderecVM> ESProdEnderec { get; set; }
        public virtual GEEstabVM GEEstab { get; set; }
    }
}
