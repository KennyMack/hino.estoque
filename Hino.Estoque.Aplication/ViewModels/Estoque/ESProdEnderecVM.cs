﻿using Hino.Estoque.Aplication.ViewModels.Fiscal;
using Hino.Estoque.Infra.Cross.Utils.Attributes;

namespace Hino.Estoque.Aplication.ViewModels.Estoque
{
    public class ESProdEnderecVM : BaseVM
    {
        [DisplayField]
        [RequiredField]
        public short codestab { get; set; }
        [DisplayField]
        [RequiredField]
        public int codendestoque { get; set; }
        [DisplayField]
        [RequiredField]
        public string codproduto { get; set; }

        public virtual ESEnderecamentoVM ESEnderecamento { get; set; }
        public virtual FSProdutoparamEstabVM FSProdutoparamEstab { get; set; }
    }
}
