﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.ViewModels.Engenharia
{
    public class ENSearchOEEVM
    {
        public ENMaquinasVM[] Maquinas { get; set; }
        public ENProcessosVM[] Processos { get; set; }
        public ENProcAgrupVM[] Agrupamentos { get; set; }
    }
}
