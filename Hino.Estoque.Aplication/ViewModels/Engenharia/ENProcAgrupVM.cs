﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.ViewModels.Engenharia
{
    public class ENProcAgrupVM : BaseVM
    {
        public int codestab { get; set; }
        public long codgrupo { get; set; }
        public string descricao { get; set; }
    }
}
