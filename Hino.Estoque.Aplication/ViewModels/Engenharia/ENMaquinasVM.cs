﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.ViewModels.Engenharia
{
    public class ENMaquinasVM : BaseVM
    {
        public short codestab { get; set; }
        public string codmaquina { get; set; }
        public string descricao { get; set; }
        public short codcelula { get; set; }
        public short? codprocesso { get; set; }
    }
}
