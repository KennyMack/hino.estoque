using Hino.Estoque.Aplication.ViewModels.Estoque;
using Hino.Estoque.Aplication.ViewModels.Fiscal;
using System;
using System.Collections.Generic;

namespace Hino.Estoque.Aplication.ViewModels.Producao
{
    public class PDOrdemProdVM : BaseVM 
    {
        public PDOrdemProdVM()
        {
            this.estransferencia = new HashSet<ESTransferenciaVM>();
            this.pdordemprodrotinas = new HashSet<PDOrdemProdRotinasVM>();
            this.pdordemprodcomp = new HashSet<PDOrdemProdCompVM>();
        }
        
        public short codestab { get; set; }
        public decimal codprograma { get; set; }
        public decimal codordprod { get; set; }
        public string nivelordprod { get; set; }
        public decimal codordprodpai { get; set; }
        public string nivelordprodpai { get; set; }
        public int codroteiro { get; set; }
        public decimal codestrutura { get; set; }
        public string codproduto { get; set; }
        public DateTime dtinicio { get; set; }
        public DateTime dttermino { get; set; }
        public DateTime? dtencerramento { get; set; }
        public decimal programado { get; set; }
        public decimal realizado { get; set; }
        public decimal refugado { get; set; }
        public string status { get; set; }
        public decimal ultimasequencia { get; set; }
        public decimal fator { get; set; }
        public decimal qtdinicial { get; set; }
        public decimal custoinicial { get; set; }
        public decimal custoreproc { get; set; }
        public int seqbarras { get; set; }
        public int? codagrupfoto { get; set; }
        public bool liberado { get; set; }
        public DateTime dtemissao { get; set; }
        public bool usaopestoque { get; set; }
        public bool usasaldoestoque { get; set; }

        public virtual ICollection<ESTransferenciaVM> estransferencia { get; set; }
        public virtual FSProdutoVM fsproduto { get; set; }
        public virtual ICollection<PDOrdemProdRotinasVM> pdordemprodrotinas { get; set; }
        public virtual ICollection<PDOrdemProdCompVM> pdordemprodcomp { get; set; }
    }
}
