
using Hino.Estoque.Aplication.ViewModels.Fiscal;
using System.Collections.Generic;

namespace Hino.Estoque.Aplication.ViewModels.Producao
{
    public class PDOrdemProdCompVM : BaseVM 
    {
        public PDOrdemProdCompVM()
        {
            this.pdoptransf = new HashSet<PDOPTransfVM>();
        }
        
        public short codestab { get; set; }
        public decimal codordprod { get; set; }
        public string nivelordprod { get; set; }
        public decimal codestrutura { get; set; }
        public int codroteiro { get; set; }
        public int operacao { get; set; }
        public string codcomponente { get; set; }
        public string codestoque { get; set; }
        public decimal quantidade { get; set; }
        public decimal qtdconsumida { get; set; }
        public bool baixaapto { get; set; }
        public decimal variacao { get; set; }
        public decimal perda { get; set; }
        public bool reaproveita { get; set; }
        public decimal altmanual { get; set; }
        public decimal fator { get; set; }
        public decimal geraconsudif { get; set; }
        public decimal sequencia { get; set; }
        public decimal camadas { get; set; }
        public decimal passadas { get; set; }
        public byte nivel { get; set; }
        public short? codtecido { get; set; }
        public decimal percagrup { get; set; }
        public string codunidade { get; set; }
        public decimal qtdeeng { get; set; }

        public virtual FSLocalEstoqueVM fslocalestoque { get; set; }
        public virtual FSProdutoVM fsproduto { get; set; }
        public virtual ICollection<PDOPTransfVM> pdoptransf { get; set; }
        public virtual PDOrdemProdVM pdordemprod { get; set; }
        public virtual PDOrdemProdRotinasVM pdordemprodrotinas { get; set; }
    }
}
