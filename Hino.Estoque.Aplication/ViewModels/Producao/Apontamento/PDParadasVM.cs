﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.ViewModels.Producao.Apontamento
{
    public class PDParadasVM : BaseVM
    {
        public long codparada { get; set; }
        public short codestab { get; set; }
        public long codordprod { get; set; }
        public string nivelordprod { get; set; }
        public DateTime inicio { get; set; }
        public DateTime termino { get; set; }
        public int codfuncionario { get; set; }
        public short codmotivo { get; set; }
        public string observacao { get; set; }
        public decimal codestrutura { get; set; }
        public string turno { get; set; }
        public string codmaquina { get; set; }
    }
}
