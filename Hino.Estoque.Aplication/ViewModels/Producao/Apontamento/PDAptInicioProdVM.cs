﻿using Hino.Estoque.Aplication.ViewModels.Gerais;
using Hino.Estoque.Infra.Cross.Utils.Attributes;
using System;

namespace Hino.Estoque.Aplication.ViewModels.Producao.Apontamento
{
    public class PDAptInicioProdVM : BaseVM
    {
        [DisplayField]
        public long codiniapt { get; set; }
        [DisplayField]
        public short codestab { get; set; }
        [DisplayField]
        public decimal codordprod { get; set; }
        [DisplayField]
        public string nivelordprod { get; set; }
        [DisplayField]
        public decimal codestrutura { get; set; }
        [DisplayField]
        public short codroteiro { get; set; }
        [DisplayField]
        public short operacao { get; set; }
        [DisplayField]
        public decimal codfuncionario { get; set; }
        [DisplayField]
        public string codusuario { get; set; }
        [DisplayField]
        public DateTime dtinicio { get; set; }
        /// <summary>
        /// 1 - Inicio Preparação
        /// 2 - Fim Preparação
        /// 3 - Inicio de Produção
        /// 4 - Fim de Produção
        /// 5 - Inicio Troca de Serviço
        /// 6 - Fim Troca de Serviço
        /// 7 - Inicio de Manutenção
        /// 8 - Fim de Manutenção
        /// 9 - Inicio Consulta Desenho
        /// 10 - Fim Consulta Desenho
        /// 11 - Início Retrabalho
        /// 12 - Fim Retrabalho
        /// 13 - Término turno
        /// </summary>
        [DisplayField]
        public short tipo { get; set; }
        [DisplayField]
        public int? codmotivo { get; set; }
        [DisplayField]
        public string codmaquina { get; set; }

        public virtual GEEstabVM geestab { get; set; }
        public virtual PDOrdemProdRotinasVM pdordemprodrotinas { get; set; }
    }

    public class PDAptInicioProdCreateVM: BaseVM
    {
        [DisplayField]
        [RequiredField]
        public short codestab { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal codordprod { get; set; }
        [DisplayField]
        [RequiredField]
        public string nivelordprod { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal codestrutura { get; set; }
        [DisplayField]
        [RequiredField]
        public short codroteiro { get; set; }
        [DisplayField]
        [RequiredField]
        public short operacao { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal codfuncionario { get; set; }
        [DisplayField]
        [RequiredField]
        public string codusuario { get; set; }
        [DisplayField]
        [RequiredField]
        [IntegerRangeField(1, 13)]
        public short tipo { get; set; }
        [DisplayField]
        public int? codmotivo { get; set; }
        [DisplayField]
        public string codmaquina { get; set; }
    }
}
