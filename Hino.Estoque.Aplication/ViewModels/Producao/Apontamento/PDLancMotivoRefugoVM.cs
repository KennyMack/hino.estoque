﻿using Hino.Estoque.Infra.Cross.Utils.Attributes;

namespace Hino.Estoque.Aplication.ViewModels.Producao.Apontamento
{
    public class PDLancMotivoRefugoVM: BaseVM
    {
        [DisplayField]
        [RequiredField]
        public short codmotivo { get; set; }
        [DisplayField]
        public string descricao { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal quantidade { get; set; }
        public decimal qtdreaproveitada { get; set; }
        public decimal qtdsucata { get; set; }
        public bool apontarpeso { get; set; }
    }
}
