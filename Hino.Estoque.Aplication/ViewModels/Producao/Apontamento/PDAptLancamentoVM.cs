﻿using Hino.Estoque.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.ViewModels.Producao.Apontamento
{
    public class PDAptLancamentoVM: BaseVM
    {
        [DisplayField]
        [RequiredField]
        public long codfimapt { get; set; }
        [DisplayField]
        [RequiredField]
        public short codestab { get; set; }
        [DisplayField]
        [RequiredField]
        public long codlancamento { get; set; }
    }
}
