﻿using Hino.Estoque.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.ViewModels.Producao.Apontamento
{
    public class PDAptTerminoProdVM : BaseVM
    {
        [DisplayField]
        public long codfimapt { get; set; }
        [DisplayField]
        public short codestab { get; set; }
        [DisplayField]
        public long codiniapt { get; set; }
        [DisplayField]
        public string codusuario { get; set; }
        [DisplayField]
        public int codfuncionario { get; set; }
        [DisplayField]
        public DateTime dttermino { get; set; }
        /// <summary>
        /// 1 - Inicio Preparação
        /// 2 - Fim Preparação
        /// 3 - Inicio de Produção
        /// 4 - Fim de Produção
        /// 5 - Inicio Troca de Serviço
        /// 6 - Fim Troca de Serviço
        /// 7 - Inicio de Manutenção
        /// 8 - Fim de Manutenção
        /// 9 - Inicio Consulta Desenho
        /// 10 - Fim Consulta Desenho
        /// 11 - Início Retrabalho
        /// 12 - Fim Retrabalho
        /// 13 - Termino turno
        /// </summary>
        [DisplayField]
        public short tipo { get; set; }
        [DisplayField]
        public decimal quantidade { get; set; }
        [DisplayField]
        public short? codmotivo { get; set; }
        [DisplayField]
        public decimal qtdrefugo { get; set; }
        [DisplayField]
        public decimal qtdretrabalho { get; set; }
        [DisplayField]
        public string observacao { get; set; }
        public bool askstartnew { get; set; }
    }

    public class PDAptTerminoProdCreateVM : BaseVM
    {
        public PDAptTerminoProdCreateVM()
        {
            Refugos = new HashSet<PDLancMotivoRefugoVM>();
        }

        [DisplayField]
        [RequiredField]
        public long codfimapt { get; set; }
        [DisplayField]
        [RequiredField]
        public short codestab { get; set; }
        [DisplayField]
        [RequiredField]
        [LongGreaterThan(0)]
        public long codiniapt { get; set; }
        [DisplayField]
        [RequiredField]
        public string codusuario { get; set; }
        [DisplayField]
        [RequiredField]
        [LongGreaterThan(0)]
        public int codfuncionario { get; set; }
        [DisplayField]
        [RequiredField]
        public DateTime dttermino { get; set; }
        /// <summary>
        /// 1 - Inicio Preparação
        /// 2 - Fim Preparação
        /// 3 - Inicio de Produção
        /// 4 - Fim de Produção
        /// 5 - Inicio Troca de Serviço
        /// 6 - Fim Troca de Serviço
        /// 7 - Inicio de Manutenção
        /// 8 - Fim de Manutenção
        /// 9 - Inicio Consulta Desenho
        /// 10 - Fim Consulta Desenho
        /// 11 - Início Retrabalho
        /// 12 - Fim Retrabalho
        /// 13 - Termino turno
        /// </summary>
        [DisplayField]
        [RequiredField]
        [IntegerRangeField(1, 13)]
        public short tipo { get; set; }
        [DisplayField]
        [RequiredField]
        [DefaultValue(0)]
        public decimal quantidade { get; set; }
        [DisplayField]
        public short? codmotivo { get; set; }
        [DisplayField]
        [RequiredField]
        [DefaultValue(0)]
        public decimal qtdretrabalho { get; set; }
        [DisplayField]
        public string observacao { get; set; }
        [DisplayField]
        public string codmaquina { get; set; }

        public ICollection<PDLancMotivoRefugoVM> Refugos { get; set; }
    }
}
