﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.ViewModels.Producao.Apontamento
{
    public class PDAptLancListVM: BaseVM
    {
        public PDAptLancListVM()
        {
            Refugos = new HashSet<PDLancMotivoRefugoVM>();
        }

        public long CodAptInicio { get; set; }
        public long? CodAptTermino { get; set; }
        public long? CodLancamento { get; set; }
        public short Tipo { get; set; }
        public DateTime DtInicio { get; set; }
        public DateTime? DtTermino { get; set; }
        public decimal CodOrdProd { get; set; }
        public string NivelOrdProd { get; set; }
        public int CodRoteiro { get; set; }
        public int Operacao { get; set; }
        public string DescOperacao { get; set; }
        public string Turno { get; set; }
        public string CodMaquina { get; set; }
        public string CodProduto { get; set; }
        public string Descricao { get; set; }
        public decimal CodEstrutura { get; set; }

        public decimal Quantidade { get; set; }
        public decimal QtdRefugo { get; set; }
        public int CodFuncionario { get; set; }
        public string NomeFuncionario { get; set; }
        public string CodUsuario { get; set; }

        public int TotalMin { get; set; }
        public string DescTotalMin { get; set; }
        public int TotalMinEsp { get; set; }
        public string DescTotalMinEsp { get; set; }
        public int Produtividade { get; set; }

        public ICollection<PDLancMotivoRefugoVM> Refugos { get; set; }
    }
}
