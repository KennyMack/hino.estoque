
using Hino.Estoque.Aplication.ViewModels.Estoque;
using Hino.Estoque.Aplication.ViewModels.Gerais;
using System.Collections.Generic;

namespace Hino.Estoque.Aplication.ViewModels.Producao.Apontamento
{
    public class PDLancamentosVM : BaseVM 
    {
        public PDLancamentosVM()
        {
            this.eskardex = new HashSet<ESKardexVM>();
            this.eslote = new HashSet<ESLoteVM>();
        }

        public short codestab { get; set; }
        public long codlancamento { get; set; }
        public decimal codordprod { get; set; }
        public string nivelordprod { get; set; }
        public int codroteiro { get; set; }
        public int operacao { get; set; }
        public System.DateTime dtinicio { get; set; }
        public System.DateTime dttermino { get; set; }
        public decimal codfuncionario { get; set; }
        public decimal quantidade { get; set; }
        public decimal codestrutura { get; set; }
        public decimal? codlancpai { get; set; }
        public string turno { get; set; }
        public string codmaquina { get; set; }
        public decimal? codcor { get; set; }
        public string lotemanual { get; set; }
        public bool reginsp { get; set; }
        public short? codmotivo { get; set; }
        public int totalmin { get; set; }
        public int totalminesp { get; set; }
        public int produtividade { get; set; }

        public virtual ICollection<ESKardexVM> eskardex { get; set; }
        public virtual ICollection<ESLoteVM> eslote { get; set; }
        public virtual GEEstabVM geestab { get; set; }
        public virtual GEFuncionariosVM gefuncionarios { get; set; }
        public virtual PDOrdemProdRotinasVM pdordemprodrotinas { get; set; }
    }
}
