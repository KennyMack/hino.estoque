﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.ViewModels.Producao.Apontamento
{
    public class PDAptParadasVM: BaseVM
    {
        public long codfimapt { get; set; }
        public short codestab { get; set; }
        public long codparada { get; set; }
    }
}
