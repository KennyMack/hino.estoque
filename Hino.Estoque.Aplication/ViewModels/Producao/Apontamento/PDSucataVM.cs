﻿using Hino.Estoque.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.ViewModels.Producao.Apontamento
{
    public class PDSucataVM : BaseVM
    {
        public short codestab { get; set; }
        public long codsucata { get; set; }
        public DateTime datasuc { get; set; }
        public long? lote { get; set; }
        public string codproduto { get; set; }
        public string codestoque { get; set; }
        public decimal? qtdeproduto { get; set; }
        public string codprdsuc { get; set; }
        public string codestsuc { get; set; }
        public decimal qtdesucata { get; set; }
        public int codmotivo { get; set; }
        public decimal codfuncionario { get; set; }
        public decimal codordprod { get; set; }
        public string nivelordprod { get; set; }
        public decimal codestrutura { get; set; }
        public long? codlancamento { get; set; }
    }

    public class PDSucataCreateVM : BaseVM
    {
        [DisplayField]
        [RequiredField]
        public short codestab { get; set; }
        public long? lote { get; set; }
        [DisplayField]
        [RequiredField]
        public string codproduto { get; set; }
        [DisplayField]
        [RequiredField]
        public string codestoque { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal qtdeproduto { get; set; }
        [DisplayField]
        public string codprdsuc { get; set; }
        [DisplayField]
        public string codestsuc { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal qtdesucata { get; set; }
        [DisplayField]
        [RequiredField]
        public int codmotivo { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal codfuncionario { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal codordprod { get; set; }
        [DisplayField]
        [RequiredField]
        public string nivelordprod { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal codestrutura { get; set; }
        public long? codlancamento { get; set; }
    }

    public class PDOPProductsSucVM
    {
        [DisplayField]
        [RequiredField]
        [LongGreaterThan(0)]
        public long CodOrdProd { get; set; }

        [DisplayField]
        [RequiredField]
        public string NivelOrdProd { get; set; }
    }
}
