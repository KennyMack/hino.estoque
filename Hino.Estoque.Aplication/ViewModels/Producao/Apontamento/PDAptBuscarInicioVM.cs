﻿using Hino.Estoque.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.ViewModels.Producao.Apontamento
{
    public class PDAptBuscarInicioVM
    {
        [DisplayField]
        [RequiredField]
        public short CodEstab { get; set; }
        [DisplayField]
        [RequiredField]
        public long CodOrdProd { get; set; }
        [DisplayField]
        [RequiredField]
        public string NivelOrdProd { get; set; }
        [DisplayField]
        [RequiredField]
        public long CodEstrutura { get; set; }
        [DisplayField]
        [RequiredField]
        public int CodRoteiro { get; set; }
        [DisplayField]
        [RequiredField]
        public int Operacao { get; set; }
        [DisplayField]
        [RequiredField]
        public int CodFuncionario { get; set; }
        [DisplayField]
        [RequiredField]
        public short Tipo { get; set; }
    }
}
