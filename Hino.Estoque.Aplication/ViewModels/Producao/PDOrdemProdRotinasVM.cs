
using Hino.Estoque.Aplication.ViewModels.Producao.Apontamento;
using System;
using System.Collections.Generic;

namespace Hino.Estoque.Aplication.ViewModels.Producao
{
    public class PDOrdemProdRotinasVM : BaseVM 
    {
        public PDOrdemProdRotinasVM()
        {
            this.pdlancamentos = new HashSet<PDLancamentosVM>();
            this.pdordemprodcomp = new HashSet<PDOrdemProdCompVM>();
        }
        
        public short codestab { get; set; }
        public decimal codordprod { get; set; }
        public string nivelordprod { get; set; }
        public decimal codestrutura { get; set; }
        public int codroteiro { get; set; }
        public int operacao { get; set; }
        public short codrotina { get; set; }
        public string descricao { get; set; }
        public string codmaquina { get; set; }
        public decimal prodhoraria { get; set; }
        public decimal operador { get; set; }
        public bool aptoprocesso { get; set; }
        public bool aptoacabado { get; set; }
        public decimal qtdeproducao { get; set; }
        public decimal qtderefugo { get; set; }
        // public DateTime dtinicio { get; set; }
        // public DateTime dttermino { get; set; }
        // public decimal alteradomanual { get; set; }

        public virtual ICollection<PDLancamentosVM> pdlancamentos { get; set; }
        public virtual PDOrdemProdVM pdordemprod { get; set; }
        public virtual ICollection<PDOrdemProdCompVM> pdordemprodcomp { get; set; }
    }
}
