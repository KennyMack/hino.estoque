﻿using Hino.Estoque.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.ViewModels.Producao
{
    public class PDEficienciaProducaoVM
    {
        [DisplayField]
        [RequiredField]
        public short CodEstab { get; set; }
        [DisplayField]
        [RequiredField]
        public DateTime DataInicio { get; set; }
        [DisplayField]
        [RequiredField]
        public DateTime DataTermino { get; set; }
    }
}
