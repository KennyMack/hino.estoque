﻿using Hino.Estoque.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.ViewModels.Producao.OEE
{
    public class PDOEEGerarVM
    {
        [RequiredField]
        public short codestab { get; set; }
        [RequiredField]
        public string codusuario { get; set; }
        [RequiredField]
        public DateTime dataini { get; set; }
        [RequiredField]
        public DateTime datafim { get; set; }
        public short? codprocesso { get; set; }
        public string codmaquina { get; set; }
        public int? codagrup { get; set; }
        [RequiredField]
        public string turno { get; set; }
        [RequiredField]
        public int gerardet { get; set; }
    }

    public class PDOEEListarVM
    {
        [RequiredField]
        public string codusuario { get; set; }
        public string codmaquina { get; set; }
        public DateTime dataini { get; set; }
    }
}
