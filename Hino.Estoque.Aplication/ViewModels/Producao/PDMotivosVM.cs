﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.ViewModels.Producao
{
    public class PDMotivosVM: BaseVM
    {
        public int codmotivo { get; set; }
        public string descricao { get; set; }
        /// <summary>
        /// 0 'PARADA'
        /// 1 'REFUGO'
        /// 2 'RETRABALHO'
        /// 3 'PARADA PROGRAMADA'
        /// </summary>
        public short tipo { get; set; }
        public bool status { get; set; }
    }
}
