﻿using Hino.Estoque.Infra.Cross.Entities.Producao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.ViewModels.Producao
{
    public class PDResultTransfOPVM
    {
        public PDResultTransfOPVM()
        {
            TransfResults = new PDOPTransfResultVM();
        }

        public string Result { get; set; }
        public bool Success { get; set; }
        public PDOPTransfResultVM TransfResults { get; set; }

    }
}
