
using Hino.Estoque.Aplication.ViewModels.Estoque;

namespace Hino.Estoque.Aplication.ViewModels.Producao
{
    public class PDOPTransfLoteVM : BaseVM 
    {
        public short codestab { get; set; }
        public int codtransf { get; set; }
        public decimal lote { get; set; }
        public decimal quantidade { get; set; }

        public virtual ESLoteVM eslote { get; set; }
        public virtual PDOPTransfVM pdoptransf { get; set; }
    }
}
