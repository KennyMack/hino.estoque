
using Hino.Estoque.Aplication.ViewModels.Fiscal;
using Hino.Estoque.Aplication.ViewModels.Gerais;
using Hino.Estoque.Infra.Cross.Utils.Attributes;
using System.Collections.Generic;

namespace Hino.Estoque.Aplication.ViewModels.Producao
{
    public class PDOPCreateTransfVM : BaseVM
    {
        public PDOPCreateTransfVM()
        {
        }

        [DisplayField]
        [RequiredField]
        [IntegerRangeField(0, 999)]
        public short codestab { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal codordprod { get; set; }
        [DisplayField]
        [RequiredField]
        public string nivelordprod { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal codestrutura { get; set; }
        [DisplayField]
        [RequiredField]
        [IntegerRangeField(0, 999)]
        public int codroteiro { get; set; }
        [DisplayField]
        [RequiredField]
        [IntegerRangeField(0, 999)]
        public int operacao { get; set; }
        [DisplayField]
        [RequiredField]
        public string codcomponente { get; set; }
        [DisplayField]
        [RequiredField]
        public string codestoqueori { get; set; }
        [DisplayField]
        [RequiredField]
        public string codestoquedest { get; set; }
        [DisplayField]
        [RequiredField]
        public string codusuario { get; set; }
        [DisplayField]
        [RequiredField]
        public double quantidade { get; set; }
        [DisplayField]
        [RequiredField]
        public double qtdseparada { get; set; }
    }

    public class PDOPTransfVM : BaseVM 
    {
        public PDOPTransfVM()
        {
            this.pdoptransflote = new HashSet<PDOPTransfLoteVM>();
        }
        
        public short codestab { get; set; }
        public int codtransf { get; set; }
        public decimal codordprod { get; set; }
        public string nivelordprod { get; set; }
        public decimal codestrutura { get; set; }
        public byte codroteiro { get; set; }
        public byte operacao { get; set; }
        public string codcomponente { get; set; }
        public string codestoqueori { get; set; }
        public string codestoquedest { get; set; }
        public string codusuario { get; set; }
        public System.DateTime datatransf { get; set; }
        public double quantidade { get; set; }
        public byte status { get; set; }
        public decimal? codtransfest { get; set; }

        public virtual FSLocalEstoqueVM fslocalestoque { get; set; }
        public virtual GEUsuariosVM geusuarios { get; set; }
        public virtual ICollection<PDOPTransfLoteVM> pdoptransflote { get; set; }
        public virtual PDOrdemProdCompVM pdordemprodcomp { get; set; }
    }
}
