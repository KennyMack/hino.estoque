﻿using Hino.Estoque.Infra.Cross.Entities.Producao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.ViewModels.Producao
{
    public class PDOPTransfResultVM
    {
        public PDOPTransfResultVM()
        {
            OrdemProd = new PDOrdemProd();
            Transferencias = new List<PDOPTransfItemResultVM>();
        }

        public PDOrdemProd OrdemProd { get; set; }
        public List<PDOPTransfItemResultVM> Transferencias { get; set; }
    }

    public class PDOPTransfItemResultVM
    {
        public PDOPTransf Transferencia { get; set; }
        public bool Rastreabilidade { get; set; }
        public bool Sucesso { get; set; }
        public string Motivo { get; set; }
        public bool Validado { get; set; }
        public double QtdSeparada { get; set; }
        public double StockBalance { get; set; }
    }
}
