using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Estoque.Aplication.ViewModels.Gerais
{
    public class GELogDetVM : BaseVM
    {
        public decimal codlog { get; set; }
        public string campo { get; set; }
        public string de { get; set; }
        public string para { get; set; }

        public virtual GELogVM gelog { get; set; }
    }
}
