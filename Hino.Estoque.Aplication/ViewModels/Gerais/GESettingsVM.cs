﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.ViewModels.Gerais
{
    public class GESettingsVM
    {
        public string DefaultOriginStock { get; set; }
        public string DefaultStock { get; set; }
        public bool LoadQtd { get; set; }
        public bool ShowResultsOP { get; set; }
        public bool AskStartNew { get; set; }
        public bool AllowTransfMoreThanOp { get; set; }
        public bool KeepTransfLocations { get; set; }
        public bool AllowChangeStockOrigin { get; set; }
        public bool UsePassword { get; set; }
        public bool QuantityManual { get; set; }
        public bool UseStockOriginSuggested { get; set; }
        public bool MachineOnStartAppointment { get; set; }

    }
}
