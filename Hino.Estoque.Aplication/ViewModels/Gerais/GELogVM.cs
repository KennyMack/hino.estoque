using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Estoque.Aplication.ViewModels.Gerais
{
    public class GELogVM : BaseVM
    {
        public GELogVM()
        {
            this.GELogDet = new HashSet<GELogDetVM>();
        }

        public decimal codlog { get; set; }
        public DateTime datalog { get; set; }
        public string tabela { get; set; }
        public string chavereg { get; set; }
        public string codusuario { get; set; }
        public string estacao { get; set; }
        public string acao { get; set; }
        
        public virtual ICollection<GELogDetVM> GELogDet { get; set; }
        public virtual GEUsuariosVM GEUsuarios { get; set; }
    }
}
