﻿using System;

namespace Hino.Estoque.Aplication.ViewModels.Fiscal
{
    public class CZIntegrEmpresaVM : BaseVM
    {
        public long IdIntegracao { get; set; }
        public long ID { get; set; }
        public string CNPJCPF { get; set; }
        public string GrupoEIP { get; set; }
        public string Site { get; set; }
        public string EMailNFe { get; set; }
        public int CodEstado { get; set; }
        public string Fone { get; set; }
        public string Fax { get; set; }
        public string Tipo { get; set; }
        public bool Apagar { get; set; }

        public long? CodEmpresa { get; set; }
        public DateTime? DataIntegra { get; set; }
        public bool EmpIntegra { get; set; }
    }
}
