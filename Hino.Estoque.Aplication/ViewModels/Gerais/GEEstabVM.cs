using Hino.Estoque.Aplication.ViewModels.Estoque;
using Hino.Estoque.Aplication.ViewModels.Fiscal;
using System.Collections.Generic;

namespace Hino.Estoque.Aplication.ViewModels.Gerais
{
    public class GEEstabVM : BaseVM
    {
        public GEEstabVM()
        {
            this.ESInventario = new HashSet<ESInventarioVM>();
            this.ESKardex = new HashSet<ESKardexVM>();
            this.ESLote = new HashSet<ESLoteVM>();
            this.ESTransferencia = new HashSet<ESTransferenciaVM>();
            this.FSProdutoparamEstab = new HashSet<FSProdutoparamEstabVM>();
            this.FSProdutoPcp = new HashSet<FSProdutoPcpVM>();
            this.GEFuncionarios = new HashSet<GEFuncionariosVM>();
            this.ESEnderecamento = new HashSet<ESEnderecamentoVM>();
        }

        public short codestab { get; set; }
        public string razaosocial { get; set; }
        public string nomefantasia { get; set; }
        public int? codendereco { get; set; }
        public byte codmoeda { get; set; }
        public decimal ativo { get; set; }
        public bool cdquantidade { get; set; }
        public byte cdvlunitario { get; set; }
        public bool cdvltotal { get; set; }
        public short cdpercentual { get; set; }
        public short? codnatopercompra { get; set; }
        public short? codnatopervenda { get; set; }
        public bool? gerarcontaempresa { get; set; }
        public string caminhoimagens { get; set; }
        public decimal cdtamanhocubico { get; set; }
        public short? codnatopercf { get; set; }
        public bool consestoque { get; set; }
        public string certificado { get; set; }
        public short? codgrupoestab { get; set; }
        public bool usaunnegocio { get; set; }
        public string tokenbuscacnpj { get; set; }
        
        public virtual ICollection<ESInventarioVM> ESInventario { get; set; }
        public virtual ICollection<ESKardexVM> ESKardex { get; set; }
        public virtual ICollection<ESLoteVM> ESLote { get; set; }
        public virtual ICollection<ESTransferenciaVM> ESTransferencia { get; set; }
        public virtual ICollection<FSProdutoparamEstabVM> FSProdutoparamEstab { get; set; }
        public virtual ICollection<FSProdutoPcpVM> FSProdutoPcp { get; set; }
        public virtual ICollection<GEFuncionariosVM> GEFuncionarios { get; set; }
        public virtual ICollection<ESEnderecamentoVM> ESEnderecamento { get; set; }
    }
}
