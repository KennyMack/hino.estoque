﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.ViewModels.Gerais
{
    public class CZIntegracaoRevisaoVM : BaseVM
    {
        public long IdIntegracao { get; set; }
        public string Desenho { get; set; }
        public string Revisao { get; set; }
        public DateTime? DataIntegracao { get; set; }
        public bool Integrado { get; set; }
    }
}
