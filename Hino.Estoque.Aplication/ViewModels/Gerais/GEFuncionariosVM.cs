using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Estoque.Aplication.ViewModels.Gerais
{
    public class GEFuncionariosVM : BaseVM
    {
        public short codestab { get; set; }
        public decimal codfuncionario { get; set; }
        public string identificacao { get; set; }
        public bool status { get; set; }
        public short? codprocesso { get; set; }
        public int codpessoa { get; set; }
        public string codccusto { get; set; }
        public int? contacorrente { get; set; }
        public string digitocc { get; set; }
        public string agencia { get; set; }
        public string digitoag { get; set; }
        public short? codbanco { get; set; }
        public string pis { get; set; }
        public string ctsp { get; set; }
        public string seriectsp { get; set; }
        public string ufctsp { get; set; }
        public string cargo { get; set; }
        public DateTime? admissao { get; set; }
        public DateTime? demissao { get; set; }
        public string titulo { get; set; }
        public string setor { get; set; }
        public string categoriahab { get; set; }
        public DateTime? validadehab { get; set; }
        public string codusuario { get; set; }
        public string numcnh { get; set; }
        public string tituloeleitor { get; set; }
        public string zona { get; set; }
        public string secao { get; set; }
        public string nomepai { get; set; }
        public string nomemae { get; set; }
        public string turno { get; set; }
        public bool aptproducao { get; set; }
        public bool aptsetup { get; set; }
        public bool apttrocaserv { get; set; }
        public bool aptmanut { get; set; }
        public bool aptconsdesenho { get; set; }
        public bool aptretrab { get; set; }
        public bool aptfimturno { get; set; }
        public bool aptestoque { get; set; }

        public virtual GEEstabVM GEESTAB { get; set; }
        public virtual GEUsuariosVM GEUSUARIOS { get; set; }
    }
}
