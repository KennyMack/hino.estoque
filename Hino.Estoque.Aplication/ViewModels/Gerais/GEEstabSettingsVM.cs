﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.ViewModels.Gerais
{
    public class GEEstabSettingsVM
    {
        public GEEstabSettingsVM()
        {
            Settings = new GESettingsVM();
            Estab = new GEEstabVM();
        }
        public GEEstabVM Estab { get; set; }
        public GESettingsVM Settings { get; set; }
    }
}
