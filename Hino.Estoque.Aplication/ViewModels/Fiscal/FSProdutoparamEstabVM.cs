using Hino.Estoque.Aplication.ViewModels.Estoque;
using Hino.Estoque.Aplication.ViewModels.Gerais;
using Hino.Estoque.Infra.Cross.Entities.Estoque;
using Hino.Estoque.Infra.Cross.Entities.Gerais;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Estoque.Aplication.ViewModels.Fiscal
{
    public class FSProdutoparamEstabVM : BaseVM
    {
        public FSProdutoparamEstabVM()
        {
            this.ESLote = new HashSet<ESLoteVM>();
            this.FSSaldoEstoque = new HashSet<FSSaldoEstoqueVM>();
            this.FSProdutoPcp = new HashSet<FSProdutoPcpVM>();
            this.ESProdEnderecVM = new HashSet<ESProdEnderecVM>();
        }
        
        public string codproduto { get; set; }
        public short codestab { get; set; }
        public string codfamilia { get; set; }
        public string codtipo { get; set; }
        public string codestoque { get; set; }
        public string codunidade { get; set; }
        public string ncm { get; set; }
        public bool status { get; set; }
        public bool contestoque { get; set; }
        public decimal? codaplicentrada { get; set; }
        public decimal? codaplicsaida { get; set; }
        public string codserv { get; set; }
        public bool? codorigmerc { get; set; }
        public string codcest { get; set; }
        public decimal perclucro { get; set; }
        public bool atualizapreco { get; set; }
        public bool? tipoatualizacao { get; set; }
        public bool? mrpplanjcomp { get; set; }
        public decimal perperdasped { get; set; }
        public string codanp { get; set; }
        public bool enviagtinxml { get; set; }
        public decimal fatconvunidex { get; set; }
        public DateTime? dtultcompra { get; set; }
        public decimal vlrultcompra { get; set; }
        public bool imobilizado { get; set; }
        public int? codsubfamilia { get; set; }
        public DateTime datamodificacao { get; set; }
        public string uniquekey { get; set; }
        public int? idapi { get; set; }
        public decimal vlrultcompraimp { get; set; }
        
        public virtual ICollection<ESLoteVM> ESLote { get; set; }
        public virtual FSLocalEstoqueVM FSLocalEstoque { get; set; }
        public virtual FSProdutoVM FSProduto { get; set; }
        public virtual ICollection<FSSaldoEstoqueVM> FSSaldoEstoque { get; set; }
        public virtual ICollection<FSProdutoPcpVM> FSProdutoPcp { get; set; }
        public virtual GEEstabVM GEEstab { get; set; }
        public virtual ICollection<ESProdEnderecVM> ESProdEnderecVM { get; set; }
    }
}
