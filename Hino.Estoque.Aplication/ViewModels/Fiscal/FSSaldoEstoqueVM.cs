
namespace Hino.Estoque.Aplication.ViewModels.Fiscal
{
    public class FSSaldoEstoqueVM : BaseVM
    {
        public string codproduto { get; set; }
        public short codestab { get; set; }
        public string codestoque { get; set; }
        public bool status { get; set; }
        public decimal anterior { get; set; }
        public decimal entrada { get; set; }
        public decimal saida { get; set; }
        
        public decimal saldoEstoque
        {
            get => (anterior + entrada) - saida;
        }

        public virtual FSLocalEstoqueVM FSLocalEstoque { get; set; }
        public virtual FSProdutoparamEstabVM FSProdutoparamEstab { get; set; }
    }
}
