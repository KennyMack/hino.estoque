using Hino.Estoque.Aplication.ViewModels.Estoque;
using Hino.Estoque.Infra.Cross.Entities.Estoque;
using Hino.Estoque.Infra.Cross.Entities.Fiscal;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Estoque.Aplication.ViewModels.Fiscal
{
    public class FSProdutoVM : BaseVM
    {
        public FSProdutoVM()
        {
            this.ESInventario = new HashSet<ESInventario>();
            this.ESInventDet = new HashSet<ESInventDet>();
            this.ESKardex = new HashSet<ESKardex>();
            this.ESTransfDet = new HashSet<ESTransfDet>();
            this.FSProdutoparamEstab = new HashSet<FSProdutoparamEstab>();
            this.FSProdutoPcp = new HashSet<FSProdutoPcp>();
        }
        
        public string codproduto { get; set; }
        public string descricao { get; set; }
        public string detalhamento { get; set; }
        public string infadicfiscal { get; set; }
        public string estoquedestino { get; set; }

        public virtual ICollection<ESInventario> ESInventario { get; set; }
        public virtual ICollection<ESInventDet> ESInventDet { get; set; }
        public virtual ICollection<ESKardex> ESKardex { get; set; }
        public virtual ICollection<ESTransfDet> ESTransfDet { get; set; }
        public virtual ICollection<FSProdutoparamEstab> FSProdutoparamEstab { get; set; }
        public virtual ICollection<FSProdutoPcp> FSProdutoPcp { get; set; }
    }
}
