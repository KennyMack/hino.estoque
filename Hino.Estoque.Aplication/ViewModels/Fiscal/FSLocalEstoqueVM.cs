using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Estoque.Aplication.ViewModels.Fiscal
{
    public class FSLocalEstoqueVM : BaseVM
    {
        public string codestoque { get; set; }
        public string descricao { get; set; }
        public string classificacao { get; set; }
        public string codgrupoestoq { get; set; }
        public int? codempresa { get; set; }
    }
}
