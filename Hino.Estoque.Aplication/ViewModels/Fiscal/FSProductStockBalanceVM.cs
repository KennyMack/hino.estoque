﻿using Hino.Estoque.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.ViewModels.Fiscal
{
    public class FSProductStockBalanceVM
    {
        [DisplayField]
        [RequiredField]
        public string CodProduto { get; set; }
    }
}
