﻿using Hino.Estoque.Infra.Cross.Utils.Exceptions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Estoque.Aplication.ViewModels
{    
    public class DefaultResultModel
    {
        public DefaultResultModel()
        {
            error = new List<ModelException>();
        }

        public int status { get; set; }
        public bool success { get; set; }
        public JContainer data { get; set; }
        public List<ModelException> error { get; set; }
    }
}
