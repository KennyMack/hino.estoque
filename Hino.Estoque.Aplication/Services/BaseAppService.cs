﻿using Hino.Estoque.Aplication.Interfaces;
using Hino.Estoque.Domain.Base.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Utils.Exceptions;
using Hino.Estoque.Infra.Cross.Utils.Paging;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.Services
{
    public class BaseAppService<T> : IBaseAppService<T>, IDisposable where T : class
    {
        private readonly IBaseService<T> _BaseService;
        public List<ModelException> Errors
        {
            get
            {
                return _BaseService.Errors;
            }
            set
            {
                _BaseService.Errors = value;
            }
        }

        public BaseAppService(IBaseService<T> baseService)
        {
            _BaseService = baseService;
        }

        public virtual T Add(T model)
        {
            _BaseService.Add(model);
            return model;
        }

        public virtual T Update(T model)
        {
            _BaseService.Update(model);
            return model;
        }        

        public virtual void Remove(T model)
        {
            _BaseService.Remove(model);
        }

        public async virtual Task<IEnumerable<T>> GetAllAsync(params Expression<Func<T, object>>[] includeProperties) =>
            await _BaseService.GetAllAsync(includeProperties);

        public async virtual Task<PagedResult<T>> GetAllPagedAsync(int page, int pageSize, params Expression<Func<T, object>>[] includeProperties) =>
           await _BaseService.GetAllPagedAsync(page, pageSize, includeProperties);

        public async virtual Task<IEnumerable<T>> QueryAsync(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties) =>
           await _BaseService.QueryAsync(predicate, includeProperties);

        public async virtual Task<PagedResult<T>> QueryPagedAsync(int page, int pageSize, Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties) =>
           await _BaseService.QueryPagedAsync(page, pageSize, predicate, includeProperties);

        public virtual long NextSequence() =>
            _BaseService.NextSequence();

        public virtual async Task<long> NextSequenceAsync() =>
            await _BaseService.NextSequenceAsync();

        public virtual async Task<int> SaveChanges() =>
            await _BaseService.SaveChanges();

        public void RollBackChanges() =>
            RollBackChanges();

        public void Dispose()
        {
            _BaseService.Dispose();
        }
    }
}
