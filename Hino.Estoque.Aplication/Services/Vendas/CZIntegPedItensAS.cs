using System.Threading.Tasks;
using Hino.Estoque.Aplication.Interfaces.Vendas;
using Hino.Estoque.Domain.Vendas.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Vendas;
using System.Linq;
using Hino.Estoque.Infra.Cross.Utils.Extensions;

namespace Hino.Estoque.Aplication.Services.Vendas
{
    public class CZIntegPedItensAS : BaseAppService<CZIntegPedItens>, ICZIntegPedItensAS
    {
        private readonly ICZIntegraPedidosService _ICZIntegraPedidosService;
        private readonly ICZIntegPedItensService _ICZIntegPedItensService;

        public CZIntegPedItensAS(ICZIntegPedItensService pCZIntegPedItensService,
            ICZIntegraPedidosService pICZIntegraPedidosService) : 
             base(pCZIntegPedItensService)
        {
            _ICZIntegraPedidosService = pICZIntegraPedidosService;
            _ICZIntegPedItensService = pCZIntegPedItensService;
        }

        public async Task<CZIntegPedItens> CreateAsync(long pIdIntegracao, CZIntegPedItens pOrderItem)
        {
            Errors.Clear();

            var OrderIntegraDb = (await _ICZIntegraPedidosService.QueryAsync(r => r.IdIntegracao == pIdIntegracao && !r.Integrado)).FirstOrDefault();

            if (OrderIntegraDb != null)
            {
                var OrderItemNew = new CZIntegPedItens();


                OrderItemNew.CopyProperties(pOrderItem);
                OrderItemNew.IdIntegracao = OrderIntegraDb.IdIntegracao;

                if ((OrderItemNew.Descricao ?? "").Length > 500)
                    OrderItemNew.Descricao = OrderItemNew.Descricao.Substring(0, 500);

                if ((OrderItemNew.ItemCliente ?? "").Length > 120)
                    OrderItemNew.ItemCliente = OrderItemNew.ItemCliente.Substring(0, 120);

                if ((OrderItemNew.ExtraInfo ?? "").Length > 500)
                    OrderItemNew.ExtraInfo = OrderItemNew.ExtraInfo.Substring(0, 500);

                if ((OrderItemNew.Tipo ?? "").Length > 120)
                    OrderItemNew.Tipo = OrderItemNew.Tipo.Substring(0, 120);

                if ((OrderItemNew.PedClie ?? "").Length > 120)
                    OrderItemNew.PedClie = OrderItemNew.PedClie.Substring(0, 120);

                if ((OrderItemNew.ItemClie ?? "").Length > 120)
                    OrderItemNew.ItemClie = OrderItemNew.ItemClie.Substring(0, 120);

                if ((OrderItemNew.CodHino ?? "").Length > 120)
                    OrderItemNew.CodHino = OrderItemNew.CodHino.Substring(0, 120);

                if ((await _ICZIntegPedItensService.QueryAsync(r =>
                                                         r.Pedido == OrderItemNew.Pedido &&
                                                         r.OrderIDItem == OrderItemNew.OrderIDItem)).Count() <= 0)
                {
                    OrderItemNew.IdItemIntegr = await _ICZIntegPedItensService.NextSequenceAsync();
                    _ICZIntegPedItensService.Add(OrderItemNew);
                    await _ICZIntegPedItensService.SaveChanges();

                    Errors = _ICZIntegPedItensService.Errors;
                }

                return OrderItemNew;
            }

            return pOrderItem;
        }
    }
}
