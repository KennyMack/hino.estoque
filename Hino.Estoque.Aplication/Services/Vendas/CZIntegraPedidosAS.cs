using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hino.Estoque.Aplication.Interfaces.Vendas;
using Hino.Estoque.Domain.Vendas.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Vendas;
using Hino.Estoque.Infra.Cross.Utils.Extensions;

namespace Hino.Estoque.Aplication.Services.Vendas
{
    public class CZIntegraPedidosAS : BaseAppService<CZIntegraPedidos>, ICZIntegraPedidosAS
    {
        private readonly ICZIntegraPedidosService _ICZIntegraPedidosService;

        public CZIntegraPedidosAS(ICZIntegraPedidosService pCZIntegraPedidosService) : 
             base(pCZIntegraPedidosService)
        {
            _ICZIntegraPedidosService = pCZIntegraPedidosService;
        }

        private CZIntegraPedidos ClearFields(CZIntegraPedidos pOrder)
        {

            if ((pOrder.Finalidade ?? "").Length > 120)
                pOrder.Finalidade = pOrder.Finalidade.Substring(0, 120);
            if ((pOrder.OrderIDClient ?? "").Length > 120)
                pOrder.OrderIDClient = pOrder.OrderIDClient.Substring(0, 120);
            if ((pOrder.Payment ?? "").Length > 120)
                pOrder.Payment = pOrder.Payment.Substring(0, 120);
            if ((pOrder.PaymentTerms ?? "").Length > 120)
                pOrder.PaymentTerms = pOrder.PaymentTerms.Substring(0, 120);
            if ((pOrder.Tipo ?? "").Length > 120)
                pOrder.Tipo = pOrder.Tipo.Substring(0, 120);
            if ((pOrder.Comprador ?? "").Length > 120)
                pOrder.Comprador = pOrder.Comprador.Substring(0, 120);
            if ((pOrder.OBS ?? "").Length > 255)
                pOrder.OBS = pOrder.OBS.Substring(0, 255);
            if ((pOrder.RespCancel ?? "").Length > 120)
                pOrder.RespCancel = pOrder.RespCancel.Substring(0, 120);
            if ((pOrder.Proposta ?? "").Length > 120)
                pOrder.Proposta = pOrder.Proposta.Substring(0, 120);
            if ((pOrder.Montador ?? "").Length > 120)
                pOrder.Montador = pOrder.Montador.Substring(0, 120);
            if ((pOrder.SPID ?? "").Length > 120)
                pOrder.SPID = pOrder.SPID.Substring(0, 120);


            return pOrder;
        }

        public async Task<CZIntegraPedidos> CreateAsync(CZIntegraPedidos pOrder)
        {
            Errors.Clear();

            var OrderDb = (await _ICZIntegraPedidosService.QueryAsync(r => r.OrderID == pOrder.OrderID)).FirstOrDefault();

            var OrderNew = OrderDb ?? new CZIntegraPedidos();

            OrderNew.CopyProperties(pOrder);
            OrderNew.PedIntegrado = false;

            if (OrderDb == null)
            {
                OrderNew.IdIntegracao = await _ICZIntegraPedidosService.NextSequenceAsync();
                OrderNew = ClearFields(OrderNew);
                _ICZIntegraPedidosService.Add(OrderNew);
            }
            else if (OrderDb.Cancelado)
            {
                OrderDb.CopyProperties(pOrder);
                OrderDb = ClearFields(OrderDb);
                OrderDb.PedIntegrado = false;
                OrderDb.IdIntegracao = await _ICZIntegraPedidosService.NextSequenceAsync();
                _ICZIntegraPedidosService.Add(OrderDb);
            }

            await _ICZIntegraPedidosService.SaveChanges();

            Errors = _ICZIntegraPedidosService.Errors;

            return OrderNew;
        }

        public async Task UpdateIntegratedAsync(CZIntegraPedidos pOrder)
        {
            Errors.Clear();

            var OrderDb = (await _ICZIntegraPedidosService.QueryAsync(r => r.IdIntegracao == pOrder.IdIntegracao)).FirstOrDefault();
            OrderDb.PedIntegrado = pOrder.PedIntegrado;
            OrderDb.DataIntegra = pOrder.DataIntegra;
            OrderDb.CodPedVenda = pOrder.CodPedVenda;
            _ICZIntegraPedidosService.Update(OrderDb);

            await _ICZIntegraPedidosService.SaveChanges();
        }

        public async Task<IEnumerable<CZIntegraPedidos>> GetDispIntegAsync() =>
            await _ICZIntegraPedidosService.QueryAsync(r => !r.PedIntegrado);
    }
}
