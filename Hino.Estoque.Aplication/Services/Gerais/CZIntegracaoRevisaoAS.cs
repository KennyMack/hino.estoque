﻿using Hino.Estoque.Aplication.Interfaces.Gerais;
using Hino.Estoque.Domain.Gerais.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Gerais;
using Hino.Estoque.Infra.Cross.Utils.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.Services.Gerais
{
    public class CZIntegracaoRevisaoAS : BaseAppService<CZIntegracaoRevisao>, ICZIntegracaoRevisaoAS
    {
        private readonly ICZIntegracaoRevisaoService _ICZIntegracaoRevisaoService;

        public CZIntegracaoRevisaoAS(ICZIntegracaoRevisaoService pICZIntegracaoRevisaoService) :
             base(pICZIntegracaoRevisaoService)
        {
            _ICZIntegracaoRevisaoService = pICZIntegracaoRevisaoService;
        }

        public async Task<CZIntegracaoRevisao> CreateAsync(CZIntegracaoRevisao pDraw)
        {
            Errors.Clear();

            var DrawDb = (await _ICZIntegracaoRevisaoService.
                QueryAsync(r => r.Desenho == pDraw.Desenho &&
                r.Revisao == pDraw.Revisao)).FirstOrDefault();

            var DrawNew = DrawDb ?? new CZIntegracaoRevisao();

            DrawNew.CopyProperties(pDraw);
            DrawNew.Integrado = false;

            if (DrawDb == null)
            {
                DrawNew.IdIntegracao = await _ICZIntegracaoRevisaoService.NextSequenceAsync();
                _ICZIntegracaoRevisaoService.Add(DrawNew);
                await _ICZIntegracaoRevisaoService.SaveChanges();
            }


            Errors = _ICZIntegracaoRevisaoService.Errors;

            return DrawNew;
        }

        public async Task UpdateIntegratedAsync(CZIntegracaoRevisao pDraw)
        {
            Errors.Clear();

            var EnterpriseDb = (await _ICZIntegracaoRevisaoService.QueryAsync(r => r.IdIntegracao == pDraw.IdIntegracao)).FirstOrDefault();
            EnterpriseDb.Integrado = pDraw.Integrado;
            EnterpriseDb.DataIntegracao = pDraw.DataIntegracao;
            _ICZIntegracaoRevisaoService.Update(EnterpriseDb);

            await _ICZIntegracaoRevisaoService.SaveChanges();
        }

        public async Task<IEnumerable<CZIntegracaoRevisao>> GetDispIntegAsync() =>
            await _ICZIntegracaoRevisaoService.QueryAsync(r => !r.Integrado);
    }
}
