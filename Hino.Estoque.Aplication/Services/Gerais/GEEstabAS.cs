using AutoMapper;
using Hino.Estoque.Aplication.Interfaces.Gerais;
using Hino.Estoque.Aplication.ViewModels.Gerais;
using Hino.Estoque.Domain.Gerais.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Gerais;
using System;
using System.Linq;
using System.Configuration;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.Services.Gerais
{
    public class GEEstabAS : BaseAppService<GEEstab>, IGEEstabAS
    {
        private readonly IGEEstabService _IGEEstabService;
        private readonly ICurrentSettingsAS _CurrentSettings;

        public GEEstabAS(IGEEstabService pIGEEstabService,
            ICurrentSettingsAS pCurrentSettings) : 
             base(pIGEEstabService)
        {
            _CurrentSettings = pCurrentSettings;
            _IGEEstabService = pIGEEstabService;
        }

        public async Task<GEEstabVM> GetEstabByIdAsync(short pCodEstab) =>
            Mapper.Map<GEEstab, GEEstabVM>(await _IGEEstabService.GetEstabById(pCodEstab));

        public async Task<GEEstabSettingsVM> GetEstabAndSettingsAsync(short pCodEstab)
        {
            var Settings = _CurrentSettings.GetSettings();
            Settings.Estab = await GetEstabByIdAsync(pCodEstab);

            return Settings;
        }

        public GEEstabSettingsVM GetCurrentSettings() => _CurrentSettings.GetSettings();
    }
}
