﻿using Hino.Estoque.Aplication.Interfaces.Gerais;
using Hino.Estoque.Domain.Gerais.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Gerais;
using Hino.Estoque.Infra.Cross.Utils.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.Services.Gerais
{
    public class CZIntegrEmpresaAS : BaseAppService<CZIntegrEmpresa>, ICZIntegrEmpresaAS
    {
        private readonly ICZIntegrEmpresaService _ICZIntegrEmpresaService;

        public CZIntegrEmpresaAS(ICZIntegrEmpresaService pICZIntegrEmpresaService) :
             base(pICZIntegrEmpresaService)
        {
            _ICZIntegrEmpresaService = pICZIntegrEmpresaService;
        }

        public async Task<CZIntegrEmpresa> CreateAsync(CZIntegrEmpresa pEnterprise)
        {
            Errors.Clear();

            var EnterpriseDb = (await _ICZIntegrEmpresaService.QueryAsync(r => r.CNPJCPF == pEnterprise.CNPJCPF)).FirstOrDefault();

            var EnterpriseNew = EnterpriseDb ?? new CZIntegrEmpresa();

            EnterpriseNew.CopyProperties(pEnterprise);
            EnterpriseNew.EmpIntegra = false;

            if (EnterpriseDb == null)
            {
                EnterpriseNew.IdIntegracao = await _ICZIntegrEmpresaService.NextSequenceAsync();
                _ICZIntegrEmpresaService.Add(EnterpriseNew);
                await _ICZIntegrEmpresaService.SaveChanges();
            }


            Errors = _ICZIntegrEmpresaService.Errors;

            return EnterpriseNew;
        }

        public async Task UpdateIntegratedAsync(CZIntegrEmpresa pEnterprise)
        {
            Errors.Clear();

            var EnterpriseDb = (await _ICZIntegrEmpresaService.QueryAsync(r => r.IdIntegracao == pEnterprise.IdIntegracao)).FirstOrDefault();
            EnterpriseDb.EmpIntegra = pEnterprise.EmpIntegra;
            EnterpriseDb.DataIntegra = pEnterprise.DataIntegra;
            EnterpriseDb.CodEmpresa = pEnterprise.CodEmpresa;
            _ICZIntegrEmpresaService.Update(EnterpriseDb);

            await _ICZIntegrEmpresaService.SaveChanges();
        }

        public async Task<IEnumerable<CZIntegrEmpresa>> GetDispIntegAsync() =>
            await _ICZIntegrEmpresaService.QueryAsync(r => !r.EmpIntegra);
    }
}
