using System;
using System.Configuration;
using System.Threading.Tasks;
using AutoMapper;
using Hino.Estoque.Aplication.Interfaces.Gerais;
using Hino.Estoque.Aplication.ViewModels.Gerais;
using Hino.Estoque.Domain.Gerais.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Gerais;
using Hino.Estoque.Infra.Cross.Utils;
using Hino.Estoque.Infra.Cross.Utils.Exceptions;

namespace Hino.Estoque.Aplication.Services.Gerais
{
    public class GEUsuariosAS : BaseAppService<GEUsuarios>, IGEUsuariosAS
    {
        private readonly IGEUsuariosService _IGEUsuariosService;
        private readonly IGEFuncionariosService _IGEFuncionariosService;
        private readonly IGEEstabService _IGEEstabService;
        private readonly ICurrentSettingsAS _CurrentSettings;

        public GEUsuariosAS(IGEUsuariosService pIGEUsuariosService,
            IGEFuncionariosService pIGEFuncionariosService,
            IGEEstabService pIGEEstabService,
            ICurrentSettingsAS pCurrentSettings) : 
             base(pIGEUsuariosService)
        {
            _CurrentSettings = pCurrentSettings;
            _IGEEstabService = pIGEEstabService;
            _IGEFuncionariosService = pIGEFuncionariosService;
            _IGEUsuariosService = pIGEUsuariosService;
        }

        public async Task<GEEstabSettingsVM> GetSettingsAsync(short pCodEstab)
        {
            var Settings = _CurrentSettings.GetSettings();
            Settings.Estab = Mapper.Map<GEEstab, GEEstabVM>(await _IGEEstabService.GetEstabById(pCodEstab));
            return Settings;
        }

        public async Task<GEFuncUsuariosVM> GetByIdentifier(short pCodEstab, string pSenha, string pIdentifier)
        {
            var Establishment = await _IGEEstabService.GetEstabById(pCodEstab);
            if (Establishment == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "CodEstab",
                    Value = pCodEstab.ToString(),
                    Messages = new string[] { Infra.Cross.Resources.MessagesResource.InvalidEstablishmentKeyOrNull }
                });

                return null;
            }

            var Employee = await _IGEFuncionariosService.GetByIdentifier(pCodEstab, pIdentifier);

            if (Employee == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "Identifier",
                    Value = pIdentifier,
                    Messages = new string[] { Infra.Cross.Resources.MessagesResource.AuthUserNotFound }
                });

                return null;
            }

            if (!Employee.status)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "status",
                    Value = "",
                    Messages = new string[] { Infra.Cross.Resources.MessagesResource.UserIsInactive }
                });

                return null;
            }

            var User = await _IGEUsuariosService.GetUserByUserName(Employee.codusuario);

            if (User == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "Identifier",
                    Value = pIdentifier,
                    Messages = new string[] { Infra.Cross.Resources.MessagesResource.AuthUserNotFound }
                });

                return null;
            }

            if (!User.status)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "status",
                    Value = "",
                    Messages = new string[] { Infra.Cross.Resources.MessagesResource.UserIsInactive }
                });

                return null;
            }

            var Settings = await GetSettingsAsync(pCodEstab);

            if (Settings.Settings.UsePassword)
            {
                if (Crypto.Encrypt(pSenha) != User.senha)
                {
                    Errors.Add(new ModelException
                    {
                        ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                        Field = "senha",
                        Value = "",
                        Messages = new string[] { Infra.Cross.Resources.MessagesResource.InvalidPassword }
                    });

                    return null;
                }
            }

            User.senha = "";

            return new GEFuncUsuariosVM
            {
                GEFuncionarios = Mapper.Map<GEFuncionarios, GEFuncionariosVM>(Employee),
                GEUsuarios = Mapper.Map<GEUsuarios, GEUsuariosVM>(User),
                GESettings = Settings
            };
        }
    }
}
