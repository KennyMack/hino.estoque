using Hino.Estoque.Aplication.Interfaces.Gerais;
using Hino.Estoque.Domain.Gerais.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Gerais;

namespace Hino.Estoque.Aplication.Services.Gerais
{
    public class GELogAS : BaseAppService<GELog>, IGELogAS
    {
        private readonly IGELogService _IGELogService;

        public GELogAS(IGELogService pIGELogService) : 
             base(pIGELogService)
        {
            _IGELogService = pIGELogService;
        }
    }
}
