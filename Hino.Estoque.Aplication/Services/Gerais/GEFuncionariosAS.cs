using Hino.Estoque.Aplication.Interfaces.Gerais;
using Hino.Estoque.Domain.Gerais.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Gerais;

namespace Hino.Estoque.Aplication.Services.Gerais
{
    public class GEFuncionariosAS : BaseAppService<GEFuncionarios>, IGEFuncionariosAS
    {
        private readonly IGEFuncionariosService _IGEFuncionariosService;

        public GEFuncionariosAS(IGEFuncionariosService pIGEFuncionariosService) : 
             base(pIGEFuncionariosService)
        {
            _IGEFuncionariosService = pIGEFuncionariosService;
        }
    }
}
