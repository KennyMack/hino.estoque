using Hino.Estoque.Aplication.Interfaces.Gerais;
using Hino.Estoque.Domain.Gerais.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Gerais;

namespace Hino.Estoque.Aplication.Services.Gerais
{
    public class GELogDetAS : BaseAppService<GELogDet>, IGELogDetAS
    {
        private readonly IGELogDetService _IGELogDetService;

        public GELogDetAS(IGELogDetService pIGELogDetService) : 
             base(pIGELogDetService)
        {
            _IGELogDetService = pIGELogDetService;
        }
    }
}
