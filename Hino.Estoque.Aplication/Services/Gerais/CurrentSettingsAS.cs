﻿using Hino.Estoque.Aplication.Interfaces.Gerais;
using Hino.Estoque.Aplication.ViewModels.Gerais;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.Services.Gerais
{
    public class CurrentSettingsAS : ICurrentSettingsAS
    {
        public GEEstabSettingsVM GetSettings()
        {
            var Settings = new GEEstabSettingsVM();

            try
            {
                Settings.Settings.DefaultOriginStock = Convert.ToString(ConfigurationManager.AppSettings.Get("DefaultOriginStock") ?? "");
            }
            catch (Exception)
            {
                Settings.Settings.DefaultOriginStock = "";
            }
            try
            {
                Settings.Settings.DefaultStock = Convert.ToString(ConfigurationManager.AppSettings.Get("DefaultStock") ?? "");
            }
            catch (Exception)
            {
                Settings.Settings.DefaultStock = "";
            }
            try
            {
                Settings.Settings.LoadQtd = Convert.ToBoolean(ConfigurationManager.AppSettings.Get("LoadQtd") ?? "false");
            }
            catch (Exception)
            {
                Settings.Settings.LoadQtd = false;
            }
            try
            {
                Settings.Settings.ShowResultsOP = Convert.ToBoolean(ConfigurationManager.AppSettings.Get("ShowResultsOP") ?? "false");
            }
            catch (Exception)
            {
                Settings.Settings.ShowResultsOP = false;
            }
            try
            {
                Settings.Settings.AskStartNew = Convert.ToBoolean(ConfigurationManager.AppSettings.Get("AskStartNew") ?? "false");
            }
            catch (Exception)
            {
                Settings.Settings.AskStartNew = false;
            }
            try
            {
                Settings.Settings.AllowTransfMoreThanOp = Convert.ToBoolean(ConfigurationManager.AppSettings.Get("AllowTransfMoreThanOp") ?? "false");
            }
            catch (Exception)
            {
                Settings.Settings.AllowTransfMoreThanOp = false;
            }
            try
            {
                Settings.Settings.KeepTransfLocations = Convert.ToBoolean(ConfigurationManager.AppSettings.Get("KeepTransfLocations") ?? "false");
            }
            catch (Exception)
            {
                Settings.Settings.KeepTransfLocations = false;
            }
            try
            {
                Settings.Settings.AllowChangeStockOrigin = Convert.ToBoolean(ConfigurationManager.AppSettings.Get("AllowChangeStockOrigin") ?? "false");
            }
            catch (Exception)
            {
                Settings.Settings.AllowChangeStockOrigin = false;
            }
            try
            {
                Settings.Settings.UsePassword = Convert.ToBoolean(ConfigurationManager.AppSettings.Get("UsePassword") ?? "false");
            }
            catch (Exception)
            {
                Settings.Settings.UsePassword = false;
            }
            try
            {
                Settings.Settings.QuantityManual = Convert.ToBoolean(ConfigurationManager.AppSettings.Get("QuantityManual") ?? "false");
            }
            catch (Exception)
            {
                Settings.Settings.QuantityManual = false;
            }
            try
            {
                Settings.Settings.UseStockOriginSuggested = Convert.ToBoolean(ConfigurationManager.AppSettings.Get("UseStockOriginSugested") ?? "false");
            }
            catch (Exception)
            {
                Settings.Settings.UseStockOriginSuggested = false;
            }
            try
            {
                Settings.Settings.MachineOnStartAppointment = Convert.ToBoolean(ConfigurationManager.AppSettings.Get("MachineOnStartAppointment") ?? "false");
            }
            catch (Exception)
            {
                Settings.Settings.MachineOnStartAppointment = false;
            }

            return Settings;
        }
    }
}
