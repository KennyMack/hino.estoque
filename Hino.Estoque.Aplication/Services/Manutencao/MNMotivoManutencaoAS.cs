﻿using System;
using Hino.Estoque.Aplication.Interfaces.Manutencao;
using Hino.Estoque.Domain.Manutencao.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Manutencao;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.Services.Manutencao
{
    public class MNMotivoManutencaoAS : BaseAppService<MNMotivo>, IMNMotivoManutencaoAS
    {
        private readonly IMNMotivoManutencaoService _IMNMotivoManutencaoService;

        public MNMotivoManutencaoAS(IMNMotivoManutencaoService pIMNMotivoManutencaoService) :
             base(pIMNMotivoManutencaoService)
        {
            _IMNMotivoManutencaoService = pIMNMotivoManutencaoService;
        }
    }
}
