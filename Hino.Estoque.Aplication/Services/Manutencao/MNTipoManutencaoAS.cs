﻿using Hino.Estoque.Aplication.Interfaces.Manutencao;
using Hino.Estoque.Domain.Manutencao.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Manutencao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.Services.Manutencao
{
    public class MNTipoManutencaoAS : BaseAppService<MNTipoManut>, IMNTipoManutencaoAS
    {
        private readonly IMNTipoManutencaoService _IMNTipoManutencaoService;

        public MNTipoManutencaoAS(IMNTipoManutencaoService pIMNTipoManutencaoService) :
             base(pIMNTipoManutencaoService)
        {
            _IMNTipoManutencaoService = pIMNTipoManutencaoService;
        }
    }
}
