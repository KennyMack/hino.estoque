﻿using System;
using Hino.Estoque.Aplication.Interfaces.Manutencao;
using Hino.Estoque.Domain.Manutencao.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Manutencao;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hino.Estoque.Domain.Producao.Interfaces.Services;

namespace Hino.Estoque.Aplication.Services.Manutencao
{
    public class MNEquipamentoAS : BaseAppService<MNEquipamento>, IMNEquipamentoAS
    {
        private readonly IMNEquipamentoService _IMNEquipamentoService;

        public MNEquipamentoAS(IMNEquipamentoService pIMNEquipamentoService) :
             base(pIMNEquipamentoService)
        {
            _IMNEquipamentoService = pIMNEquipamentoService;
        }
    }
}
