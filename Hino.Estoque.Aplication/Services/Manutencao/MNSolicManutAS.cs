﻿using System;
using Hino.Estoque.Aplication.Interfaces.Manutencao;
using Hino.Estoque.Domain.Manutencao.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Manutencao;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Hino.Estoque.Aplication.ViewModels.Manutencao;
using Hino.Estoque.Domain.Gerais.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Utils;
using Hino.Estoque.Infra.Cross.Utils.Exceptions;
using Hino.Estoque.Infra.Cross.Entities.Gerais;

namespace Hino.Estoque.Aplication.Services.Manutencao
{
    public class MNSolicManutAS : BaseAppService<MNSolicManut>, IMNSolicManutAS
    {
        private readonly IMNSolicManutService _IMNSolicManutService;
        private readonly IMNMotivoManutencaoService _IMNMotivoManutencaoService;
        private readonly IMNTipoManutencaoService _IMNTipoManutencaoService;
        private readonly IMNEquipamentoService _IMNEquipamentoService;
        private readonly IGEFuncionariosService _IGEFuncionariosService;

        public MNSolicManutAS(IMNSolicManutService pIMNSolicManutService,
            IMNMotivoManutencaoService pIMNMotivoManutencaoService,
            IMNTipoManutencaoService pIMNTipoManutencaoService,
            IMNEquipamentoService pIMNEquipamentoService,
            IGEFuncionariosService pIGEFuncionariosService) :
             base(pIMNSolicManutService)
        {
            _IMNTipoManutencaoService = pIMNTipoManutencaoService;
            _IMNEquipamentoService = pIMNEquipamentoService;
            _IGEFuncionariosService = pIGEFuncionariosService;
            _IMNMotivoManutencaoService = pIMNMotivoManutencaoService;
            _IMNSolicManutService = pIMNSolicManutService;
        }

        async Task<GEFuncionarios> GetFuncionarioAsync(short pCodEstab, int pCodFuncionario)
        {
            var Funcionario = (await _IGEFuncionariosService.QueryAsync(r =>
            r.codestab == pCodEstab &&
            r.codfuncionario == pCodFuncionario)).FirstOrDefault();

            if (Funcionario == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "CodFuncionario",
                    Value = pCodFuncionario.ToString(),
                    Messages = new string[] { Infra.Cross.Resources.MessagesResource.EmployeeNotFound }
                });
                return null;
            }

            if (!Funcionario.status)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "CodFuncionario",
                    Value = pCodFuncionario.ToString(),
                    Messages = new string[] { Infra.Cross.Resources.MessagesResource.InactiveEmployee }
                });
                return null;
            }
            return Funcionario;
        }

        public async Task<MNSolicManutVM> AlterarStatusSolicManutAsync(MNStatusSolicManutVM pStatus)
        {
            Errors.Clear();

            var Funcionario = await GetFuncionarioAsync(pStatus.codestab, pStatus.codfuncionario);

            if (Errors.Any())
                return null;

            var SolicManut = await _IMNSolicManutService.AlterarStatusSolicManutAsync(pStatus.codestab, pStatus.codsolic, pStatus.status, Funcionario.codusuario);

            if (SolicManut == null || _IMNSolicManutService.Errors.Any())
            {
                Errors = _IMNSolicManutService.Errors;
                return null;
            }

            return Mapper.Map<MNSolicManut, MNSolicManutVM>(SolicManut);
        }

        public async Task<MNSolicManutVM> CreateAsync(MNCreateSolicManutVM pSolicManut)
        {
            Errors.Clear();

            var CodUsuarioPadrao = await _IMNSolicManutService.BuscaAprovadorPadraoAsync(pSolicManut.codestab);

            if (CodUsuarioPadrao.IsEmpty())
            {
                Errors = _IMNSolicManutService.Errors;
                return null;
            }

            var Funcionario = await GetFuncionarioAsync(pSolicManut.codestab, pSolicManut.codfuncionario);
            if (Errors.Any())
                return null;

            var Equipamento = (await _IMNEquipamentoService.QueryAsync(r =>
                r.codestab == pSolicManut.codestab &&
                r.codequipamento == pSolicManut.codequipamento)).FirstOrDefault();

            if (Equipamento == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "CodEquipamento",
                    Value = pSolicManut.codfuncionario.ToString(),
                    Messages = new string[] { Infra.Cross.Resources.MessagesResource.EquipmentNotFound }
                });
                return null;
            }

            if (Equipamento.status <= 0)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "CodEquipamento",
                    Value = pSolicManut.codfuncionario.ToString(),
                    Messages = new string[] { Infra.Cross.Resources.MessagesResource.InactiveEquipment }
                });
                return null;
            }

            var MotivoManut = (await _IMNMotivoManutencaoService.QueryAsync(r =>
                r.codmotivo == pSolicManut.codmotivo)).FirstOrDefault();

            if (MotivoManut == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "CodMotivo",
                    Value = pSolicManut.codmotivo.ToString(),
                    Messages = new string[] { Infra.Cross.Resources.MessagesResource.ReasonIdNotFound }
                });
                return null;
            }

            var TipoManutencao = (await _IMNTipoManutencaoService.QueryAsync(r =>
                r.codtipomanut == pSolicManut.codtipomanut)).FirstOrDefault();

            if (TipoManutencao == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "CodTipoManut",
                    Value = pSolicManut.codtipomanut,
                    Messages = new string[] { Infra.Cross.Resources.MessagesResource.TypeManutNotFound }
                });
                return null;
            }

            var SolicManutNova = new MNSolicManut
            {
                codsolic = Convert.ToInt32(await _IMNSolicManutService.NextSequenceAsync()),
                codestab = pSolicManut.codestab,
                codequipamento = pSolicManut.codequipamento,
                datasolic = DateTime.Now,
                status = "P",
                codmotivo = pSolicManut.codmotivo,
                observacao = pSolicManut.observacao,
                solicitante = Funcionario.codusuario,
                aprovador = CodUsuarioPadrao,
                prioridade = pSolicManut.prioridade,
                codtipomanut = pSolicManut.codtipomanut
            };

            _IMNSolicManutService.Add(SolicManutNova);
            await _IMNSolicManutService.SaveChanges();

            if (_IMNSolicManutService.Errors.Any())
            {
                Errors = _IMNSolicManutService.Errors;
                return null;
            }

            return Mapper.Map<MNSolicManut, MNSolicManutVM>((await _IMNSolicManutService.QueryAsync(r =>
                r.codestab == SolicManutNova.codestab &&
                r.codsolic == SolicManutNova.codsolic,
                s => s.Equipamento,
                s => s.TipoManut,
                r => r.Motivo)).FirstOrDefault() ?? SolicManutNova);
        }

        public async Task<MNSearchManutVM> SearchManutAsync(short pCodEstab)
        {
            var motivos = (await _IMNMotivoManutencaoService.GetAllAsync()).ToArray();
            var TipoManut = (await _IMNTipoManutencaoService.GetAllAsync()).ToArray();
            var Equipamentos = (await _IMNEquipamentoService.QueryAsync(r =>
                     r.codestab == pCodEstab &&
                     r.status == 1)).ToArray();

            return new MNSearchManutVM
            {
                Motivos = Mapper.Map<MNMotivoVM[]>(motivos),
                TipoManut = Mapper.Map<MNTipoManutVM[]>(TipoManut),
                Equipamentos = Mapper.Map<MNEquipamentoVM[]>(Equipamentos)
            };
        }
    }
}
