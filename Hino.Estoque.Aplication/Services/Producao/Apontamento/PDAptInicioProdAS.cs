﻿using AutoMapper;
using Hino.Estoque.Aplication.Interfaces.Gerais;
using Hino.Estoque.Aplication.Interfaces.Producao.Apontamento;
using Hino.Estoque.Aplication.ViewModels.Producao.Apontamento;
using Hino.Estoque.Domain.Gerais.Interfaces.Services;
using Hino.Estoque.Domain.Producao.Interfaces.Services;
using Hino.Estoque.Domain.Producao.Interfaces.Services.Apontamento;
using Hino.Estoque.Infra.Cross.Entities.Producao;
using Hino.Estoque.Infra.Cross.Entities.Producao.Apontamento;
using Hino.Estoque.Infra.Cross.Utils;
using Hino.Estoque.Infra.Cross.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.Services.Producao.Apontamento
{
    public class PDAptInicioProdAS : BaseAppService<PDAptInicioProd>, IPDAptInicioProdAS
    {
        private readonly IPDAptInicioProdService _IPDAptInicioProdService;
        private readonly IPDOrdemProdService _IPDOrdemProdService;
        private readonly IPDLancamentosService _IPDLancamentosService;
        private readonly IPDOrdemProdRotinasService _IPDOrdemProdRotinasService;
        private readonly IGEFuncionariosService _IGEFuncionariosService;
        private readonly IPDParamEstabService _IPDParamEstabService;
        private readonly IGEEstabAS _IGEEstabAS;

        public PDAptInicioProdAS(IPDAptInicioProdService pIPDAptInicioProdService,
            IPDOrdemProdService pIPDOrdemProdService,
            IPDLancamentosService pIPDLancamentosService,
            IPDOrdemProdRotinasService pIPDOrdemProdRotinasService,
            IGEFuncionariosService pIGEFuncionariosService,
            IPDParamEstabService pIPDParamEstabService,
            IGEEstabAS pIGEEstabAS) :
             base(pIPDAptInicioProdService)
        {
            _IGEEstabAS = pIGEEstabAS;
            _IPDParamEstabService = pIPDParamEstabService;
            _IPDOrdemProdService = pIPDOrdemProdService;
            _IPDAptInicioProdService = pIPDAptInicioProdService;
            _IPDLancamentosService = pIPDLancamentosService;
            _IPDOrdemProdRotinasService = pIPDOrdemProdRotinasService;
            _IGEFuncionariosService = pIGEFuncionariosService;
        }

        async Task<bool> VerificaInicio(PDAptInicioProd pAptIniProd)
        {
            if (pAptIniProd.tipo == 3)
            {
                var Settings = _IGEEstabAS.GetCurrentSettings();
                var ParamEstab = (await _IPDParamEstabService.QueryAsync(r => r.codestab == pAptIniProd.codestab)).FirstOrDefault();

                if (Settings.Settings.MachineOnStartAppointment &&
                    ParamEstab.exigemaqapto &&
                    pAptIniProd.codmaquina.IsEmpty())
                {
                    Errors.Add(new ModelException
                    {
                        ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                        Field = "operacao",
                        Value = pAptIniProd.operacao.ToString(),
                        Messages = new string[] { Infra.Cross.Resources.MessagesResource.MachineIsRequired }
                    });
                    return false;
                }
            }

            var operacao = await _IPDOrdemProdRotinasService.QueryAsync(r => r.codestab == pAptIniProd.codestab &&
                r.codordprod == pAptIniProd.codordprod &&
                r.nivelordprod == pAptIniProd.nivelordprod &&
                r.codestrutura == pAptIniProd.codestrutura &&
                r.codroteiro == pAptIniProd.codroteiro &&
                r.operacao == pAptIniProd.operacao);

            if (!operacao.Any())
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "operacao",
                    Value = pAptIniProd.operacao.ToString(),
                    Messages = new string[] { Infra.Cross.Resources.MessagesResource.OperationNotFound }
                });
                return false;
            }

            if (!operacao.First().aptoprocesso && !operacao.First().aptoacabado)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "operacao",
                    Value = pAptIniProd.operacao.ToString(),
                    Messages = new string[] { Infra.Cross.Resources.MessagesResource.OperationDoesNotAllowAppointment }
                });
                return false;
            }

            var funcionario = await _IGEFuncionariosService.QueryAsync(r => r.codestab == pAptIniProd.codestab &&
                r.codfuncionario == pAptIniProd.codfuncionario);

            if (funcionario.Any())
            {
                if (!funcionario.First().status)
                {
                    Errors.Add(new ModelException
                    {
                        ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                        Field = "codfuncionario",
                        Value = pAptIniProd.codfuncionario.ToString(),
                        Messages = new string[] { Infra.Cross.Resources.MessagesResource.InactiveEmployee }
                    });
                    return false;
                }
            }
            else
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "codfuncionario",
                    Value = pAptIniProd.codfuncionario.ToString(),
                    Messages = new string[] { Infra.Cross.Resources.MessagesResource.EmployeeNotFound }
                });
                return false;
            }

            if (await _IPDAptInicioProdService.OperacaoJaIniciadaAsync(pAptIniProd.codestab, pAptIniProd))
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "operacao",
                    Value = pAptIniProd.operacao.ToString(),
                    Messages = new string[] { Infra.Cross.Resources.MessagesResource.OperationAlreadyInitiated }
                });
                return false;
            }
            else if (await _IPDAptInicioProdService.ExisteInspecaoRotinaAnteriorAsync(pAptIniProd.codestab, pAptIniProd))
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "operacao",
                    Value = pAptIniProd.operacao.ToString(),
                    Messages = new string[] { Infra.Cross.Resources.MessagesResource.ThereIsRoutineInspection }
                });
                return false;
            }

            return true;
        }

        public async Task<PDAptInicioProdVM> IniciarProducaoAsync(short pCodEstab, PDAptInicioProdCreateVM pAptIniCreate)
        {
            var IniProd = new PDAptInicioProd
            {
                codestab = pAptIniCreate.codestab,
                codestrutura = pAptIniCreate.codestrutura,
                codordprod = pAptIniCreate.codordprod,
                nivelordprod = pAptIniCreate.nivelordprod,
                codroteiro = pAptIniCreate.codroteiro,
                codusuario = pAptIniCreate.codusuario,
                operacao = pAptIniCreate.operacao,
                tipo = pAptIniCreate.tipo,
                codfuncionario = pAptIniCreate.codfuncionario,
                codmotivo = pAptIniCreate.codmotivo,
                codmaquina = pAptIniCreate.codmaquina
            };

            await VerificaInicio(IniProd);

            if (Errors.Any())
                return null;

            IniProd.codiniapt = await _IPDAptInicioProdService.NextSequenceAsync();
            IniProd.dtinicio = DateTime.Now;
            IniProd.tipo = 3;

            _IPDAptInicioProdService.Add(IniProd);

            if (!_IPDAptInicioProdService.Errors.Any())
                await _IPDAptInicioProdService.SaveChanges();

            return _IPDAptInicioProdService.Errors.Any() ? null : Mapper.Map<PDAptInicioProd, PDAptInicioProdVM>(IniProd);
        }

        public async Task<bool> OperacaoJaIniciadaAsync(short pCodEstab, PDAptInicioProdCreateVM pAptIniCreate) =>
           await _IPDAptInicioProdService.OperacaoJaIniciadaAsync(pCodEstab, new PDAptInicioProd
            {
                codestab = pCodEstab,
                codestrutura = pAptIniCreate.codestrutura,
                codordprod = pAptIniCreate.codordprod,
                nivelordprod = pAptIniCreate.nivelordprod,
                codroteiro = pAptIniCreate.codroteiro,
                codusuario = pAptIniCreate.codusuario,
                operacao = pAptIniCreate.operacao,
                tipo = pAptIniCreate.tipo,
                codfuncionario = pAptIniCreate.codfuncionario
            });

        public async Task<bool> ExisteInspecaoRotinaAnteriorAsync(short pCodEstab, PDAptInicioProdCreateVM pAptIniCreate) =>
           await _IPDAptInicioProdService.ExisteInspecaoRotinaAnteriorAsync(pCodEstab, new PDAptInicioProd
           {
               codestab = pCodEstab,
               codestrutura = pAptIniCreate.codestrutura,
               codordprod = pAptIniCreate.codordprod,
               nivelordprod = pAptIniCreate.nivelordprod,
               codroteiro = pAptIniCreate.codroteiro,
               codusuario = pAptIniCreate.codusuario,
               operacao = pAptIniCreate.operacao,
               tipo = pAptIniCreate.tipo,
               codfuncionario = pAptIniCreate.codfuncionario
           });

        public async Task<PDAptInicioProd> GetLastAptStartAsync(
            short pCodEstab, int pCodFuncionario, long pCodOrdProd, 
            string pNivelOrdProd, long pCodEstrutura, int pCodRoteiro, 
            int pOperacao, short pTipo) =>
            await _IPDAptInicioProdService.GetLastAptStartAsync(
                    pCodEstab, pCodFuncionario, pCodOrdProd,
                    pNivelOrdProd, pCodEstrutura, pCodRoteiro,
                    pOperacao, pTipo);

        public async Task<PDAptInicioProdVM> IniciarParadaAsync(short pCodEstab, PDAptInicioProdCreateVM pAptIniCreate)
        {
            var IniProd = new PDAptInicioProd
            {
                codestab = pAptIniCreate.codestab,
                codestrutura = pAptIniCreate.codestrutura,
                codordprod = pAptIniCreate.codordprod,
                nivelordprod = pAptIniCreate.nivelordprod,
                codroteiro = pAptIniCreate.codroteiro,
                codusuario = pAptIniCreate.codusuario,
                operacao = pAptIniCreate.operacao,
                tipo = pAptIniCreate.tipo,
                codfuncionario = pAptIniCreate.codfuncionario,
                codmotivo = pAptIniCreate.codmotivo
            };

            await VerificaInicio(IniProd);

            if (Errors.Any())
                return null;

            IniProd.codiniapt = await _IPDAptInicioProdService.NextSequenceAsync();
            IniProd.dtinicio = DateTime.Now;
            IniProd.tipo = 7;

            _IPDAptInicioProdService.Add(IniProd);

            if (!_IPDAptInicioProdService.Errors.Any())
                await _IPDAptInicioProdService.SaveChanges();

            return _IPDAptInicioProdService.Errors.Any() ? null : Mapper.Map<PDAptInicioProd, PDAptInicioProdVM>(IniProd);
        }
    }
}
