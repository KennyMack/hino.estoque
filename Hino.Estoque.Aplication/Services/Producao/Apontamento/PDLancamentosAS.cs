using Hino.Estoque.Aplication.Interfaces.Producao;
using Hino.Estoque.Aplication.Interfaces.Producao.Apontamento;
using Hino.Estoque.Domain.Producao.Interfaces.Services;
using Hino.Estoque.Domain.Producao.Interfaces.Services.Apontamento;
using Hino.Estoque.Infra.Cross.Entities.Producao;
using Hino.Estoque.Infra.Cross.Entities.Producao.Apontamento;

namespace Hino.Estoque.Aplication.Services.Producao.Apontamento
{
    public class PDLancamentosAS : BaseAppService<PDLancamentos>, IPDLancamentosAS
    {
        private readonly IPDLancamentosService _IPDLancamentosService;

        public PDLancamentosAS(IPDLancamentosService pIPDLancamentosService) : 
             base(pIPDLancamentosService)
        {
            _IPDLancamentosService = pIPDLancamentosService;
        }
    }
}
