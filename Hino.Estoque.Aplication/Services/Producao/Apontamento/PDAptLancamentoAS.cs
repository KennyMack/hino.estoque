﻿using Hino.Estoque.Aplication.Interfaces.Producao.Apontamento;
using Hino.Estoque.Domain.Producao.Interfaces.Services.Apontamento;
using Hino.Estoque.Infra.Cross.Entities.Producao.Apontamento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.Services.Producao.Apontamento
{
    public class PDAptLancamentoAS : BaseAppService<PDAptLancamento>, IPDAptLancamentoAS
    {
        private readonly IPDAptLancamentoService _IPDAptLancamentoService;

        public PDAptLancamentoAS(IPDAptLancamentoService pIPDAptLancamentoService) :
             base(pIPDAptLancamentoService)
        {
            _IPDAptLancamentoService = pIPDAptLancamentoService;
        }
    }
}
