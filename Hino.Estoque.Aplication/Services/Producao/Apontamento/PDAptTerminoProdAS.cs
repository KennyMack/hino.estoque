﻿using AutoMapper;
using Hino.Estoque.Aplication.Interfaces.Producao.Apontamento;
using Hino.Estoque.Aplication.ViewModels.Producao.Apontamento;
using Hino.Estoque.Domain.Fiscal.Interfaces.Services;
using Hino.Estoque.Domain.Gerais.Interfaces.Services;
using Hino.Estoque.Domain.Producao.Interfaces.Services;
using Hino.Estoque.Domain.Producao.Interfaces.Services.Apontamento;
using Hino.Estoque.Infra.Cross.Entities.Producao.Apontamento;
using Hino.Estoque.Infra.Cross.Utils;
using Hino.Estoque.Infra.Cross.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.Services.Producao.Apontamento
{
    public class PDAptTerminoProdAS : BaseAppService<PDAptTerminoProd>, IPDAptTerminoProdAS
    {
        private readonly IPDAptTerminoProdService _IPDAptTerminoProdService;
        private readonly IPDAptInicioProdService _IPDAptInicioProdService;
        private readonly IGEFuncionariosService _IGEFuncionariosService;
        private readonly IPDMotivosService _IPDMotivosService;
        private readonly IPDLancamentosService _IPDLancamentosService;
        private readonly IPDOrdemProdService _IPDOrdemProdService;
        private readonly IPDOrdemProdRotinasService _IPDOrdemProdRotinasService;
        private readonly IFSProdutoPcpService _IFSProdutoPcpService;
        private readonly IPDAptLancamentoService _IPDAptLancamentoService;
        private readonly IPDParadasService _IPDParadasService;
        private readonly IPDAptParadasService _IPDAptParadasService;
        private readonly IPDParamEstabService _IPDParamEstabService;

        public PDAptTerminoProdAS(IPDAptTerminoProdService pIPDAptTerminoProdService,
            IGEFuncionariosService pIGEFuncionariosService,
            IPDAptInicioProdService pIPDAptInicioProdService,
            IPDMotivosService pIPDMotivosService,
            IPDLancamentosService pIPDLancamentosService,
            IPDOrdemProdService pIPDOrdemProdService,
            IFSProdutoPcpService pIFSProdutoPcpService,
            IPDAptLancamentoService pIPDAptLancamentoService,
            IPDOrdemProdRotinasService pIPDOrdemProdRotinasService,
            IPDParadasService pIPDParadasService,
            IPDAptParadasService pIPDAptParadasService,
            IPDParamEstabService pIPDParamEstabService) :
             base(pIPDAptTerminoProdService)
        {
            _IPDOrdemProdRotinasService = pIPDOrdemProdRotinasService;
            _IPDAptLancamentoService = pIPDAptLancamentoService;
            _IFSProdutoPcpService = pIFSProdutoPcpService;
            _IPDOrdemProdService = pIPDOrdemProdService;
            _IPDLancamentosService = pIPDLancamentosService;
            _IPDMotivosService = pIPDMotivosService;
            _IPDAptTerminoProdService = pIPDAptTerminoProdService;
            _IGEFuncionariosService = pIGEFuncionariosService;
            _IPDAptInicioProdService = pIPDAptInicioProdService;
            _IPDParadasService = pIPDParadasService;
            _IPDAptParadasService = pIPDAptParadasService;
            _IPDParamEstabService = pIPDParamEstabService;
        }

        async Task<PDAptInicioProd> GetInicioAsync(short pCodEstab, long pCodIniProd, int pCodFuncionario, short pTipo) =>
            (await _IPDAptInicioProdService.QueryAsync(r => 
                r.codestab == pCodEstab &&
                r.codiniapt == pCodIniProd &&
                r.codfuncionario == pCodFuncionario &&
                r.tipo == pTipo// 3
            )).FirstOrDefault();

        async Task<bool> WasFinishedAsync(PDAptInicioProd pInicioProd) =>
            (await _IPDAptTerminoProdService.QueryAsync(r => r.codestab == pInicioProd.codestab && r.codiniapt == pInicioProd.codiniapt)).Any();

        async Task<bool> ValidateDataAsync(PDAptTerminoProd pTerminoProd, PDAptInicioProd pInicioProd)
        {
            if (pInicioProd == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "codiniapt",
                    Value = "",
                    Messages = new string[] { Infra.Cross.Resources.MessagesResource.OperationIsNotStartedYet }
                });
                return false;
            }

            if (await WasFinishedAsync(pInicioProd))
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "codiniapt",
                    Value = pInicioProd.codiniapt.ToString(),
                    Messages = new string[] { Infra.Cross.Resources.MessagesResource.OperationAlreadyFinished }
                });
                return false;
            }

            if (pTerminoProd.quantidade < 0 && pTerminoProd.tipo == 4)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "codiniapt",
                    Value = pInicioProd.codiniapt.ToString(),
                    Messages = new string[] { Infra.Cross.Resources.MessagesResource.OperationAlreadyFinished }
                });
                return false;
            }

            return true;
        }

        public async Task<PDAptTerminoProdVM> TerminarProducaoAsync(short pCodEstab, PDAptTerminoProdCreateVM pAptTermCreate)
        {
            var rand = new Random();
            
            var TerminoProd = new PDAptTerminoProd
            {
                codestab = pCodEstab,
                codiniapt = pAptTermCreate.codiniapt,
                codusuario = pAptTermCreate.codusuario,
                codfuncionario = pAptTermCreate.codfuncionario,
                tipo = pAptTermCreate.tipo,
                quantidade = pAptTermCreate.quantidade,
                qtdrefugo = pAptTermCreate.Refugos.Sum(r => r.quantidade),
                qtdretrabalho = pAptTermCreate.qtdretrabalho,
                observacao = pAptTermCreate.observacao,
                dttermino = DateTime.Now
            };

            var InicioProd = await GetInicioAsync(pCodEstab, pAptTermCreate.codiniapt, pAptTermCreate.codfuncionario, 3);

            await ValidateDataAsync(TerminoProd, InicioProd);

            if (!Errors.Any())
            {
                foreach (var item in pAptTermCreate.Refugos)
                {
                    if (!(await _IPDMotivosService.QueryAsync(r => r.codmotivo == item.codmotivo && r.tipo == 1 && r.status)).Any())
                    {
                        Errors.Add(new ModelException
                        {
                            ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                            Field = "Refugos.codmotivo",
                            Value = item.codmotivo.ToString(),
                            Messages = new string[] { Infra.Cross.Resources.MessagesResource.ReasonIdNotFound }
                        });
                        break;
                    }
                }
            }

            if (Errors.Any())
                return null;

            var ParamEstab = (await _IPDParamEstabService.QueryAsync(r => r.codestab == pCodEstab)).FirstOrDefault();

            var Lancamento = new PDLancamentos
            {
                codestab = pCodEstab,
                codlancamento = rand.Next(),
                codordprod = InicioProd.codordprod,
                nivelordprod = InicioProd.nivelordprod,
                operacao = InicioProd.operacao,
                codfuncionario = TerminoProd.codfuncionario,
                codusuario = TerminoProd.codusuario,
                quantidade = TerminoProd.quantidade + TerminoProd.qtdrefugo,
                dtinicio = InicioProd.dtinicio,
                dttermino = TerminoProd.dttermino,
                codestrutura = InicioProd.codestrutura,
                codroteiro = InicioProd.codroteiro,
                turno = "",
                codmaquina = InicioProd.codmaquina
            };

            if (ParamEstab.exigemaqapto && Lancamento.codmaquina.IsEmpty())
            {
                var rotinas = await _IPDOrdemProdRotinasService.QueryAsync(r =>
                     r.codestab == pCodEstab &&
                     r.codordprod == Lancamento.codordprod &&
                     r.nivelordprod == Lancamento.nivelordprod);

                if (rotinas.Where(r => r.aptoprocesso || r.aptoacabado).Count() > 1)
                {
                    Lancamento.codmaquina = ParamEstab.codmaqpadrao ?? rotinas.FirstOrDefault(r => r.operacao == InicioProd.operacao).codmaquina;
                }
                else
                {
                    Lancamento.codmaquina = rotinas.Any() ?
                        (rotinas.FirstOrDefault(r => r.operacao == 10) ?? rotinas.OrderBy(r => r.operacao).FirstOrDefault()).codmaquina : "";
                }
            }

            if (ParamEstab.exigeturnoapto)
            {
                var funcionario = (await _IGEFuncionariosService.QueryAsync(r => r.codestab == pCodEstab && r.codfuncionario == Lancamento.codfuncionario)).FirstOrDefault();
                Lancamento.turno = funcionario.turno ?? ParamEstab.turnopadrao;
            }

            await _IPDLancamentosService.ValidateAppointmentAsync(Lancamento);
            Errors = _IPDLancamentosService.Errors;

            if (Errors.Any())
                return null;

            var OrdemProd = (await _IPDOrdemProdService.QueryAsync(r =>
                r.codestab == pCodEstab &&
                r.codordprod == InicioProd.codordprod &&
                r.nivelordprod == InicioProd.nivelordprod
            )).FirstOrDefault();

            var ProdutoPCP = (await _IFSProdutoPcpService.QueryAsync(r => 
                r.codestab == pCodEstab &&
                r.codproduto == OrdemProd.codproduto
            )).FirstOrDefault();

            foreach (var item in pAptTermCreate.Refugos)
            {
                item.qtdreaproveitada = 0;
                item.qtdsucata = item.apontarpeso && ProdutoPCP.pesoliquido > 0 ?
                    item.quantidade :
                    item.quantidade * ProdutoPCP.pesoliquido;
                // item.quantidade * ProdutoPCP.pesoliquido;
                item.quantidade = item.apontarpeso && ProdutoPCP.pesoliquido > 0 ?
                    item.quantidade / ProdutoPCP.pesoliquido :
                    item.quantidade;
            }

            TerminoProd.qtdrefugo = pAptTermCreate.Refugos.Sum(r => r.quantidade);

            Lancamento.quantidade = TerminoProd.quantidade + TerminoProd.qtdrefugo;


            var CodLancamento = await _IPDLancamentosService.NextSequenceAsync();

            await _IPDLancamentosService.ApontamentoProducaoTempAsync(
                CodLancamento, Lancamento, Mapper.Map<List<PDLancMotivoRefugo>>(pAptTermCreate.Refugos));

            if (!_IPDLancamentosService.Errors.Any())
                await _IPDLancamentosService.SaveChanges();

            if (_IPDLancamentosService.Errors.Any())
            {
                Errors = _IPDLancamentosService.Errors;
                return null;
            }

            var LancamentoBD = (await _IPDLancamentosService.QueryAsync(r => 
                r.codestab == Lancamento.codestab && 
                r.codlancamento == CodLancamento)).FirstOrDefault();

            if (LancamentoBD == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "codlancamento",
                    Value = "",
                    Messages = new string[] { Infra.Cross.Resources.MessagesResource.AppointmentDoesNotSaved }
                });
                return null;
            }

            TerminoProd.tipo = 4;
            TerminoProd.codfimapt = await _IPDAptTerminoProdService.NextSequenceAsync();
            TerminoProd.codmotivo = null;

            if (pAptTermCreate.Refugos.Any())
                TerminoProd.codmotivo = pAptTermCreate.Refugos.First().codmotivo;

            _IPDAptTerminoProdService.Add(TerminoProd);

            if (!_IPDAptTerminoProdService.Errors.Any())
                await _IPDAptTerminoProdService.SaveChanges();

            if (_IPDAptTerminoProdService.Errors.Any())
            {
                Errors = _IPDAptTerminoProdService.Errors;
                return null;
            }

            _IPDAptLancamentoService.Add(new PDAptLancamento
            {
                codestab = TerminoProd.codestab,
                codfimapt = TerminoProd.codfimapt,
                codlancamento = LancamentoBD.codlancamento
            });

            if (!_IPDAptLancamentoService.Errors.Any())
                await _IPDAptLancamentoService.SaveChanges();


            if (_IPDAptLancamentoService.Errors.Any())
            {
                Errors = _IPDAptLancamentoService.Errors;
                return null;
            }

            return Mapper.Map<PDAptTerminoProdVM>(TerminoProd);
        }

        public async Task<IEnumerable<PDAptLancListVM>> GetListApontamentoAsync(short pCodEstab, int pCodFuncionario) =>
            Mapper.Map<IEnumerable<PDAptLancList>, IEnumerable<PDAptLancListVM>>(await _IPDAptTerminoProdService.GetListApontamentoAsync(pCodEstab, pCodFuncionario));

        public async Task<PDAptTerminoProdVM> TerminarTurnoProducaoAsync(short pCodEstab, PDAptTerminoProdCreateVM pAptTermCreate)
        {
            var rand = new Random();

            var TerminoProd = new PDAptTerminoProd
            {
                codestab = pCodEstab,
                codiniapt = pAptTermCreate.codiniapt,
                codusuario = pAptTermCreate.codusuario,
                codfuncionario = pAptTermCreate.codfuncionario,
                tipo = pAptTermCreate.tipo,
                quantidade = 0,
                qtdrefugo = 0,
                qtdretrabalho = pAptTermCreate.qtdretrabalho,
                observacao = pAptTermCreate.observacao,
                dttermino = DateTime.Now
            };

            var InicioProd = await GetInicioAsync(pCodEstab, pAptTermCreate.codiniapt, pAptTermCreate.codfuncionario, 3);

            await ValidateDataAsync(TerminoProd, InicioProd);

            var Lancamento = new PDLancamentos
            {
                codestab = pCodEstab,
                codlancamento = rand.Next(),
                codordprod = InicioProd.codordprod,
                nivelordprod = InicioProd.nivelordprod,
                operacao = InicioProd.operacao,
                codfuncionario = TerminoProd.codfuncionario,
                codusuario = TerminoProd.codusuario,
                quantidade = 0,
                dtinicio = InicioProd.dtinicio,
                dttermino = TerminoProd.dttermino,
                codestrutura = InicioProd.codestrutura,
                codroteiro = InicioProd.codroteiro,
                turno = "",
                codmaquina = "",
                terminoturno = 1
            };
            await _IPDLancamentosService.ValidateAppointmentAsync(Lancamento);

            if (Errors.Any())
                return null;

            var CodLancamento = await _IPDLancamentosService.NextSequenceAsync();
            Lancamento.codlancamento = CodLancamento;
            _IPDLancamentosService.Add(Lancamento);

            if (!_IPDLancamentosService.Errors.Any())
                await _IPDLancamentosService.SaveChanges();

            if (_IPDLancamentosService.Errors.Any())
            {
                Errors = _IPDLancamentosService.Errors;
                return null;
            }

            TerminoProd.tipo = 13;
            TerminoProd.codfimapt = await _IPDAptTerminoProdService.NextSequenceAsync();
            TerminoProd.codmotivo = null;
            _IPDAptTerminoProdService.Add(TerminoProd);

            if (!_IPDAptTerminoProdService.Errors.Any())
                await _IPDAptTerminoProdService.SaveChanges();

            if (_IPDAptTerminoProdService.Errors.Any())
            {
                Errors = _IPDAptTerminoProdService.Errors;
                return null;
            }

            _IPDAptLancamentoService.Add(new PDAptLancamento
            {
                codestab = TerminoProd.codestab,
                codfimapt = TerminoProd.codfimapt,
                codlancamento = Lancamento.codlancamento
            });

            if (!_IPDAptLancamentoService.Errors.Any())
                await _IPDAptLancamentoService.SaveChanges();

            if (_IPDAptLancamentoService.Errors.Any())
            {
                Errors = _IPDAptLancamentoService.Errors;
                return null;
            }

            return Mapper.Map<PDAptTerminoProdVM>(TerminoProd);
        }

        public async Task<PDAptTerminoProdVM> TerminarParadaAsync(short pCodEstab, PDAptTerminoProdCreateVM pAptTermCreate)
        {
            var TerminoProd = new PDAptTerminoProd
            {
                codestab = pCodEstab,
                codiniapt = pAptTermCreate.codiniapt,
                codusuario = pAptTermCreate.codusuario,
                codfuncionario = pAptTermCreate.codfuncionario,
                tipo = pAptTermCreate.tipo,
                quantidade = 0,
                qtdrefugo = 0,
                qtdretrabalho = 0,
                codmotivo = (short)pAptTermCreate.codmotivo,
                observacao = (pAptTermCreate.observacao ?? "").ToUpper(),
                dttermino = DateTime.Now
            };

            var InicioProd = await GetInicioAsync(pCodEstab, pAptTermCreate.codiniapt, pAptTermCreate.codfuncionario, 7);
            
            await ValidateDataAsync(TerminoProd, InicioProd);

            var CodParada = await _IPDParadasService.NextSequenceAsync();

            _IPDParadasService.Add(new PDParadas
            {
                codestab = pCodEstab,
                codestrutura = InicioProd.codestrutura,
                codmaquina = pAptTermCreate.codmaquina,
                codfuncionario = pAptTermCreate.codfuncionario,
                codordprod = InicioProd.codordprod,
                nivelordprod = InicioProd.nivelordprod,
                observacao = pAptTermCreate.observacao,
                inicio = InicioProd.dtinicio,
                termino = TerminoProd.dttermino,
                turno = null,
                codmotivo = (short)TerminoProd.codmotivo,
                codparada = CodParada
            });

            if (!_IPDParadasService.Errors.Any())
                await _IPDParadasService.SaveChanges();

            if (_IPDParadasService.Errors.Any())
            {
                Errors = _IPDParadasService.Errors;
                return null;
            }

            TerminoProd.tipo = 8;
            TerminoProd.codfimapt = await _IPDAptTerminoProdService.NextSequenceAsync();
            _IPDAptTerminoProdService.Add(TerminoProd);

            if (!_IPDAptTerminoProdService.Errors.Any())
                await _IPDAptTerminoProdService.SaveChanges();

            if (_IPDAptTerminoProdService.Errors.Any())
            {
                Errors = _IPDAptTerminoProdService.Errors;
                return null;
            }

            _IPDAptParadasService.Add(new PDAptParadas
            {
                codestab = pCodEstab,
                codfimapt = TerminoProd.codfimapt,
                codparada = CodParada
            });

            if (!_IPDAptParadasService.Errors.Any())
                await _IPDAptParadasService.SaveChanges();

            if (_IPDAptParadasService.Errors.Any())
            {
                Errors = _IPDAptParadasService.Errors;
                return null;
            }

            return Mapper.Map<PDAptTerminoProdVM>(TerminoProd);
        }
    }
}
