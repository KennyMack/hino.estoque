﻿using Hino.Estoque.Aplication.Interfaces.Producao.Apontamento;
using Hino.Estoque.Aplication.ViewModels.Producao.Apontamento;
using Hino.Estoque.Domain.Estoque.Interfaces.Services.Estoque;
using Hino.Estoque.Domain.Fiscal.Interfaces.Services;
using Hino.Estoque.Domain.Gerais.Interfaces.Services;
using Hino.Estoque.Domain.Producao.Interfaces.Services;
using Hino.Estoque.Domain.Producao.Interfaces.Services.Apontamento;
using Hino.Estoque.Infra.Cross.Entities.Producao.Apontamento;
using Hino.Estoque.Infra.Cross.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.Services.Producao.Apontamento
{
    public class PDSucataAS : BaseAppService<PDSucata>, IPDSucataAS
    {
        private readonly IPDSucataService _IPDSucataService;
        private readonly IESKardexService _IESKardexService;
        private readonly IFSSaldoEstoqueService _IFSSaldoEstoqueService;
        private readonly IPDLancamentosService _IPDLancamentosService;
        private readonly IPDParamEstabService _IPDParamEstabService;
        private readonly IFSProdutoPcpService _IFSProdutoPcpService;
        private readonly IFSProdutoparamEstabService _IFSProdutoparamEstabService;
        private readonly IPDOrdemProdService _IPDOrdemProdService;
        private readonly IGEFuncionariosService _IGEFuncionariosService;
        private readonly IPDMotivosService _IPDMotivosService;

        public PDSucataAS(IPDSucataService pIPDSucataService,
            IESKardexService pIESKardexService,
            IFSSaldoEstoqueService pIFSSaldoEstoqueService,
            IPDLancamentosService pIPDLancamentosService,
            IPDParamEstabService pIPDParamEstabService,
            IFSProdutoPcpService pIFSProdutoPcpService,
            IFSProdutoparamEstabService pIFSProdutoparamEstabService,
            IPDOrdemProdService pIPDOrdemProdService,
            IGEFuncionariosService pIGEFuncionariosService,
            IPDMotivosService pIPDMotivosService) :
             base(pIPDSucataService)
        {
            _IESKardexService = pIESKardexService;
            _IPDSucataService = pIPDSucataService;
            _IFSSaldoEstoqueService = pIFSSaldoEstoqueService;
            _IPDLancamentosService = pIPDLancamentosService;
            _IPDParamEstabService = pIPDParamEstabService;
            _IFSProdutoPcpService = pIFSProdutoPcpService;
            _IFSProdutoparamEstabService = pIFSProdutoparamEstabService;
            _IPDOrdemProdService = pIPDOrdemProdService;
            _IGEFuncionariosService = pIGEFuncionariosService;
            _IPDMotivosService = pIPDMotivosService;
        }

        public async Task<PDSucata> PostApontarSucata(short pCodEstab, PDSucataCreateVM pPDSucataCreate)
        {
            var Sucata = new PDSucata
            {
                datasuc = DateTime.Now,
                codestab = pCodEstab,
                codproduto = pPDSucataCreate.codproduto,
                lote = pPDSucataCreate.lote,
                codestoque = pPDSucataCreate.codestoque,
                qtdeproduto = pPDSucataCreate.qtdeproduto,
                codprdsuc = "",
                codestsuc = "",
                qtdesucata = 0,
                codmotivo = pPDSucataCreate.codmotivo,
                codfuncionario = pPDSucataCreate.codfuncionario,
                codordprod = pPDSucataCreate.codordprod,
                nivelordprod = pPDSucataCreate.nivelordprod,
                codestrutura = pPDSucataCreate.codestrutura,
                codlancamento = pPDSucataCreate.codlancamento
            };

            var Parameters = (await _IPDParamEstabService.QueryAsync(r => r.codestab == pCodEstab)).FirstOrDefault();

            if (Parameters == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { "Parametros de produção não definidos" }
                });
                return null;
            }

            var Funcionario = (await _IGEFuncionariosService.QueryAsync(r => r.codestab == pCodEstab && r.codfuncionario == Sucata.codfuncionario)).FirstOrDefault();

            if (Funcionario == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { "Funcionário não encontrado" }
                });
                return null;
            }

            if (Funcionario.codusuario == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { "Usuário não configurado no funcionário" }
                });
                return null;
            }

            var OrderProd = await _IPDOrdemProdService.GetOPById(pCodEstab, Sucata.codordprod, Sucata.nivelordprod);

            if (OrderProd == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { "Ordem de produção não encontrada" }
                });
                return null;
            }

            Sucata.codordprod = OrderProd.codordprod;
            Sucata.nivelordprod = OrderProd.nivelordprod;
            Sucata.codestrutura = OrderProd.codestrutura;

            var ProdutoParam = (await _IFSProdutoparamEstabService.QueryAsync(r =>
                r.codestab == pCodEstab &&
                r.codproduto == Sucata.codproduto)).FirstOrDefault();

            var ProdutoPcp = (await _IFSProdutoPcpService.QueryAsync(r =>
                r.codestab == pCodEstab &&
                r.codproduto == Sucata.codproduto)).FirstOrDefault();

            if (ProdutoParam == null || ProdutoPcp == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { "Dados do produto não localizados" }
                });
                return null;
            }

            if (ProdutoPcp.codsucata == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { "Dados do produto sucata não encontrados" }
                });
                return null;
            }

            if (ProdutoPcp.pesoliquido <= 0)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { "Produto com peso liquido zerado" }
                });
                return null;
            }

            Sucata.codprdsuc = ProdutoPcp.codsucata;

            var ProdutoSucParam = (await _IFSProdutoparamEstabService.QueryAsync(r =>
                r.codestab == pCodEstab &&
                r.codproduto == Sucata.codprdsuc)).FirstOrDefault();

            Sucata.codestsuc = ProdutoSucParam.codestoque;
            Sucata.qtdesucata = ProdutoPcp.pesoliquido * Sucata.qtdeproduto;

            if (Sucata.qtdeproduto <= 0)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { "Quantidade deve ser maior que zero." }
                });
                return null;
            }

            var motivo = (await _IPDMotivosService.QueryAsync(r => r.codmotivo == Sucata.codmotivo)).FirstOrDefault();
            
            if (motivo == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { "Motivo não encontrado." }
                });
                return null;
            }

            if (Sucata.qtdesucata <= 0)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { "Quantidade sucata deve ser maior que zero." }
                });
                return null;
            }

            var Estoque = (await _IFSSaldoEstoqueService.GetProdutoSaldo(pCodEstab, Sucata.codproduto, Sucata.codestoque)).FirstOrDefault();

            if (Estoque == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { "Saldo de estoque não encontrado" }
                });
                return null;
            }

            if (Estoque.saldoEstoque < Sucata.qtdeproduto)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { "Quantidade do produto é maior que o saldo de estoque" }
                });
                return null;
            }

            var lancamentos = await _IPDLancamentosService.QueryAsync(r =>
                r.codestab == pCodEstab &&
                r.codordprod == Sucata.codordprod &&
                r.nivelordprod == Sucata.nivelordprod);

            if (!lancamentos.Any() && Parameters.validadtapont == 1)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { "Não existe apontamento de produção, sucateamento não permitido" }
                });
                return null;
            }
            var validaDtSuc = await _IPDSucataService.ValidaDtSuc(pCodEstab, Sucata.codordprod, Sucata.nivelordprod, Sucata.datasuc);

            if (validaDtSuc && Parameters.validadtapont == 1)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { "Data sucateamento é maior que a data do apontamento de produção" }
                });
                return null;
            }

            var _dtFechamento = await _IESKardexService.GetDataFechamento(pCodEstab);

            if (_dtFechamento >= Sucata.datasuc)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { "Bloqueado por fechamento de estoque" }
                });
                return null;
            }

            var result = await _IESKardexService.GetValidaSaldoDataLancConsumo(pCodEstab,
                Sucata.codproduto, Sucata.codestoque, Sucata.qtdeproduto, Sucata.datasuc);

            if (!result)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { $"Kardex negativo para a sucata na OP na data: {Sucata.datasuc.ToString("dd/MM/yyyy")}" }
                });
                return null;
            }

            Sucata.codsucata = await _IPDSucataService.NextSequenceAsync();

            _IPDSucataService.Add(Sucata);

            if (!_IPDSucataService.Errors.Any())
                await _IPDSucataService.SaveChanges();
            else
            {
                Errors = _IPDSucataService.Errors;
                return null;
            }

            if (!Errors.Any())
                await _IPDSucataService.ApontaSucataAsync(pCodEstab, Sucata.codsucata, Funcionario.codusuario);

            if (Errors.Any())
            {
                _IPDSucataService.Remove(Sucata);
                await _IPDSucataService.SaveChanges();
                return null;
            }

            return Sucata;

        }
    }
}
