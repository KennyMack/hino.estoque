using Hino.Estoque.Aplication.Interfaces.Producao;
using Hino.Estoque.Domain.Producao.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Producao;

namespace Hino.Estoque.Aplication.Services.Producao
{
    public class PDOrdemProdRotinasAS : BaseAppService<PDOrdemProdRotinas>, IPDOrdemProdRotinasAS
    {
        private readonly IPDOrdemProdRotinasService _IPDOrdemProdRotinasService;

        public PDOrdemProdRotinasAS(IPDOrdemProdRotinasService pIPDOrdemProdRotinasService) : 
             base(pIPDOrdemProdRotinasService)
        {
            _IPDOrdemProdRotinasService = pIPDOrdemProdRotinasService;
        }
    }
}
