using System.Threading.Tasks;
using AutoMapper;
using Hino.Estoque.Aplication.Interfaces.Producao;
using Hino.Estoque.Aplication.ViewModels.Producao;
using Hino.Estoque.Domain.Fiscal.Interfaces.Services;
using Hino.Estoque.Domain.Producao.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Producao;
using System.Linq;
using Hino.Estoque.Infra.Cross.Utils.Exceptions;
using System.Collections.Generic;
using System;
using System.Configuration;
using Hino.Estoque.Aplication.Interfaces.Fiscal;
using Hino.Estoque.Infra.Cross.Entities.Fiscal;
using Hino.Estoque.Domain.Estoque.Interfaces.Services.Estoque;
using Hino.Estoque.Aplication.ViewModels.Engenharia;
using Hino.Estoque.Domain.Engenharia.Interfaces.Services;

namespace Hino.Estoque.Aplication.Services.Producao
{
    public class PDOrdemProdAS : BaseAppService<PDOrdemProd>, IPDOrdemProdAS
    {
        readonly IENMaquinasService _IENMaquinasService;
        private readonly IFSSaldoEstoqueService _IFSSaldoEstoqueService;
        private readonly IPDOrdemProdService _IPDOrdemProdService;
        private readonly IPDOrdemProdCompService _IPDOrdemProdCompService;
        private readonly IPDOPTransfService _IPDOPTransfService;
        private readonly IFSProdutoService _IFSProdutoService;
        private readonly IFSProdutoPcpService _FSProdutoPcpService;
        private readonly IFSProdutoparamEstabService _IFSProdutoparamEstabService;
        private readonly IESProdEnderecService _IESProdEnderecService;
        private readonly IESEnderecamentoService _IESEnderecamentoService;

        public PDOrdemProdAS(IPDOrdemProdService pIPDOrdemProdService,
            IFSSaldoEstoqueService pIFSSaldoEstoqueService,
            IPDOPTransfService pIPDOPTransfService,
            IFSProdutoPcpService pFSProdutoPcpService,
            IPDOrdemProdCompService pIPDOrdemProdCompService,
            IFSProdutoparamEstabService pIFSProdutoparamEstabService,
            IFSProdutoService pIFSProdutoService,
            IESProdEnderecService pIESProdEnderecService,
            IESEnderecamentoService pIESEnderecamentoService,
            IENMaquinasService pIENMaquinasService) :
             base(pIPDOrdemProdService)
        {
            _IENMaquinasService = pIENMaquinasService;
            _IFSProdutoService = pIFSProdutoService;
            _IPDOPTransfService = pIPDOPTransfService;
            _IFSSaldoEstoqueService = pIFSSaldoEstoqueService;
            _IPDOrdemProdService = pIPDOrdemProdService;
            _FSProdutoPcpService = pFSProdutoPcpService;
            _IPDOrdemProdCompService = pIPDOrdemProdCompService;
            _IFSProdutoparamEstabService = pIFSProdutoparamEstabService;
            _IESProdEnderecService = pIESProdEnderecService;
            _IESEnderecamentoService = pIESEnderecamentoService;
        }

        bool AllowChangeStockOrigin()
        {
            try
            {
                return Convert.ToBoolean(ConfigurationManager.AppSettings.Get("AllowChangeStockOrigin") ?? "false");
            }
            catch (Exception)
            {
                return false;
            }
        }

        async Task LoadOPCompEstqEnd(PDOrdemProd pOrdemProd)
        {
            if (pOrdemProd != null && pOrdemProd.pdordemprodcomp != null)
            {
                foreach (var item in pOrdemProd.pdordemprodcomp)
                {
                    var estoques = await _IPDOrdemProdCompService.BuscaListaEstqEndAsync(item.codcomponente, pOrdemProd.codestab);

                    foreach (var estq in estoques)
                    {
                        estq.codordprod = item.codordprod;
                        estq.nivelordprod = item.nivelordprod;
                    }
                    item.OrdemProdCompEstqEnd = new List<PDOrdemProdCompEstqEnd>(estoques);
                }
            }
        }

        public async Task<IEnumerable<PDOrdemProdComp>> GetCompToSucAsync(short pCodEstab, long pCodOrdProd, string pNivelOrdProd)
        {
            return await _IPDOrdemProdCompService.QueryAsync(r => 
                r.codestab == pCodEstab &&
                r.codordprod == pCodOrdProd &&
                r.nivelordprod == pNivelOrdProd, s => s.fsproduto);
        }

        private async Task<PDOrdemProd> LoadOPByIdAsync(short pCodEstab, decimal pCodOrdProd, string pNivelOrdProd)
        {
            var Ordem = (await _IPDOrdemProdService.QueryAsync(r =>
                r.codestab == pCodEstab &&
                r.codordprod == pCodOrdProd &&
                r.nivelordprod == pNivelOrdProd &&
                r.status == "P",
                e => e.fsproduto,
                e => e.pdordemprodcomp,
                e => e.pdordemprodrotinas)).FirstOrDefault();

            if (Ordem == null) return null;

            return await LoadDetailOPAsync(Ordem);
        }

        private async Task<PDOrdemProd> LoadOPByBarCodeAsync(short pCodEstab, string pBarCode)
        {
            try
            {

                var code = Convert.ToInt64(pBarCode.Length >= 14 ? pBarCode.Substring(4) : pBarCode);

                var Ordem = (await _IPDOrdemProdService.QueryAsync(r =>
                    r.codestab == pCodEstab &&
                    r.seqbarras == code &&
                    r.status == "P",
                    e => e.pdordemprodcomp,
                    e => e.pdordemprodrotinas)).FirstOrDefault();

                if (Ordem == null) return null;

                return await LoadDetailOPAsync(Ordem);
            }
            catch (Exception ex)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "BarCode",
                    Value = pBarCode.ToString(),
                    Messages = new string[] { ex.Message, ex.InnerException?.Message }
                });
                return null;
            }
        }

        private async Task<PDOrdemProd> LoadDetailOPAsync(PDOrdemProd Ordem)
        {
            var Produto = (await _IFSProdutoService.QueryAsync(r => r.codproduto == Ordem.codproduto)).FirstOrDefault();

            Ordem.fsproduto = Produto;

            var ProdutoParam = (await _IFSProdutoparamEstabService.QueryAsync(r => r.codestab == Ordem.codestab && r.codproduto == Ordem.codproduto)).FirstOrDefault();
            var ProdutoPCP = (await _FSProdutoPcpService.QueryAsync(r => r.codestab == Ordem.codestab && r.codproduto == Ordem.codproduto)).FirstOrDefault();

            Ordem.fsproduto.FSProdutoparamEstab.Add(ProdutoParam);
            Ordem.fsproduto.FSProdutoPcp.Add(ProdutoPCP);

            foreach (var item in Ordem.pdordemprodcomp)
            {
                var ProdutoComp = await _IFSProdutoService.QueryAsync(r => r.codproduto == item.codcomponente);

                item.fsproduto = ProdutoComp.FirstOrDefault();

                var ProdutoCompParam = (await _IFSProdutoparamEstabService.QueryAsync(r => r.codestab == Ordem.codestab && r.codproduto == item.codcomponente)).FirstOrDefault();
                var ProdutoCompPCP = (await _FSProdutoPcpService.QueryAsync(r => r.codestab == Ordem.codestab && r.codproduto == item.codcomponente)).FirstOrDefault();

                ProdutoCompParam.ESProdEnderec = (await _IESProdEnderecService.QueryAsync(r => r.codestab == item.codestab && r.codproduto == item.codcomponente, x => x.ESEnderecamento)).ToArray();

                item.fsproduto.FSProdutoparamEstab.Add(ProdutoCompParam);
                item.fsproduto.FSProdutoPcp.Add(ProdutoCompPCP);
            }

            if (AllowChangeStockOrigin())
                await LoadOPCompEstqEnd(Ordem);

            return Ordem;
        }

        public async Task<PDOrdemProd> AddOPSaldo(PDOrdemProd pOrdemProd)
        {
            /* pOrdemProd.fsproduto.FSProdutoparamEstab.ToArray()[0].FSSaldoEstoque =
                     await _IFSSaldoEstoqueService.GetProdutoSaldo(pOrdemProd.codestab, pOrdemProd.codproduto,
                     pOrdemProd.fsproduto.FSProdutoparamEstab.ToArray()[0].codestoque);

             foreach (var item in pOrdemProd.pdordemprodcomp)
             {
                 item.fsproduto.FSProdutoparamEstab.ToArray()[0]
                     .FSSaldoEstoque =
                     await _IFSSaldoEstoqueService.GetProdutoSaldo(pOrdemProd.codestab, item.codcomponente, item.codestoque);
             }*/

            foreach (var item in pOrdemProd.pdordemprodcomp)
            {
                item.saldoorigem = await _IFSSaldoEstoqueService.SaldoEstoqueProduto(pOrdemProd.codestab,
                    item.codcomponente, item.fsproduto.ProdutoParamEstab.codestoque);
                item.saldodestino = await _IFSSaldoEstoqueService.SaldoEstoqueProduto(pOrdemProd.codestab,
                    item.codcomponente, item.codestoque);
                item.fsproduto.FSProdutoPcp = new HashSet<FSProdutoPcp>(
                    await _FSProdutoPcpService.QueryAsync(r => r.codestab == pOrdemProd.codestab &&
                    r.codproduto == item.codcomponente
                ));
            }

            return pOrdemProd;
        }

        public async Task<PDOrdemProd> AddOPTransf(PDOrdemProd pOrdemProd)
        {
            foreach (var item in pOrdemProd.pdordemprodcomp)
            {
                item.pdoptransf = new HashSet<PDOPTransf>(
                    await _IPDOPTransfService.QueryAsync(r => r.codestab == item.codestab &&
                        r.codordprod == item.codordprod &&
                        r.nivelordprod == item.nivelordprod &&
                        r.codcomponente == item.codcomponente &&
                        r.operacao == item.operacao));
            }

            return pOrdemProd;
        }

        public async Task<PDOrdemProd> GetOPByBarCode(short pCodEstab, string pBarCode)
        {
            var IsOracleOnze = Convert.ToBoolean(ConfigurationManager.AppSettings.Get("IsOracleOnze") ?? "false");

            PDOrdemProd OrdemProd;

            if (!IsOracleOnze)
                OrdemProd = await _IPDOrdemProdService.GetOPByBarCode(pCodEstab, pBarCode);
            else
                OrdemProd = await LoadOPByBarCodeAsync(pCodEstab, pBarCode);

            if (OrdemProd != null)
            {
                OrdemProd.pdordemprodcomp = OrdemProd.pdordemprodcomp.OrderBy(r => r.codcomponente).ToArray();
                OrdemProd = await AddOPSaldo(OrdemProd);
                OrdemProd = await AddOPTransf(OrdemProd);

                OrdemProd.ShowResultOP = Convert.ToBoolean(ConfigurationManager.AppSettings.Get("ShowResultsOP") ?? "false");
                var LoadQtd = Convert.ToBoolean(ConfigurationManager.AppSettings.Get("LoadQtd") ?? "false");

                if (LoadQtd)
                {
                    foreach (var item in OrdemProd.pdordemprodcomp)
                        item.loadqtd = LoadQtd;
                }
            }
            else
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "BarCode",
                    Value = pBarCode.ToString(),
                    Messages = new string[] { Infra.Cross.Resources.MessagesResource.OrdemProdNotFound }
                });

                return null;
            }

            if (AllowChangeStockOrigin())
                await LoadOPCompEstqEnd(OrdemProd);

            return OrdemProd;
        }

        public async Task<PDOrdemProd> GetOPById(short pCodEstab, decimal pCodOrdProd, string pNivelOrdProd)
        {
            var IsOracleOnze = Convert.ToBoolean(ConfigurationManager.AppSettings.Get("IsOracleOnze") ?? "false");

            PDOrdemProd OrdemProd;
            if (!IsOracleOnze)
                OrdemProd = await _IPDOrdemProdService.GetOPById(pCodEstab, pCodOrdProd, pNivelOrdProd);
            else
                OrdemProd = await LoadOPByIdAsync(pCodEstab, pCodOrdProd, pNivelOrdProd);

            if (OrdemProd != null)
            {
                OrdemProd.pdordemprodcomp = OrdemProd.pdordemprodcomp.OrderBy(r => r.codcomponente).ToArray();
                OrdemProd = await AddOPSaldo(OrdemProd);
            }
            else
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "CodOrdProd",
                    Value = pCodOrdProd.ToString(),
                    Messages = new string[] { Infra.Cross.Resources.MessagesResource.OrdemProdNotFound }
                });

                return null;
            }

            if (AllowChangeStockOrigin())
                await LoadOPCompEstqEnd(OrdemProd);

            return OrdemProd;
        }

        public async Task<IEnumerable<PDEficienciaProducao>> BuscaEficienciaProducaoAsync(short pCodEstab, DateTime pDtInicio, DateTime pDtTermino) =>
            await _IPDOrdemProdService.BuscaEficienciaProducaoAsync(pCodEstab, pDtInicio, pDtTermino);

        public async Task<IEnumerable<ENMaquinasVM>> GetMachinesAsync(short pCodEstab)
        {
            var Maquinas = (await _IENMaquinasService.QueryAsync(r => r.codestab == pCodEstab && r.status)).ToArray();
            return Mapper.Map<ENMaquinasVM[]>(Maquinas);
        }

        public async Task<long?> GetCodEstruturaAsync(string pCodProduto, short pCodEstab) =>
            await _IPDOrdemProdService.GetCodEstruturaAsync(pCodProduto, pCodEstab);
    }
}
