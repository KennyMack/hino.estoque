using Hino.Estoque.Aplication.Interfaces.Producao;
using Hino.Estoque.Domain.Producao.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Producao;

namespace Hino.Estoque.Aplication.Services.Producao
{
    public class PDOPTransfLoteAS : BaseAppService<PDOPTransfLote>, IPDOPTransfLoteAS
    {
        private readonly IPDOPTransfLoteService _IPDOPTransfLoteService;

        public PDOPTransfLoteAS(IPDOPTransfLoteService pIPDOPTransfLoteService) : 
             base(pIPDOPTransfLoteService)
        {
            _IPDOPTransfLoteService = pIPDOPTransfLoteService;
        }
    }
}
