﻿using AutoMapper;
using Hino.Estoque.Aplication.Interfaces.Producao.OEE;
using Hino.Estoque.Aplication.ViewModels.Engenharia;
using Hino.Estoque.Aplication.ViewModels.Producao.OEE;
using Hino.Estoque.Domain.Engenharia.Interfaces.Services;
using Hino.Estoque.Domain.Producao.Interfaces.Services.OEE;
using Hino.Estoque.Infra.Cross.Entities.Producao.OEE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.Services.Producao.OEE
{
    public class PDOEEAS: BaseAppService<PDOEE>, IPDOEEAS
    {
        private readonly IPDOEEService _IPDOEEService;
        private readonly IENMaquinasService _IENMaquinasService;
        private readonly IENProcessosService _IENProcessosService;
        private readonly IENProcAgrupService _IENProcAgrupService;

        public PDOEEAS(IPDOEEService pIPDOEEService,
            IENMaquinasService pIENMaquinasService,
            IENProcessosService pIENProcessosService,
            IENProcAgrupService pIENProcAgrupService) :
                base(pIPDOEEService)
        {
            _IPDOEEService = pIPDOEEService;
            _IENMaquinasService = pIENMaquinasService;
            _IENProcessosService = pIENProcessosService;
            _IENProcAgrupService = pIENProcAgrupService;
        }

        public async Task<bool> GerarOEEAsync(PDOEEGerarVM pOee) =>
            await _IPDOEEService.GerarOEEAsync(Mapper.Map<PDOEE>(pOee));

        public async Task<bool> GerarOEEMensalAsync(PDOEEGerarVM pOee) =>
            await _IPDOEEService.GerarOEEMensalAsync(Mapper.Map<PDOEE>(pOee));

        public async Task<IEnumerable<PDOEEResultado>> ListagemAsync(short pCodEstab, string pCodUsuario) =>
            await _IPDOEEService.ListagemAsync(pCodEstab, pCodUsuario);

        public async Task<IEnumerable<PDOEEMensalResultado>> ListagemMensalAsync(short pCodEstab, string pCodUsuario) =>
            await _IPDOEEService.ListagemMensalAsync(pCodEstab, pCodUsuario);

        public async Task<ENSearchOEEVM> SearchOEEAsync(short pCodEstab)
        {
            var Maquinas = (await _IENMaquinasService.QueryAsync(r => r.codestab == pCodEstab && r.status)).ToArray();
            var Processos = (await _IENProcessosService.QueryAsync(r => r.codestab == pCodEstab)).ToArray();
            var Agrupamentos = (await _IENProcAgrupService.QueryAsync(r => r.codestab == pCodEstab)).ToArray();

            return new ENSearchOEEVM
            {
                Maquinas = Mapper.Map<ENMaquinasVM[]>(Maquinas),
                Agrupamentos = Mapper.Map<ENProcAgrupVM[]>(Agrupamentos),
                Processos = Mapper.Map<ENProcessosVM[]>(Processos)
            };
        }
    }
}
