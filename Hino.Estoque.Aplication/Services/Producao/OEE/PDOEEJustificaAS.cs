﻿using Hino.Estoque.Aplication.Interfaces.Producao.OEE;
using Hino.Estoque.Domain.Producao.Interfaces.Services.OEE;
using Hino.Estoque.Infra.Cross.Entities.Producao.OEE;
using System;
using System.Collections.Generic;

namespace Hino.Estoque.Aplication.Services.Producao.OEE
{
    public class PDOEEJustificaAS : BaseAppService<PDOEEJustifica>, IPDOEEJustificaAS
    {
        private readonly IPDOEEJustificaService _IPDOEEJustificaService;

        public PDOEEJustificaAS(IPDOEEJustificaService pIPDOEEParamService) :
                base(pIPDOEEParamService)
        {
            _IPDOEEJustificaService = pIPDOEEParamService;
        }

        public IEnumerable<PDOEEJustifica> GetOEEJustificativas(short pCodEstab, string pCodMaquina, DateTime pDtPeriodo) =>
            _IPDOEEJustificaService.GetOEEJustificativas(pCodEstab, pCodMaquina, pDtPeriodo);
    }
}
