﻿using Hino.Estoque.Aplication.Interfaces.Producao.OEE;
using Hino.Estoque.Domain.Producao.Interfaces.Services.OEE;
using Hino.Estoque.Infra.Cross.Entities.Producao.OEE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.Services.Producao.OEE
{
    public class PDOEEParamAS : BaseAppService<PDOEEParam>, IPDOEEParamAS
    {
        private readonly IPDOEEParamService _IPDOEEParamService;

        public PDOEEParamAS(IPDOEEParamService pIPDOEEParamService) :
                base(pIPDOEEParamService)
        {
            _IPDOEEParamService = pIPDOEEParamService;
        }

        public IEnumerable<PDOEEParam> GetOEEColorsByEstabAsync(short pCodEstab) =>
            _IPDOEEParamService.GetOEEColorsByEstabAsync(pCodEstab);
    }
}
