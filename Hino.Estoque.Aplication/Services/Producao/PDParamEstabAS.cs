using Hino.Estoque.Aplication.Interfaces.Producao;
using Hino.Estoque.Domain.Producao.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Producao;

namespace Hino.Estoque.Aplication.Services.Producao
{
    public class PDParamEstabAS : BaseAppService<PDParamEstab>, IPDParamEstabAS
    {
        private readonly IPDParamEstabService _IPDParamEstabService;

        public PDParamEstabAS(IPDParamEstabService pIPDParamEstabService) : 
             base(pIPDParamEstabService)
        {
            _IPDParamEstabService = pIPDParamEstabService;
        }
    }
}
