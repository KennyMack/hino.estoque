using Hino.Estoque.Aplication.Interfaces.Producao;
using Hino.Estoque.Domain.Producao.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Producao;

namespace Hino.Estoque.Aplication.Services.Producao
{
    public class PDOrdemProdCompAS : BaseAppService<PDOrdemProdComp>, IPDOrdemProdCompAS
    {
        private readonly IPDOrdemProdCompService _IPDOrdemProdCompService;

        public PDOrdemProdCompAS(IPDOrdemProdCompService pIPDOrdemProdCompService) : 
             base(pIPDOrdemProdCompService)
        {
            _IPDOrdemProdCompService = pIPDOrdemProdCompService;
        }
    }
}
