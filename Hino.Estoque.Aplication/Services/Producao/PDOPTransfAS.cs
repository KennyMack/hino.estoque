using Hino.Estoque.Aplication.Interfaces.Producao;
using Hino.Estoque.Aplication.ViewModels.Producao;
using Hino.Estoque.Domain.Estoque.Interfaces.Services.Estoque;
using Hino.Estoque.Domain.Fiscal.Interfaces.Services;
using Hino.Estoque.Domain.Producao.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Estoque;
using Hino.Estoque.Infra.Cross.Entities.Producao;
using Hino.Estoque.Infra.Cross.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.Services.Producao
{
    public class PDOPTransfAS : BaseAppService<PDOPTransf>, IPDOPTransfAS
    {
        const double ValorDeErroAoSalvar = -1328;
        private readonly IPDOPTransfService _IPDOPTransfService;
        private readonly IPDOPTransfLoteService _IPDOPTransfLoteService;
        private readonly IPDOrdemProdAS _IPDOrdemProdAS;
        private readonly IFSSaldoEstoqueService _IFSSaldoEstoqueService;
        private readonly IESLoteService _IESLoteService;
        private readonly IESLoteSaldoService _IESLoteSaldoService;
        private readonly IFSProdutoPcpService _IFSProdutoPcpService;
        private readonly IPDParamEstabService _IPDParamEstabService;

        public PDOPTransfAS(IPDOPTransfService pIPDOPTransfService,
            IPDOrdemProdAS pIPDOrdemProdAS,
            IFSSaldoEstoqueService pIFSSaldoEstoqueService,
            IESLoteSaldoService pIESLoteSaldoService,
            IESLoteService pIESLoteService,
            IFSProdutoPcpService pIFSProdutoPcpService,
            IPDOPTransfLoteService pIPDOPTransfLoteService,
            IPDParamEstabService pIPDParamEstabService) :
             base(pIPDOPTransfService)
        {
            _IPDParamEstabService = pIPDParamEstabService;
            _IFSSaldoEstoqueService = pIFSSaldoEstoqueService;
            _IPDOrdemProdAS = pIPDOrdemProdAS;
            _IPDOPTransfService = pIPDOPTransfService;
            _IESLoteSaldoService = pIESLoteSaldoService;
            _IESLoteService = pIESLoteService;
            _IFSProdutoPcpService = pIFSProdutoPcpService;
            _IPDOPTransfLoteService = pIPDOPTransfLoteService;
        }

        public async Task<PDResultTransfOPVM> SeparateOPComponents(short pCodEstab, PDOPCreateTransfVM[] pLstTransf)
        {
            bool AllowTransfMoreThanOp = false;
            try
            {
                AllowTransfMoreThanOp = Convert.ToBoolean(ConfigurationManager.AppSettings.Get("AllowTransfMoreThanOp") ?? "false");
            }
            catch (Exception)
            {
            }

            var ParamEstab = (await _IPDParamEstabService.QueryAsync(r => r.codestab == pCodEstab)).FirstOrDefault();

            _IPDOPTransfLoteService.Errors.Clear();
            _IPDOPTransfService.Errors.Clear();
            var resultData = new PDResultTransfOPVM
            {
                Success = false
            };
            var OrdemProd = await _IPDOrdemProdAS.GetOPById(pCodEstab, pLstTransf[0].codordprod, pLstTransf[0].nivelordprod);
            var TransfResult = new PDOPTransfResultVM
            {
                OrdemProd = OrdemProd,
                Transferencias = pLstTransf.Select(item  => new PDOPTransfItemResultVM
                {
                    Transferencia = new PDOPTransf
                    {
                        codestab = pCodEstab,
                        codordprod = item.codordprod,
                        nivelordprod = item.nivelordprod,
                        codestrutura = item.codestrutura,
                        codroteiro = item.codroteiro,
                        operacao = item.operacao,
                        codcomponente = item.codcomponente,
                        codestoqueori = item.codestoqueori,
                        codestoquedest = item.codestoquedest,
                        codusuario = item.codusuario,
                        status = 0,
                        datatransf = DateTime.Now,
                        quantidade = item.qtdseparada,
                        codtransf = 0
                    },
                    QtdSeparada = item.qtdseparada,
                    Sucesso = false,
                    Motivo = Infra.Cross.Resources.MessagesResource.TransferNotPerformed,
                    Validado = false
                }).ToList()
            };

            foreach (var item in TransfResult.Transferencias)
            {
                if (item.QtdSeparada <= 0)
                    continue;

                var Component = OrdemProd.pdordemprodcomp.FirstOrDefault(r => r.codcomponente == item.Transferencia.codcomponente);
                var Product = (await _IFSProdutoPcpService.QueryAsync(r => r.codestab == pCodEstab && r.codproduto == item.Transferencia.codcomponente)).FirstOrDefault();

                item.Rastreabilidade = Product.rastreabilidade;
                if (item.Transferencia.codestoqueori == item.Transferencia.codestoquedest)
                {
                    item.Motivo = string.Concat($"Componente '{item.Transferencia.codcomponente}'",
                            string.Format(Infra.Cross.Resources.MessagesResource.SamePlaceTransference, item.Transferencia.codestoqueori, item.Transferencia.codestoquedest));
                    continue;
                }

                if (Product == null)
                {
                    item.Motivo = string.Concat(Infra.Cross.Resources.MessagesResource.ProductIdNotFound, " Prod: ", item.Transferencia.codcomponente);
                    continue;
                }

                if (Component == null)
                {
                    item.Motivo = string.Format(Infra.Cross.Resources.MessagesResource.ComponentNotFound, item.Transferencia.codcomponente);
                    continue;
                }
                item.StockBalance = await _IFSSaldoEstoqueService.SaldoEstoqueProduto(pCodEstab, item.Transferencia.codcomponente, item.Transferencia.codestoqueori);

                if (item.StockBalance < item.QtdSeparada)
                {
                    item.Motivo = string.Format(Infra.Cross.Resources.MessagesResource.ComponentWithoutBalance, item.Transferencia.codcomponente);
                    continue;
                }

                if (!AllowTransfMoreThanOp)
                {
                    var QtdTransferida = await _IPDOPTransfService.QtdCompTransferida(pCodEstab, item.Transferencia.codordprod, item.Transferencia.nivelordprod, item.Transferencia.codcomponente);

                    if (item.QtdSeparada > (Component.quantidade - QtdTransferida))
                    {
                        item.Motivo = string.Format(Infra.Cross.Resources.MessagesResource.QuantityTotalSeparated, item.Transferencia.codcomponente);
                        continue;
                    }
                }
                item.Validado = true;
            }

            if (TransfResult.Transferencias.Any(r => r.Validado))
            {
                foreach (var item in TransfResult.Transferencias.Where(r => r.Validado))
                {
                    item.Transferencia.codtransf = Convert.ToInt32(await _IPDOPTransfService.NextSequenceAsync());
                    var pdTransf = new PDOPTransf
                    {
                        codestab = pCodEstab,
                        codordprod = item.Transferencia.codordprod,
                        nivelordprod = item.Transferencia.nivelordprod,
                        codestrutura = item.Transferencia.codestrutura,
                        codroteiro = item.Transferencia.codroteiro,
                        operacao = item.Transferencia.operacao,
                        codcomponente = item.Transferencia.codcomponente,
                        codestoqueori = item.Transferencia.codestoqueori,
                        codestoquedest = item.Transferencia.codestoquedest,
                        codusuario = item.Transferencia.codusuario,
                        status = 0,
                        datatransf = DateTime.Now,
                        quantidade = item.QtdSeparada,
                        codtransf = item.Transferencia.codtransf
                    };

                    _IPDOPTransfService.Add(pdTransf);
                    if (_IPDOPTransfService.Errors.Any())
                    {
                        item.Motivo = string.Join(" ", _IPDOPTransfService.Errors.First().Messages);
                        continue;
                    }

                    if (item.Rastreabilidade)
                    {
                        var CodCompEstrutura = await _IPDOrdemProdAS.GetCodEstruturaAsync(
                            item.Transferencia.codcomponente,
                            pCodEstab);

                        var LinhasLote = await _IESLoteSaldoService.BuscaLoteSaldoOPAsync(
                            pCodEstab,
                            item.Transferencia.codcomponente,
                            item.Transferencia.codestoqueori,
                            item.Transferencia.codordprod,
                            item.Transferencia.nivelordprod
                        );

                        IEnumerable<ESSaldoLoteOP> ListaOrdenada = null;

                        // Verificando o tipo do fifo para ordernar as linhas
                        // 0 - LOTE
                        // 1 - O.P.
                        if (ParamEstab.tipofifolote == 0)
                        {
                            ListaOrdenada = LinhasLote
                                .OrderBy(r =>r.LOTE)
                                .ThenBy(r => r.DATALOTE);
                        }
                        else
                        {
                            ListaOrdenada = LinhasLote
                                .OrderBy(r => r.DATALOTE)
                                .ThenBy(r => r.CODOPLOTE)
                                .ThenBy(r => r.LOTE);
                        }

                        var QtdNec = item.QtdSeparada;
                        var QtdAcum = 0D;
                        var HasError = false;

                        // Verificando se possui estrutura
                        if ((CodCompEstrutura ?? 0) > 0)
                        {
                            // Verificando se faz o consumo dos lotes somente da mesma O.P. 
                            // nesse caso somente lotes que foram gerados por niveis abaixo do nivel atual
                            // ou lotes que est�o marcados com o flag livre ser�o utilizados para fazer a separa��o
                            if (ParamEstab.consumolote == 1)
                                ListaOrdenada = ListaOrdenada.Where(r => r.CODOPLOTE == item.Transferencia.codordprod)
                                    .OrderBy(r => r.CODOPLOTE)
                                    .ThenBy(r => r.LOTE)
                                    .ThenBy(r => r.DATALOTE);
                        }

                        // verificando se usa lote unico na op
                        // caso seja lote unico, o saldo do lote deve ser maior ou igual
                        // a quantidade necess�ria na O.P.
                        if (ParamEstab.usaloteunico == 1)
                        {
                            foreach (var Linha in ListaOrdenada)
                            {
                                var SaldoLote = GetSaldoLoteSeparacao((short)ParamEstab.tipofifolote, ListaOrdenada, Linha, (CodCompEstrutura ?? 0) > 0);

                                if (SaldoLote >= QtdNec)
                                {
                                    var result = MarcaItensFifoLoteOriginal(item, pdTransf, ListaOrdenada, Linha, QtdNec);
                                    HasError = result == ValorDeErroAoSalvar;
                                    QtdAcum = QtdNec;
                                    break;
                                }
                            }

                            // N�o h� lotes com saldo suficiente para realizar a transfer�ncia
                            if (QtdAcum != QtdNec || HasError)
                                break;
                        }
                        // Caso n�o utilize o lote unico faz o fifo normalmente dos lotes
                        else
                        {
                            foreach (var Linha in ListaOrdenada)
                            {
                                if (Linha.SALDOESTOQUEPADRAO >= (QtdNec - QtdAcum))
                                {
                                    Linha.QTDETRANSFATUAL = QtdNec - QtdAcum;
                                    QtdAcum = QtdNec;
                                }
                                else
                                {
                                    Linha.QTDETRANSFATUAL = Linha.SALDOESTOQUEPADRAO;
                                    QtdAcum += Linha.SALDOESTOQUEPADRAO;
                                }

                                var pdTransfLote = new PDOPTransfLote
                                {
                                    codestab = pCodEstab,
                                    codtransf = pdTransf.codtransf,
                                    lote = Linha.LOTE,
                                    quantidade = Linha.QTDETRANSFATUAL
                                };


                                var result = SalvarLotesSelecionadoTransf(item, pdTransfLote);
                                if (!result)
                                {
                                    HasError = true;
                                    break;
                                }

                                if (QtdAcum >= QtdNec || HasError)
                                    break;
                            }
                        }


                        /*
                        var lotes = (await _IESLoteService.QueryAsync(r =>
                            r.codestab == pCodEstab &&
                            r.codproduto == item.Transferencia.codcomponente &&
                            (r.status == "A" || r.status == "M")))
                            .OrderBy(r => r.datalote);

                        var HasError = false;
                        var QtdAcum = 0D;
                        var QtdAtual = 0D;
                        foreach (var lote in lotes)
                        {
                            var lotesSaldo = await _IESLoteSaldoService.QueryAsync(r =>
                                r.codestab == pCodEstab &&
                                r.lote == lote.lote &&
                                r.codestoque == item.Transferencia.codestoqueori &&
                                (r.qtdentrada - r.qtdsaida) > 0);

                            foreach (var saldo in lotesSaldo)
                            {
                                if (saldo.saldoLote >= (item.QtdSeparada - QtdAcum))
                                {
                                    QtdAtual = (item.QtdSeparada - QtdAcum);
                                    QtdAcum += QtdAtual;
                                }
                                else if (saldo.saldoLote < (item.QtdSeparada - QtdAcum))
                                {
                                    QtdAtual = saldo.saldoLote;
                                    QtdAcum += QtdAtual;
                                }

                                var pdTransfLote = new PDOPTransfLote
                                {
                                    codestab = pCodEstab,
                                    codtransf = pdTransf.codtransf,
                                    lote = saldo.lote,
                                    quantidade = QtdAtual
                                };

                                item.Transferencia.TransfLote.Add(pdTransfLote);
                                _IPDOPTransfLoteService.Add(pdTransfLote);
                                if (_IPDOPTransfLoteService.Errors.Any())
                                {
                                    item.Motivo = string.Join(" ", _IPDOPTransfLoteService.Errors.First().Messages);
                                    _IPDOPTransfService.Errors = _IPDOPTransfLoteService.Errors;
                                    HasError = true;
                                    break;
                                }

                                if (QtdAcum >= item.QtdSeparada)
                                    break;
                            }

                            if (QtdAcum >= item.QtdSeparada || HasError)
                                break;
                        }
                        */

                    }

                    if (_IPDOPTransfService.Errors.Any())
                    {
                        Errors = _IPDOPTransfService.Errors;
                        _IPDOPTransfService.RollBackChanges();
                        _IPDOPTransfLoteService.RollBackChanges();
                        continue;
                    }
                    else
                    {
                        item.Sucesso = true;
                        await _IPDOPTransfService.SaveChanges();
                        await _IPDOPTransfLoteService.SaveChanges();
                    }
                }
            }

            if (TransfResult.Transferencias.Any(r => r.Validado && r.Sucesso))
            {
                foreach (var item in TransfResult.Transferencias.Where(r => r.Validado && r.Sucesso))
                {
                    if (!await _IPDOPTransfService.AtualizaTransfOP(pCodEstab, 1, item.Transferencia))
                    {
                        Errors = _IPDOPTransfService.Errors;
                        item.Motivo = string.Join(" ", Errors.First().Messages);
                        item.Sucesso = false;
                        await _IPDOPTransfService.AtualizaTransfOP(pCodEstab, 3, item.Transferencia);
                    }
                    else
                        item.Motivo = Infra.Cross.Resources.MessagesResource.SeparateSuccess;
                }
            }

            resultData.Success = TransfResult.Transferencias.Any(r => r.Validado && r.Sucesso);
            resultData.TransfResults = TransfResult;

            return resultData;
        }

        private double GetSaldoLoteSeparacao(short pTipofifolote, IEnumerable<ESSaldoLoteOP> Itens, ESSaldoLoteOP ItemAtual, bool TemEstrutura)
        {
            var retorno = ItemAtual.SALDOESTOQUEPADRAO;

            if (TemEstrutura && pTipofifolote == 1)
            {
                retorno = Itens.Where(r => r.CODOPLOTEORIGINAL.ToString() == ItemAtual.CODOPLOTEORIGINAL.ToString())
                    .Sum(r => r.SALDOESTOQUEPADRAO);
            }
            return retorno;
        }

        private double MarcaItensFifoLoteOriginal(PDOPTransfItemResultVM pPDOPTransfItemResult, PDOPTransf pdTransf, IEnumerable<ESSaldoLoteOP> Itens, ESSaldoLoteOP ItemAtual, double pQtdNec)
        {
            var LinhasLote = Itens.Where(r => r.CODOPLOTEORIGINAL.ToString() == ItemAtual.CODOPLOTEORIGINAL.ToString());
            var QtdNec = pQtdNec;
            var QtdAcum = 0D;

            foreach (var Linha in LinhasLote)
            {
                if (Linha.SALDOESTOQUEPADRAO <= 0)
                    continue;

                if (Linha.SALDOESTOQUEPADRAO >= (QtdNec - QtdAcum))
                {
                    Linha.QTDETRANSFATUAL = QtdNec - QtdAcum;
                    QtdAcum = QtdNec;
                }
                else
                {
                    Linha.QTDETRANSFATUAL = Linha.SALDOESTOQUEPADRAO;
                    QtdAcum += Linha.SALDOESTOQUEPADRAO;
                }


                var pdTransfLote = new PDOPTransfLote
                {
                    codestab = pdTransf.codestab,
                    codtransf = pdTransf.codtransf,
                    lote = Linha.LOTE,
                    quantidade = Linha.QTDETRANSFATUAL
                };

                var result = SalvarLotesSelecionadoTransf(pPDOPTransfItemResult, pdTransfLote);
                if (!result)
                {
                    QtdAcum = ValorDeErroAoSalvar;
                    break;
                }

                if (QtdAcum >= QtdNec || QtdAcum == ValorDeErroAoSalvar)
                    break;
            }
            return QtdAcum;
        }

        bool SalvarLotesSelecionadoTransf(PDOPTransfItemResultVM pPDOPTransfItemResult, PDOPTransfLote pTransfLote)
        {
            pPDOPTransfItemResult.Transferencia.TransfLote.Add(pTransfLote);
            _IPDOPTransfLoteService.Add(pTransfLote);
            if (_IPDOPTransfLoteService.Errors.Any())
            {
                pPDOPTransfItemResult.Motivo = string.Join(" ", _IPDOPTransfLoteService.Errors.First().Messages);
                _IPDOPTransfService.Errors = _IPDOPTransfLoteService.Errors;
                return false;
            }

            return true;
        }
    }
}
