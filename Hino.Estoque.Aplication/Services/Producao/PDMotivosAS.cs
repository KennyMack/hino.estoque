﻿using Hino.Estoque.Aplication.Interfaces.Producao;
using Hino.Estoque.Domain.Producao.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Producao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.Services.Producao
{
    public class PDMotivosAS : BaseAppService<PDMotivos>, IPDMotivosAS
    {
        private readonly IPDMotivosService _IPDMotivosService;

        public PDMotivosAS(IPDMotivosService pIPDMotivosService) :
             base(pIPDMotivosService)
        {
            _IPDMotivosService = pIPDMotivosService;
        }
    }
}