using Hino.Estoque.Aplication.Interfaces.Fiscal;
using Hino.Estoque.Domain.Fiscal.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Fiscal;

namespace Hino.Estoque.Aplication.Services.Fiscal
{
    public class FSProdutoparamEstabAS : BaseAppService<FSProdutoparamEstab>, IFSProdutoparamEstabAS
    {
        private readonly IFSProdutoparamEstabService _IFSProdutoparamEstabService;

        public FSProdutoparamEstabAS(IFSProdutoparamEstabService pIFSProdutoparamEstabService) : 
             base(pIFSProdutoparamEstabService)
        {
            _IFSProdutoparamEstabService = pIFSProdutoparamEstabService;
        }
    }
}
