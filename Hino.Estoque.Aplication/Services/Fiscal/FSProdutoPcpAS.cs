using Hino.Estoque.Aplication.Interfaces.Fiscal;
using Hino.Estoque.Domain.Fiscal.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Fiscal;

namespace Hino.Estoque.Aplication.Services.Fiscal
{
    public class FSProdutoPcpAS : BaseAppService<FSProdutoPcp>, IFSProdutoPcpAS
    {
        private readonly IFSProdutoPcpService _IFSProdutoPcpService;

        public FSProdutoPcpAS(IFSProdutoPcpService pIFSProdutoPcpService) : 
             base(pIFSProdutoPcpService)
        {
            _IFSProdutoPcpService = pIFSProdutoPcpService;
        }
    }
}
