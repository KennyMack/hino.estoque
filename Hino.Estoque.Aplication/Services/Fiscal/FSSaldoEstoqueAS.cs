using Hino.Estoque.Aplication.Interfaces.Fiscal;
using Hino.Estoque.Domain.Fiscal.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Fiscal;

namespace Hino.Estoque.Aplication.Services.Fiscal
{
    public class FSSaldoEstoqueAS : BaseAppService<FSSaldoEstoque>, IFSSaldoEstoqueAS
    {
        private readonly IFSSaldoEstoqueService _IFSSaldoEstoqueService;

        public FSSaldoEstoqueAS(IFSSaldoEstoqueService pIFSSaldoEstoqueService) : 
             base(pIFSSaldoEstoqueService)
        {
            _IFSSaldoEstoqueService = pIFSSaldoEstoqueService;
        }
    }
}
