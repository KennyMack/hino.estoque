using Hino.Estoque.Aplication.Interfaces.Fiscal;
using Hino.Estoque.Domain.Fiscal.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Fiscal;

namespace Hino.Estoque.Aplication.Services.Fiscal
{
    public class FSLocalEstoqueAS : BaseAppService<FSLocalEstoque>, IFSLocalEstoqueAS
    {
        private readonly IFSLocalEstoqueService _IFSLocalEstoqueService;

        public FSLocalEstoqueAS(IFSLocalEstoqueService pIFSLocalEstoqueService) : 
             base(pIFSLocalEstoqueService)
        {
            _IFSLocalEstoqueService = pIFSLocalEstoqueService;
        }
    }
}
