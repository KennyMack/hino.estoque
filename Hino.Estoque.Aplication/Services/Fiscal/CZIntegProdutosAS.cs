﻿using Hino.Estoque.Aplication.Interfaces.Fiscal;
using Hino.Estoque.Domain.Fiscal.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Fiscal;
using Hino.Estoque.Infra.Cross.Utils.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.Services.Fiscal
{
    public class CZIntegProdutosAS : BaseAppService<CZIntegProdutos>, ICZIntegProdutosAS
    {
        private readonly ICZIntegProdutosService _ICZIntegProdutosService;

        public CZIntegProdutosAS(ICZIntegProdutosService pICZIntegProdutosService) :
             base(pICZIntegProdutosService)
        {
            _ICZIntegProdutosService = pICZIntegProdutosService;
        }

        public async Task<CZIntegProdutos> CreateAsync(CZIntegProdutos pProduct)
        {
            Errors.Clear();

            var ProductDb = (await _ICZIntegProdutosService.QueryAsync(r => r.PartNumber == pProduct.PartNumber)).FirstOrDefault();

            var ProductNew = ProductDb ?? new CZIntegProdutos();

            ProductNew.CopyProperties(pProduct);
            ProductNew.ProdIntegra = false;

            if (ProductDb == null)
            {
                ProductNew.IdIntegracao = await _ICZIntegProdutosService.NextSequenceAsync();
                if ((ProductNew.PartDescription ?? "").Length > 255)
                    ProductNew.PartDescription = ProductNew.PartDescription.Substring(0, 255);
                if ((ProductNew.Faturamento ?? "").Length > 255)
                    ProductNew.Faturamento = ProductNew.Faturamento.Substring(0, 255);
                if ((ProductNew.FinishID1 ?? "").Length > 255)
                    ProductNew.FinishID1 = ProductNew.FinishID1.Substring(0, 255);
                if ((ProductNew.FinishID2 ?? "").Length > 255)
                    ProductNew.FinishID2 = ProductNew.FinishID2.Substring(0, 255);
                if ((ProductNew.FinishID3 ?? "").Length > 255)
                    ProductNew.FinishID3 = ProductNew.FinishID3.Substring(0, 255);
                if ((ProductNew.ModificationID ?? "").Length > 255)
                    ProductNew.ModificationID = ProductNew.ModificationID.Substring(0, 255);
                if ((ProductNew.Classificacao ?? "").Length > 120)
                    ProductNew.Classificacao = ProductNew.Classificacao.Substring(0, 120);
                if ((ProductNew.CodWohner ?? "").Length > 30)
                    ProductNew.CodWohner = ProductNew.CodWohner.Substring(0, 30);
                if ((ProductNew.Dimensions ?? "").Length > 20)
                    ProductNew.Dimensions = ProductNew.Dimensions.Substring(0, 20);

                _ICZIntegProdutosService.Add(ProductNew);
                await _ICZIntegProdutosService.SaveChanges();
            }/*
            else
                _ICZIntegProdutosService.Update(ProductNew);*/


            Errors = _ICZIntegProdutosService.Errors;

            return ProductNew;
        }

        public async Task UpdateIntegratedAsync(CZIntegProdutos pProduct)
        {
            Errors.Clear();

            var ProductDb = (await _ICZIntegProdutosService.QueryAsync(r => r.IdIntegracao == pProduct.IdIntegracao)).FirstOrDefault();
            ProductDb.ProdIntegra = pProduct.ProdIntegra;
            ProductDb.DataIntegra = pProduct.DataIntegra;
            _ICZIntegProdutosService.Update(ProductDb);

            await _ICZIntegProdutosService.SaveChanges();
        }

        public async Task<IEnumerable<CZIntegProdutos>> GetDispIntegAsync() =>
            await _ICZIntegProdutosService.QueryAsync(r => !r.ProdIntegra);
    }
}
