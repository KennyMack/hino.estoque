﻿using Hino.Estoque.Aplication.Interfaces.Fiscal;
using Hino.Estoque.Domain.Fiscal.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Fiscal;
using Hino.Estoque.Infra.Cross.Utils.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.Services.Fiscal
{
    public class CZIntegrProdDetAS : BaseAppService<CZIntegrProdDet>, ICZIntegrProdDetAS
    {
        private readonly ICZIntegrProdDetService _ICZIntegrProdDetService;

        public CZIntegrProdDetAS(ICZIntegrProdDetService pICZIntegrProdDetService) :
             base(pICZIntegrProdDetService)
        {
            _ICZIntegrProdDetService = pICZIntegrProdDetService;
        }

        public async Task<CZIntegrProdDet> CreateAsync(CZIntegrProdDet pProduct)
        {
            Errors.Clear();

            var ProductDb = (await _ICZIntegrProdDetService.QueryAsync(r => r.PartNumber == pProduct.PartNumber)).FirstOrDefault();

            var ProductNew = ProductDb ?? new CZIntegrProdDet();

            ProductNew.CopyProperties(pProduct);

            if (ProductDb == null)
            {
                ProductNew.IdIntegDet = await _ICZIntegrProdDetService.NextSequenceAsync();

                if ((ProductNew.FiscalClass ?? "").Length > 40)
                    ProductNew.FiscalClass = ProductNew.FiscalClass.Substring(0, 40);
                if ((ProductNew.DiscountClass ?? "").Length > 40)
                    ProductNew.DiscountClass = ProductNew.DiscountClass.Substring(0, 40);
                if ((ProductNew.LeadtimeClass ?? "").Length > 40)
                    ProductNew.LeadtimeClass = ProductNew.LeadtimeClass.Substring(0, 40);

                _ICZIntegrProdDetService.Add(ProductNew);
                await _ICZIntegrProdDetService.SaveChanges();
            }/*
            else
                _ICZIntegrProdDetService.Update(ProductNew);*/


            Errors = _ICZIntegrProdDetService.Errors;

            return ProductNew;
        }

        public async Task<IEnumerable<CZIntegrProdDet>> GetDispIntegAsync(string pPartNumber) =>
            await _ICZIntegrProdDetService.QueryAsync(r => r.PartNumber == pPartNumber);
        
    }
}
