using AutoMapper;
using Hino.Estoque.Aplication.Interfaces.Fiscal;
using Hino.Estoque.Aplication.ViewModels.Fiscal;
using Hino.Estoque.Domain.Fiscal.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Fiscal;
using System.Configuration;
using System.Threading.Tasks;
using System.Linq;

namespace Hino.Estoque.Aplication.Services.Fiscal
{
    public class FSProdutoAS : BaseAppService<FSProduto>, IFSProdutoAS
    {
        private readonly IFSProdutoService _IFSProdutoService;
        private readonly IFSProdutoPcpService _IFSProdutoPcpService;

        public FSProdutoAS(IFSProdutoService pIFSProdutoService,
            IFSProdutoPcpService pIFSProdutoPcpService) : 
             base(pIFSProdutoService)
        {
            _IFSProdutoService = pIFSProdutoService;
            _IFSProdutoPcpService = pIFSProdutoPcpService;
        }

        public async Task<FSProdutoVM> GetProductByBarCodeAsync(short pCodEstab, string pBarCode)
        {
            var product = await _IFSProdutoPcpService.QueryAsync(r => r.codestab == pCodEstab &&
                (r.codproduto == pBarCode ||
                r.codgtin == pBarCode ||
                r.codgtinemb == pBarCode));

            if (!product.Any())
                return null;

            var result = Mapper.Map<FSProduto, FSProdutoVM>(await _IFSProdutoService.GetProductByBarCodeAsync(pCodEstab, product.First().codproduto));

            if (result != null)
            {
                result.estoquedestino =
                    ConfigurationManager.AppSettings.Get("DefaultStock") ?? "";
            }

            return result;
        }



    }
}
