using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using Hino.Estoque.Aplication.Interfaces.Estoque;
using Hino.Estoque.Aplication.ViewModels.Estoque;
using Hino.Estoque.Domain.Estoque.Interfaces.Services.Estoque;
using Hino.Estoque.Domain.Fiscal.Interfaces.Services;
using Hino.Estoque.Domain.Fiscal.Services;
using Hino.Estoque.Infra.Cross.Entities.Estoque;
using Hino.Estoque.Infra.Cross.Entities.Fiscal;
using Hino.Estoque.Infra.Cross.Utils.Exceptions;

namespace Hino.Estoque.Aplication.Services.Estoque
{
    public class ESInventDetAS : BaseAppService<ESInventDet>, IESInventDetAS
    {
        private readonly IESInventDetService _IESInventDetService;
        private readonly IESInventarioService _IESInventarioService;
        private readonly IESInventLoteService _IESInventLoteService;
        private readonly IFSSaldoEstoqueService _IFSSaldoEstoqueService;
        private readonly IFSProdutoPcpService _IFSProdutoPcpService;
        private readonly IESLoteSaldoService _IESLoteSaldoService;
        private readonly IESLoteService _IESLoteService;

        public ESInventDetAS(IESInventDetService pIESInventDetService,
            IFSSaldoEstoqueService pIFSSaldoEstoqueService,
            IESInventarioService pIESInventarioService,
            IFSProdutoPcpService pIFSProdutoPcpService,
            IESInventLoteService pIESInventLoteService,
            IESLoteSaldoService pIESLoteSaldoService,
            IESLoteService pIESLoteService) : 
             base(pIESInventDetService)
        {
            _IFSSaldoEstoqueService = pIFSSaldoEstoqueService;
            _IESInventDetService = pIESInventDetService;
            _IFSProdutoPcpService = pIFSProdutoPcpService;
            _IESInventLoteService = pIESInventLoteService;
            _IESLoteSaldoService = pIESLoteSaldoService;
            _IESLoteService = pIESLoteService;
            _IESInventarioService = pIESInventarioService;
        }

        public async Task<bool> ConfirmaContagemItens(short pCodEstab, decimal pCodInv, decimal pContagem, ESUpdateInventDetVM[] lstItensDet)
        {
            var inventario = await _IESInventarioService.QueryAsync(r =>
               r.codestab == pCodEstab &&
               r.codinv == pCodInv);

            if (!inventario.Any())
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { Infra.Cross.Resources.MessagesResource.InventoryInvalid }
                });
                return false;
            }

            var OnlyLastList = Convert.ToBoolean(ConfigurationManager.AppSettings.Get("OnlyLastList") ?? "false");
            if (OnlyLastList)
            {
                var LastCount = _IESInventarioService.GetMaxInventCount(pCodEstab, pCodInv);
                if (LastCount > pContagem)
                {
                    Errors.Add(new ModelException
                    {
                        ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                        Field = "Body",
                        Value = "",
                        Messages = new string[] { string.Format(Infra.Cross.Resources.MessagesResource.InventCountIs, LastCount) }
                    });
                    return false;
                }
            }

            foreach (var item in lstItensDet)
            {
                var StockBalance = await _IFSSaldoEstoqueService.SaldoEstoqueProduto(pCodEstab, item.codproduto, item.codestoque);
                var InventDet = await _IESInventDetService.GetById(pCodEstab, pCodInv, pContagem, item.codproduto, item.codestoque);

                var product = (await _IFSProdutoPcpService.QueryAsync(r => r.codestab == pCodEstab && r.codproduto == item.codproduto)).FirstOrDefault();

                InventDet.saldofisico = Convert.ToDecimal(StockBalance);
                InventDet.saldoinvent = item.saldoinvent;
                InventDet.codusuario = item.codusuario;
                InventDet.datacontagem = item.datacontagem;

                if (!product.rastreabilidade)
                    _IESInventDetService.Update(InventDet);
                else
                {
                    var lotes = (await _IESInventLoteService.QueryAsync(r => r.codestab == pCodEstab &&
                           r.codproduto == item.codproduto &&
                           r.codinv == pCodInv &&
                           r.codestoque == item.codestoque &&
                           r.contagem == pContagem)).OrderByDescending(r => r.lote);

                    if (!lotes.Any())
                    {
                        _IESInventDetService.Errors.Add(new ModelException
                        {
                            ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                            Field = "Body",
                            Value = "",
                            Messages = new string[] { $"Nenhum lote encontrado para o produto: {item.codproduto}" }
                        });
                        break;
                    }

                    var lotesCod = lotes.Select(r => r.lote);

                    var listaLotes = await _IESLoteService.QueryAsync(r => lotesCod.Contains(r.lote));

                    var codLote = listaLotes.Where(r => r.codunnegocio == null).Select(r => r.lote).Max();

                    var LoteAtualizar = lotes.Where(r => r.lote == codLote).First();

                    if (!string.IsNullOrEmpty(item.codlote))
                    {
                        try
                        {
                            LoteAtualizar = lotes.Where(r => r.lote == Convert.ToInt32(item.codlote)).FirstOrDefault();
                            if (LoteAtualizar == null)
                                throw new Exception($"Lote {item.codlote} n�o � v�lido.");
                        }
                        catch (Exception ex)
                        {
                            _IESInventDetService.Errors.Add(new ModelException
                            {
                                ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                                Field = "Body",
                                Value = "",
                                Messages = new string[] { ex.Message }
                            });
                            break;
                        }
                    }

                    foreach (var lote in lotes)
                    {
                        lote.eslote = null;
                        lote.saldolote = Convert.ToDecimal(await _IESLoteSaldoService.SaldoEstoqueLote(pCodEstab, lote.lote, lote.codestoque));
                        lote.saldoinvent = 0;
                        _IESInventLoteService.Update(lote);
                    }

                    var StockLoteBalance = await _IESLoteSaldoService.SaldoEstoqueLote(pCodEstab, LoteAtualizar.lote, item.codestoque);
                    LoteAtualizar.saldolote = Convert.ToDecimal(StockLoteBalance);
                    LoteAtualizar.saldoinvent = item.saldoinvent;
                    _IESInventLoteService.Update(LoteAtualizar);
                    if (!_IESInventLoteService.Errors.Any())
                    {
                        await _IESInventLoteService.SaveChanges();

                        InventDet.saldoinvent = (await _IESInventLoteService.QueryAsync(r => 
                            r.codestab == pCodEstab &&
                            r.codproduto == item.codproduto &&
                            r.codinv == pCodInv &&
                            r.codestoque == item.codestoque &&
                            r.contagem == pContagem)).Sum(r => r.saldoinvent);
                        _IESInventDetService.Update(InventDet);
                    }
                }
            }

            if (_IESInventDetService.Errors.Count > 0)
            {
                this.Errors = _IESInventDetService.Errors;
                _IESInventDetService.RollBackChanges();
                _IESInventLoteService.RollBackChanges();
                return false;
            }
            else
                await _IESInventDetService.SaveChanges();

            return true;
        }

        public async Task<ESInventDet> GetInventarioItemAsync(short pCodEstab, ESInventDetItemVM pDados)
        {
            var result = await _IESInventDetService.GetById(pCodEstab, pDados.codinv, pDados.contagem, pDados.codproduto, pDados.codestoque);

            if (result == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { "Produto n�o encontrado na lista e/ou contagem" }
                });
            }

            return result;
        }

        public async Task<IEnumerable<ESInventDet>> GetItensInventario(short pCodEstab, decimal pCodInv, decimal pContagem) =>
            await _IESInventDetService.GetItensInventario(pCodEstab, pCodInv, pContagem);
    }
}
