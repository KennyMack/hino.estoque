using Hino.Estoque.Aplication.Interfaces.Estoque;
using Hino.Estoque.Domain.Estoque.Interfaces.Services.Estoque;
using Hino.Estoque.Infra.Cross.Entities.Estoque;

namespace Hino.Estoque.Aplication.Services.Estoque
{
    public class ESInventLoteAS : BaseAppService<ESInventLote>, IESInventLoteAS
    {
        private readonly IESInventLoteService _IESInventLoteService;

        public ESInventLoteAS(IESInventLoteService pIESInventLoteService) : 
             base(pIESInventLoteService)
        {
            _IESInventLoteService = pIESInventLoteService;
        }
    }
}
