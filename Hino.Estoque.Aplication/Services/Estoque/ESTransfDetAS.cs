using Hino.Estoque.Aplication.Interfaces.Estoque;
using Hino.Estoque.Domain.Estoque.Interfaces.Services.Estoque;
using Hino.Estoque.Infra.Cross.Entities.Estoque;

namespace Hino.Estoque.Aplication.Services.Estoque
{
    public class ESTransfDetAS : BaseAppService<ESTransfDet>, IESTransfDetAS
    {
        private readonly IESTransfDetService _IESTransfDetService;

        public ESTransfDetAS(IESTransfDetService pIESTransfDetService) : 
             base(pIESTransfDetService)
        {
            _IESTransfDetService = pIESTransfDetService;
        }
    }
}
