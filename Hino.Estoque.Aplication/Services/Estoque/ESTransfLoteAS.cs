using Hino.Estoque.Aplication.Interfaces.Estoque;
using Hino.Estoque.Domain.Estoque.Interfaces.Services.Estoque;
using Hino.Estoque.Infra.Cross.Entities.Estoque;

namespace Hino.Estoque.Aplication.Services.Estoque
{
    public class ESTransfLoteAS : BaseAppService<ESTransfLote>, IESTransfLoteAS
    {
        private readonly IESTransfLoteService _IESTransfLoteService;

        public ESTransfLoteAS(IESTransfLoteService pIESTransfLoteService) : 
             base(pIESTransfLoteService)
        {
            _IESTransfLoteService = pIESTransfLoteService;
        }
    }
}
