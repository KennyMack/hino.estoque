using AutoMapper;
using Hino.Estoque.Aplication.Interfaces.Estoque;
using Hino.Estoque.Aplication.ViewModels.Estoque;
using Hino.Estoque.Domain.Estoque.Interfaces.Services.Estoque;
using Hino.Estoque.Domain.Fiscal.Interfaces.Services;
using Hino.Estoque.Domain.Gerais.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Estoque;
using Hino.Estoque.Infra.Cross.Utils;
using Hino.Estoque.Infra.Cross.Utils.Exceptions;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.Services.Estoque
{
    public class ESKardexAS : BaseAppService<ESKardex>, IESKardexAS
    {
        private readonly IESKardexService _IESKardexService;
        private readonly IFSProdutoPcpService _IFSProdutoPcpService;
        private readonly IESLoteService _IESLoteService;
        private readonly IESLoteSaldoService _IESLoteSaldoService;
        private readonly IFSSaldoEstoqueService _IFSSaldoEstoqueService;
        private readonly IFSLocalEstoqueService _IFSLocalEstoqueService;
        private readonly IESRequisicaoService _IESRequisicaoService;
        private readonly IGEFuncionariosService _IGEFuncionariosService;

        public ESKardexAS(IESKardexService pIESKardexService,
            IFSProdutoPcpService pIFSProdutoPcpService,
            IESLoteService pIESLoteService,
            IESLoteSaldoService pIESLoteSaldoService,
            IFSSaldoEstoqueService pIFSSaldoEstoqueService,
            IFSLocalEstoqueService pIFSLocalEstoqueService,
            IGEFuncionariosService pIGEFuncionariosService,
            IESRequisicaoService pIESRequisicaoService) : 
             base(pIESKardexService)
        {
            _IGEFuncionariosService = pIGEFuncionariosService;
            _IESRequisicaoService = pIESRequisicaoService;
            _IESKardexService = pIESKardexService;
            _IFSProdutoPcpService = pIFSProdutoPcpService;
            _IESLoteService = pIESLoteService;
            _IESLoteSaldoService = pIESLoteSaldoService;
            _IFSSaldoEstoqueService = pIFSSaldoEstoqueService;
            _IFSLocalEstoqueService = pIFSLocalEstoqueService;
        }

        public async Task<bool> CreateProdTransferenciaAsync(ESCreateProdTransferenciaVM pCreateTransf)
        {
            _IESKardexService.Errors.Clear();

            if (pCreateTransf.CodEstab <= 0)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { Infra.Cross.Resources.MessagesResource.InvalidEstablishmentKeyOrNull }
                });

                return false;
            }

            if (pCreateTransf.Quantidade <= 0)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { Infra.Cross.Resources.MessagesResource.InvalidQuantity }
                });

                return false;
            }

            var estoqueOri = await _IFSSaldoEstoqueService.QueryAsync(r => r.codestoque == pCreateTransf.CodEstoqueOri && r.codestab == pCreateTransf.CodEstab);
            if (!estoqueOri.Any())
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { string.Format(Infra.Cross.Resources.MessagesResource.InvalidStockName, pCreateTransf.CodEstoqueOri) }
                });

                return false;
            }

            var estoqueDest = await _IFSSaldoEstoqueService.QueryAsync(r => r.codestoque == pCreateTransf.CodEstoqueDest && r.codestab == pCreateTransf.CodEstab);
            if (!estoqueDest.Any())
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { string.Format(Infra.Cross.Resources.MessagesResource.InvalidStockName, pCreateTransf.CodEstoqueOri) }
                });

                return false;
            }


            var product = await _IFSProdutoPcpService.QueryAsync(r => r.codestab == pCreateTransf.CodEstab &&
                (r.codproduto == pCreateTransf.CodProduto ||
                r.codgtin == pCreateTransf.CodProduto ||
                r.codgtinemb == pCreateTransf.CodProduto));

            if (!product.Any())
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { Infra.Cross.Resources.MessagesResource.ProductIdNotFound }
                });

                return false;
            }

            pCreateTransf.CodProduto = product.First().codproduto;

            if (pCreateTransf.Lote != 0)
            {
                var lote = await _IESLoteService.QueryAsync(r => r.lote == pCreateTransf.Lote);
                if (!lote.Any())
                {
                    Errors.Add(new ModelException
                    {
                        ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                        Field = "Body",
                        Value = "",
                        Messages = new string[] { Infra.Cross.Resources.MessagesResource.InvalidLote }
                    });

                    return false;
                }
                
                var LoteItem = lote.First();

                if (!LoteItem.status.In("A", "M"))
                {
                    Errors.Add(new ModelException
                    {
                        ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                        Field = "Body",
                        Value = "",
                        Messages = new string[] { Infra.Cross.Resources.MessagesResource.InvalidLoteStatus }
                    });

                    return false;
                }

                var idEstab = Convert.ToInt16(pCreateTransf.CodEstab);
                var saldoLote = (await _IESLoteSaldoService.QueryAsync(r => 
                        r.lote == LoteItem.lote &&
                        r.codestab == idEstab &&
                        r.codestoque == pCreateTransf.CodEstoqueOri)).FirstOrDefault();

                if (saldoLote == null)
                {
                    Errors.Add(new ModelException
                    {
                        ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                        Field = "Body",
                        Value = "",
                        Messages = new string[] { Infra.Cross.Resources.MessagesResource.InvalidLote }
                    });

                    return false;
                }

                if (saldoLote.saldoLote < pCreateTransf.Quantidade)
                {
                    Errors.Add(new ModelException
                    {
                        ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                        Field = "Body",
                        Value = "",
                        Messages = new string[] { 
                            string.Format(Infra.Cross.Resources.MessagesResource.QtdLoteInvalid,
                                saldoLote.saldoLote,
                                pCreateTransf.Quantidade
                            ) }
                    });

                    return false;
                }
            }

            var ret = await _IESKardexService.CreateProdTransferenciaAsync(
                Mapper.Map<ESCreateProdTransferenciaVM, ESCreateProdTransferencia>(pCreateTransf));

            this.Errors = _IESKardexService.Errors;

            return ret;
        }


        public async Task<bool> CreateProdEndTransferenciaAsync(ESCreateProdEndTransfVM pCreateTransf)
        {
            _IESKardexService.Errors.Clear();

            var exists = await _IFSLocalEstoqueService.QueryAsync(r => r.codestoque == pCreateTransf.CodEstoqueOri);
            if (!exists.Any())
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { string.Format(Infra.Cross.Resources.ResultMessageResource.StockLocationDoesNotExists, pCreateTransf.CodEstoqueOri) }
                });

                return false;
            }
            else
            {
                var result = await _IESKardexService.ValidaPermisMovEstoqueAsync(pCreateTransf.CodEstab, pCreateTransf.CodUsuario, DateTime.Now, pCreateTransf.CodEstoqueOri);
                var message = "";
                switch (result)
                {
                    case EPermisEstoque.SemPermissao:
                        message = string.Format(Infra.Cross.Resources.ResultMessageResource.UserHasNoPermissionInStock, pCreateTransf.CodEstoqueOri);
                        break;
                    case EPermisEstoque.DataInvalida:
                        message = Infra.Cross.Resources.ResultMessageResource.MaximumDaysDiff;
                        break;
                }

                if (!message.IsEmpty())
                {
                    Errors.Add(new ModelException
                    {
                        ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                        Field = "Body",
                        Value = "",
                        Messages = new string[] { string.Format(Infra.Cross.Resources.ResultMessageResource.StockValueMustBeGreatherThanZero, pCreateTransf.CodEstoqueOri) }
                    });
                    return false;
                }
            }

            foreach (var item in pCreateTransf.Items)
            {
                exists = await _IFSLocalEstoqueService.QueryAsync(r => r.codestoque == item.CodEstoqueDest);
                if (!exists.Any())
                {
                    Errors.Add(new ModelException
                    {
                        ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                        Field = "Body",
                        Value = "",
                        Messages = new string[] { string.Format(Infra.Cross.Resources.ResultMessageResource.StockLocationDoesNotExists, item.CodEstoqueDest) }
                    });
                }
                else if (item.Quantidade <= 0 && exists.Any())
                {
                    Errors.Add(new ModelException
                    {
                        ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                        Field = "Body",
                        Value = "",
                        Messages = new string[] { string.Format(Infra.Cross.Resources.ResultMessageResource.StockValueMustBeGreatherThanZero, item.CodEstoqueDest) }
                    });
                }
                else
                {
                    var result = await _IESKardexService.ValidaPermisMovEstoqueAsync(pCreateTransf.CodEstab, pCreateTransf.CodUsuario, DateTime.Now, item.CodEstoqueDest);
                    var message = "";
                    switch (result)
                    {
                        case EPermisEstoque.SemPermissao:
                            message = string.Format(Infra.Cross.Resources.ResultMessageResource.UserHasNoPermissionInStock, item.CodEstoqueDest);
                            break;
                        case EPermisEstoque.DataInvalida:
                            message = Infra.Cross.Resources.ResultMessageResource.MaximumDaysDiff;
                            break;
                    }
                    if (!message.IsEmpty())
                    {
                        Errors.Add(new ModelException
                        {
                            ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                            Field = "Body",
                            Value = "",
                            Messages = new string[] { string.Format(Infra.Cross.Resources.ResultMessageResource.StockValueMustBeGreatherThanZero, item.CodEstoqueDest) }
                        });
                    }
                }
            }

            if (Errors.Any())
                return false;


            var prodExists = await _IFSProdutoPcpService.QueryAsync(r => r.codestab == pCreateTransf.CodEstab &&
            r.codproduto == pCreateTransf.CodProduto);
            if (!prodExists.Any())
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { Infra.Cross.Resources.MessagesResource.ProductIdNotFound }
                });
                return false;
            }

            var EstoqueSaldo = (await _IFSSaldoEstoqueService.QueryAsync(r =>
                r.codestab == pCreateTransf.CodEstab &&
                r.codproduto == pCreateTransf.CodProduto &&
                r.codestoque == pCreateTransf.CodEstoqueOri
            )).FirstOrDefault();

            var TotalTransf = pCreateTransf.Items.Sum(r => r.Quantidade);

            if (TotalTransf > EstoqueSaldo.saldoEstoque)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { string.Format(Infra.Cross.Resources.MessagesResource.StockIsNotEnough, pCreateTransf.CodEstoqueOri) }
                });

                return false;
            }

            if (!pCreateTransf.Items.Any())
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { Infra.Cross.Resources.ResultMessageResource.TransferenceHasNoItems }
                });

                return false;
            }

            foreach (var item in pCreateTransf.Items)
            {
                if (item.Quantidade <= 0)
                    continue;

                var lotes = await _IESLoteSaldoService.GetLotesUnidadeNegAsync(pCreateTransf.CodEstab, pCreateTransf.CodProduto, pCreateTransf.CodEstoqueOri);

                if (lotes.Any())
                {
                    var QtdNec = item.Quantidade;
                    var QtdAcum = 0M;

                    foreach (var Linha in lotes)
                    {
                        if (Linha.Saldo >= (QtdNec - QtdAcum))
                        {
                            Linha.QtdSeparada = QtdNec - QtdAcum;
                            QtdAcum = QtdNec;
                        }
                        else
                        {
                            Linha.QtdSeparada = Linha.Saldo;
                            QtdAcum += Linha.Saldo;
                        }

                        try
                        {
                            var ret = await _IESKardexService.CreateProdTransferenciaAsync(
                                new ESCreateProdTransferencia
                                {
                                    CodEstab = pCreateTransf.CodEstab,
                                    CodEstoqueDest = item.CodEstoqueDest,
                                    CodEstoqueOri = pCreateTransf.CodEstoqueOri,
                                    CodUsuario = pCreateTransf.CodUsuario,
                                    Quantidade = Linha.QtdSeparada,
                                    CodProduto = pCreateTransf.CodProduto,
                                    Lote = Linha.CodLote
                                });

                            this.Errors = _IESKardexService.Errors;
                        }
                        catch (Exception e)
                        {
                            Errors.Add(new ModelException
                            {
                                ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                                Field = "Body",
                                Value = "",
                                Messages = new string[] { e.Message }
                            });
                            break;
                        }

                        if (QtdAcum >= QtdNec)
                            break;
                    }
                }
                else
                {
                    try
                    {
                        var ret = await _IESKardexService.CreateProdTransferenciaAsync(
                            new ESCreateProdTransferencia
                            {
                                CodEstab = pCreateTransf.CodEstab,
                                CodEstoqueDest = item.CodEstoqueDest,
                                CodEstoqueOri = pCreateTransf.CodEstoqueOri,
                                CodUsuario = pCreateTransf.CodUsuario,
                                Quantidade = item.Quantidade,
                                CodProduto = pCreateTransf.CodProduto,
                                Lote = 0
                            });

                        this.Errors = _IESKardexService.Errors;
                    }
                    catch (Exception e)
                    {
                        Errors.Add(new ModelException
                        {
                            ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                            Field = "Body",
                            Value = "",
                            Messages = new string[] { e.Message }
                        });
                        break;
                    }
                }

            }

            return !Errors.Any();
        }

        public async Task<ESResultRequisicaoVM> AprovarRequisicaoEstoqueAsync(ESUpdateRequisicaoVM pRequisicao)
        {
            var retorno = new ESResultRequisicaoVM();
            _IESKardexService.Errors.Clear();
            Errors.Clear();
            var Funcionario = (await _IGEFuncionariosService.QueryAsync(r =>
             r.codestab == pRequisicao.codestab &&
             r.codfuncionario == pRequisicao.codfuncionario)).FirstOrDefault();

            if (Funcionario == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "CodFuncionario",
                    Value = pRequisicao.codfuncionario.ToString(),
                    Messages = new string[] { Infra.Cross.Resources.MessagesResource.EmployeeNotFound }
                });
                return null;
            }

            if (!Funcionario.status)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "CodFuncionario",
                    Value = pRequisicao.codfuncionario.ToString(),
                    Messages = new string[] { Infra.Cross.Resources.MessagesResource.InactiveEmployee }
                });
                return null;
            }

            var Requisicao = (await _IESRequisicaoService.QueryAsync(r =>
                 r.codestab == pRequisicao.codestab &&
                 r.codrequisicao == pRequisicao.codrequisicao)).FirstOrDefault();

            var prodExists = (await _IFSProdutoPcpService.QueryAsync(r =>
                r.codestab == Requisicao.codestab &&
                r.codproduto == Requisicao.codproduto)).FirstOrDefault();

            if (prodExists == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { Infra.Cross.Resources.MessagesResource.ProductIdNotFound }
                });
                return null;
            }

            if (Requisicao == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "CodRequisicao",
                    Value = pRequisicao.codrequisicao.ToString(),
                    Messages = new string[] { Infra.Cross.Resources.MessagesResource.RequestNotFound }
                });
                return null;
            }

            var exists = await _IFSLocalEstoqueService.QueryAsync(r => r.codestoque == Requisicao.codestoque);
            if (!exists.Any())
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { string.Format(Infra.Cross.Resources.ResultMessageResource.StockLocationDoesNotExists, Requisicao.codestoque) }
                });

                return null;
            }
            else
            {
                var result = await _IESKardexService.ValidaPermisMovEstoqueAsync(Requisicao.codestab, Funcionario.codusuario, DateTime.Now, Requisicao.codestoque);
                var message = "";
                switch (result)
                {
                    case EPermisEstoque.SemPermissao:
                        message = string.Format(Infra.Cross.Resources.ResultMessageResource.UserHasNoPermissionInStock, Requisicao.codestoque);
                        break;
                    case EPermisEstoque.DataInvalida:
                        message = Infra.Cross.Resources.ResultMessageResource.MaximumDaysDiff;
                        break;
                }

                if (!message.IsEmpty())
                {
                    Errors.Add(new ModelException
                    {
                        ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                        Field = "Body",
                        Value = "",
                        Messages = new string[] { string.Format(Infra.Cross.Resources.ResultMessageResource.StockValueMustBeGreatherThanZero, Requisicao.codestoque) }
                    });
                    return null;
                }
            }

            if (Errors.Any())
                return null;

            var EstoqueSaldo = (await _IFSSaldoEstoqueService.QueryAsync(r =>
                r.codestab == Requisicao.codestab &&
                r.codproduto == Requisicao.codproduto &&
                r.codestoque == Requisicao.codestoque
            )).FirstOrDefault();

            if (Requisicao.quantidade > EstoqueSaldo.saldoEstoque)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { string.Format(Infra.Cross.Resources.MessagesResource.StockIsNotEnough, Requisicao.codestoque) }
                });

                return null;
            }

            if (prodExists.rastreabilidade)
            {
                var lotes = await _IESLoteSaldoService.GetLotesUnidadeNegAsync(Requisicao.codestab, Requisicao.codproduto, Requisicao.codestoque);

                if (lotes.Any())
                {
                    var QtdNec = Requisicao.quantidade;
                    var QtdAcum = 0M;

                    foreach (var Linha in lotes)
                    {
                        if (Linha.Saldo >= (QtdNec - QtdAcum))
                        {
                            Linha.QtdSeparada = QtdNec - QtdAcum;
                            QtdAcum = QtdNec;
                        }
                        else
                        {
                            Linha.QtdSeparada = Linha.Saldo;
                            QtdAcum += Linha.Saldo;
                        }

                        try
                        {
                            var ret = await _IESKardexService.GeraMovimentoRequisicaoAsync(
                                Requisicao.codestab, Requisicao.codrequisicao, Funcionario.codusuario,
                                Linha.CodLote, Linha.QtdSeparada
                            );

                            retorno.Lotes.Add(new ESResultRequisicaoLoteVM
                            {
                                CodEstoque = Linha.CodEstoque,
                                Lote = Linha.CodLote,
                                Quantidade = Linha.QtdSeparada
                            });

                            this.Errors = _IESKardexService.Errors;
                        }
                        catch (Exception e)
                        {
                            Errors.Add(new ModelException
                            {
                                ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                                Field = "Body",
                                Value = "",
                                Messages = new string[] { e.Message }
                            });
                            break;
                        }

                        if (QtdAcum >= QtdNec)
                            break;
                    }
                }
            }
            else
            {
                try
                {
                    var ret = await _IESKardexService.GeraMovimentoRequisicaoAsync(
                        Requisicao.codestab, Requisicao.codrequisicao, Funcionario.codusuario,
                        0, Requisicao.quantidade
                    );

                    this.Errors = _IESKardexService.Errors;
                }
                catch (Exception e)
                {
                    Errors.Add(new ModelException
                    {
                        ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                        Field = "Body",
                        Value = "",
                        Messages = new string[] { e.Message }
                    });
                    return null;
                }
            }

            if (!Errors.Any())
            {
                await _IESKardexService.SaveChanges();

                Requisicao.codusuaprov = Funcionario.codusuario;
                Requisicao.status = 1;

                _IESRequisicaoService.Update(Requisicao);
                await _IESRequisicaoService.SaveChanges();
            }

            retorno.Sucesso = !Errors.Any();
            retorno.CodProduto = Requisicao.codproduto;
            retorno.CodEstoque = Requisicao.codestoque;
            retorno.Quantidade = Requisicao.quantidade;

            return retorno;
        }

        public async Task<ESResultRequisicaoVM> ReprovarRequisicaoEstoqueAsync(ESUpdateRequisicaoVM pRequisicao)
        {
            Errors.Clear();
            var Funcionario = (await _IGEFuncionariosService.QueryAsync(r =>
             r.codestab == pRequisicao.codestab &&
             r.codfuncionario == pRequisicao.codfuncionario)).FirstOrDefault();

            if (Funcionario == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "CodFuncionario",
                    Value = pRequisicao.codfuncionario.ToString(),
                    Messages = new string[] { Infra.Cross.Resources.MessagesResource.EmployeeNotFound }
                });
                return null;
            }

            if (!Funcionario.status)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "CodFuncionario",
                    Value = pRequisicao.codfuncionario.ToString(),
                    Messages = new string[] { Infra.Cross.Resources.MessagesResource.InactiveEmployee }
                });
                return null;
            }

            var Requisicao = (await _IESRequisicaoService.QueryAsync(r =>
                 r.codestab == pRequisicao.codestab &&
                 r.codrequisicao == pRequisicao.codrequisicao)).FirstOrDefault();

            if (Requisicao == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "CodRequisicao",
                    Value = pRequisicao.codrequisicao.ToString(),
                    Messages = new string[] { Infra.Cross.Resources.MessagesResource.RequestNotFound }
                });
                return null;
            }

            Requisicao.codusuaprov = Funcionario.codusuario;
            Requisicao.status = 2;

            _IESRequisicaoService.Update(Requisicao);
            await _IESRequisicaoService.SaveChanges();

            return new ESResultRequisicaoVM
            {
                Sucesso = true,
                CodProduto = Requisicao.codproduto,
                CodEstoque = Requisicao.codestoque,
                Quantidade = Requisicao.quantidade
            };
        }
    }
}
