using Hino.Estoque.Aplication.Interfaces.Estoque;
using Hino.Estoque.Domain.Estoque.Interfaces.Services.Estoque;
using Hino.Estoque.Infra.Cross.Entities.Estoque;

namespace Hino.Estoque.Aplication.Services.Estoque
{
    public class ESTransferenciaAS : BaseAppService<ESTransferencia>, IESTransferenciaAS
    {
        private readonly IESTransferenciaService _IESTransferenciaService;

        public ESTransferenciaAS(IESTransferenciaService pIESTransferenciaService) : 
             base(pIESTransferenciaService)
        {
            _IESTransferenciaService = pIESTransferenciaService;
        }
    }
}
