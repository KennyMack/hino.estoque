using Hino.Estoque.Aplication.Interfaces.Estoque;
using Hino.Estoque.Domain.Estoque.Interfaces.Services.Estoque;
using Hino.Estoque.Infra.Cross.Entities.Estoque;

namespace Hino.Estoque.Aplication.Services.Estoque
{
    public class ESLoteAS : BaseAppService<ESLote>, IESLoteAS
    {
        private readonly IESLoteService _IESLoteService;

        public ESLoteAS(IESLoteService pIESLoteService) : 
             base(pIESLoteService)
        {
            _IESLoteService = pIESLoteService;
        }
    }
}
