using Hino.Estoque.Aplication.Interfaces.Estoque;
using Hino.Estoque.Domain.Estoque.Interfaces.Services.Estoque;
using Hino.Estoque.Infra.Cross.Entities.Estoque;

namespace Hino.Estoque.Aplication.Services.Estoque
{
    public class ESInventTercAS : BaseAppService<ESInventTerc>, IESInventTercAS
    {
        private readonly IESInventTercService _IESInventTercService;

        public ESInventTercAS(IESInventTercService pIESInventTercService) : 
             base(pIESInventTercService)
        {
            _IESInventTercService = pIESInventTercService;
        }
    }
}
