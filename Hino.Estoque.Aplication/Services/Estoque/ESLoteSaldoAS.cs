using Hino.Estoque.Aplication.Interfaces.Estoque;
using Hino.Estoque.Domain.Estoque.Interfaces.Services.Estoque;
using Hino.Estoque.Infra.Cross.Entities.Estoque;

namespace Hino.Estoque.Aplication.Services.Estoque
{
    public class ESLoteSaldoAS : BaseAppService<ESLoteSaldo>, IESLoteSaldoAS
    {
        private readonly IESLoteSaldoService _IESLoteSaldoService;

        public ESLoteSaldoAS(IESLoteSaldoService pIESLoteSaldoService) : 
             base(pIESLoteSaldoService)
        {
            _IESLoteSaldoService = pIESLoteSaldoService;
        }
    }
}
