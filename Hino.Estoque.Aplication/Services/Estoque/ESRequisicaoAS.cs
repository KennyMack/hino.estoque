﻿using Hino.Estoque.Aplication.Interfaces.Estoque;
using Hino.Estoque.Aplication.Interfaces.Fiscal;
using Hino.Estoque.Aplication.Services.Fiscal;
using Hino.Estoque.Aplication.ViewModels.Estoque;
using Hino.Estoque.Domain.Estoque.Interfaces.Services.Estoque;
using Hino.Estoque.Domain.Gerais.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Estoque;
using Hino.Estoque.Infra.Cross.Utils;
using Hino.Estoque.Infra.Cross.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.Services.Estoque
{
    public class ESRequisicaoAS : BaseAppService<ESRequisicao>, IESRequisicaoAS
    {
        private readonly IESRequisicaoService _IESRequisicaoService;
        private readonly IFSProdutoAS _IFSProdutoAS;
        private readonly IFSSaldoEstoqueAS _IFSSaldoEstoqueAS;
        private readonly IFSLocalEstoqueAS _IFSLocalEstoqueAS;
        private readonly IFSProdutoparamEstabAS _IFSProdutoparamEstabAS;
        private readonly IGEFuncionariosService _IGEFuncionariosService;

        public ESRequisicaoAS(IESRequisicaoService pIESRequisicaoService,
            IGEFuncionariosService pIGEFuncionariosService,
            IFSProdutoAS pIFSProdutoAS,
            IFSProdutoparamEstabAS pIFSProdutoparamEstabAS,
            IFSSaldoEstoqueAS pIFSSaldoEstoqueAS,
            IFSLocalEstoqueAS pIFSLocalEstoqueAS
            ) :
             base(pIESRequisicaoService)
        {
            _IFSSaldoEstoqueAS = pIFSSaldoEstoqueAS;
            _IFSLocalEstoqueAS = pIFSLocalEstoqueAS;
            _IFSProdutoparamEstabAS = pIFSProdutoparamEstabAS;
            _IFSProdutoAS = pIFSProdutoAS;
            _IGEFuncionariosService = pIGEFuncionariosService;
            _IESRequisicaoService = pIESRequisicaoService;
        }

        public async Task<ESRequisicao> Create(ESCreateRequisicaoVM pRequisicao)
        {
            Errors.Clear();
            var Funcionario = (await _IGEFuncionariosService.QueryAsync(r =>
             r.codestab == pRequisicao.codestab &&
             r.codfuncionario == pRequisicao.codfuncionario)).FirstOrDefault();

            if (Funcionario == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "CodFuncionario",
                    Value = pRequisicao.codfuncionario.ToString(),
                    Messages = new string[] { Infra.Cross.Resources.MessagesResource.EmployeeNotFound }
                });
                return null;
            }

            if (!Funcionario.status)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "CodFuncionario",
                    Value = pRequisicao.codfuncionario.ToString(),
                    Messages = new string[] { Infra.Cross.Resources.MessagesResource.InactiveEmployee }
                });
                return null;
            }

            if (Funcionario.codccusto.IsEmpty())
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "CodFuncionario",
                    Value = Funcionario.codccusto,
                    Messages = new string[] { Infra.Cross.Resources.MessagesResource.CodCCustoNotFound }
                });
                return null;
            }

            var Produto = (await _IFSProdutoAS.QueryAsync(r =>
                r.codproduto == pRequisicao.codproduto)).FirstOrDefault();
            var ProdutoParam = (await _IFSProdutoparamEstabAS.QueryAsync(r =>
                r.codestab == pRequisicao.codestab &&
                r.codproduto == pRequisicao.codproduto)).FirstOrDefault();

            if (Produto == null || ProdutoParam == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "CodProduto",
                    Value = pRequisicao.codproduto,
                    Messages = new string[] { Infra.Cross.Resources.MessagesResource.ProductIdNotFound }
                });
                return null;
            }

            if (ProdutoParam.status == 0)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "CodProduto",
                    Value = pRequisicao.codproduto,
                    Messages = new string[] { Infra.Cross.Resources.MessagesResource.InactiveProduct }
                });
                return null;
            }

            var LocalEstoque = (await _IFSLocalEstoqueAS.QueryAsync(r => r.codestoque == pRequisicao.codestoque)).FirstOrDefault();
            var SaldoEstoque = (await _IFSSaldoEstoqueAS.QueryAsync(r => 
                        r.codestoque == pRequisicao.codestoque &&
                        r.codproduto == pRequisicao.codproduto &&
                        r.codestab == pRequisicao.codestab)).FirstOrDefault();

            if (LocalEstoque == null || SaldoEstoque == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "CodEstoque",
                    Value = pRequisicao.codestoque,
                    Messages = new string[] {
                        string.Format(
                            Infra.Cross.Resources.MessagesResource.InvalidStockName,
                            pRequisicao.codestoque) 
                    }
                });
                return null;
            }

            if (SaldoEstoque.saldoEstoque < pRequisicao.quantidade)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "CodEstoque",
                    Value = pRequisicao.codestoque,
                    Messages = new string[] {
                        string.Format(
                            Infra.Cross.Resources.MessagesResource.StockIsNotEnough,
                            pRequisicao.codestoque)
                    }
                });
                return null;
            }

            var RequisicaoNova = new ESRequisicao
            {
                codrequisicao = Convert.ToInt32(await _IESRequisicaoService.NextSequenceAsync()),
                codestab = pRequisicao.codestab,
                codccusto = Funcionario.codccusto,
                codproduto = pRequisicao.codproduto,
                data = DateTime.Now,
                codfuncionario = pRequisicao.codfuncionario,
                status = 0,
                observacao = pRequisicao.observacao,
                codestoque = pRequisicao.codestoque,
                quantidade = pRequisicao.quantidade
            };

            _IESRequisicaoService.Add(RequisicaoNova);
            await _IESRequisicaoService.SaveChanges();

            if (_IESRequisicaoService.Errors.Any())
            {
                Errors = _IESRequisicaoService.Errors;
                return null;
            }

            return (await _IESRequisicaoService.QueryAsync(r => 
                r.codestab == RequisicaoNova.codestab &&
                r.codrequisicao == RequisicaoNova.codrequisicao,
                s => s.FSProduto,
                r => r.FSProdutoparamEstab)).FirstOrDefault() ?? RequisicaoNova;
        }
    }
}
