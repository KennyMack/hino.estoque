using AutoMapper;
using Hino.Estoque.Aplication.Interfaces.Estoque;
using Hino.Estoque.Aplication.ViewModels.Estoque;
using Hino.Estoque.Domain.Estoque.Interfaces.Services.Estoque;
using Hino.Estoque.Infra.Cross.Entities.Estoque;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.Services.Estoque
{
    public class ESInventarioAS : BaseAppService<ESInventario>, IESInventarioAS
    {
        private readonly IESInventarioService _IESInventarioService;

        public ESInventarioAS(IESInventarioService pIESInventarioService) : 
             base(pIESInventarioService)
        {
            _IESInventarioService = pIESInventarioService;
        }

        public async Task<IEnumerable<ESInventario>> GetInventariosPendentes(short pCodEstab) =>
            await _IESInventarioService.GetInventariosPendentes(pCodEstab);

        public async Task<ESInventario> GetInventarioAsync(short pCodEstab, decimal pCodInv, decimal pContagem) =>
            await _IESInventarioService.GetInventarioAsync(pCodEstab, pCodInv, pContagem);

        public async Task<ESCreateItemInventarioVM> CreateItemInventarioAsync(ESCreateItemInventarioVM pCreateItemInventario)
        {
            var Result = await _IESInventarioService.CreateItemInventarioAsync(Mapper.Map<ESCreateItemInventario>(pCreateItemInventario));

            if (Result == null)
            {
                pCreateItemInventario.Result = string.Join(" ", _IESInventarioService.Errors.First().Messages);
                return null;
            }
            pCreateItemInventario.Result = "";
            pCreateItemInventario.codproduto = Result.codproduto;

            return Mapper.Map<ESCreateItemInventarioVM>(pCreateItemInventario);
        }
    }
}
