﻿using Hino.Estoque.Aplication.Interfaces.Engenharia;
using Hino.Estoque.Domain.Engenharia.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Engenharia;

namespace Hino.Estoque.Aplication.Services.Engenharia
{
    public class ENProcessosAS : BaseAppService<ENProcessos>, IENProcessosAS
    {
        private readonly IENProcessosService _IENProcessosService;

        public ENProcessosAS(IENProcessosService pIENProcessosService) :
             base(pIENProcessosService)
        {
            _IENProcessosService = pIENProcessosService;
        }
    }
}
