﻿using Hino.Estoque.Aplication.Interfaces.Engenharia;
using Hino.Estoque.Domain.Engenharia.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Engenharia;

namespace Hino.Estoque.Aplication.Services.Engenharia
{
    public class ENMaquinasAS : BaseAppService<ENMaquinas>, IENMaquinasAS
    {
        private readonly IENMaquinasService _IENMaquinasService;

        public ENMaquinasAS(IENMaquinasService pIENMaquinasService) :
             base(pIENMaquinasService)
        {
            _IENMaquinasService = pIENMaquinasService;
        }
    }
}
