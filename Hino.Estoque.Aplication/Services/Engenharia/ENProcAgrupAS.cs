﻿using Hino.Estoque.Aplication.Interfaces.Engenharia;
using Hino.Estoque.Domain.Engenharia.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Engenharia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.Services.Engenharia
{
    public class ENProcAgrupAS : BaseAppService<ENProcAgrup>, IENProcAgrupAS
    {
        private readonly IENProcAgrupService _IENProcAgrupService;

        public ENProcAgrupAS(IENProcAgrupService pIENProcAgrupService) :
             base(pIENProcAgrupService)
        {
            _IENProcAgrupService = pIENProcAgrupService;
        }
    }
}
