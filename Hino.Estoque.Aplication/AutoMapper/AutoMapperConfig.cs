﻿using AutoMapper;
using Hino.Estoque.Aplication.ViewModels.Engenharia;
using Hino.Estoque.Aplication.ViewModels.Estoque;
using Hino.Estoque.Aplication.ViewModels.Fiscal;
using Hino.Estoque.Aplication.ViewModels.Gerais;
using Hino.Estoque.Aplication.ViewModels.Manutencao;
using Hino.Estoque.Aplication.ViewModels.Producao;
using Hino.Estoque.Aplication.ViewModels.Producao.Apontamento;
using Hino.Estoque.Aplication.ViewModels.Producao.OEE;
using Hino.Estoque.Aplication.ViewModels.Vendas;
using Hino.Estoque.Infra.Cross.Entities.Engenharia;
using Hino.Estoque.Infra.Cross.Entities.Estoque;
using Hino.Estoque.Infra.Cross.Entities.Fiscal;
using Hino.Estoque.Infra.Cross.Entities.Gerais;
using Hino.Estoque.Infra.Cross.Entities.Manutencao;
using Hino.Estoque.Infra.Cross.Entities.Producao;
using Hino.Estoque.Infra.Cross.Entities.Producao.Apontamento;
using Hino.Estoque.Infra.Cross.Entities.Producao.OEE;
using Hino.Estoque.Infra.Cross.Entities.Vendas;

namespace Hino.Estoque.Aplication.AutoMapper
{
    public class AutoMapperConfig
    {
        public static void Register()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<ESInventario, ESInventarioVM>();
                cfg.CreateMap<ESInventDet, ESInventDetVM>();
                cfg.CreateMap<ESInventLote, ESInventLoteVM>();
                cfg.CreateMap<ESInventTerc, ESInventTercVM>();
                cfg.CreateMap<ESKardex, ESKardexVM>();
                cfg.CreateMap<ESLote, ESLoteVM>();
                cfg.CreateMap<ESLoteSaldo, ESLoteSaldoVM>();
                cfg.CreateMap<ESTransfDet, ESTransfDetVM>();
                cfg.CreateMap<ESTransferencia, ESTransferenciaVM>();
                cfg.CreateMap<ESEnderecamento, ESEnderecamentoVM>();
                cfg.CreateMap<ESProdEnderec, ESProdEnderecVM>();
                cfg.CreateMap<ESTransfLote, ESTransfLoteVM>();
                cfg.CreateMap<FSLocalEstoque, FSLocalEstoqueVM>();
                cfg.CreateMap<FSProduto, FSProdutoVM>();
                cfg.CreateMap<FSProdutoparamEstab, FSProdutoparamEstabVM>();
                cfg.CreateMap<FSProdutoPcp, FSProdutoPcpVM>();
                cfg.CreateMap<FSSaldoEstoque, FSSaldoEstoqueVM>();
                cfg.CreateMap<GEEstab, GEEstabVM>();
                cfg.CreateMap<GEFuncionarios, GEFuncionariosVM>();
                cfg.CreateMap<GELog, GELogVM>();
                cfg.CreateMap<GELogDet, GELogDetVM>();
                cfg.CreateMap<GEUsuarios, GEUsuariosVM>();
                cfg.CreateMap<PDLancamentos, PDLancamentosVM>();
                cfg.CreateMap<PDOPTransf, PDOPTransfVM>();
                cfg.CreateMap<PDOPTransfLote, PDOPTransfLoteVM>();
                cfg.CreateMap<PDOrdemProd, PDOrdemProdVM>();
                cfg.CreateMap<PDOrdemProdComp, PDOrdemProdCompVM>();
                cfg.CreateMap<PDOrdemProdRotinas, PDOrdemProdRotinasVM>();
                cfg.CreateMap<PDParamEstab, PDParamEstabVM>();
                cfg.CreateMap<CZIntegraPedidos, CZIntegraPedidosVM>();
                cfg.CreateMap<CZIntegPedItens, CZIntegPedItensVM>();
                cfg.CreateMap<CZIntegracaoRevisao, CZIntegracaoRevisaoVM>();
                cfg.CreateMap<ESCreateProdTransferencia, ESCreateProdTransferenciaVM>();
                cfg.CreateMap<PDAptInicioProd, PDAptInicioProdVM>();
                cfg.CreateMap<PDLancMotivoRefugo, PDLancMotivoRefugoVM>();
                cfg.CreateMap<PDMotivos, PDMotivosVM>();
                cfg.CreateMap<PDAptLancamento, PDAptLancamentoVM>();
                cfg.CreateMap<PDAptLancList, PDAptLancListVM>();
                cfg.CreateMap<PDParadas, PDParadasVM>();
                cfg.CreateMap<PDOEE, PDOEEGerarVM>();
                cfg.CreateMap<PDAptParadas, PDAptParadasVM>();
                cfg.CreateMap<PDSucata, PDSucataVM>();
                cfg.CreateMap<ESRequisicao, ESRequisicaoVM>();
                cfg.CreateMap<ESCreateItemInventario, ESCreateItemInventarioVM>();
                cfg.CreateMap<ENMaquinas, ENMaquinasVM>();
                cfg.CreateMap<ENProcessos, ENProcessosVM>();
                cfg.CreateMap<MNEquipamento, MNEquipamentoVM>();
                cfg.CreateMap<MNMotivo, MNMotivoVM>();
                cfg.CreateMap<MNSolicManut, MNSolicManutVM>();
                cfg.CreateMap<MNTipoManut, MNTipoManutVM>();
                cfg.CreateMap<ENProcAgrup, ENProcAgrupVM>();

                cfg.CreateMap<ESInventarioVM, ESInventario>();
                cfg.CreateMap<ESInventDetVM, ESInventDet>();
                cfg.CreateMap<ESInventLoteVM, ESInventLote>();
                cfg.CreateMap<ESInventTercVM, ESInventTerc>();
                cfg.CreateMap<ESKardexVM, ESKardex>();
                cfg.CreateMap<ESLoteVM, ESLote>();
                cfg.CreateMap<ESLoteSaldoVM, ESLoteSaldo>();
                cfg.CreateMap<ESTransfDetVM, ESTransfDet>();
                cfg.CreateMap<ESTransferenciaVM, ESTransferencia>();
                cfg.CreateMap<ESEnderecamentoVM, ESEnderecamento>();
                cfg.CreateMap<ESProdEnderecVM, ESProdEnderec>();
                cfg.CreateMap<ESTransfLoteVM, ESTransfLote>();
                cfg.CreateMap<FSLocalEstoqueVM, FSLocalEstoque>();
                cfg.CreateMap<FSProdutoVM, FSProduto>();
                cfg.CreateMap<FSProdutoparamEstabVM, FSProdutoparamEstab>();
                cfg.CreateMap<FSProdutoPcpVM, FSProdutoPcp>();
                cfg.CreateMap<FSSaldoEstoqueVM, FSSaldoEstoque>();
                cfg.CreateMap<GEEstabVM, GEEstab>();
                cfg.CreateMap<GEFuncionariosVM, GEFuncionarios>();
                cfg.CreateMap<GELogVM, GELog>();
                cfg.CreateMap<GELogDetVM, GELogDet>();
                cfg.CreateMap<GEUsuariosVM, GEUsuarios>();
                cfg.CreateMap<PDLancamentosVM, PDLancamentos>();
                cfg.CreateMap<PDOPTransfVM, PDOPTransf>();
                cfg.CreateMap<PDOPTransfLoteVM, PDOPTransfLote>();
                cfg.CreateMap<PDOrdemProdVM, PDOrdemProd>();
                cfg.CreateMap<PDOrdemProdCompVM, PDOrdemProdComp>();
                cfg.CreateMap<PDOrdemProdRotinasVM, PDOrdemProdRotinas>();
                cfg.CreateMap<PDParamEstabVM, PDParamEstab>();
                cfg.CreateMap<CZIntegraPedidosVM, CZIntegraPedidos>();
                cfg.CreateMap<CZIntegPedItensVM, CZIntegPedItens>();
                cfg.CreateMap<CZIntegracaoRevisaoVM, CZIntegracaoRevisao>();
                cfg.CreateMap<ESCreateProdTransferenciaVM, ESCreateProdTransferencia>();
                cfg.CreateMap<PDAptInicioProdVM, PDAptInicioProd>();
                cfg.CreateMap<PDLancMotivoRefugoVM, PDLancMotivoRefugo>();
                cfg.CreateMap<PDMotivosVM, PDMotivos>();
                cfg.CreateMap<PDAptLancamentoVM, PDAptLancamento>();
                cfg.CreateMap<PDAptLancListVM, PDAptLancList>();
                cfg.CreateMap<PDParadasVM, PDParadas>();
                cfg.CreateMap<PDOEEGerarVM, PDOEE>();
                cfg.CreateMap<PDAptParadasVM, PDAptParadas>(); 
                cfg.CreateMap<PDSucataVM, PDSucata>();
                cfg.CreateMap<ESRequisicaoVM, ESRequisicao>();
                cfg.CreateMap<ESCreateItemInventarioVM, ESCreateItemInventario>();
                cfg.CreateMap<ENMaquinasVM, ENMaquinas>();
                cfg.CreateMap<ENProcessosVM, ENProcessos>();
                cfg.CreateMap<ENProcAgrupVM, ENProcAgrup>();

                cfg.CreateMap<MNEquipamentoVM, MNEquipamento>();
                cfg.CreateMap<MNMotivoVM, MNMotivo>();
                cfg.CreateMap<MNSolicManutVM, MNSolicManut>();
                cfg.CreateMap<MNTipoManutVM, MNTipoManut>();
            });
        }
    }
}
