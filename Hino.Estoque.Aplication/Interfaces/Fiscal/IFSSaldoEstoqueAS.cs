using Hino.Estoque.Infra.Cross.Entities.Fiscal;

namespace Hino.Estoque.Aplication.Interfaces.Fiscal
{
    public interface IFSSaldoEstoqueAS : IBaseAppService<FSSaldoEstoque>
    {
    }
}
