﻿using Hino.Estoque.Infra.Cross.Entities.Fiscal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.Interfaces.Fiscal
{
    public interface ICZIntegProdutosAS : IBaseAppService<CZIntegProdutos>
    {
        Task UpdateIntegratedAsync(CZIntegProdutos pProduct);
        Task<CZIntegProdutos> CreateAsync(CZIntegProdutos pProduct);
        Task<IEnumerable<CZIntegProdutos>> GetDispIntegAsync();
    }
}
