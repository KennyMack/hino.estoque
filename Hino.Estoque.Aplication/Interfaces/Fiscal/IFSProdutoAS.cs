using Hino.Estoque.Aplication.ViewModels.Fiscal;
using Hino.Estoque.Infra.Cross.Entities.Fiscal;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.Interfaces.Fiscal
{
    public interface IFSProdutoAS : IBaseAppService<FSProduto>
    {
        Task<FSProdutoVM> GetProductByBarCodeAsync(short pCodEstab, string pBarCode);
    }
}
