﻿using Hino.Estoque.Infra.Cross.Entities.Fiscal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.Interfaces.Fiscal
{
    public interface ICZIntegrProdDetAS : IBaseAppService<CZIntegrProdDet>
    {
        Task<CZIntegrProdDet> CreateAsync(CZIntegrProdDet pProduct);
        Task<IEnumerable<CZIntegrProdDet>> GetDispIntegAsync(string pPartNumber);
    }
}
