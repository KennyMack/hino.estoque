﻿using Hino.Estoque.Infra.Cross.Utils.Exceptions;
using Hino.Estoque.Infra.Cross.Utils.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.Interfaces
{
    public interface IBaseAppService<T> where T : class
    {
        List<ModelException> Errors { get; set; }
        T Add(T model);
        Task<PagedResult<T>> GetAllPagedAsync(int page, int pageSize, params Expression<Func<T, object>>[] includeProperties);
        Task<IEnumerable<T>> GetAllAsync(params Expression<Func<T, object>>[] includeProperties);
        Task<IEnumerable<T>> QueryAsync(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties);
        Task<PagedResult<T>> QueryPagedAsync(int page, int pageSize, Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties);
        T Update(T model);
        void Remove(T model);
        long NextSequence();
        Task<long> NextSequenceAsync();
        Task<int> SaveChanges();
        void RollBackChanges();
        void Dispose();

    }
}
