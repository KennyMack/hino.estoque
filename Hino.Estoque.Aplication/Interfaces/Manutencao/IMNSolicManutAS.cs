﻿using Hino.Estoque.Aplication.ViewModels.Manutencao;
using Hino.Estoque.Infra.Cross.Entities.Manutencao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.Interfaces.Manutencao
{
    public interface IMNSolicManutAS : IBaseAppService<MNSolicManut>
    {
        Task<MNSolicManutVM> CreateAsync(MNCreateSolicManutVM pSolicManut);
        Task<MNSolicManutVM> AlterarStatusSolicManutAsync(MNStatusSolicManutVM pStatus);
        Task<MNSearchManutVM> SearchManutAsync(short pCodEstab);
    }
}
