﻿using Hino.Estoque.Infra.Cross.Entities.Manutencao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.Interfaces.Manutencao
{
    public interface IMNEquipamentoAS : IBaseAppService<MNEquipamento>
    {
    }
}
