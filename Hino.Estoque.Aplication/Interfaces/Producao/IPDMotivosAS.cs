﻿using Hino.Estoque.Infra.Cross.Entities.Producao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.Interfaces.Producao
{
    public interface IPDMotivosAS : IBaseAppService<PDMotivos>
    {
    }
}
