using Hino.Estoque.Aplication.ViewModels.Engenharia;
using Hino.Estoque.Aplication.ViewModels.Producao;
using Hino.Estoque.Infra.Cross.Entities.Producao;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.Interfaces.Producao
{
    public interface IPDOrdemProdAS : IBaseAppService<PDOrdemProd>
    {
        Task<IEnumerable<ENMaquinasVM>> GetMachinesAsync(short pCodEstab);
        Task<IEnumerable<PDOrdemProdComp>> GetCompToSucAsync(short pCodEstab, long pCodOrdProd, string pNivelOrdProd);
        Task<PDOrdemProd> GetOPByBarCode(short pCodEstab, string pBarCode);
        Task<PDOrdemProd> GetOPById(short pCodEstab, decimal pCodOrdProd, string pNivelOrdProd);
        Task<PDOrdemProd> AddOPSaldo(PDOrdemProd pOrdemProd);
        Task<IEnumerable<PDEficienciaProducao>> BuscaEficienciaProducaoAsync(short pCodEstab, DateTime pDtInicio, DateTime pDtTermino);
        Task<long?> GetCodEstruturaAsync(string pCodProduto, short pCodEstab);
    }
}
