using Hino.Estoque.Infra.Cross.Entities.Producao;

namespace Hino.Estoque.Aplication.Interfaces.Producao
{
    public interface IPDOPTransfLoteAS : IBaseAppService<PDOPTransfLote>
    {
    }
}
