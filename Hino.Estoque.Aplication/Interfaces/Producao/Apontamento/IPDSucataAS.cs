﻿using Hino.Estoque.Aplication.ViewModels.Producao.Apontamento;
using Hino.Estoque.Infra.Cross.Entities.Producao.Apontamento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.Interfaces.Producao.Apontamento
{
    public interface IPDSucataAS : IBaseAppService<PDSucata>
    {
        Task<PDSucata> PostApontarSucata(short pCodEstab, PDSucataCreateVM pPDSucataCreate);
    }
}
