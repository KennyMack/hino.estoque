﻿using Hino.Estoque.Aplication.ViewModels.Producao.Apontamento;
using Hino.Estoque.Infra.Cross.Entities.Producao.Apontamento;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.Interfaces.Producao.Apontamento
{
    public interface IPDAptTerminoProdAS : IBaseAppService<PDAptTerminoProd>
    {
        Task<PDAptTerminoProdVM> TerminarProducaoAsync(short pCodEstab, PDAptTerminoProdCreateVM pAptTermCreate);
        Task<PDAptTerminoProdVM> TerminarParadaAsync(short pCodEstab, PDAptTerminoProdCreateVM pAptTermCreate);
        Task<PDAptTerminoProdVM> TerminarTurnoProducaoAsync(short pCodEstab, PDAptTerminoProdCreateVM pAptTermCreate);
        Task<IEnumerable<PDAptLancListVM>> GetListApontamentoAsync(short pCodEstab, int pCodFuncionario);
    }
}
