﻿using Hino.Estoque.Aplication.ViewModels.Producao.Apontamento;
using Hino.Estoque.Infra.Cross.Entities.Producao.Apontamento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.Interfaces.Producao.Apontamento
{
    public interface IPDAptInicioProdAS : IBaseAppService<PDAptInicioProd>
    {
        Task<PDAptInicioProdVM> IniciarProducaoAsync(short pCodEstab, PDAptInicioProdCreateVM pAptIniCreate);
        Task<bool> OperacaoJaIniciadaAsync(short pCodEstab, PDAptInicioProdCreateVM pAptIniCreate);
        Task<bool> ExisteInspecaoRotinaAnteriorAsync(short pCodEstab, PDAptInicioProdCreateVM pAptIniCreate);
        Task<PDAptInicioProd> GetLastAptStartAsync(
            short pCodEstab, int pCodFuncionario,
            long pCodOrdProd, string pNivelOrdProd,
            long pCodEstrutura, int pCodRoteiro,
            int pOperacao, short pTipo);
        Task<PDAptInicioProdVM> IniciarParadaAsync(short pCodEstab, PDAptInicioProdCreateVM pAptIniCreate);
    }
}
