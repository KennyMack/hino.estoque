﻿using Hino.Estoque.Infra.Cross.Entities.Producao.OEE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.Interfaces.Producao.OEE
{
    public interface IPDOEEParamAS : IBaseAppService<PDOEEParam>
    {
        IEnumerable<PDOEEParam> GetOEEColorsByEstabAsync(short pCodEstab);
    }
}
