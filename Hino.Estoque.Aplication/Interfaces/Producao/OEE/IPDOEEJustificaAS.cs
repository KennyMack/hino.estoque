﻿using Hino.Estoque.Infra.Cross.Entities.Producao.OEE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.Interfaces.Producao.OEE
{
    public interface IPDOEEJustificaAS : IBaseAppService<PDOEEJustifica>
    {
        IEnumerable<PDOEEJustifica> GetOEEJustificativas(short pCodEstab, string pCodMaquina, DateTime pDtPeriodo);
    }
}
