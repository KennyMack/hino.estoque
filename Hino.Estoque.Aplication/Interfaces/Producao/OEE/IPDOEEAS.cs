﻿using Hino.Estoque.Aplication.ViewModels.Engenharia;
using Hino.Estoque.Aplication.ViewModels.Producao.OEE;
using Hino.Estoque.Infra.Cross.Entities.Producao.OEE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.Interfaces.Producao.OEE
{
    public interface IPDOEEAS : IBaseAppService<PDOEE>
    {
        Task<bool> GerarOEEAsync(PDOEEGerarVM pOee);
        Task<bool> GerarOEEMensalAsync(PDOEEGerarVM pOee);
        Task<IEnumerable<PDOEEResultado>> ListagemAsync(short pCodEstab, string pCodUsuario);
        Task<IEnumerable<PDOEEMensalResultado>> ListagemMensalAsync(short pCodEstab, string pCodUsuario);
        Task<ENSearchOEEVM> SearchOEEAsync(short pCodEstab);
    }
}
