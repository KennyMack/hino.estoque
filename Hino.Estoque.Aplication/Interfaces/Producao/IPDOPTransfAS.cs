using Hino.Estoque.Aplication.ViewModels.Producao;
using Hino.Estoque.Infra.Cross.Entities.Producao;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.Interfaces.Producao
{
    public interface IPDOPTransfAS : IBaseAppService<PDOPTransf>
    {
        Task<PDResultTransfOPVM> SeparateOPComponents(short pCodEstab, PDOPCreateTransfVM[] pLstTransf);
    }
}
