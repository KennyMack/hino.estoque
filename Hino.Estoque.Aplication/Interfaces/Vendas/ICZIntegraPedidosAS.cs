using Hino.Estoque.Infra.Cross.Entities.Vendas;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.Interfaces.Vendas
{
    public interface ICZIntegraPedidosAS : IBaseAppService<CZIntegraPedidos>
    {
        Task UpdateIntegratedAsync(CZIntegraPedidos pOrder);
        Task<CZIntegraPedidos> CreateAsync(CZIntegraPedidos pOrder);
        Task<IEnumerable<CZIntegraPedidos>> GetDispIntegAsync();
    }
}
