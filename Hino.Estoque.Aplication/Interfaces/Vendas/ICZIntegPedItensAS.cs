using Hino.Estoque.Infra.Cross.Entities.Vendas;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.Interfaces.Vendas
{
    public interface ICZIntegPedItensAS : IBaseAppService<CZIntegPedItens>
    {
        Task<CZIntegPedItens> CreateAsync(long pIdIntegracao, CZIntegPedItens pOrder);
    }
}
