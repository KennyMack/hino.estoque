using Hino.Estoque.Infra.Cross.Entities.Estoque;
using Hino.Estoque.Domain.Base.Interfaces;

namespace Hino.Estoque.Aplication.Interfaces.Estoque
{
    public interface IESTransfDetAS : IBaseAppService<ESTransfDet>
    {
    }
}
