﻿using Hino.Estoque.Aplication.ViewModels.Estoque;
using Hino.Estoque.Infra.Cross.Entities.Estoque;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.Interfaces.Estoque
{
    public interface IESRequisicaoAS : IBaseAppService<ESRequisicao>
    {
        Task<ESRequisicao> Create(ESCreateRequisicaoVM pRequisicao);
    }
}
