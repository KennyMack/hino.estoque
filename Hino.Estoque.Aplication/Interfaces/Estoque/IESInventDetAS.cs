using Hino.Estoque.Aplication.ViewModels.Estoque;
using Hino.Estoque.Infra.Cross.Entities.Estoque;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.Interfaces.Estoque
{
    public interface IESInventDetAS : IBaseAppService<ESInventDet>
    {
        Task<ESInventDet> GetInventarioItemAsync(short pCodEstab, ESInventDetItemVM pDados);
        Task<IEnumerable<ESInventDet>> GetItensInventario(short pCodEstab, decimal pCodInv, decimal pContagem);
        Task<bool> ConfirmaContagemItens(short pCodEstab, decimal pCodInv, decimal pContagem, ESUpdateInventDetVM[] lstItensDet);
    }
}
