using Hino.Estoque.Aplication.ViewModels.Estoque;
using Hino.Estoque.Infra.Cross.Entities.Estoque;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.Interfaces.Estoque
{
    public interface IESInventarioAS : IBaseAppService<ESInventario>
    {
        Task<ESInventario> GetInventarioAsync(short pCodEstab, decimal pCodInv, decimal pContagem);
        Task<IEnumerable<ESInventario>> GetInventariosPendentes(short pCodEstab);
        Task<ESCreateItemInventarioVM> CreateItemInventarioAsync(ESCreateItemInventarioVM pCreateItemInventario);
    }
}
