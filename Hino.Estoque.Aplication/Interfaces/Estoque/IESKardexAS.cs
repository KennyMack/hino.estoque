using Hino.Estoque.Aplication.ViewModels.Estoque;
using Hino.Estoque.Infra.Cross.Entities.Estoque;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.Interfaces.Estoque
{
    public interface IESKardexAS : IBaseAppService<ESKardex>
    {
        Task<bool> CreateProdTransferenciaAsync(ESCreateProdTransferenciaVM pCreateTransf);
        Task<bool> CreateProdEndTransferenciaAsync(ESCreateProdEndTransfVM pCreateTransf);
        Task<ESResultRequisicaoVM> AprovarRequisicaoEstoqueAsync(ESUpdateRequisicaoVM pRequisicao);
        Task<ESResultRequisicaoVM> ReprovarRequisicaoEstoqueAsync(ESUpdateRequisicaoVM pRequisicao);
    }
}
