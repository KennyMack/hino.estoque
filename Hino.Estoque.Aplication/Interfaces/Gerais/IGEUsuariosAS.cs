using Hino.Estoque.Aplication.ViewModels.Gerais;
using Hino.Estoque.Infra.Cross.Entities.Gerais;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.Interfaces.Gerais
{
    public interface IGEUsuariosAS : IBaseAppService<GEUsuarios>
    {
        Task<GEFuncUsuariosVM> GetByIdentifier(short pCodEstab, string pSenha, string pIdentifier);
    }
}
