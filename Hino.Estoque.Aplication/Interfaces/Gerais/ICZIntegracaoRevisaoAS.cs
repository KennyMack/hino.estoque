﻿using Hino.Estoque.Infra.Cross.Entities.Gerais;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.Interfaces.Gerais
{
    public interface ICZIntegracaoRevisaoAS : IBaseAppService<CZIntegracaoRevisao>
    {
        Task UpdateIntegratedAsync(CZIntegracaoRevisao pDraw);
        Task<CZIntegracaoRevisao> CreateAsync(CZIntegracaoRevisao pDraw);
        Task<IEnumerable<CZIntegracaoRevisao>> GetDispIntegAsync();
    }
}
