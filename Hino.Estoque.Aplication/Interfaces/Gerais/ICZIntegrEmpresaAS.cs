﻿using Hino.Estoque.Infra.Cross.Entities.Gerais;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.Interfaces.Gerais
{
    public interface ICZIntegrEmpresaAS : IBaseAppService<CZIntegrEmpresa>
    {
        Task UpdateIntegratedAsync(CZIntegrEmpresa pEnterprise);
        Task<CZIntegrEmpresa> CreateAsync(CZIntegrEmpresa pEnterprise);
        Task<IEnumerable<CZIntegrEmpresa>> GetDispIntegAsync();
    }
}
