﻿using Hino.Estoque.Aplication.ViewModels.Gerais;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.Interfaces.Gerais
{
    public interface ICurrentSettingsAS
    {
        GEEstabSettingsVM GetSettings();
    }
}
