using Hino.Estoque.Aplication.ViewModels.Gerais;
using Hino.Estoque.Infra.Cross.Entities.Gerais;
using System.Threading.Tasks;

namespace Hino.Estoque.Aplication.Interfaces.Gerais
{
    public interface IGEEstabAS : IBaseAppService<GEEstab>
    {
        Task<GEEstabVM> GetEstabByIdAsync(short pCodEstab);
        Task<GEEstabSettingsVM> GetEstabAndSettingsAsync(short pCodEstab);
        GEEstabSettingsVM GetCurrentSettings();
    }
}
