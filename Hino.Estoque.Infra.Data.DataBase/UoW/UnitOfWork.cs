﻿using Hino.Estoque.Domain.Base.Interfaces.UoW;
using Hino.Estoque.Infra.Data.DataBase.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Infra.Data.DataBase.UoW
{
    public class UnitOfWork : IUnitOfWork
    {
        public long RowsAffected { get; private set; }

        private readonly AppDbContext _AppDbContext;

        public UnitOfWork(AppDbContext appDbContext)
        {
            _AppDbContext = appDbContext;
        }

        public void Commit()
        {
            RowsAffected = _AppDbContext.SaveChanges();
        }

        public void Rollback()
        {
            _AppDbContext
                .ChangeTracker
                .Entries()
                .ToList()
                .ForEach(x =>
                {
                    try
                    {
                        x.Reload();
                    }
                    catch
                    {
                    }
                });

        }

        public void Dispose()
        {
        }

    }
}
