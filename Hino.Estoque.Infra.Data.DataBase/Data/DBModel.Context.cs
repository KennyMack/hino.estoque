﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Hino.Estoque.Infra.Data.DataBase.Data
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class Entities : DbContext
    {
        public Entities()
            : base("name=Entities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<ESINVENTARIO> ESINVENTARIO { get; set; }
        public virtual DbSet<ESINVENTDET> ESINVENTDET { get; set; }
        public virtual DbSet<ESINVENTLOTE> ESINVENTLOTE { get; set; }
        public virtual DbSet<ESINVENTTERC> ESINVENTTERC { get; set; }
        public virtual DbSet<ESKARDEX> ESKARDEX { get; set; }
        public virtual DbSet<ESLOTE> ESLOTE { get; set; }
        public virtual DbSet<ESLOTESALDO> ESLOTESALDO { get; set; }
        public virtual DbSet<ESTRANSFDET> ESTRANSFDET { get; set; }
        public virtual DbSet<ESTRANSFERENCIA> ESTRANSFERENCIA { get; set; }
        public virtual DbSet<ESTRANSFLOTE> ESTRANSFLOTE { get; set; }
        public virtual DbSet<FSLOCALESTOQUE> FSLOCALESTOQUE { get; set; }
        public virtual DbSet<FSPRODUTO> FSPRODUTO { get; set; }
        public virtual DbSet<FSPRODUTOPARAMESTAB> FSPRODUTOPARAMESTAB { get; set; }
        public virtual DbSet<FSPRODUTOPCP> FSPRODUTOPCP { get; set; }
        public virtual DbSet<FSSALDOESTOQUE> FSSALDOESTOQUE { get; set; }
        public virtual DbSet<GEESTAB> GEESTAB { get; set; }
        public virtual DbSet<GEFUNCIONARIOS> GEFUNCIONARIOS { get; set; }
        public virtual DbSet<GELOG> GELOG { get; set; }
        public virtual DbSet<GELOGDET> GELOGDET { get; set; }
        public virtual DbSet<GEUSUARIOS> GEUSUARIOS { get; set; }
        public virtual DbSet<PDLANCAMENTOS> PDLANCAMENTOS { get; set; }
        public virtual DbSet<PDOPTRANSF> PDOPTRANSF { get; set; }
        public virtual DbSet<PDOPTRANSFLOTE> PDOPTRANSFLOTE { get; set; }
        public virtual DbSet<PDORDEMPROD> PDORDEMPROD { get; set; }
        public virtual DbSet<PDORDEMPRODCOMP> PDORDEMPRODCOMP { get; set; }
        public virtual DbSet<PDORDEMPRODROTINAS> PDORDEMPRODROTINAS { get; set; }
        public virtual DbSet<PDPARAMESTAB> PDPARAMESTAB { get; set; }
        public virtual DbSet<ESENDERECAMENTO> ESENDERECAMENTO { get; set; }
        public virtual DbSet<ESPRODENDEREC> ESPRODENDEREC { get; set; }
    }
}
