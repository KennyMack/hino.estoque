//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Hino.Estoque.Infra.Data.DataBase.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class ESINVENTLOTE
    {
        public short CODESTAB { get; set; }
        public decimal CODINV { get; set; }
        public decimal CONTAGEM { get; set; }
        public string CODPRODUTO { get; set; }
        public string CODESTOQUE { get; set; }
        public decimal LOTE { get; set; }
        public decimal SALDOLOTE { get; set; }
        public Nullable<decimal> SALDOINVENT { get; set; }
    
        public virtual ESINVENTDET ESINVENTDET { get; set; }
        public virtual ESLOTE ESLOTE { get; set; }
        public virtual FSLOCALESTOQUE FSLOCALESTOQUE { get; set; }
    }
}
