﻿using Hino.Estoque.Infra.Cross.Entities.Engenharia;
using Hino.Estoque.Infra.Cross.Entities.Estoque;
using Hino.Estoque.Infra.Cross.Entities.Fiscal;
using Hino.Estoque.Infra.Cross.Entities.Gerais;
using Hino.Estoque.Infra.Cross.Entities.Manutencao;
using Hino.Estoque.Infra.Cross.Entities.Producao;
using Hino.Estoque.Infra.Cross.Entities.Producao.Apontamento;
using Hino.Estoque.Infra.Cross.Entities.Producao.OEE;
using Hino.Estoque.Infra.Cross.Entities.Vendas;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Infra.Data.DataBase.Context
{
    public class AppDbContext : DbContext
    {
        public virtual DbSet<ENMaquinas> ENMaquinas { get; set; }
        public virtual DbSet<ENProcessos> ENProcessos { get; set; }

        public virtual DbSet<ESInventario> ESInventario { get; set; }
        public virtual DbSet<ESInventDet> ESInventDet { get; set; }
        public virtual DbSet<ESInventLote> ESInventLote { get; set; }
        public virtual DbSet<ESInventTerc> ESInventTerc { get; set; }
        public virtual DbSet<ESKardex> ESKardex { get; set; }
        public virtual DbSet<ESLote> ESLote { get; set; }
        public virtual DbSet<ESLoteSaldo> ESLoteSaldo { get; set; }
        public virtual DbSet<ENProcAgrup> ENProcAgrup { get; set; }
        public virtual DbSet<ESRequisicao> ESRequisicao { get; set; }
        public virtual DbSet<ESTransfDet> ESTransfDet { get; set; }
        public virtual DbSet<ESTransferencia> ESTransferencia { get; set; }
        public virtual DbSet<ESTransfLote> ESTransfLote { get; set; }
        public virtual DbSet<FSLocalEstoque> FSLocalEstoque { get; set; }
        public virtual DbSet<FSProduto> FSProduto { get; set; }
        public virtual DbSet<FSProdutoparamEstab> FSProdutoparamEstab { get; set; }
        public virtual DbSet<FSProdutoPcp> FSProdutoPcp { get; set; }
        public virtual DbSet<FSSaldoEstoque> FSSaldoEstoque { get; set; }
        public virtual DbSet<FSParamLocEstoqEstab> FSParamLocEstoqEstab { get; set; }
        public virtual DbSet<GEEstab> GEEstab { get; set; }
        public virtual DbSet<GEFuncionarios> GEFuncionarios { get; set; }
        public virtual DbSet<GELog> GELog { get; set; }
        public virtual DbSet<GELogDet> GELogDet { get; set; }
        public virtual DbSet<GEUsuarios> GEUsuarios { get; set; }
        public virtual DbSet<PDLancamentos> PDLancamentos { get; set; }
        public virtual DbSet<PDOPTransf> PDOPTransf { get; set; }
        public virtual DbSet<PDAptInicioProd> PDAptInicioProd { get; set; }
        public virtual DbSet<PDAptTerminoProd> PDAptTerminoProd { get; set; }
        public virtual DbSet<PDAptLancamento> PDAptLancamento { get; set; }
        public virtual DbSet<PDMotivos> PDMotivos { get; set; }
        public virtual DbSet<PDOPTransfLote> PDOPTransfLote { get; set; }
        public virtual DbSet<PDOrdemProd> PDOrdemProd { get; set; }
        public virtual DbSet<PDOrdemProdComp> PDOrdemProdComp { get; set; }
        public virtual DbSet<PDOrdemProdRotinas> PDOrdemProdRotinas { get; set; }
        public virtual DbSet<PDParamEstab> PDParamEstab { get; set; }
        public virtual DbSet<PDOEEParam> PDOEEParam { get; set; }
        public virtual DbSet<PDOEEJustifica> PDOEEJustifica { get; set; }
        public virtual DbSet<PDParadas> PDParadas { get; set; }
        public virtual DbSet<PDAptParadas> PDAptParadas { get; set; }
        public virtual DbSet<PDSucata> PDSucata { get; set; }
        public virtual DbSet<CZIntegraPedidos> CZIntegraPedidos { get; set; }
        public virtual DbSet<CZIntegPedItens> CZIntegPedItens { get; set; }
        public virtual DbSet<CZIntegrEmpresa> CZIntegrEmpresa { get; set; }
        public virtual DbSet<CZIntegProdutos> CZIntegProdutos { get; set; }
        public virtual DbSet<CZIntegrProdDet> CZIntegrProdDet { get; set; }
        public virtual DbSet<CZIntegracaoRevisao> CZIntegracaoRevisao { get; set; }

        public virtual DbSet<MNEquipamento> MNEquipamento { get; set; }
        public virtual DbSet<MNMotivo> MNMotivo { get; set; }
        public virtual DbSet<MNSolicManut> MNSolicManut { get; set; }
        public virtual DbSet<MNTipoManut> MNTipoManut { get; set; }

        public AppDbContext() : base("name=AppDbContext")
        {
            Database.SetInitializer<AppDbContext>(null);
            Database.Log = (S) =>
            {
                Debug.WriteLine(S);
            };
            Configuration.LazyLoadingEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            var config = ConfigurationManager.AppSettings;
            modelBuilder.HasDefaultSchema(config["dbUser"].ToString());


            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            modelBuilder.Properties().Configure(c =>
            {
                c.HasColumnName(c.ClrPropertyInfo.Name.ToUpper());
            });

            base.OnModelCreating(modelBuilder);
        }
    }
}
