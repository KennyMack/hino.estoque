﻿using Hino.Estoque.Domain.Base.Interfaces.Repositories;
using Hino.Estoque.Infra.Data.DataBase.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity.Infrastructure;
using Hino.Estoque.Infra.Cross.Utils.Paging;
using Hino.Estoque.Infra.Cross.Entities;

namespace Hino.Estoque.Infra.Data.DataBase.Repositories
{
    public class BaseRepository<T> : IDisposable, IBaseRepository<T> where T : BaseEntity
    {
        protected AppDbContext DbConn;
        protected DbSet<T> DbEntity;

        public BaseRepository(AppDbContext appDbContext)
        {
            DbConn = appDbContext;
            DbEntity = DbConn.Set<T>();
        }

        protected IQueryable<T> AddQueryProperties(IQueryable<T> query, params System.Linq.Expressions.Expression<Func<T, object>>[] includeProperties)
        {
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query.AsNoTracking();
        }

        protected async Task<PagedResult<T>> PaginateQueryAsync(IQueryable<T> query, int page, int pageSize)
        {
            var result = new PagedResult<T>
            {
                CurrentPage = page,
                PageSize = pageSize,
                RowCount = 10
            };

            var pageCount = (double)result.RowCount / pageSize;
            result.PageCount = (int)Math.Ceiling(pageCount);

            result.Results = await query.ToListAsync();
            return result;
        }

        public virtual async Task<PagedResult<T>> GetAllPagedAsync(int page, int pageSize,
                                       params System.Linq.Expressions.Expression<Func<T, object>>[] includeProperties) =>
            await PaginateQueryAsync(AddQueryProperties(DbEntity, includeProperties).AsNoTracking(), page, pageSize);


        public virtual async Task<PagedResult<T>> QueryPagedAsync(int page, int pageSize,
            System.Linq.Expressions.Expression<Func<T, bool>> predicate,
            params System.Linq.Expressions.Expression<Func<T, object>>[] includeProperties) =>
            await PaginateQueryAsync(AddQueryProperties(DbEntity, includeProperties).AsNoTracking()
                            .Where(predicate), page, pageSize);

        public virtual async Task<IEnumerable<T>> QueryAsync(System.Linq.Expressions.Expression<Func<T, bool>> predicate,
            params System.Linq.Expressions.Expression<Func<T, object>>[] includeProperties) =>
            await AddQueryProperties(DbEntity, includeProperties).AsNoTracking()
                            .Where(predicate)
                            .ToListAsync();

        public virtual async Task<IEnumerable<T>> GetAllAsync(params System.Linq.Expressions.Expression<Func<T, object>>[] includeProperties) =>
            await AddQueryProperties(DbEntity, includeProperties).AsNoTracking().ToListAsync();

        public virtual void Add(T model)
        {
            /*model.Created = DateTime.Now;
            model.Modified = DateTime.Now;
            model.IsActive = true;
            model.UniqueKey = Guid.NewGuid().ToString();*/

            // if (model.Id == 0)
            //     model.Id = NextSequence();

            DbEntity.Add(model);
        }

        public virtual void Update(T model)
        {
            DbEntity.Attach(model);
            // DbConn.Entry(model).State = EntityState.Detached;
            DbConn.Entry(model).State = EntityState.Modified;
        }

        public virtual void Remove(T model)
        {
            //DbEntity.Attach(model);
            //DbConn.Entry(model).State = EntityState.Detached;
            //DbConn.Entry(model).State = EntityState.Deleted;
            DbEntity.Remove(model);
        }

        DbRawSqlQuery<decimal> GetNextSequence()
        {
            var sql = $"SELECT SEQ_{typeof(T).Name}.NEXTVAL FROM DUAL";

            return DbConn.Database.SqlQuery<decimal>(sql);
        }

        public virtual long NextSequence() =>
            Convert.ToInt64(GetNextSequence().First());

        public virtual async Task<long> NextSequenceAsync() =>
            Convert.ToInt64(await GetNextSequence().FirstAsync());

        public virtual async Task<int> SaveChanges() =>
            await DbConn.SaveChangesAsync();

        public virtual void RollBackChanges()
        {
            var changedEntries = DbConn.ChangeTracker.Entries()
                .Where(x => x.State != EntityState.Unchanged).ToList();

            foreach (var entry in changedEntries)
            {
                switch (entry.State)
                {
                    case EntityState.Modified:
                        entry.CurrentValues.SetValues(entry.OriginalValues);
                        entry.State = EntityState.Unchanged;
                        break;
                    case EntityState.Added:
                        entry.State = EntityState.Detached;
                        break;
                    case EntityState.Deleted:
                        entry.State = EntityState.Unchanged;
                        break;
                }
            }
        }

        public void Dispose()
        {
            DbConn.Dispose();
            // GC.SuppressFinalize(this);
        }
    }
}
