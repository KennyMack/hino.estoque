﻿using Hino.Estoque.Domain.Fiscal.Interfaces.Repositories;
using Hino.Estoque.Infra.Cross.Entities.Fiscal;
using Hino.Estoque.Infra.Data.DataBase.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Infra.Data.DataBase.Repositories.Fiscal
{
    public class FSParamLocEstoqEstabRepository : BaseRepository<FSParamLocEstoqEstab>, IFSParamLocEstoqEstabRepository
    {
        public FSParamLocEstoqEstabRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
