using Hino.Estoque.Domain.Fiscal.Interfaces.Repositories;
using Hino.Estoque.Infra.Cross.Entities.Fiscal;
using Hino.Estoque.Infra.Data.DataBase.Context;
using Hino.Estoque.Infra.Data.DataBase.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Data.Entity;

namespace Hino.Estoque.Infra.DataBase.Repositories.Fiscal
{
    public class FSProdutoRepository : BaseRepository<FSProduto>, IFSProdutoRepository
    {
        public FSProdutoRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }

        public async Task<FSProduto> GetProductByBarCodeAsync(short pCodEstab, string pBarCode) =>
            await AddQueryProperties(DbEntity,
                e => e.FSProdutoparamEstab,
                e => e.FSProdutoPcp)
            .Include("fsprodutoparamestab.esprodenderec")
            .Include("fsprodutoparamestab.esprodenderec.esenderecamento")
            .Where(r => (r.codproduto == pBarCode))
            .FirstOrDefaultAsync();
    }
}
