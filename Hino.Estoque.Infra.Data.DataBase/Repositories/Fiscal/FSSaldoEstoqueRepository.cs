using Hino.Estoque.Domain.Fiscal.Interfaces.Repositories;
using Hino.Estoque.Infra.Cross.Entities.Fiscal;
using Hino.Estoque.Infra.Data.DataBase.Context;
using Hino.Estoque.Infra.Data.DataBase.Repositories;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.Estoque.Infra.DataBase.Repositories.Fiscal
{
    public class FSSaldoEstoqueRepository : BaseRepository<FSSaldoEstoque>, IFSSaldoEstoqueRepository
    {
        public FSSaldoEstoqueRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }

        public async Task<double> SaldoEstoqueProduto(short pCodEstab, string pCodProduto, string pCodEstoque) =>
            Convert.ToDouble((await QueryAsync(r => r.codestab == pCodEstab && r.codestoque == pCodEstoque &&
                r.codproduto == pCodProduto)).Sum(r => r.saldoEstoque));
    }
}
