using Hino.Estoque.Domain.Fiscal.Interfaces.Repositories;
using Hino.Estoque.Infra.Cross.Entities.Fiscal;
using Hino.Estoque.Infra.Data.DataBase.Context;
using Hino.Estoque.Infra.Data.DataBase.Repositories;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.Estoque.Infra.DataBase.Repositories.Fiscal
{
    public class FSProdutoparamEstabRepository : BaseRepository<FSProdutoparamEstab>, IFSProdutoparamEstabRepository
    {
        public FSProdutoparamEstabRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }

    }
}
