﻿using Hino.Estoque.Domain.Fiscal.Interfaces.Repositories;
using Hino.Estoque.Infra.Cross.Entities.Fiscal;
using Hino.Estoque.Infra.Data.DataBase.Context;

namespace Hino.Estoque.Infra.Data.DataBase.Repositories.Fiscal
{
    public class CZIntegProdutosRepository : BaseRepository<CZIntegProdutos>, ICZIntegProdutosRepository
    {
        public CZIntegProdutosRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
