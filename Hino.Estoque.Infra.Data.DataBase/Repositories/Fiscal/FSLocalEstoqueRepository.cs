using Hino.Estoque.Domain.Fiscal.Interfaces.Repositories;
using Hino.Estoque.Infra.Cross.Entities.Fiscal;
using Hino.Estoque.Infra.Data.DataBase.Context;
using Hino.Estoque.Infra.Data.DataBase.Repositories;

namespace Hino.Estoque.Infra.DataBase.Repositories.Fiscal
{
    public class FSLocalEstoqueRepository : BaseRepository<FSLocalEstoque>, IFSLocalEstoqueRepository
    {
        public FSLocalEstoqueRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
