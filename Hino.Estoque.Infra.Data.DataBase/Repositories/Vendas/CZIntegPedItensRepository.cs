﻿using Hino.Estoque.Domain.Vendas.Interfaces.Repositories;
using Hino.Estoque.Infra.Cross.Entities.Vendas;
using Hino.Estoque.Infra.Data.DataBase.Context;

namespace Hino.Estoque.Infra.Data.DataBase.Repositories.Vendas
{
    public class CZIntegPedItensRepository : BaseRepository<CZIntegPedItens>, ICZIntegPedItensRepository
    {
        public CZIntegPedItensRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
