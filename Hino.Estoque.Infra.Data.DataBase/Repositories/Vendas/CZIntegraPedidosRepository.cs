﻿using Hino.Estoque.Domain.Vendas.Interfaces.Repositories;
using Hino.Estoque.Infra.Cross.Entities.Vendas;
using Hino.Estoque.Infra.Data.DataBase.Context;

namespace Hino.Estoque.Infra.Data.DataBase.Repositories.Vendas
{
    public class CZIntegraPedidosRepository : BaseRepository<CZIntegraPedidos>, ICZIntegraPedidosRepository
    {
        public CZIntegraPedidosRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
