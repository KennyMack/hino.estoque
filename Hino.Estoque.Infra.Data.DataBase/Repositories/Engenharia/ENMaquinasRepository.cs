﻿using Hino.Estoque.Domain.Engenharia.Interfaces.Repositories;
using Hino.Estoque.Infra.Cross.Entities.Engenharia;
using Hino.Estoque.Infra.Data.DataBase.Context;

namespace Hino.Estoque.Infra.Data.DataBase.Repositories.Engenharia
{
    public class ENMaquinasRepository : BaseRepository<ENMaquinas>, IENMaquinasRepository
    {
        public ENMaquinasRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
