using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Hino.Estoque.Domain.Producao.Interfaces.Repositories;
using Hino.Estoque.Infra.Cross.Entities.Producao;
using Hino.Estoque.Infra.Cross.Utils;
using Hino.Estoque.Infra.Cross.Utils.Reader;
using Hino.Estoque.Infra.Data.DataBase.Context;
using Hino.Estoque.Infra.Data.DataBase.Repositories;
using Oracle.ManagedDataAccess.Client;

namespace Hino.Estoque.Infra.DataBase.Repositories.Producao
{
    public class PDOrdemProdRepository : BaseRepository<PDOrdemProd>, IPDOrdemProdRepository
    {
        public PDOrdemProdRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }


        #region Select Busca Efic. Producao
        private string SelectBuscaEficProducao =>
            @"SELECT TBOP.CODPROCESSO || ' - ' || TBOP.DESCPROCESSO PROCESSO,
                     TBOP.NUMOP || '/' || TBOP.NVLOP OP,
                     TBOP.CODPRODUTO || ' - ' || TBOP.DESCRICAO PRODUTO,
                     TO_NUMBER(TBOP.TOTALPROGRAMADO) TOTALPROGRAMADO,
                     TBOP.TURNO, TO_NUMBER(TBOP.METADIA) METADIA, TO_NUMBER(TBOP.QTDEPROD) QTDEPROD,
                     TO_NUMBER(TBOP.QTDEREF) QTDEREF, TO_NUMBER(TBOP.EFICIENCIA) EFICIENCIA,
                     TO_CHAR(CRITICA) CRITICA, TBOP.CODMAQUINA || ' - ' || TBOP.ENMAQUINASDESCRICAO MAQUINA,
                     TBOP.CODFUNCIONARIO || ' - ' || TBOP.NOMEFUNCIONARIO FUNCIONARIO, 
                     SUM(TBOP.EFICIENCIA) OVER (PARTITION BY TBOP.CODMAQUINA ORDER BY TBOP.CODMAQUINA) EFICTOT,
                     SUM(1) OVER (PARTITION BY TBOP.CODMAQUINA ORDER BY TBOP.CODMAQUINA) QTDMAQ,
                     (CASE
                       WHEN TBOP.QTDEPROD > 0 AND TBOP.QTDEREF > 0 THEN
                         ROUND(TBOP.QTDEREF / TBOP.QTDEPROD, 4)
                       ELSE
                         0
                     END * 100) PERCREFUGO
                FROM (SELECT GEESTAB.RAZAOSOCIAL ESTABELECIMENTO,
                             CCTTEFICPROD.CODPROCESSO,
                             ENPROCESSOS.DESCRICAO DESCPROCESSO,
                             CCTTEFICPROD.CODORDPROD NUMOP, 
                             CCTTEFICPROD.NIVELORDPROD NVLOP, 
                             CCTTEFICPROD.CODPRODUTO, 
                             FSPRODUTO.DESCRICAO,
                             CCTTEFICPROD.INICIOPROD DTINICIO, 
                             CCTTEFICPROD.TERMINOPROD DTTERMINO, 
                             TO_NUMBER(TO_CHAR(CCTTEFICPROD.TERMINOPROD, 'YYYY')) - TO_NUMBER(TO_CHAR(CCTTEFICPROD.INICIOPROD, 'YYYY')) DIFANO,
                             CCTTEFICPROD.DIASPROD DIAS, 
                             CCTTEFICPROD.PROGTOTAL TOTALPROGRAMADO,
                             CCTTEFICPROD.TURNO, 
                             CCTTEFICPROD.METATURNO METADIA,
                             CCTTEFICPROD.QTDEPROD, 
                             CCTTEFICPROD.QTDEREF, 
                             CCTTEFICPROD.EFICIENCIA,
                             CCTTEFICPROD.CRITICA,
                             PDTURNOS.DESCRICAO PDTURNOSDESCRICAO,
                             ENMAQUINAS.CODMAQUINA,
                             ENMAQUINAS.DESCRICAO ENMAQUINASDESCRICAO,
                             CCTTEFICPROD.CODFUNCIONARIO,
                             CCTTEFICPROD.NOMEFUNCIONARIO
                        FROM CCTTEFICPROD,
                             GEESTAB,
                             ENPROCESSOS,
                             FSPRODUTO,
                             PDTURNOS,
                             (SELECT PDORDEMPRODROTINAS.CODMAQUINA, 
                                     PDORDEMPRODROTINAS.CODESTAB,
                                     PDORDEMPRODROTINAS.CODORDPROD,
                                     PDORDEMPRODROTINAS.CODESTRUTURA,
                                     PDORDEMPRODROTINAS.CODROTEIRO,
                                     PDORDEMPRODROTINAS.NIVELORDPROD
                                FROM PDORDEMPRODROTINAS
                               WHERE PDORDEMPRODROTINAS.OPERACAO = 10) TBMAQUINA,
                             ENMAQUINAS
                       WHERE GEESTAB.CODESTAB        = CCTTEFICPROD.CODESTAB
                         AND ENPROCESSOS.CODESTAB    = CCTTEFICPROD.CODESTAB
                         AND ENPROCESSOS.CODPROCESSO = CCTTEFICPROD.CODPROCESSO
                         AND FSPRODUTO.CODPRODUTO    = CCTTEFICPROD.CODPRODUTO
                         AND PDTURNOS.CODESTAB       = CCTTEFICPROD.CODESTAB
                         AND PDTURNOS.TURNO          = CCTTEFICPROD.TURNO
                         AND TBMAQUINA.CODESTAB      = CCTTEFICPROD.CODESTAB
                         AND TBMAQUINA.CODORDPROD    = CCTTEFICPROD.CODORDPROD
                         AND TBMAQUINA.NIVELORDPROD  = CCTTEFICPROD.NIVELORDPROD
                         AND TBMAQUINA.CODESTRUTURA  = CCTTEFICPROD.CODESTRUTURA
                         AND ENMAQUINAS.CODMAQUINA   = TBMAQUINA.CODMAQUINA
                         AND ENMAQUINAS.CODESTAB     = TBMAQUINA.CODESTAB
                         ) TBOP
               --#FILTROPROCESSO#
               --#FILTRO#";
        #endregion

        public async Task<PDOrdemProd> GetOPById(short pCodEstab, decimal pCodOrdProd, string pNivelOrdProd) =>
            await AddQueryProperties(DbEntity,
                             e => e.fsproduto,
                             e => e.pdordemprodcomp,
                             e => e.pdordemprodrotinas)
                            .Include("fsproduto.fsprodutoparamestab")
                            .Include("fsproduto.fsprodutopcp")
                            .Include("pdordemprodcomp.fsproduto")
                            .Include("pdordemprodcomp.fsproduto.fsprodutoparamestab")
                            .Include("pdordemprodcomp.fsproduto.fsprodutopcp")
                            .Include("pdordemprodcomp.fsproduto.fsprodutoparamestab.esprodenderec")
                            .Include("pdordemprodcomp.fsproduto.fsprodutoparamestab.esprodenderec.esenderecamento")
                            .Where(r => r.codordprod == pCodOrdProd && r.nivelordprod == pNivelOrdProd && r.codestab == pCodEstab && r.status == "P")
                            .FirstOrDefaultAsync();

        public async Task<PDOrdemProd> GetOPByBarCode(short pCodEstab, string pBarCode)
        {
            var code = Convert.ToInt64(pBarCode.Length >= 14 ? pBarCode.Substring(4) : pBarCode);
            return await AddQueryProperties(DbEntity,
                             e => e.fsproduto,
                             e => e.pdordemprodcomp,
                             e => e.pdordemprodrotinas
                            )
                            .Include("fsproduto.fsprodutoparamestab")
                            .Include("fsproduto.fsprodutopcp")
                            .Include("pdordemprodcomp.fsproduto")
                            .Include("pdordemprodcomp.fsproduto.fsprodutoparamestab")
                            .Include("pdordemprodcomp.fsproduto.fsprodutopcp")
                            .Include("pdordemprodcomp.fsproduto.fsprodutoparamestab.esprodenderec")
                            .Include("pdordemprodcomp.fsproduto.fsprodutoparamestab.esprodenderec.esenderecamento")
                            .Where(r =>
                                r.seqbarras == code &&
                                r.codestab == pCodEstab &&
                                r.status == "P")
                            .FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<PDEficienciaProducao>> BuscaEficienciaProducaoAsync(
            short pCodEstab, DateTime pDtInicio, DateTime pDtTermino)
        {
            var retorno = new List<PDEficienciaProducao>();

            using (var cmd = DbConn.Database.Connection.CreateCommand())
            {
                try
                {
                    await cmd.Connection.OpenAsync();
                }
                catch
                {
                }

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PCKG_CUBOSCENARIOS.CALCEFICPROD";

                cmd.Parameters.AddRange(new[]
                {
                    new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input),
                    new OracleParameter("pDATAINI", OracleDbType.Date, pDtInicio, ParameterDirection.Input),
                    new OracleParameter("pDATATERM", OracleDbType.Date, pDtTermino, ParameterDirection.Input)
                });

                try
                {
                    await cmd.ExecuteNonQueryAsync();
                }
                catch (Exception e)
                {
                    cmd.Connection.Close();
                    throw new Exception(e.Message);
                }

                cmd.CommandType = CommandType.Text;
                cmd.CommandText = SelectBuscaEficProducao;
                cmd.Parameters.Clear();

                try
                {
                    using (var reader = await cmd.ExecuteReaderAsync())
                    {
                        retorno = new List<PDEficienciaProducao>(reader.Select(r => new PDEficienciaProducao
                        {
                            CodEstab = pCodEstab,
                            Processo = Convert.ToString(r["PROCESSO"]),
                            OP = Convert.ToString(r["OP"]),
                            Produto = Convert.ToString(r["PRODUTO"]),
                            TotalProgramado = Convert.ToDecimal(r["TOTALPROGRAMADO"]),
                            Turno = Convert.ToString(r["TURNO"]),
                            MetaDia = Convert.ToDecimal(r["METADIA"]),
                            QtdeProd = Convert.ToDecimal(r["QTDEPROD"]),
                            QtdeRef = Convert.ToDecimal(r["QTDEREF"]),
                            Eficiencia = Convert.ToDecimal(r["EFICIENCIA"]),
                            Critica = Convert.ToString(r["CRITICA"]),
                            Maquina = Convert.ToString(r["MAQUINA"]),
                            Funcionario = Convert.ToString(r["FUNCIONARIO"]),
                            EficTotal = Convert.ToDecimal(r["EFICTOT"]),
                            QtdMaquinas = Convert.ToDecimal(r["QTDMAQ"]),
                            PercRefugo = Convert.ToDecimal(r["PERCREFUGO"])
                        }));
                    }
                }
                catch (Exception e)
                {
                    cmd.Connection.Close();
                    throw new Exception(e.Message);
                }

                cmd.Connection.Close();

                return retorno;
            }
        }

        public async Task<long?> GetCodEstruturaAsync(string pCodProduto, short pCodEstab)
        {
            long? CodEstrutura = null;
            var SqlLote = @"SELECT PCKG_ENGENHARIA.ESTRUTURAVIGENTE(:pCODCOMPONENTE, :pCODESTAB)
                              FROM DUAL";

            using (var cmd = DbConn.Database.Connection.CreateCommand())
            {
                cmd.CommandText = SqlLote;
                cmd.Connection = DbConn.Database.Connection;

                cmd.Parameters.AddRange(new[]
                {
                    new OracleParameter("pCODCOMPONENTE", OracleDbType.Varchar2, pCodProduto, ParameterDirection.Input),
                    new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input)
                });
                try
                {
                    await base.DbConn.Database.Connection.OpenAsync();
                }
                catch (Exception)
                {
                }

                try
                {
                    var result = await cmd.ExecuteScalarAsync();
                    if (result != null)
                        CodEstrutura = Convert.ToInt64(result);
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
                try
                {
                    base.DbConn.Database.Connection.Close();
                }
                catch (Exception)
                {
                }
            }

            return CodEstrutura;
        }
    }
}
