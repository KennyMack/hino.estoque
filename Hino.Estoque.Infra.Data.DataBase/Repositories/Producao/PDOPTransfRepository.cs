using Hino.Estoque.Domain.Producao.Interfaces.Repositories;
using Hino.Estoque.Infra.Cross.Entities.Producao;
using Hino.Estoque.Infra.Data.DataBase.Context;
using Hino.Estoque.Infra.Data.DataBase.Repositories;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using System;
using System.Linq;
using System.Data;
using System.Threading.Tasks;

namespace Hino.Estoque.Infra.DataBase.Repositories.Producao
{
    public class PDOPTransfRepository : BaseRepository<PDOPTransf>, IPDOPTransfRepository
    {
        public PDOPTransfRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }

        public async Task<double> QtdCompTransferida(short pCodEstab, decimal pCodOrdProd, string pNivelOrdProd, string pCodComponente) =>
            (await QueryAsync(r => 
                r.codestab == pCodEstab && 
                r.codordprod == pCodOrdProd &&
                r.nivelordprod == pNivelOrdProd && 
                r.codcomponente == pCodComponente &&
                r.status == 1)).Sum(r => r.quantidade);

        public async Task<bool> AtualizaTransfOP(short pCodEstab, byte pStatus, PDOPTransf pPDOPTransf)
        {
            using (var cmd = DbConn.Database.Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PCKG_PRODUCAO.ATUALIZATRANSFOP";

                cmd.Parameters.AddRange(new[]
                {
                    new OracleParameter("nRETURN", OracleDbType.Int32, ParameterDirection.ReturnValue),
                    new OracleParameter("p1", OracleDbType.Int32, pPDOPTransf.codestab, ParameterDirection.Input),
                    new OracleParameter("p2", OracleDbType.Decimal, pPDOPTransf.codordprod, ParameterDirection.Input),
                    new OracleParameter("p3", OracleDbType.Varchar2, pPDOPTransf.nivelordprod, ParameterDirection.Input),
                    new OracleParameter("p4", OracleDbType.Varchar2, pPDOPTransf.codusuario, ParameterDirection.Input),
                    new OracleParameter("p5", OracleDbType.Decimal, pPDOPTransf.codtransf, ParameterDirection.Input),
                    new OracleParameter("p6", OracleDbType.Int32, pStatus, ParameterDirection.Input),
                    new OracleParameter("pMSERRO", OracleDbType.Clob, ParameterDirection.Output)
                });
                
                try
                {
                    await cmd.Connection.OpenAsync();
                    await cmd.ExecuteNonQueryAsync();
                    var nret =cmd.Parameters["nRETURN"].Value.ToString();
                    var ms = ((OracleClob)cmd.Parameters["pMSERRO"].Value).Value.ToString();

                    if (ms != "*OK*")
                        throw new Exception(ms);
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
                finally
                {
                    cmd.Connection.Close();
                }
            }

            return true;
        }
    }
}
