﻿using Hino.Estoque.Domain.Producao.Interfaces.Repositories.OEE;
using Hino.Estoque.Infra.Cross.Entities.Producao.OEE;
using Hino.Estoque.Infra.Data.DataBase.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Infra.Data.DataBase.Repositories.Producao.OEE
{
    public class PDOEEParamRepository : BaseRepository<PDOEEParam>, IPDOEEParamRepository
    {
        public PDOEEParamRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }

        public IEnumerable<PDOEEParam> GetOEEColorsByEstabAsync(short pCodEstab) =>
            DbEntity.Where(r => r.codestab == pCodEstab).ToList();
    }
}
