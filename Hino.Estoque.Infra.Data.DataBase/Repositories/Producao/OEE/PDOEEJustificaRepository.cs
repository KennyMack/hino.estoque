﻿using Hino.Estoque.Domain.Producao.Interfaces.Repositories.OEE;
using Hino.Estoque.Infra.Cross.Entities.Producao.OEE;
using Hino.Estoque.Infra.Data.DataBase.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Infra.Data.DataBase.Repositories.Producao.OEE
{
    public class PDOEEJustificaRepository : BaseRepository<PDOEEJustifica>, IPDOEEJustificaRepository
    {
        public PDOEEJustificaRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }

        public IEnumerable<PDOEEJustifica> GetOEEJustificativas(short pCodEstab, string pCodMaquina, DateTime pDtPeriodo)
        {
            var dtInicio = new DateTime(pDtPeriodo.Year, pDtPeriodo.Month, 1);
            var dtTerminio = new DateTime(pDtPeriodo.Year, pDtPeriodo.Month,
                        DateTime.DaysInMonth(pDtPeriodo.Year, pDtPeriodo.Month));

            return DbEntity.Where(r => 
                r.codestab == pCodEstab &&
                r.codmaquina == pCodMaquina &&
                (dtInicio.CompareTo(r.dataoee) <= 0) && (r.dataoee.CompareTo(dtTerminio) <= 0)).ToList();
        }
    }
}
