using Hino.Estoque.Domain.Producao.Interfaces.Repositories;
using Hino.Estoque.Infra.Cross.Entities.Producao;
using Hino.Estoque.Infra.Cross.Utils.Reader;
using Hino.Estoque.Infra.Data.DataBase.Context;
using Hino.Estoque.Infra.Data.DataBase.Repositories;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace Hino.Estoque.Infra.DataBase.Repositories.Producao
{
    public class PDOrdemProdCompRepository : BaseRepository<PDOrdemProdComp>, IPDOrdemProdCompRepository
    {
        public PDOrdemProdCompRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }

        public async Task<IEnumerable<PDOrdemProdCompEstqEnd>> BuscaListaEstqEndAsync(string pCodProduto, short pCodEstab)
        {
            var retorno = new List<PDOrdemProdCompEstqEnd>();

            using (var cmd = DbConn.Database.Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = @"SELECT ESTOQUE.CODESTAB, ESTOQUE.CODESTOQUE, LOCALESTOQUE.DESCRICAO, 
                                           ESTOQUE.CODPRODUTO, ESTOQUE.SALDODISPONIVEL
                                      FROM FSVWSALDOESTOQUE ESTOQUE,
                                           FSPRODUTOPARAMESTAB PRODUTO,
                                           FSLOCALESTOQUE LOCALESTOQUE,
                                           FSPARAMLOCESTOQESTAB LOCALESTESTAB
                                     WHERE ESTOQUE.CODESTAB           = PRODUTO.CODESTAB 
                                       AND ESTOQUE.CODPRODUTO         = PRODUTO.CODPRODUTO
                                       AND LOCALESTOQUE.CODESTOQUE    = ESTOQUE.CODESTOQUE
                                       AND LOCALESTOQUE.CODGRUPOESTOQ = PRODUTO.CODESTOQUE
                                       AND LOCALESTESTAB.CODESTAB     = ESTOQUE.CODESTAB
                                       AND LOCALESTESTAB.CODESTOQUE   = ESTOQUE.CODESTOQUE 
                                       AND ESTOQUE.STATUS             = 1
                                       AND ESTOQUE.SALDODISPONIVEL    > 0
                                       AND ESTOQUE.CODESTAB           = :pCODESTAB
                                       AND ESTOQUE.CODPRODUTO         = :pCODPRODUTO
                                     ORDER BY LOCALESTESTAB.RUA, 
                                              LOCALESTESTAB.ANDAR, 
                                              LOCALESTESTAB.PREDIO, 
                                              LOCALESTESTAB.APARTAMENTO";


                cmd.Parameters.AddRange(new[]
                {
                    new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input),
                    new OracleParameter("pCODPRODUTO", OracleDbType.Varchar2, pCodProduto, ParameterDirection.Input)
                });

                try
                {
                    await cmd.Connection.OpenAsync();

                    using (var reader = await cmd.ExecuteReaderAsync())
                    {
                        retorno = new List<PDOrdemProdCompEstqEnd>(reader.Select(r => new PDOrdemProdCompEstqEnd
                        {
                            codestab = pCodEstab,
                            codestoque = Convert.ToString(r["CODESTOQUE"]),
                            descricao = Convert.ToString(r["DESCRICAO"]),
                            codproduto = Convert.ToString(r["CODPRODUTO"]),
                            saldodisponivel = Convert.ToDecimal(r["SALDODISPONIVEL"])
                        }));
                    }
                }
                catch (Exception ex)
                {
                    cmd.Connection.Close();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    cmd.Connection.Close();
                }
            }

            return retorno;
        }
    }
}
