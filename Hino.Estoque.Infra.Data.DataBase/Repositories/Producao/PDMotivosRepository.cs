﻿using Hino.Estoque.Domain.Producao.Interfaces.Repositories;
using Hino.Estoque.Infra.Cross.Entities.Producao;
using Hino.Estoque.Infra.Data.DataBase.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Infra.Data.DataBase.Repositories.Producao
{
    public class PDMotivosRepository : BaseRepository<PDMotivos>, IPDMotivosRepository
    {
        public PDMotivosRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
