using Hino.Estoque.Domain.Producao.Interfaces.Repositories;
using Hino.Estoque.Infra.Cross.Entities.Producao;
using Hino.Estoque.Infra.Data.DataBase.Context;
using Hino.Estoque.Infra.Data.DataBase.Repositories;

namespace Hino.Estoque.Infra.DataBase.Repositories.Producao
{
    public class PDOrdemProdRotinasRepository : BaseRepository<PDOrdemProdRotinas>, IPDOrdemProdRotinasRepository
    {
        public PDOrdemProdRotinasRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
