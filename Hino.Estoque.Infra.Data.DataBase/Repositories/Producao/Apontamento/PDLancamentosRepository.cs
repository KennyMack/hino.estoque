using Hino.Estoque.Domain.Producao.Interfaces.Repositories.Apontamento;
using Hino.Estoque.Infra.Cross.Entities.Producao.Apontamento;
using Hino.Estoque.Infra.Data.DataBase.Context;
using Hino.Estoque.Infra.Data.DataBase.Repositories;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.Estoque.Infra.DataBase.Repositories.Producao.Apontamento
{
    public class PDLancamentosRepository : BaseRepository<PDLancamentos>, IPDLancamentosRepository
    {
        public PDLancamentosRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }

        DbRawSqlQuery<decimal> GetNextSequence()
        {
            var sql = $"SELECT SEQ_PDLANCAMENTO.NEXTVAL FROM DUAL";

            return DbConn.Database.SqlQuery<decimal>(sql);
        }

        public override long NextSequence() =>
            Convert.ToInt64(GetNextSequence().First());

        public override async Task<long> NextSequenceAsync() =>
            Convert.ToInt64(await GetNextSequence().FirstAsync());

        async Task LimparTabelaApontamentoAsync(DbCommand cmd)
        {
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = @"DELETE FROM PDTTLANCPRODUCAO";

            try
            {
                await cmd.ExecuteNonQueryAsync();
            }
            catch (Exception)
            {
                throw;
            }
        }

        async Task LimparTabelaRefugoAsync(DbCommand cmd)
        {
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = @"DELETE FROM PDTTLANCPRODREFUGO";

            try
            {
                await cmd.ExecuteNonQueryAsync();
            }
            catch (Exception)
            {
                throw;
            }
        }

        async Task SalvarLancamentoTempAsync(DbCommand cmd, PDLancamentos pLancamento)
        {
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = @"INSERT INTO PDTTLANCPRODUCAO (
                                        CODESTAB, CODLANCAMENTO, CODORDPROD, NIVELORDPROD, 
                                        OPERACAO, CODFUNCIONARIO, QUANTIDADE, DTINICIO, DTTERMINO, 
                                        CODESTRUTURA, CODROTEIRO, TURNO, 
                                        CODMAQUINA, TERMINOTURNO)
                                    VALUES (:pCODESTAB, :pCODLANCAMENTO, :pCODORDPROD, :pNIVELORDPROD, 
                                        :pOPERACAO, :pCODFUNCIONARIO, :pQUANTIDADE, :pDTINICIO, :pDTTERMINO, 
                                        :pCODESTRUTURA, :pCODROTEIRO, :pTURNO, 
                                        :pCODMAQUINA, :pTERMINOTURNO)";

            cmd.Parameters.AddRange(new[]
            {
                    new OracleParameter("pCODESTAB", OracleDbType.Int32, pLancamento.codestab, ParameterDirection.Input),
                    new OracleParameter("pCODLANCAMENTO", OracleDbType.Int64, pLancamento.codlancamento, ParameterDirection.Input),
                    new OracleParameter("pCODORDPROD", OracleDbType.Int64, pLancamento.codordprod, ParameterDirection.Input),
                    new OracleParameter("pNIVELORDPROD", OracleDbType.Varchar2, pLancamento.nivelordprod, ParameterDirection.Input),
                    new OracleParameter("pOPERACAO", OracleDbType.Int32, pLancamento.operacao, ParameterDirection.Input),
                    new OracleParameter("pCODFUNCIONARIO", OracleDbType.Int32, pLancamento.codfuncionario, ParameterDirection.Input),
                    new OracleParameter("pQUANTIDADE", OracleDbType.Decimal, pLancamento.quantidade, ParameterDirection.Input),
                    new OracleParameter("pDTINICIO", OracleDbType.Date, pLancamento.dtinicio, ParameterDirection.Input),
                    new OracleParameter("pDTTERMINO", OracleDbType.Date, pLancamento.dttermino, ParameterDirection.Input),
                    new OracleParameter("pCODESTRUTURA", OracleDbType.Int32, pLancamento.codestrutura, ParameterDirection.Input),
                    new OracleParameter("pCODROTEIRO", OracleDbType.Int32, pLancamento.codroteiro, ParameterDirection.Input),
                    new OracleParameter("pTURNO", OracleDbType.Varchar2, pLancamento.turno, ParameterDirection.Input),
                    new OracleParameter("pCODMAQUINA", OracleDbType.Varchar2, pLancamento.codmaquina, ParameterDirection.Input),
                    new OracleParameter("pTERMINOTURNO", OracleDbType.Int32, pLancamento.terminoturno, ParameterDirection.Input)
                });

            try
            {
                await cmd.ExecuteNonQueryAsync();
            }
            catch (Exception)
            {
                throw;
            }
        }

        async Task SalvarRefugoLancamentoTempAsync(DbCommand cmd, long pCodLancamento, PDLancamentos pLancamento, PDLancMotivoRefugo pRefugo)
        {
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = @"INSERT INTO PDTTLANCPRODREFUGO (CODESTAB, CODLANCAMENTO, CODLANCTEMP, CODMOTIVO, 
                                                                QUANTIDADE, QTDSUCATA, QTDREAPROVEITADA)
                                                        VALUES (:pCODESTAB, :pCODLANCAMENTO, :pCODLANCTEMP, :pCODMOTIVO, 
                                                                :pQUANTIDADE, :pQTDSUCATA, :pQTDREAPROVEITADA)";
            cmd.Parameters.Clear();
            cmd.Parameters.AddRange(new[]
            {
                new OracleParameter("pCODESTAB", OracleDbType.Int32, pLancamento.codestab, ParameterDirection.Input),
                new OracleParameter("pCODLANCAMENTO", OracleDbType.Int64, pCodLancamento, ParameterDirection.Input),
                new OracleParameter("pCODLANCTEMP", OracleDbType.Int64, pLancamento.codlancamento, ParameterDirection.Input),
                new OracleParameter("pCODMOTIVO", OracleDbType.Varchar2, pRefugo.codmotivo, ParameterDirection.Input),
                new OracleParameter("pQUANTIDADE", OracleDbType.Int32, pRefugo.quantidade, ParameterDirection.Input),
                new OracleParameter("pQTDSUCATA", OracleDbType.Int32, pRefugo.qtdsucata, ParameterDirection.Input),
                new OracleParameter("pQTDREAPROVEITADA", OracleDbType.Int32, pRefugo.qtdreaproveitada, ParameterDirection.Input)
            });

            try
            {
                await cmd.ExecuteNonQueryAsync();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<bool> ApontamentoProducaoTempAsync(long pCodLancamento, PDLancamentos pLancamento, List<PDLancMotivoRefugo> pRefugos)
        {
            var retorno = true;
            using (var cmd = DbConn.Database.Connection.CreateCommand())
            {
                await cmd.Connection.OpenAsync();
                var transaction = cmd.Connection.BeginTransaction();

                cmd.CommandType = CommandType.Text;

                try
                {
                    await LimparTabelaApontamentoAsync(cmd);
                    await LimparTabelaRefugoAsync(cmd);

                    await SalvarLancamentoTempAsync(cmd, pLancamento);

                    foreach (var item in pRefugos)
                        await SalvarRefugoLancamentoTempAsync(cmd, pCodLancamento, pLancamento, item);
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    cmd.Connection.Close();
                    throw;
                }

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = @"PCKG_PROG_PRODUCAO.APONTAMENTOPRODUCAOTEMP";

                cmd.Parameters.Clear();
                cmd.Parameters.AddRange(new[]
                {
                    new OracleParameter("nRETURN", OracleDbType.Int32, ParameterDirection.ReturnValue),
                    new OracleParameter("p1", OracleDbType.Int32, pLancamento.codestab, ParameterDirection.Input),
                    new OracleParameter("p2", OracleDbType.Int64, pCodLancamento, ParameterDirection.Input),
                    new OracleParameter("p3", OracleDbType.Int64, pLancamento.codlancamento, ParameterDirection.Input),
                    new OracleParameter("p4", OracleDbType.Int64, pLancamento.codordprod, ParameterDirection.Input),
                    new OracleParameter("p5", OracleDbType.Varchar2, pLancamento.nivelordprod, ParameterDirection.Input),
                    new OracleParameter("p6", OracleDbType.Int64, pLancamento.codestrutura, ParameterDirection.Input),
                    new OracleParameter("p7", OracleDbType.Decimal, pRefugos.Sum(r => r.quantidade), ParameterDirection.Input),
                    new OracleParameter("p8", OracleDbType.Varchar2, pLancamento.codusuario, ParameterDirection.Input),
                    new OracleParameter("pMSERRO", OracleDbType.Clob, ParameterDirection.Output)
                });

                try
                {
                    await cmd.ExecuteNonQueryAsync();
                    var nret = cmd.Parameters["nRETURN"].Value.ToString();
                    var ms = ((OracleClob)cmd.Parameters["pMSERRO"].Value).Value.ToString();

                    if (ms != "*OK*")
                        throw new Exception(ms);
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    cmd.Connection.Close();
                    throw;
                }

                try
                {
                    await LimparTabelaApontamentoAsync(cmd);
                    await LimparTabelaRefugoAsync(cmd);
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    cmd.Connection.Close();
                    throw;
                }

                transaction.Commit();
                cmd.Connection.Close();
            }

            return retorno;
        }

        public async Task<bool> ValidateAppointmentAsync(PDLancamentos pLancamento)
        {
            var retorno = true;
            using (var cmd = DbConn.Database.Connection.CreateCommand())
            {
                await cmd.Connection.OpenAsync();
                cmd.CommandType = CommandType.Text;

                await LimparTabelaApontamentoAsync(cmd);

                await SalvarLancamentoTempAsync(cmd, pLancamento);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = @"PCKG_PROG_PRODUCAO.VALIDARAPONTAMENTOPROD";

                cmd.Parameters.Clear();
                cmd.Parameters.AddRange(new[]
                {
                    new OracleParameter("nRETURN", OracleDbType.Int32, ParameterDirection.ReturnValue),
                    new OracleParameter("p1", OracleDbType.Int32, pLancamento.codestab, ParameterDirection.Input),
                    new OracleParameter("p2", OracleDbType.Decimal, pLancamento.codlancamento, ParameterDirection.Input),
                    new OracleParameter("p3", OracleDbType.Varchar2, pLancamento.codusuario, ParameterDirection.Input),
                    new OracleParameter("pMSERRO", OracleDbType.Clob, ParameterDirection.Output)
                });

                try
                {
                    await cmd.ExecuteNonQueryAsync();
                    var nret = cmd.Parameters["nRETURN"].Value.ToString();
                    var ms = ((OracleClob)cmd.Parameters["pMSERRO"].Value).Value.ToString();

                    if (ms != "*OK*")
                        throw new Exception(ms);
                }
                catch (Exception)
                {
                    cmd.Connection.Close();
                    throw;
                }

                await LimparTabelaApontamentoAsync(cmd);

                cmd.Connection.Close();
            }

            return retorno;
        }

        public async Task<bool> VerifyOperations(PDLancamentos pLancamento)
        {
            var retorno = true;
            using (var cmd = DbConn.Database.Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = @"PCKG_PRODUCAO.VERIFICAOPERACOES";

                cmd.Parameters.AddRange(new[]
                {
                    new OracleParameter("nRETURN", OracleDbType.Int32, ParameterDirection.ReturnValue),
                    new OracleParameter("p1", OracleDbType.Int32, pLancamento.codestab, ParameterDirection.Input),
                    new OracleParameter("p2", OracleDbType.Decimal, pLancamento.codordprod, ParameterDirection.Input),
                    new OracleParameter("p3", OracleDbType.Varchar2, pLancamento.nivelordprod, ParameterDirection.Input),
                    new OracleParameter("p4", OracleDbType.Varchar2, pLancamento.operacao, ParameterDirection.Input),
                    new OracleParameter("p5", OracleDbType.Decimal, pLancamento.quantidade, ParameterDirection.Input),
                    new OracleParameter("pMSERRO", OracleDbType.Clob, ParameterDirection.Output)
                });

                try
                {
                    await cmd.Connection.OpenAsync();
                    await cmd.ExecuteNonQueryAsync();
                    var nret = cmd.Parameters["nRETURN"].Value.ToString();
                    var ms = ((OracleClob)cmd.Parameters["pMSERRO"].Value).Value.ToString();

                    if (ms != "*OK*")
                        throw new Exception(ms);
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    cmd.Connection.Close();
                }
            }

            return retorno;
        }
    }
}
