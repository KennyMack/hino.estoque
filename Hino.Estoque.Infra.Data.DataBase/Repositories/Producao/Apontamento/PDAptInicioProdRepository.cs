﻿using Hino.Estoque.Domain.Producao.Interfaces.Repositories.Apontamento;
using Hino.Estoque.Infra.Cross.Entities.Producao.Apontamento;
using Hino.Estoque.Infra.Data.DataBase.Context;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Infra.Data.DataBase.Repositories.Producao.Apontamento
{
    public class PDAptInicioProdRepository : BaseRepository<PDAptInicioProd>, IPDAptInicioProdRepository
    {
        public PDAptInicioProdRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }

        public async Task<PDAptInicioProd> GetLastAptStartAsync(
            short pCodEstab,
            int pCodFuncionario,
            long pCodOrdProd,
            string pNivelOrdProd,
            long pCodEstrutura,
            int pCodRoteiro,
            int pOperacao,
            short pTipo
        )
        {
            var retorno = new PDAptInicioProd();
            var codIniApt = -1;
            using (var cmd = DbConn.Database.Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = @"SELECT NVL(MAX(PDAPTINICIOPROD.CODINIAPT), -1)
                                      FROM PDAPTINICIOPROD
                                     WHERE PDAPTINICIOPROD.CODESTRUTURA = :pCODESTRUTURA
                                       AND PDAPTINICIOPROD.CODFUNCIONARIO = :pCODFUNCIONARIO
                                       AND PDAPTINICIOPROD.CODORDPROD = :pCODORDPROD
                                       AND PDAPTINICIOPROD.CODROTEIRO = :pCODROTEIRO
                                       AND PDAPTINICIOPROD.NIVELORDPROD = :pNIVELORDPROD
                                       AND PDAPTINICIOPROD.OPERACAO = :pOPERACAO
                                       AND PDAPTINICIOPROD.TIPO = :pTIPO
                                       AND PDAPTINICIOPROD.CODESTAB = :pCODESTAB
                                       AND NOT EXISTS (SELECT 1
                                                         FROM PDAPTTERMINOPROD
                                                        WHERE PDAPTTERMINOPROD.CODINIAPT = PDAPTINICIOPROD.CODINIAPT
                                                          AND PDAPTTERMINOPROD.CODESTAB  = PDAPTINICIOPROD.CODESTAB)";

                cmd.Parameters.AddRange(new[]
                {
                    new OracleParameter("pCODORDPROD", OracleDbType.Int64, pCodEstrutura, ParameterDirection.Input),
                    new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodFuncionario, ParameterDirection.Input),
                    new OracleParameter("pCODORDPROD", OracleDbType.Int64, pCodOrdProd, ParameterDirection.Input),
                    new OracleParameter("pCODORDPROD", OracleDbType.Int32, pCodRoteiro, ParameterDirection.Input),
                    new OracleParameter("pCODORDPROD", OracleDbType.Varchar2, pNivelOrdProd, ParameterDirection.Input),
                    new OracleParameter("pCODORDPROD", OracleDbType.Int32, pOperacao, ParameterDirection.Input),
                    new OracleParameter("pCODORDPROD", OracleDbType.Int16, pTipo, ParameterDirection.Input),
                    new OracleParameter("pCODORDPROD", OracleDbType.Int32, pCodEstab, ParameterDirection.Input)
                });

                try
                {
                    await cmd.Connection.OpenAsync();
                    codIniApt = (int.Parse((await cmd.ExecuteScalarAsync()).ToString()));
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    cmd.Connection.Close();
                }
            }

            if (codIniApt > -1)
                retorno = (await QueryAsync(r => r.codestab == pCodEstab && r.codiniapt == codIniApt)).FirstOrDefault();

            return retorno;
        }

        public async Task<bool> ExisteInspecaoRotinaAnteriorAsync(short pCodEstab, PDAptInicioProd pPDAptInicioProd)
        {
            var retorno = true;
            using (var cmd = DbConn.Database.Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = @"SELECT COUNT(*) TOTREG
                                  FROM PDLANCAMENTOS,
                                       PDORDEMPRODROTINAS,
                                       PDORDEMPROD,
                                       QLPLANOINSP,
                                       QLPLANOINSPDET
                                 WHERE PDLANCAMENTOS.CODORDPROD = :pCODORDPROD
                                   AND PDLANCAMENTOS.CODESTAB = :pCODESTAB
                                   AND PDLANCAMENTOS.NIVELORDPROD = :pNIVELORDPROD
                                   AND PDLANCAMENTOS.CODESTRUTURA = :pCODESTRUTURA
                                   AND PDLANCAMENTOS.OPERACAO <= :pOPERACAO
                                   AND PDORDEMPROD.CODESTAB = PDLANCAMENTOS.CODESTAB
                                   AND PDORDEMPROD.CODESTRUTURA = PDLANCAMENTOS.CODESTRUTURA
                                   AND PDORDEMPROD.CODORDPROD = PDLANCAMENTOS.CODORDPROD
                                   AND PDORDEMPROD.CODROTEIRO = PDLANCAMENTOS.CODROTEIRO
                                   AND PDORDEMPROD.NIVELORDPROD = PDLANCAMENTOS.NIVELORDPROD
                                   AND PDORDEMPRODROTINAS.CODESTAB = PDLANCAMENTOS.CODESTAB
                                   AND PDORDEMPRODROTINAS.CODORDPROD = PDLANCAMENTOS.CODORDPROD
                                   AND PDORDEMPRODROTINAS.CODESTRUTURA = PDLANCAMENTOS.CODESTRUTURA
                                   AND PDORDEMPRODROTINAS.CODROTEIRO = PDLANCAMENTOS.CODROTEIRO
                                   AND PDORDEMPRODROTINAS.NIVELORDPROD = PDLANCAMENTOS.NIVELORDPROD
                                   AND PDORDEMPRODROTINAS.OPERACAO = PDLANCAMENTOS.OPERACAO
                                   AND QLPLANOINSP.CODPRODUTO = PDORDEMPROD.CODPRODUTO
                                   AND QLPLANOINSP.CODESTAB = PDORDEMPROD.CODESTAB
                                   AND QLPLANOINSPDET.CODPLANOINSP = QLPLANOINSP.CODPLANOINSP
                                   AND QLPLANOINSPDET.CODROTINA = PDORDEMPRODROTINAS.CODROTINA
                                   AND QLPLANOINSP.STATUS = 'V'
                                   AND ( NOT EXISTS ( SELECT 1
                                                        FROM QLINSPRECEB
                                                       WHERE QLINSPRECEB.CODESTAB = PDLANCAMENTOS.CODESTAB
                                                         AND QLINSPRECEB.CODLANCAMENTO = PDLANCAMENTOS.CODLANCAMENTO )
                                    OR  EXISTS ( SELECT 1
                                                   FROM QLINSPRECEB,
                                                        QLINSPPLANREC
                                                  WHERE QLINSPRECEB.CODESTAB = PDLANCAMENTOS.CODESTAB
                                                    AND QLINSPRECEB.CODLANCAMENTO = PDLANCAMENTOS.CODLANCAMENTO
                                                    AND QLINSPPLANREC.CODINSPRECEBE = QLINSPRECEB.CODINSPRECEBE
                                                    AND QLINSPPLANREC.CODESTAB = QLINSPRECEB.CODESTAB
                                                    AND QLINSPPLANREC.STATUS = 0 ) )";

                cmd.Parameters.AddRange(new[]
                {
                    new OracleParameter("pCODORDPROD", OracleDbType.Decimal, pPDAptInicioProd.codordprod, ParameterDirection.Input),
                    new OracleParameter("pCODESTAB", OracleDbType.Int32, pPDAptInicioProd.codestab, ParameterDirection.Input),
                    new OracleParameter("pNIVELORDPROD", OracleDbType.Varchar2, pPDAptInicioProd.nivelordprod, ParameterDirection.Input),
                    new OracleParameter("pCODESTRUTURA", OracleDbType.Decimal, pPDAptInicioProd.codestrutura, ParameterDirection.Input),
                    new OracleParameter("pOPERACAO", OracleDbType.Int16, pPDAptInicioProd.operacao, ParameterDirection.Input)
                });

                try
                {
                    await cmd.Connection.OpenAsync();
                    retorno = (int.Parse((await cmd.ExecuteScalarAsync()).ToString()) > 0);
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    cmd.Connection.Close();
                }
            }

            return retorno;
        }

        public async Task<bool> OperacaoJaIniciadaAsync(short pCodEstab, PDAptInicioProd pAptIniCreate)
        {
            var retorno = true;
            using (var cmd = DbConn.Database.Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = @"SELECT COUNT(*) TOTREG
                                      FROM PDAPTINICIOPROD
                                     WHERE PDAPTINICIOPROD.CODESTRUTURA   = :pCODESTRUTURA
                                       AND PDAPTINICIOPROD.CODFUNCIONARIO = :pCODFUNCIONARIO
                                       AND PDAPTINICIOPROD.CODORDPROD     = :pCODORDPROD
                                       AND PDAPTINICIOPROD.CODROTEIRO     = :pCODROTEIRO
                                       AND PDAPTINICIOPROD.NIVELORDPROD   = :pNIVELORDPROD
                                       AND PDAPTINICIOPROD.CODUSUARIO     = :pCODUSUARIO
                                       AND PDAPTINICIOPROD.OPERACAO       = :pOPERACAO
                                       AND PDAPTINICIOPROD.TIPO           = :pTIPO
                                       AND PDAPTINICIOPROD.CODESTAB       = :pCODESTAB
                                       AND NOT EXISTS (SELECT 1
                                                         FROM PDAPTTERMINOPROD
                                                        WHERE PDAPTTERMINOPROD.CODINIAPT = PDAPTINICIOPROD.CODINIAPT
                                                          AND PDAPTTERMINOPROD.CODESTAB  = PDAPTINICIOPROD.CODESTAB)";

                cmd.Parameters.AddRange(new[]
                {
                    new OracleParameter("pCODESTRUTURA", OracleDbType.Decimal, pAptIniCreate.codestrutura, ParameterDirection.Input),
                    new OracleParameter("pCODFUNCIONARIO", OracleDbType.Decimal, pAptIniCreate.codfuncionario, ParameterDirection.Input),
                    new OracleParameter("pCODORDPROD", OracleDbType.Decimal, pAptIniCreate.codordprod, ParameterDirection.Input),
                    new OracleParameter("pCODROTEIRO", OracleDbType.Int16, pAptIniCreate.codroteiro, ParameterDirection.Input),
                    new OracleParameter("pNIVELORDPROD", OracleDbType.Varchar2, pAptIniCreate.nivelordprod, ParameterDirection.Input),
                    new OracleParameter("pCODUSUARIO", OracleDbType.Varchar2, pAptIniCreate.codusuario, ParameterDirection.Input),
                    new OracleParameter("pOPERACAO", OracleDbType.Int16, pAptIniCreate.operacao, ParameterDirection.Input),
                    new OracleParameter("pTIPO", OracleDbType.Int16, pAptIniCreate.tipo, ParameterDirection.Input),
                    new OracleParameter("pCODESTAB", OracleDbType.Int32, pAptIniCreate.codestab, ParameterDirection.Input)
                });

                try
                {
                    await cmd.Connection.OpenAsync();
                    retorno = (int.Parse((await cmd.ExecuteScalarAsync()).ToString()) > 0);
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    cmd.Connection.Close();
                }
            }

            return retorno;
        }
    }
}
