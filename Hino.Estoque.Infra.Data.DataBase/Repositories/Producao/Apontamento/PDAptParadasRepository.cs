﻿using Hino.Estoque.Domain.Producao.Interfaces.Repositories.Apontamento;
using Hino.Estoque.Infra.Cross.Entities.Producao.Apontamento;
using Hino.Estoque.Infra.Data.DataBase.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Infra.Data.DataBase.Repositories.Producao.Apontamento
{
    public class PDAptParadasRepository : BaseRepository<PDAptParadas>, IPDAptParadasRepository
    {
        public PDAptParadasRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
