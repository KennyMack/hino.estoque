﻿using Hino.Estoque.Domain.Producao.Interfaces.Repositories.Apontamento;
using Hino.Estoque.Infra.Cross.Entities.Producao.Apontamento;
using Hino.Estoque.Infra.Cross.Utils;
using Hino.Estoque.Infra.Data.DataBase.Context;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Infra.Data.DataBase.Repositories.Producao.Apontamento
{
    public class PDAptTerminoProdRepository : BaseRepository<PDAptTerminoProd>, IPDAptTerminoProdRepository
    {
        public PDAptTerminoProdRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }

        public async Task<IEnumerable<PDAptLancList>> GetListApontamentoAsync(short pCodEstab, int pCodFuncionario)
        {
            var retorno = new List<PDAptLancList>();
            using (var cmd = DbConn.Database.Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = @"SELECT TBBASE.CODINIAPT, TBBASE.CODFIMAPT, PDAPTLANCAMENTO.CODLANCAMENTO,
                                          TBBASE.TIPO, TBBASE.DTINICIO, TBBASE.DTTERMINO, TBBASE.CODORDPROD,
                                          TBBASE.NIVELORDPROD, TBBASE.CODROTEIRO, TBBASE.OPERACAO, TBBASE.PDMOTIVOSDESCRICAO,
                                          TBBASE.TURNOPAD, TBBASE.CODMAQUINA, TBBASE.CODPRODUTO, TBBASE.FSPRODUTODESCRICAO,
                                          TBBASE.CODESTRUTURA, TBBASE.QUANTIDADE, TBBASE.QTDREFUGO, TBBASE.QTDRETRABALHO,
                                          TBBASE.CODFUNCIONARIO, TBBASE.GEPESSOANOME, TBBASE.CODUSUARIO, TBBASE.PDORDEMPRODROTINASDESCRICAO,
                                          PDAPTPARADAS.CODPARADA, PDAPTRETRABALHO.CODRETRAB,
                                          NVL(PDLANCAMENTOS.TOTALMIN, 0) TOTALMIN, 
                                          PCKG_UTEIS.MINTOHORAMIN(NVL(PDLANCAMENTOS.TOTALMIN, 0)) DESCTOTALMIN,
                                          NVL(PDLANCAMENTOS.TOTALMINESP, 0) TOTALMINESP,
                                          PCKG_UTEIS.MINTOHORAMIN(NVL(PDLANCAMENTOS.TOTALMINESP, 0)) DESCTOTALMINESP,
                                          NVL(PDLANCAMENTOS.PRODUTIVIDADE, 0) PRODUTIVIDADE
                                     FROM PDAPTLANCAMENTO,
                                          PDAPTPARADAS, 
                                          PDAPTRETRABALHO,
                                          PDLANCAMENTOS,
                                          (SELECT PDAPTINICIOPROD.TIPO,
                                                  PDAPTINICIOPROD.CODORDPROD, PDAPTINICIOPROD.NIVELORDPROD, FSPRODUTO.CODPRODUTO, FSPRODUTO.DESCRICAO FSPRODUTODESCRICAO,
                                                  PDAPTINICIOPROD.CODESTAB, PDAPTINICIOPROD.CODESTRUTURA, 
                                                  PDAPTINICIOPROD.DTINICIO, PDAPTTERMINOPROD.DTTERMINO,
                                                  PDAPTINICIOPROD.CODFUNCIONARIO, GEPESSOA.NOME GEPESSOANOME, 
                                                  PDAPTINICIOPROD.CODINIAPT, PDAPTTERMINOPROD.CODFIMAPT,
                                                  PDAPTINICIOPROD.CODROTEIRO, PDAPTINICIOPROD.CODUSUARIO,
                                                  PDAPTINICIOPROD.OPERACAO, NVL(PDAPTTERMINOPROD.QUANTIDADE, 0) QUANTIDADE, 
                                                  PDAPTTERMINOPROD.CODMOTIVO, PDMOTIVOS.DESCRICAO PDMOTIVOSDESCRICAO,
                                                  NVL(PDAPTTERMINOPROD.QTDREFUGO, 0) QTDREFUGO, NVL(PDAPTTERMINOPROD.QTDRETRABALHO, 0) QTDRETRABALHO,
                                                  PDORDEMPRODROTINAS.CODMAQUINA, ENMAQUINAS.DESCRICAO ENMAQUINASDESCRICAO,
                                                  GEFUNCIONARIOS.TURNO TURNOPAD, PDTURNOS.DESCRICAO PDTURNOSDESCRICAOPAD,
                                                  PDAPTTERMINOPROD.OBSERVACAO, PDORDEMPRODROTINAS.DESCRICAO PDORDEMPRODROTINASDESCRICAO
                                             FROM PDAPTINICIOPROD,
                                                  PDAPTTERMINOPROD,
                                                  PDORDEMPROD,
                                                  PDORDEMPRODROTINAS,
                                                  ENMAQUINAS,
                                                  FSPRODUTO,
                                                  GEFUNCIONARIOS,
                                                  GEPESSOA,
                                                  PDMOTIVOS,
                                                  PDTURNOS
                                            WHERE PDAPTTERMINOPROD.CODINIAPT   (+)= PDAPTINICIOPROD.CODINIAPT    
                                              AND PDAPTTERMINOPROD.CODESTAB    (+)= PDAPTINICIOPROD.CODESTAB
                                              AND PDORDEMPROD.CODORDPROD          = PDAPTINICIOPROD.CODORDPROD
                                              AND PDORDEMPROD.NIVELORDPROD        = PDAPTINICIOPROD.NIVELORDPROD
                                              AND PDORDEMPROD.CODESTAB            = PDAPTINICIOPROD.CODESTAB
                                              AND PDORDEMPROD.CODESTRUTURA        = PDAPTINICIOPROD.CODESTRUTURA
                                              AND PDORDEMPRODROTINAS.CODESTAB     = PDORDEMPROD.CODESTAB
                                              AND PDORDEMPRODROTINAS.CODESTRUTURA = PDORDEMPROD.CODESTRUTURA
                                              AND PDORDEMPRODROTINAS.CODORDPROD   = PDORDEMPROD.CODORDPROD
                                              AND PDORDEMPRODROTINAS.NIVELORDPROD = PDORDEMPROD.NIVELORDPROD
                                              AND PDORDEMPRODROTINAS.CODROTEIRO   = PDORDEMPROD.CODROTEIRO
                                              AND PDORDEMPRODROTINAS.OPERACAO     = PDAPTINICIOPROD.OPERACAO
                                              AND ENMAQUINAS.CODMAQUINA           = PDORDEMPRODROTINAS.CODMAQUINA
                                              AND ENMAQUINAS.CODESTAB             = PDORDEMPRODROTINAS.CODESTAB
                                              AND FSPRODUTO.CODPRODUTO            = PDORDEMPROD.CODPRODUTO
                                              AND GEFUNCIONARIOS.CODFUNCIONARIO   = PDAPTINICIOPROD.CODFUNCIONARIO
                                              AND GEPESSOA.CODPESSOA              = GEFUNCIONARIOS.CODPESSOA
                                              AND PDTURNOS.TURNO               (+)= GEFUNCIONARIOS.TURNO
                                              AND PDTURNOS.CODESTAB            (+)= GEFUNCIONARIOS.CODESTAB
                                              AND PDMOTIVOS.CODMOTIVO         (+)= PDAPTTERMINOPROD.CODMOTIVO) TBBASE
                                    WHERE PDAPTLANCAMENTO.CODESTAB            (+)= TBBASE.CODESTAB
                                      AND PDAPTLANCAMENTO.CODFIMAPT           (+)= TBBASE.CODFIMAPT
                                      AND PDAPTPARADAS.CODESTAB               (+)= TBBASE.CODESTAB
                                      AND PDAPTPARADAS.CODFIMAPT              (+)= TBBASE.CODFIMAPT
                                      AND PDAPTRETRABALHO.CODESTAB            (+)= TBBASE.CODESTAB
                                      AND PDAPTRETRABALHO.CODFIMAPT           (+)= TBBASE.CODFIMAPT
                                      AND PDLANCAMENTOS.CODESTAB              (+)= PDAPTLANCAMENTO.CODESTAB
                                      AND PDLANCAMENTOS.CODLANCAMENTO         (+)= PDAPTLANCAMENTO.CODLANCAMENTO
                                      AND TBBASE.CODESTAB                        = :pCODESTAB
                                      AND TBBASE.CODFUNCIONARIO                  = :pCODFUNCIONARIO
                                      AND TRUNC(TBBASE.DTINICIO)                >= TRUNC(SYSDATE - 5)
                                    ORDER BY TBBASE.CODINIAPT DESC";

                cmd.Parameters.AddRange(new[]
                {
                    new OracleParameter("pCODORDPROD", OracleDbType.Int32, pCodEstab, ParameterDirection.Input),
                    new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodFuncionario, ParameterDirection.Input)
                });

                try
                {
                    await cmd.Connection.OpenAsync();

                    using (var reader = await cmd.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            var apt = new PDAptLancList
                            {
                                CodAptInicio = Convert.ToInt64(reader["CODINIAPT"]),
                                Tipo = Convert.ToInt16(reader["TIPO"]),
                                DtInicio = Convert.ToDateTime(reader["DTINICIO"]),
                                CodOrdProd = Convert.ToInt64(reader["CODORDPROD"]),
                                NivelOrdProd = Convert.ToString(reader["NIVELORDPROD"]),
                                CodEstrutura = Convert.ToInt64(reader["CODESTRUTURA"]),
                                CodRoteiro = Convert.ToInt32(reader["CODROTEIRO"]),
                                Operacao = Convert.ToInt32(reader["OPERACAO"]),
                                DescOperacao = Convert.ToString(reader["PDORDEMPRODROTINASDESCRICAO"]),
                                Descricao = Convert.ToString(reader["FSPRODUTODESCRICAO"]),
                                CodProduto = Convert.ToString(reader["CODPRODUTO"]),
                                Quantidade = Convert.ToDecimal(reader["QUANTIDADE"]),
                                QtdRefugo = Convert.ToDecimal(reader["QTDREFUGO"]),
                                CodUsuario = Convert.ToString(reader["CODUSUARIO"]),
                                CodFuncionario = Convert.ToInt32(reader["CODFUNCIONARIO"]),
                                NomeFuncionario = Convert.ToString(reader["GEPESSOANOME"]),
                                Turno = Convert.ToString(reader["TURNOPAD"]),
                                CodMaquina = Convert.ToString(reader["CODMAQUINA"]),
                                DtTermino = ConvertEx.ToDateTime(reader["DTTERMINO"]),
                                CodAptTermino = ConvertEx.ToInt64(reader["CODFIMAPT"]),
                                CodLancamento = ConvertEx.ToInt64(reader["CODLANCAMENTO"]),
                                TotalMin = Convert.ToInt32(reader["TOTALMIN"]),
                                DescTotalMin = Convert.ToString(reader["DESCTOTALMIN"]),
                                TotalMinEsp = Convert.ToInt32(reader["TOTALMINESP"]),
                                DescTotalMinEsp = Convert.ToString(reader["DESCTOTALMINESP"]),
                                Produtividade = Convert.ToInt32(reader["PRODUTIVIDADE"])
                            };

                            retorno.Add(apt);
                        }
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    cmd.Connection.Close();
                }
            }

            return retorno;
        }
    }
}
