﻿using Hino.Estoque.Domain.Producao.Interfaces.Repositories.Apontamento;
using Hino.Estoque.Infra.Cross.Entities.Producao.Apontamento;
using Hino.Estoque.Infra.Data.DataBase.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Infra.Data.DataBase.Repositories.Producao.Apontamento
{
    public class PDParadasRepository : BaseRepository<PDParadas>, IPDParadasRepository
    {
        public PDParadasRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
