﻿using Hino.Estoque.Domain.Producao.Interfaces.Repositories.Apontamento;
using Hino.Estoque.Infra.Cross.Entities.Producao.Apontamento;
using Hino.Estoque.Infra.Data.DataBase.Context;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Hino.Estoque.Infra.Data.DataBase.Repositories.Producao.Apontamento
{
    public class PDSucataRepository : BaseRepository<PDSucata>, IPDSucataRepository
    {
        public PDSucataRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }

        public async Task<bool> ApontaSucataAsync(short pCodEstab, long pCodSucata, string pCodUsuario)
        {
            using (var cmd = DbConn.Database.Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PCKG_PRODUCAO.APONTASUCATA";

                cmd.Parameters.AddRange(new[]
                {
                    new OracleParameter("p1", OracleDbType.Int32, pCodEstab, ParameterDirection.Input),
                    new OracleParameter("p2", OracleDbType.Int64, pCodSucata, ParameterDirection.Input),
                    new OracleParameter("p3", OracleDbType.Varchar2, pCodUsuario, ParameterDirection.Input)
                });

                try
                {
                    await cmd.Connection.OpenAsync();

                    await cmd.ExecuteNonQueryAsync();
                }
                catch (Exception e)
                {
                    var mensErro = "";
                    if (e.Message.IndexOf("20099") > -1)
                    {
                        Regex red = new Regex(@"((99\:)(.*))", RegexOptions.IgnoreCase);
                        mensErro = red.Matches(e.Message)[0].Value.Substring(3);
                    }
                    else if (e.Message.IndexOf("20010") > -1)
                    {
                        Regex red = new Regex(@"((10\:)(.*))", RegexOptions.IgnoreCase);
                        mensErro = red.Matches(e.Message)[0].Value.Substring(3);
                    }
                    else
                        mensErro = $"Problema ao incluir o registro. Motivo: {e.Message}";

                    throw new Exception(mensErro);
                }
                finally
                {
                    cmd.Connection.Close();
                }
            }

            return true;
        }

        public async Task<bool> ValidaDtSuc(short pCodEstab, decimal pCodOrdProd, string pNivelOrdProd, DateTime pDataConsumo)
        {
            using (var cmd = DbConn.Database.Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;

                cmd.CommandText = @"SELECT COUNT(1)
                                      FROM (
                                            SELECT MAX(TRUNC(PDLANCAMENTOS.DTTERMINO)) DATAAPTO
                                              FROM PDLANCAMENTOS
                                             WHERE PDLANCAMENTOS.CODESTAB = :pCODESTAB
                                               AND PDLANCAMENTOS.CODORDPROD = :pCODORDPROD
                                               AND PDLANCAMENTOS.NIVELORDPROD = :pNIVELORDPROD
                                            )
                                     WHERE TRUNC(:pDATACONS) > DATAAPTO";

                cmd.Parameters.AddRange(new[]
                {
                    new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input),
                    new OracleParameter("pCODORDPROD", OracleDbType.Varchar2, pCodOrdProd, ParameterDirection.Input),
                    new OracleParameter("pNIVELORDPROD", OracleDbType.Varchar2, pNivelOrdProd, ParameterDirection.Input),
                    new OracleParameter("pDATACONS", OracleDbType.Decimal, pDataConsumo, ParameterDirection.Input)
                });

                try
                {
                    await cmd.Connection.OpenAsync();
                    return Convert.ToInt32(await cmd.ExecuteScalarAsync()) > 0;
                }
                catch (Exception)
                {
                }
                finally
                {
                    cmd.Connection.Close();
                }
            }

            return false;
        }
    }
}
