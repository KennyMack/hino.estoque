﻿using Hino.Estoque.Infra.Cross.Entities.Manutencao;
using Hino.Estoque.Infra.Data.DataBase.Context;
using Hino.Estoque.Domain.Manutencao.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;

namespace Hino.Estoque.Infra.Data.DataBase.Repositories.Manutencao
{
    public class MNSolicManutRepository : BaseRepository<MNSolicManut>, IMNSolicManutRepository
    {
        public MNSolicManutRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }

        public async Task<string> BuscaAprovadorPadraoAsync(int pCodEstab)
        {
            using (var cmd = DbConn.Database.Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;

                cmd.CommandText = $"SELECT MNPARAMETROS.USUAPROVSO FROM MNPARAMETROS WHERE CODESTAB = :pCODESTAB";
                
                cmd.Parameters.AddRange(new[]
                {
                    new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input)
                });

                try
                {
                    await cmd.Connection.OpenAsync();

                    return Convert.ToString(await cmd.ExecuteScalarAsync());
                }
                catch (Exception)
                {
                }
                finally
                {
                    cmd.Connection.Close();
                }
            }

            return "";
        }

        public async Task<string> AtualizaSolicitacaoAsync(
            short pCodEstab, long pCodSolic, string pStatus)
        {
            var retorno = "";

            using (var cmd = DbConn.Database.Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PCKG_MANUTENCAO.ATUALIZASOLICITACAO";

                cmd.Parameters.AddRange(new[]
                {
                    new OracleParameter("nRETURN", OracleDbType.Clob, ParameterDirection.ReturnValue),
                    new OracleParameter("pCODSOLIC", OracleDbType.Int64, pCodSolic, ParameterDirection.Input),
                    new OracleParameter("pSTATUS", OracleDbType.Varchar2, pStatus, ParameterDirection.Input)
                });

                try
                {
                    await cmd.Connection.OpenAsync();

                    await cmd.ExecuteNonQueryAsync();

                    retorno = ((OracleClob)cmd.Parameters["nRETURN"].Value).Value.ToString();

                    if (!retorno.ToUpper().Contains("SUCESSO") &&
                        !retorno.ToUpper().Contains("VOLTOU"))
                        throw new Exception(retorno);
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
                finally
                {
                    cmd.Connection.Close();
                }
            }

            return retorno;
        }
    }
}
