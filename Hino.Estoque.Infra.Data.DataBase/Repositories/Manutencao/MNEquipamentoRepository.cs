﻿using Hino.Estoque.Domain.Manutencao.Interfaces.Repositories;
using Hino.Estoque.Infra.Cross.Entities.Manutencao;
using Hino.Estoque.Infra.Data.DataBase.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Infra.Data.DataBase.Repositories.Manutencao
{
    public class MNEquipamentoRepository : BaseRepository<MNEquipamento>, IMNEquipamentoRepository
    {
        public MNEquipamentoRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
