﻿using Hino.Estoque.Domain.Gerais.Interfaces.Repositories;
using Hino.Estoque.Infra.Cross.Entities.Gerais;
using Hino.Estoque.Infra.Data.DataBase.Context;

namespace Hino.Estoque.Infra.Data.DataBase.Repositories.Gerais
{
    public class CZIntegrEmpresaRepository : BaseRepository<CZIntegrEmpresa>, ICZIntegrEmpresaRepository
    {
        public CZIntegrEmpresaRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
