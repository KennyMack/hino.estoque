using System.Linq;
using System.Threading.Tasks;
using Hino.Estoque.Domain.Gerais.Interfaces.Repositories;
using Hino.Estoque.Infra.Cross.Entities.Gerais;
using Hino.Estoque.Infra.Data.DataBase.Context;
using Hino.Estoque.Infra.Data.DataBase.Repositories;

namespace Hino.Estoque.Infra.DataBase.Repositories.Gerais
{
    public class GEEstabRepository : BaseRepository<GEEstab>, IGEEstabRepository
    {
        public GEEstabRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }

        public async Task<GEEstab> GetEstabById(int pCodEstab) =>
            (await QueryAsync(r => r.codestab == pCodEstab)).FirstOrDefault();
    }
}
