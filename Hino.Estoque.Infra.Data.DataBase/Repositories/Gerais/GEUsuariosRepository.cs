using Hino.Estoque.Domain.Gerais.Interfaces.Repositories;
using Hino.Estoque.Infra.Cross.Entities.Gerais;
using Hino.Estoque.Infra.Data.DataBase.Context;
using Hino.Estoque.Infra.Data.DataBase.Repositories;

namespace Hino.Estoque.Infra.DataBase.Repositories.Gerais
{
    public class GEUsuariosRepository : BaseRepository<GEUsuarios>, IGEUsuariosRepository
    {
        public GEUsuariosRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
