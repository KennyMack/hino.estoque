﻿using Hino.Estoque.Domain.Gerais.Interfaces.Repositories;
using Hino.Estoque.Infra.Cross.Entities.Gerais;
using Hino.Estoque.Infra.Data.DataBase.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Infra.Data.DataBase.Repositories.Gerais
{
    public class CZIntegracaoRevisaoRepository : BaseRepository<CZIntegracaoRevisao>, ICZIntegracaoRevisaoRepository
    {
        public CZIntegracaoRevisaoRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
