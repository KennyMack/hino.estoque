﻿using Hino.Estoque.Domain.Estoque.Interfaces.Repositories.Estoque;
using Hino.Estoque.Infra.Cross.Entities.Estoque;
using Hino.Estoque.Infra.Data.DataBase.Context;

namespace Hino.Estoque.Infra.Data.DataBase.Repositories.Estoque
{
    public class ESEnderecamentoRepository : BaseRepository<ESEnderecamento>, IESEnderecamentoRepository
    {
        public ESEnderecamentoRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
