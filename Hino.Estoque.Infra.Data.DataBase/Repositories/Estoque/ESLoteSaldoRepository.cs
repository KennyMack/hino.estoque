using Hino.Estoque.Domain.Estoque.Interfaces.Repositories.Estoque;
using Hino.Estoque.Infra.Cross.Entities.Estoque;
using Hino.Estoque.Infra.Cross.Utils;
using Hino.Estoque.Infra.Cross.Utils.Extensions;
using Hino.Estoque.Infra.Data.DataBase.Context;
using Hino.Estoque.Infra.Data.DataBase.Repositories;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.Estoque.Infra.DataBase.Repositories.Estoque
{
    public class ESLoteSaldoRepository : BaseRepository<ESLoteSaldo>, IESLoteSaldoRepository
    {
        public ESLoteSaldoRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }

        public async Task<double> SaldoEstoqueLote(short pCodEstab, decimal pLote, string pCodEstoque) =>
            (await QueryAsync(r => r.codestab == pCodEstab && r.codestoque == pCodEstoque &&
                r.lote == pLote)).Sum(r => r.qtdentrada - r.qtdsaida);


        public async Task<IEnumerable<ESSaldoLoteOP>> BuscaLoteSaldoOPAsync(int pCodEstab, string pCodComponente, string pCodEstoqueOri, long pCodOrdProd, string pNivelOrdProd)
        {
            var lotes = new List<ESSaldoLoteOP>();
            var SqlLote = @"SELECT TBTRANSFERENCIA.CODCOMPONENTE, TBTRANSFERENCIA.LOTE, TBTRANSFERENCIA.QTDENECESSARIA,  
                                     TBTRANSFERENCIA.CODESTOQUEORI, TBTRANSFERENCIA.SALDOLOTE SALDOESTOQUEPADRAO, 
                                     TBTRANSFERENCIA.CODESTOQUEDEST,
                                     TBTRANSFERENCIA.QUANTIDADETRANSF, TBTRANSFERENCIA.SALDOTRANSF, TBTRANSFERENCIA.MESMOLOCAL,
                                     TBTRANSFERENCIA.CODORDPROD, TBTRANSFERENCIA.NIVELORDPROD, TBTRANSFERENCIA.CODESTRUTURA, 
                                     TBTRANSFERENCIA.CODROTEIRO, TBTRANSFERENCIA.OPERACAO, TBTRANSFERENCIA.CODOPLOTE, TBTRANSFERENCIA.CODOPLOTEORIGINAL, 
                                     TBTRANSFERENCIA.LOTELIVRE, TBTRANSFERENCIA.DATALOTE, 
                                     TBTRANSFERENCIA.ORIGEMLOTE, TBTRANSFERENCIA.SEQLOTE
                                FROM (SELECT TBITENSLOTE.CODESTAB, TBITENSLOTE.CODCOMPONENTE, TBITENSLOTE.LOTE, TBITENSLOTE.CODESTOQUEORI, 
                                            TBITENSLOTE.QTDENECESSARIA, TBITENSLOTE.SALDOLOTE, TBITENSLOTE.CODESTOQUEDEST,
                                            TBITENSLOTE.CODORDPROD, TBITENSLOTE.NIVELORDPROD, TBITENSLOTE.CODESTRUTURA, 
                                            TBITENSLOTE.CODROTEIRO, TBITENSLOTE.OPERACAO,
                                            DECODE(TBITENSLOTE.CODESTOQUEORI, TBITENSLOTE.CODESTOQUEDEST, '1', '0') MESMOLOCAL, 
                                            NVL(TBOPTRANSF.QUANTIDADE, 0) QUANTIDADETRANSF,
                                            CASE 
                                                WHEN TBITENSLOTE.CODESTOQUEORI = TBITENSLOTE.CODESTOQUEDEST THEN
                                                0
                                                WHEN TBITENSLOTE.QTDENECESSARIA - SUM(NVL(TBOPTRANSF.QUANTIDADE, 0)) 
                                                OVER (PARTITION BY TBITENSLOTE.CODCOMPONENTE, TBITENSLOTE.CODORDPROD, TBITENSLOTE.NIVELORDPROD) <= 0 THEN  
                                                0
                                                ELSE
                                                TBITENSLOTE.QTDENECESSARIA - SUM(NVL(TBOPTRANSF.QUANTIDADE, 0)) 
                                                OVER (PARTITION BY TBITENSLOTE.CODCOMPONENTE, TBITENSLOTE.CODORDPROD, TBITENSLOTE.NIVELORDPROD)
                                            END SALDOTRANSF, TBITENSLOTE.DATALOTE, TBITENSLOTE.CODOPLOTE, TBITENSLOTE.SEQLOTE, TBITENSLOTE.CODOPLOTEORIGINAL, TBITENSLOTE.LOTELIVRE, TBITENSLOTE.ORIGEMLOTE
                                        FROM (SELECT PDORDEMPRODCOMP.CODESTAB, PDORDEMPRODCOMP.CODCOMPONENTE, TBLOTE.LOTE, FSPRODUTOPARAMESTAB.CODESTOQUE CODESTOQUEORI, 
                                                     PDORDEMPRODCOMP.QUANTIDADE QTDENECESSARIA, TBLOTE.SALDOLOTE, PDORDEMPRODCOMP.CODESTOQUE CODESTOQUEDEST,
                                                     PDORDEMPRODCOMP.CODORDPROD, PDORDEMPRODCOMP.NIVELORDPROD, PDORDEMPRODCOMP.CODESTRUTURA, 
                                                     PDORDEMPRODCOMP.CODROTEIRO, PDORDEMPRODCOMP.OPERACAO, TBLOTE.DATALOTE, TBLOTE.CODOPLOTE, TBLOTE.CODOPLOTEORIGINAL, TBLOTE.LOTELIVRE,
                                                     TBLOTE.ORIGEM ORIGEMLOTE, TBLOTE.SEQLOTE
                                                FROM PDORDEMPRODCOMP,
                                                     FSPRODUTOPARAMESTAB,
                                                     (SELECT ESLOTE.LOTE, ESLOTE.CODESTAB, ESLOTE.CODPRODUTO, ESLOTESALDO.CODESTOQUE, ESLOTE.ORIGEM, ESLOTE.LOTELIVRE,
                                                             ESLOTESALDO.SALDODISPONIVEL SALDOLOTE, ESLOTE.DATALOTE,
                                                             NVL(PDLANCAMENTOS.CODORDPROD, ESLOTE.LOTE) CODOPLOTEORIGINAL,
                                                             NVL(DECODE(ESLOTE.LOTELIVRE, 1, :pCODORDPROD, PDLANCAMENTOS.CODORDPROD), :pCODORDPROD) CODOPLOTE,
                                                             ESLOTE.SEQLOTE
                                                        FROM ESLOTE,
                                                             ESVWLOTESALDO ESLOTESALDO,
                                                             PDLANCAMENTOS
                                                       WHERE ESLOTE.LOTE                     = ESLOTESALDO.LOTE
                                                         AND ESLOTE.CODESTAB                 = ESLOTESALDO.CODESTAB
                                                         AND PDLANCAMENTOS.CODESTAB       (+)= ESLOTE.CODESTAB
                                                         AND PDLANCAMENTOS.CODLANCAMENTO  (+)= ESLOTE.CODLANCAMENTO) TBLOTE
                                               WHERE FSPRODUTOPARAMESTAB.CODESTAB         = PDORDEMPRODCOMP.CODESTAB
                                                 AND FSPRODUTOPARAMESTAB.CODPRODUTO       = PDORDEMPRODCOMP.CODCOMPONENTE
                                                 AND TBLOTE.CODPRODUTO                    = PDORDEMPRODCOMP.CODCOMPONENTE
                                                 AND TBLOTE.CODESTAB                      = PDORDEMPRODCOMP.CODESTAB
                                                 AND TBLOTE.CODESTOQUE                    = :pCODESTOQUEORI) TBITENSLOTE,
                                            (SELECT PDOPTRANSF.CODESTAB, PDOPTRANSF.CODORDPROD, PDOPTRANSF.NIVELORDPROD, PDOPTRANSFLOTE.LOTE,
                                                    PDOPTRANSF.CODESTRUTURA, PDOPTRANSF.CODROTEIRO, PDOPTRANSF.OPERACAO, PDOPTRANSF.CODCOMPONENTE, 
                                                    PDOPTRANSF.CODESTOQUEORI, PDOPTRANSF.CODESTOQUEDEST, SUM(PDOPTRANSFLOTE.QUANTIDADE) QUANTIDADE,
                                                    MAX(PDOPTRANSF.DATATRANSF) DATATRANSF, MIN(PDOPTRANSF.CODUSUARIO) CODUSUARIO
                                                FROM PDOPTRANSF,
                                                    PDOPTRANSFLOTE
                                                WHERE PDOPTRANSF.CODESTAB    = PDOPTRANSFLOTE.CODESTAB
                                                AND PDOPTRANSF.CODTRANSF   = PDOPTRANSFLOTE.CODTRANSF
                                                AND PDOPTRANSF.STATUS      = 1
                                                GROUP BY PDOPTRANSF.CODESTAB, PDOPTRANSF.CODORDPROD, PDOPTRANSF.NIVELORDPROD, PDOPTRANSFLOTE.LOTE,
                                                    PDOPTRANSF.CODESTRUTURA, PDOPTRANSF.CODROTEIRO, PDOPTRANSF.OPERACAO, PDOPTRANSF.CODCOMPONENTE, 
                                                    PDOPTRANSF.CODESTOQUEORI, PDOPTRANSF.CODESTOQUEDEST) TBOPTRANSF
                                        WHERE TBOPTRANSF.CODESTAB              (+)= TBITENSLOTE.CODESTAB
                                        AND TBOPTRANSF.CODORDPROD            (+)= TBITENSLOTE.CODORDPROD
                                        AND TBOPTRANSF.NIVELORDPROD          (+)= TBITENSLOTE.NIVELORDPROD
                                        AND TBOPTRANSF.CODCOMPONENTE         (+)= TBITENSLOTE.CODCOMPONENTE
                                        AND TBOPTRANSF.CODESTRUTURA          (+)= TBITENSLOTE.CODESTRUTURA
                                        AND TBOPTRANSF.CODROTEIRO            (+)= TBITENSLOTE.CODROTEIRO
                                        AND TBOPTRANSF.OPERACAO              (+)= TBITENSLOTE.OPERACAO
                                        AND TBOPTRANSF.LOTE                  (+)= TBITENSLOTE.LOTE
                                        AND (TBITENSLOTE.SALDOLOTE > 0 
                                        OR TBOPTRANSF.LOTE IS NOT NULL)) TBTRANSFERENCIA,
                                    (SELECT PDTTOPTRANSFLOTE.CODESTAB, PDTTOPTRANSFLOTE.LOTE, PDTTOPTRANSFLOTE.CODORDPROD, PDTTOPTRANSFLOTE.NIVELORDPROD, 
                                            PDTTOPTRANSFLOTE.CODESTRUTURA, PDTTOPTRANSFLOTE.CODROTEIRO, PDTTOPTRANSFLOTE.OPERACAO, PDTTOPTRANSFLOTE.CODCOMPONENTE, 
                                            PDTTOPTRANSFLOTE.CODESTOQUEORI, PDTTOPTRANSFLOTE.CODESTOQUEDEST, PDTTOPTRANSFLOTE.QUANTIDADE  
                                        FROM PDTTOPTRANSFLOTE) TBLOTESSEL,
                                    PDPARAMESTAB
                              WHERE TBLOTESSEL.CODESTAB            (+)= TBTRANSFERENCIA.CODESTAB
                                AND TBLOTESSEL.LOTE                (+)= TBTRANSFERENCIA.LOTE
                                AND TBLOTESSEL.CODORDPROD          (+)= TBTRANSFERENCIA.CODORDPROD
                                AND TBLOTESSEL.NIVELORDPROD        (+)= TBTRANSFERENCIA.NIVELORDPROD
                                AND TBLOTESSEL.CODESTRUTURA        (+)= TBTRANSFERENCIA.CODESTRUTURA
                                AND TBLOTESSEL.CODROTEIRO          (+)= TBTRANSFERENCIA.CODROTEIRO
                                AND TBLOTESSEL.OPERACAO            (+)= TBTRANSFERENCIA.OPERACAO
                                AND TBLOTESSEL.CODCOMPONENTE       (+)= TBTRANSFERENCIA.CODCOMPONENTE
                                AND PDPARAMESTAB.CODESTAB             = TBTRANSFERENCIA.CODESTAB
                                AND TBTRANSFERENCIA.CODESTAB          = :pCODESTAB
                                AND TBTRANSFERENCIA.CODORDPROD        = :pCODORDPROD
                                AND TBTRANSFERENCIA.NIVELORDPROD      = :pNIVELORDPROD
                                AND TBTRANSFERENCIA.CODCOMPONENTE     = :pCODCOMPONENTE
                                AND (
                                    ((
                                      PDPARAMESTAB.CONSUMOLOTE = 1 
                                 AND TBTRANSFERENCIA.CODOPLOTE        = :pCODORDPROD) OR
                                    (PDPARAMESTAB.USALOTEPOROP = 1 
                                 AND TBTRANSFERENCIA.CODOPLOTE        = :pCODORDPROD
                                    )
                                 ) OR
                                    (PDPARAMESTAB.CONSUMOLOTE = 0 AND PDPARAMESTAB.USALOTEPOROP = 0))
                              ORDER BY TBTRANSFERENCIA.DATALOTE, 
                                       TBTRANSFERENCIA.LOTE";

            
            using (var cmd = base.DbConn.Database.Connection.CreateCommand())
            {
                cmd.CommandText = SqlLote;
                cmd.Connection = base.DbConn.Database.Connection;

                cmd.Parameters.AddRange(new[]
                {
                    new OracleParameter("pCODORDPROD", OracleDbType.Decimal, pCodOrdProd, ParameterDirection.Input),
                    new OracleParameter("pCODORDPROD", OracleDbType.Decimal, pCodOrdProd, ParameterDirection.Input),
                    new OracleParameter("pCODESTOQUEORI", OracleDbType.Varchar2, pCodEstoqueOri, ParameterDirection.Input),
                    new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input),
                    new OracleParameter("pCODORDPROD", OracleDbType.Decimal, pCodOrdProd, ParameterDirection.Input),
                    new OracleParameter("pNIVELORDPROD", OracleDbType.Varchar2, pNivelOrdProd, ParameterDirection.Input),
                    new OracleParameter("pCODCOMPONENTE", OracleDbType.Varchar2, pCodComponente, ParameterDirection.Input),
                    new OracleParameter("pCODORDPROD", OracleDbType.Decimal, pCodOrdProd, ParameterDirection.Input),
                    new OracleParameter("pCODORDPROD", OracleDbType.Decimal, pCodOrdProd, ParameterDirection.Input),
                });
                try
                {
                    await base.DbConn.Database.Connection.OpenAsync();
                }
                catch (Exception) 
                { 
                }

                try
                {
                    using (var r = await cmd.ExecuteReaderAsync())
                    {
                        while (r.Read())
                        {
                            var item = new ESSaldoLoteOP();

                            item.CODCOMPONENTE = Convert.ToString(r["CODCOMPONENTE"]);
                            item.LOTE = Convert.ToInt64(r["LOTE"]);
                            item.QTDENECESSARIA = Convert.ToDecimal(r["QTDENECESSARIA"]);
                            item.CODESTOQUEORI = Convert.ToString(r["CODESTOQUEORI"]);
                            item.SALDOESTOQUEPADRAO = Convert.ToDouble(r["SALDOESTOQUEPADRAO"]);
                            item.CODESTOQUEDEST = Convert.ToString(r["CODESTOQUEDEST"]);
                            item.QUANTIDADETRANSF = Convert.ToDecimal(r["QUANTIDADETRANSF"]);
                            item.SALDOTRANSF = Convert.ToDecimal(r["SALDOTRANSF"]);
                            item.MESMOLOCAL = Convert.ToInt32(r["MESMOLOCAL"]);
                            item.CODORDPROD = Convert.ToInt64(r["CODORDPROD"]);
                            item.NIVELORDPROD = Convert.ToString(r["NIVELORDPROD"]);
                            item.CODESTRUTURA = Convert.ToInt64(r["CODESTRUTURA"]);
                            item.CODROTEIRO = Convert.ToInt32(r["CODROTEIRO"]);
                            item.OPERACAO = Convert.ToInt32(r["OPERACAO"]);
                            item.CODOPLOTE = Convert.ToInt64(r["CODOPLOTE"]);
                            item.CODOPLOTEORIGINAL = Convert.ToInt64(r["CODOPLOTEORIGINAL"]);
                            item.LOTELIVRE = Convert.ToInt16(r["LOTELIVRE"]);
                            item.DATALOTE = Convert.ToDateTime(r["DATALOTE"]);
                            item.ORIGEMLOTE = Convert.ToInt16(r["ORIGEMLOTE"]);

                            if (!r["SEQLOTE"].ToString().IsEmpty())
                                item.SEQLOTE = Convert.ToInt64(r["SEQLOTE"]);

                            lotes.Add(item);
                        }
                    }
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
                try
                {
                    base.DbConn.Database.Connection.Close();
                }
                catch (Exception)
                {
                }
            }
            return lotes;
        }


        #region Busca Lotes Unidade
        public async Task<IEnumerable<esTransfUNLotes>> GetLotesUnidadeNegAsync(int pCodEstab, string pCodProduto, string pCodEstoque)
        {
            var lotes = new List<esTransfUNLotes>();
            var SqlLote = @"SELECT ESLOTE.LOTE, ESLOTE.CODPRODUTO,  ESLOTESALDO.CODESTOQUE,
                                   NVL(ESLOTE.CODUNNEGOCIO, -1) CODUNNEGOCIO, GEUNNEGOCIO.DESCRICAO DESCUNNEGOCIO,
                                   ESLOTESALDO.QTDENTRADA, ESLOTESALDO.QTDSAIDA, ESLOTE.SEQLOTE,
                                   (ESLOTESALDO.QTDENTRADA - ESLOTESALDO.QTDSAIDA) SALDOLOTE
                              FROM ESLOTESALDO,
                                   FSSALDOESTOQUE,
                                   ESLOTE,
                                   GEUNNEGOCIO
                             WHERE ESLOTE.CODESTAB              = ESLOTESALDO.CODESTAB
                               AND ESLOTE.LOTE                  = ESLOTESALDO.LOTE
                               AND FSSALDOESTOQUE.CODESTAB      = ESLOTESALDO.CODESTAB
                               AND FSSALDOESTOQUE.CODESTOQUE    = ESLOTESALDO.CODESTOQUE
                               AND FSSALDOESTOQUE.CODPRODUTO    = ESLOTE.CODPRODUTO
                               AND GEUNNEGOCIO.CODESTAB      (+)= ESLOTE.CODESTAB
                               AND GEUNNEGOCIO.CODUNNEGOCIO  (+)= ESLOTE.CODUNNEGOCIO
                               AND ESLOTE.CODPRODUTO            = :pCODPRODUTO
                               AND ESLOTE.CODESTAB              = :pCODESTAB
                               AND ESLOTESALDO.CODESTOQUE       = :pCODESTOQUE
                               AND (ESLOTESALDO.QTDENTRADA - ESLOTESALDO.QTDSAIDA) > 0
                             ORDER BY (ESLOTESALDO.QTDENTRADA - ESLOTESALDO.QTDSAIDA) DESC";


            using (var cmd = base.DbConn.Database.Connection.CreateCommand())
            {
                cmd.CommandText = SqlLote;
                cmd.Connection = base.DbConn.Database.Connection;

                cmd.Parameters.AddRange(new[]
                {
                    new OracleParameter("pCODPRODUTO", OracleDbType.Varchar2, pCodProduto, ParameterDirection.Input),
                    new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input),
                    new OracleParameter("pCODESTOQUE", OracleDbType.Varchar2, pCodEstoque, ParameterDirection.Input)
                });
                try
                {
                    await base.DbConn.Database.Connection.OpenAsync();
                }
                catch (Exception) 
                { 
                }

                try
                {
                    using (var r = await cmd.ExecuteReaderAsync())
                    {
                        while (r.Read())
                        {
                            var item = new esTransfUNLotes();

                            item.CodLote = Convert.ToInt64(r["LOTE"]);
                            item.CodEstoque = r["CODESTOQUE"].ToString();
                            item.CodProduto = r["CODPRODUTO"].ToString();
                            item.Entrada = Convert.ToDecimal(r["QTDENTRADA"]);
                            item.Saida = Convert.ToDecimal(r["QTDSAIDA"]);
                            item.SeqLote = 0;
                            item.CodUnNegocio = null;

                            if (!r["SEQLOTE"].ToString().IsEmpty())
                                item.SeqLote = Convert.ToInt32(r["SEQLOTE"].ToString());

                            if (!r["CODUNNEGOCIO"].ToString().IsEmpty())
                                item.CodUnNegocio = Convert.ToInt32(r["CODUNNEGOCIO"].ToString());

                            lotes.Add(item);
                        }
                    }
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
                try
                {
                    base.DbConn.Database.Connection.Close();
                }
                catch (Exception)
                {
                }
            }
            return lotes;
        }
        #endregion
    }
}
