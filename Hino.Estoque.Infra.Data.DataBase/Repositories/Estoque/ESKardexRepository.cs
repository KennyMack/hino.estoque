using Hino.Estoque.Aplication.ViewModels.Estoque;
using Hino.Estoque.Domain.Estoque.Interfaces.Repositories.Estoque;
using Hino.Estoque.Infra.Cross.Entities.Estoque;
using Hino.Estoque.Infra.Data.DataBase.Context;
using Hino.Estoque.Infra.Data.DataBase.Repositories;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using System;
using System.Linq;
using System.Data;
using System.Globalization;
using System.Threading.Tasks;
using Hino.Estoque.Infra.Cross.Entities.Fiscal;

namespace Hino.Estoque.Infra.DataBase.Repositories.Estoque
{
    public class ESKardexRepository : BaseRepository<ESKardex>, IESKardexRepository
    {
        public ESKardexRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }

        public async Task<DateTime> GetDataFechamento(short pCodEstab)
        {
            using (var cmd = DbConn.Database.Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;

                cmd.CommandText = $"SELECT PCKG_ESTOQUE.GETDATAFEC({pCodEstab}) FROM DUAL";

                try
                {
                    await cmd.Connection.OpenAsync();

                    return Convert.ToDateTime(await cmd.ExecuteScalarAsync());
                }
                catch (Exception)
                {
                }
                finally
                {
                    cmd.Connection.Close();
                }
            }

            return new DateTime(1900, 01, 01);
        }

        public async Task<bool> GetValidaSaldoDataLancConsumo(short pCodEstab, string pCodProduto, string pCodEstoque, 
            decimal pQuantidade, DateTime pDtLanc)
        {
            using (var cmd = DbConn.Database.Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PCKG_PRODUCAO.GETVALIDASLDDTLANCCONSUMO";

                cmd.Parameters.AddRange(new[]
                {
                    new OracleParameter("nRETURN", OracleDbType.Clob, ParameterDirection.ReturnValue),
                    new OracleParameter("p1", OracleDbType.Int32, pCodEstab, ParameterDirection.Input),
                    new OracleParameter("p2", OracleDbType.Varchar2, pCodProduto, ParameterDirection.Input),
                    new OracleParameter("p3", OracleDbType.Varchar2, pCodEstoque, ParameterDirection.Input),
                    new OracleParameter("p4", OracleDbType.Decimal, pQuantidade, ParameterDirection.Input),
                    new OracleParameter("p5", OracleDbType.Date, pDtLanc, ParameterDirection.Input)
                });

                try
                {
                    await cmd.Connection.OpenAsync();

                    await cmd.ExecuteNonQueryAsync();

                    var ms = ((OracleClob)cmd.Parameters["nRETURN"].Value).Value.ToString();

                    if (ms == "**DIVERG-DT-KARDEX**")
                        return false;
                }
                catch (Exception)
                {
                    return false;
                }
                finally
                {
                    cmd.Connection.Close();
                }
            }

            return true;
        }

        public async Task<bool> CreateProdTransferenciaAsync(ESCreateProdTransferencia pCreateTransf)
        {
            using (var cmd = DbConn.Database.Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PCKG_ESTOQUE.GERARTRANFESTOQUE";

                cmd.Parameters.AddRange(new[]
                {
                    new OracleParameter("nRETURN", OracleDbType.Int32, ParameterDirection.ReturnValue),
                    new OracleParameter("p1", OracleDbType.Int32, pCreateTransf.CodEstab, ParameterDirection.Input),
                    new OracleParameter("p2", OracleDbType.Varchar2, pCreateTransf.CodProduto, ParameterDirection.Input),
                    new OracleParameter("p3", OracleDbType.Varchar2, pCreateTransf.CodEstoqueOri, ParameterDirection.Input),
                    new OracleParameter("p4", OracleDbType.Varchar2, pCreateTransf.CodEstoqueDest, ParameterDirection.Input),
                    new OracleParameter("p4", OracleDbType.Varchar2, pCreateTransf.CodUsuario, ParameterDirection.Input),
                    new OracleParameter("p4", OracleDbType.Decimal, pCreateTransf.Quantidade, ParameterDirection.Input),
                    new OracleParameter("p5", OracleDbType.Varchar2, "TRANSF. DE ESTOQUE A PARTIR DO MOBILE", ParameterDirection.Input),
                    new OracleParameter("pMSERRO", OracleDbType.Clob, ParameterDirection.Output),
                    new OracleParameter("p6", OracleDbType.Int64, pCreateTransf.Lote, ParameterDirection.Input)
                });
                IDbTransaction transaction = null;
                try
                {
                    await cmd.Connection.OpenAsync();

                    transaction = cmd.Connection.BeginTransaction();
                    await cmd.ExecuteNonQueryAsync();
                    var nret = cmd.Parameters["nRETURN"].Value.ToString();
                    var ms = ((OracleClob)cmd.Parameters["pMSERRO"].Value).Value.ToString();

                    if (ms != "*OK*")
                    {
                        throw new Exception(
                            $@"{ms.Substring(0, 1).ToUpper()}{ms.Substring(1).ToLower()}");
                    }
                    transaction.Commit();
                }
                catch (Exception e)
                {
                    transaction?.Rollback();
                    throw new Exception(e.Message);
                }
                finally
                {
                    cmd.Connection.Close();
                }
            }

            return true;
        }

        public async Task<EPermisEstoque> ValidaPermisMovEstoqueAsync(int pCodEstab, string pCodUsuario, DateTime pData, string pCodEstoque)
        {
            var retorno = EPermisEstoque.Sucesso;

            using (var cmd = DbConn.Database.Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PCKG_ESTOQUE.VALIDAPERMISMOVESTOQUE";

                cmd.Parameters.AddRange(new[]
                {
                    new OracleParameter("nRETURN", OracleDbType.Int32, ParameterDirection.ReturnValue),
                    new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input),
                    new OracleParameter("pCODUSUARIO", OracleDbType.Varchar2, pCodUsuario, ParameterDirection.Input),
                    new OracleParameter("pDATA", OracleDbType.Date, pData, ParameterDirection.Input),
                    new OracleParameter("pCODESTOQUE", OracleDbType.Varchar2, pCodEstoque, ParameterDirection.Input)
                });

                try
                {
                    await cmd.Connection.OpenAsync();

                    await cmd.ExecuteNonQueryAsync();
                    var nret = cmd.Parameters["nRETURN"].Value.ToString();
                    if (nret == "0")
                        retorno = EPermisEstoque.Sucesso;
                    else if (nret == "1")
                        retorno = EPermisEstoque.SemPermissao;
                    else if (nret == "2")
                        retorno = EPermisEstoque.DataInvalida;
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
                finally
                {
                    cmd.Connection.Close();
                }
            }

            return retorno;
        }

        public async Task<bool> GeraMovimentoRequisicaoAsync(
            short pCodEstab, long pCodRequisicao, string pCodUsuario, 
            long pLote, decimal pQuantidade)
        {
            var retorno = false;

            using (var cmd = DbConn.Database.Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PCKG_ESTOQUE.GERAMOVIMENTOREQUISICAO";

                cmd.Parameters.AddRange(new[]
                {
                    new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input),
                    new OracleParameter("pCODREQUISICAO", OracleDbType.Int64, pCodRequisicao, ParameterDirection.Input),
                    new OracleParameter("pCODUSUARIO", OracleDbType.Varchar2, pCodUsuario, ParameterDirection.Input),
                    new OracleParameter("pLOTE", OracleDbType.Int64, pLote, ParameterDirection.Input),
                    new OracleParameter("pQUANTIDADE", OracleDbType.Decimal, pQuantidade, ParameterDirection.Input)
                });

                try
                {
                    await cmd.Connection.OpenAsync();

                    await cmd.ExecuteNonQueryAsync();
                    retorno = true;
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
                finally
                {
                    cmd.Connection.Close();
                }
            }

            return retorno;
        }
    }
}
