using Hino.Estoque.Domain.Estoque.Interfaces.Repositories.Estoque;
using Hino.Estoque.Infra.Cross.Entities.Estoque;
using Hino.Estoque.Infra.Data.DataBase.Context;
using Hino.Estoque.Infra.Data.DataBase.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Data.Entity;

namespace Hino.Estoque.Infra.DataBase.Repositories.Estoque
{
    public class ESInventDetRepository : BaseRepository<ESInventDet>, IESInventDetRepository
    {
        public ESInventDetRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }

        public async Task<IEnumerable<ESInventDet>> GetItensInventariosPendentes(short pCodEstab) =>
            await AddQueryProperties(DbEntity,
                             e => e.FSProduto,
                             e => e.ESInventario)
            .Include("fsproduto.fsprodutoparamestab")
            .Include("fsproduto.fsprodutoparamestab.esprodenderec")
            .Include("fsproduto.fsprodutoparamestab.esprodenderec.esenderecamento")
            .Where(r => r.codestab == pCodEstab && r.ESInventario.status == "P")
            .ToListAsync();

        public async Task<IEnumerable<ESInventDet>> GetItensInventario(short pCodEstab, decimal pCodInv, decimal pContagem) =>
            await AddQueryProperties(DbEntity,
                             e => e.FSProduto,
                             e => e.ESInventario)
            .Include("fsproduto.fsprodutoparamestab")
            .Include("fsproduto.fsprodutopcp")
            .Include("fsproduto.fsprodutoparamestab.esprodenderec")
            .Include("fsproduto.fsprodutoparamestab.esprodenderec.esenderecamento")
            .Where(r => r.codestab == pCodEstab && r.codinv == pCodInv && r.contagem == pContagem)
            .ToListAsync();

        public async Task<ESInventDet> GetById(short pCodEstab, decimal pCodInv, decimal pContagem, string pCodProduto, string pCodEstoque) =>
            (await QueryAsync(r => r.codestab == pCodEstab && 
                              r.codinv == pCodInv && 
                              r.contagem == pContagem &&
                              r.codestoque == pCodEstoque &&
                              r.codproduto == pCodProduto
            )).FirstOrDefault();
    }
}
