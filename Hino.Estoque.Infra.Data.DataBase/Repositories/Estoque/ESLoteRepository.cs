using Hino.Estoque.Domain.Estoque.Interfaces.Repositories.Estoque;
using Hino.Estoque.Infra.Cross.Entities.Estoque;
using Hino.Estoque.Infra.Data.DataBase.Context;
using Hino.Estoque.Infra.Data.DataBase.Repositories;
using System.Collections;

namespace Hino.Estoque.Infra.DataBase.Repositories.Estoque
{
    public class ESLoteRepository : BaseRepository<ESLote>, IESLoteRepository
    {
        public ESLoteRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
