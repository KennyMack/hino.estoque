﻿using Hino.Estoque.Domain.Estoque.Interfaces.Repositories.Estoque;
using Hino.Estoque.Infra.Cross.Entities.Estoque;
using Hino.Estoque.Infra.Data.DataBase.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Infra.Data.DataBase.Repositories.Estoque
{
    public class ESRequisicaoRepository : BaseRepository<ESRequisicao>, IESRequisicaoRepository
    {
        public ESRequisicaoRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
