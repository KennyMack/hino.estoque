using Hino.Estoque.Domain.Estoque.Interfaces.Repositories.Estoque;
using Hino.Estoque.Infra.Cross.Entities.Estoque;
using Hino.Estoque.Infra.Data.DataBase.Context;
using Hino.Estoque.Infra.Data.DataBase.Repositories;

namespace Hino.Estoque.Infra.DataBase.Repositories.Estoque
{
    public class ESTransfLoteRepository : BaseRepository<ESTransfLote>, IESTransfLoteRepository
    {
        public ESTransfLoteRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
