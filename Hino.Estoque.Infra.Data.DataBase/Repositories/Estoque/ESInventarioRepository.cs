using Hino.Estoque.Domain.Estoque.Interfaces.Repositories.Estoque;
using Hino.Estoque.Infra.Cross.Entities.Estoque;
using Hino.Estoque.Infra.Data.DataBase.Context;
using Hino.Estoque.Infra.Data.DataBase.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Data.Entity;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using System;
using Hino.Estoque.Infra.Cross.Utils;
using Oracle.ManagedDataAccess.Types;
using System.Configuration;

namespace Hino.Estoque.Infra.DataBase.Repositories.Estoque
{
    public class ESInventarioRepository : BaseRepository<ESInventario>, IESInventarioRepository
    {
        public ESInventarioRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }

        public int GetMaxInventCount(short pCodEstab, decimal pCodInv)
        {
            var invItems = DbConn.ESInventDet.Where(r => r.codestab == pCodEstab && r.codinv == pCodInv)
                .AsNoTracking()
                .OrderByDescending(r => r.contagem)
                .FirstOrDefault();

            return Convert.ToInt32(invItems.contagem);
        }

        public async Task<ESInventario> GetInventarioAsync(short pCodEstab, decimal pCodInv, decimal pContagem) =>
            await DbConn.ESInventario.Where(r => r.codestab == pCodEstab && r.codinv == pCodInv).FirstOrDefaultAsync();

        public async Task<IEnumerable<ESInventario>> GetInventariosPendentes(short pCodEstab)
        {
            var inventarios = await DbConn.ESInventario.Where(r => r.codestab == pCodEstab && r.status == "P").AsNoTracking().ToListAsync();
            var invItems = await DbConn.ESInventDet.Where(r => r.codestab == pCodEstab && r.ESInventario.status == "P")
                .GroupBy(r => new 
                {
                    contagem = r.contagem,
                    codinv = r.codinv,
                    codestab = r.codestab
                })
                .Select(r => new
                {
                    codestab = r.Key.codestab,
                    codinv = r.Key.codinv,
                    contagem = r.Key.contagem
                })
                .AsNoTracking()
                .ToListAsync();

            var OnlyLastList = Convert.ToBoolean(ConfigurationManager.AppSettings.Get("OnlyLastList") ?? "false");

            if (OnlyLastList)
            {
                var query = inventarios
                    .Where(r => invItems.Select(t => t.codinv).Contains(r.codinv)).Select(r => new ESInventario
                {
                    codestab = r.codestab,
                    codinv = r.codinv,
                    status = r.status,
                    datainv = r.datainv,
                    contagem = invItems.Where(x => x.codinv == r.codinv && x.codestab == r.codestab).Max(x => x.contagem),
                    qtdecontag = r.qtdecontag
                });
                return query.OrderByDescending(r => r.codinv)
                    .ThenBy(r => r.contagem);
            }
            else
            {
                var query = from itemsgrp in
                                from items in invItems
                                group items by new { items.codestab, items.codinv, items.contagem } into grp
                                select grp.First()
                            join cabec in inventarios
                            on
                            new
                            {
                                itemsgrp.codestab,
                                itemsgrp.codinv
                            }
                            equals
                            new
                            {
                                cabec.codestab,
                                cabec.codinv
                            }
                            select new ESInventario
                            {
                                codestab = cabec.codestab,
                                codinv = cabec.codinv,
                                status = cabec.status,
                                datainv = cabec.datainv,
                                contagem = itemsgrp.contagem,
                                qtdecontag = cabec.qtdecontag
                            };
                return query.OrderByDescending(r => r.codinv)
                    .ThenBy(r => r.contagem);
            }
        }

        public async Task<string> CreateItemInventarioAsync(ESCreateItemInventario pCreateItemInventario)
        {
            using (var cmd = DbConn.Database.Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PCKG_ESTOQUE.INCLUIITEMLISTAINV";

                cmd.Parameters.AddRange(new[]
                {
                    new OracleParameter("nRETURN", OracleDbType.Clob, ParameterDirection.ReturnValue),
                    new OracleParameter("p1", OracleDbType.Int32, pCreateItemInventario.codestab, ParameterDirection.Input),
                    new OracleParameter("p3", OracleDbType.Int64, pCreateItemInventario.codinv, ParameterDirection.Input),
                    new OracleParameter("p2", OracleDbType.Varchar2, pCreateItemInventario.codproduto?.Trim(), ParameterDirection.Input),
                    new OracleParameter("p4", OracleDbType.Varchar2, pCreateItemInventario.codestoque?.Trim(), ParameterDirection.Input)
                });

                IDbTransaction transaction = null;
                try
                {
                    await cmd.Connection.OpenAsync();

                    transaction = cmd.Connection.BeginTransaction();
                    await cmd.ExecuteNonQueryAsync();
                    var nret = ((OracleClob)cmd.Parameters["nRETURN"].Value).Value.ToString();

                    transaction.Commit();
                    return nret;
                }
                catch (Exception e)
                {
                    transaction?.Rollback();
                    throw new Exception(e.Message);
                }
                finally
                {
                    cmd.Connection.Close();
                }
            }
        }
    }
}
