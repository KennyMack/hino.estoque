﻿using Hino.Estoque.Domain.Base.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Manutencao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Domain.Manutencao.Interfaces.Services
{
    public interface IMNSolicManutService : IBaseService<MNSolicManut>
    {
        Task<string> BuscaAprovadorPadraoAsync(int pCodEstab);
        Task<MNSolicManut> AlterarStatusSolicManutAsync(short pCodEstab, long pCodSolic, int pStatus, string pCodUsuario);
    }
}
