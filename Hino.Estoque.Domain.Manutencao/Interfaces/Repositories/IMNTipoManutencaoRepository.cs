﻿using System;
using Hino.Estoque.Domain.Base.Interfaces.Repositories;
using Hino.Estoque.Infra.Cross.Entities.Manutencao;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Domain.Manutencao.Interfaces.Repositories
{
    public interface IMNTipoManutencaoRepository : IBaseRepository<MNTipoManut>
    {
    }
}
