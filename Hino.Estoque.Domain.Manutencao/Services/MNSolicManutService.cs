﻿using Hino.Estoque.Domain.Base.Services;
using Hino.Estoque.Domain.Manutencao.Interfaces.Repositories;
using Hino.Estoque.Domain.Manutencao.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Manutencao;
using Hino.Estoque.Infra.Cross.Resources;
using Hino.Estoque.Infra.Cross.Utils;
using Hino.Estoque.Infra.Cross.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Domain.Manutencao.Services
{
    public class MNSolicManutService : BaseService<MNSolicManut>, IMNSolicManutService
    {
        private readonly IMNSolicManutRepository _IMNSolicManutRepository;

        public MNSolicManutService(IMNSolicManutRepository pMNSolicManutRepository) :
             base(pMNSolicManutRepository)
        {
            _IMNSolicManutRepository = pMNSolicManutRepository;
        }

        public async Task<MNSolicManut> AlterarStatusSolicManutAsync(
            short pCodEstab, long pCodSolic, int pStatus, string pCodUsuario)
        {
            var SolicManut = (await _IMNSolicManutRepository.QueryAsync(r =>
                 r.codestab == pCodEstab &&
                 r.codsolic == pCodSolic,
                 s => s.Equipamento,
                 s => s.TipoManut,
                 r => r.Motivo)).FirstOrDefault();

            if (SolicManut == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "CodSolic",
                    Value = pCodSolic.ToString(),
                    Messages = new string[] { MessagesResource.SolicManutNotFound }
                });
                return null;
            }

            if (SolicManut.status != "P")
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "CodSolic",
                    Value = pCodSolic.ToString(),
                    Messages = new string[] { MessagesResource.SolicManutStatusInvalidOperation }
                });
                return null;
            }

            if (SolicManut.aprovador != pCodUsuario)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "CodSolic",
                    Value = pCodSolic.ToString(),
                    Messages = new string[] { MessagesResource.SolicManutUserNotAllowed }
                });
                return null;
            }

            try
            {
                await _IMNSolicManutRepository.AtualizaSolicitacaoAsync(pCodEstab, pCodSolic, pStatus == 1 ? "A" : "R");
            }
            catch (Exception ex)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "CodSolic",
                    Value = pCodSolic.ToString(),
                    Messages = new string[] { ex.Message }
                });
                return null;
            }

            return (await _IMNSolicManutRepository.QueryAsync(r =>
                 r.codestab == pCodEstab &&
                 r.codsolic == pCodSolic,
                 s => s.Equipamento,
                 s => s.TipoManut,
                 r => r.Motivo)).FirstOrDefault();
        }

        public async Task<string> BuscaAprovadorPadraoAsync(int pCodEstab)
        {
            try
            {
                var ret = await _IMNSolicManutRepository.BuscaAprovadorPadraoAsync(pCodEstab);
                if (ret.IsEmpty())
                    throw new Exception("Usuário aprovador não informado (Manutenção|Parâmetros).");

                return ret;
            }
            catch (Exception ex)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "CodUsuário",
                    Value = "",
                    Messages = new string[] { ex.Message }
                });
            }

            return "";
        }
    }
}
