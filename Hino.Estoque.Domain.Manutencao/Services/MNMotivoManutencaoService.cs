﻿using System;
using Hino.Estoque.Domain.Base.Services;
using Hino.Estoque.Domain.Manutencao.Interfaces.Repositories;
using Hino.Estoque.Domain.Manutencao.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Manutencao;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Domain.Manutencao.Services
{
    public class MNMotivoManutencaoService : BaseService<MNMotivo>, IMNMotivoManutencaoService
    {
        private readonly IMNMotivoManutencaoRepository _IMNMotivoManutencaoRepository;

        public MNMotivoManutencaoService(IMNMotivoManutencaoRepository pMNMotivoManutencaoRepository) :
             base(pMNMotivoManutencaoRepository)
        {
            _IMNMotivoManutencaoRepository = pMNMotivoManutencaoRepository;
        }
    }
}
