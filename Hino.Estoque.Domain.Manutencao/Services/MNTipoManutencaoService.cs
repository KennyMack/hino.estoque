﻿using System;
using Hino.Estoque.Domain.Base.Services;
using Hino.Estoque.Domain.Manutencao.Interfaces.Repositories;
using Hino.Estoque.Domain.Manutencao.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Manutencao;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Domain.Manutencao.Services
{
    public class MNTipoManutencaoService : BaseService<MNTipoManut>, IMNTipoManutencaoService
    {
        private readonly IMNTipoManutencaoRepository _IMNTipoManutencaoRepository;

        public MNTipoManutencaoService(IMNTipoManutencaoRepository pMNTipoManutencaoRepository) :
             base(pMNTipoManutencaoRepository)
        {
            _IMNTipoManutencaoRepository = pMNTipoManutencaoRepository;
        }
    }
}
