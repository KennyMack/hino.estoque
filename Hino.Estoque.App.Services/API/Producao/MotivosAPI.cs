﻿using Hino.Estoque.App.Models;
using Hino.Estoque.App.Models.Producao;
using Hino.Estoque.App.Services.API.Interfaces.Producao;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.API.Producao
{
    public class MotivosAPI : BaseAPI<PDMotivosModel>, IMotivosAPI
    {
        public async Task<IEnumerable<PDMotivosModel>> GetMotivosRefugoAsync()
        {
            var result =
               await _Request.GetAsync(
                   $"{EndPoint.Replace("{pCodEstab}", CurrentApp.CODESTAB.ToString())}/Por/Tipo/1", true);

            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<IEnumerable<PDMotivosModel>>(this._JsonSerializer);
        }

        public async Task<IEnumerable<PDMotivosModel>> GetMotivosParadaAsync()
        {
            var result =
               await _Request.GetAsync(
                   $"{EndPoint.Replace("{pCodEstab}", CurrentApp.CODESTAB.ToString())}/Por/Tipo/0", true);

            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<IEnumerable<PDMotivosModel>>(this._JsonSerializer);
        }

        public async Task<IEnumerable<PDMotivosModel>> GetMotivosParadaProgAsync()
        {
            var result =
               await _Request.GetAsync(
                   $"{EndPoint.Replace("{pCodEstab}", CurrentApp.CODESTAB.ToString())}/Por/Tipo/3", true);

            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<IEnumerable<PDMotivosModel>>(this._JsonSerializer);
        }
    }
}
