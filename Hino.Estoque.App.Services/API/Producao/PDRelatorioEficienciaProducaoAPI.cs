﻿using Hino.Estoque.App.Models;
using Hino.Estoque.App.Models.Producao;
using Hino.Estoque.App.Services.API.Interfaces.Producao;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.API.Producao
{
    public class PDRelatorioEficienciaProducaoAPI : BaseAPI<PDRelatorioEficienciaProducaoModel>, IPDRelatorioEficienciaProducaoAPI
    {
        public async Task<IEnumerable<PDRelatorioEficienciaProducaoModel>> PostRelatorioEficienciaAsync(DateTime pDataInicio, DateTime pDataTermino)
        {
            var content = new StringContent(
                    JsonConvert.SerializeObject(new
                    {
                        CodEstab = CurrentApp.CODESTAB,
                        DataInicio = pDataInicio,
                        DataTermino = pDataTermino
                    }),
                    Encoding.UTF8, "application/json");

            var result =
               await _Request.PostAsync(
                   $"{EndPoint.Replace("{pCodEstab}", CurrentApp.CODESTAB.ToString())}/Relatorio/Eficiencia", true, content);

            var data = result.ToObject<DefaultResultModel>(_JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<IEnumerable<PDRelatorioEficienciaProducaoModel>>(_JsonSerializer);
        }
    }
}
