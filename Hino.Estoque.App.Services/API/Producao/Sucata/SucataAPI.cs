﻿using Hino.Estoque.App.Models;
using Hino.Estoque.App.Models.Producao.Apontamento;
using Hino.Estoque.App.Services.API.Interfaces.Producao.Sucata;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.API.Producao.Sucata
{
    public class SucataAPI : BaseAPI<PDSucataModel>, ISucataAPI
    {
        public async Task<PDSucataModel> PostApontarSucataAsync(PDSucataCreateVM pModel)
        {
            var content = new StringContent(
                  JsonConvert.SerializeObject(pModel),
                  Encoding.UTF8, "application/json");

            var result =
               await _Request.PostAsync(
                   $"{EndPoint.Replace("{pCodEstab}", CurrentApp.CODESTAB.ToString())}/Apontar/Sucata", true, content);

            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<PDSucataModel>(this._JsonSerializer);
        }
    }
}
