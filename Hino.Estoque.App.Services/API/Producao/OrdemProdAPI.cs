﻿using Hino.Estoque.App.Models;
using Hino.Estoque.App.Models.Producao;
using Hino.Estoque.App.Models.Producao.Apontamento;
using Hino.Estoque.App.Services.API.Interfaces.Producao;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.API.Producao
{
    public class OrdemProdAPI : BaseAPI<PDOrdemProdModel>, IOrdemProdAPI
    {
        public async Task<PDOrdemProdModel> GetOPByBarCodeAsync(string pBarCode)
        {
            var content = new StringContent(
                JsonConvert.SerializeObject(""),
                Encoding.UTF8, "application/json");

            var result =
               await _Request.PostAsync(
                   $"{EndPoint.Replace("{pCodEstab}", CurrentApp.CODESTAB.ToString())}/Barras/{pBarCode}", true, content);

            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<PDOrdemProdModel>(this._JsonSerializer);
        }

        public async Task<IEnumerable<PDOrdemProdCompModel>> GetProductsToSucAsync(PDOrdemProdModel pModel)
        {
            var content = new StringContent(
                JsonConvert.SerializeObject(new
                {
                    codordprod = pModel.codordprod,
                    nivelordprod = pModel.nivelordprod
                }),
                Encoding.UTF8, "application/json");

            var result =
               await _Request.PostAsync(
                   $"{EndPoint.Replace("{pCodEstab}", CurrentApp.CODESTAB.ToString())}/Componentes/Sucata", true, content);

            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<IEnumerable<PDOrdemProdCompModel>>(this._JsonSerializer);
        }
    }
}
