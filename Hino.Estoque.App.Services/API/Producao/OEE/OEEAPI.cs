﻿using Hino.Estoque.App.Models;
using Hino.Estoque.App.Models.Engenharia;
using Hino.Estoque.App.Models.Producao.OEE;
using Hino.Estoque.App.Services.API.Interfaces.Producao.OEE;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.API.Producao.OEE
{
    public class OEEAPI : BaseAPI<PDOEEModel>, IOEEAPI
    {
        public async Task<ENSearchOEEModel> SearchOEEAsync(int pCodEstab)
        {
            var result =
               await _Request.GetAsync(
                   $"{EndPoint.Replace("{pCodEstab}", pCodEstab.ToString())}/Search", true);

            var data = result.ToObject<DefaultResultModel>(_JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<ENSearchOEEModel>(_JsonSerializer);
        }

        public async Task<bool> GerarOEEMensalAsync(int pCodEstab, PDGerarOEEModel pGerar)
        {
            var content = new StringContent(
                       JsonConvert.SerializeObject(pGerar),
                       Encoding.UTF8, "application/json");

            var result =
               await _Request.PostAsync(
                   $"{EndPoint.Replace("{pCodEstab}", CurrentApp.CODESTAB.ToString())}/Gerar/Mensal", true, content);

            var data = result.ToObject<DefaultResultModel>(_JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return false;
            }

            return Convert.ToBoolean(data.data["result"]);
        }

        public async Task<bool> GerarOEEAsync(int pCodEstab, PDGerarOEEModel pGerar)
        {
            var content = new StringContent(
                       JsonConvert.SerializeObject(pGerar),
                       Encoding.UTF8, "application/json");

            var result =
               await _Request.PostAsync(
                   $"{EndPoint.Replace("{pCodEstab}", CurrentApp.CODESTAB.ToString())}/Gerar", true, content);

            var data = result.ToObject<DefaultResultModel>(_JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return false;
            }

            return Convert.ToBoolean(data.data["result"]);
        }

        public async Task<PDOEEResultModel> ListarOEEAsync(int pCodEstab, string pCodUsuario, DateTime pDtInicio, string pCodMaquina)
        {
            var content = new StringContent(
                       JsonConvert.SerializeObject(
                           new
                           {
                               codusuario = pCodUsuario,
                               dataini = pDtInicio,
                               codmaquina = pCodMaquina
                           }
                       ),
                       Encoding.UTF8, "application/json");

            var result =
               await _Request.PostAsync(
                   $"{EndPoint.Replace("{pCodEstab}", pCodEstab.ToString())}/Listar", true, content);

            var data = result.ToObject<DefaultResultModel>(_JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<PDOEEResultModel>(_JsonSerializer);
        }

        public async Task<PDOEEMensalResultModel> ListarOEEMensalAsync(int pCodEstab, string pCodUsuario, DateTime pDtInicio)
        {
            var content = new StringContent(
                       JsonConvert.SerializeObject(
                           new
                           {
                               codusuario = pCodUsuario,
                               dataini = pDtInicio
                           }
                       ),
                       Encoding.UTF8, "application/json");

            var result =
               await _Request.PostAsync(
                   $"{EndPoint.Replace("{pCodEstab}", pCodEstab.ToString())}/Listar/Mensal", true, content);

            var data = result.ToObject<DefaultResultModel>(_JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<PDOEEMensalResultModel>(_JsonSerializer);
        }

    }
}
