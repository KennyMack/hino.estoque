﻿using Hino.Estoque.App.Models;
using Hino.Estoque.App.Models.Producao.Apontamento;
using Hino.Estoque.App.Services.API.Interfaces.Producao.Apontamento;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.API.Producao.Apontamento
{
    public class AptTerminoProdAPI : BaseAPI<PDAptTerminoProdModel>, IAptTerminoProdAPI
    {
        public async Task<PDAptTerminoProdModel> FinalizarProducaoAsync(PDAptTerminoProdModel pModel)
        {
            var content = new StringContent(
                JsonConvert.SerializeObject(pModel),
                Encoding.UTF8, "application/json");

            var result =
               await _Request.PostAsync(
                   $"{EndPoint.Replace("{pCodEstab}", CurrentApp.CODESTAB.ToString())}/Terminar/Producao", true, content);

            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<PDAptTerminoProdModel>(this._JsonSerializer);
        }

        public async Task<PDAptTerminoProdModel> FinalizarTurnoProducaoAsync(PDAptTerminoProdModel pModel)
        {
            var content = new StringContent(
                JsonConvert.SerializeObject(pModel),
                Encoding.UTF8, "application/json");

            var result =
               await _Request.PostAsync(
                   $"{EndPoint.Replace("{pCodEstab}", CurrentApp.CODESTAB.ToString())}/Terminar/Turno", true, content);

            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<PDAptTerminoProdModel>(this._JsonSerializer);
        }

        public async Task<PDAptTerminoProdModel> FinalizarParadaAsync(PDAptTerminoProdModel pModel)
        {
            var content = new StringContent(
                JsonConvert.SerializeObject(pModel),
                Encoding.UTF8, "application/json");

            var result =
               await _Request.PostAsync(
                   $"{EndPoint.Replace("{pCodEstab}", CurrentApp.CODESTAB.ToString())}/Terminar/Parada", true, content);

            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<PDAptTerminoProdModel>(this._JsonSerializer);
        }
    }
}
