﻿using Hino.Estoque.App.Models;
using Hino.Estoque.App.Models.Engenharia;
using Hino.Estoque.App.Models.Producao.Apontamento;
using Hino.Estoque.App.Services.API.Interfaces.Producao.Apontamento;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.API.Producao.Apontamento
{
    public class AptInicioProdAPI : BaseAPI<PDAptInicioProdModel>, IAptInicioProdAPI
    {
        public async Task<PDAptInicioProdModel> IniciarProducaoAsync(PDAptInicioProdModel pModel)
        {
            var content = new StringContent(
                JsonConvert.SerializeObject(pModel),
                Encoding.UTF8, "application/json");

            var result =
               await _Request.PostAsync(
                   $"{EndPoint.Replace("{pCodEstab}", CurrentApp.CODESTAB.ToString())}/Iniciar/Producao", true, content);

            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<PDAptInicioProdModel>(this._JsonSerializer);
        }

        public async Task<PDAptInicioProdModel> IniciarParadaAsync(PDAptInicioProdModel pModel)
        {
            var content = new StringContent(
                JsonConvert.SerializeObject(pModel),
                Encoding.UTF8, "application/json");

            var result =
               await _Request.PostAsync(
                   $"{EndPoint.Replace("{pCodEstab}", CurrentApp.CODESTAB.ToString())}/Iniciar/Parada", true, content);

            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<PDAptInicioProdModel>(this._JsonSerializer);
        }

        public async Task<IEnumerable<PDAptLancamentosModel>> GetListApontamentosAsync()
        {
            var result =
               await _Request.GetAsync(
                   $"{EndPoint.Replace("{pCodEstab}", CurrentApp.CODESTAB.ToString())}/Apontamentos/{CurrentApp.UserLogged.GEFuncionarios.codfuncionario}", true);

            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<IEnumerable<PDAptLancamentosModel>>(this._JsonSerializer);
        }

        public async Task<PDAptInicioProdModel> GetApontamentoInicioAsync(PDAptInicioProdModel pModel)
        {
            var content = new StringContent(
                JsonConvert.SerializeObject(pModel),
                Encoding.UTF8, "application/json");

            var result =
               await _Request.PostAsync(
                   $"{EndPoint.Replace("{pCodEstab}", CurrentApp.CODESTAB.ToString())}/Buscar/Inicio/{CurrentApp.UserLogged.GEFuncionarios.codfuncionario}", true, content);

            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<PDAptInicioProdModel>(this._JsonSerializer);
        }

        public async Task<IEnumerable<ENMaquinasModel>> GetMachinesAsync()
        {
            var result =
               await _Request.GetAsync(
                   $"{EndPoint.Replace("{pCodEstab}", CurrentApp.CODESTAB.ToString())}/Maquinas", true);

            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<IEnumerable<ENMaquinasModel>>(this._JsonSerializer);
        }
    }
}
