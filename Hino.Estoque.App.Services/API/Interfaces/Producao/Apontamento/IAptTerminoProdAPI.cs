﻿using Hino.Estoque.App.Models.Producao.Apontamento;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.API.Interfaces.Producao.Apontamento
{
    public interface IAptTerminoProdAPI : IBaseAPI<PDAptTerminoProdModel>
    {
        Task<PDAptTerminoProdModel> FinalizarProducaoAsync(PDAptTerminoProdModel pModel);
        Task<PDAptTerminoProdModel> FinalizarTurnoProducaoAsync(PDAptTerminoProdModel pModel);
        Task<PDAptTerminoProdModel> FinalizarParadaAsync(PDAptTerminoProdModel pModel);
    }
}
