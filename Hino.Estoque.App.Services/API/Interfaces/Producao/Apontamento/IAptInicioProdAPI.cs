﻿using Hino.Estoque.App.Models.Engenharia;
using Hino.Estoque.App.Models.Producao.Apontamento;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.API.Interfaces.Producao.Apontamento
{
    public interface IAptInicioProdAPI : IBaseAPI<PDAptInicioProdModel>
    {
        Task<IEnumerable<ENMaquinasModel>> GetMachinesAsync();
        Task<PDAptInicioProdModel> IniciarProducaoAsync(PDAptInicioProdModel pModel);
        Task<PDAptInicioProdModel> IniciarParadaAsync(PDAptInicioProdModel pModel);
        Task<PDAptInicioProdModel> GetApontamentoInicioAsync(PDAptInicioProdModel pModel);
        Task<IEnumerable<PDAptLancamentosModel>> GetListApontamentosAsync();
    }
}
