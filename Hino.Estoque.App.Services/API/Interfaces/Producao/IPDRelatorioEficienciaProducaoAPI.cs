﻿using Hino.Estoque.App.Models.Producao;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.API.Interfaces.Producao
{
    public interface IPDRelatorioEficienciaProducaoAPI : IBaseAPI<PDRelatorioEficienciaProducaoModel>
    {
        Task<IEnumerable<PDRelatorioEficienciaProducaoModel>> PostRelatorioEficienciaAsync(DateTime pDataInicio, DateTime pDataTermino);
    }
}
