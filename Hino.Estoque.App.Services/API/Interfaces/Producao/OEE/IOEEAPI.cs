﻿using Hino.Estoque.App.Models.Engenharia;
using Hino.Estoque.App.Models.Producao.OEE;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.API.Interfaces.Producao.OEE
{
    public interface IOEEAPI : IBaseAPI<PDOEEModel>
    {
        Task<ENSearchOEEModel> SearchOEEAsync(int pCodEstab);
        Task<bool> GerarOEEAsync(int pCodEstab, PDGerarOEEModel pGerar);
        Task<bool> GerarOEEMensalAsync(int pCodEstab, PDGerarOEEModel pGerar);
        Task<PDOEEResultModel> ListarOEEAsync(int pCodEstab, string pCodUsuario, DateTime pDtInicio, string pCodMaquina);
        Task<PDOEEMensalResultModel> ListarOEEMensalAsync(int pCodEstab, string pCodUsuario, DateTime pDtInicio);
    }
}
