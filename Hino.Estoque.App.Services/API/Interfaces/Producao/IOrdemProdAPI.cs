﻿using Hino.Estoque.App.Models.Producao;
using Hino.Estoque.App.Models.Producao.Apontamento;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.API.Interfaces.Producao
{
    public interface IOrdemProdAPI : IBaseAPI<PDOrdemProdModel>
    {
        Task<PDOrdemProdModel> GetOPByBarCodeAsync(string pBarCode);
        Task<IEnumerable<PDOrdemProdCompModel>> GetProductsToSucAsync(PDOrdemProdModel pModel);
    }
}
