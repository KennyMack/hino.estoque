﻿using Hino.Estoque.App.Models.Producao;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.API.Interfaces.Producao
{
    public interface IMotivosAPI : IBaseAPI<PDMotivosModel>
    {
        Task<IEnumerable<PDMotivosModel>> GetMotivosRefugoAsync();
        Task<IEnumerable<PDMotivosModel>> GetMotivosParadaAsync();
        Task<IEnumerable<PDMotivosModel>> GetMotivosParadaProgAsync();
    }
}
