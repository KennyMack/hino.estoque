﻿using Hino.Estoque.App.Models.Producao.Apontamento;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.API.Interfaces.Producao.Sucata
{
    public interface ISucataAPI : IBaseAPI<PDSucataModel>
    {
        Task<PDSucataModel> PostApontarSucataAsync(PDSucataCreateVM pModel);
    }
}
