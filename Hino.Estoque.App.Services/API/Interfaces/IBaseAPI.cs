﻿using Hino.Estoque.App.Models;
using Hino.Estoque.App.Utils.Exceptions;
using Hino.Estoque.App.Utils.Paging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.API.Interfaces
{
    public interface IBaseAPI<T> where T : BaseEntity
    {
        List<ModelException> Errors { get; set; }
        string BASEURL { get; }
        Task<IEnumerable<T>> GetAllAsync();
        Task<T> PostAsync(T model);
        Task<T> PutAsync(T model);
        Task<T> DeleteAsync(T model);
        Task<bool> ConectionTest();
    }

}
