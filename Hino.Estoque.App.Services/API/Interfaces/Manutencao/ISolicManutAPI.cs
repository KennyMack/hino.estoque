﻿using Hino.Estoque.App.Models.Manutencao;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.API.Interfaces.Manutencao
{
    public interface ISolicManutAPI : IBaseAPI<MNSolicManutModel>
    {
        Task<IEnumerable<MNSolicManutModel>> BuscaListaSolicManutAsync(MNSolicForUserModel pSolicForUser);
        Task<MNSolicManutModel> PostSolicitarManutencaoAsync(MNCriarSolicManutModel pCriar);
        Task<MNSolicManutModel> PostChangeStatus(MNStatusSolicManutModel pStatus);
        Task<MNSearchManutModel> SearchManutAsync(int pCodEstab);
    }
}
