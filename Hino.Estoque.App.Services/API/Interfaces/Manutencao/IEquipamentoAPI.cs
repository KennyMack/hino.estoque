﻿using System;
using Hino.Estoque.App.Models.Manutencao;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.API.Interfaces.Manutencao
{
    public interface IEquipamentoAPI : IBaseAPI<MNEquipamentoModel>
    {
        Task<IEnumerable<MNEquipamentoModel>> BuscaEquipamentoAsync(MNSearchEquipamentoModel pSearch);
    }
}
