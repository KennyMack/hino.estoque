﻿using System;
using Hino.Estoque.App.Models.Manutencao;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.API.Interfaces.Manutencao
{
    public interface IMotivoManutencaoAPI : IBaseAPI<MNMotivoModel>
    {
        Task<IEnumerable<MNMotivoModel>> BuscaMotivoManutencaoAsync(MNSearchMotivoModel pSearch);
    }
}
