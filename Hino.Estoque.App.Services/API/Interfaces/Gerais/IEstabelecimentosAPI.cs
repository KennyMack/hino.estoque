﻿using Hino.Estoque.App.Models.Gerais;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.API.Interfaces.Gerais
{
    public interface IEstabelecimentosAPI : IBaseAPI<GEEstabModel>
    {
        Task<IEnumerable<GEEstabModel>> GetAllEstabs();
        Task<GESettingsModel> GetSettings();
    }
}
