﻿using Hino.Estoque.App.Models;
using Hino.Estoque.App.Models.Auth;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.API.Interfaces.Auth
{
    public interface IAuthAPI : IBaseAPI<GEUsuariosLoginModel>
    {
        Task<GEUsuariosLogadoModel> LoginAsync(GEUsuariosLoginModel pLogin);
        Task<bool> TestServerAsync();
    }
}
