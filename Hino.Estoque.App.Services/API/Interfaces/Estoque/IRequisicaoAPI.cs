﻿using Hino.Estoque.App.Models.Estoque;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.API.Interfaces.Estoque
{
    public interface IRequisicaoAPI : IBaseAPI<ESRequisicoesModel>
    {
        Task<IEnumerable<ESRequisicoesModel>> GetPendentesAsync();
        Task<ESRequisicoesModel> PostRequisitarAsync(ESCreateRequestModel pCreate);
        Task<ESResultRequisicaoModel> PostReprovarRequisicaoAsync(ESUpdateRequestModel pCreate);
        Task<ESResultRequisicaoModel> PostAprovarRequisicaoAsync(ESUpdateRequestModel pCreate);
    }
}
