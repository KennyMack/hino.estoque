﻿using Hino.Estoque.App.Models.Estoque;
using Hino.Estoque.App.Models.Fiscal;
using Hino.Estoque.App.Models.Producao;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.API.Interfaces.Estoque
{
    public interface ITransferenciaAPI : IBaseAPI<PDOPCreateTransfModel>
    {
        Task<OPTransfResult> PostOPSeparateAsync(PDOPCreateTransfModel[] plstTransf);
        Task<bool> PostTransferirAsync(ESCreateProdTransferenciaModel pCreateTransf);
        Task<FSSaldoEstoqueModel> GetProductSaldoEstoqueAsync(ESBuscaSaldoEstoqueModel pBuscaSaldo);
        Task<bool> PostTransferirEndAsync(ESCreateProdEndTransfModel pCreateTranf);
    }
}
