﻿using Hino.Estoque.App.Models.Estoque;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.API.Interfaces.Estoque
{
    public interface IInventarioAPI : IBaseAPI<ESInventarioModel>
    {
        Task<ESInventDetModel> PostInventarioItemAsync(ESInventDetItemModel pInventItem);
        Task<ESInventarioModel> GetInventarioAsync(short pCodInv, decimal pContagem);
        Task<IEnumerable<ESInventarioModel>> GetPendentesAsync();
        Task<IEnumerable<ESInventDetModel>> GetItensInventarioAsync(short pCodInv, decimal pContagem);
        Task<bool> PostContagemAsync(short pCodInv, decimal pContagem, ESUpdateInventDetModel[] pESUpdateInventDet);
        Task<ESCreateItemInventarioModel> PostCreateItem(ESCreateItemInventarioModel pCreateItem);
    }
}
