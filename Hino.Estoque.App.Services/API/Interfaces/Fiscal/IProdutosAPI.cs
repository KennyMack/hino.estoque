﻿using Hino.Estoque.App.Models.Estoque;
using Hino.Estoque.App.Models.Fiscal;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.API.Interfaces.Fiscal
{
    public interface IProdutosAPI : IBaseAPI<FSProdutoModel>
    {
        Task<FSProdutoModel> GetProductByBarCodeAsync(short pCodEstab, string pBarCode);
        Task<IEnumerable<FSSaldoEstoqueModel>> GetProductInventoryBalancesAsync(ESBuscaSaldoEstoqueModel pProduct);
    }
}
