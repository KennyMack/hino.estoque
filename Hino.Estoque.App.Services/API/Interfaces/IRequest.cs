﻿using Hino.Estoque.App.Utils.Exceptions;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.API.Interfaces
{
    public interface IRequest
    {
        List<ModelException> Errors { get; set; }
        Task<JContainer> GetAsync(string path, bool Token, TimeSpan? timeout = null);
        Task<JContainer> PostAsync(string path, bool Token, HttpContent content);
        Task<JContainer> PutAsync(string path, bool Token, HttpContent content);
        Task<JContainer> DeleteAsync(string path, bool Token);
    }
}
