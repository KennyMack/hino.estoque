﻿using Hino.Estoque.App.Models;
using Hino.Estoque.App.Models.Estoque;
using Hino.Estoque.App.Services.API.Interfaces.Estoque;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.API.Estoque
{
    public class RequisicaoAPI : BaseAPI<ESRequisicoesModel>, IRequisicaoAPI
    {
        public async Task<IEnumerable<ESRequisicoesModel>> GetPendentesAsync()
        {
            var result =
               await _Request.GetAsync(
                   $"{EndPoint.Replace("{pCodEstab}", CurrentApp.CODESTAB.ToString())}/Pendentes", true);

            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<IEnumerable<ESRequisicoesModel>>(_JsonSerializer);
        }

        public async Task<ESResultRequisicaoModel> PostAprovarRequisicaoAsync(ESUpdateRequestModel pCreate)
        {
            var content = new StringContent(
                   JsonConvert.SerializeObject(pCreate),
                   Encoding.UTF8, "application/json");

            var result =
               await _Request.PostAsync(
                   $"{EndPoint.Replace("{pCodEstab}", CurrentApp.CODESTAB.ToString())}/Aprovar", true, content);

            var data = result.ToObject<DefaultResultModel>(_JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<ESResultRequisicaoModel>(_JsonSerializer);
        }

        public async Task<ESResultRequisicaoModel> PostReprovarRequisicaoAsync(ESUpdateRequestModel pCreate)
        {
            var content = new StringContent(
                   JsonConvert.SerializeObject(pCreate),
                   Encoding.UTF8, "application/json");

            var result =
               await _Request.PostAsync(
                   $"{EndPoint.Replace("{pCodEstab}", CurrentApp.CODESTAB.ToString())}/Reprovar", true, content);

            var data = result.ToObject<DefaultResultModel>(_JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<ESResultRequisicaoModel>(_JsonSerializer);
        }

        public async Task<ESRequisicoesModel> PostRequisitarAsync(ESCreateRequestModel pCreate)
        {
            var content = new StringContent(
                   JsonConvert.SerializeObject(pCreate),
                   Encoding.UTF8, "application/json");

            var result =
               await _Request.PostAsync(
                   $"{EndPoint.Replace("{pCodEstab}", CurrentApp.CODESTAB.ToString())}/Novo", true, content);

            var data = result.ToObject<DefaultResultModel>(_JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<ESRequisicoesModel>(_JsonSerializer);
        }
    }
}
