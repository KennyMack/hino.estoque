﻿using Hino.Estoque.App.Models;
using Hino.Estoque.App.Models.Estoque;
using Hino.Estoque.App.Models.Fiscal;
using Hino.Estoque.App.Models.Producao;
using Hino.Estoque.App.Services.API.Interfaces.Estoque;
using Newtonsoft.Json;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.API.Estoque
{
    public class TransferenciaAPI : BaseAPI<PDOPCreateTransfModel>, ITransferenciaAPI
    {
        public async Task<OPTransfResult> PostOPSeparateAsync(PDOPCreateTransfModel[] plstTransf)
        {
            var resRet = new OPTransfResult
            {
                Success = false
            };

            var content = new StringContent(
                JsonConvert.SerializeObject(plstTransf),
                Encoding.UTF8, "application/json");

            var result =
               await _Request.PostAsync(
                   $"{EndPoint.Replace("{pCodEstab}", CurrentApp.CODESTAB.ToString())}/Ordem/Producao/Separar", true, content);

            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return resRet;
            }

            resRet = data.data.ToObject<OPTransfResult>(this._JsonSerializer);

            return resRet;
        }

        public async Task<bool> PostTransferirAsync(ESCreateProdTransferenciaModel pCreateTransf)
        {
            var content = new StringContent(
                JsonConvert.SerializeObject(pCreateTransf),
                Encoding.UTF8, "application/json");

            var result =
               await _Request.PostAsync(
                   $"{EndPoint.Replace("{pCodEstab}", CurrentApp.CODESTAB.ToString())}/Produto/Transferir", true, content);

            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return false;
            }

            return true;
        }

        public async Task<bool> PostTransferirEndAsync(ESCreateProdEndTransfModel pCreateTranf)
        {
            var content = new StringContent(
                JsonConvert.SerializeObject(pCreateTranf),
                Encoding.UTF8, "application/json");

            var result =
               await _Request.PostAsync(
                   $"{EndPoint.Replace("{pCodEstab}", CurrentApp.CODESTAB.ToString())}/Endereco/Transferir", true, content);

            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return false;
            }

            return true;
        }

        public async Task<FSSaldoEstoqueModel> GetProductSaldoEstoqueAsync(ESBuscaSaldoEstoqueModel pBuscaSaldo)
        {
            var content = new StringContent(
                   JsonConvert.SerializeObject(pBuscaSaldo),
                   Encoding.UTF8, "application/json");

            var result =
               await _Request.PostAsync(
                   $"{EndPoint.Replace("{pCodEstab}", CurrentApp.CODESTAB.ToString())}/Produto/Saldo", true, content);

            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<FSSaldoEstoqueModel>(this._JsonSerializer);
        }
    }
}
