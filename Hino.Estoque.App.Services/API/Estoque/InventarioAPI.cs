﻿using Hino.Estoque.App.Models;
using Hino.Estoque.App.Models.Estoque;
using Hino.Estoque.App.Services.API.Interfaces.Estoque;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.API.Estoque
{
    public class InventarioAPI : BaseAPI<ESInventarioModel>, IInventarioAPI
    {
        public async Task<IEnumerable<ESInventarioModel>> GetPendentesAsync()
        {
            var result =
               await _Request.GetAsync(
                   $"{EndPoint.Replace("{pCodEstab}", CurrentApp.CODESTAB.ToString())}/Pendentes", true);

            var data = result.ToObject<DefaultResultModel>(_JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<IEnumerable<ESInventarioModel>>(_JsonSerializer);
        }

        public async Task<IEnumerable<ESInventDetModel>> GetItensInventarioAsync(short pCodInv, decimal pContagem)
        {
            var result =
               await _Request.GetAsync(
                   $"{EndPoint.Replace("{pCodEstab}", CurrentApp.CODESTAB.ToString())}/Lista/{pCodInv}/Contagem/{pContagem}", true);

            var data = result.ToObject<DefaultResultModel>(_JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<IEnumerable<ESInventDetModel>>(_JsonSerializer);
        }

        public async Task<bool> PostContagemAsync(short pCodInv, decimal pContagem, ESUpdateInventDetModel[] pESUpdateInventDet)
        {
            var content = new StringContent(
                JsonConvert.SerializeObject(pESUpdateInventDet),
                Encoding.UTF8, "application/json");

            var result =
               await _Request.PostAsync(
                   $"{EndPoint.Replace("{pCodEstab}", CurrentApp.CODESTAB.ToString())}/Lista/{pCodInv}/Contagem/{pContagem}/Confirmar", true, content);

            var data = result.ToObject<DefaultResultModel>(_JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return false;
            }

            return true;
        }

        public async Task<ESInventarioModel> GetInventarioAsync(short pCodInv, decimal pContagem)
        {
            var result =
                  await _Request.GetAsync(
                      $"{EndPoint.Replace("{pCodEstab}", CurrentApp.CODESTAB.ToString())}/Invent/{pCodInv}/Contagem/{pContagem}/Lista", true);

            var data = result.ToObject<DefaultResultModel>(_JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<ESInventarioModel>(_JsonSerializer);
        }

        public async Task<ESInventDetModel> PostInventarioItemAsync(ESInventDetItemModel pInventItem)
        {
            var content = new StringContent(
                   JsonConvert.SerializeObject(pInventItem),
                   Encoding.UTF8, "application/json");

            var result =
               await _Request.PostAsync(
                   $"{EndPoint.Replace("{pCodEstab}", CurrentApp.CODESTAB.ToString())}/Inventario/Produto", true, content);

            var data = result.ToObject<DefaultResultModel>(_JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }
            return data.data.ToObject<ESInventDetModel>(_JsonSerializer);
        }

        public async Task<ESCreateItemInventarioModel> PostCreateItem(ESCreateItemInventarioModel pCreateItem)
        {
            var content = new StringContent(
                   JsonConvert.SerializeObject(pCreateItem),
                   Encoding.UTF8, "application/json");

            var result =
               await _Request.PostAsync(
                   $"{EndPoint.Replace("{pCodEstab}", CurrentApp.CODESTAB.ToString())}/Lista/{pCreateItem.codinv}/Contagem/{pCreateItem.contagem}/Create", true, content);

            var data = result.ToObject<DefaultResultModel>(_JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }
            return data.data.ToObject<ESCreateItemInventarioModel>(_JsonSerializer);
        }
    }
}
