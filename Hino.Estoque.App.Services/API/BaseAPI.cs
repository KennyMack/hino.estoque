﻿using Hino.Estoque.App.Models;
using Hino.Estoque.App.Models.Auth;
using Hino.Estoque.App.Services.API.Interfaces;
using Hino.Estoque.App.Utils.Attributes;
using Hino.Estoque.App.Utils.Exceptions;
using Hino.Estoque.App.Utils.Paging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.API
{
    public class BaseAPI<T> : IBaseAPI<T> where T : BaseEntity
    {
        protected readonly Request _Request;
        public List<ModelException> Errors { get => _Request.Errors; set => _Request.Errors = value; }
        public JsonSerializerSettings _JsonSerializerSettings { get => _Request._JsonSerializerSettings; }
        public JsonSerializer _JsonSerializer { get => _Request._JsonSerializer; }

        public GEUsuariosLogadoModel User;
        public readonly string EndPoint;
        public string BASEURL
        {
            get => _Request.BASEURL;
        }

        public BaseAPI()
        {
            try
            {
                _Request = new Request();
                User = CurrentApp.UserLogged;
            }
            catch (Exception)
            {
                
            }
            try
            {
                Type baseType = typeof(T);
                EndPoint =
                    ((EndPointAttribute)baseType.GetCustomAttributes(typeof(EndPointAttribute), false).FirstOrDefault()).EndPoint;
            }
            catch (Exception)
            {
                throw new NotImplementedException("FALTOU O EndPointAttribute BAHIA !!!");
            }
        }

        #region Replace EndPoint
        private string ReplaceEndPoint()
        {
            var retorno = EndPoint;

            if (retorno.Contains("{pCodEstab}"))
                retorno = retorno.Replace("{pCodEstab}", (User?.GEFuncionarios?.codestab ?? 0).ToString());

            return retorno;
        }
        #endregion

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var result =
                await _Request.GetAsync(
                    $"{ReplaceEndPoint()}/all", true);

            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<IEnumerable<T>>(this._JsonSerializer);
        }

        public async Task<T> PostAsync(T model)
        {
            var content = new StringContent(
                JsonConvert.SerializeObject(model),
                Encoding.UTF8, "application/json");

            var result =
               await _Request.PostAsync(
                   $"{ReplaceEndPoint()}/save", true, content);

            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<T>(this._JsonSerializer);
        }

        public async Task<T> PutAsync(T model)
        {
            var content = new StringContent(
                   JsonConvert.SerializeObject(model),
                   Encoding.UTF8, "application/json");

            var result =
               await _Request.PutAsync(
                   $"{ReplaceEndPoint()}/save", true, content);

            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<T>(this._JsonSerializer);
        }

        public async Task<T> DeleteAsync(T model)
        {
            var result =
               await _Request.DeleteAsync(
                   $"{ReplaceEndPoint()}/delete", true);

            return null;
        }

        public async Task<bool> ConectionTest()
        {
            var result =
              await _Request.GetAsync(
                  $"Test", false, new TimeSpan(0, 0, 5));

            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return false;
            }

            return true;
        }
    }
}

