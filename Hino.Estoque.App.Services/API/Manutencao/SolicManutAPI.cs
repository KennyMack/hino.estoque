﻿using Hino.Estoque.App.Models;
using Hino.Estoque.App.Models.Manutencao;
using Hino.Estoque.App.Services.API.Interfaces.Manutencao;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.API.Manutencao
{
    public class SolicManutAPI : BaseAPI<MNSolicManutModel>, ISolicManutAPI
    {
        public async Task<IEnumerable<MNSolicManutModel>> BuscaListaSolicManutAsync(MNSolicForUserModel pSolicForUser)
        {
            var content = new StringContent(
                    JsonConvert.SerializeObject(pSolicForUser),
                    Encoding.UTF8, "application/json");

            var result =
               await _Request.PostAsync(
                   $"{EndPoint.Replace("{pCodEstab}", CurrentApp.CODESTAB.ToString())}/Funcionario/{pSolicForUser.codfuncionario}/All", true, content);

            var data = result.ToObject<DefaultResultModel>(_JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<IEnumerable<MNSolicManutModel>>(_JsonSerializer);
        }

        public async Task<MNSolicManutModel> PostChangeStatus(MNStatusSolicManutModel pStatus)
        {
            var content = new StringContent(
                JsonConvert.SerializeObject(pStatus),
                Encoding.UTF8, "application/json");

            var urlPart = pStatus.status == 1 ? "Aprovar" : "Reprovar";

            var result =
               await _Request.PostAsync(
                   $"{EndPoint.Replace("{pCodEstab}", CurrentApp.CODESTAB.ToString())}/{urlPart}", true, content);

            var data = result.ToObject<DefaultResultModel>(_JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<MNSolicManutModel>(_JsonSerializer);
        }

        public async Task<MNSolicManutModel> PostSolicitarManutencaoAsync(MNCriarSolicManutModel pCriar)
        {
            var content = new StringContent(
                       JsonConvert.SerializeObject(pCriar),
                       Encoding.UTF8, "application/json");

            var result =
               await _Request.PostAsync(
                   $"{EndPoint.Replace("{pCodEstab}", CurrentApp.CODESTAB.ToString())}/NOVO", true, content);

            var data = result.ToObject<DefaultResultModel>(_JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<MNSolicManutModel>(_JsonSerializer);
        }

        public async Task<MNSearchManutModel> SearchManutAsync(int pCodEstab)
        {
            var result =
               await _Request.GetAsync(
                   $"{EndPoint.Replace("{pCodEstab}", pCodEstab.ToString())}/Search", true);

            var data = result.ToObject<DefaultResultModel>(_JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<MNSearchManutModel>(_JsonSerializer);
        }
    }
}
