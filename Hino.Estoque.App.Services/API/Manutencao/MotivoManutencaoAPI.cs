﻿using Hino.Estoque.App.Models;
using Hino.Estoque.App.Models.Manutencao;
using Hino.Estoque.App.Services.API.Interfaces.Manutencao;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.API.Manutencao
{
    public class MotivoManutencaoAPI : BaseAPI<MNMotivoModel>, IMotivoManutencaoAPI
    {
        public async Task<IEnumerable<MNMotivoModel>> BuscaMotivoManutencaoAsync(MNSearchMotivoModel pSearch)
        {
            var content = new StringContent(
                    JsonConvert.SerializeObject(pSearch),
                    Encoding.UTF8, "application/json");

            var result =
               await _Request.PostAsync(
                   $"{EndPoint.Replace("{pCodEstab}", CurrentApp.CODESTAB.ToString())}/Barras", true, content);

            var data = result.ToObject<DefaultResultModel>(_JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<IEnumerable<MNMotivoModel>>(_JsonSerializer);
        }
    }
}
