﻿using Hino.Estoque.App.Models;
using Hino.Estoque.App.Models.Auth;
using Hino.Estoque.App.Models.Gerais;
using Hino.Estoque.App.Services.API.Interfaces.Gerais;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.API.Gerais
{
    public class EstabelecimentosAPI : BaseAPI<GEEstabModel>, IEstabelecimentosAPI
    {
        public async Task<IEnumerable<GEEstabModel>> GetAllEstabs()
        {
            var result =
                 await _Request.GetAsync(
                     $"{EndPoint}/all", true);

            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<IEnumerable<GEEstabModel>>(this._JsonSerializer);

        }

        public async Task<GESettingsModel> GetSettings()
        {
            var result =
                 await _Request.GetAsync(
                     $"{EndPoint}/configuracoes", true);

            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            var EstabSettings = data.data.ToObject<GEEstabSettingsModel>(this._JsonSerializer);

            return EstabSettings.Settings;
        }
    }
}
