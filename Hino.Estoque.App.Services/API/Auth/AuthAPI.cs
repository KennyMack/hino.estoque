﻿using Hino.Estoque.App.Models;
using Hino.Estoque.App.Models.Auth;
using Hino.Estoque.App.Services.API.Interfaces.Auth;
using Newtonsoft.Json;
using System.Net.Http;
using System.Text;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.API.Auth
{
    public class AuthAPI : BaseAPI<GEUsuariosLoginModel>, IAuthAPI
    {
        public async Task<GEUsuariosLogadoModel> LoginAsync(GEUsuariosLoginModel pLogin)
        {
            var content = new StringContent(
                JsonConvert.SerializeObject(pLogin),
                Encoding.UTF8, "application/json");

            var result =
                  await _Request.PostAsync(
                      $"{EndPoint}/Login", true, content);

            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return new GEUsuariosLogadoModel
                {
                    success = false,
                    error = string.Join(", ", data.error.FirstOrDefault().Messages)
                };
            }

            var user = data.data.ToObject<GEUsuariosLogadoModel>(this._JsonSerializer);
            user.success = data.success;

            return user;
        }

        public async Task<bool> TestServerAsync()
        {
            var result =
               await _Request.GetAsync(
                   $"{EndPoint}/Test", true);

            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return false;
            }

            return true;

        }
    }
}
