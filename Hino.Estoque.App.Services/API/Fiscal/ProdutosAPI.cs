﻿using Hino.Estoque.App.Models;
using Hino.Estoque.App.Models.Estoque;
using Hino.Estoque.App.Models.Fiscal;
using Hino.Estoque.App.Models.Producao;
using Hino.Estoque.App.Services.API.Interfaces.Estoque;
using Hino.Estoque.App.Services.API.Interfaces.Fiscal;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.API.Fiscal
{
    public class ProdutosAPI : BaseAPI<FSProdutoModel>, IProdutosAPI
    {
        public async Task<FSProdutoModel> GetProductByBarCodeAsync(short pCodEstab, string pBarCode)
        {
            var content = new StringContent(
                JsonConvert.SerializeObject(new FSProdutoModel { codproduto = pBarCode }),
                Encoding.UTF8, "application/json");

            var result =
               await _Request.PostAsync(
                   $"{EndPoint.Replace("{pCodEstab}", CurrentApp.CODESTAB.ToString())}/Barras", true, content);
            
            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<FSProdutoModel>(this._JsonSerializer);
        }

        public async Task<IEnumerable<FSSaldoEstoqueModel>> GetProductInventoryBalancesAsync(ESBuscaSaldoEstoqueModel pProduct)
        {
            var content = new StringContent(
                JsonConvert.SerializeObject(pProduct),
                Encoding.UTF8, "application/json");

            var result =
               await _Request.PostAsync(
                   $"{EndPoint.Replace("{pCodEstab}", CurrentApp.CODESTAB.ToString())}/Saldos", true, content);

            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<IEnumerable<FSSaldoEstoqueModel>>(this._JsonSerializer);
        }
    }
}
