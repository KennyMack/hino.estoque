﻿using Hino.Estoque.App.Models.Engenharia;
using Hino.Estoque.App.Models.Producao.OEE;
using Hino.Estoque.App.Services.API.Interfaces.Producao.OEE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.Application.Producao.OEE
{
    public class OEEService
    {
        public async Task<ENSearchOEEModel> SearchOEEAsync(int pCodEstab)
        {
            var _ISolicManutAPI = DInjection.GetIntance<IOEEAPI>();
            return await _ISolicManutAPI.SearchOEEAsync(pCodEstab);
        }

        public async Task<string> GerarOEEAsync(int pCodEstab, PDGerarOEEModel pGerar)
        {
            var _IOEEAPI = DInjection.GetIntance<IOEEAPI>();
            _IOEEAPI.Errors.Clear();
            var ret = await _IOEEAPI.GerarOEEAsync(pCodEstab, pGerar);
            if (!ret && _IOEEAPI.Errors.Any())
                return string.Join(", ", _IOEEAPI.Errors.First().Messages);
            else if (!ret && !_IOEEAPI.Errors.Any())
                return "Não foi possível gerar o OEE";

            return "";
        }
        public async Task<string> GerarOEEMensalAsync(int pCodEstab, PDGerarOEEModel pGerar)
        {
            var _IOEEAPI = DInjection.GetIntance<IOEEAPI>();
            _IOEEAPI.Errors.Clear();
            var ret = await _IOEEAPI.GerarOEEMensalAsync(pCodEstab, pGerar);
            if (!ret && _IOEEAPI.Errors.Any())
                return string.Join(", ", _IOEEAPI.Errors.First().Messages);
            else if (!ret && !_IOEEAPI.Errors.Any())
                return "Não foi possível gerar o OEE Mensal";

            return "";
        }

        public async Task<PDOEEResultModel> ListarOEEAsync(short pCodEstab, string pCodUsuario, DateTime pDtInicio, string pCodMaquina)
        {
            var _IOEEAPI = DInjection.GetIntance<IOEEAPI>();
            _IOEEAPI.Errors.Clear();
            var ret = await _IOEEAPI.ListarOEEAsync(pCodEstab, pCodUsuario, pDtInicio, pCodMaquina);

            if (ret == null && _IOEEAPI.Errors.Any())
                throw new Exception(string.Join(", ", _IOEEAPI.Errors.First().Messages));
            else if (ret == null && !_IOEEAPI.Errors.Any())
                throw new Exception("Não foi possível gerar o OEE");

            return ret;
        }

        public async Task<PDOEEMensalResultModel> ListarOEEMensalAsync(short pCodEstab, 
            string pCodUsuario, DateTime pDtInicio)
        {
            var _IOEEAPI = DInjection.GetIntance<IOEEAPI>();
            _IOEEAPI.Errors.Clear();
            var ret = await _IOEEAPI.ListarOEEMensalAsync(pCodEstab, pCodUsuario, pDtInicio);

            if (ret == null && _IOEEAPI.Errors.Any())
                throw new Exception(string.Join(", ", _IOEEAPI.Errors.First().Messages));
            else if (ret == null && !_IOEEAPI.Errors.Any())
                throw new Exception("Não foi possível gerar o OEE");

            return ret;
        }
    }
}
