﻿using Hino.Estoque.App.Models.Producao.Apontamento;
using Hino.Estoque.App.Services.API.Interfaces.Producao.Apontamento;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Hino.Estoque.App.Services.API.Interfaces.Producao;
using Hino.Estoque.App.Models.Producao;
using Hino.Estoque.App.Services.API.Interfaces.Producao.Sucata;
using Hino.Estoque.App.Services.DB.Interfaces.Producao;
using Hino.Estoque.App.Models.Engenharia;

namespace Hino.Estoque.App.Services.Application.Producao.Apontamento
{
    public class AppointmentService
    {
        public async Task<PDAptInicioProdModel> IniciarProducaoAsync(PDAptInicioProdModel pModel)
        {
            try
            {
                var _IAptInicioProdAPI = DInjection.GetIntance<IAptInicioProdAPI>();
                var retorno = await _IAptInicioProdAPI.IniciarProducaoAsync(pModel);

                if (_IAptInicioProdAPI.Errors.Any())
                    throw new Exception(_IAptInicioProdAPI.Errors.Select(r => string.Join(", ", r.Messages)).First());

                return retorno;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<PDAptInicioProdModel> IniciarParadaAsync(PDAptInicioProdModel pModel)
        {
            try
            {
                var _IAptInicioProdAPI = DInjection.GetIntance<IAptInicioProdAPI>();
                var retorno = await _IAptInicioProdAPI.IniciarParadaAsync(pModel);

                if (_IAptInicioProdAPI.Errors.Any())
                    throw new Exception(_IAptInicioProdAPI.Errors.Select(r => string.Join(", ", r.Messages)).First());

                return retorno;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<PDAptTerminoProdModel> FinalizarProducaoAsync(PDAptTerminoProdModel pModel)
        {
            try
            {
                var _IAptTerminoProdAPI = DInjection.GetIntance<IAptTerminoProdAPI>();
                var retorno = await _IAptTerminoProdAPI.FinalizarProducaoAsync(pModel);

                if (_IAptTerminoProdAPI.Errors.Any())
                    throw new Exception(_IAptTerminoProdAPI.Errors.Select(r => string.Join(", ", r.Messages)).First());

                return retorno;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<PDAptTerminoProdModel> FinalizarTurnoProducaoAsync(PDAptTerminoProdModel pModel)
        {
            try
            {
                var _IAptTerminoProdAPI = DInjection.GetIntance<IAptTerminoProdAPI>();
                var retorno = await _IAptTerminoProdAPI.FinalizarTurnoProducaoAsync(pModel);

                if (_IAptTerminoProdAPI.Errors.Any())
                    throw new Exception(_IAptTerminoProdAPI.Errors.Select(r => string.Join(", ", r.Messages)).First());

                return retorno;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<PDAptTerminoProdModel> FinalizarParadaAsync(PDAptTerminoProdModel pModel)
        {
            try
            {
                var _IAptTerminoProdAPI = DInjection.GetIntance<IAptTerminoProdAPI>();
                var retorno = await _IAptTerminoProdAPI.FinalizarParadaAsync(pModel);

                if (_IAptTerminoProdAPI.Errors.Any())
                    throw new Exception(_IAptTerminoProdAPI.Errors.Select(r => string.Join(", ", r.Messages)).First());

                return retorno;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<PDSucataModel> SalvarSucataAsync(PDSucataCreateVM pModel)
        {
            try
            {
                var _ISucataAPI = DInjection.GetIntance<ISucataAPI>();
                var retorno = await _ISucataAPI.PostApontarSucataAsync(pModel);

                if (_ISucataAPI.Errors.Any())
                    throw new Exception(_ISucataAPI.Errors.Select(r => string.Join(", ", r.Messages)).First());

                return retorno;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<PDAptInicioProdModel> GetAptInicioAsync(PDAptInicioProdModel pModel)
        {
            try
            {
                var _IAptInicioProdAPI = DInjection.GetIntance<IAptInicioProdAPI>();
                var retorno = await _IAptInicioProdAPI.GetApontamentoInicioAsync(pModel);

                if (_IAptInicioProdAPI.Errors.Any())
                    throw new Exception(_IAptInicioProdAPI.Errors.Select(r => string.Join(", ", r.Messages)).First());

                return retorno;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<PDAptLancamentosModel>> GetListApontamentosAsync()
        {
            try
            {
                var _IAptInicioProdAPI = DInjection.GetIntance<IAptInicioProdAPI>();
                var retorno = await _IAptInicioProdAPI.GetListApontamentosAsync();

                if (_IAptInicioProdAPI.Errors.Any())
                    throw new Exception(_IAptInicioProdAPI.Errors.Select(r => string.Join(", ", r.Messages)).First());

                return retorno;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<PDOrdemProdCompModel>> GetProductsToSucAsync(PDOrdemProdModel pOp)
        {
            try
            {
                var _IOrdemProdAPI = DInjection.GetIntance<IOrdemProdAPI>();
                var retorno = await _IOrdemProdAPI.GetProductsToSucAsync(pOp);

                if (_IOrdemProdAPI.Errors.Any())
                    throw new Exception(_IOrdemProdAPI.Errors.Select(r => string.Join(", ", r.Messages)).First());

                return retorno;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<PDMotivosModel>> GetRefugoMotivosAsync()
        {
            try
            {
                var _IMotivosAPI = DInjection.GetIntance<IMotivosAPI>();
                var retorno = await _IMotivosAPI.GetMotivosRefugoAsync();

                if (_IMotivosAPI.Errors.Any())
                    throw new Exception(_IMotivosAPI.Errors.Select(r => string.Join(", ", r.Messages)).First());

                return retorno;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<PDMotivosModel>> GetParadaMotivosAsync()
        {
            try
            {
                var _IMotivosAPI = DInjection.GetIntance<IMotivosAPI>();
                var retornoParada = await _IMotivosAPI.GetMotivosParadaAsync();

                if (_IMotivosAPI.Errors.Any())
                    throw new Exception(_IMotivosAPI.Errors.Select(r => string.Join(", ", r.Messages)).First());

                var retornoProg = await _IMotivosAPI.GetMotivosParadaProgAsync();

                if (_IMotivosAPI.Errors.Any())
                    throw new Exception(_IMotivosAPI.Errors.Select(r => string.Join(", ", r.Messages)).First());

                var retorno = new List<PDMotivosModel>();
                
                foreach (var item in retornoParada)
                    retorno.Add(item);
                foreach (var item in retornoProg)
                    retorno.Add(item);

                return retorno;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task ClearRelatorioEficienciaProdAsync()
        {
            var _RelatorioDB = DInjection.GetIntance<IPDRelatorioEficienciaProducaoDB>();
            await _RelatorioDB.DeleteAllAsync();
        }

        public async Task SaveRelatorioEficienciaProdAsync(IEnumerable<PDRelatorioEficienciaProducaoModel> pRelatorio)
        {
            var _RelatorioDB = DInjection.GetIntance<IPDRelatorioEficienciaProducaoDB>();
            await _RelatorioDB.SaveAllAsync(pRelatorio);
        }

        public async Task GenerateAndLoadEficienciaProdAsync(DateTime pDataInicio, DateTime pDataTermino)
        {
            await ClearRelatorioEficienciaProdAsync();
            var _IPDRelatorioEficienciaProducaoAPI = DInjection.GetIntance<IPDRelatorioEficienciaProducaoAPI>();

            var result = await _IPDRelatorioEficienciaProducaoAPI.PostRelatorioEficienciaAsync(pDataInicio, pDataTermino);
            await SaveRelatorioEficienciaProdAsync(result);
        }

        public async Task<IList<PDRelatorioEficienciaProducaoModel>> GetRelatorioEficienciaProdAsync()
        {
            var _RelatorioDB = DInjection.GetIntance<IPDRelatorioEficienciaProducaoDB>();
            return (await _RelatorioDB.GetItemsAsync(0)).Results;
        }

        public async Task<IEnumerable<ENMaquinasModel>> GetMachinesAsync()
        {
            try
            {
                var _IAptInicioProdAPI = DInjection.GetIntance<IAptInicioProdAPI>();
                return await _IAptInicioProdAPI.GetMachinesAsync();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
