﻿using Hino.Estoque.App.Models.Producao;
using Hino.Estoque.App.Services.API.Interfaces.Estoque;
using Hino.Estoque.App.Services.API.Interfaces.Producao;
using Hino.Estoque.App.Services.DB.Interfaces.Producao;
using Hino.Estoque.App.Utils.Exceptions;
using Hino.Estoque.App.Utils.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.Application.Estoque
{
    public class OPTransferenciaService
    {
        public async Task<PDOrdemProdModel> GetOPByBarCodeAsync(string pBarCode)
        {
            if (!long.TryParse(pBarCode, out long BarCode))
                throw new Exception(Resources.ErrorMessagesResource.InvalidBarCode);

            var _IOrdemProdAPI = DInjection.GetIntance<IOrdemProdAPI>();

            var ret = await _IOrdemProdAPI.GetOPByBarCodeAsync(pBarCode);

            if (_IOrdemProdAPI.Errors.Count > 0)
                throw new Exception(_IOrdemProdAPI.Errors.GetMessages());

            return ret;
        }

        public async Task<OPTransfResult> PostOPSeparateAsync(decimal pCodOrdProd, string pNivelOrdProd, PDOPCreateTransfModel[] plstTransf)
        {
            if (plstTransf.Length <= 0)
                throw new Exception(Resources.ErrorMessagesResource.ItemArrayIsEmpty);

            var _ITransferenciaAPI = DInjection.GetIntance<ITransferenciaAPI>();
            var _IPDOPCreateTransfDB = DInjection.GetIntance<IPDOPCreateTransfDB>();

            await _IPDOPCreateTransfDB.SaveTransfAsync(plstTransf);

            var ret = await _ITransferenciaAPI.PostOPSeparateAsync(plstTransf);

            if (_ITransferenciaAPI.Errors.Count > 0)
            {
                await _IPDOPCreateTransfDB.UpdateTransfResultAsync(plstTransf, ret);
                throw new Exception(_ITransferenciaAPI.Errors.GetMessages());
            }
            else
                await RemoveOpComponentsAsync(pCodOrdProd, pNivelOrdProd, plstTransf, ret);

            return ret;
        }
        
        public async Task<bool> LoadOPComponentsAsync(PDOrdemProdModel pPDOrdemProdModel)
        {
            try
            {
                var _IPDOrdemProdCompDB = DInjection.GetIntance<IPDOrdemProdCompDB>();
                var _IPDOrdemProdCompEstqEndDB = DInjection.GetIntance<IPDOrdemProdCompEstqEndDB>();
                var AnyRegister = await _IPDOrdemProdCompDB.GetAnyRegisterAsync(
                    (short)CurrentApp.CODESTAB, pPDOrdemProdModel.codordprod, pPDOrdemProdModel.nivelordprod);

                if (AnyRegister == null)
                {
                    var registers = pPDOrdemProdModel.pdordemprodcomp;

                    await _IPDOrdemProdCompDB.AddFromListResult(registers);

                    foreach (var componente in registers)
                    {
                        if (componente.ordemprodcompestqend != null && componente.ordemprodcompestqend.Any())
                            await _IPDOrdemProdCompEstqEndDB.AddFromListResult(componente.ordemprodcompestqend);
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        public async Task<PagedResult<PDOrdemProdCompModel>> SearchAsync(decimal pCodOrdProd, string pNivelOrdProd, string pSearchData, int page)
        {
            var _IESInventDetDB = DInjection.GetIntance<IPDOrdemProdCompDB>();
            var Components = await _IESInventDetDB.SearchOPComponentsAsync((short)CurrentApp.CODESTAB, pCodOrdProd, pNivelOrdProd, pSearchData, page);

            var Enderecos = (await GetAllCompStockEndAsync(pCodOrdProd, pNivelOrdProd)).Results;

            foreach (var item in Components.Results)
                item.ordemprodcompestqend = Enderecos.Where(r => r.codproduto == item.codcomponente).ToList();

            return Components;
        }

        public async Task<PagedResult<PDOrdemProdCompModel>> GetAllComponentsAsync(decimal pCodOrdProd, string pNivelOrdProd)
        {
            var _IESInventDetDB = DInjection.GetIntance<IPDOrdemProdCompDB>();
            return await _IESInventDetDB.GetAllComponentsAsync((short)CurrentApp.CODESTAB, pCodOrdProd, pNivelOrdProd);
        }

        public async Task<PagedResult<PDOrdemProdCompEstqEndModel>> GetAllCompStockEndAsync(decimal pCodOrdProd, string pNivelOrdProd)
        {
            var _IPDOrdemProdCompEstqEndDB = DInjection.GetIntance<IPDOrdemProdCompEstqEndDB>();
            return await _IPDOrdemProdCompEstqEndDB.GetAllCompStockEndAsync((short)CurrentApp.CODESTAB, pCodOrdProd, pNivelOrdProd);
        }

        public async Task<PagedResult<PDOPCreateTransfItemModel>> GetAllTransfItemAsync(decimal pCodOrdProd, string pNivelOrdProd)
        {
            var _PDOPCreateTransfItem = DInjection.GetIntance<IPDOPCreateTransfItemDB>();
            return await _PDOPCreateTransfItem.GetAllCompTransfAsync((short)CurrentApp.CODESTAB, pCodOrdProd, pNivelOrdProd);
        }

        public async Task<bool> SaveValueAsync(OPComponentsResult pOPComponentsResult)
        {
            try
            {
                var _IESInventDetDB = DInjection.GetIntance<IPDOrdemProdCompDB>();
                var OPComponent = await _IESInventDetDB.GetItemAsync(pOPComponentsResult.IdMobile);

                if (OPComponent.FSProduto_separacaomaximo > 0 &&
                    pOPComponentsResult.qtdseparada > OPComponent.FSProduto_separacaomaximo)
                {
                    var except = new Exception(Resources.ValidationMessagesResource.QtdMaxSepExceeded)
                    {
                        Source = "exceeded"
                    };
                    throw except;
                }
                OPComponent.qtdSeparada = pOPComponentsResult.qtdseparada;
                OPComponent.separado = pOPComponentsResult.qtdseparada > 0;

                await _IESInventDetDB.UpdateItemAsync(OPComponent);
            }
            catch (Exception ex)
            {
                if (ex.Source == "exceeded")
                    throw ex;
                return false;
            }

            return true;
        }

        public async Task<IEnumerable<PDOrdemProdCompModel>> GetAllComponentsAlteradosAsync(decimal pCodOrdProd, string pNivelOrdProd)
        {
            var _IPDOrdemProdCompDB = DInjection.GetIntance<IPDOrdemProdCompDB>();
            return await _IPDOrdemProdCompDB.GetAllComponentsAlteradosAsync((short)CurrentApp.CODESTAB, pCodOrdProd, pNivelOrdProd);
        }

        public async Task<bool> RemoveOpComponentsAsync(decimal pCodOrdProd, string pNivelOrdProd, PDOPCreateTransfModel[] plstTransf, OPTransfResult pTransfResult)
        {
            try
            {
                var _IPDOrdemProdCompDB = DInjection.GetIntance<IPDOrdemProdCompDB>();
                var _IPDOPCreateTransfDB = DInjection.GetIntance<IPDOPCreateTransfDB>();
                var _PDOPCreateTransfItemDB = DInjection.GetIntance<IPDOPCreateTransfItemDB>();
                var _IPDOrdemProdCompEstqEndDB = DInjection.GetIntance<IPDOrdemProdCompEstqEndDB>();

                var itemsOp = await GetAllComponentsAsync(pCodOrdProd, pNivelOrdProd);

                var itemsTransf = await GetAllTransfItemAsync(pCodOrdProd, pNivelOrdProd);

                var itemsStockEnd = await GetAllCompStockEndAsync(pCodOrdProd, pNivelOrdProd);

                foreach (var item in itemsOp.Results)
                    await _IPDOrdemProdCompDB.DeleteItemAsync(item.IdMobile);

                foreach (var item in itemsTransf.Results)
                    await _PDOPCreateTransfItemDB.DeleteItemAsync(item.IdMobile);

                foreach (var item in itemsStockEnd.Results)
                    await _IPDOrdemProdCompEstqEndDB.DeleteItemAsync(item.IdMobile);

                // await _IPDOPCreateTransfDB.ClearTransfAsync(pCodOrdProd, pNivelOrdProd);

                if (pTransfResult != null)
                    await _IPDOPCreateTransfDB.UpdateTransfResultAsync(plstTransf, pTransfResult);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return true;
        }
    }
}
