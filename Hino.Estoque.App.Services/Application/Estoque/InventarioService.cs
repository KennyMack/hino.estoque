﻿using Hino.Estoque.App.Models.Estoque;
using Hino.Estoque.App.Services.API.Interfaces.Estoque;
using Hino.Estoque.App.Services.DB.Interfaces.Estoque;
using Hino.Estoque.App.Utils.Exceptions;
using Hino.Estoque.App.Utils.Paging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.Application.Estoque
{
    public class InventarioService
    {
        public async Task<IEnumerable<ESInventarioModel>> GetPendentesAsync()
        {
            var _IInventarioAPI = DInjection.GetIntance<IInventarioAPI>();
            return await _IInventarioAPI.GetPendentesAsync();
        }

        public string[] GetValuesFromBarCode(string pBarCode)
        {
            if (!pBarCode.Contains("-"))
                throw new Exception(Resources.ErrorMessagesResource.InvalidBarCode);

            return pBarCode.Split('-');
        }

        public int[] ValidateBarCode(string[] pBarCode)
        {
            if (pBarCode.Length != 2)
                throw new Exception(Resources.ErrorMessagesResource.InvalidBarCode);

            if (!int.TryParse(pBarCode[0], out int CodInv))
                throw new Exception(Resources.ErrorMessagesResource.InvalidInventary);

            if (!int.TryParse(pBarCode[1], out int Contagem))
                throw new Exception(Resources.ErrorMessagesResource.InvalidInventaryCount);

            return new int[] { CodInv, Contagem };
        }

        public async Task<IEnumerable<ESInventDetModel>> GetItensInventByBarCodeAsync(string pBarCode)
        {
            var BarCode = ValidateBarCode(GetValuesFromBarCode(pBarCode));

            return await GetItensInventarioAsync((short)BarCode[0], BarCode[1]);
        }

        public async Task<ESInventarioModel> GetInventarioAsync(string pBarCode)
        {
            var BarCode = ValidateBarCode(GetValuesFromBarCode(pBarCode));
            var _IInventarioAPI = DInjection.GetIntance<IInventarioAPI>();

            return await _IInventarioAPI.GetInventarioAsync((short)BarCode[0], BarCode[1]);
        }

        public async Task<IEnumerable<ESInventDetModel>> GetItensInventarioAsync(string pCodInv, string pContagem)
        {
            var BarCode = ValidateBarCode(new string[] { pCodInv, pContagem });

            return await GetItensInventarioAsync((short)BarCode[0], BarCode[1]);
        }

        private async Task<IEnumerable<ESInventDetModel>> GetItensInventarioAsync(short pCodInv, decimal pContagem)
        {
            var _IInventarioAPI = DInjection.GetIntance<IInventarioAPI>();
            return await _IInventarioAPI.GetItensInventarioAsync(pCodInv, pContagem);
        }

        public async Task<ESInventDetModel> PostInventarioItemAsync(ESInventDetItemModel pInventItem)
        {
            var _IInventarioAPI = DInjection.GetIntance<IInventarioAPI>();
            var result = await _IInventarioAPI.PostInventarioItemAsync(pInventItem);

            if (_IInventarioAPI.Errors.Count > 0)
                throw new Exception(_IInventarioAPI.Errors.GetMessages());

            return result;
        }

        public async Task<bool> PostContagemAsync(string pCodInv, string pContagem, ESUpdateInventDetModel[] pESUpdateInventDet)
        {
            if (pESUpdateInventDet.Length <= 0)
                throw new Exception(Resources.ErrorMessagesResource.ItemArrayIsEmpty);

            var BarCode = ValidateBarCode(new string[] { pCodInv, pContagem });

            var _IInventarioAPI = DInjection.GetIntance<IInventarioAPI>();

            var ret = await _IInventarioAPI.PostContagemAsync((short)BarCode[0], BarCode[1], pESUpdateInventDet);

            if (_IInventarioAPI.Errors.Count > 0)
                throw new Exception(_IInventarioAPI.Errors.GetMessages());
            else
                await AtualizaAlteradoAsync(pCodInv, pContagem, pESUpdateInventDet);

            return true;
        }

        public async Task<IEnumerable<ESInventDetModel>> GetAllInventAlterados(decimal pCodInv, decimal pContagem)
        {
            var _IESInventDetDB = DInjection.GetIntance<IESInventDetDB>();
            return await _IESInventDetDB.GetAllInventAlterados((short)CurrentApp.CODESTAB, pCodInv, pContagem);
        }

        public async Task<bool> AtualizaAlteradoAsync(string pCodInv, string pContagem, ESUpdateInventDetModel[] pESUpdateInventDet)
        {
            try
            {
                var _IESInventDetDB = DInjection.GetIntance<IESInventDetDB>();

                foreach (var item in pESUpdateInventDet)
                {
                    var InventDetItem = await _IESInventDetDB.GetItemAsync(item.IdMobile);

                    if (InventDetItem != null)
                    {
                        InventDetItem.saldooriginal = InventDetItem.saldoinvent;
                        InventDetItem.alterado = false;
                        await _IESInventDetDB.UpdateItemAsync(InventDetItem);
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return true;
        }

        public async Task<bool> HasInventDetItems()
        {
            var _IESInventDetDB = DInjection.GetIntance<IESInventDetDB>();

            return (await _IESInventDetDB.HasDataAsync()) > 0;
        }

        public async Task<PagedResult<ESInventDetModel>> Search(decimal pCodInv, decimal pContagem, string pSearchData, int page)
        {
            var _IESInventDetDB = DInjection.GetIntance<IESInventDetDB>();
            return await _IESInventDetDB.SearchInvItems((short)CurrentApp.CODESTAB, pCodInv, pContagem, pSearchData, page);
        }

        public async Task<bool> SaveValueAsync(InventDetResult pInventDetResult)
        {
            var codUser = "";
            try
            {
                codUser = CurrentApp.UserLogged.GEUsuarios.codusuario;
            }
            catch (Exception)
            {
            }
            
            try
            {
                var _IESInventDetDB = DInjection.GetIntance<IESInventDetDB>();
                var InventDetItem = await _IESInventDetDB.GetItemAsync(pInventDetResult.IdMobile);

                InventDetItem.saldoinvent = pInventDetResult.saldoinvent;
                InventDetItem.alterado = true;
                InventDetItem.codusuario = codUser;
                InventDetItem.datacontagem = DateTime.Now;

                await _IESInventDetDB.UpdateItemAsync(InventDetItem);
            }
            catch
            {
                return false;
            }

            return true;
        }

        public async Task<bool> ReloadData(decimal pCodInv, decimal pContagem)
        {
            var _IESInventDetDB = DInjection.GetIntance<IESInventDetDB>();
            await _IESInventDetDB.DeleteAllInventItems((short)CurrentApp.CODESTAB, pCodInv, pContagem);

            return await LoadInventDetItems(pCodInv, pContagem);
        }

        public async Task<bool> LoadInventDetItems(decimal pCodInv, decimal pContagem)
        {
            try
            {
                var _IESInventDetDB = DInjection.GetIntance<IESInventDetDB>();
                var AnyRegister = await _IESInventDetDB.GetAnyRegister((short)CurrentApp.CODESTAB, pCodInv, pContagem);

                if (AnyRegister == null)
                {
                    var registers = await GetItensInventarioAsync((short)pCodInv, pContagem);

                    await _IESInventDetDB.AddFromListResult(registers);
                }
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        public async Task<ESCreateItemInventarioModel> CreateItemInventContagemAsync(ESCreateItemInventarioModel pCreateItem)
        {
            var _IInventarioAPI = DInjection.GetIntance<IInventarioAPI>();
            var result = await _IInventarioAPI.PostCreateItem(pCreateItem);

            if (_IInventarioAPI.Errors.Count > 0)
                throw new Exception(_IInventarioAPI.Errors.GetMessages());

            return result;
        }
    }
}
