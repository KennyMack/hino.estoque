﻿using Hino.Estoque.App.Models.Estoque;
using Hino.Estoque.App.Services.API.Interfaces.Estoque;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.Application.Estoque
{
    public class RequisicaoService
    {
        public async Task<IEnumerable<ESRequisicoesModel>> GetPendentesAsync()
        {
            var _IRequisicaoAPI = DInjection.GetIntance<IRequisicaoAPI>();
            return await _IRequisicaoAPI.GetPendentesAsync();
        }

        public async Task<ESRequisicoesModel> PostRequisitarAsync(ESCreateRequestModel pCreate)
        {
            var _IRequisicaoAPI = DInjection.GetIntance<IRequisicaoAPI>();
            return await _IRequisicaoAPI.PostRequisitarAsync(pCreate);
        }

        public async Task<ESResultRequisicaoModel> PostReprovarRequisicaoAsync(ESUpdateRequestModel pCreate)
        {
            var _IRequisicaoAPI = DInjection.GetIntance<IRequisicaoAPI>();
            return await _IRequisicaoAPI.PostReprovarRequisicaoAsync(pCreate);
        }

        public async Task<ESResultRequisicaoModel> PostAprovarRequisicaoAsync(ESUpdateRequestModel pCreate)
        {
            var _IRequisicaoAPI = DInjection.GetIntance<IRequisicaoAPI>();
            return await _IRequisicaoAPI.PostAprovarRequisicaoAsync(pCreate);
        }
    }
}
