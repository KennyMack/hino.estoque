﻿using Hino.Estoque.App.Models.Manutencao;
using Hino.Estoque.App.Services.API.Interfaces.Manutencao;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.Application.Manutencao
{
    public class SolicManutService
    {
        public async Task<MNSolicManutModel> PostSolicitarManutencaoAsync(MNCriarSolicManutModel pCriar)
        {
            var _ISolicManutAPI = DInjection.GetIntance<ISolicManutAPI>();
            try
            {
                var retorno = await _ISolicManutAPI.PostSolicitarManutencaoAsync(pCriar);
                if (_ISolicManutAPI.Errors.Any())
                    throw new Exception(string.Concat(_ISolicManutAPI.Errors.First().Messages));
                return retorno;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<MNSolicManutModel>> BuscaListaSolicManutAsync(MNSolicForUserModel pSolicForUser)
        {
            var _ISolicManutAPI = DInjection.GetIntance<ISolicManutAPI>();
            return await _ISolicManutAPI.BuscaListaSolicManutAsync(pSolicForUser);
        }

        public async Task<MNSolicManutModel> AprovarSolicManutAsync(MNSolicManutModel pSolicManut)
        {
            var _ISolicManutAPI = DInjection.GetIntance<ISolicManutAPI>();
            return await _ISolicManutAPI.PostChangeStatus(new MNStatusSolicManutModel
            {
                codestab = pSolicManut.codestab,
                codsolic = pSolicManut.codsolic,
                codfuncionario = Convert.ToInt32(CurrentApp.UserLogged.GEFuncionarios.codfuncionario),
                status = 1
            });
        }

        public async Task<MNSolicManutModel> ReprovarSolicManutAsync(MNSolicManutModel pSolicManut)
        {
            var _ISolicManutAPI = DInjection.GetIntance<ISolicManutAPI>();
            return await _ISolicManutAPI.PostChangeStatus(new MNStatusSolicManutModel
            {
                codestab = pSolicManut.codestab,
                codsolic = pSolicManut.codsolic,
                codfuncionario = Convert.ToInt32(CurrentApp.UserLogged.GEFuncionarios.codfuncionario),
                status = 2
            });
        }

        public async Task<IEnumerable<MNEquipamentoModel>> BuscaEquipamentoAsync(string pEquipamento)
        {
            var _IEquipamentoAPI = DInjection.GetIntance<IEquipamentoAPI>();
            return await _IEquipamentoAPI.BuscaEquipamentoAsync(new MNSearchEquipamentoModel
            {
                equipamento = pEquipamento
            });
        }

        public async Task<IEnumerable<MNMotivoModel>> BuscaMotivoManutencaoAsync(int pCodMotivo)
        {
            var _IMotivoManutencaoAPI = DInjection.GetIntance<IMotivoManutencaoAPI>();
            return await _IMotivoManutencaoAPI.BuscaMotivoManutencaoAsync(new MNSearchMotivoModel
            {
                codmotivo = pCodMotivo
            });
        }

        public async Task<IEnumerable<MNTipoManutModel>> BuscaTipoManutencaoAsync(int pCodTipoManut)
        {
            var _ITipoManutencaoAPI = DInjection.GetIntance<ITipoManutencaoAPI>();
            return await _ITipoManutencaoAPI.BuscaTipoManutencaoAsync(new MNSearchTipoManutModel
            {
                codtipomanut = pCodTipoManut
            });
        }

        public async Task<MNSearchManutModel> SearchManutAsync(int pCodEstab)
        {
            var _ISolicManutAPI = DInjection.GetIntance<ISolicManutAPI>();
            return await _ISolicManutAPI.SearchManutAsync(pCodEstab);
        }
    }
}
