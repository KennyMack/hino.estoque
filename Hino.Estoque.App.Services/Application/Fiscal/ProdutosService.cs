﻿using Hino.Estoque.App.Models.Estoque;
using Hino.Estoque.App.Models.Fiscal;
using Hino.Estoque.App.Services.API.Interfaces.Estoque;
using Hino.Estoque.App.Services.API.Interfaces.Fiscal;
using Hino.Estoque.App.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.Application.Fiscal
{
    public class ProdutosService
    {
        public async Task<FSProdutoModel> GetProductByBarCodeAsync(short pCodEstab, string pBarCode)
        {
            var _IProdutosAPI = DInjection.GetIntance<IProdutosAPI>();
            return await _IProdutosAPI.GetProductByBarCodeAsync(pCodEstab, pBarCode);
        }
        public async Task<IEnumerable<FSSaldoEstoqueModel>> GetProductInventoryBalancesAsync(ESBuscaSaldoEstoqueModel pProduct)
        {
            var _IProdutosAPI = DInjection.GetIntance<IProdutosAPI>();
            return await _IProdutosAPI.GetProductInventoryBalancesAsync(pProduct);
        }

        public async Task<FSSaldoEstoqueModel> GetProductSaldoEstoqueAsync(ESBuscaSaldoEstoqueModel pBuscaSaldo)
        {
            var _ITransferenciaAPI = DInjection.GetIntance<ITransferenciaAPI>();
            return await _ITransferenciaAPI.GetProductSaldoEstoqueAsync(pBuscaSaldo);
        }

        public async Task<bool> PostTransferirAsync(ESCreateProdTransferenciaModel pCreateTranf)
        {
            var _ITransferenciaAPI = DInjection.GetIntance<ITransferenciaAPI>();

            var ret = await _ITransferenciaAPI.PostTransferirAsync(pCreateTranf);

            if (_ITransferenciaAPI.Errors.Count > 0)
                throw new Exception(_ITransferenciaAPI.Errors.GetMessages());

            return ret;
        }

        public async Task<bool> PostTransferirEndAsync(ESCreateProdEndTransfModel pCreateTranf)
        {
            var _ITransferenciaAPI = DInjection.GetIntance<ITransferenciaAPI>();

            var ret = await _ITransferenciaAPI.PostTransferirEndAsync(pCreateTranf);

            if (_ITransferenciaAPI.Errors.Count > 0)
                throw new Exception(_ITransferenciaAPI.Errors.GetMessages());

            return ret;
        }
    }
}
