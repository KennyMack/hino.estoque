﻿using Hino.Estoque.App.Models.Auth;
using Hino.Estoque.App.Services.API.Interfaces.Auth;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.Application.Auth
{
    public class AuthenticationService
    {
        public async Task<GEUsuariosLogadoModel> LoginAsync(GEUsuariosLoginModel pLoginModel)
        {
            var _IAuthAPI = DInjection.GetIntance<IAuthAPI>();
            var ret = await _IAuthAPI.LoginAsync(pLoginModel);

            if (!ret.success)
                return ret;

            ret.success = true;

            CurrentApp.UserLogged = ret;
            CurrentApp.CODESTAB = ret.GEFuncionarios.codestab;

            return ret;
        }

        public async Task<bool> TestServerAsync()
        {
            var _IAuthAPI = DInjection.GetIntance<IAuthAPI>();
            return await _IAuthAPI.TestServerAsync();
        }
    }
}
