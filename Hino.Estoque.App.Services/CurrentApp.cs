﻿using Hino.Estoque.App.Models.Auth;
using Hino.Estoque.App.Utils.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Hino.Estoque.App.Services
{
    public enum TelaOpcao
    {
        Producao = 0,
        Estoque = 1,
        FimTurno = 2
    }

    public static class CurrentApp
    {
        public static void ClearPreferences()
        {
            UserLogged = null;
            aptfimturno = false;
            aptestoque = false;
            aptproducao = false;
        }

        public static bool IsWIFI() =>
            Connectivity.ConnectionProfiles.Any(r => r == ConnectionProfile.WiFi);

        /// <summary>
        /// Cód. Estab.
        /// </summary>
        public static int? CODESTAB
        {
            get
            {
                try
                {
                    return Convert.ToInt32(Preferences.Get("CODESTAB", "0"));
                }
                catch (Exception)
                {
                    return 0;
                }
            }
            set
            {
                if (value == null)
                    Preferences.Remove("CODESTAB");
                else
                    Preferences.Set("CODESTAB", value.ToString());
            }
        }

        public static GEUsuariosLogadoModel UserLogged
        {
            get
            {
                var user = new GEUsuariosLogadoModel
                {
                    GEUsuarios = new Models.Gerais.GEUsuariosModel
                    {
                        codusuario = Preferences.Get("user_codusuario", ""),
                        nome = Preferences.Get("user_nome", ""),
                        tipusuario = Convert.ToByte(Preferences.Get("user_tipusuario", "0")),
                        email = Preferences.Get("user_email", "")
                    },

                    GEFuncionarios = new Models.Gerais.GEFuncionariosModel
                    {
                        codestab = Convert.ToInt16(Preferences.Get("user_codestab", "0")),
                        codfuncionario = Convert.ToDecimal(Preferences.Get("user_codfuncionario", "0")),
                        identificacao = Preferences.Get("user_identificacao", ""),
                        aptfimturno = Preferences.Get("user_aptfimturno", "0") == "1",
                        aptestoque = Preferences.Get("user_aptestoque", "0") == "1",
                        aptproducao = Preferences.Get("user_aptproducao", "0") == "1"
                    }
                };

                user.GESettings.Settings.DefaultOriginStock = Preferences.Get("estabSetting_defaultoriginstock", "");
                user.GESettings.Settings.DefaultStock = Preferences.Get("estabSetting_defaultstock", "");
                user.GESettings.Settings.LoadQtd = Preferences.Get("estabSetting_loadqtd", "0") == "1";
                user.GESettings.Settings.ShowResultsOP = Preferences.Get("estabSetting_showresultsop", "0") == "1";
                user.GESettings.Settings.AskStartNew = Preferences.Get("estabSetting_askstartnew", "0") == "1";
                user.GESettings.Settings.AllowTransfMoreThanOp = Preferences.Get("estabSetting_allowtransfmorethanop", "0") == "1";
                user.GESettings.Settings.KeepTransfLocations = Preferences.Get("estabSetting_keeptransflocations", "0") == "1";
                user.GESettings.Settings.AllowChangeStockOrigin = Preferences.Get("estabSetting_allowchangestockorigin", "0") == "1";
                user.GESettings.Settings.UsePassword = Preferences.Get("estabSetting_usepassword", "0") == "1";
                user.GESettings.Settings.QuantityManual = Preferences.Get("estabSetting_quantitymanual", "0") == "1";
                user.GESettings.Settings.UseStockOriginSuggested = Preferences.Get("estabSetting_usestockoriginsuggested", "0") == "1";
                user.GESettings.Settings.MachineOnStartAppointment = Preferences.Get("estabSetting_machineonstartappointment", "0") == "1";

                return user;
            }
            set
            {
                if (value == null)
                {
                    Preferences.Remove("user_codusuario");
                    Preferences.Remove("user_nome");
                    Preferences.Remove("user_tipusuario");
                    Preferences.Remove("user_email");
                    Preferences.Remove("user_codestab");
                    Preferences.Remove("user_codfuncionario");
                    Preferences.Remove("user_identificacao");
                    Preferences.Remove("user_aptfimturno");
                    Preferences.Remove("user_aptestoque");
                    Preferences.Remove("user_aptproducao");

                    Preferences.Remove("estabSetting_defaultoriginstock");
                    Preferences.Remove("estabSetting_defaultstock");
                    Preferences.Remove("estabSetting_loadqtd");
                    Preferences.Remove("estabSetting_showresultsop");
                    Preferences.Remove("estabSetting_askstartnew");
                    Preferences.Remove("estabSetting_allowtransfmorethanop");
                    Preferences.Remove("estabSetting_keeptransflocations");
                    Preferences.Remove("estabSetting_allowchangestockorigin");
                    Preferences.Remove("estabSetting_usepassword");
                    Preferences.Remove("estabSetting_quantitymanual");
                    Preferences.Remove("estabSetting_usestockoriginsuggested");
                    Preferences.Remove("estabSetting_machineonstartappointment");

                    Preferences.Remove("location_laststockorigin");
                    Preferences.Remove("location_laststockdestiny");
                    
                    // Preferences.Remove("user_status");
                    // Preferences.Remove("user_codprocesso");
                }
                else
                {
                    Preferences.Set("user_codusuario", value.GEUsuarios.codusuario);
                    Preferences.Set("user_nome", value.GEUsuarios.nome);
                    Preferences.Set("user_tipusuario", value.GEUsuarios.tipusuario.ToString());
                    Preferences.Set("user_email", value.GEUsuarios.email);
                    Preferences.Set("user_codestab", value.GEFuncionarios.codestab.ToString());
                    Preferences.Set("user_codfuncionario", value.GEFuncionarios.codfuncionario.ToString());
                    Preferences.Set("user_identificacao", value.GEFuncionarios.identificacao);

                    Preferences.Set("user_aptfimturno", value.GEFuncionarios.aptfimturno ? "1" : "0");
                    Preferences.Set("user_aptestoque", value.GEFuncionarios.aptestoque ? "1" : "0");
                    Preferences.Set("user_aptproducao", value.GEFuncionarios.aptproducao ? "1" : "0");


                    Preferences.Set("estabSetting_defaultoriginstock", value.GESettings.Settings.DefaultOriginStock);
                    Preferences.Set("estabSetting_defaultstock", value.GESettings.Settings.DefaultStock);
                    Preferences.Set("estabSetting_loadqtd", value.GESettings.Settings.LoadQtd ? "1" : "0");
                    Preferences.Set("estabSetting_showresultsop", value.GESettings.Settings.ShowResultsOP ? "1" : "0");
                    Preferences.Set("estabSetting_askstartnew", value.GESettings.Settings.AskStartNew ? "1" : "0");
                    Preferences.Set("estabSetting_allowtransfmorethanop", value.GESettings.Settings.AllowTransfMoreThanOp ? "1" : "0");
                    Preferences.Set("estabSetting_keeptransflocations", value.GESettings.Settings.KeepTransfLocations ? "1" : "0");
                    Preferences.Set("estabSetting_allowchangestockorigin", value.GESettings.Settings.AllowChangeStockOrigin ? "1" : "0");
                    Preferences.Set("estabSetting_usepassword", value.GESettings.Settings.UsePassword ? "1" : "0");
                    Preferences.Set("estabSetting_quantitymanual", value.GESettings.Settings.QuantityManual ? "1" : "0");
                    Preferences.Set("estabSetting_usestockoriginsuggested", value.GESettings.Settings.UseStockOriginSuggested ? "1" : "0");
                    Preferences.Set("estabSetting_machineonstartappointment", value.GESettings.Settings.MachineOnStartAppointment ? "1" : "0");
                }
            }
        }

        public static string IPBASE => Preferences.Get("IPBASE", "");

        public static bool Resume
        {
            get;
            set;
        }

        public static string LastStockOrigin
        {
            get => Preferences.Get("location_laststockorigin", "");
            set => Preferences.Set("location_laststockorigin", value);
        }

        public static string LastStockDestiny
        {
            get => Preferences.Get("location_laststockdestiny", "");
            set => Preferences.Set("location_laststockdestiny", value);
        }

        /// <summary>
        /// BASE URL
        /// </summary>
        public static string BASEURL
        {
            get
            {
                try
                {
                    return Preferences.Get("BASEURL", "http://192.168.1.1/Hino.Estoque.API/api");
                }
                catch (Exception)
                {
                    return "http://192.168.1.1/Hino.Estoque.API/api";
                }
            }
            set
            {
                if (value == null)
                {
                    Preferences.Remove("BASEURL");
                    Preferences.Remove("IPBASE");
                }
                else
                {
                    var Address = value;
                    if (Address.Contains("http://") || Address.Contains("https://"))
                        Address = Address.Replace("http://", "").Replace("https://", "");

                    Preferences.Set("IPBASE", Address);

                    Address = !Address.EndsWith("/") ? $"{Address}/Hino.Estoque.API/api" : $"{Address}Hino.Estoque.API/api";

                    Preferences.Set("BASEURL", Address);
                }
            }
        }

        public static bool aptfimturno { get; set; }
        public static bool aptestoque { get; set; }
        public static bool aptproducao { get; set; }

        public static void LoadPermissions()
        {
            aptfimturno = false;
            aptestoque = false;
            aptproducao = false;
            if (UserLogged?.GEFuncionarios != null)
            {
                var funcionario = UserLogged.GEFuncionarios;
                aptfimturno = funcionario.aptfimturno;
                aptestoque = funcionario.aptestoque;
                aptproducao = funcionario.aptproducao;
            }
        }

        public static bool ValidatePermission(TelaOpcao pTela)
        {
            var instance = DependencyService.Get<IMessage>();
            switch (pTela)
            {
                case TelaOpcao.Producao:
                    if (!aptproducao)
                    {
                        instance.ShowShortMessage(Resources.ValidationMessagesResource.UserDontHavePermission);
                        return false;
                    }
                    break;
                case TelaOpcao.Estoque:
                    if (!aptestoque)
                    {
                        instance.ShowShortMessage(Resources.ValidationMessagesResource.UserDontHavePermission);
                        return false;
                    }
                    break;
                case TelaOpcao.FimTurno:
                    if (!aptfimturno)
                    {
                        instance.ShowShortMessage(Resources.ValidationMessagesResource.UserDontHavePermission);
                        return false;
                    }
                    break;
                default:
                    break;
            }
            return true;
        }
    }
}
