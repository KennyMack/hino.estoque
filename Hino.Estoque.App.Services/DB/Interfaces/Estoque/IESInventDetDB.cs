﻿using Hino.Estoque.App.Models.Estoque;
using Hino.Estoque.App.Utils.Paging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.DB.Interfaces.Estoque
{
    public interface IESInventDetDB : IBaseDataStore<ESInventDetModel>
    {
        Task<IEnumerable<ESInventDetModel>> GetAllInventAlterados(short pCodEstab, decimal pCodInv, decimal pContagem);
        Task<ESInventDetModel> GetAnyRegister(short pCodEstab, decimal pCodInv, decimal pContagem);
        Task<bool> DeleteAllInventItems(short pCodEstab, decimal pCodInv, decimal pContagem);
        Task<IEnumerable<ESInventDetModel>> GetAllInventItems(short pCodEstab, decimal pCodInv, decimal pContagem);
        Task<PagedResult<ESInventDetModel>> SearchInvItems(short pCodEstab, decimal pCodInv, decimal pContagem, string pSearchData, int page);
    }
}
