﻿using Hino.Estoque.App.Models;
using Hino.Estoque.App.Utils.Paging;
using SQLite;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.DB.Interfaces
{
    public interface IBaseDataStore<T> where T : BaseEntity, new()
    {
        Task<long> NextSequence();
        Task AddFromListResult(IEnumerable<T> result);
        Task AddFromPagedResult(PagedResult<T> result);
        Task<T> AddOrUpdateItemAsync(T item);
        Task<T> AddItemAsync(T item);
        Task<T> UpdateItemAsync(T item);
        Task<bool> DeleteItemAsync(long id);
        Task<T> GetItemAsync(long id);
        Task<int> HasDataAsync();
        Task<PagedResult<T>> GetItemsAsync(int page);
        Task<PagedResult<T>> Query(System.Linq.Expressions.Expression<Func<T, bool>> predicate, int page);
        Task<PagedResult<T>> Search(string pSearchData, int page);
        void ClearTable();

    }
}
