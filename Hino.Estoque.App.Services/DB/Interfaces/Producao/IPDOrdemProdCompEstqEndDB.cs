﻿using Hino.Estoque.App.Models.Producao;
using Hino.Estoque.App.Utils.Paging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.DB.Interfaces.Producao
{
    public interface IPDOrdemProdCompEstqEndDB : IBaseDataStore<PDOrdemProdCompEstqEndModel>
    {
        Task<PagedResult<PDOrdemProdCompEstqEndModel>> GetAllCompStockEndAsync(short pCodEstab, decimal pCodOrdProd, string pNivelOrdProd);
    }
}
