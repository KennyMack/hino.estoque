﻿using Hino.Estoque.App.Models.Producao;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.DB.Interfaces.Producao
{
    public interface IPDOPCreateTransfDB : IBaseDataStore<PDOPCreateTransfModel>
    {
        Task<PDOPCreateTransfModel[]> SaveTransfAsync(PDOPCreateTransfModel[] lstTransf);
        Task ClearTransfAsync(decimal pCodOrdProd, string pNivelOrdProd);
        Task UpdateTransfResultAsync(PDOPCreateTransfModel[] plstTransf, OPTransfResult pTransfResult);
        // Task UpdateTransfResultAsync(PDOPCreateTransfModel[] plstTransf, OPTransfResult pTransfResult, string pMessage);
    }
}
