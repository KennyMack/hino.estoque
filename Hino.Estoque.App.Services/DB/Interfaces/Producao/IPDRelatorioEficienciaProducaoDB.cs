﻿using Hino.Estoque.App.Models.Producao;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.DB.Interfaces.Producao
{
    public interface IPDRelatorioEficienciaProducaoDB : IBaseDataStore<PDRelatorioEficienciaProducaoModel>
    {
        Task<int> DeleteAllAsync();
        Task SaveAllAsync(IEnumerable<PDRelatorioEficienciaProducaoModel> pRelatorio);
    }
}
