﻿using Hino.Estoque.App.Models.Producao;
using Hino.Estoque.App.Utils.Paging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.DB.Interfaces.Producao
{
    public interface IPDOPCreateTransfItemDB : IBaseDataStore<PDOPCreateTransfItemModel>
    {
        Task<PDOPCreateTransfItemModel> SaveTransfItemAsync(PDOPCreateTransfItemModel lstTransf);
        Task<PagedResult<PDOPCreateTransfItemModel>> GetAllCompTransfAsync(short pCodEstab, decimal pCodOrdProd, string pNivelOrdProd);
    }
}
