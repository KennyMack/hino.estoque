﻿using Hino.Estoque.App.Models.Producao;
using Hino.Estoque.App.Utils.Paging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.DB.Interfaces.Producao
{
    public interface IPDOrdemProdCompDB : IBaseDataStore<PDOrdemProdCompModel>
    {
        Task<PagedResult<PDOrdemProdCompModel>> GetAllComponentsAsync(short pCodEstab, decimal pCodOrdProd, string pNivelOrdProd);
        Task<PagedResult<PDOrdemProdCompModel>> SearchOPComponentsAsync(short pCodEstab, decimal pCodOrdProd, string pNivelOrdProd, string pSearchData, int page);
        Task<PDOrdemProdCompModel> GetAnyRegisterAsync(short pCodEstab, decimal pCodOrdProd, string pNivelOrdProd);
        Task<IEnumerable<PDOrdemProdCompModel>> GetAllComponentsAlteradosAsync(short pCodEstab, decimal pCodOrdProd, string pNivelOrdProd);
    }
}
