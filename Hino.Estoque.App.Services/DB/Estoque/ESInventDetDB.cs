﻿using System.Threading.Tasks;
using Hino.Estoque.App.Models.Estoque;
using Hino.Estoque.App.Services.DB.Interfaces.Estoque;
using System.Linq;
using System.Collections.Generic;
using Hino.Estoque.App.Utils.Paging;

namespace Hino.Estoque.App.Services.DB.Estoque
{
    public class ESInventDetDB : BaseDataStore<ESInventDetModel>, IESInventDetDB
    {
        public async Task<bool> DeleteAllInventItems(short pCodEstab, decimal pCodInv, decimal pContagem)
        {
            var results = await GetAllInventItems(pCodEstab, pCodInv, pContagem);

            foreach (var item in results)
            {
                await DeleteItemAsync(item.IdMobile);
            }

            return true;
        }

        public async Task<IEnumerable<ESInventDetModel>> GetAllInventAlterados(short pCodEstab, decimal pCodInv, decimal pContagem) =>
            (await Query(r => r.codestab == pCodEstab &&
                             r.codinv == pCodInv &&
                             r.contagem == pContagem &&
                             r.alterado == true &&
                             r.saldooriginal != r.saldoinvent, -1)).Results;

        public async Task<IEnumerable<ESInventDetModel>> GetAllInventItems(short pCodEstab, decimal pCodInv, decimal pContagem) =>
            (await Query(r => r.codestab == pCodEstab &&
                             r.codinv == pCodInv &&
                             r.contagem == pContagem, -1)).Results;

        public async Task<ESInventDetModel> GetAnyRegister(short pCodEstab, decimal pCodInv, decimal pContagem) =>
             (await Query(r => r.codestab == pCodEstab && 
                          r.codinv == pCodInv &&
                          r.contagem == pContagem, 1)).Results.FirstOrDefault();

        public async Task<PagedResult<ESInventDetModel>> SearchInvItems(short pCodEstab, decimal pCodInv, decimal pContagem, string pSearchData, int page)
        {
            if (!string.IsNullOrEmpty(pSearchData))
            {
                var filterValue = pSearchData.Trim().ToLower();

                return await Query(r =>
                    r.codestab == pCodEstab &&
                    r.codinv == pCodInv &&
                    r.contagem == pContagem &&
                    (r.codproduto.ToLower().Equals(filterValue) ||
                     r.FSProduto_GTIN.Equals(filterValue) ||
                     r.FSProduto_GTINEmbalagem.Equals(filterValue)
                    ), page);
            }
            else
                return await Query(r =>
                    r.codestab == pCodEstab &&
                    r.codinv == pCodInv &&
                    r.contagem == pContagem, page);
        }

        public override async Task AddFromListResult(IEnumerable<ESInventDetModel> result)
        {
            foreach (var item in result)
            {
                item.FSProduto_descricao = item.FSProduto.descricao;
                item.saldooriginal = item.saldoinvent;
                item.alterado = false;

                var ProdParam = item.FSProduto.FSProdutoparamEstab.First();
                var ProdPCP = item.FSProduto.FSProdutoPcp.First();

                var Endereco = ProdParam?.Enderecamento;
                if (Endereco != null)
                {
                    item.ESEnderecamento_codenderecamento = Endereco.codendestoque;
                    item.ESEnderecamento_descricao = Endereco.descricao;
                    item.ESEnderecamento_sigla = Endereco.sigla;
                    item.ESEnderecamento_rua = Endereco.rua;
                    item.ESEnderecamento_nivel = Endereco.nivel;
                    item.ESEnderecamento_posicao = Endereco.posicao;
                }

                item.FSProduto_GTIN = ProdPCP.codgtin;
                item.FSProduto_GTINEmbalagem = ProdPCP.codgtinemb;

                await this.AddItemAsync(item);
            }
        }
    }
}
