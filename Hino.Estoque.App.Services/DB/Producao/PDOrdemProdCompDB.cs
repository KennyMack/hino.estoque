﻿using Hino.Estoque.App.Models.Producao;
using Hino.Estoque.App.Services.DB.Interfaces.Producao;
using Hino.Estoque.App.Utils.Paging;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.DB.Producao
{
    public class PDOrdemProdCompDB : BaseDataStore<PDOrdemProdCompModel>, IPDOrdemProdCompDB
    {
        public async Task<PDOrdemProdCompModel> GetAnyRegisterAsync(short pCodEstab, decimal pCodOrdProd, string pNivelOrdProd) =>
             (await Query(r => r.codestab == pCodEstab &&
                               r.codordprod == pCodOrdProd &&
                               r.nivelordprod == pNivelOrdProd, 1)).Results.FirstOrDefault();

        public async Task<PagedResult<PDOrdemProdCompModel>> GetAllComponentsAsync(short pCodEstab, decimal pCodOrdProd, string pNivelOrdProd) =>
            await Query(r => r.codestab == pCodEstab &&
                              r.codordprod == pCodOrdProd &&
                              r.nivelordprod == pNivelOrdProd, -1);

        public async Task<PagedResult<PDOrdemProdCompModel>> SearchOPComponentsAsync(short pCodEstab, decimal pCodOrdProd, string pNivelOrdProd, string pSearchData, int page)
        {
            if (!string.IsNullOrEmpty(pSearchData))
            {
                var filterValue = pSearchData.Trim().ToLower();

                return await Query(r =>
                    r.codestab == pCodEstab &&
                    r.codordprod == pCodOrdProd &&
                    r.nivelordprod == pNivelOrdProd &&
                    r.separacao && 
                    (r.codcomponente.ToLower().Equals(filterValue) ||
                     r.FSProduto_GTIN.Equals(filterValue) ||
                     r.FSProduto_GTINEmbalagem.Equals(filterValue)
                    ), page);
            }
            else
                return await Query(r =>
                    r.codestab == pCodEstab &&
                    r.codordprod == pCodOrdProd &&
                    r.nivelordprod == pNivelOrdProd &&
                    r.separacao, page);
        }

        public override async Task AddFromListResult(IEnumerable<PDOrdemProdCompModel> result)
        {
            foreach (var item in result)
            {
                item.FSProduto_descricao = item.fsproduto.descricao;

                var ProdParam = item.fsproduto.FSProdutoparamEstab.First();
                var ProdPCP = item.fsproduto.FSProdutoPcp.First();

                var Endereco = ProdParam?.Enderecamento;
                if (Endereco != null)
                {
                    item.ESEnderecamento_codenderecamento = Endereco.codendestoque;
                    item.ESEnderecamento_descricao = Endereco.descricao;
                    item.ESEnderecamento_sigla = Endereco.sigla;
                    item.ESEnderecamento_rua = Endereco.rua;
                    item.ESEnderecamento_nivel = Endereco.nivel;
                    item.ESEnderecamento_posicao = Endereco.posicao;
                }

                item.FSProduto_GTIN = ProdPCP.codgtin;
                item.FSProduto_GTINEmbalagem = ProdPCP.codgtinemb;
                item.codestoqueori = ProdParam.codestoque;
                item.FSProduto_separacaomaximo = ProdPCP.separacaomaximo;
                //item.saldoorigem = ;
                item.codestoquedest = item.codestoque;
                //item.saldodestino = 0;
                item.PDOPTransfQtdSep = item.pdoptransf.Where(r => r.status == 1).Sum(r => r.quantidade);
                item.qtdSeparada = (item.loadqtd) ? item.quantidade : 0;
                item.separado = item.loadqtd;
                item.Estoque_Enderecos = item.OriginLocations;

                await this.AddItemAsync(item);
            }
        }

        public async Task<IEnumerable<PDOrdemProdCompModel>> GetAllComponentsAlteradosAsync(short pCodEstab, decimal pCodOrdProd, string pNivelOrdProd) =>
            (await Query(r => r.codestab == pCodEstab &&
                              r.codordprod == pCodOrdProd &&
                              r.nivelordprod == pNivelOrdProd &&
                              r.separado == true &&
                              r.qtdSeparada > 0, -1)).Results;
    }
}
