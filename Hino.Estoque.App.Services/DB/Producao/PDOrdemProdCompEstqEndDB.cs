﻿using Hino.Estoque.App.Models.Producao;
using Hino.Estoque.App.Services.DB.Interfaces.Producao;
using Hino.Estoque.App.Utils.Paging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.DB.Producao
{
    public class PDOrdemProdCompEstqEndDB : BaseDataStore<PDOrdemProdCompEstqEndModel>, IPDOrdemProdCompEstqEndDB
    {
        public async Task<PagedResult<PDOrdemProdCompEstqEndModel>> GetAllCompStockEndAsync(short pCodEstab, decimal pCodOrdProd, string pNivelOrdProd) =>
            await Query(r => r.codestab == pCodEstab &&
                              r.codordprod == pCodOrdProd &&
                              r.nivelordprod == pNivelOrdProd, -1);
    }
}
