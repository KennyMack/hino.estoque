﻿using Hino.Estoque.App.Models.Producao;
using Hino.Estoque.App.Services.DB.Interfaces.Producao;
using Hino.Estoque.App.Utils.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.DB.Producao
{
    public class PDOPCreateTransfItemDB : BaseDataStore<PDOPCreateTransfItemModel>, IPDOPCreateTransfItemDB
    {
        public async Task<PagedResult<PDOPCreateTransfItemModel>> GetAllCompTransfAsync(short pCodEstab, decimal pCodOrdProd, string pNivelOrdProd) =>
            await Query(r => r.codestab == pCodEstab &&
                              r.codordprod == pCodOrdProd &&
                              r.nivelordprod == pNivelOrdProd, -1);

        public async Task<PDOPCreateTransfItemModel> SaveTransfItemAsync(PDOPCreateTransfItemModel pItem)
        {
            var Exists = (await Query(r => r.Identificador == pItem.Identificador, -1)).Results;

            if (!Exists.Any())
            {
                pItem.datatransf = DateTime.Now;
                return await AddItemAsync(pItem);
            }
            else
            {
                var updateItem = await GetItemAsync(Exists.FirstOrDefault().IdMobile);

                updateItem.qtdseparada = pItem.qtdseparada;

                return await UpdateItemAsync(updateItem);
            }
        }
    }
}
