﻿using Hino.Estoque.App.Models.Producao;
using Hino.Estoque.App.Services.DB.Interfaces.Producao;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.DB.Producao
{
    public class PDRelatorioEficienciaProducaoDB : BaseDataStore<PDRelatorioEficienciaProducaoModel>, IPDRelatorioEficienciaProducaoDB
    {
        public async Task<int> DeleteAllAsync() =>
            await _DataBase.DeleteAllAsync<PDRelatorioEficienciaProducaoModel>();

        public async Task SaveAllAsync(IEnumerable<PDRelatorioEficienciaProducaoModel> pRelatorio)
        {
            await _DataBase.InsertAllAsync(pRelatorio);
        }
    }
}
