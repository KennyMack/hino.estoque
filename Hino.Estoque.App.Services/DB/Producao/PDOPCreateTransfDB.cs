﻿using Hino.Estoque.App.Models.Producao;
using Hino.Estoque.App.Services.DB.Interfaces.Producao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.DB.Producao
{
    public class PDOPCreateTransfDB : BaseDataStore<PDOPCreateTransfModel>, IPDOPCreateTransfDB
    {
        public async Task ClearTransfAsync(decimal pCodOrdProd, string pNivelOrdProd)
        {
            var Items = (await Query(r => r.codordprod == pCodOrdProd && r.nivelordprod == pNivelOrdProd, -1)).Results;
            foreach (var item in Items)
                await DeleteItemAsync(item.IdMobile);
        }

        public async Task<PDOPCreateTransfModel[]> SaveTransfAsync(PDOPCreateTransfModel[] lstTransf)
        {
            foreach (var item in lstTransf)
            {
                var Exists = (await Query(r => r.Identificador == item.Identificador, -1)).Results;

                if (!Exists.Any())
                {
                    item.datatransf = DateTime.Now;
                    await this.AddItemAsync(item);
                }
                else
                {
                    var updateItem = await GetItemAsync(Exists.FirstOrDefault().IdMobile);

                    updateItem.quantidade = item.quantidade;
                    updateItem.qtdseparada = item.qtdseparada;
                    updateItem.complete = false;

                    await UpdateItemAsync(updateItem);
                }

            }
            return lstTransf;
        }

        public async Task UpdateTransfResultAsync(PDOPCreateTransfModel[] plstTransf, OPTransfResult pTransfResult)
        {
            try
            {
                if (plstTransf.Any())
                {
                    var Results = pTransfResult?.TransfResults?.Transferencias;

                    foreach (var item in plstTransf)
                    {
                        var Exists = (await Query(r => r.Identificador == item.Identificador, -1)).Results;
                        if (Exists.Any())
                        {
                            var observacao = pTransfResult.Result;
                            var complete = pTransfResult.Success;
                            PDOPTransfItemResultModel modelResult = null;
                            if (Results.Any())
                            {
                                modelResult = Results.FirstOrDefault(r => r.Transferencia.codestab == item.codestab &&
                                         r.Transferencia.codordprod == item.codordprod &&
                                         r.Transferencia.nivelordprod == item.nivelordprod &&
                                         r.Transferencia.codcomponente == item.codcomponente &&
                                         r.Transferencia.codestrutura == item.codestrutura &&
                                         r.Transferencia.codroteiro == item.codroteiro &&
                                         r.Transferencia.codestoqueori == item.codestoqueori &&
                                         r.Transferencia.codestoquedest == item.codestoquedest &&
                                         r.Transferencia.operacao == item.operacao
                                     );
                                observacao = modelResult.Motivo;
                                complete = modelResult.Sucesso;
                            }
                            var updateItem = await GetItemAsync(Exists.FirstOrDefault().IdMobile);

                            updateItem.observacao = observacao;
                            updateItem.complete = complete;

                            await UpdateItemAsync(updateItem);
                        }
                    }
                }
            }
            catch (Exception)
            {

            }

            /*
            if (pTransfResult.lstPDOPTransf.Any())
            {
                var first = pTransfResult.lstPDOPTransf.First();

                var Items = (await Query(r => r.codordprod == first.codordprod && r.nivelordprod == first.nivelordprod, -1)).Results;

                foreach (var item in Items)
                {
                    var updateItem = await GetItemAsync(item.IdMobile);

                    updateItem.observacao = pTransfResult.Result;
                    updateItem.complete = pTransfResult.Success;

                    await UpdateItemAsync(updateItem);
                }
            }*/
        }

        //public async Task UpdateTransfResultAsync(PDOPCreateTransfModel[] plstTransf, OPTransfResult pTransfResult, string pMessage)
        //{
        //    try
        //    {

        //        if (plstTransf.Any())
        //        {
        //            var Results = pTransfResult.lstPDOPTransf;

        //            foreach (var item in plstTransf)
        //            {
        //                var Exists = (await Query(r => r.Identificador == item.Identificador, -1)).Results;

        //                if (Exists.Any())
        //                {
        //                    var updateItem = await GetItemAsync(Exists.FirstOrDefault().IdMobile);

        //                    updateItem.observacao = pMessage;

        //                    await UpdateItemAsync(updateItem);
        //                }
        //                /* }
        //                 else
        //                 {
        //                     var result = Results.Where(r => r.codestab == item.codestab &&
        //                         r.codordprod == item.codordprod &&
        //                         r.nivelordprod == item.nivelordprod &&
        //                         r.codcomponente == item.codcomponente &&
        //                         r.codestrutura == item.codestrutura &&
        //                         r.codroteiro == item.codroteiro &&
        //                         r.codestoqueori == item.codestoqueori &&
        //                         r.codestoquedest == item.codestoquedest &&
        //                         r.operacao == item.operacao
        //                     ).FirstOrDefault();

        //                     if (result != null)
        //                     {
        //                         var Exists = (await Query(r => r.Identificador == item.Identificador, -1)).Results;

        //                         if (Exists.Any())
        //                         {
        //                             var updateItem = await GetItemAsync(Exists.FirstOrDefault().IdMobile);

        //                             updateItem.observacao = pMessage;

        //                             await UpdateItemAsync(updateItem);
        //                         }
        //                     }
        //                 }*/
        //            }
        //        }
        //    }
        //    catch (Exception)
        //    {

        //    }
        //    /*
        //    var Items = (await Query(r => r.codordprod == pCodOrdProd && r.nivelordprod == pNivelOrdProd, -1)).Results;

        //    foreach (var item in Items)
        //    {
        //        var updateItem = await GetItemAsync(item.IdMobile);

        //        updateItem.observacao = pMessage;

        //        await UpdateItemAsync(updateItem);
        //    }*/
        //}
    }
}
