﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Estoque.App.Services.DB
{
    public interface IDBSQLite
    {
        string DataBasePath { get; }
    }
}
