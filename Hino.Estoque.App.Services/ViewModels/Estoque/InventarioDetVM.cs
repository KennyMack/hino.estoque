﻿using Hino.Estoque.App.Models.Estoque;
using Hino.Estoque.App.Services.Application.Estoque;
using Hino.Estoque.App.Utils.Extensions;
using Hino.Estoque.App.Utils.Messages;
using Hino.Estoque.App.Utils.Paging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using XF.Material.Forms.UI.Dialogs;

namespace Hino.Estoque.App.Services.ViewModels.Estoque
{
    public class InventarioDetVM : BaseViewModel
    {
        public ObservableCollection<ESInventDetModel> Items { get; set; }
        private readonly InventarioService _InventarioService;
        private readonly ESInventarioModel _ESInventario;
        public event EventHandler GoToList;

        public InventarioDetVM(ESInventarioModel pESInventario)
        {
            _ESInventario = pESInventario;
            Title = $"Inventário {pESInventario.codinv}/{pESInventario.contagem}";
            Items = new ObservableCollection<ESInventDetModel>();
            _InventarioService = new InventarioService();
            base.PageNum = 1;
        }

        private int _BarCodeHeight = 0;
        public int BarCodeHeight
        {
            get => BarCodeHeight;
            set
            {
                SetProperty(ref _BarCodeHeight, value);
            }
        }

        #region Get Data From Source
        private async Task<PagedResult<ESInventDetModel>> GetDataFromSourceAsync() =>
            await _InventarioService.Search(_ESInventario.codinv, _ESInventario.contagem, _SearchData, base.PageNum);
        #endregion

        #region Search Data
        public async override Task SearchData_Changed()
        {
            base.PageNum = 1;

            Items.CopyFromPagedResult(await GetDataFromSourceAsync());
        }
        #endregion

        protected async override Task LoadMoreData()
        {
            IsBusy = true;

            try
            {
                await _InventarioService.LoadInventDetItems(_ESInventario.codinv, _ESInventario.contagem);

                Items.CopyFromPagedResult(await GetDataFromSourceAsync(), false);
                IsBusy = false;
            }
            catch (Exception e)
            {
                ShowShortMessage(e.Message);

                IsBusy = false;
            }
            finally
            {
                IsBusy = false;
            }
        }

        protected async override Task RefreshData()
        {
            await base.RefreshData();

            IsBusy = true;

            base.PageNum = 1;
            Items.Clear();

            await LoadMoreData();
        }

        public async Task SaveValueAsync(InventDetResult pInventDetResult)
        {
            if (IsBusy)
                return;

            IsBusy = true;
            var IsOk = false;
            try
            {
                await _InventarioService.SaveValueAsync(pInventDetResult);
                IsOk = true;
            }
            catch (Exception e)
            {
                ShowShortMessage(e.Message);

                IsBusy = false;
            }
            finally
            {
                IsBusy = false;
                if (IsOk)
                    await SyncValueAsync(false);
            }
        }

        public void ConfirmReload() =>
            OnMessageConfirmRaise(new MessageRaiseEvent(Resources.ValidationMessagesResource.Confirm, Resources.ValidationMessagesResource.ConfirmReloadInvent));

        public async Task ReloadData()
        {
            if (IsBusy)
                return;

            IsBusy = true;
            using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
            {
                try
                {
                    var ret = await _InventarioService.ReloadData(_ESInventario.codinv, _ESInventario.contagem);
                    
                    if (ret)
                        ShowShortMessage(Resources.ValidationMessagesResource.SendWithSuccess);
                }
                catch (Exception e)
                {
                    ShowShortMessage(e.Message);

                    IsBusy = false;
                }
                finally
                {
                    IsBusy = false;
                    await RefreshData();
                }
            }
        }

        public void ConfirmSave() =>
            OnMessageConfirmRaise(new MessageRaiseEvent(Resources.ValidationMessagesResource.ConfirmTyping, Resources.ValidationMessagesResource.Confirm));

        public async Task SyncValueAsync(bool goBack)
        {
            if (IsBusy)
                return;

            IsBusy = true;
            using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
            {
                try
                {
                    var results = await _InventarioService.GetAllInventAlterados(_ESInventario.codinv, _ESInventario.contagem);

                    var lstResults = new List<ESUpdateInventDetModel>();

                    foreach (var item in results)
                    {
                        var InventDet = new ESUpdateInventDetModel();
                        InventDet.CopyProperties(item);

                        lstResults.Add(InventDet);
                    }

                    var ret = await _InventarioService.PostContagemAsync(_ESInventario.codinv.ToString(), _ESInventario.contagem.ToString(), lstResults.ToArray());
                    if (ret && goBack)
                    {
                        ShowShortMessage(Resources.ValidationMessagesResource.SendWithSuccess);
                        GoToList?.Invoke(this, null);
                    }
                }
                catch (Exception e)
                {
                    ShowShortMessage(e.Message);

                    IsBusy = false;
                }
                finally
                {
                    IsBusy = false;
                }
            }
        }
    }
}
