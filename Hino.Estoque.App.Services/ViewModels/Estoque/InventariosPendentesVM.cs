﻿using Hino.Estoque.App.Models.Estoque;
using Hino.Estoque.App.Services.Application.Estoque;
using Hino.Estoque.App.Utils.Messages;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.ViewModels.Estoque
{
    public class InventariosPendentesVM : BaseViewModel
    {
        public ObservableCollection<ESInventarioModel> Items { get; set; }
        private readonly InventarioService _InventarioService;

        public InventariosPendentesVM()
        {
            Title = "Listas Pendentes";
            Items = new ObservableCollection<ESInventarioModel>();
            _InventarioService = new InventarioService();
            base.PageNum = 1;
        }

        #region Get Data From Source
        private async Task<IEnumerable<ESInventarioModel>> GetDataFromSourceAsync()
        {
            return await _InventarioService.GetPendentesAsync();
        }
        #endregion

        protected async override Task LoadMoreData()
        {
            IsBusy = true;

            try
            {
                Items.CopyFromList(await GetDataFromSourceAsync(), false);
                IsBusy = false;
            }
            catch (Exception e)
            {
                ShowShortMessage(e.Message);

                IsBusy = false;
            }
            finally
            {
                IsBusy = false;
            }
        }

        protected async override Task RefreshData()
        {
            await base.RefreshData();
            
            IsBusy = true;

            base.PageNum = 1;
            Items.Clear();

            await LoadMoreData();
        }

    }
}
