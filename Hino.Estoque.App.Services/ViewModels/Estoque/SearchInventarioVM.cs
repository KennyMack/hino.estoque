﻿using Hino.Estoque.App.Models.Estoque;
using Hino.Estoque.App.Models.Fiscal;
using Hino.Estoque.App.Services.Application.Estoque;
using Hino.Estoque.App.Services.Application.Fiscal;
using Hino.Estoque.App.Services.ViewModels.Fiscal;
using Hino.Estoque.App.Utils.Extensions;
using Hino.Estoque.App.Utils.Messages;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using XF.Material.Forms.UI.Dialogs;

namespace Hino.Estoque.App.Services.ViewModels.Estoque
{
    public class SearchInventarioVM : BaseViewModel
    {
        public event EventHandler evtTextFocused;
        public event EventHandler SaveSuccess;
        private ESInventarioModel _ESInventarioModel;
        private FSProdutoModel _FSProdutoModel;

        public string LBLINVENTPLACEHOLDER => Resources.FieldsNameResource.InventIdentifier;
        public string LBLINVENTHELPERTEXT => Resources.FieldsNameResource.InventIdentifier;
        public string LBLINVENTERRORTEXT => "Informe o inventário";

        public string LBLPRODUCTPLACEHOLDER => Resources.FieldsNameResource.ProductKey;
        string _LBLPRODUCTHELPERTEXT = Resources.FieldsNameResource.ProductKey;
        public string LBLPRODUCTHELPERTEXT
        {
            get => _LBLPRODUCTHELPERTEXT;
            set => SetProperty(ref _LBLPRODUCTHELPERTEXT, value);
        }
        public string LBLPRODUCTERRORTEXT => "Informe o produto";

        public string LBLQUANTITYPLACEHOLDER => Resources.FieldsNameResource.Quantity;
        public string LBLQUANTITYHELPERTEXT => Resources.FieldsNameResource.Quantity;
        public string LBLQUANTITYERRORTEXT => "Informe a quantidade";

        public string LBLLOTEPLACEHOLDER => Resources.FieldsNameResource.Lote;
        public string LBLLOTEHELPERTEXT => Resources.FieldsNameResource.Lote;
        public string LBLLOTEERRORTEXT => "Informe o lote";

        public string LBLSTOCKPLACEHOLDER => Resources.FieldsNameResource.StockOrigin;
        public string _LBLSTOCKHELPERTEXT = Resources.FieldsNameResource.StockOrigin;
        public string LBLSTOCKHELPERTEXT
        {
            get => _LBLSTOCKHELPERTEXT;
            set => SetProperty(ref _LBLSTOCKHELPERTEXT, value);
        }
        public string LBLSTOCKERRORTEXT => "Informe o estoque";

        public ICommand CmdGetInventarioClick { protected set; get; }
        public ICommand CmdSaveCountClick { protected set; get; }
        public ICommand CmdInformPackagesClick { protected set; get; }

        public string PRODUCTBARCODE { set; get; }

        public bool _HasProductError = false;
        public bool HASPRODUCTERROR
        {
            get => _HasProductError;
            set => SetProperty(ref _HasProductError, value);
        }

        public bool _HasStockError = false;
        public bool HASSTOCKERROR
        {
            get => _HasStockError;
            set => SetProperty(ref _HasStockError, value);
        }

        public string _Product;
        public string PRODUCT
        {
            get => _Product;
            set => SetProperty(ref _Product, value?.ToUpperInvariant());
        }

        public string _Stock;
        public string STOCK
        {
            get => _Stock;
            set => SetProperty(ref _Stock, value?.ToUpperInvariant());
        }

        public bool _HasQuantityError = false;
        public bool HASQUANTITYERROR
        {
            get => _HasQuantityError;
            set => SetProperty(ref _HasQuantityError, value);
        }

        public double _Quantity = 0;
        public double QUANTITY
        {
            get => _Quantity;
            set
            {
                SetProperty(ref _Quantity, value);
            }
        }

        public double _PackagesQuantity = 1;
        public double PACKAGESQUANTITY
        {
            get => _PackagesQuantity;
            set
            {
                SetProperty(ref _PackagesQuantity, value);
            }
        }

        public bool _ManualQuantity = false;
        public bool MANUALQUANTITY
        {
            get => _ManualQuantity;
            set
            {
                SetProperty(ref _ManualQuantity, value);
                INVERSEMANUALQUANTITY = !value;
            }
        }

        public bool _InverseManualQuantity = true;
        public bool INVERSEMANUALQUANTITY
        {
            get => _InverseManualQuantity;
            set => SetProperty(ref _InverseManualQuantity, value);
        }

        public bool _InformPackages = false;
        public bool INFORMPACKAGES
        {
            get => _InformPackages;
            set
            {
                SetProperty(ref _InformPackages, value);
                INVERSEINFORMPACKAGES = !value;
            }
        }
        public bool _InformedPackages = false;
        public bool INFORMEDPACKAGES
        {
            get => _InformPackages;
            set => SetProperty(ref _InformedPackages, value);
        }

        public bool _InverseInformPackages = true;
        public bool INVERSEINFORMPACKAGES
        {
            get => _InverseInformPackages;
            set => SetProperty(ref _InverseInformPackages, value);
        }

        private readonly ProdutosService _ProdutosService;

        public SearchInventarioVM(ESInventarioModel pESInventarioModel)
        {
            Title = "Buscar Inventário";
            _ESInventarioModel = pESInventarioModel;
            _ProdutosService = new ProdutosService();
            CmdSaveCountClick = new Command(SaveCountClick);
            CmdInformPackagesClick = new Command(OnInformPackagesClick);
        }

        public void OnInformPackagesClick()
        {
            INFORMPACKAGES = true;
        }

        public async void SaveCountClick()
        {
            if (IsBusy)
                return;

            HASPRODUCTERROR = PRODUCT.IsEmpty();
            HASSTOCKERROR = STOCK.IsEmpty();

            if (!HASPRODUCTERROR &&
                !HASSTOCKERROR)
            {
                using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
                {
                    if (_FSProdutoModel == null || _FSProdutoModel.ProdutoPcp == null)
                    {
                        ShowShortMessage(Resources.ErrorMessagesResource.ProductNotFound);
                        return;
                    }

                    IsBusy = true;

                    try
                    {
                        var inventarioService = new InventarioService();

                        var CreateInventResult = await inventarioService.CreateItemInventContagemAsync(new ESCreateItemInventarioModel
                        {
                            codestab = Convert.ToSByte(CurrentApp.CODESTAB),
                            codusuario = CurrentApp.UserLogged.GEUsuarios.codusuario,
                            datacontagem = DateTime.Now,
                            quantidade = Convert.ToDecimal(QUANTITY),
                            Result = "",
                            codestoque = STOCK,
                            codinv = (long)_ESInventarioModel.codinv,
                            contagem = (int)_ESInventarioModel.contagem,
                            codproduto = _FSProdutoModel.codproduto
                        });

                        var msg = CreateInventResult.Result;
                        if (msg.IsEmpty())
                            msg = Resources.ValidationMessagesResource.SaveSuccess;

                        ShowShortMessage(msg);

                        if (CreateInventResult.Result.IsEmpty())
                            SaveSuccess?.Invoke(this, null);
                    }
                    catch (Exception ex)
                    {
                        OnMessageRaise(new MessageRaiseEvent(
                            Resources.ValidationMessagesResource.ProblemToSave,
                            ex.Message
                        ));

                        IsBusy = false;
                    }
                    finally
                    {
                        IsBusy = false;
                    }
                }
            }
        }

        public void CountQuantity()
        {
            HASPRODUCTERROR = string.IsNullOrEmpty(PRODUCT);
            HASSTOCKERROR = string.IsNullOrEmpty(STOCK);

            if (HASPRODUCTERROR)
            {
                evtTextFocused?.Invoke("PRODUCT", null);
                return;
            }

            if (HASSTOCKERROR)
            {
                evtTextFocused?.Invoke("STOCK", null);
                return;
            }

            if (_FSProdutoModel == null || _FSProdutoModel.ProdutoPcp == null)
            {
                ShowShortMessage(Resources.ErrorMessagesResource.ProductNotFound);
                evtTextFocused?.Invoke("PRODUCT", null);
                return;
            }

            if (PRODUCTBARCODE.ToUpperInvariant().In(
                    (_FSProdutoModel.codproduto ?? "").ToUpperInvariant(),
                    ((_FSProdutoModel?.ProdutoPcp?.codgtin) ?? "").ToUpperInvariant(),
                    ((_FSProdutoModel?.ProdutoPcp?.codgtinemb) ?? "").ToUpperInvariant()))
            {
                QUANTITY += Convert.ToDouble(_FSProdutoModel.ProdutoPcp.qtdeporemb) * PACKAGESQUANTITY;
                PACKAGESQUANTITY = 1;
            }
            else
            {
                OnMessageRaise(new MessageRaiseEvent(
                    Resources.ValidationMessagesResource.FailToRead,
                    Resources.ErrorMessagesResource.InvalidProduct
                ));
            }
        }

        public async Task GetProductByBarCodeAsync()
        {
            if (IsBusy)
                return;

            HASPRODUCTERROR = string.IsNullOrEmpty(PRODUCT);

            if (!HASPRODUCTERROR)
            {
                IsBusy = true;
                using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
                {
                    try
                    {
                        _FSProdutoModel = await _ProdutosService.GetProductByBarCodeAsync((short)CurrentApp.CODESTAB, PRODUCT);

                        if (_FSProdutoModel != null)
                        {
                            PRODUCT = _FSProdutoModel.codproduto;
                            LBLPRODUCTHELPERTEXT = $"{_FSProdutoModel.codproduto} - {_FSProdutoModel.descricao}".SubStrTitle(50);
                        }
                        else
                        {
                            LBLPRODUCTHELPERTEXT = Resources.FieldsNameResource.ProductKey;
                            throw new Exception(Resources.ErrorMessagesResource.ProductNotFound);
                        }

                        evtTextFocused?.Invoke("STOCK", null);
                    }
                    catch (Exception)
                    {
                        ShowShortMessage(Resources.ErrorMessagesResource.ProductNotFound);

                        evtTextFocused?.Invoke("PRODUCT", null);

                        IsBusy = false;
                    }
                    finally
                    {
                        IsBusy = false;
                    }
                }
            }
        }

        public async Task GetStockByBarCodeAsync()
        {
            if (IsBusy)
                return;

            HASSTOCKERROR = STOCK.IsEmpty();
            HASPRODUCTERROR = PRODUCT.IsEmpty();

            if (!HASSTOCKERROR && !HASPRODUCTERROR)
            {
                IsBusy = true;
                var Produto = new ProdutosVM();
                Produto.PRODUCT = PRODUCT;
                Produto.STOCKORIGIN = STOCK;
                Produto.STOCKDESTINY = STOCK;
                _FSProdutoModel.estoquedestino = STOCK;
                var result = await Produto.CarregaDetailsProductAsync(_FSProdutoModel);
                if (result)
                    LBLSTOCKHELPERTEXT = Produto.SaldoEstoqueDestiny.codestoque;
                else
                    ShowShortMessage(
                        Resources.ErrorMessagesResource.ActualStockBalanceNotFound);

                IsBusy = false;
            }
        }

        public void ClearInstance()
        {
            PRODUCT = "";
            STOCK = "";
            QUANTITY = 0;
            PACKAGESQUANTITY = 1;
            LBLPRODUCTHELPERTEXT = Resources.FieldsNameResource.ProductKey;
            LBLSTOCKHELPERTEXT = Resources.FieldsNameResource.StockOrigin;
            HASPRODUCTERROR = false;
            HASSTOCKERROR = false;
            PRODUCTBARCODE = "";
            HASQUANTITYERROR = false;
            MANUALQUANTITY = false;
            _FSProdutoModel = null;

        }
    }
}
