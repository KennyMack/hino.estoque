﻿using Hino.Estoque.App.Models.Estoque;
using Hino.Estoque.App.Models.Fiscal;
using Hino.Estoque.App.Services.Application.Fiscal;
using Hino.Estoque.App.Utils.Extensions;
using Hino.Estoque.App.Utils.Messages;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using XF.Material.Forms.UI.Dialogs;

namespace Hino.Estoque.App.Services.ViewModels.Estoque
{
    public class EstoqueEndVM : BaseViewModel
    {
        private readonly ProdutosService _ProdutosService;
        private FSProdutoModel _FSProdutoModel;

        public string LBLPRODUCTPLACEHOLDER { get => Resources.FieldsNameResource.ProductKey; }
        public string LBLPRODUCTHELPERTEXT { get => Resources.FieldsNameResource.ProductKey; }
        public string LBLPRODUCTERRORTEXT { get => "Informe o produto"; }

        public string LBLSTOCKORIGINPLACEHOLDER { get => Resources.FieldsNameResource.StockOrigin; }
        public string LBLSTOCKORIGINHELPERTEXT { get => Resources.FieldsNameResource.StockOrigin; }
        public string LBLSTOCKORIGINERRORTEXT { get => "Informe a origem"; }

        public string LBLQUANTITYPLACEHOLDER { get => Resources.FieldsNameResource.Quantity; }
        public string LBLQUANTITYHELPERTEXT { get => Resources.FieldsNameResource.Quantity; }
        public string LBLQUANTITYERRORTEXT { get => "Informe a quantidade"; }

        public string LBLSTOCKDESTINYPLACEHOLDER { get => Resources.FieldsNameResource.StockDestiny; }
        public string LBLSTOCKDESTINYHELPERTEXT { get => Resources.FieldsNameResource.StockDestiny; }
        public string LBLSTOCKDESTINYERRORTEXT { get => "Informe o estoque"; }

        public ObservableCollection<ESItemTranspEndEstoqueModel> Items { get; set; }

        public event EventHandler evtTextFocused;
        public ICommand CmdCarregarEndClick { protected set; get; }
        public ICommand CmdLoadAnotherClick { protected set; get; }
        public ICommand CmdAddEnderecoClick { protected set; get; }
        public event EventHandler GoToMain;

        bool _HasProductError = false;
        public bool HASPRODUCTERROR
        {
            get
            {
                return _HasProductError;
            }
            set
            {
                SetProperty(ref _HasProductError, value);
            }
        }

        bool _HasStockOriginError = false;
        public bool HASSTOCKORIGINERROR
        {
            get
            {
                return _HasStockOriginError;
            }
            set
            {
                SetProperty(ref _HasStockOriginError, value);
            }
        }

        bool _HasStockDestinyError = false;
        public bool HASSTOCKDESTINYERROR
        {
            get
            {
                return _HasStockDestinyError;
            }
            set
            {
                SetProperty(ref _HasStockDestinyError, value);
            }
        }

        string _StockOriginSaldo;
        public string STOCKORIGINSALDO
        {
            get
            {
                return _StockOriginSaldo;
            }
            set
            {
                SetProperty(ref _StockOriginSaldo, value);
            }
        }

        string _StockEnderecoSaldo;
        public string STOCKENDERECOSALDO
        {
            get
            {
                return _StockEnderecoSaldo;
            }
            set
            {
                SetProperty(ref _StockEnderecoSaldo, value);
            }
        }

        string _ProductDetail;
        public string PRODUCTDETAIL
        {
            get
            {
                return _ProductDetail;
            }
            set
            {
                SetProperty(ref _ProductDetail, value);
            }
        }

        string _Product;
        public string PRODUCT
        {
            get
            {
                return _Product;
            }
            set
            {
                SetProperty(ref _Product, value);
            }
        }

        string _StockOrigin;
        public string STOCKORIGIN
        {
            get
            {
                return _StockOrigin;
            }
            set
            {
                SetProperty(ref _StockOrigin, value);
            }
        }

        bool _detailLoaded = false;
        public bool DETAILLOADED
        {
            get
            {
                return _detailLoaded;
            }
            set
            {
                DETAILNOTLOADED = !value;
                SetProperty(ref _detailLoaded, value);
            }
        }

        bool _detailNotLoaded = true;
        public bool DETAILNOTLOADED
        {
            get
            {
                return _detailNotLoaded;
            }
            set
            {
                SetProperty(ref _detailNotLoaded, value);
            }
        }

        public double _Quantity = 0;
        public double QUANTITY
        {
            get
            {
                return _Quantity;
            }
            set
            {
                SetProperty(ref _Quantity, value);
            }
        }

        string _StockDestiny;
        public string STOCKDESTINY
        {
            get => _StockDestiny;
            set => SetProperty(ref _StockDestiny, value);
        }

        #region Construtor
        public EstoqueEndVM()
        {
            Title = "Transf. prod. endereço";
            Items = new ObservableCollection<ESItemTranspEndEstoqueModel>();

            _ProdutosService = new ProdutosService();
            CmdCarregarEndClick = new Command(OnCarregarEndClick);
            CmdLoadAnotherClick = new Command(OnLoadAnotherClick);
            CmdAddEnderecoClick = new Command(OnAddEnderecoClick);
            base.PageNum = 1;
            DETAILLOADED = false;
        }
        #endregion

        public void OnAddEnderecoClick()
        {
            HASPRODUCTERROR = _FSProdutoModel == null || PRODUCT.IsEmpty();
            HASSTOCKDESTINYERROR = STOCKDESTINY.IsEmpty();
            HASSTOCKORIGINERROR = STOCKORIGIN.IsEmpty();

            if (!HASPRODUCTERROR &&
                !HASSTOCKORIGINERROR &&
                !HASSTOCKDESTINYERROR &&
                QUANTITY > 0)
            {
                Items.Add(new ESItemTranspEndEstoqueModel
                {
                    CodProduto = _FSProdutoModel.codproduto,
                    LocalOrigem = STOCKORIGIN,
                    LocalDestino = STOCKDESTINY,
                    Quantidade = QUANTITY
                });
                STOCKDESTINY = "";
                QUANTITY = 0;
            }
        }

        #region OnLoadAnotherClick
        public void OnLoadAnotherClick()
        {
            STOCKORIGIN = "";
            PRODUCT = "";
            STOCKORIGINSALDO = "";
            _ProductDetail = null;
            DETAILLOADED = false;
        }
        #endregion

        #region OnCarregarEndClick
        public async void OnCarregarEndClick()
        {
            if (IsBusy)
                return;

            HASPRODUCTERROR = PRODUCT.IsEmpty();

            if (!HASPRODUCTERROR)
                await BuscaSaldoEstoqueAsync(STOCKORIGIN);
        }
        #endregion

        #region GetProductByBarCodeAsync
        public async Task GetProductByBarCodeAsync()
        {
            if (IsBusy)
                return;

            HASPRODUCTERROR = (string.IsNullOrEmpty(PRODUCT));

            if (!HASPRODUCTERROR)
            {
                IsBusy = true;
                using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
                {
                    try
                    {
                        _FSProdutoModel = await GetProductDataAPIAsync();
                        evtTextFocused.Invoke("STOCKORIGIN", null);
                    }
                    catch (Exception)
                    {
                        ShowShortMessage(Resources.ErrorMessagesResource.ProductNotFound);

                        evtTextFocused.Invoke("PRODUCT", null);

                        IsBusy = false;
                    }
                    finally
                    {
                        IsBusy = false;
                    }
                }
            }
        }
        #endregion

        #region Busca dados do produto na API
        private async Task<FSProdutoModel> GetProductDataAPIAsync() =>
            await _ProdutosService.GetProductByBarCodeAsync((short)CurrentApp.CODESTAB, PRODUCT);
        #endregion

        #region Busca saldo estoque
        public async Task BuscaSaldoEstoqueAsync(string pCodEstoque)
        {
            if (IsBusy)
                return;

            if (PRODUCT.IsEmpty())
            {
                ShowShortMessage(Resources.ValidationMessagesResource.ProductIsNullOrEmpty);

                HASPRODUCTERROR = (string.IsNullOrEmpty(PRODUCT));
                return;
            }

            if (pCodEstoque.IsEmpty())
            {
                ShowShortMessage(
                    string.Format(Resources.ValidationMessagesResource.StockIsNullOrEmpty,
                    "de origem"));
                return;
            }

            IsBusy = true;
            using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
            {
                ShowBarCode = false;
                try
                {
                    var result = await BuscaSaldoEstoqueAPI(pCodEstoque);

                    if (result.saldoEstoque <= 0)
                    {
                        OnMessageRaise(new MessageRaiseEvent(
                            Resources.ValidationMessagesResource.NotFound,
                            Resources.ErrorMessagesResource.ActualStockBalanceNotFound));
                        ShowBarCode = true;
                        PRODUCT = "";
                        STOCKORIGIN = "";
                        evtTextFocused.Invoke("PRODUCT", null);
                        return;
                    }
                    UpdateSaldoEstoque(result);
                    DETAILLOADED = true;
                }
                catch (Exception)
                {
                    ShowShortMessage(Resources.ErrorMessagesResource.ProductNotFound);

                    evtTextFocused.Invoke("PRODUCT", null);

                    IsBusy = false;
                }
                finally
                {
                    IsBusy = false;
                }
            }
        }
        #endregion

        #region Busca Saldo Estoque da API
        private async Task<FSSaldoEstoqueModel> BuscaSaldoEstoqueAPI(string pCodEstoque)
        {
            var result = await _ProdutosService.GetProductSaldoEstoqueAsync(new Models.Estoque.ESBuscaSaldoEstoqueModel
            {
                CodEstab = (int)CurrentApp.CODESTAB,
                CodEstoque = pCodEstoque,
                CodProduto = _FSProdutoModel.codproduto,
                SaldoAtual = 0
            });

            if (result == null)
                ShowShortMessage(
                    Resources.ErrorMessagesResource.ActualStockBalanceNotFound);

            return result;
        }
        #endregion

        #region Update saldo estoque
        private void UpdateSaldoEstoque(FSSaldoEstoqueModel pSaldo)
        {
            if (pSaldo != null)
            {
                STOCKORIGINSALDO = $"{STOCKORIGIN} - Saldo: {pSaldo.saldoEstoque.ToString("n6")}";
                PRODUCTDETAIL = "";

                if (_FSProdutoModel != null)
                    PRODUCTDETAIL = $"{ _FSProdutoModel?.codproduto} - {_FSProdutoModel.descricao}";
            }
        }
        #endregion

        #region Update saldo estoque Address
        private void UpdateSaldoEstoqueEndereco(FSSaldoEstoqueModel pSaldo)
        {
            if (pSaldo != null)
            {
                STOCKENDERECOSALDO = $"{STOCKDESTINY} - Saldo: {pSaldo.saldoEstoque.ToString("n6")}";
            }
        }
        #endregion

        #region GetAddresDestinyInfo
        public async Task GetAddresDestinyInfoAsync()
        {
            if (IsBusy)
                return;

            HASPRODUCTERROR = string.IsNullOrEmpty(PRODUCT);

            if (!HASPRODUCTERROR)
            {
                IsBusy = true;
                using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
                {
                    try
                    {
                        var result = await _ProdutosService.GetProductSaldoEstoqueAsync(new Models.Estoque.ESBuscaSaldoEstoqueModel
                        {
                            CodEstab = (int)CurrentApp.CODESTAB,
                            CodEstoque = STOCKDESTINY,
                            CodProduto = _FSProdutoModel.codproduto,
                            SaldoAtual = 0
                        });
                        UpdateSaldoEstoqueEndereco(result);
                    }
                    catch (Exception)
                    {
                        ShowShortMessage(Resources.ErrorMessagesResource.ProductNotFound);

                        evtTextFocused.Invoke("PRODUCT", null);

                        IsBusy = false;
                    }
                    finally
                    {
                        IsBusy = false;
                    }
                }
            }
        }
        #endregion

        public void ConfirmSave() =>
            OnMessageConfirmRaise(new MessageRaiseEvent(Resources.ValidationMessagesResource.ConfirmTyping, Resources.ValidationMessagesResource.Confirm));

        #region Confirmar Transferencia
        public async Task ConfirmarTransfAsync()
        {
            if (IsBusy)
                return;

            if (Items.Count <= 0)
            {
                ShowShortMessage(Resources.ValidationMessagesResource.TransferenceHasNoItems);
                return;
            }

            HASPRODUCTERROR = _FSProdutoModel == null || PRODUCT.IsEmpty();
            HASSTOCKORIGINERROR = STOCKORIGIN.IsEmpty();

            if (HASPRODUCTERROR ||
                HASSTOCKORIGINERROR)
            {
                ShowShortMessage(Resources.ValidationMessagesResource.ProductIsNullOrEmpty);
                return;
            }

            IsBusy = true;
            using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
            {
                var Create = new ESCreateProdEndTransfModel
                {
                    CodEstab = (int)CurrentApp.CODESTAB,
                    CodEstoqueOri = STOCKORIGIN,
                    CodUsuario = CurrentApp.UserLogged.GEUsuarios.codusuario,
                    CodProduto = _FSProdutoModel.codproduto
                };

                foreach (var item in Items)
                {
                    Create.Items.Add(new ESProdEndTransfItemModel
                    {
                        CodEstoqueDest = item.LocalDestino,
                        Quantidade = item.Quantidade
                    });
                }
                try
                {
                    var ret = await _ProdutosService.PostTransferirEndAsync(Create);

                    if (ret)
                    {
                        ShowShortMessage(Resources.ValidationMessagesResource.SaveSuccess);
                        GoToMain.Invoke(this, null);
                    }
                }
                catch (Exception e)
                {
                    ShowShortMessage(e.Message);

                    IsBusy = false;
                }
                finally
                {
                    IsBusy = false;
                }

            }
        }
        #endregion

    }
}
