﻿using Hino.Estoque.App.Models.Estoque;
using Hino.Estoque.App.Services.Application.Estoque;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XF.Material.Forms.UI.Dialogs;

namespace Hino.Estoque.App.Services.ViewModels.Estoque
{
    public class RequisicoesPendenteVM : BaseViewModel
    {
        public ObservableCollection<ESRequisicoesModel> Items { get; set; }
        private readonly RequisicaoService _RequisicaoService;

        public RequisicoesPendenteVM()
        {
            Title = "Requisições Pendentes";
            Items = new ObservableCollection<ESRequisicoesModel>();
            _RequisicaoService = new RequisicaoService();
            PageNum = 1;
        }

        #region Get Data From Source
        private async Task<IEnumerable<ESRequisicoesModel>> GetDataFromSourceAsync()
        {
            return (await _RequisicaoService.GetPendentesAsync()).OrderByDescending(r => r.codrequisicao);
        }
        #endregion

        protected override async Task LoadMoreData()
        {
            IsBusy = true;

            try
            {
                Items.CopyFromList(await GetDataFromSourceAsync(), false);
                IsBusy = false;
            }
            catch (Exception e)
            {
                ShowShortMessage(e.Message);

                IsBusy = false;
            }
            finally
            {
                IsBusy = false;
            }
        }

        protected override async Task RefreshData()
        {
            await base.RefreshData();

            IsBusy = true;

            PageNum = 1;
            Items.Clear();

            await LoadMoreData();
        }

        public async Task AproveRequest(int pCodRequisicao)
        {
            if (IsBusy)
                return;

            IsBusy = true;
            using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
            {
                try
                {
                    var ret = await _RequisicaoService.PostAprovarRequisicaoAsync(new ESUpdateRequestModel
                    {
                        codestab = (short)CurrentApp.CODESTAB,
                        codrequisicao = pCodRequisicao,
                        codfuncionario = Convert.ToInt32(CurrentApp.UserLogged.GEFuncionarios.codfuncionario)
                    });

                    if (ret != null)
                    {
                        ShowShortMessage(Resources.ValidationMessagesResource.SaveSuccess);
                        await RefreshData();
                    }
                }
                catch (Exception e)
                {
                    ShowShortMessage(e.Message);

                    IsBusy = false;
                }
                finally
                {
                    IsBusy = false;
                }
            }
        }

        public async Task ReproveRequest(int pCodRequisicao)
        {
            if (IsBusy)
                return;

            IsBusy = true;
            using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
            {
                try
                {
                    var ret = await _RequisicaoService.PostReprovarRequisicaoAsync(new ESUpdateRequestModel
                    {
                        codestab = (short)CurrentApp.CODESTAB,
                        codrequisicao = pCodRequisicao,
                        codfuncionario = Convert.ToInt32(CurrentApp.UserLogged.GEFuncionarios.codfuncionario)
                    });

                    if (ret != null)
                    {
                        ShowShortMessage(Resources.ValidationMessagesResource.SaveSuccess);
                        await RefreshData();
                    }
                }
                catch (Exception e)
                {
                    ShowShortMessage(e.Message);

                    IsBusy = false;
                }
                finally
                {
                    IsBusy = false;
                }
            }
        }

    }
}
