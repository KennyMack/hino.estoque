﻿using Hino.Estoque.App.Models.Fiscal;
using Hino.Estoque.App.Services.Application.Estoque;
using Hino.Estoque.App.Services.Application.Fiscal;
using Hino.Estoque.App.Utils.Extensions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using XF.Material.Forms.UI.Dialogs;

namespace Hino.Estoque.App.Services.ViewModels.Estoque
{
    public class RequisicaoNovaVM : BaseViewModel
    {
        public FSSaldoEstoqueModel SaldoEstoqueOrigin { get; set; }
        private readonly RequisicaoService _RequisicaoService;
        private readonly ProdutosService _ProdutosService;

        public ICommand CmdRequestItemClick { protected set; get; }
        public event EventHandler GoToMain;
        public event EventHandler evtTextFocused;

        public string LBLPRODUCTPLACEHOLDER => Resources.FieldsNameResource.ProductKey;
        public string LBLPRODUCTHELPERTEXT => Resources.FieldsNameResource.ProductKey;
        public string LBLPRODUCTERRORTEXT => "Informe o produto";

        public string LBLSTOCKORIGINPLACEHOLDER => Resources.FieldsNameResource.StockOrigin;
        public string LBLSTOCKORIGINHELPERTEXT => Resources.FieldsNameResource.StockOrigin;
        public string LBLSTOCKORIGINERRORTEXT => "Informe a origem";

        public string LBLQUANTITYPLACEHOLDER => Resources.FieldsNameResource.Quantity;
        public string LBLQUANTITYHELPERTEXT => Resources.FieldsNameResource.Quantity;
        public string LBLQUANTITYERRORTEXT => "Informe a quantidade";

        public bool _HasProductError = false;
        public bool HASPRODUCTERROR
        {
            get => _HasProductError;
            set => SetProperty(ref _HasProductError, value);
        }

        public bool _HasStockOriginError = false;
        public bool HASSTOCKORIGINERROR
        {
            get => _HasStockOriginError;
            set => SetProperty(ref _HasStockOriginError, value);
        }

        public bool _HasQuantityError = false;
        public bool HASQUANTITYERROR
        {
            get => _HasQuantityError;
            set => SetProperty(ref _HasQuantityError, value);
        }

        public string _Product;
        public string PRODUCT
        {
            get => _Product;
            set => SetProperty(ref _Product, value);
        }

        public string _StockOrigin;
        public string STOCKORIGIN
        {
            get => _StockOrigin;
            set => SetProperty(ref _StockOrigin, value);
        }

        public string _StockOriginSaldo;
        public string STOCKORIGINSALDO
        {
            get => _StockOriginSaldo;
            set => SetProperty(ref _StockOriginSaldo, value);
        }

        public double _Quantity = 0;
        public double QUANTITY
        {
            get => _Quantity;
            set => SetProperty(ref _Quantity, value);
        }
        public bool _Rast = false;
        public bool HASRAST
        {
            get => _Rast;
            set => SetProperty(ref _Rast, value);
        }

        public RequisicaoNovaVM()
        {
            Title = "Nova Requisição";
            _ProdutosService = new ProdutosService();
            _RequisicaoService = new RequisicaoService();
            CmdRequestItemClick = new Command(OnRequestItemClick);
            base.PageNum = 1;
        }

        #region Busca dados do produto na API
        private async Task<FSProdutoModel> GetProductDataAPIAsync() =>
            await _ProdutosService.GetProductByBarCodeAsync((short)CurrentApp.CODESTAB, PRODUCT);
        #endregion

        #region Carrega detalhes produto
        private async Task CarregaDetailsProductAsync(FSProdutoModel pProduct)
        {
            if (pProduct != null)
            {
                if (STOCKORIGIN.IsEmpty())
                    STOCKORIGIN = pProduct.ProdutoParamEstab.codestoque;
                
                HASRAST = pProduct.ProdutoPcp.rastreabilidade;

                if (!pProduct.ProdutoParamEstab.codestoque.IsEmpty())
                {
                    SaldoEstoqueOrigin = await BuscaSaldoEstoqueAPI(pProduct.ProdutoParamEstab.codestoque);
                    UpdateSaldoEstoque(SaldoEstoqueOrigin);
                }

                evtTextFocused.Invoke(
                    string.IsNullOrEmpty(pProduct.estoquedestino) ?
                    "STOCKDESTINY" : HASRAST ? "LOTE" : "QUANTITY", null);
            }
        }
        #endregion

        #region Update saldo estoque
        private void UpdateSaldoEstoque(FSSaldoEstoqueModel pSaldo)
        {
            if (pSaldo != null)
            {
                STOCKORIGINSALDO = string.Format(
                    Resources.ValidationMessagesResource.ActualStockBalance,
                    pSaldo.saldoEstoque.ToString("n6"));
            }
        }
        #endregion

        #region Busca Saldo Estoque da API
        private async Task<FSSaldoEstoqueModel> BuscaSaldoEstoqueAPI(string pCodEstoque)
        {
            var result = await _ProdutosService.GetProductSaldoEstoqueAsync(new Models.Estoque.ESBuscaSaldoEstoqueModel
            {
                CodEstab = (int)CurrentApp.CODESTAB,
                CodEstoque = pCodEstoque,
                CodProduto = PRODUCT,
                SaldoAtual = 0
            });

            if (result == null)
                ShowShortMessage(
                    Resources.ErrorMessagesResource.ActualStockBalanceNotFound);

            return result;
        }
        #endregion

        #region Busca dados do produto pelo codigo de barras
        public async Task GetProductByBarCodeAsync()
        {
            if (IsBusy)
                return;

            HASPRODUCTERROR = string.IsNullOrEmpty(PRODUCT);

            if (!HASPRODUCTERROR)
            {
                IsBusy = true;
                using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
                {
                    try
                    {
                        var result = await GetProductDataAPIAsync();

                        await CarregaDetailsProductAsync(result);
                    }
                    catch (Exception)
                    {
                        ShowShortMessage(Resources.ErrorMessagesResource.ProductNotFound);

                        evtTextFocused.Invoke("PRODUCT", null);

                        IsBusy = false;
                    }
                    finally
                    {
                        IsBusy = false;
                    }
                }
            }
        }
        #endregion

        #region Busca saldo estoque
        public async Task BuscaSaldoEstoqueAsync(string pCodEstoque)
        {
            if (IsBusy)
                return;

            if (PRODUCT.IsEmpty())
            {
                ShowShortMessage(Resources.ValidationMessagesResource.ProductIsNullOrEmpty);

                HASPRODUCTERROR = PRODUCT.IsEmpty();
                return;
            }

            if (pCodEstoque.IsEmpty())
            {
                ShowShortMessage(
                    string.Format(Resources.ValidationMessagesResource.StockIsNullOrEmpty,
                    "de origem"));
                return;
            }

            IsBusy = true;
            using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
            {
                try
                {
                    var result = await BuscaSaldoEstoqueAPI(pCodEstoque);
                    UpdateSaldoEstoque(result);
                }
                catch (Exception)
                {
                    ShowShortMessage(Resources.ErrorMessagesResource.ProductNotFound);

                    evtTextFocused.Invoke("PRODUCT", null);

                    IsBusy = false;
                }
                finally
                {
                    IsBusy = false;
                }
            }
        }
        #endregion

        #region Ao clicar em requisitar
        private async void OnRequestItemClick()
        {
            if (IsBusy)
                return;

            HASPRODUCTERROR = string.IsNullOrEmpty(PRODUCT);
            HASSTOCKORIGINERROR = string.IsNullOrEmpty(STOCKORIGIN);

            if (!HASPRODUCTERROR &&
                !HASSTOCKORIGINERROR)
            {
                if (_Quantity <= 0)
                {
                    ShowShortMessage(Resources.ValidationMessagesResource.QuantityIsNotValid);
                    return;
                }

                IsBusy = true;
                using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
                {
                    try
                    {
                        var ret = await _RequisicaoService.PostRequisitarAsync(new Models.Estoque.ESCreateRequestModel
                        {
                            codestab = (short)CurrentApp.CODESTAB,
                            codestoque = STOCKORIGIN,
                            quantidade = Convert.ToDecimal(QUANTITY),
                            codproduto = PRODUCT,
                            codfuncionario = Convert.ToInt32(CurrentApp.UserLogged.GEFuncionarios.codfuncionario)
                        });

                        if (ret != null)
                        {
                            ShowShortMessage(Resources.ValidationMessagesResource.SaveSuccess);
                            GoToMain.Invoke(this, null);
                        }
                    }
                    catch (Exception e)
                    {
                        ShowShortMessage(e.Message);

                        IsBusy = false;
                    }
                    finally
                    {
                        IsBusy = false;
                    }
                }
            }
        }
        #endregion
    }
}
