﻿using Hino.Estoque.App.Services.Application.Auth;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using XF.Material.Forms.UI.Dialogs;

namespace Hino.Estoque.App.Services.ViewModels.Auth
{
    public class ServerVM : BaseViewModel
    {
        public string LBLSERVERADDRESSPLACEHOLDER { get => Hino.Estoque.App.Resources.FieldsNameResource.ServerAddress; }
        public string LBLSERVERADDRESSHELPERTEXT { get => Hino.Estoque.App.Resources.FieldsNameResource.ServerAddress; }
        public string LBLSERVERADDRESSERRORTEXT { get => "Informe o endereço"; }

        public event EventHandler SaveComplete;
        public event EventHandler BackPage;

        public ICommand CmdSaveClick { protected set; get; }
        public ICommand CmdBackClick { protected set; get; }

        public bool _HasServerAddressError = false;
        public bool HASSERVERADDRESSERROR
        {
            get
            {
                return _HasServerAddressError;
            }
            set
            {
                SetProperty(ref _HasServerAddressError, value);
            }
        }

        private string _ServerAddress;
        public string SERVERADDRESS
        {
            get
            {
                return _ServerAddress;
            }
            set
            {
                SetProperty(ref _ServerAddress, value);
            }
        }

        public ServerVM()
        {
            CmdSaveClick = new Command(OnSaveClick);
            CmdBackClick = new Command(OnBackClick);
            
            SERVERADDRESS = CurrentApp.IPBASE;
        }


        private async void OnSaveClick()
        {
            if (IsBusy)
                return;

            HASSERVERADDRESSERROR = (string.IsNullOrEmpty(SERVERADDRESS));
            bool Ok = false;
            if (!HASSERVERADDRESSERROR)
            {
                using (await MaterialDialog.Instance.LoadingSnackbarAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
                {
                    IsBusy = true;
                    try
                    {
                        var _AuthService = new AuthenticationService();

                        CurrentApp.BASEURL = SERVERADDRESS;
                        var ret = await _AuthService.TestServerAsync();

                        if (!ret)
                        {
                            CurrentApp.BASEURL = null;
                            ShowShortMessage(Resources.ErrorMessagesResource.ComunicationFail);
                        }

                        CurrentApp.BASEURL = SERVERADDRESS;

                        ShowShortMessage(Resources.ValidationMessagesResource.SaveSuccess);

                        Ok = true;
                    }
                    catch (Exception)
                    {
                        CurrentApp.BASEURL = null;
                        ShowShortMessage(Resources.ErrorMessagesResource.ComunicationFail);
                        Ok = false;
                    }
                    finally
                    {
                        IsBusy = false;
                    }
                }
                if (Ok)
                    SaveComplete?.Invoke(this, null);
            }
        }

        private void OnBackClick() =>
            BackPage?.Invoke(this, null);
    }
}
