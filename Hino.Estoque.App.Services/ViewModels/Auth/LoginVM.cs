﻿using Hino.Estoque.App.Models.Auth;
using Hino.Estoque.App.Models.Gerais;
using Hino.Estoque.App.Services.API.Interfaces.Gerais;
using Hino.Estoque.App.Services.Application.Auth;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using XF.Material.Forms.UI;
using XF.Material.Forms.UI.Dialogs;
using Hino.Estoque.App.Utils.Messages;
using Hino.Estoque.App.Services.DB.Interfaces.Producao;
using XF.Material.Forms;

namespace Hino.Estoque.App.Services.ViewModels.Auth
{
    public class LoginVM : BaseViewModel
    {
        public ObservableCollection<string> Estabelecimentos { get; set; }
        private IPDOPCreateTransfDB _IPDOPCreateTransfDB;
        private GEUsuariosLoginModel _LoginModel;

        public string LBLUSERPLACEHOLDER { get => Hino.Estoque.App.Resources.FieldsNameResource.UserIdentifier; }
        public string LBLUSERHELPERTEXT { get => Hino.Estoque.App.Resources.FieldsNameResource.UserIdentifier; }
        public string LBLUSERERRORTEXT { get => "Informe o usuário"; }

        public string LBLUSERPASSPLACEHOLDER { get => Hino.Estoque.App.Resources.FieldsNameResource.Password; }
        public string LBLUSERPASSHELPERTEXT { get => Hino.Estoque.App.Resources.FieldsNameResource.Password; }
        public string LBLUSERPASSERRORTEXT { get => "Informe a senha"; }

        public event EventHandler GoToLogin;
        public event EventHandler GoToMainPage;
        public ICommand CmdLoginClick { protected set; get; }

        public string _EstabSelected;
        public string ESTABSELECTED
        {
            get => _EstabSelected;
            set => SetProperty(ref _EstabSelected, value);
        }

        public short CodEstabSelected
        {
            get
            {
                try
                {
                    return Convert.ToInt16(ESTABSELECTED.Split('-')[0]);
                }
                catch (Exception)
                {
                    return 0;
                }
            }
        }

        public string _DisplayPendentes = "";
        public string DISPLAYPENDENTES
        {
            get => _DisplayPendentes;
            set => SetProperty(ref _DisplayPendentes, value);
        }

        public string _UserLogged = "";
        public string USERLOGGED
        {
            get => _UserLogged;
            set => SetProperty(ref _UserLogged, value);
        }

        public bool _ShowSelectEstab = false;
        public bool SHOWSELECTESTAB
        {
            get => _ShowSelectEstab;
            set => SetProperty(ref _ShowSelectEstab, value);
        }

        public bool _ShowPassword = false;
        public bool SHOWPASSWORD
        {
            get => _ShowPassword;
            set => SetProperty(ref _ShowPassword, value);
        }

        public bool _HasUserError = false;
        public bool HASUSERERROR
        {
            get => _HasUserError;
            set => SetProperty(ref _HasUserError, value);
        }

        public bool _HasUserPassError = false;
        public bool HASUSERPASSERROR
        {
            get => _HasUserPassError;
            set => SetProperty(ref _HasUserPassError, value);
        }

        public bool _HasPendentes = false;
        public bool HASPENDENTES
        {
            get => _HasPendentes;
            set => SetProperty(ref _HasPendentes, value);
        }

        private string _UserIdentifier;
        public string USERIDENTIFIER
        {
            get => _UserIdentifier;
            set => SetProperty(ref _UserIdentifier, value);
        }

        private string _Password;
        public string PASSWORD
        {
            get => _Password;
            set => SetProperty(ref _Password, value);
        }

        public LoginVM()
        {
            Estabelecimentos = new ObservableCollection<string>();

            _IPDOPCreateTransfDB = DInjection.GetIntance<IPDOPCreateTransfDB>();
            _LoginModel = new GEUsuariosLoginModel();
            CmdLoginClick = new Command(OnLoginClick);
        }

        public async void OnLoginClick()
        {
            if (IsBusy)
                return;

            _LoginModel.codestab = CodEstabSelected;
            _LoginModel.identifier = USERIDENTIFIER;
            _LoginModel.senha = PASSWORD;
            
            HASUSERERROR = string.IsNullOrEmpty(_LoginModel.identifier);

            if (string.IsNullOrEmpty(CurrentApp.IPBASE))
            {
                ShowShortMessage(Resources.ErrorMessagesResource.ServerAddressNotFound);
                return;
            }

            if (SHOWPASSWORD)
                HASUSERPASSERROR = string.IsNullOrEmpty(_LoginModel.senha);

            if (_LoginModel.codestab <= 0)
                ShowShortMessage(Resources.ErrorMessagesResource.SelectEstablishment);

            if (!HASUSERERROR &&
                !HASUSERPASSERROR &&
                _LoginModel.codestab > 0
                )
            {
                using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
                {
                    IsBusy = true;
                    await Task.Delay(200);
                    try
                    {
                        var AuthService = new AuthenticationService();
                        var ret = await AuthService.LoginAsync(_LoginModel);

                        if (!ret.success)
                        {
                            IsBusy = false;
                            ShowShortMessage(ret.error);
                            return;
                        }

                        await Task.Delay(200);

                        GoToMainPage?.Invoke(ret, null);
                    }
                    catch (Exception)
                    {
                        ShowShortMessage(Resources.ErrorMessagesResource.ComunicationFail);
                    }
                    finally
                    {
                        IsBusy = false;
                    }
                }
            }
        }

        public async Task ShowEstabs()
        {
            var view = new MaterialTextField()
            {
                Choices = Estabelecimentos,
                InputType = MaterialTextFieldInputType.Choice,
                SelectedChoice = Estabelecimentos.Count > 0 ? Estabelecimentos[0] : "",
                Style = Material.GetResource<Style>("MaterialTextFieldStyle"),
            };


            bool wasConfirmed = await MaterialDialog.Instance.ShowCustomContentAsync(view, 
                "Estabelecimentos", "Selecione", "Ok", "Cancelar") ?? false;


            if (wasConfirmed && view.SelectedChoice != null)
                ESTABSELECTED = view.SelectedChoice.ToString();

            // var view = new MaterialRadioButtonGroup()
            // {
            //     Choices = Estabelecimentos
            // };
            // 
            // bool wasConfirmed = await MaterialDialog.Instance.ShowCustomContentAsync(view, "Estabelecimentos", "Selecione", "Ok", "Cancelar") ?? false;
            // 
            // if (wasConfirmed && view.SelectedIndex > -1)
            //     ESTABSELECTED = Estabelecimentos[view.SelectedIndex];
        }

        #region Load Estabs
        public async Task LoadEstabs()
        {
            using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
            {
                Estabelecimentos.Clear();
                try
                {
                    var _IEstabelecimentosAPI = DInjection.GetIntance<IEstabelecimentosAPI>();
                    var items = await _IEstabelecimentosAPI.GetAllEstabs();

                    var setting = await _IEstabelecimentosAPI.GetSettings();

                    SHOWPASSWORD = setting.UsePassword;

                    foreach (var r in items)
                        Estabelecimentos.Add($"{r.codestab}-{r.nomefantasia}");

                    /* Estabelecimentos.Add("2-ESTAB 2");
                     Estabelecimentos.Add("3-ESTAB 3");
                     Estabelecimentos.Add("4-ESTAB 4");*/

                    SHOWSELECTESTAB = Estabelecimentos.Count > 1;

                    if (Estabelecimentos.Count > 0)
                    {
                        if (CurrentApp.CODESTAB == 0)
                            ESTABSELECTED = Estabelecimentos.FirstOrDefault();
                        else
                            ESTABSELECTED = Estabelecimentos.Where(r => r.Split('-')[0] == CurrentApp.CODESTAB.ToString()).FirstOrDefault();
                    }
                }
                catch (Exception)
                {
                    Estabelecimentos.Clear();
                }
            }
        }
        #endregion

        public void ConfirmExit() =>
            OnMessageConfirmRaise(new MessageRaiseEvent(
                Resources.ValidationMessagesResource.Confirm, Resources.ValidationMessagesResource.ConfirmLogout));

        public void LogOut()
        {
            CurrentApp.ClearPreferences();
            GoToLogin?.Invoke(this, null);
        }

        public async Task LoadPendentesAync()
        {
            var Items = await _IPDOPCreateTransfDB.GetItemsAsync(-1);
            USERLOGGED = CurrentApp.UserLogged?.GEUsuarios?.codusuario ?? "";
            USERLOGGED = Items.Results.Any() ?
                $"{USERLOGGED}{Environment.NewLine}{Items.Results.Count} transf. pendentes" :
                USERLOGGED;
        }

    }
}
