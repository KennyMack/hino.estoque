﻿using Hino.Estoque.App.Models.Fiscal;
using Hino.Estoque.App.Models.Gerais;
using Hino.Estoque.App.Services.Application.Fiscal;
using Hino.Estoque.App.Utils.Extensions;
using Hino.Estoque.App.Utils.Messages;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using XF.Material.Forms.UI;
using XF.Material.Forms.UI.Dialogs;

namespace Hino.Estoque.App.Services.ViewModels.Fiscal
{
    public class ProdutosVM : BaseViewModel
    {
        public string LBLPRODUCTPLACEHOLDER { get => Resources.FieldsNameResource.ProductKey; }
        public string LBLPRODUCTHELPERTEXT { get => Resources.FieldsNameResource.ProductKey; }
        public string LBLPRODUCTERRORTEXT { get => "Informe o produto"; }

        public string LBLSTOCKORIGINPLACEHOLDER { get => Resources.FieldsNameResource.StockOrigin; }
        public string LBLSTOCKORIGINHELPERTEXT { get => Resources.FieldsNameResource.StockOrigin; }
        public string LBLSTOCKORIGINERRORTEXT { get => "Informe a origem"; }

        public string LBLSTOCKDESTINYPLACEHOLDER { get => Resources.FieldsNameResource.StockDestiny; }
        public string LBLSTOCKDESTINYHELPERTEXT { get => Resources.FieldsNameResource.StockDestiny; }
        public string LBLSTOCKDESTINYERRORTEXT { get => "Informe o destino"; }

        public string LBLQUANTITYPLACEHOLDER { get => Resources.FieldsNameResource.Quantity; }
        public string LBLQUANTITYHELPERTEXT { get => Resources.FieldsNameResource.Quantity; }
        public string LBLQUANTITYERRORTEXT { get => "Informe a quantidade"; }

        public string LBLLOTEPLACEHOLDER { get => Resources.FieldsNameResource.Lote; }
        public string LBLLOTEHELPERTEXT { get => Resources.FieldsNameResource.Lote; }

        public ObservableCollection<FSSaldoEstoqueModel> Enderecamentos { get; set; }
        public ObservableCollection<FSSaldoEstoqueModel> Items { get; set; }
        public ObservableCollection<GEDetalhesListagemModel> Detalhes { get; set; }

        public event EventHandler evtTextFocused;
        public ICommand CmdTransfItemClick { protected set; get; }
        public ICommand CmdConsultarClick { protected set; get; }
        public ICommand CmdDetalharClick { protected set; get; }
        public ICommand TapAddressSearchCommand { protected set; get; }
        public event EventHandler GoToMain;

        public FSSaldoEstoqueModel SaldoEstoqueOrigin { get; set; }
        public FSSaldoEstoqueModel SaldoEstoqueDestiny { get; set; }

        public bool _HasProductError = false;
        public bool HASPRODUCTERROR
        {
            get => _HasProductError;
            set => SetProperty(ref _HasProductError, value);
        }

        public bool _HasStockOriginError = false;
        public bool HASSTOCKORIGINERROR
        {
            get => _HasStockOriginError;
            set => SetProperty(ref _HasStockOriginError, value);
        }

        public bool _HasStockDestinyError = false;
        public bool HASSTOCKDESTINYERROR
        {
            get => _HasStockDestinyError;
            set => SetProperty(ref _HasStockDestinyError, value);
        }

        public bool _HasQuantityError = false;
        public bool HASQUANTITYERROR
        {
            get => _HasQuantityError;
            set => SetProperty(ref _HasQuantityError, value);
        }

        public string _Product;
        public string PRODUCT
        {
            get => _Product;
            set => SetProperty(ref _Product, value);
        }

        public string _StockOrigin;
        public string STOCKORIGIN
        {
            get => _StockOrigin;
            set => SetProperty(ref _StockOrigin, value);
        }
        public string _StockOriginSaldo;
        public string STOCKORIGINSALDO
        {
            get => _StockOriginSaldo;
            set => SetProperty(ref _StockOriginSaldo, value);
        }

        public string _StockDestiny;
        public string STOCKDESTINY
        {
            get => _StockDestiny;
            set => SetProperty(ref _StockDestiny, value);
        }

        public string _StockDestinySaldo;
        public string STOCKDESTINYSALDO
        {
            get => _StockDestinySaldo;
            set => SetProperty(ref _StockDestinySaldo, value);
        }

        public double _Quantity = 0;
        public double QUANTITY
        {
            get => _Quantity;
            set => SetProperty(ref _Quantity, value);
        }

        public string _Lote;
        public string LOTE
        {
            get => _Lote;
            set => SetProperty(ref _Lote, value);
        }
        public bool _Rast = false;
        public bool HASRAST
        {
            get => _Rast;
            set => SetProperty(ref _Rast, value);
        }
        bool _detailLoaded = false;
        public bool DETAILLOADED
        {
            get => _detailLoaded;
            set
            {
                DETAILNOTLOADED = !value;
                SetProperty(ref _detailLoaded, value);
            }
        }
        bool _detailNotLoaded = true;
        public bool DETAILNOTLOADED
        {
            get => _detailNotLoaded;
            set => SetProperty(ref _detailNotLoaded, value);
        }

        private readonly ProdutosService _ProdutosService;

        public ProdutosVM(string title = "Transferir Produtos")
        {
            Title = title;
            Items = new ObservableCollection<FSSaldoEstoqueModel>();
            Enderecamentos = new ObservableCollection<FSSaldoEstoqueModel>();
            Detalhes = new ObservableCollection<GEDetalhesListagemModel>();
            TapAddressSearchCommand = new Command(OnTapAddressSearch);
            _ProdutosService = new ProdutosService();
            CmdTransfItemClick = new Command(OnTransfItemClick);
            CmdConsultarClick = new Command(OnConsultaItemClick);
            CmdDetalharClick = new Command(OnDetalharClick);
            PageNum = 1;
            HASRAST = false;
        }

        public async void OnDetalharClick()
        {
            if (IsBusy)
                return;

            HASPRODUCTERROR = PRODUCT.IsEmpty();

            if (!HASPRODUCTERROR)
                await GetDetailProductByBarCodeAsync();
        }

        public async void OnConsultaItemClick()
        {
            if (IsBusy)
                return;

            HASPRODUCTERROR = PRODUCT.IsEmpty();

            if (!HASPRODUCTERROR)
                await GetBasicProductByBarCodeAsync();
        }

        #region Ao clicar para buscar endereço
        async void OnTapAddressSearch(object origem)
        {
            if (!Enderecamentos.Any())
            {
                ShowShortMessage(Resources.ValidationMessagesResource.NoAddressStockToDisplay);
                return;
            }

            using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    var SortedAddress = new List<FSSaldoEstoqueModel>();
                    var results = new List<string>();
                    var title = Resources.FieldsNameResource.AddressStock;

                    SortedAddress = Enderecamentos
                        .OrderByDescending(r => r.saldoEstoque)
                        .ThenByDescending(r => r.capacidadedisp)
                        .ToList();

                    results = Enderecamentos
                        .OrderByDescending(r => r.saldoEstoque)
                        .ThenByDescending(r => r.capacidadedisp)
                        .Select(r => $"({r.CapacidadeDescricao}) {r.Enderecamento}")
                        .ToList();

                    var view = new MaterialRadioButtonGroup()
                    {
                        Choices = results
                    };

                    var SelecteIndex = await MaterialDialog.Instance.SelectChoiceAsync(
                        title: title,
                        choices: results,
                        confirmingText: "OK",
                        dismissiveText: "CANCELAR");

                    if (SelecteIndex > -1)
                    {
                        var ValueSelected = SortedAddress[SelecteIndex];
                        if (origem.ToString() == "btnStockOrigin")
                        {
                            STOCKORIGIN = ValueSelected.codestoque.ToUpper();
                            await BuscaSaldoEstoqueAsync(STOCKORIGIN, true);
                        }
                        else if (origem.ToString() == "btnStockDestiny")
                        {
                            STOCKDESTINY = ValueSelected.codestoque.ToUpper();
                            await BuscaSaldoEstoqueAsync(STOCKDESTINY, false);
                        }
                    }
                });
            }
        }
        #endregion

        public async Task TransfConfirm()
        {
            if (IsBusy)
                return;

            HASPRODUCTERROR = string.IsNullOrEmpty(PRODUCT);
            HASSTOCKORIGINERROR = string.IsNullOrEmpty(STOCKORIGIN);
            HASSTOCKDESTINYERROR = string.IsNullOrEmpty(STOCKDESTINY);

            if (!HASPRODUCTERROR &&
                !HASSTOCKORIGINERROR &&
                !HASSTOCKDESTINYERROR)
            {
                if (_Quantity <= 0)
                {
                    ShowShortMessage(Resources.ValidationMessagesResource.QuantityIsNotValid);
                    return;
                }
                long loteId = 0;
                if (!string.IsNullOrEmpty(LOTE))
                {
                    if (!long.TryParse(LOTE, out loteId))
                    {
                        ShowShortMessage(Resources.ValidationMessagesResource.InvalidLote);
                        return;
                    }
                }

                IsBusy = true;
                using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
                {
                    try
                    {
                        var ret = await _ProdutosService.PostTransferirAsync(new Models.Estoque.ESCreateProdTransferenciaModel
                        {
                            CodEstab = (int)CurrentApp.CODESTAB,
                            CodEstoqueOri = STOCKORIGIN,
                            CodEstoqueDest = STOCKDESTINY,
                            Quantidade = Convert.ToDecimal(QUANTITY),
                            CodProduto = PRODUCT,
                            CodUsuario = CurrentApp.UserLogged.GEUsuarios.codusuario,
                            Lote = loteId
                        });

                        if (ret)
                        {
                            if (CurrentApp.UserLogged.GESettings.Settings.KeepTransfLocations)
                            {
                                CurrentApp.LastStockOrigin = STOCKORIGIN;
                                CurrentApp.LastStockDestiny = STOCKDESTINY;
                            }

                            ShowShortMessage(Resources.ValidationMessagesResource.SaveSuccess);
                            GoToMain.Invoke(this, null);
                        }
                    }
                    catch (Exception e)
                    {
                        ShowShortMessage(e.Message);

                        IsBusy = false;
                    }
                    finally
                    {
                        IsBusy = false;
                    }
                }
            }
        }

        public async void OnTransfItemClick()
        {
            if (Enderecamentos.Any())
            {
                var Address = Enderecamentos.FirstOrDefault(r => 
                    r.codestoque.ToUpper() == STOCKDESTINY.ToUpper().Trim());

                if (Address != null)
                {
                    if (Address.capacidadedisp - Convert.ToDecimal(QUANTITY) < 0)
                    {
                        OnMessageConfirmRaise(new MessageRaiseEvent(
                            Resources.ValidationMessagesResource.ConfirmTransf,
                            Resources.ValidationMessagesResource.AddressCapacityWasExceded));
                        return;
                    }
                }
            }
            await TransfConfirm();
        }

        public async Task<FSProdutoModel> GetProductByBarCodeAsync()
        {
            FSProdutoModel retorno = null;
            if (IsBusy)
                return retorno;

            HASPRODUCTERROR = string.IsNullOrEmpty(PRODUCT);

            if (!HASPRODUCTERROR)
            {
                IsBusy = true;
                using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
                {
                    try
                    {
                        Enderecamentos = new ObservableCollection<FSSaldoEstoqueModel>();

                        retorno = await GetProductDataAPIAsync();

                        try
                        {
                            var ProductAddress = await _ProdutosService.GetProductInventoryBalancesAsync(new Models.Estoque.ESBuscaSaldoEstoqueModel
                            {
                                CodProduto = PRODUCT
                            });

                            /*.Where(r =>
                                (r.FSParamLocEstoqEstab.tipoend == 0 ||
                                r.FSParamLocEstoqEstab.tipoend == 1 ||
                                r.FSParamLocEstoqEstab.tipoend == 3) &&
                                !r.FSParamLocEstoqEstab.sigla.IsEmpty() &&
                                r.FSParamLocEstoqEstab.capacidade > 0 &&
                                !r.Enderecamento.IsEmpty())*/

                            foreach (var item in ProductAddress)
                                Enderecamentos.Add(item);
                        }
                        catch
                        {
                        }

                        await CarregaDetailsProductAsync(retorno);
                    }
                    catch (Exception)
                    {
                        ShowShortMessage(Resources.ErrorMessagesResource.ProductNotFound);

                        evtTextFocused.Invoke("PRODUCT", null);

                        IsBusy = false;
                    }
                    finally
                    {
                        IsBusy = false;
                    }
                }
            }
            return retorno;
        }

        public async Task GetBasicProductByBarCodeAsync()
        {
            if (IsBusy)
                return;

            HASPRODUCTERROR = string.IsNullOrEmpty(PRODUCT);

            if (!HASPRODUCTERROR)
            {
                IsBusy = true;
                using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
                {
                    try
                    {
                        var result = await GetProductDataAPIAsync();
                        await GetProductSaldosAsync(result);
                        DETAILLOADED = true;
                    }
                    catch (Exception)
                    {
                        ShowShortMessage(Resources.ErrorMessagesResource.ProductNotFound);

                        evtTextFocused.Invoke("PRODUCT", null);

                        IsBusy = false;
                    }
                    finally
                    {
                        IsBusy = false;
                    }
                }
            }
        }

        public async Task GetDetailProductByBarCodeAsync()
        {
            if (IsBusy)
                return;

            HASPRODUCTERROR = string.IsNullOrEmpty(PRODUCT);

            if (!HASPRODUCTERROR)
            {
                IsBusy = true;
                using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
                {
                    try
                    {
                        Detalhes.Clear();
                        var result = await GetProductDataAPIAsync();

                        if (result == null || result.ProdutoParamEstab == null)
                        {
                            ShowShortMessage(
                                Resources.ErrorMessagesResource.ProductNotFound);
                            return;
                        }

                        var StockBalance = new FSSaldoEstoqueModel();
                         await GetProductSaldosAsync(new FSProdutoModel
                        {
                            codproduto = result.codproduto
                        });

                        if (Items.Any())
                            StockBalance = Items.Where(r => r.codestoque == result.ProdutoParamEstab.codestoque).FirstOrDefault();

                        Detalhes.Add(new GEDetalhesListagemModel
                        {
                            Description = Resources.FieldsNameResource.ProductKey,
                            Data = result.codproduto
                        });
                        Detalhes.Add(new GEDetalhesListagemModel
                        {
                            Description = Resources.FieldsNameResource.DescriptionProd,
                            Data = result.descricao
                        });
                        Detalhes.Add(new GEDetalhesListagemModel
                        {
                            Description = Resources.FieldsNameResource.DetailingProduct,
                            Data = result.detalhamento
                        });
                        Detalhes.Add(new GEDetalhesListagemModel
                        {
                            Description = Resources.FieldsNameResource.StockDefault,
                            Data = result.ProdutoParamEstab.codestoque
                        });
                        Detalhes.Add(new GEDetalhesListagemModel
                        {
                            Description = Resources.FieldsNameResource.StockBalance,
                            Data = StockBalance.saldoEstoque.ToString("n6")
                        });
                        Detalhes.Add(new GEDetalhesListagemModel
                        {
                            Description = Resources.FieldsNameResource.MinimumStock,
                            Data = result.ProdutoPcp.estoqueminimo.ToString("n6")
                        });
                        Detalhes.Add(new GEDetalhesListagemModel
                        {
                            Description = Resources.FieldsNameResource.MaximumStock,
                            Data = result.ProdutoPcp.estoquemaximo.ToString("n6")
                        });
                        Detalhes.Add(new GEDetalhesListagemModel
                        {
                            Description = Resources.FieldsNameResource.Unit,
                            Data = result.ProdutoParamEstab.codunidade
                        });
                        Detalhes.Add(new GEDetalhesListagemModel
                        {
                            Description = Resources.FieldsNameResource.NCM,
                            Data = result.ProdutoParamEstab.ncm
                        });
                        Detalhes.Add(new GEDetalhesListagemModel
                        {
                            Description = Resources.FieldsNameResource.LiquidWeight,
                            Data = result.ProdutoPcp.pesoliquido.ToString("n6")
                        });
                        Detalhes.Add(new GEDetalhesListagemModel
                        {
                            Description = Resources.FieldsNameResource.Weight,
                            Data = result.ProdutoPcp.pesobruto.ToString("n6")
                        });
                        Detalhes.Add(new GEDetalhesListagemModel
                        {
                            Description = Resources.FieldsNameResource.CubicSize,
                            Data = result.ProdutoPcp.tamanhocubico.ToString()
                        });
                        Detalhes.Add(new GEDetalhesListagemModel
                        {
                            Description = Resources.FieldsNameResource.PackageQtd,
                            Data = result.ProdutoPcp.qtddeembalagem.ToString()
                        });
                        Detalhes.Add(new GEDetalhesListagemModel
                        {
                            Description = Resources.FieldsNameResource.PackageByQtd,
                            Data = result.ProdutoPcp.qtdeporemb.ToString()
                        });
                        Detalhes.Add(new GEDetalhesListagemModel
                        {
                            Description = Resources.FieldsNameResource.Leadtime,
                            Data = result.ProdutoPcp.leadtime.ToString()
                        });
                        DETAILLOADED = true;
                    }
                    catch (Exception)
                    {
                        ShowShortMessage(Resources.ErrorMessagesResource.ProductNotFound);

                        evtTextFocused.Invoke("PRODUCT", null);

                        IsBusy = false;
                    }
                    finally
                    {
                        IsBusy = false;
                    }
                }
            }
        }

        #region Busca dados do produto na API
        private async Task<FSProdutoModel> GetProductDataAPIAsync() =>
            await _ProdutosService.GetProductByBarCodeAsync((short)CurrentApp.CODESTAB, PRODUCT);
        #endregion

        #region Carrega detalhes produto
        public async Task<bool> CarregaDetailsProductAsync(FSProdutoModel pProduct)
        {
            var retorno = false;
            if (pProduct != null)
            {
                if (STOCKORIGIN.IsEmpty())
                    STOCKORIGIN = pProduct.ProdutoParamEstab.codestoque;
                if (STOCKDESTINY.IsEmpty())
                    STOCKDESTINY = pProduct.estoquedestino;
                HASRAST = pProduct.ProdutoPcp.rastreabilidade;

                if (!pProduct.ProdutoParamEstab.codestoque.IsEmpty())
                {
                    SaldoEstoqueOrigin = await BuscaSaldoEstoqueAPI(pProduct.ProdutoParamEstab.codestoque, true);
                    UpdateSaldoEstoque(SaldoEstoqueOrigin, true);
                    retorno = true;
                }
                if (!pProduct.estoquedestino.IsEmpty())
                {
                    SaldoEstoqueDestiny = await BuscaSaldoEstoqueAPI(pProduct.estoquedestino, false);
                    UpdateSaldoEstoque(SaldoEstoqueDestiny, false);
                    retorno = true;
                }

                evtTextFocused?.Invoke(
                    string.IsNullOrEmpty(pProduct.estoquedestino) ?
                    "STOCKDESTINY" : HASRAST ? "LOTE" : "QUANTITY", null);
            }
            return retorno;
        }
        #endregion

        #region Busca todos os saldos do produto
        public async Task GetProductSaldosAsync(FSProdutoModel pProduct)
        {
            Items.Clear();
            if (pProduct != null)
            {
                var InventoryBalances = await _ProdutosService.GetProductInventoryBalancesAsync(new Models.Estoque.ESBuscaSaldoEstoqueModel
                {
                    CodProduto = pProduct.codproduto
                });

                foreach (var item in InventoryBalances.OrderByDescending(r => r.saldoEstoque))
                    Items.Add(item);
            }
        }
        #endregion

        #region Busca Saldo Estoque da API
        private async Task<FSSaldoEstoqueModel> BuscaSaldoEstoqueAPI(string pCodEstoque, bool isOrigem)
        {
            var result = await _ProdutosService.GetProductSaldoEstoqueAsync(new Models.Estoque.ESBuscaSaldoEstoqueModel
            {
                CodEstab = (int)CurrentApp.CODESTAB,
                CodEstoque = pCodEstoque,
                CodProduto = PRODUCT,
                SaldoAtual = 0
            });

            if (result == null)
                ShowShortMessage(
                    Resources.ErrorMessagesResource.ActualStockBalanceNotFound);

            return result;
        }
        #endregion

        #region Update saldo estoque
        private void UpdateSaldoEstoque(FSSaldoEstoqueModel pSaldo, bool isOrigem)
        {
            if (pSaldo != null && isOrigem)
            {
                STOCKORIGINSALDO = string.Format(
                    Resources.ValidationMessagesResource.ActualStockBalance,
                    pSaldo.saldoEstoque.ToString("n6"));
            }
            else if (pSaldo != null && !isOrigem)
            {
                STOCKDESTINYSALDO = string.Format(
                    Resources.ValidationMessagesResource.ActualStockBalance,
                    pSaldo.saldoEstoque.ToString("n6"));
            }
        }
        #endregion

        #region Busca saldo estoque
        public async Task BuscaSaldoEstoqueAsync(string pCodEstoque, bool isOrigem)
        {
            if (IsBusy)
                return;

            if (PRODUCT.IsEmpty())
            {
                ShowShortMessage(Resources.ValidationMessagesResource.ProductIsNullOrEmpty);

                HASPRODUCTERROR = string.IsNullOrEmpty(PRODUCT);
                return;
            }

            if (pCodEstoque.IsEmpty())
            {
                ShowShortMessage(
                   string.Format(Resources.ValidationMessagesResource.StockIsNullOrEmpty,
                   isOrigem ? "de origem" : "de destino"));
                return;
            }

            IsBusy = true;
            using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
            {
                try
                {
                    var result = await BuscaSaldoEstoqueAPI(pCodEstoque, isOrigem);
                    UpdateSaldoEstoque(result, isOrigem);
                }
                catch (Exception)
                {
                    ShowShortMessage(Resources.ErrorMessagesResource.ProductNotFound);

                    evtTextFocused.Invoke("PRODUCT", null);

                    IsBusy = false;
                }
                finally
                {
                    IsBusy = false;
                }
            }
        }
        #endregion
    }
}
