﻿using Hino.Estoque.App.Models.Manutencao;
using Hino.Estoque.App.Services.Application.Manutencao;
using Hino.Estoque.App.Utils.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using XF.Material.Forms.UI;
using XF.Material.Forms.UI.Dialogs;

namespace Hino.Estoque.App.Services.ViewModels.Manutencao
{
    public class CriarSolicManutencaoVM : BaseViewModel
    {
        public MNSearchManutModel _SearchManut { get; private set; }
        private readonly SolicManutService _SolicManutService;
        private MNEquipamentoModel _MNEquipamentoModel = null;
        private MNMotivoModel _MNMotivoModel = null;
        private MNTipoManutModel _MNTipoManutModel = null;

        public ICommand TapSearchCommand { protected set; get; }
        public ICommand CmdSolicManutClick { protected set; get; }

        public event EventHandler GoToMain;
        public event EventHandler evtTextFocused;

        public string LBLEQUIPMENTPLACEHOLDER => Resources.FieldsNameResource.Equipment;
        string _LBLEQUIPMENTHELPERTEXT = Resources.FieldsNameResource.Equipment;
        public string LBLEQUIPMENTHELPERTEXT
        {
            get => _LBLEQUIPMENTHELPERTEXT;
            set => SetProperty(ref _LBLEQUIPMENTHELPERTEXT, value);
        }
        public string LBLEQUIPMENTERRORTEXT => "Informe o equipamento";

        public string _Equipment;
        public string EQUIPMENT
        {
            get => _Equipment;
            set => SetProperty(ref _Equipment, value);
        }

        public bool _HasEquipmentError = false;
        public bool HASEQUIPMENTERROR
        {
            get => _HasEquipmentError;
            set => SetProperty(ref _HasEquipmentError, value);
        }

        public string LBLTIPOMANUTPLACEHOLDER => Resources.FieldsNameResource.TypeMaintenance;

        string _LBLTIPOMANUTHELPERTEXT = Resources.FieldsNameResource.TypeMaintenance;
        public string LBLTIPOMANUTHELPERTEXT
        {
            get => _LBLTIPOMANUTHELPERTEXT;
            set => SetProperty(ref _LBLTIPOMANUTHELPERTEXT, value);
        }
        
        public string LBLTIPOMANUTERRORTEXT => "Informe o tipo de manutenção";

        public bool _HasTipoManutError = false;
        public bool HASTIPOMANUTERROR
        {
            get => _HasTipoManutError;
            set => SetProperty(ref _HasTipoManutError, value);
        }

        public string _TipoManut;
        public string TIPOMANUT
        {
            get => _TipoManut;
            set => SetProperty(ref _TipoManut, value);
        }

        public string LBLMOTIVOMANUTPLACEHOLDER => Resources.FieldsNameResource.ReasonMaintenance;
        string _LBLMOTIVOMANUTHELPERTEXT = Resources.FieldsNameResource.ReasonMaintenance;
        public string LBLMOTIVOMANUTHELPERTEXT
        {
            get => _LBLMOTIVOMANUTHELPERTEXT;
            set => SetProperty(ref _LBLMOTIVOMANUTHELPERTEXT, value);
        }
        
        public string LBLMOTIVOMANUTERRORTEXT => "Informe o motivo de manutenção";

        public bool _HasMotivoManutError = false;
        public bool HASMOTIVOMANUTERROR
        {
            get => _HasMotivoManutError;
            set => SetProperty(ref _HasMotivoManutError, value);
        }

        public string _MotivoManut;
        public string MOTIVOMANUT
        {
            get => _MotivoManut ?? "";
            set => SetProperty(ref _MotivoManut, value);
        }

        private string _PRIORIDADE = "B";
        public string PRIORIDADE
        {
            get => _PRIORIDADE ?? "";
            set => SetProperty(ref _PRIORIDADE, value);
        }

        public string LBLNOTEPLACEHOLDER => Resources.FieldsNameResource.Note;
        public string LBLNOTEHELPERTEXT = Resources.FieldsNameResource.Note;

        public string _Note;
        public string NOTE
        {
            get => _Note ?? "";
            set => SetProperty(ref _Note, value);
        }

        public CriarSolicManutencaoVM()
        {
            Title = "Nova Solicitação";
            PageNum = 1;
            CmdSolicManutClick = new Command(OnSolicManutClick);
            TapSearchCommand = new Command(OnTapSearch);
            _SolicManutService = new SolicManutService();
            _MNEquipamentoModel = null;
            _MNMotivoModel = null;
            _MNTipoManutModel = null;
            _SearchManut = new MNSearchManutModel();
        }

        #region Ao clicar em Solicitar
        private async void OnSolicManutClick()
        {
            if (IsBusy)
                return;

            HASEQUIPMENTERROR = EQUIPMENT.IsEmpty();
            HASMOTIVOMANUTERROR = MOTIVOMANUT.IsEmpty();
            HASTIPOMANUTERROR = TIPOMANUT.IsEmpty();

            if (HASEQUIPMENTERROR &&
                HASMOTIVOMANUTERROR &&
                HASTIPOMANUTERROR)
                return;

            IsBusy = true;
            using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
            {
                try
                {
                    var ret = await _SolicManutService.PostSolicitarManutencaoAsync(new MNCriarSolicManutModel
                    {
                        codestab = (short)CurrentApp.CODESTAB,
                        codequipamento = EQUIPMENT.Trim().ToUpper(),
                        datasolic = DateTime.Now,
                        codmotivo = Convert.ToInt16(MOTIVOMANUT),
                        observacao = NOTE.Trim().ToUpper(),
                        codfuncionario = Convert.ToInt32(CurrentApp.UserLogged.GEFuncionarios.codfuncionario),
                        prioridade = PRIORIDADE.Trim().ToUpper(),
                        codtipomanut = TIPOMANUT.Trim().ToUpper()
                    });
                    
                    if (ret != null)
                    {
                        ShowShortMessage(Resources.ValidationMessagesResource.SaveSuccess);
                        GoToMain.Invoke(this, null);
                    }
                }
                catch (Exception e)
                {
                    ShowShortMessage(e.Message);

                    IsBusy = false;
                }
                finally
                {
                    IsBusy = false;
                }
            }
        }
        #endregion

        #region Busca dados do equipamento na API
        private async Task<MNEquipamentoModel> GetEquipmentDataAPIAsync() =>
            (await _SolicManutService.BuscaEquipamentoAsync(EQUIPMENT)).FirstOrDefault();
        #endregion

        #region Busca dados do tipo na API
        private async Task<MNTipoManutModel> GetTipoManutencaoAsync(int pTipo) =>
            (await _SolicManutService.BuscaTipoManutencaoAsync(pTipo)).FirstOrDefault();
        #endregion

        #region Busca dados do motivo na API
        private async Task<MNMotivoModel> GetMotivoDataAPIAsync(int pMotivo) =>
            (await _SolicManutService.BuscaMotivoManutencaoAsync(pMotivo)).FirstOrDefault();
        #endregion

        #region Busca dados do equipamento pelo codigo de barras
        public async Task GetEquipmentByBarCodeAsync()
        {
            if (IsBusy)
                return;

            HASEQUIPMENTERROR = EQUIPMENT.IsEmpty();

            if (!HASEQUIPMENTERROR)
            {
                IsBusy = true;
                using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
                {
                    try
                    {
                        var result = await GetEquipmentDataAPIAsync();
                        if (result == null)
                            throw new Exception();

                        LBLEQUIPMENTHELPERTEXT = $"{result.codequipamento} - {result.descricao}".SubStrTitle(50);
                        _MNEquipamentoModel = result;
                        // await CarregaDetailsProductAsync(result);
                    }
                    catch (Exception)
                    {
                        _MNEquipamentoModel = null;
                        LBLEQUIPMENTHELPERTEXT = Resources.ErrorMessagesResource.EquipmentNotFound;
                        ShowShortMessage(Resources.ErrorMessagesResource.EquipmentNotFound);

                        evtTextFocused.Invoke("EQUIPMENT", null);

                        IsBusy = false;
                    }
                    finally
                    {
                        IsBusy = false;
                    }
                }
            }
        }
        #endregion

        #region Busca dados do motivo pelo codigo de barras
        public async Task GetMotivoByBarCodeAsync()
        {
            if (IsBusy)
                return;

            HASMOTIVOMANUTERROR = MOTIVOMANUT.IsEmpty();

            if (!HASMOTIVOMANUTERROR)
            {
                IsBusy = true;
                using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
                {
                    try
                    {
                        var motivo = Convert.ToInt32(MOTIVOMANUT);

                        var result = await GetMotivoDataAPIAsync(motivo);
                        if (result == null)
                            throw new Exception();

                        LBLMOTIVOMANUTHELPERTEXT = $"{result.codmotivo} - {result.descricao}".SubStrTitle(50);
                        _MNMotivoModel = result;
                    }
                    catch (Exception)
                    {
                        _MNEquipamentoModel = null;
                        LBLEQUIPMENTHELPERTEXT = Resources.ErrorMessagesResource.ReasonNotFound;
                        ShowShortMessage(Resources.ErrorMessagesResource.ReasonNotFound);

                        evtTextFocused.Invoke("REASON", null);

                        IsBusy = false;
                    }
                    finally
                    {
                        IsBusy = false;
                    }
                }
            }
        }
        #endregion

        #region Busca dados do tipo pelo codigo de barras
        public async Task GetTipoByBarCodeAsync()
        {
            if (IsBusy)
                return;

            HASTIPOMANUTERROR = TIPOMANUT.IsEmpty();

            if (!HASTIPOMANUTERROR)
            {
                IsBusy = true;
                using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
                {
                    try
                    {
                        var tipomanut = Convert.ToInt32(TIPOMANUT);

                        var result = await GetTipoManutencaoAsync(tipomanut);
                        if (result == null)
                            throw new Exception();

                        LBLTIPOMANUTHELPERTEXT = $"{result.codtipomanut} - {result.descricao}".SubStrTitle(50);
                        _MNTipoManutModel = result;
                    }
                    catch (Exception)
                    {
                        _MNEquipamentoModel = null;
                        LBLEQUIPMENTHELPERTEXT = Resources.ErrorMessagesResource.TypeNotFound;
                        ShowShortMessage(Resources.ErrorMessagesResource.TypeNotFound);

                        evtTextFocused.Invoke("TYPE", null);

                        IsBusy = false;
                    }
                    finally
                    {
                        IsBusy = false;
                    }
                }
            }
        }
        #endregion

        #region Carregar Buscas da manutenção
        public async Task LoadSearchManutSolicAsync()
        {
            using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
            {
                _SearchManut = await _SolicManutService.SearchManutAsync((int)CurrentApp.CODESTAB);
                _SearchManut.Loaded = true;
            }
        }
        #endregion

        #region Ao clicar no search
        async void OnTapSearch(object origem)
        {
            if (!_SearchManut.Loaded)
                await LoadSearchManutSolicAsync();
            using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    var results = new List<string>();
                    var sortedResults = new List<string>();
                    var title = Resources.FieldsNameResource.Equipment;

                    if (origem.ToString() == "btnSearchEquipment")
                    {
                        title = Resources.FieldsNameResource.Equipment;
                        var listData = _SearchManut.Equipamentos.OrderBy(r => r.codequipamento);
                        sortedResults = listData.Select(r => r.codequipamento).ToList();
                        results = listData.Select(r => r.Detail).ToList();
                    }
                    else if (origem.ToString() == "btnSearchMotivoManut")
                    {
                        title = Resources.FieldsNameResource.ReasonMaintenance;
                        var listData = _SearchManut.Motivos.OrderBy(r => r.codmotivo);
                        sortedResults = listData.Select(r => r.codmotivo.ToString()).ToList();
                        results = listData.Select(r => r.Detail).ToList();
                    }
                    else if (origem.ToString() == "btnSearchTipoManut")
                    {
                        title = Resources.FieldsNameResource.TypeMaintenance;
                        var listData = _SearchManut.TipoManut.OrderBy(r => r.descricao);
                        sortedResults = listData.Select(r => r.codtipomanut.ToString()).ToList();
                        results = listData.Select(r => r.Detail).ToList();
                    }

                    var view = new MaterialRadioButtonGroup()
                    {
                        Choices = results
                    };

                    var SelecteIndex = await MaterialDialog.Instance.SelectChoiceAsync(
                        title: title,
                        choices: results);

                    if (SelecteIndex > -1)
                    {
                        var ValueSelected = sortedResults[SelecteIndex].Trim();
                        if (origem.ToString() == "btnSearchEquipment")
                        {
                            EQUIPMENT = ValueSelected;
                            await GetEquipmentByBarCodeAsync();
                        }
                        else if (origem.ToString() == "btnSearchMotivoManut")
                        {
                            MOTIVOMANUT = ValueSelected;
                            await GetMotivoByBarCodeAsync();
                        }
                        else if (origem.ToString() == "btnSearchTipoManut")
                        {
                            TIPOMANUT = ValueSelected;
                            await GetTipoByBarCodeAsync();
                        }
                    }
                });
            }
        }
        #endregion

    }


}
