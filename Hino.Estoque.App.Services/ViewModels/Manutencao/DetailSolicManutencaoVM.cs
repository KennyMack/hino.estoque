﻿using Hino.Estoque.App.Models.Manutencao;
using Hino.Estoque.App.Services.Application.Manutencao;
using Hino.Estoque.App.Utils.Interfaces;
using Hino.Estoque.App.Utils.Messages;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using XF.Material.Forms.UI.Dialogs;

namespace Hino.Estoque.App.Services.ViewModels.Manutencao
{
    public class DetailSolicManutencaoVM: BaseViewModel
    {
        public MNSolicManutModel _MNSolicManutModel { get; private set; }
        private readonly SolicManutService _SolicManutService;

        public event EventHandler<IMessageRaise> AprovarSolicRaise;
        private void OnAprovarSolicRaise(IMessageRaise e) =>
            AprovarSolicRaise?.Invoke(this, e);

        public event EventHandler<IMessageRaise> ReprovarSolicRaise;
        private void OnReprovarSolicRaise(IMessageRaise e) =>
            ReprovarSolicRaise?.Invoke(this, e);

        public ICommand CmdAprovarClicked { protected set; get; }
        public ICommand CmdReprovarClicked { protected set; get; }
        public event EventHandler GoToMain;

        public DetailSolicManutencaoVM(MNSolicManutModel pSolicManut)
        {
            _SolicManutService = new SolicManutService();
            Title = "Detalhes da solicitação";
            _MNSolicManutModel = pSolicManut;
            PageNum = 1;

            CmdAprovarClicked = new Command(OnAprovarClicked);
            CmdReprovarClicked = new Command(OnReprovarClicked);
        }

        public void OnAprovarClicked() =>
            OnAprovarSolicRaise(new MessageRaiseEvent(
                Resources.ValidationMessagesResource.Confirm,
                Resources.ValidationMessagesResource.ConfirmAprovSolic,
                Resources.ValidationMessagesResource.AproveSolic,
                Resources.ValidationMessagesResource.KeepPendingSolic));

        public void OnReprovarClicked() =>
            OnReprovarSolicRaise(new MessageRaiseEvent(
                Resources.ValidationMessagesResource.Confirm,
                Resources.ValidationMessagesResource.ConfirmReprovSolic,
                Resources.ValidationMessagesResource.ReproveSolic,
                Resources.ValidationMessagesResource.KeepPendingSolic));

        public async Task AproveSolicAsync()
        {
            if (IsBusy)
                return;
            IsBusy = true;
            using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
            {
                try
                {
                    var ret = await _SolicManutService.AprovarSolicManutAsync(_MNSolicManutModel);

                    if (ret != null)
                    {
                        ShowShortMessage(Resources.ValidationMessagesResource.SaveSuccess);
                        GoToMain.Invoke(this, null);
                    }
                }
                catch (Exception e)
                {
                    ShowShortMessage(e.Message);

                    IsBusy = false;
                }
                finally
                {
                    IsBusy = false;
                }
            }
        }

        public async Task ReproveSolicAsync()
        {
            if (IsBusy)
                return;
            IsBusy = true;
            using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
            {
                try
                {
                    var ret = await _SolicManutService.ReprovarSolicManutAsync(_MNSolicManutModel);

                    if (ret != null)
                    {
                        ShowShortMessage(Resources.ValidationMessagesResource.SaveSuccess);
                        GoToMain.Invoke(this, null);
                    }
                }
                catch (Exception e)
                {
                    ShowShortMessage(e.Message);

                    IsBusy = false;
                }
                finally
                {
                    IsBusy = false;
                }
            }
        }
    }
}
