﻿using Hino.Estoque.App.Models.Auth;
using Hino.Estoque.App.Models.Manutencao;
using Hino.Estoque.App.Services.Application.Manutencao;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.ViewModels.Manutencao
{
    public class SolicitacoesManutencaoVM : BaseViewModel
    {
        private readonly SolicManutService _SolicManutService;
        public ObservableCollection<MNSolicManutModel> Items { get; set; }
        private GEUsuariosLogadoModel _UserLogged;

        public SolicitacoesManutencaoVM()
        {
            Title = "Solic. de manutenção";
            Items = new ObservableCollection<MNSolicManutModel>();
            _SolicManutService = new SolicManutService();
            _UserLogged = CurrentApp.UserLogged;
            PageNum = 1;
        }

        #region Get Data From Source
        private async Task<IEnumerable<MNSolicManutModel>> GetDataFromSourceAsync()
        {
            return (await _SolicManutService.BuscaListaSolicManutAsync(new MNSolicForUserModel
            {
                codfuncionario = Convert.ToInt32(_UserLogged.GEFuncionarios.codfuncionario),
                codestab = _UserLogged.GEFuncionarios.codestab,
                codusuario = _UserLogged.GEUsuarios.codusuario
            })).OrderByDescending(r => r.codsolic);
        }
        #endregion

        protected override async Task LoadMoreData()
        {
            IsBusy = true;

            try
            {
                Items.CopyFromList(await GetDataFromSourceAsync(), false);
                IsBusy = false;
            }
            catch (Exception e)
            {
                ShowShortMessage(e.Message);

                IsBusy = false;
            }
            finally
            {
                IsBusy = false;
            }
        }

        protected override async Task RefreshData()
        {
            await base.RefreshData();

            IsBusy = true;

            PageNum = 1;
            Items.Clear();

            await LoadMoreData();
        }
    }
}
