﻿using Hino.Estoque.App.Models.Producao;
using Hino.Estoque.App.Utils.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.ViewModels.Producao
{
    public class DetailRelatorioEficienciaProdVM: BaseViewModel
    {
        readonly PDMaquinaGrupoModel _PDMaquinaGrupoModel;
        public ObservableCollection<PDMaquinaEficDetalheModel> Items { get; private set; }

        public DetailRelatorioEficienciaProdVM(PDMaquinaGrupoModel pMaquinaGrupo)
        {
            _PDMaquinaGrupoModel = pMaquinaGrupo;
            Title = pMaquinaGrupo.Maquina.SubStrTitle();
            Items = new ObservableCollection<PDMaquinaEficDetalheModel>();
            PageNum = 1;
        }

        protected override async Task LoadMoreData()
        {
            IsBusy = true;

            try
            {
                Items.CopyFromList(_PDMaquinaGrupoModel.Detalhes, false);
                IsBusy = false;
                await Task.Delay(1);
            }
            catch (Exception e)
            {
                ShowShortMessage(e.Message);

                IsBusy = false;
            }
            finally
            {
                IsBusy = false;
            }
        }

        protected override async Task RefreshData()
        {
            await base.RefreshData();

            IsBusy = true;

            PageNum = 1;
            Items.Clear();

            await LoadMoreData();
        }
    }
}
