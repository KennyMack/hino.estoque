﻿using Hino.Estoque.App.Models.Producao;
using Hino.Estoque.App.Utils.Paging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.ViewModels.Producao
{
    public class OrdemProdTransfVM : BaseViewModel
    {
        public ObservableCollection<PDOPTransfModel> Items { get; set; }
        private OPTransfResult _ListTransf;
        private readonly PDOrdemProdModel _PDOrdemProdModel;

        public OrdemProdTransfVM(PDOrdemProdModel pPDOrdemProdModel, OPTransfResult pListTransf)
        {
            _PDOrdemProdModel = pPDOrdemProdModel;
            _ListTransf = pListTransf;
            Title = $"Transferencia OP: {pPDOrdemProdModel.codordprod}-{pPDOrdemProdModel.nivelordprod}";
            base.PageNum = 1;
            Items = new ObservableCollection<PDOPTransfModel>();
        }

        #region Get Data From Source
        private async Task<PagedResult<PDOPTransfModel>> GetDataFromSourceAsync() =>
            await Task.Run(() =>
            {
                var retLst = new List<PDOPTransfModel>();

                foreach (var item in _ListTransf.TransfResults.Transferencias)
                {
                    var prod = _PDOrdemProdModel.pdordemprodcomp.Where(r => r.codcomponente == item.Transferencia.codcomponente).First();

                    retLst.Add(new PDOPTransfModel
                    {
                        codestab = prod.codestab,
                        codtransf = item.Transferencia.codtransf,
                        codordprod = item.Transferencia.codordprod,
                        nivelordprod = item.Transferencia.nivelordprod,
                        codestrutura = item.Transferencia.codestrutura,
                        codroteiro = item.Transferencia.codroteiro,
                        operacao = item.Transferencia.operacao,
                        codcomponente = prod.codcomponente,
                        codestoqueori = prod.codestoqueori,
                        codestoquedest = prod.codestoquedest,
                        Detail3 = prod.Detail3,
                        ProductDescription = $"{prod.codcomponente} - {prod.ProductDescription}",
                        SaldoOrigin = prod.saldoorigem,
                        QtdTotalNec = prod.quantidade,
                        QtdTransferida = item.Transferencia.quantidade,
                        Success = item.Sucesso,
                        Message = item.Motivo,
                        ColorResult = Xamarin.Forms.Color.FromHex(item.Sucesso ? "#05a05a" : "#B80000"),
                        DetailDest = prod.DetailDest,
                        DetailOrigin = prod.DetailOrigin,
                        TransfLote = item.Transferencia.TransfLote
                    });
                }

                return new PagedResult<PDOPTransfModel>
                {
                    CurrentPage = 1,
                    PageCount = 1,
                    PageSize = _ListTransf.TransfResults.Transferencias.Count,
                    RowCount = _ListTransf.TransfResults.Transferencias.Count,
                    Results = retLst.OrderBy(r => r.Sort)
                                    .ThenBy(r => r.codtransf)
                                    .ToList()
                };
            });
        #endregion

        protected async override Task LoadMoreData()
        {
            IsBusy = true;

            try
            {
                Items.CopyFromPagedResult(await GetDataFromSourceAsync(), false);
                IsBusy = false;
            }
            catch (Exception e)
            {
                ShowShortMessage(e.Message);

                IsBusy = false;
            }
            finally
            {
                IsBusy = false;
            }
        }

        protected async override Task RefreshData()
        {
            await base.RefreshData();

            IsBusy = true;

            base.PageNum = 1;
            Items.Clear();

            await LoadMoreData();
        }
    }
}
