﻿using Hino.Estoque.App.Models.Producao;
using Hino.Estoque.App.Services.Application.Estoque;
using Hino.Estoque.App.Services.DB.Interfaces.Producao;
using Hino.Estoque.App.Utils.Extensions;
using Hino.Estoque.App.Utils.Messages;
using Hino.Estoque.App.Utils.Paging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XF.Material.Forms.UI.Dialogs;

namespace Hino.Estoque.App.Services.ViewModels.Producao
{
    public class OrdemProdVM: BaseViewModel
    {
        public ObservableCollection<PDOrdemProdCompModel> Items { get; set; }
        public readonly PDOrdemProdModel _PDOrdemProdModel;
        private readonly IEnumerable<PDOrdemProdCompModel> _PDOrdemProdCompModel;
        private readonly OPTransferenciaService _OPTransferenciaService;
        public OPTransfResult _ResultTransf;
        public event EventHandler GoToTransfResult;
        public bool ShowResultTransf { get; set; }

        public OrdemProdVM(PDOrdemProdModel pPDOrdemProdModel)
        {
            _OPTransferenciaService = new OPTransferenciaService();
            _PDOrdemProdModel = pPDOrdemProdModel;
            _PDOrdemProdCompModel = _PDOrdemProdModel.pdordemprodcomp;

            Title = $"O.P. {_PDOrdemProdModel.codordprod}-{_PDOrdemProdModel.nivelordprod}";

            Items = new ObservableCollection<PDOrdemProdCompModel>();
            base.PageNum = 1;
        }

        #region Get Data From Source
        private async Task<PagedResult<PDOrdemProdCompModel>> GetDataFromSourceAsync() =>
            await _OPTransferenciaService.SearchAsync(_PDOrdemProdModel.codordprod, _PDOrdemProdModel.nivelordprod, _SearchData, base.PageNum);
        #endregion

        #region Search Data
        public async override Task SearchData_Changed()
        {
            base.PageNum = 1;

            Items.CopyFromPagedResult(await GetDataFromSourceAsync());
        }
        #endregion

        protected async override Task LoadMoreData()
        {
            IsBusy = true;

            try
            {
                await _OPTransferenciaService.LoadOPComponentsAsync(_PDOrdemProdModel);

                Items.CopyFromPagedResult(await GetDataFromSourceAsync(), false);
                IsBusy = false;
            }
            catch (Exception e)
            {
                ShowShortMessage(e.Message);

                IsBusy = false;
            }
            finally
            {
                IsBusy = false;
            }
        }

        protected async override Task RefreshData()
        {
            await base.RefreshData();

            IsBusy = true;

            base.PageNum = 1;
            Items.Clear();

            await LoadMoreData();
        }

        public async Task SaveValueAsync(OPComponentsResult pOPComponentsResult)
        {
            if (IsBusy)
                return;

            IsBusy = true;
            try
            {
                await _OPTransferenciaService.SaveValueAsync(pOPComponentsResult);
            }
            catch (Exception e)
            {
               this.Items.First(r => r.IdMobile == pOPComponentsResult.IdMobile)
                    .qtdSeparada = 0;
                this.Items.First(r => r.IdMobile == pOPComponentsResult.IdMobile)
                    .separado = false;
                ShowShortMessage(e.Message);

                IsBusy = false;
            }
            finally
            {
                IsBusy = false;
            }
        }

        public void ConfirmSave() =>
            OnMessageConfirmRaise(new MessageRaiseEvent(Resources.ValidationMessagesResource.ConfirmTyping, Resources.ValidationMessagesResource.Confirm));

        public async Task SyncValueAsync()
        {
            if (IsBusy)
                return;

            if (!CurrentApp.IsWIFI())
            {
                ShowShortMessage(Hino.Estoque.App.Resources.ErrorMessagesResource.NoInternetConnection);
                return;
            }

            IsBusy = true;
            using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
            {
                try
                {
                    var results = await _OPTransferenciaService.GetAllComponentsAlteradosAsync(_PDOrdemProdModel.codordprod, _PDOrdemProdModel.nivelordprod);

                    var lstResults = new List<PDOPCreateTransfModel>();

                    foreach (var item in results)
                    {
                        var OPComponent = new PDOPCreateTransfModel();
                        OPComponent.CopyProperties(item);
                        OPComponent.codusuario = CurrentApp.UserLogged.GEUsuarios.codusuario;
                        OPComponent.qtdseparada = item.qtdSeparada;
                        OPComponent.Identificador = Guid.NewGuid().ToString();
                        lstResults.Add(OPComponent);
                    }

                    _ResultTransf = await _OPTransferenciaService.PostOPSeparateAsync(_PDOrdemProdModel.codordprod, _PDOrdemProdModel.nivelordprod, lstResults.ToArray());
                    
                    ShowShortMessage(Resources.ValidationMessagesResource.SendWithSuccess);

                    GoToTransfResult?.Invoke(this, null);

                    /*
                    if (!ShowResultTransf)
                            GoToSearch?.Invoke(this, null);
                        else
                            GoToTransfResult?.Invoke(this, null);
                    }*/
                }
                catch (Exception e)
                {
                    ShowShortMessage(e.Message);

                    IsBusy = false;
                }
                finally
                {
                    IsBusy = false;
                }
            }
        }

        public async Task SyncValueItemsAsync()
        {
            if (IsBusy)
                return;

            if (!CurrentApp.IsWIFI())
            {
                ShowShortMessage(Hino.Estoque.App.Resources.ErrorMessagesResource.NoInternetConnection);
                return;
            }

            IsBusy = true;
            using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
            {
                try
                {
                    var _PDOPCreateTransfItemDB = DInjection.GetIntance<IPDOPCreateTransfItemDB>();
                    var results = await _OPTransferenciaService.GetAllComponentsAlteradosAsync(_PDOrdemProdModel.codordprod, _PDOrdemProdModel.nivelordprod);

                    var ComponentesTransf = (await _PDOPCreateTransfItemDB.GetAllCompTransfAsync(
                        (short)CurrentApp.CODESTAB, _PDOrdemProdModel.codordprod, _PDOrdemProdModel.nivelordprod)).Results;

                    var lstResults = new List<PDOPCreateTransfModel>();

                    foreach (var item in results)
                    {
                        var lstTransf = ComponentesTransf.Where(r =>
                            r.codestab == item.codestab &&
                            r.codordprod == item.codordprod &&
                            r.operacao == item.operacao &&
                            r.codcomponente == item.codcomponente
                        );
                        foreach (var local in lstTransf)
                        {
                            var OPComponent = new PDOPCreateTransfModel();
                            OPComponent.CopyProperties(item);
                            OPComponent.codusuario = CurrentApp.UserLogged.GEUsuarios.codusuario;
                            OPComponent.qtdseparada = local.qtdseparada;
                            OPComponent.codestoqueori = local.codestoqueori;
                            OPComponent.Identificador = Guid.NewGuid().ToString();
                            lstResults.Add(OPComponent);
                        }
                    }

                    _ResultTransf = await _OPTransferenciaService.PostOPSeparateAsync(_PDOrdemProdModel.codordprod, _PDOrdemProdModel.nivelordprod, lstResults.ToArray());

                    ShowShortMessage(Resources.ValidationMessagesResource.SendWithSuccess);

                    GoToTransfResult?.Invoke(this, null);
                }
                catch (Exception e)
                {
                    ShowShortMessage(e.Message);

                    IsBusy = false;
                }
                finally
                {
                    IsBusy = false;
                }
            }
        }
    }
}
