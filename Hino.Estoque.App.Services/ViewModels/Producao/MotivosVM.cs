﻿using Hino.Estoque.App.Models.Producao;
using Hino.Estoque.App.Services.Application.Producao.Apontamento;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.ViewModels.Producao
{
    public class MotivosVM : BaseViewModel
    {
        public bool IsEvent { get; set; }
        public ObservableCollection<PDMotivosModel> Motivos { get; set; }
        public ObservableCollection<PDMotivosModel> ParadaMotivos { get; set; }
        private readonly AppointmentService _AppointmentService;
        public MotivosVM()
        {
            Motivos = new ObservableCollection<PDMotivosModel>();
            ParadaMotivos = new ObservableCollection<PDMotivosModel>();
            _AppointmentService = new AppointmentService();
        }

        public async Task GetMotivosAsync()
        {
            if (IsBusy)
                return;

            IsBusy = true;
            try
            {
                Motivos = new ObservableCollection<PDMotivosModel>(
                    await _AppointmentService.GetRefugoMotivosAsync());
            }
            catch (Exception e)
            {
                ShowShortMessage(e.Message);

                IsBusy = false;
            }
            finally
            {
                IsBusy = false;
            }
        }

        public async Task GetStopMotivosAsync()
        {
            if (IsBusy)
                return;

            IsBusy = true;
            try
            {
                ParadaMotivos = new ObservableCollection<PDMotivosModel>(
                    await _AppointmentService.GetParadaMotivosAsync());
            }
            catch (Exception e)
            {
                ShowShortMessage(e.Message);

                IsBusy = false;
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
