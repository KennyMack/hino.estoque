﻿using Hino.Estoque.App.Models.Engenharia;
using Hino.Estoque.App.Services.Application.Producao.OEE;
using Hino.Estoque.App.Utils.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using XF.Material.Forms.UI;
using XF.Material.Forms.UI.Dialogs;

namespace Hino.Estoque.App.Services.ViewModels.Producao.OEE
{
    public class SearchOEEVM : BaseViewModel
    {
        public PageOrigem PageOrigem { get; private set; }
        public ENSearchOEEModel _SearchOEE { get; private set; }
        public readonly OEEService _OEEService;
        public event EventHandler GoToResultOEEMensal;
        public event EventHandler GoToResultOEE;

        public ICommand TapSearchCommand { protected set; get; }
        public ICommand CmdGeraOEEClick { protected set; get; }

        private ENMaquinasModel _ENMaquinasModel;
        private ENProcessosModel _ENProcessosModel;
        private ENProcAgrupModel _ENProcAgrupModel;

        string _LBLMAQUINAHELPERTEXT = "Máquina";
        public string LBLMAQUINAHELPERTEXT
        {
            get => _LBLMAQUINAHELPERTEXT;
            set => SetProperty(ref _LBLMAQUINAHELPERTEXT, value);
        }

        string _LBLPROCESSOHELPERTEXT = "Processo";
        public string LBLPROCESSOHELPERTEXT
        {
            get => _LBLPROCESSOHELPERTEXT;
            set => SetProperty(ref _LBLPROCESSOHELPERTEXT, value);
        }

        string _LBLAGRUPPROCESSOHELPERTEXT = "Agrup. Processo";
        public string LBLAGRUPPROCESSOHELPERTEXT
        {
            get => _LBLAGRUPPROCESSOHELPERTEXT;
            set => SetProperty(ref _LBLAGRUPPROCESSOHELPERTEXT, value);
        }

        private DateTime _DtInicio;
        public DateTime DTINICIO
        {
            get => _DtInicio;
            set => SetProperty(ref _DtInicio, value);
        }

        private DateTime _DtTermino;
        public DateTime DTTERMINO
        {
            get => _DtTermino;
            set => SetProperty(ref _DtTermino, value);
        }

        private bool _hasMaquinaError = false;
        public bool HASMAQUINAERROR
        {
            get => _hasMaquinaError;
            set => SetProperty(ref _hasMaquinaError, value);
        }

        private bool _hasAgrupProcesso = false;
        public bool HASAGRUPPROCESSO
        {
            get => _hasAgrupProcesso;
            set => SetProperty(ref _hasAgrupProcesso, value);
        }

        private string _MAQUINA;
        public string MAQUINA
        {
            get => _MAQUINA;
            set => SetProperty(ref _MAQUINA, value);
        }

        private string _AGRUPPROCESSO;
        public string AGRUPPROCESSO
        {
            get => _AGRUPPROCESSO;
            set => SetProperty(ref _AGRUPPROCESSO, value);
        }

        private string _TURNO;
        public string TURNO
        {
            get => _TURNO;
            set => SetProperty(ref _TURNO, value);
        }

        private bool _hasProcessoError = false;
        public bool HASPROCESSOERROR
        {
            get => _hasProcessoError;
            set => SetProperty(ref _hasProcessoError, value);
        }

        private string _PROCESSO;
        public string PROCESSO
        {
            get => _PROCESSO;
            set => SetProperty(ref _PROCESSO, value);
        }

        private bool _GerarDetalhes = false;
        public bool GERARDETALHES
        {
            get => _GerarDetalhes;
            set => SetProperty(ref _GerarDetalhes, value);
        }

        public SearchOEEVM(PageOrigem pageOrigem)
        {
            PageOrigem = pageOrigem;
            DTINICIO = DateTime.Now.FirstDay();
            DTTERMINO = DateTime.Now.LastDay();
            Title = pageOrigem == PageOrigem.OEE ? "Gerar OEE" : "Gerar OEE Mensal";
            _SearchOEE = new ENSearchOEEModel();
            _OEEService = new OEEService();
            TapSearchCommand = new Command(OnTapSearch);
            CmdGeraOEEClick = new Command(OnGerarOEE);
        }

        #region Gerar OEE
        private async Task GerarOEE()
        {
            try
            {
                if (DTINICIO.ToString("MM/yyyy") != DTTERMINO.ToString("MM/yyyy"))
                {
                    IsBusy = false;
                    ShowShortMessage("O período deve estar no mesmo mês.");
                    return;
                }

                if (DTTERMINO < DTINICIO)
                {
                    IsBusy = false;
                    ShowShortMessage("O término deve ser maior que o início.");
                    return;
                }

                if (MAQUINA.IsEmpty() && PROCESSO.IsEmpty())
                {
                    IsBusy = false;
                    ShowShortMessage("Necessário informar PROCESSO ou MÁQUINA.");
                    return;
                }

                if (!MAQUINA.IsEmpty() && !PROCESSO.IsEmpty())
                {
                    IsBusy = false;
                    ShowShortMessage("Não permitido informar PROCESSO e MÁQUINA.");
                    return;
                }

                short? codprocesso = null;
                if (short.TryParse(PROCESSO, out short nprocesso))
                    codprocesso = nprocesso;

                var turno = TURNO;

                if (turno.IsEmpty())
                    turno = "XX";

                var ret = await _OEEService.GerarOEEAsync((short)CurrentApp.CODESTAB, new Models.Producao.OEE.PDGerarOEEModel
                {
                    codestab = (short)CurrentApp.CODESTAB,
                    codusuario = CurrentApp.UserLogged.GEUsuarios.codusuario,
                    dataini = DTINICIO,
                    datafim = DTTERMINO,
                    codprocesso = codprocesso,
                    codmaquina = _ENMaquinasModel?.codmaquina,
                    gerardet = 1,
                    turno = turno
                });

                if (ret.IsEmpty())
                {
                    ret = "Gerado com sucesso.";
                    GoToResultOEE?.Invoke(null, null);
                }

                ShowShortMessage(ret);
            }
            catch (Exception e)
            {
                ShowShortMessage(e.Message);

                IsBusy = false;
            }
            finally
            {
                IsBusy = false;
            }
        }
        #endregion

        #region Gerar OEE Mensal
        private async Task GerarOEEMensal()
        {
            try
            {
                if (DTINICIO.ToString("MM/yyyy") != DTTERMINO.ToString("MM/yyyy"))
                {
                    IsBusy = false;
                    ShowShortMessage("O período deve estar no mesmo mês.");
                    return;
                }

                if (DTTERMINO < DTINICIO)
                {
                    IsBusy = false;
                    ShowShortMessage("O término deve ser maior que o início.");
                    return;
                }

                if (AGRUPPROCESSO.IsEmpty())
                {
                    IsBusy = false;
                    ShowShortMessage("Necessário informar o agrupamento de processo.");
                    return;
                }

                var turno = TURNO;

                if (turno.IsEmpty())
                    turno = "XX";

                var ret = await _OEEService.GerarOEEMensalAsync((short)CurrentApp.CODESTAB, new Models.Producao.OEE.PDGerarOEEModel
                {
                    codestab = (short)CurrentApp.CODESTAB,
                    codusuario = CurrentApp.UserLogged.GEUsuarios.codusuario,
                    dataini = DTINICIO,
                    datafim = DTTERMINO,
                    codprocesso = 0,
                    codagrup = _ENProcAgrupModel?.codgrupo,
                    codmaquina = _ENMaquinasModel?.codmaquina,
                    gerardet = 1,
                    turno = turno
                });

                if (ret.IsEmpty())
                {
                    ret = "Gerado com sucesso.";
                    GoToResultOEEMensal?.Invoke(null, null);
                }

                ShowShortMessage(ret);
            }
            catch (Exception e)
            {
                ShowShortMessage(e.Message);

                IsBusy = false;
            }
            finally
            {
                IsBusy = false;
            }
        }
        #endregion

        #region Gerar OEE Click
        async void OnGerarOEE()
        {
            if (IsBusy)
                return;

            IsBusy = true;
            using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
            {
                if (PageOrigem == PageOrigem.OEE)
                    await GerarOEE();
                else if (PageOrigem == PageOrigem.OEEMENSAL)
                    await GerarOEEMensal();
            }
        }
        #endregion

        #region Carregar Buscas do OEE
        public async Task LoadSearchOEEAsync()
        {
            using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
            {
                _SearchOEE = await _OEEService.SearchOEEAsync((int)CurrentApp.CODESTAB);
                _SearchOEE.Loaded = true;
            }
        }
        #endregion

        #region Ao clicar no search
        async void OnTapSearch(object origem)
        {
            if (!_SearchOEE.Loaded)
                await LoadSearchOEEAsync();
            using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    var results = new List<string>();
                    var sortedResults = new List<string>();
                    var title = "Máquinas";

                    if (origem.ToString() == "btnSearchMaquina")
                    {
                        title = "Máquinas";
                        var listData = _SearchOEE.Maquinas.OrderBy(r => r.codmaquina);
                        sortedResults = listData.Select(r => r.codmaquina).ToList();
                        results = listData.Select(r => r.Detail).ToList();
                    }
                    if (origem.ToString() == "btnSearchMaquinaFiltro")
                    {
                        title = "Máquinas";
                        var listData = _SearchOEE.Maquinas.Where(r =>
                            r.descricao.ToUpper().Contains(MAQUINA) ||
                            r.codmaquina.ToUpper().Contains(MAQUINA)
                        ).OrderBy(r => r.codmaquina);
                        sortedResults = listData.Select(r => r.codmaquina).ToList();
                        results = listData.Select(r => r.Detail).ToList();
                    }
                    else if (origem.ToString() == "btnSearchProcesso")
                    {
                        title = "Processos";
                        var listData = _SearchOEE.Processos.OrderBy(r => r.codprocesso);
                        sortedResults = listData.Select(r => r.codprocesso.ToString()).ToList();
                        results = listData.Select(r => r.Detail).ToList();
                    }
                    else if (origem.ToString() == "btnSearchAgrupProcesso")
                    {
                        title = "Agrup. Processos";
                        var listData = _SearchOEE.Agrupamentos.OrderBy(r => r.codgrupo);
                        sortedResults = listData.Select(r => r.codgrupo.ToString()).ToList();
                        results = listData.Select(r => r.Detail).ToList();
                    }

                    var view = new MaterialRadioButtonGroup()
                    {
                        Choices = results
                    };

                    var SelecteIndex = await MaterialDialog.Instance.SelectChoiceAsync(
                        title: title,
                        choices: results);

                    if (SelecteIndex > -1)
                    {
                        var ValueSelected = sortedResults[SelecteIndex].Trim();
                        if (origem.ToString() == "btnSearchMaquina" ||
                        origem.ToString() == "btnSearchMaquinaFiltro")
                        {
                            MAQUINA = ValueSelected;
                            await GetMaquinaAsync();
                        }
                        else if (origem.ToString() == "btnSearchProcesso")
                        {
                            PROCESSO = ValueSelected;
                            await GetProcessoAsync();
                        }
                        else if (origem.ToString() == "btnSearchAgrupProcesso")
                        {
                            AGRUPPROCESSO = ValueSelected;
                            await GetProcessoAsync();
                        }

                    }
                });
            }
        }
        #endregion

        #region Busca dados da máquina pelo codigo de barras
        public async Task GetMaquinaAsync()
        {
            if (IsBusy)
                return;

            IsBusy = true;
            using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
            {
                if (MAQUINA.IsEmpty())
                {
                    _ENMaquinasModel = null;
                    LBLMAQUINAHELPERTEXT = "";
                    IsBusy = false;
                    return;
                }

                try
                {
                    if (!_SearchOEE.Loaded)
                        await LoadSearchOEEAsync();

                    var result = _SearchOEE.Maquinas.FirstOrDefault(r => r.codmaquina == MAQUINA);
                    if (result == null)
                    {
                        var list = _SearchOEE.Maquinas.Where(r =>
                            r.descricao.ToUpper().Contains(MAQUINA) ||
                            r.codmaquina.ToUpper().Contains(MAQUINA)
                        ).ToArray();
                        if (!list.Any())
                            throw new Exception();

                        OnTapSearch("btnSearchMaquinaFiltro");
                        return;
                        // throw new Exception();
                    }

                    LBLMAQUINAHELPERTEXT = $"{result.codmaquina} - {result.descricao}".SubStrTitle(50);
                    _ENMaquinasModel = result;
                }
                catch (Exception)
                {
                    _ENMaquinasModel = null;
                    LBLMAQUINAHELPERTEXT = "Máquina não encontrada";
                    ShowShortMessage("Máquina não encontrada");

                    IsBusy = false;
                }
                finally
                {
                    IsBusy = false;
                }
            }
        }
        #endregion

        #region Busca dados do processo pelo codigo de barras
        public async Task GetProcessoAsync()
        {
            if (IsBusy)
                return;

            IsBusy = true;
            using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
            {
                if (PROCESSO.IsEmpty())
                {
                    _ENProcessosModel = null;
                    LBLPROCESSOHELPERTEXT = "";
                    IsBusy = false;
                    return;
                }

                try
                {
                    if (!_SearchOEE.Loaded)
                        await LoadSearchOEEAsync();

                    var result = _SearchOEE.Processos.FirstOrDefault(r => r.codprocesso.ToString() == PROCESSO);
                    if (result == null)
                        throw new Exception();

                    LBLPROCESSOHELPERTEXT = $"{result.codprocesso} - {result.descricao}".SubStrTitle(50);
                    _ENProcessosModel = result;
                }
                catch (Exception)
                {
                    _ENProcessosModel = null;
                    LBLPROCESSOHELPERTEXT = "Processo não encontrado";
                    ShowShortMessage("Processo não encontrado");

                    IsBusy = false;
                }
                finally
                {
                    IsBusy = false;
                }
            }
        }
        #endregion

        #region Busca dados do agrup. processo pelo codigo de barras
        public async Task GetAgrupProcessoAsync()
        {
            if (IsBusy)
                return;

            IsBusy = true;
            using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
            {
                if (AGRUPPROCESSO.IsEmpty())
                {
                    _ENProcAgrupModel = null;
                    LBLAGRUPPROCESSOHELPERTEXT = "";
                    IsBusy = false;
                    return;
                }

                try
                {
                    if (!_SearchOEE.Loaded)
                        await LoadSearchOEEAsync();

                    var result = _SearchOEE.Agrupamentos.FirstOrDefault(r => r.codgrupo.ToString() == AGRUPPROCESSO);
                    if (result == null)
                        throw new Exception();

                    LBLAGRUPPROCESSOHELPERTEXT = $"{result.codgrupo} - {result.descricao}".SubStrTitle(50);
                    _ENProcAgrupModel = result;
                }
                catch (Exception)
                {
                    _ENProcAgrupModel = null;
                    LBLAGRUPPROCESSOHELPERTEXT = "Agrup. Processo não encontrado";
                    ShowShortMessage("Agrup. Processo não encontrado");

                    IsBusy = false;
                }
                finally
                {
                    IsBusy = false;
                }
            }
        }
        #endregion
    }
}
