﻿using Hino.Estoque.App.Models.Producao.OEE;
using Hino.Estoque.App.Services.Application.Producao.OEE;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.ViewModels.Producao.OEE
{
    public class ListOEEVM : BaseViewModel
    {
        public ObservableCollection<PDOEEResultadoModel> Items { get; set; }
        public readonly OEEService _OEEService;
        public List<PDOEEParam> OEECores;
        public List<PDOEEJustificativa> OEEJustificativas;

        private string _MAQUINA;
        public string MAQUINA
        {
            get => _MAQUINA;
            set => SetProperty(ref _MAQUINA, value);
        }

        private DateTime _DtInicio;
        public DateTime DTINICIO
        {
            get => _DtInicio;
            set => SetProperty(ref _DtInicio, value);
        }

        private DateTime _DtTermino;
        public DateTime DTTERMINO
        {
            get => _DtTermino;
            set => SetProperty(ref _DtTermino, value);
        }

        private decimal _AverageOee;
        public decimal AverageOee
        {
            get => _AverageOee;
            set => SetProperty(ref _AverageOee, value);
        }

        private decimal _AverageQuality;
        public decimal AverageQuality
        {
            get => _AverageQuality;
            set => SetProperty(ref _AverageQuality, value);
        }
        private decimal _AverageEfic;
        public decimal AverageEfic
        {
            get => _AverageEfic;
            set => SetProperty(ref _AverageEfic, value);
        }

        private decimal _AverageDisp;
        public decimal AverageDisp
        {
            get => _AverageDisp;
            set => SetProperty(ref _AverageDisp, value);
        }

        private short CodEstab;
        private string CodUsuario;

        public ListOEEVM(DateTime pInicio, DateTime pTermino, string pCodMaquina)
        {
            DTINICIO = pInicio;
            DTTERMINO = pTermino;
            MAQUINA = pCodMaquina;

            Title = $"OEE {pInicio:dd/MM} - {pTermino:dd/MM}";
            OEECores = new List<PDOEEParam>();
            OEEJustificativas = new List<PDOEEJustificativa>();

            _OEEService = new OEEService();
            Items = new ObservableCollection<PDOEEResultadoModel>();
            CodEstab = (short)CurrentApp.CODESTAB;
            CodUsuario = CurrentApp.UserLogged.GEUsuarios.codusuario;
        }


        #region Get Decimal Value
        private decimal GetDecimalValueDate(int pDay, PDOEEListagemModel pOEEItem)
        {
            switch (pDay)
            {
                case 1:
                    return Convert.ToDecimal(pOEEItem.C_01);
                case 2:
                    return Convert.ToDecimal(pOEEItem.C_02);
                case 3:
                    return Convert.ToDecimal(pOEEItem.C_03);
                case 4:
                    return Convert.ToDecimal(pOEEItem.C_04);
                case 5:
                    return Convert.ToDecimal(pOEEItem.C_05);
                case 6:
                    return Convert.ToDecimal(pOEEItem.C_06);
                case 7:
                    return Convert.ToDecimal(pOEEItem.C_07);
                case 8:
                    return Convert.ToDecimal(pOEEItem.C_08);
                case 9:
                    return Convert.ToDecimal(pOEEItem.C_09);
                case 10:
                    return Convert.ToDecimal(pOEEItem.C_10);
                case 11:
                    return Convert.ToDecimal(pOEEItem.C_11);
                case 12:
                    return Convert.ToDecimal(pOEEItem.C_12);
                case 13:
                    return Convert.ToDecimal(pOEEItem.C_13);
                case 14:
                    return Convert.ToDecimal(pOEEItem.C_14);
                case 15:
                    return Convert.ToDecimal(pOEEItem.C_15);
                case 16:
                    return Convert.ToDecimal(pOEEItem.C_16);
                case 17:
                    return Convert.ToDecimal(pOEEItem.C_17);
                case 18:
                    return Convert.ToDecimal(pOEEItem.C_18);
                case 19:
                    return Convert.ToDecimal(pOEEItem.C_19);
                case 20:
                    return Convert.ToDecimal(pOEEItem.C_20);
                case 21:
                    return Convert.ToDecimal(pOEEItem.C_21);
                case 22:
                    return Convert.ToDecimal(pOEEItem.C_22);
                case 23:
                    return Convert.ToDecimal(pOEEItem.C_23);
                case 24:
                    return Convert.ToDecimal(pOEEItem.C_24);
                case 25:
                    return Convert.ToDecimal(pOEEItem.C_25);
                case 26:
                    return Convert.ToDecimal(pOEEItem.C_26);
                case 27:
                    return Convert.ToDecimal(pOEEItem.C_27);
                case 28:
                    return Convert.ToDecimal(pOEEItem.C_28);
                case 29:
                    return Convert.ToDecimal(pOEEItem.C_29);
                case 30:
                    return Convert.ToDecimal(pOEEItem.C_30);
                case 31:
                    return Convert.ToDecimal(pOEEItem.C_31);
                default:
                    return 0;
            }
        }
        #endregion

        #region Get String Value
        private string GetStringValueDate(int pDay, PDOEEListagemModel pOEEItem)
        {
            switch (pDay)
            {
                case 1:
                    return Convert.ToString(pOEEItem.C_01);
                case 2:
                    return Convert.ToString(pOEEItem.C_02);
                case 3:
                    return Convert.ToString(pOEEItem.C_03);
                case 4:
                    return Convert.ToString(pOEEItem.C_04);
                case 5:
                    return Convert.ToString(pOEEItem.C_05);
                case 6:
                    return Convert.ToString(pOEEItem.C_06);
                case 7:
                    return Convert.ToString(pOEEItem.C_07);
                case 8:
                    return Convert.ToString(pOEEItem.C_08);
                case 9:
                    return Convert.ToString(pOEEItem.C_09);
                case 10:
                    return Convert.ToString(pOEEItem.C_10);
                case 11:
                    return Convert.ToString(pOEEItem.C_11);
                case 12:
                    return Convert.ToString(pOEEItem.C_12);
                case 13:
                    return Convert.ToString(pOEEItem.C_13);
                case 14:
                    return Convert.ToString(pOEEItem.C_14);
                case 15:
                    return Convert.ToString(pOEEItem.C_15);
                case 16:
                    return Convert.ToString(pOEEItem.C_16);
                case 17:
                    return Convert.ToString(pOEEItem.C_17);
                case 18:
                    return Convert.ToString(pOEEItem.C_18);
                case 19:
                    return Convert.ToString(pOEEItem.C_19);
                case 20:
                    return Convert.ToString(pOEEItem.C_20);
                case 21:
                    return Convert.ToString(pOEEItem.C_21);
                case 22:
                    return Convert.ToString(pOEEItem.C_22);
                case 23:
                    return Convert.ToString(pOEEItem.C_23);
                case 24:
                    return Convert.ToString(pOEEItem.C_24);
                case 25:
                    return Convert.ToString(pOEEItem.C_25);
                case 26:
                    return Convert.ToString(pOEEItem.C_26);
                case 27:
                    return Convert.ToString(pOEEItem.C_27);
                case 28:
                    return Convert.ToString(pOEEItem.C_28);
                case 29:
                    return Convert.ToString(pOEEItem.C_29);
                case 30:
                    return Convert.ToString(pOEEItem.C_30);
                case 31:
                    return Convert.ToString(pOEEItem.C_31);
                default:
                    return "";
            }
        }
        #endregion

        public void ConvertListagem(List<PDOEEListagemModel> pListagem)
        {
            Items.Clear();

            var Dias = DTTERMINO.Day - DTINICIO.Day;

            for (int i = 0; i <= Dias; i++)
            {
                Items.Add(new PDOEEResultadoModel
                {
                    Date = new DateTime(DTINICIO.Year, DTINICIO.Month, DTINICIO.Day + i),
                    Meta = GetDecimalValueDate(DTINICIO.Day + i, pListagem[0]),
                    Producao = GetDecimalValueDate(DTINICIO.Day + i, pListagem[1]),
                    Refugo = GetDecimalValueDate(DTINICIO.Day + i, pListagem[2]),
                    HorasTotais = GetStringValueDate(DTINICIO.Day + i, pListagem[3]),
                    HorasDisponiveis = GetStringValueDate(DTINICIO.Day + i, pListagem[4]),
                    HorasTrabalhadas = GetStringValueDate(DTINICIO.Day + i, pListagem[5]),
                    HorasParadas = GetStringValueDate(DTINICIO.Day + i, pListagem[6]),
                    OEE = GetDecimalValueDate(DTINICIO.Day + i, pListagem[7]) * 100,
                    OEEColor = GetOEEColor(0, GetDecimalValueDate(DTINICIO.Day + i, pListagem[7])),
                    EficProducao = GetDecimalValueDate(DTINICIO.Day + i, pListagem[8]) * 100,
                    EficProducaoColor = GetOEEColor(1, GetDecimalValueDate(DTINICIO.Day + i, pListagem[8])),
                    Disponibilidade = GetDecimalValueDate(DTINICIO.Day + i, pListagem[9]) * 100,
                    Qualidade = GetDecimalValueDate(DTINICIO.Day + i, pListagem[10]) * 100,
                    TemJustificativa = OEEJustificativas.Any(r => r.codmaquina == MAQUINA && r.dataoee == new DateTime(DTINICIO.Year, DTINICIO.Month, DTINICIO.Day + i))
                });
            }

            AverageOee = Items.Average(r => r.OEE);
            AverageEfic = Items.Average(r => r.EficProducao);
            AverageDisp = Items.Average(r => r.Disponibilidade);
            AverageQuality = Items.Average(r => r.Qualidade);
        }

        public string GetJustificativa(DateTime pDate)
        {
            return string.Join(System.Environment.NewLine, 
                OEEJustificativas.Where(r => 
                    r.codmaquina == MAQUINA &&
                    r.dataoee == pDate).Select(r => r.justificativa));
        }

        public Color GetOEEColor(int tipo, decimal Oee)
        {
            var Cor = OEECores.FirstOrDefault(r =>
                r.tipo == tipo &&
                (r.inicio.CompareTo(Oee) <= 0) && (Oee.CompareTo(r.termino) <= 0)
            );
            if (Cor == null)
                return Color.Black;

            return Cor.CorEscolhida;
        }

        protected async override Task LoadMoreData()
        {
            IsBusy = true;

            try
            {
                var resultado = await _OEEService.ListarOEEAsync(CodEstab, CodUsuario, DTINICIO, MAQUINA);
                OEECores = resultado.Colors;
                OEEJustificativas = resultado.Justificativas;
                ConvertListagem(resultado.Results);
                IsBusy = false;
            }
            catch (Exception e)
            {
                ShowShortMessage(e.Message);

                IsBusy = false;
            }
            finally
            {
                IsBusy = false;
            }
        }

        protected async override Task RefreshData()
        {
            await base.RefreshData();

            IsBusy = true;

            base.PageNum = 1;
            Items.Clear();

            await LoadMoreData();
        }
    }
}
