﻿using Hino.Estoque.App.Models.Producao.OEE;
using Hino.Estoque.App.Services.Application.Producao.OEE;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.ViewModels.Producao.OEE
{
    public class ListOEEMensalVM : BaseViewModel
    {
        public ObservableCollection<PDOEEMensalResultadoModel> Items { get; set; }
        public readonly OEEService _OEEService;
        public List<PDOEEParam> OEECores;

        private DateTime _DtInicio;
        public DateTime DTINICIO
        {
            get => _DtInicio;
            set => SetProperty(ref _DtInicio, value);
        }

        private DateTime _DtTermino;
        public DateTime DTTERMINO
        {
            get => _DtTermino;
            set => SetProperty(ref _DtTermino, value);
        }

        private readonly short CodEstab;
        private readonly string CodUsuario;

        public ListOEEMensalVM(DateTime pInicio, DateTime pTermino)
        {
            DTINICIO = pInicio;
            DTTERMINO = pTermino;

            Title = $"OEE Mensal {pInicio:dd/MM} - {pTermino:dd/MM}";
            OEECores = new List<PDOEEParam>();

            _OEEService = new OEEService();
            Items = new ObservableCollection<PDOEEMensalResultadoModel>();
            CodEstab = (short)CurrentApp.CODESTAB;
            CodUsuario = CurrentApp.UserLogged.GEUsuarios.codusuario;
        }

        public void ConvertListagem(List<PDOEEMensalResultadoModel> pListagem)
        {
            Items.Clear();

            foreach (var item in pListagem)
            {
                Items.Add(new PDOEEMensalResultadoModel
                {
                    codmaq = item.codmaq,
                    enmaquinasdescricao = item.enmaquinasdescricao,
                    meta = item.meta,
                    producao = item.producao,
                    refugo = item.refugo,
                    horastrab = item.horastrab,
                    horasparada = item.horasparada,
                    oee = item.oee * 100,
                    oeeColor = GetOEEColor(0, item.oee),
                    disponibilidade = item.disponibilidade,
                    produtividade = item.produtividade,
                    qualidade = item.qualidade
                });
            }
        }

        public Color GetOEEColor(int tipo, decimal Oee)
        {
            var Cor = OEECores.FirstOrDefault(r =>
                r.tipo == tipo &&
                (r.inicio.CompareTo(Oee) <= 0) && (Oee.CompareTo(r.termino) <= 0)
            );
            if (Cor == null)
                return Color.Black;

            return Cor.CorEscolhida;
        }

        protected async override Task LoadMoreData()
        {
            IsBusy = true;

            try
            {
                var resultado = await _OEEService.ListarOEEMensalAsync(CodEstab, CodUsuario, DTINICIO);
                OEECores = resultado.Colors;
                ConvertListagem(resultado.Results);
                IsBusy = false;
            }
            catch (Exception e)
            {
                ShowShortMessage(e.Message);

                IsBusy = false;
            }
            finally
            {
                IsBusy = false;
            }
        }

        protected async override Task RefreshData()
        {
            await base.RefreshData();

            IsBusy = true;

            base.PageNum = 1;
            Items.Clear();

            await LoadMoreData();
        }
    }
}
