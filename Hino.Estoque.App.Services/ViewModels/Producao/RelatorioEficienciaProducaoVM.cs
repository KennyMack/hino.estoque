﻿using Hino.Estoque.App.Services.Application.Producao.Apontamento;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Hino.Estoque.App.Utils.Extensions;
using Hino.Estoque.App.Models.Producao;
using XF.Material.Forms.UI.Dialogs;

namespace Hino.Estoque.App.Services.ViewModels.Producao
{
    public class RelatorioEficienciaProducaoVM : BaseViewModel
    {
        public ObservableCollection<PDMaquinaGrupoModel> Items { get; private set; }
        public ObservableCollection<PDMaquinaEficDetalheModel> ItemsDetail { get; private set; }
        private readonly AppointmentService _AppointmentService;
        private List<PDRelatorioEficienciaProducaoModel> _Relatorio;
        private DateTime _DataInicio = DateTime.Now;
        public DateTime DATAINICIO
        {
            get => _DataInicio;
            set => SetProperty(ref _DataInicio, value);
        }

        private DateTime _DataTermino = DateTime.Now;
        public DateTime DATATERMINO
        {
            get => _DataTermino;
            set => SetProperty(ref _DataTermino, value);
        }

        private bool _HasLoaded = false;
        public bool HasLoaded
        {
            get => _HasLoaded;
            set
            {
                HasNotLoaded = !value;
                SetProperty(ref _HasLoaded, value);
            }
        }

        private bool _HasNotLoaded = true;
        public bool HasNotLoaded
        {
            get => _HasNotLoaded;
            set => SetProperty(ref _HasNotLoaded, value);
        }

        public ICommand CmdLoadDataClick { get; private set; }

        public RelatorioEficienciaProducaoVM()
        {
            CmdLoadDataClick = new Command(async () => await OnLoadDataClick());
            _AppointmentService = new AppointmentService();
            Title = "Eficiência de produção";
            Items = new ObservableCollection<PDMaquinaGrupoModel>();
            ItemsDetail = new ObservableCollection<PDMaquinaEficDetalheModel>();
            PageNum = 1;
        }

        private async Task OnLoadDataClick()
        {
            if (IsBusy)
                return;

            IsBusy = true;
            using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
            {
                try
                {
                    await _AppointmentService.GenerateAndLoadEficienciaProdAsync(DATAINICIO, DATATERMINO);

                    _Relatorio = new List<PDRelatorioEficienciaProducaoModel>(
                        await _AppointmentService.GetRelatorioEficienciaProdAsync());

                    await LoadMoreData();

                    HasLoaded = true;
                    IsBusy = false;
                }
                catch (Exception e)
                {
                    ShowShortMessage(e.Message);

                    IsBusy = false;
                }
                finally
                {
                    IsBusy = false;
                }
            }
        }

        protected override async Task LoadMoreData()
        {
            IsBusy = true;

            try
            {
                var agrupado = _Relatorio
                   .Select(r => new PDMaquinaGrupoModel
                   {
                       Maquina = r.Maquina,
                       Processo = r.Processo,
                       Turno = r.Turno,
                       QtdMaquinas = r.QtdMaquinas,
                       EficTotal = r.EficTotal,
                       Expanded = false
                   })
                   .GroupBy(r => r.Maquina)
                   .Select(r => r.First())
                   .ToList();

                ItemsDetail = new ObservableCollection<PDMaquinaEficDetalheModel>(_Relatorio.Select(r => new PDMaquinaEficDetalheModel
                {
                    Maquina = r.Maquina,
                    Eficiencia = r.Eficiencia,
                    MetaDia = r.MetaDia,
                    OP = r.OP,
                    PercRefugo = r.PercRefugo,
                    Produto = r.Produto,
                    QtdeProd = r.QtdeProd,
                    QtdeRef = r.QtdeRef,
                    TotalProgramado = r.TotalProgramado
                }).ToList());

                foreach (var item in agrupado)
                {
                    item.Detalhes = ItemsDetail.Where(r => r.Maquina == item.Maquina).ToArray();
                    Items.Add(item);
                }
                IsBusy = false;
                await Task.Delay(1);
            }
            catch (Exception e)
            {
                ShowShortMessage(e.Message);

                IsBusy = false;
            }
            finally
            {
                IsBusy = false;
            }
        }

        protected override async Task RefreshData()
        {
            await base.RefreshData();

            IsBusy = true;

            PageNum = 1;
            Items.Clear();

            await LoadMoreData();
        }
    }
}
