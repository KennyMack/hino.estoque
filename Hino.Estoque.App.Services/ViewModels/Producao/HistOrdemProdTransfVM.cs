﻿using Hino.Estoque.App.Models.Producao;
using Hino.Estoque.App.Services.DB.Interfaces.Producao;
using Hino.Estoque.App.Utils.Paging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.ViewModels.Producao
{
    public class HistOrdemProdTransfVM : BaseViewModel
    {
        public ObservableCollection<PDOPHistTransf> Items { get; set; }
        private IPDOPCreateTransfDB _IPDOPCreateTransfDB;

        public HistOrdemProdTransfVM()
        {
            _IPDOPCreateTransfDB = DInjection.GetIntance<IPDOPCreateTransfDB>();
            Items = new ObservableCollection<PDOPHistTransf>();
            base.PageNum = 1;
            Title = $"Hist. Transferencia OP";
        }

        #region Get Data From Source
        private async Task<PagedResult<PDOPCreateTransfModel>> GetDataFromSourceAsync() =>
            await _IPDOPCreateTransfDB.GetItemsAsync(base.PageNum);
        #endregion

        protected async override Task LoadMoreData()
        {
            IsBusy = true;

            try
            {
                var results = await GetDataFromSourceAsync();

                var lstResult = results.Results.Select(r => new PDOPHistTransf
                {
                    OP = $"{r.codordprod}-{r.nivelordprod}",
                    DataTransf = r.datatransf,
                    IdMobile = r.IdMobile,
                    CodProduto = r.codcomponente,
                    CodEstoqueOri = r.codestoqueori,
                    CodEstoqueDest = r.codestoquedest,
                    Quantidade = r.quantidade,
                    QtdSeparada = r.qtdseparada,
                    Complete = r.complete,
                    Observacao = r.observacao,
                    Color = Xamarin.Forms.Color.FromHex(r.complete ? "#05a05a" : "#B80000")
                });

                foreach (var item in lstResult)
                {
                    Items.Add(item);
                }

                // Items.CopyFromPagedResult(results, false);
                IsBusy = false;
            }
            catch (Exception e)
            {
                ShowShortMessage(e.Message);

                IsBusy = false;
            }
            finally
            {
                IsBusy = false;
            }
        }

        protected async override Task RefreshData()
        {
            await base.RefreshData();

            IsBusy = true;

            base.PageNum = 1;
            Items.Clear();

            await LoadMoreData();
        }
    }
}
