﻿using Hino.Estoque.App.Models.Engenharia;
using Hino.Estoque.App.Models.Producao;
using Hino.Estoque.App.Models.Producao.Apontamento;
using Hino.Estoque.App.Services.Application.Producao.Apontamento;
using Hino.Estoque.App.Utils.Extensions;
using Hino.Estoque.App.Utils.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using XF.Material.Forms.UI;
using XF.Material.Forms.UI.Dialogs;

namespace Hino.Estoque.App.Services.ViewModels.Producao.Apontamento
{
    public class StartProductionVM : BaseViewModel
    {
        public ICommand TapSearchCommand { protected set; get; }

        private readonly PDOrdemProdModel _PDOrdemProdModel;
        public PDAptInicioProdModel LastEventOpenned { get; private set; }
        public ENMaquinasModel[] Machines { get; set; }
        public ENMaquinasModel _ENMaquinasModel;

        private bool LoadedMachines;

        public int SelectedOperation { get; set; }
        public bool FinishLastStopOpenned { get; set; }

        string _LBLMAQUINAHELPERTEXT = "Máquina";
        public string LBLMAQUINAHELPERTEXT
        {
            get => _LBLMAQUINAHELPERTEXT;
            set => SetProperty(ref _LBLMAQUINAHELPERTEXT, value);
        }

        private bool _hasMaquinaError = false;
        public bool HASMAQUINAERROR
        {
            get => _hasMaquinaError;
            set => SetProperty(ref _hasMaquinaError, value);
        }

        private string _MAQUINA;
        public string MAQUINA
        {
            get => _MAQUINA;
            set => SetProperty(ref _MAQUINA, value);
        }

        public StartProductionVM(PDOrdemProdModel pOp)
        {
            LoadedMachines = false;
            _PDOrdemProdModel = pOp;
            base.PageNum = 1;
            TapSearchCommand = new Command(OnTapSearch);
        }

        public async Task LoadSearchOEEAsync()
        {
            if (!LoadedMachines)
            {
                LoadedMachines = true;
                using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
                {
                    var _AppointmentService = new AppointmentService();
                    Machines = (await _AppointmentService.GetMachinesAsync()).ToArray();
                }
            }
        }


        #region Ao clicar no search
        async void OnTapSearch(object origem)
        {
            if (!LoadedMachines)
                await LoadSearchOEEAsync();
            using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    var results = new List<string>();
                    var sortedResults = new List<string>();
                    var title = "Máquinas";

                    if (origem.ToString() == "btnSearchMaquina")
                    {
                        title = "Máquinas";
                        var listData = Machines.OrderBy(r => r.codmaquina);
                        sortedResults = listData.Select(r => r.codmaquina).ToList();
                        results = listData.Select(r => r.Detail).ToList();
                    }
                    if (origem.ToString() == "btnSearchMaquinaFiltro")
                    {
                        title = "Máquinas";
                        var listData = Machines.Where(r =>
                            r.descricao.ToUpper().Contains(MAQUINA) ||
                            r.codmaquina.ToUpper().Contains(MAQUINA)
                        ).OrderBy(r => r.codmaquina);
                        sortedResults = listData.Select(r => r.codmaquina).ToList();
                        results = listData.Select(r => r.Detail).ToList();
                    }

                    var view = new MaterialRadioButtonGroup()
                    {
                        Choices = results
                    };

                    var SelecteIndex = await MaterialDialog.Instance.SelectChoiceAsync(
                        title: title,
                        choices: results);

                    if (SelecteIndex > -1)
                    {
                        var ValueSelected = sortedResults[SelecteIndex].Trim();
                        if (origem.ToString() == "btnSearchMaquina" ||
                        origem.ToString() == "btnSearchMaquinaFiltro")
                        {
                            MAQUINA = ValueSelected;
                            await GetMaquinaAsync();
                        }
                    }
                });
            }
        }
        #endregion

        #region Busca dados da máquina pelo codigo de barras
        public async Task GetMaquinaAsync()
        {
            if (!CurrentApp.UserLogged.GESettings.Settings.MachineOnStartAppointment)
                return;

            if (IsBusy)
                return;

            IsBusy = true;
            using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
            {
                if (MAQUINA.IsEmpty())
                {
                    _ENMaquinasModel = null;
                    LBLMAQUINAHELPERTEXT = "";
                    IsBusy = false;
                    return;
                }

                try
                {
                    if (!LoadedMachines)
                        await LoadSearchOEEAsync();

                    var result = Machines.FirstOrDefault(r => r.codmaquina == MAQUINA);
                    if (result == null)
                    {
                        var list = Machines.Where(r =>
                            r.descricao.ToUpper().Contains(MAQUINA) ||
                            r.codmaquina.ToUpper().Contains(MAQUINA)
                        ).ToArray();
                        if (!list.Any())
                            throw new Exception();

                        OnTapSearch("btnSearchMaquinaFiltro");
                        return;
                        // throw new Exception();
                    }

                    LBLMAQUINAHELPERTEXT = $"{result.codmaquina} - {result.descricao}".SubStrTitle(50);
                    _ENMaquinasModel = result;
                }
                catch (Exception)
                {
                    _ENMaquinasModel = null;
                    LBLMAQUINAHELPERTEXT = "Máquina não encontrada";
                    ShowShortMessage("Máquina não encontrada");

                    IsBusy = false;
                }
                finally
                {
                    IsBusy = false;
                }
            }
        }
        #endregion


        public async Task<bool?> HasLastEventOpenned()
        {
            if (IsBusy)
                return null;

            IsBusy = true;
            using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
            {
                try
                {
                    LastEventOpenned = null;
                    var OperacaoApontamento = _PDOrdemProdModel.pdordemprodrotinas.Where(r => r.operacao == SelectedOperation)
                             .First();
                    var _AppointmentService = new AppointmentService();

                    var ObjInicio = new Models.Producao.Apontamento.PDAptInicioProdModel
                    {
                        codestab = (int)CurrentApp.CODESTAB,
                        codiniapt = 0,
                        operacao = OperacaoApontamento.operacao,
                        codroteiro = OperacaoApontamento.codroteiro,
                        codordprod = OperacaoApontamento.codordprod,
                        codestrutura = OperacaoApontamento.codestrutura,
                        nivelordprod = OperacaoApontamento.nivelordprod,
                        codusuario = CurrentApp.UserLogged.GEUsuarios.codusuario,
                        codfuncionario = CurrentApp.UserLogged.GEFuncionarios.codfuncionario,
                        dtinicio = DateTime.Now,
                        tipo = 7
                    };

                    LastEventOpenned = await _AppointmentService.GetAptInicioAsync(ObjInicio);

                    if (LastEventOpenned.codiniapt > 0)
                    {
                        OnMessageConfirmRaise(new MessageRaiseEvent(
                            Resources.ValidationMessagesResource.Confirm,
                            Resources.ValidationMessagesResource.FinishLastStopOpened,
                            Resources.ValidationMessagesResource.Close,
                            Resources.ValidationMessagesResource.DoNotClose));
                    }
                }
                catch (Exception e)
                {
                    LastEventOpenned = null;
                    ShowShortMessage(e.Message);

                    IsBusy = false;
                }
                finally
                {
                    IsBusy = false;
                }
            }

            return LastEventOpenned?.codiniapt > 0;
        }
    }
}
