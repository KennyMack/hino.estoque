﻿using Hino.Estoque.App.Models.Producao;
using Hino.Estoque.App.Models.Producao.Apontamento;
using Hino.Estoque.App.Services.Application.Producao.Apontamento;
using Hino.Estoque.App.Utils.Extensions;
using Hino.Estoque.App.Utils.Messages;
using Hino.Estoque.App.Utils.Paging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XF.Material.Forms.UI.Dialogs;

namespace Hino.Estoque.App.Services.ViewModels.Producao.Apontamento
{
    public class AptTerminoProdVM : BaseViewModel
    {
        public ObservableCollection<PDAptLancRefugoModel> RefugoMotivo { get; set; }
        public int? MotivoSelecionado { get; set; }
        public string Observacao { get; set; }

        PDOrdemProdModel _OrdemProd;

        int _Operacao;
        decimal _QtdBoas;
        decimal _QtdRefugo;
        string _Observacao;

        public event EventHandler GoToStart;
        public AptTerminoProdVM(PDOrdemProdModel pDOrdemProd, int pOperacao,
            decimal pQtdBoas,
            decimal pQtdRefugo,
            string pObservacao)
        {
            _OrdemProd = pDOrdemProd;
            _Operacao = pOperacao;
            _QtdBoas = pQtdBoas;
            _QtdRefugo = pQtdRefugo;
            _Observacao = pObservacao;
            RefugoMotivo = new ObservableCollection<PDAptLancRefugoModel>();
        }

        public decimal QtdBoas { get => _QtdBoas; }
        public decimal QtdRefugo { get => _QtdRefugo; }
        public string OPDescription { get => $"{_OrdemProd.codordprod} - {_OrdemProd.nivelordprod}"; }
        public string OPProduct { get => $"{ _OrdemProd.codproduto} - {_OrdemProd.fsproduto.descricao}"; }
        public string Usuario { get => $"{ CurrentApp.UserLogged.GEUsuarios.nome } "; }
        public DateTime OPDTTermino => DateTime.Now;
        public string Operacao
        {
            get
            {
                if (_Operacao > -1)
                {
                    try
                    {
                        return _OrdemProd.pdordemprodrotinas.Where(r => r.operacao == _Operacao)
                             .First()
                             .DetailOperacao;
                    }
                    catch (Exception)
                    {
                    }
                }

                return "";
            }
        }

        public bool ValidarRefugos()
        {
            if (_QtdRefugo > 0 && RefugoMotivo.Sum(r => r.Quantidade) != _QtdRefugo)
            {
                ShowLongMessage(string.Format(Resources.ValidationMessagesResource.BadQtdInvalid, _QtdRefugo, RefugoMotivo.Sum(r => r.Quantidade)));
                return false;
            }
            return true;
        }

        public async Task Finalizar()
        {
            if (IsBusy)
                return;

            IsBusy = true;
            using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
            {
                try
                { 
                    if (_QtdRefugo > 0 && RefugoMotivo.Sum(r => r.Quantidade) != _QtdRefugo)
                        throw new Exception(string.Format(Resources.ValidationMessagesResource.BadQtdInvalid, _QtdRefugo, RefugoMotivo.Sum(r => r.Quantidade)));

                    var OperacaoApontamento = _OrdemProd.pdordemprodrotinas.Where(r => r.operacao == _Operacao)
                             .First();
                    var _AppointmentService = new AppointmentService();

                    var Inicio = new PDAptInicioProdModel
                    {
                        codestab = (int)CurrentApp.CODESTAB,
                        operacao = OperacaoApontamento.operacao,
                        codroteiro = OperacaoApontamento.codroteiro,
                        codordprod = OperacaoApontamento.codordprod,
                        codestrutura = OperacaoApontamento.codestrutura,
                        nivelordprod = OperacaoApontamento.nivelordprod,
                        codfuncionario = CurrentApp.UserLogged.GEFuncionarios.codfuncionario,
                        tipo = 3,
                        codiniapt = 0
                    };
                    var PDAptInicio = await _AppointmentService.GetAptInicioAsync(Inicio);

                    if (PDAptInicio == null || PDAptInicio?.codiniapt == 0)
                        throw new Exception(Resources.ValidationMessagesResource.StartAppointmentNotFound);

                    var results = await _AppointmentService.FinalizarProducaoAsync(new PDAptTerminoProdModel
                    {
                        codestab = (short)CurrentApp.CODESTAB,
                        codiniapt = PDAptInicio.codiniapt,
                        codfimapt = 0,
                        codusuario = CurrentApp.UserLogged.GEUsuarios.codusuario,
                        codfuncionario = (int)CurrentApp.UserLogged.GEFuncionarios.codfuncionario,
                        dttermino = DateTime.Now,
                        quantidade = this.QtdBoas,
                        qtdrefugo = this.QtdRefugo,
                        qtdretrabalho = 0,
                        tipo = 4,
                        observacao = _Observacao,
                        Refugos = RefugoMotivo
                    });
                    if (!results.askstartnew)
                    {
                        OnMessageRaise(new MessageRaiseEvent(
                                Resources.ValidationMessagesResource.Complete,
                                Resources.ValidationMessagesResource.AppointmentFinishedWithSuccess));
                        GoToStart?.Invoke(this, null);
                    }
                    else
                    {
                        var start = new StartProductionVM(_OrdemProd)
                        {
                            SelectedOperation = _Operacao
                        };
                        OnMessageConfirmRaise(new MessageRaiseEvent(
                            Resources.ValidationMessagesResource.Confirm,
                            Resources.ValidationMessagesResource.StartNewAppointmentConfirm,
                            start));
                    }
                }
                catch (Exception e)
                {
                    ShowShortMessage(e.Message);

                    IsBusy = false;
                }
                finally
                {
                    IsBusy = false;
                }
            }
        }

        public async Task FinalizarTurnoAsync()
        {
            if (IsBusy)
                return;

            IsBusy = true;
            using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
            {
                try
                {
                    var OperacaoApontamento = _OrdemProd.pdordemprodrotinas.Where(r => r.operacao == _Operacao)
                             .First();
                    var _AppointmentService = new AppointmentService();

                    var Inicio = new PDAptInicioProdModel
                    {
                        codestab = (int)CurrentApp.CODESTAB,
                        operacao = OperacaoApontamento.operacao,
                        codroteiro = OperacaoApontamento.codroteiro,
                        codordprod = OperacaoApontamento.codordprod,
                        codestrutura = OperacaoApontamento.codestrutura,
                        nivelordprod = OperacaoApontamento.nivelordprod,
                        codfuncionario = CurrentApp.UserLogged.GEFuncionarios.codfuncionario,
                        tipo = 3,
                        codiniapt = 0
                    };
                    var PDAptInicio = await _AppointmentService.GetAptInicioAsync(Inicio);

                    if (PDAptInicio == null || PDAptInicio?.codiniapt == 0)
                        throw new Exception(Resources.ValidationMessagesResource.StartAppointmentNotFound);

                    var results = await _AppointmentService.FinalizarTurnoProducaoAsync(new PDAptTerminoProdModel
                    {
                        codestab = (short)CurrentApp.CODESTAB,
                        codiniapt = PDAptInicio.codiniapt,
                        codfimapt = 0,
                        codusuario = CurrentApp.UserLogged.GEUsuarios.codusuario,
                        codfuncionario = (int)CurrentApp.UserLogged.GEFuncionarios.codfuncionario,
                        dttermino = DateTime.Now,
                        quantidade = 0,
                        qtdrefugo = 0,
                        qtdretrabalho = 0,
                        tipo = 13,
                        observacao = "",
                        Refugos = RefugoMotivo
                    });
                    if (!results.askstartnew)
                    {
                        OnMessageRaise(new MessageRaiseEvent(
                                Resources.ValidationMessagesResource.Complete,
                                Resources.ValidationMessagesResource.AppointmentTurnFinishedWithSuccess));
                        GoToStart?.Invoke(this, null);
                    }
                    else
                    {
                        var start = new StartProductionVM(_OrdemProd)
                        {
                            SelectedOperation = _Operacao
                        };
                        OnMessageConfirmRaise(new MessageRaiseEvent(
                            Resources.ValidationMessagesResource.Confirm,
                            Resources.ValidationMessagesResource.StartNewAppointmentConfirm,
                            start));
                    }
                }
                catch (Exception e)
                {
                    ShowShortMessage(e.Message);

                    IsBusy = false;
                }
                finally
                {
                    IsBusy = false;
                }
            }
        }

        public bool ValidaMotivoParada()
        {
            if (MotivoSelecionado == null)
            {
                ShowShortMessage(Resources.ErrorMessagesResource.StopReasonNotInformed);
                return false;
            }
            return true;
        }

        public async Task FinalizarParadaAsync()
        {
            if (IsBusy)
                return;

            IsBusy = true;
            using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
            {
                try
                {
                    var OperacaoApontamento = _OrdemProd.pdordemprodrotinas.Where(r => r.operacao == _Operacao)
                             .First();
                    var _AppointmentService = new AppointmentService();

                    var Inicio = new PDAptInicioProdModel
                    {
                        codestab = (int)CurrentApp.CODESTAB,
                        operacao = OperacaoApontamento.operacao,
                        codroteiro = OperacaoApontamento.codroteiro,
                        codordprod = OperacaoApontamento.codordprod,
                        codestrutura = OperacaoApontamento.codestrutura,
                        nivelordprod = OperacaoApontamento.nivelordprod,
                        codfuncionario = CurrentApp.UserLogged.GEFuncionarios.codfuncionario,
                        tipo = 7,
                        codiniapt = 0
                    };
                    var PDAptInicio = await _AppointmentService.GetAptInicioAsync(Inicio);

                    if (PDAptInicio == null || PDAptInicio?.codiniapt == 0)
                        throw new Exception(Resources.ValidationMessagesResource.StartAppointmentNotFound);

                    if (MotivoSelecionado == null && PDAptInicio.codmotivo == null)
                    {
                        OnMessageRaise(new MessageRaiseEvent(
                            Resources.ValidationMessagesResource.InvalidData,
                            Resources.ErrorMessagesResource.StopReasonNotInformed));
                        IsBusy = false;
                        return;
                    }

                    if (MotivoSelecionado == null)
                        MotivoSelecionado = PDAptInicio.codmotivo;

                    var codmotivo = Convert.ToInt32(MotivoSelecionado);
                    RefugoMotivo.Clear();
                    var results = await _AppointmentService.FinalizarParadaAsync(new PDAptTerminoProdModel
                    {
                        codestab = (short)CurrentApp.CODESTAB,
                        codiniapt = PDAptInicio.codiniapt,
                        codfimapt = 0,
                        codusuario = CurrentApp.UserLogged.GEUsuarios.codusuario,
                        codfuncionario = (int)CurrentApp.UserLogged.GEFuncionarios.codfuncionario,
                        dttermino = DateTime.Now,
                        quantidade = 0,
                        qtdrefugo = 0,
                        qtdretrabalho = 0,
                        tipo = 8,
                        observacao = Observacao,
                        codmotivo = codmotivo,
                        Refugos = RefugoMotivo
                    });
                    OnMessageRaise(new MessageRaiseEvent(
                            Resources.ValidationMessagesResource.Complete,
                            Resources.ValidationMessagesResource.StopFinishedWithSuccess));

                    GoToStart?.Invoke(this, null);
                }
                catch (Exception e)
                {
                    ShowShortMessage(e.Message);

                    IsBusy = false;
                }
                finally
                {
                    IsBusy = false;
                }
            }
        }
    }
}
