﻿using Hino.Estoque.App.Models.Producao;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.ViewModels.Producao.Apontamento
{
    public class StartEventVM: BaseViewModel
    {
        private readonly PDOrdemProdModel _PDOrdemProdModel;

        public int SelectedOperation { get; set; }
        public PDMotivosModel SelectedReason { get; set; }

        public StartEventVM(PDOrdemProdModel pOp)
        {
            _PDOrdemProdModel = pOp;
            base.PageNum = 1;
        }

    }
}
