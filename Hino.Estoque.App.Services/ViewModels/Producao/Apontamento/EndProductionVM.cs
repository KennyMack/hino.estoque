﻿using Hino.Estoque.App.Models.Producao;
using Hino.Estoque.App.Models.Producao.Apontamento;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;

namespace Hino.Estoque.App.Services.ViewModels.Producao.Apontamento
{
    public class EndProductionVM : BaseViewModel
    {
        public ObservableCollection<PDAptLancRefugoModel> RefugoMotivo { get; set; }
        public event EventHandler ReasonListChanged;
        public readonly PDOrdemProdModel _PDOrdemProdModel;

        public int SelectedOperation { get; set; }
        public PDMotivosModel SelectedReason { get; set; }

        private string _RefugoRestanteDesc = "";
        public string RefugoRestanteDesc
        {
            get => _RefugoRestanteDesc;
            set => SetProperty(ref _RefugoRestanteDesc, value);
        }

        private decimal _QtdBoas = 0;
        public decimal QtdBoas
        {
            get => _QtdBoas;
            set => SetProperty(ref _QtdBoas, value);
        }

        private decimal _QtdRefugo = 0;
        public decimal QtdRefugo
        {
            get => _QtdRefugo;
            set => SetProperty(ref _QtdRefugo, value);
        }

        private string _Observacao = "";
        public string Observacao
        {
            get => _Observacao;
            set => SetProperty(ref _Observacao, value);
        }

        bool _ApontarPeso;
        public bool ApontarPeso
        {
            get => _ApontarPeso;
            set => SetProperty(ref _ApontarPeso, value);
        }

        private decimal _QtdItemRefugo = 0;
        public decimal QtdItemRefugo
        {
            get => _QtdItemRefugo;
            set => SetProperty(ref _QtdItemRefugo, value);
        }

        bool _ShowMainAppointment;
        public bool ShowMainAppointment
        {
            get => _ShowMainAppointment;
            set => SetProperty(ref _ShowMainAppointment, value);
        }

        bool _ShowRefugoDetail;
        public bool ShowRefugoDetail
        {
            get => _ShowRefugoDetail;
            set => SetProperty(ref _ShowRefugoDetail, value);
        }

        bool _ShowFinalConfirm;
        public bool ShowFinalConfirm
        {
            get => _ShowFinalConfirm;
            set => SetProperty(ref _ShowFinalConfirm, value);
        }

        public EndProductionVM(PDOrdemProdModel pOp)
        {
            _PDOrdemProdModel = pOp;
            SelectedReason = null;
            ShowMainAppointment = true;
            ShowRefugoDetail = false;
            ShowFinalConfirm = false;
            RefugoMotivo = new ObservableCollection<PDAptLancRefugoModel>();
            RefugoMotivo.CollectionChanged -= RefugoMotivo_CollectionChanged;
            RefugoMotivo.CollectionChanged += RefugoMotivo_CollectionChanged;
            RecalculaRefugoRestante();
            base.PageNum = 1;
        }

        public bool ValidateContinue()
        {
            if (ShowMainAppointment)
            {
                if (SelectedOperation <= 0)
                {
                    ShowLongMessage(Resources.ValidationMessagesResource.OperationNotSelected);
                    return false;
                }
                else if (QtdBoas <= 0)
                {
                    ShowLongMessage(Resources.ValidationMessagesResource.QuantityIsNotValid);
                    return false;
                }
                else if (QtdRefugo < 0)
                {
                    ShowLongMessage(Resources.ValidationMessagesResource.QuantityIsNotValid);
                    return false;
                }
            }
            else if (ShowRefugoDetail)
            {
                if (QtdRefugo != RefugoMotivo.Sum(r => r.Quantidade))
                {
                    ShowLongMessage(string.Format(
                        Resources.ValidationMessagesResource.BadQtdInvalid,
                        QtdRefugo, RefugoMotivo.Sum(r => r.Quantidade)
                    ));
                    return false;
                }
            }

            return true;
        }

        private void RefugoMotivo_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e) =>
            RefreshListMotivo();

        private void RefreshListMotivo()
        {
            RecalculaRefugoRestante();
            ReasonListChanged?.Invoke(this, new EventArgs());
        }

        public void RecalculaRefugoRestante()
        {
            try
            {
                RefugoRestanteDesc = $"Refugo: {RefugoMotivo.Sum(r => r.Quantidade):n4} de {QtdRefugo:n4}";
            }
            catch (Exception)
            {
            }
        }

        public void AddToReasonList(decimal pQtdItemRefugo)
        {
            if (IsBusy)
                return;

            if (pQtdItemRefugo <= 0)
            {
                ShowLongMessage(Resources.ValidationMessagesResource.QuantityIsNotValid);
                return;
            }

            if (SelectedReason == null)
            {
                ShowLongMessage(Resources.ValidationMessagesResource.ReasonInvalid);
                return;
            }

            try
            {
                IsBusy = true;

                var itemExists = RefugoMotivo.Where(r => r.CodMotivo == SelectedReason.codmotivo).FirstOrDefault();

                if (itemExists != null)
                {
                    itemExists.Quantidade += pQtdItemRefugo;
                    itemExists.ApontarPeso = ApontarPeso;
                    RefreshListMotivo();
                }
                else
                {
                    RefugoMotivo.Add(new PDAptLancRefugoModel
                    {
                        CodMotivo = SelectedReason.codmotivo,
                        Descricao = SelectedReason.descricao,
                        Quantidade = pQtdItemRefugo,
                        ApontarPeso = ApontarPeso
                    });
                }
                SelectedReason = null;
                QtdItemRefugo = 0;
            }
            catch (Exception e)
            {
                ShowShortMessage(e.Message);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
