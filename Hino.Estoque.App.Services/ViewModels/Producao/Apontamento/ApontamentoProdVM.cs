﻿using Hino.Estoque.App.Models.Producao;
using Hino.Estoque.App.Models.Producao.Apontamento;
using Hino.Estoque.App.Services.Application.Producao.Apontamento;
using Hino.Estoque.App.Utils.Paging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using XF.Material.Forms.UI.Dialogs;

namespace Hino.Estoque.App.Services.ViewModels.Producao.Apontamento
{
    public class ApontamentoProdVM : BaseViewModel
    {
        public ObservableCollection<PDAptLancamentosModel> Items { get; set; }
        public ObservableCollection<PDOrdemProdRotinasModel> Rotinas { get; set; }
        public ObservableCollection<PDMotivosModel> Motivos { get; set; }
        public ObservableCollection<PDMotivosModel> ParadaMotivos { get; set; }
        public ObservableCollection<string> RotinasNome { get; set; }
        public ObservableCollection<string> MotivosNome { get; set; }
        public ObservableCollection<string> ParadaMotivosNome { get; set; }

        AppointmentService _AppointmentService;

        private bool _IsSelected;
        public bool IsSelected
        {
            get => _IsSelected;
            set  
            {
                IsNotSelected = !value;
                SetProperty(ref _IsSelected, value);
            }
        }

        private bool _IsNotSelected = true;
        public bool IsNotSelected
        {
            get => _IsNotSelected;
            set => SetProperty(ref _IsNotSelected, value);
        }

        private bool _IsFinish = false;
        public bool IsFinish
        {
            get => _IsFinish;
            set
            {
                SetProperty(ref _IsFinish, value);
                OnPropertyChanged("ShowFinishContinue");
            }
        }

        private bool _IsFinishParada = false;
        public bool IsFinishParada
        {
            get => _IsFinishParada;
            set
            {
                SetProperty(ref _IsFinishParada, value);
                OnPropertyChanged("ShowFinishParadaContinue");
            }
        }

        private bool _IsTurnFinish = false;
        public bool IsTurnFinish
        {
            get => _IsTurnFinish;
            set
            {
                SetProperty(ref _IsTurnFinish, value);
                OnPropertyChanged("ShowTurnFinishContinue");
            }
        }

        private bool _IsStart = false;
        public bool IsStart
        {
            get => _IsStart;
            set => SetProperty(ref _IsStart, value);
        }

        private bool _IsStartParada = false;
        public bool IsStartParada
        {
            get => _IsStartParada;
            set => SetProperty(ref _IsStartParada, value);
        }

        private bool _ShowRefugoPanel = false;
        public bool ShowRefugoPanel
        {
            get => _ShowRefugoPanel;
            set
            {
                NotShowRefugoPanel = !value;
                SetProperty(ref _ShowRefugoPanel, value);
            }
        }

        private bool _NotShowRefugoPanel = true;
        public bool NotShowRefugoPanel
        {
            get => _NotShowRefugoPanel;
            set => SetProperty(ref _NotShowRefugoPanel, value);
        }

        private bool _NotShowConfirmation = true;
        public bool NotShowConfirmation
        {
            get => _NotShowConfirmation;
            set => SetProperty(ref _NotShowConfirmation, value);
        }

        private bool _IsContinueFinishParada = true;
        public bool IsContinueFinishParada
        {
            get => _IsContinueFinishParada;
            set => SetProperty(ref _IsContinueFinishParada, value);
        }

        public bool ShowFinishContinue => IsFinish && NotShowConfirmation;
        public bool ShowTurnFinishContinue => IsTurnFinish && NotShowConfirmation;
        public bool ShowFinishParadaContinue => IsFinishParada && NotShowConfirmation;

        private string _Operacao;
        public string Operacao
        {
            get => _Operacao;
            set => SetProperty(ref _Operacao, value);
        }

        private int _SelectedOperation = 0;
        public int SelectedOperation
        {
            get => _SelectedOperation;
            set => SetProperty(ref _SelectedOperation, value);
        }

        private decimal _QtdBoas = 0;
        public decimal QtdBoas
        {
            get => _QtdBoas;
            set => SetProperty(ref _QtdBoas, value);
        }

        private decimal _QtdRefugo = 0;
        public decimal QtdRefugo
        {
            get => _QtdRefugo;
            set => SetProperty(ref _QtdRefugo, value);
        }

        private decimal _QtdItemRefugo = 0;
        public decimal QtdItemRefugo
        {
            get => _QtdItemRefugo;
            set => SetProperty(ref _QtdItemRefugo, value);
        }

        private string _Observacao = "";
        public string Observacao
        {
            get => _Observacao;
            set => SetProperty(ref _Observacao, value);
        }

        bool _ShowHeader;
        public bool ShowHeader
        {
            get => _ShowHeader;
            set => SetProperty(ref _ShowHeader, value);
        }
        bool _ShowOptions;
        public bool ShowOptions
        {
            get => _ShowOptions;
            set => SetProperty(ref _ShowOptions, value);
        }

        bool _ShowOperations;
        public bool ShowOperations
        {
            get => _ShowOperations;
            set => SetProperty(ref _ShowOperations, value);
        }
        bool _ShowQtd;
        public bool ShowQtd
        {
            get => _ShowQtd;
            set => SetProperty(ref _ShowQtd, value);
        }
        bool _ShowRefugo;
        public bool ShowRefugo
        {
            get => _ShowRefugo;
            set => SetProperty(ref _ShowRefugo, value);
        }
        bool _ShowConfirmation;
        public bool ShowConfirmation
        {
            get => _ShowConfirmation;
            set => SetProperty(ref _ShowConfirmation, value);
        }
        bool _ShowCancelation;
        public bool ShowCancelation
        {
            get => _ShowCancelation;
            set => SetProperty(ref _ShowCancelation, value);
        }


        public ApontamentoProdVM()
        {
            Items = new ObservableCollection<PDAptLancamentosModel>();
            Rotinas = new ObservableCollection<PDOrdemProdRotinasModel>();
            RotinasNome = new ObservableCollection<string>();
            Motivos = new ObservableCollection<PDMotivosModel>();
            MotivosNome = new ObservableCollection<string>();
            ParadaMotivos = new ObservableCollection<PDMotivosModel>();
            ParadaMotivosNome = new ObservableCollection<string>();
            base.PageNum = 1;
            Title = $"Apontamentos";
            _AppointmentService = new AppointmentService();
        }

        public ApontamentoProdVM(PDOrdemProdModel pOp)
        {
            Items = new ObservableCollection<PDAptLancamentosModel>();
            Rotinas = new ObservableCollection<PDOrdemProdRotinasModel>(pOp.pdordemprodrotinas);
            RotinasNome = new ObservableCollection<string>(pOp.pdordemprodrotinas.Select(r => r.DetailOperacao));
            Motivos = new ObservableCollection<PDMotivosModel>();
            MotivosNome = new ObservableCollection<string>();
            ParadaMotivos = new ObservableCollection<PDMotivosModel>();
            ParadaMotivosNome = new ObservableCollection<string>();
            base.PageNum = 1;
            Title = $"Novo Apontamento";
            _AppointmentService = new AppointmentService();
        }

        public async Task GetMotivosAsync()
        {
            if (IsBusy)
                return;

            IsBusy = true;
            try
            {
                Motivos = new ObservableCollection<PDMotivosModel>(
                    await _AppointmentService.GetRefugoMotivosAsync());
            }
            catch (Exception e)
            {
                ShowShortMessage(e.Message);

                IsBusy = false;
            }
            finally
            {
                IsBusy = false;
            }
        }

        public async Task GetStopMotivosAsync()
        {
            if (IsBusy)
                return;

            IsBusy = true;
            try
            {
                ParadaMotivos = new ObservableCollection<PDMotivosModel>(
                    await _AppointmentService.GetParadaMotivosAsync());
            }
            catch (Exception e)
            {
                ShowShortMessage(e.Message);

                IsBusy = false;
            }
            finally
            {
                IsBusy = false;
            }
        }

        public bool ValidarFinishContinue()
        {
            if (QtdBoas <= 0)
            {
                ShowShortMessage(Resources.ValidationMessagesResource.TypeGoodPartsAmount);
                return false;
            }
            else if (SelectedOperation <= -1)
            {
                ShowShortMessage(Resources.ValidationMessagesResource.TypeOperationId);
                return false;
            }

            return true;
        }

        public bool ValidarFinishParadaContinue()
        {
            if (SelectedOperation <= -1)
            {
                ShowShortMessage(Resources.ValidationMessagesResource.TypeOperationId);
                return false;
            }

            return true;
        }

        protected async override Task LoadMoreData()
        {
            IsBusy = true;
            try
            {
                await Task.Delay(1);
                Device.BeginInvokeOnMainThread(async () =>
                {
                    try
                    {
                        var results = await _AppointmentService.GetListApontamentosAsync();
                        if (results != null)
                        {
                            foreach (var item in results)
                                Items.Add(item);
                        }
                    }
                    catch (Exception e)
                    {
                        ShowShortMessage(e.Message);

                        IsBusy = false;
                    }
                    finally
                    {
                        IsBusy = false;
                    }
                });

                IsBusy = false;
            }
            catch (Exception e)
            {
                ShowShortMessage(e.Message);

                IsBusy = false;
            }
            finally
            {
                IsBusy = false;
            }
        }

        protected async override Task RefreshData()
        {
            await base.RefreshData();

            IsBusy = true;

            base.PageNum = 1;
            Items.Clear();

            await LoadMoreData();
        }
    }
}
