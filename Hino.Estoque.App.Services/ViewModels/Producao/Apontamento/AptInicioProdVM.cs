using Hino.Estoque.App.Models.Producao;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Hino.Estoque.App.Services.Application.Producao.Apontamento;
using XF.Material.Forms.UI.Dialogs;
using Hino.Estoque.App.Utils.Messages;
using Hino.Estoque.App.Models.Engenharia;

namespace Hino.Estoque.App.Services.ViewModels.Producao.Apontamento
{
    public class AptInicioProdVM : BaseViewModel
    {
        PDOrdemProdModel _OrdemProd;
        public ENMaquinasModel MaquinasModel { get; set; }
        int _Operacao;
        int? _CodMotivo;

        public event EventHandler GoToStart;
        public AptInicioProdVM(PDOrdemProdModel pDOrdemProd, int pOperacao)
        {
            _OrdemProd = pDOrdemProd;
            _Operacao = pOperacao;
        }

        public AptInicioProdVM(PDOrdemProdModel pDOrdemProd, int pOperacao, int? pCodMotivo)
        {
            _OrdemProd = pDOrdemProd;
            _Operacao = pOperacao;
            _CodMotivo = pCodMotivo;
        }

        public string OPDescription { get => $"{_OrdemProd.codordprod} - {_OrdemProd.nivelordprod}"; }
        public string OPProduct { get => $"{ _OrdemProd.codproduto} - {_OrdemProd.fsproduto.descricao}"; }
        public string Usuario { get => $"{ CurrentApp.UserLogged.GEUsuarios.nome } "; }
        public DateTime OPDTInicio => DateTime.Now;
        public string Operacao
        {
            get
            {
                if (_Operacao > -1)
                {
                    try
                    {
                        return _OrdemProd.pdordemprodrotinas.Where(r => r.operacao == _Operacao)
                             .First()
                             .DetailOperacao;
                    }
                    catch (Exception)
                    {
                    }
                }

                return "";
            }
        }

        public async Task Iniciar()
        {
            if (IsBusy)
                return;

            IsBusy = true;
            using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
            {
                try
                {
                    var OperacaoApontamento = _OrdemProd.pdordemprodrotinas.Where(r => r.operacao == _Operacao)
                             .First();
                    var _AppointmentService = new AppointmentService();

                    var ObjInicio = new Models.Producao.Apontamento.PDAptInicioProdModel
                    {
                        codestab = (int)CurrentApp.CODESTAB,
                        codiniapt = 0,
                        operacao = OperacaoApontamento.operacao,
                        codroteiro = OperacaoApontamento.codroteiro,
                        codordprod = OperacaoApontamento.codordprod,
                        codestrutura = OperacaoApontamento.codestrutura,
                        nivelordprod = OperacaoApontamento.nivelordprod,
                        codusuario = CurrentApp.UserLogged.GEUsuarios.codusuario,
                        codfuncionario = CurrentApp.UserLogged.GEFuncionarios.codfuncionario,
                        codmaquina = MaquinasModel?.codmaquina,
                        dtinicio = DateTime.Now,
                        tipo = 3
                    };

                    var results = await _AppointmentService.IniciarProducaoAsync(ObjInicio);
                    OnMessageRaise(new MessageRaiseEvent(
                            Resources.ValidationMessagesResource.Complete,
                            Resources.ValidationMessagesResource.OperationStartedWithSuccess));
                    GoToStart?.Invoke(this, null);
                }
                catch (Exception e)
                {
                    ShowShortMessage(e.Message);

                    IsBusy = false;
                }
                finally
                {
                    IsBusy = false;
                }
            }
        }

        public async Task IniciarParadaAsync()
        {
            if (IsBusy)
                return;

            IsBusy = true;
            using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
            {
                try
                {
                    var OperacaoApontamento = _OrdemProd.pdordemprodrotinas.Where(r => r.operacao == _Operacao)
                             .First();
                    var _AppointmentService = new AppointmentService();
                    var results = await _AppointmentService.IniciarParadaAsync(new Models.Producao.Apontamento.PDAptInicioProdModel
                    {
                        codestab = (int)CurrentApp.CODESTAB,
                        codiniapt = 0,
                        operacao = OperacaoApontamento.operacao,
                        codroteiro = OperacaoApontamento.codroteiro,
                        codordprod = OperacaoApontamento.codordprod,
                        codestrutura = OperacaoApontamento.codestrutura,
                        nivelordprod = OperacaoApontamento.nivelordprod,
                        codusuario = CurrentApp.UserLogged.GEUsuarios.codusuario,
                        codfuncionario = CurrentApp.UserLogged.GEFuncionarios.codfuncionario,
                        dtinicio = DateTime.Now,
                        codmotivo = _CodMotivo,
                        tipo = 7
                    });
                    OnMessageRaise(new MessageRaiseEvent(
                            Resources.ValidationMessagesResource.Complete,
                            Resources.ValidationMessagesResource.StopStartedWithSuccess));
                    GoToStart?.Invoke(this, null);
                }
                catch (Exception e)
                {
                    ShowShortMessage(e.Message);

                    IsBusy = false;
                }
                finally
                {
                    IsBusy = false;
                }
            }
        }
    }
}
