﻿using Hino.Estoque.App.Models.Producao;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Estoque.App.Services.ViewModels.Producao.Apontamento
{
    public class EndTurnProductionVM : BaseViewModel
    {
        private readonly PDOrdemProdModel _PDOrdemProdModel;

        public int SelectedOperation { get; set; }

        public EndTurnProductionVM(PDOrdemProdModel pOp)
        {
            _PDOrdemProdModel = pOp;
            base.PageNum = 1;
        }
    }
}
