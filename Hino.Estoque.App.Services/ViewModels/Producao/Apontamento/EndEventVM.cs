﻿using Hino.Estoque.App.Models.Producao;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hino.Estoque.App.Services.ViewModels.Producao.Apontamento
{
    public class EndEventVM : BaseViewModel
    {
        string _Observacao;
        public string OBSERVACAO
        {
            get => _Observacao;
            set => SetProperty(ref _Observacao, value);
        }

        private readonly PDOrdemProdModel _PDOrdemProdModel;

        public int SelectedOperation { get; set; }
        public PDMotivosModel SelectedReason { get; set; }

        public EndEventVM(PDOrdemProdModel pOp)
        {
            _PDOrdemProdModel = pOp;
            base.PageNum = 1;
        }
    }
}
