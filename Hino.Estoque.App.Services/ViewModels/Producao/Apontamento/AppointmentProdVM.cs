using Hino.Estoque.App.Models.Producao;
using Hino.Estoque.App.Services.Application.Producao.Apontamento;
using Hino.Estoque.App.Utils.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.App.Services.ViewModels.Producao.Apontamento
{
    public class ConfirmStartNewProduction
    {
        public string Title { get; set; }
        public string Text { get; set; }
        public StartProductionVM startProductionVM { get; set; }
    }

    public class AppointmentProdVM : BaseViewModel
    {
        private readonly PDOrdemProdModel _PDOrdemProdModel;
        public event EventHandler StartNewOperation;
        public event EventHandler GoToStart;

        public AppointmentProdVM(PDOrdemProdModel pOp)
        {
            _PDOrdemProdModel = pOp;
            base.PageNum = 1;
            Title = $"Apontamento";
        }

        private bool ValidateOperationInOrderProd(int pOperation)
        {
            if (!_PDOrdemProdModel.pdordemprodrotinas.Any(r => r.operacao == pOperation))
            {
                OnMessageRaise(new MessageRaiseEvent(
                            Resources.ValidationMessagesResource.Operation,
                            Resources.ValidationMessagesResource.OperationNotSelected));
                return false;
            }

            return true;
        }

        private void AptInicioProdVM_GoToStart(object sender, EventArgs e) =>
            GoToStart?.Invoke(sender, e);

        private void AptInicioProdVM_MessageRaise(object sender, App.Utils.Interfaces.IMessageRaise e) =>
            OnMessageRaise(e);

        public async Task StartProductionAsync(StartProductionVM pStartProduction)
        {
            var _AptInicioProdVM = new AptInicioProdVM(_PDOrdemProdModel, pStartProduction.SelectedOperation);
            _AptInicioProdVM.MaquinasModel = pStartProduction._ENMaquinasModel;
            _AptInicioProdVM.MessageRaise += AptInicioProdVM_MessageRaise;
            _AptInicioProdVM.GoToStart += AptInicioProdVM_GoToStart;

            if (ValidateOperationInOrderProd(pStartProduction.SelectedOperation))
                await _AptInicioProdVM.Iniciar();
        }

        public async Task EndProductionAsync(EndProductionVM pEndProduction)
        {
            var _AptTerminoProdVM = new AptTerminoProdVM(_PDOrdemProdModel, pEndProduction.SelectedOperation, 
                pEndProduction.QtdBoas, pEndProduction.QtdRefugo, 
                pEndProduction.Observacao); ;
            _AptTerminoProdVM.RefugoMotivo = pEndProduction.RefugoMotivo;
            _AptTerminoProdVM.MessageRaise += AptInicioProdVM_MessageRaise;
            _AptTerminoProdVM.GoToStart += AptInicioProdVM_GoToStart;
            _AptTerminoProdVM.MessageConfirmRaise += StartNewOperation_MessageConfirmRaise;

            if (ValidateOperationInOrderProd(pEndProduction.SelectedOperation))
                await _AptTerminoProdVM.Finalizar();
        }

        private void StartNewOperation_MessageConfirmRaise(object sender, App.Utils.Interfaces.IMessageRaise e)
        {
            StartNewOperation?.Invoke(new ConfirmStartNewProduction
            {
                Title = e.Title,
                Text = e.Text,
                startProductionVM = (StartProductionVM)e.Tag
            }, new EventArgs());
        }

        public async Task EndTurnProductionAsync(EndTurnProductionVM pEndTurnProduction)
        {
            var _AptTerminoProdVM = new AptTerminoProdVM(_PDOrdemProdModel, pEndTurnProduction.SelectedOperation, 0, 0, "");
            _AptTerminoProdVM.MessageRaise += AptInicioProdVM_MessageRaise;
            _AptTerminoProdVM.GoToStart += AptInicioProdVM_GoToStart;
            _AptTerminoProdVM.MessageConfirmRaise += StartNewOperation_MessageConfirmRaise;

            if (ValidateOperationInOrderProd(pEndTurnProduction.SelectedOperation))
                await _AptTerminoProdVM.FinalizarTurnoAsync();
        }

        public async Task StartEventAsync(StartEventVM pStartEvent)
        {
            var _AptInicioProdVM = new AptInicioProdVM(_PDOrdemProdModel, pStartEvent.SelectedOperation, pStartEvent.SelectedReason?.codmotivo);
            _AptInicioProdVM.MessageRaise += AptInicioProdVM_MessageRaise;
            _AptInicioProdVM.GoToStart += AptInicioProdVM_GoToStart;

            if (ValidateOperationInOrderProd(pStartEvent.SelectedOperation))
                await _AptInicioProdVM.IniciarParadaAsync();
        }

        public async Task EndEventAsync(EndEventVM pEndEvent)
        {
            var _AptTerminoProdVM = new AptTerminoProdVM(_PDOrdemProdModel, pEndEvent.SelectedOperation, 0, 0, "");
            _AptTerminoProdVM.MotivoSelecionado = pEndEvent.SelectedReason?.codmotivo;
            _AptTerminoProdVM.Observacao = pEndEvent.OBSERVACAO ?? "";
            _AptTerminoProdVM.MessageRaise += AptInicioProdVM_MessageRaise;
            _AptTerminoProdVM.GoToStart += AptInicioProdVM_GoToStart;

            if (ValidateOperationInOrderProd(pEndEvent.SelectedOperation))
                await _AptTerminoProdVM.FinalizarParadaAsync();
        }
    }
}
