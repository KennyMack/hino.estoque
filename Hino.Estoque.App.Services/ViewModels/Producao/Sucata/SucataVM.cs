﻿using Hino.Estoque.App.Models.Producao;
using Hino.Estoque.App.Models.Producao.Apontamento;
using Hino.Estoque.App.Services.Application.Fiscal;
using Hino.Estoque.App.Services.Application.Producao.Apontamento;
using Hino.Estoque.App.Services.ViewModels.Fiscal;
using Hino.Estoque.App.Utils.Extensions;
using Hino.Estoque.App.Utils.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XF.Material.Forms.UI.Dialogs;

namespace Hino.Estoque.App.Services.ViewModels.Producao.Sucata
{
    public class SucataVM: BaseViewModel
    {
        public event EventHandler GoToStart;
        public event EventHandler evtTextFocused;
        private List<PDOrdemProdCompModel> Componentes;
        private readonly AppointmentService _AppointmentService;
        private readonly ProdutosVM _ProdutosVM;
        public readonly PDOrdemProdModel _PDOrdemProdModel;

        public string LBLPRODUCTPLACEHOLDER { get => Resources.FieldsNameResource.ProductKey; }
        public string LBLPRODUCTHELPERTEXT { get => Resources.FieldsNameResource.ProductKey; }
        public string LBLPRODUCTERRORTEXT { get => "Informe o produto"; }

        public string LBLSTOCKORIGINPLACEHOLDER { get => Resources.FieldsNameResource.StockOrigin; }
        public string LBLSTOCKORIGINHELPERTEXT { get => Resources.FieldsNameResource.StockOrigin; }
        public string LBLSTOCKORIGINERRORTEXT { get => "Informe a origem"; }

        public string LBLQUANTITYPLACEHOLDER { get => Resources.FieldsNameResource.Quantity; }
        public string LBLQUANTITYHELPERTEXT { get => Resources.FieldsNameResource.Quantity; }
        public string LBLQUANTITYERRORTEXT { get => "Informe a quantidade"; }
        public PDMotivosModel SelectedReason { get; set; }

        public bool _HasProductError = false;
        public bool HASPRODUCTERROR
        {
            get
            {
                return _HasProductError;
            }
            set
            {
                SetProperty(ref _HasProductError, value);
            }
        }

        public bool _HasStockOriginError = false;
        public bool HASSTOCKORIGINERROR
        {
            get
            {
                return _HasStockOriginError;
            }
            set
            {
                SetProperty(ref _HasStockOriginError, value);
            }
        }

        public bool _HasQuantityError = false;
        public bool HASQUANTITYERROR
        {
            get
            {
                return _HasQuantityError;
            }
            set
            {
                SetProperty(ref _HasQuantityError, value);
            }
        }

        public string _Product;
        public string PRODUCT
        {
            get
            {
                return _Product;
            }
            set
            {
                SetProperty(ref _Product, value);
            }
        }

        public string _StockOrigin;
        public string STOCKORIGIN
        {
            get
            {
                return _StockOrigin;
            }
            set
            {
                SetProperty(ref _StockOrigin, value);
            }
        }

        public string _StockOriginSaldo;
        public string STOCKORIGINSALDO
        {
            get
            {
                return _StockOriginSaldo;
            }
            set
            {
                SetProperty(ref _StockOriginSaldo, value);
            }
        }

        public double _Quantity = 0;
        public double QUANTITY
        {
            get
            {
                return _Quantity;
            }
            set
            {
                SetProperty(ref _Quantity, value);
            }
        }

        public SucataVM(PDOrdemProdModel pOp)
        {
            _PDOrdemProdModel = pOp;
            _AppointmentService = new AppointmentService();
            _ProdutosVM = new ProdutosVM();
            Componentes = new List<PDOrdemProdCompModel>();
            _ProdutosVM.evtTextFocused += ProdutosVM_evtTextFocused;
        }

        private void ProdutosVM_evtTextFocused(object sender, EventArgs e)
        {
        }

        public async Task LoadComponentsAsync()
        {
            Componentes = new List<PDOrdemProdCompModel>(
                await _AppointmentService.GetProductsToSucAsync(_PDOrdemProdModel));
        }

        public bool ProductExists(string pCodProduto) =>
            Componentes.Any(r => r.codcomponente == pCodProduto);

        public async Task GetProductByBarCodeAsync()
        {
            if (_ProdutosVM.IsBusy)
                return;

            HASPRODUCTERROR = string.IsNullOrEmpty(PRODUCT);
            if (!HASPRODUCTERROR)
            {
                _ProdutosVM.PRODUCT = PRODUCT;
                _ProdutosVM.STOCKDESTINY = "";
                var produto = await _ProdutosVM.GetProductByBarCodeAsync();

                var found = _PDOrdemProdModel.pdordemprodcomp.Where(r => r.codcomponente == _ProdutosVM.PRODUCT).FirstOrDefault();
                if (found != null)
                {
                    produto.ProdutoParamEstab.codestoque = found.codestoque;
                    produto.estoquedestino = "";
                    _ProdutosVM.STOCKORIGIN = found.codestoque;
                    _ProdutosVM.STOCKDESTINY = "";

                    await _ProdutosVM.CarregaDetailsProductAsync(produto);
                }
                STOCKORIGIN = _ProdutosVM.STOCKORIGIN;
                STOCKORIGINSALDO = _ProdutosVM.STOCKORIGINSALDO;
                evtTextFocused.Invoke(STOCKORIGIN == "" ? "PRODUCT" : "QUANTITY", null);
            }
        }

        public bool ValidateAppointment()
        {
            HASSTOCKORIGINERROR = STOCKORIGIN.IsEmpty();
            HASQUANTITYERROR = QUANTITY <= 0;
            HASPRODUCTERROR = PRODUCT.IsEmpty();

            if (HASPRODUCTERROR ||
                HASQUANTITYERROR ||
                HASSTOCKORIGINERROR)
                return false;

            if (!ProductExists(PRODUCT))
            {
                ShowLongMessage("Produto não permitido.");
                return false;
            }

            if (_ProdutosVM.SaldoEstoqueOrigin == null)
            {
                ShowLongMessage("Saldo de estoque não encontrado.");
                return false;
            }

            if (_ProdutosVM.SaldoEstoqueOrigin.saldoEstoque < Convert.ToDecimal(QUANTITY))
            {
                ShowLongMessage("Saldo de estoque é menor que a qtd. a sucatear.");
                return false;
            }

            return true;
        }

        public async Task SalvarSucataAsync()
        {
            if (IsBusy)
                return;

            if (!ValidateAppointment())
                return;

            IsBusy = true;
            using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
            {
                try
                {
                    var Sucata = new PDSucataCreateVM
                    {
                        codestab = (short)CurrentApp.CODESTAB,
                        codfuncionario = CurrentApp.UserLogged.GEFuncionarios.codfuncionario,
                        codmotivo = SelectedReason.codmotivo,
                        codordprod = _PDOrdemProdModel.codordprod,
                        nivelordprod = _PDOrdemProdModel.nivelordprod,
                        codestrutura = _PDOrdemProdModel.codestrutura,
                        codestoque = STOCKORIGIN,
                        codproduto = PRODUCT,
                        qtdeproduto = Convert.ToDecimal(QUANTITY)
                    };

                    var sucata = await _AppointmentService.SalvarSucataAsync(Sucata);
                    
                    if (sucata == null)
                        throw new Exception("Não foi possível apontar");

                    OnMessageRaise(new MessageRaiseEvent(
                                Resources.ValidationMessagesResource.Complete,
                                Resources.ValidationMessagesResource.WasteAppointmentSuccess));
                    GoToStart?.Invoke(this, null);
                }
                catch (Exception e)
                {
                    ShowShortMessage(e.Message);

                    IsBusy = false;
                }
                finally
                {
                    IsBusy = false;
                }
            }
        }
    }
}
