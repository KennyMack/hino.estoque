﻿using Hino.Estoque.App.Models.Fiscal;
using Hino.Estoque.App.Models.Producao;
using Hino.Estoque.App.Services.API.Interfaces.Gerais;
using Hino.Estoque.App.Services.Application.Estoque;
using Hino.Estoque.App.Services.Application.Fiscal;
using Hino.Estoque.App.Services.DB.Interfaces.Producao;
using Hino.Estoque.App.Services.DB.Producao;
using Hino.Estoque.App.Services.ViewModels.Fiscal;
using Hino.Estoque.App.Utils.Extensions;
using Hino.Estoque.App.Utils.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using XF.Material.Forms.UI.Dialogs;

namespace Hino.Estoque.App.Services.ViewModels.Producao
{
    public class ItemOPStockVM : BaseViewModel
    {
        public event EventHandler evtTextFocused;
        public event EventHandler SaveSuccess;
        readonly IPDOPCreateTransfItemDB _PDOPCreateTransfItemDB;
        readonly PDOrdemProdCompModel _PDOrdemProdCompModel;
        readonly IEstabelecimentosAPI _EstabelecimentosAPI;

        private readonly ProdutosService _ProdutosService;
        private readonly OPTransferenciaService _OPTransferenciaService;
        private FSProdutoModel _FSProdutoModel;

        public string LBLINVENTPLACEHOLDER => Resources.FieldsNameResource.InventIdentifier;
        public string LBLINVENTHELPERTEXT => Resources.FieldsNameResource.InventIdentifier;
        public string LBLINVENTERRORTEXT => "Informe o inventário";

        public string LBLPRODUCTPLACEHOLDER => Resources.FieldsNameResource.ProductKey;
        string _LBLPRODUCTHELPERTEXT = Resources.FieldsNameResource.ProductKey;
        public string LBLPRODUCTHELPERTEXT
        {
            get => _LBLPRODUCTHELPERTEXT;
            set => SetProperty(ref _LBLPRODUCTHELPERTEXT, value);
        }
        public string LBLPRODUCTERRORTEXT => "Informe o produto";


        public string LBLSUGESTEDPLACEHOLDER => Resources.FieldsNameResource.Sugested;
        public string LBLTAKENPLACEHOLDER => Resources.FieldsNameResource.Taken;


        public string LBLQUANTITYPLACEHOLDER => Resources.FieldsNameResource.Quantity;
        public string LBLQUANTITYHELPERTEXT => Resources.FieldsNameResource.Quantity;
        public string LBLQUANTITYERRORTEXT => "Informe a quantidade";

        public string LBLLOTEPLACEHOLDER => Resources.FieldsNameResource.Lote;
        public string LBLLOTEHELPERTEXT => Resources.FieldsNameResource.Lote;
        public string LBLLOTEERRORTEXT => "Informe o lote";

        public string LBLSTOCKPLACEHOLDER => Resources.FieldsNameResource.StockOrigin;
        public string _LBLSTOCKHELPERTEXT = Resources.FieldsNameResource.StockOrigin;
        public string LBLSTOCKHELPERTEXT
        {
            get => _LBLSTOCKHELPERTEXT;
            set => SetProperty(ref _LBLSTOCKHELPERTEXT, value);
        }
        public string LBLSTOCKERRORTEXT => "Informe o estoque";

        public string PRODUCTBARCODE { set; get; }

        public bool _HasProductError = false;
        public bool HASPRODUCTERROR
        {
            get => _HasProductError;
            set => SetProperty(ref _HasProductError, value);
        }

        public bool _HasStockError = false;
        public bool HASSTOCKERROR
        {
            get => _HasStockError;
            set => SetProperty(ref _HasStockError, value);
        }

        public string _Product;
        public string PRODUCT
        {
            get => _Product;
            set => SetProperty(ref _Product, value?.ToUpperInvariant());
        }

        public string _Stock;
        public string STOCK
        {
            get => _Stock;
            set => SetProperty(ref _Stock, value?.ToUpperInvariant());
        }

        public bool _HasQuantityError = false;
        public bool HASQUANTITYERROR
        {
            get => _HasQuantityError;
            set => SetProperty(ref _HasQuantityError, value);
        }

        public double _Quantity = 0;
        public double QUANTITY
        {
            get => _Quantity;
            set
            {
                SetProperty(ref _Quantity, value);
            }
        }

        public double _PackagesQuantity = 1;
        public double PACKAGESQUANTITY
        {
            get => _PackagesQuantity;
            set
            {
                SetProperty(ref _PackagesQuantity, value);
            }
        }

        public bool _ManualQuantity = false;
        public bool MANUALQUANTITY
        {
            get => _ManualQuantity;
            set
            {
                SetProperty(ref _ManualQuantity, value);
                INVERSEMANUALQUANTITY = !value;
            }
        }

        public bool _InverseManualQuantity = true;
        public bool INVERSEMANUALQUANTITY
        {
            get => _InverseManualQuantity;
            set => SetProperty(ref _InverseManualQuantity, value);
        }

        public bool _InformPackages = false;
        public bool INFORMPACKAGES
        {
            get => _InformPackages;
            set
            {
                SetProperty(ref _InformPackages, value);
                INVERSEINFORMPACKAGES = !value;
            }
        }
        public bool _InformedPackages = false;
        public bool INFORMEDPACKAGES
        {
            get => _InformPackages;
            set => SetProperty(ref _InformedPackages, value);
        }

        public bool _InverseInformPackages = true;
        public bool INVERSEINFORMPACKAGES
        {
            get => _InverseInformPackages;
            set => SetProperty(ref _InverseInformPackages, value);
        }

        public string _Readouts;
        public string READOUTS
        {
            get => _Readouts;
            set => SetProperty(ref _Readouts, value);
        }


        public string _SugestedOriginLocation;
        public string SUGESTEDORIGINLOCATION
        {
            get => _SugestedOriginLocation;
            set => SetProperty(ref _SugestedOriginLocation, value);
        }

        public ICommand CmdInformPackagesClick { protected set; get; }
        public ICommand CmdSaveCountClick { protected set; get; }

        public ItemOPStockVM(PDOrdemProdCompModel pProdCompModel)
        {
            _PDOrdemProdCompModel = pProdCompModel;
            _ProdutosService = new ProdutosService();
            _OPTransferenciaService = new OPTransferenciaService();
            _PDOPCreateTransfItemDB = DInjection.GetIntance<IPDOPCreateTransfItemDB>();
            _EstabelecimentosAPI = DInjection.GetIntance<IEstabelecimentosAPI>();
            CmdInformPackagesClick = new Command(OnInformPackagesClick);
            CmdSaveCountClick = new Command(SaveCountClick);
            Title = $"O.P. {_PDOrdemProdCompModel.codordprod}-{_PDOrdemProdCompModel.nivelordprod}";
            SUGESTEDORIGINLOCATION = pProdCompModel.SugestedOriginLocation;
        }

        public async Task UpdateTotalQuantityAsync()
        {
            var _PDOrdemProdCompDB = DInjection.GetIntance<IPDOrdemProdCompDB>();
            var result = await _PDOPCreateTransfItemDB.Query(r =>
                r.codestab == _PDOrdemProdCompModel.codestab &&
                r.codordprod == _PDOrdemProdCompModel.codordprod &&
                r.operacao == _PDOrdemProdCompModel.operacao &&
                r.codcomponente == _PDOrdemProdCompModel.codcomponente, -1);

            
            _PDOrdemProdCompModel.QtdSeparadaTotal = result.Results.Sum(r => r.qtdseparada);
            _PDOrdemProdCompModel.qtdSeparada = _PDOrdemProdCompModel.QtdSeparadaTotal;
            _PDOrdemProdCompModel.Readouts = READOUTS;
            _PDOrdemProdCompModel.separado = true;

            var dbItem = await _PDOrdemProdCompDB.GetItemAsync(_PDOrdemProdCompModel.IdMobile);
            dbItem.QtdSeparadaTotal = _PDOrdemProdCompModel.QtdSeparadaTotal;
            dbItem.qtdSeparada = dbItem.QtdSeparadaTotal;
            dbItem.Readouts = READOUTS;
            dbItem.separado = true;


            await _PDOrdemProdCompDB.UpdateItemAsync(dbItem);

            await LoadReadoutsAsync();
        }

        public async void SaveCountClick()
        {
            if (IsBusy)
                return;

            HASPRODUCTERROR = PRODUCT.IsEmpty();
            HASSTOCKERROR = STOCK.IsEmpty();

            if (!HASPRODUCTERROR &&
                !HASSTOCKERROR)
            {
                using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
                {
                    if (_FSProdutoModel == null || _FSProdutoModel.ProdutoPcp == null)
                    {
                        ShowShortMessage(Resources.ErrorMessagesResource.ProductNotFound);
                        return;
                    }

                    if (QUANTITY <= 0)
                    {
                        ShowShortMessage(Resources.ValidationMessagesResource.QuantityIsNotValid);
                        return;
                    }

                    IsBusy = true;

                    var result = await _ProdutosService.GetProductSaldoEstoqueAsync(new Models.Estoque.ESBuscaSaldoEstoqueModel
                    {
                        CodEstab = (int)CurrentApp.CODESTAB,
                        CodEstoque = _FSProdutoModel.estoquedestino,
                        CodProduto = PRODUCT,
                        SaldoAtual = 0
                    });

                    if (result.codestoque.IsEmpty())
                    {
                        ShowShortMessage(
                            Resources.ErrorMessagesResource.InvalidStock);
                        IsBusy = false;
                        return;
                    }

                    try
                    {

                        var itemTransf = new PDOPCreateTransfItemModel();

                        itemTransf.CopyProperties(_PDOrdemProdCompModel);

                        itemTransf.codestoqueori = _FSProdutoModel.estoquedestino;
                        itemTransf.qtdseparada = Convert.ToDecimal(QUANTITY);
                        itemTransf.Identificador = $"{itemTransf.codordprod}{itemTransf.nivelordprod}{itemTransf.operacao}{itemTransf.codcomponente}{itemTransf.codestoqueori}{itemTransf.codestoquedest}";

                        await _PDOPCreateTransfItemDB.SaveTransfItemAsync(itemTransf);

                        await UpdateTotalQuantityAsync();

                        ShowShortMessage(Resources.ValidationMessagesResource.SaveSuccess);

                        await LoadReadoutsAsync();

                        SaveSuccess?.Invoke(this, null);
                    }
                    catch (Exception ex)
                    {
                        OnMessageRaise(new MessageRaiseEvent(
                            Resources.ValidationMessagesResource.ProblemToSave,
                            ex.Message
                        ));

                        IsBusy = false;
                    }
                    finally
                    {
                        IsBusy = false;
                    }
                }
            }
        }

        public void OnInformPackagesClick()
        {
            INFORMPACKAGES = true;
        }

        public async Task LoadReadoutsAsync()
        {
            READOUTS = "";
            var result = await _PDOPCreateTransfItemDB.Query(r =>
                r.codestab == _PDOrdemProdCompModel.codestab &&
                r.codordprod == _PDOrdemProdCompModel.codordprod &&
                r.operacao == _PDOrdemProdCompModel.operacao &&
                r.codcomponente == _PDOrdemProdCompModel.codcomponente, -1);

            READOUTS = string.Join("\r\n",
            result.Results
                .Select(s => $"Ori.: {s.codestoqueori} / {s.qtdseparada:n4}")
                .ToArray());
        }

        public async Task GetProductByBarCodeAsync()
        {
            if (IsBusy)
                return;

            HASPRODUCTERROR = string.IsNullOrEmpty(PRODUCT);

            if (!HASPRODUCTERROR)
            {
                IsBusy = true;
                using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
                {
                    try
                    {
                        _FSProdutoModel = await _ProdutosService.GetProductByBarCodeAsync((short)CurrentApp.CODESTAB, PRODUCT);

                        if (_FSProdutoModel != null)
                        {
                            PRODUCT = _FSProdutoModel.codproduto;
                            LBLPRODUCTHELPERTEXT = $"{_FSProdutoModel.codproduto} - {_FSProdutoModel.descricao}".SubStrTitle(50);
                        }
                        else
                        {
                            LBLPRODUCTHELPERTEXT = Resources.FieldsNameResource.ProductKey;
                            throw new Exception(Resources.ErrorMessagesResource.ProductNotFound);
                        }

                        evtTextFocused?.Invoke("STOCK", null);
                    }
                    catch (Exception)
                    {
                        ShowShortMessage(Resources.ErrorMessagesResource.ProductNotFound);

                        evtTextFocused?.Invoke("PRODUCT", null);

                        IsBusy = false;
                    }
                    finally
                    {
                        IsBusy = false;
                    }
                }
            }

            await LoadReadoutsAsync();
        }

        public async Task GetStockByBarCodeAsync()
        {
            if (IsBusy)
                return;

            HASSTOCKERROR = STOCK.IsEmpty();
            HASPRODUCTERROR = PRODUCT.IsEmpty();

            if (!HASSTOCKERROR && !HASPRODUCTERROR)
            {
                IsBusy = true;
                var Produto = new ProdutosVM();
                Produto.PRODUCT = PRODUCT;
                Produto.STOCKORIGIN = STOCK;
                Produto.STOCKDESTINY = "";
                _FSProdutoModel.estoquedestino = STOCK;
                var result = await Produto.CarregaDetailsProductAsync(_FSProdutoModel);
                if (result)
                    LBLSTOCKHELPERTEXT = Produto.SaldoEstoqueDestiny.codestoque;
                else
                    ShowShortMessage(
                        Resources.ErrorMessagesResource.ActualStockBalanceNotFound);

                IsBusy = false;
            }
        }

        public void CountQuantity()
        {
            HASPRODUCTERROR = string.IsNullOrEmpty(PRODUCT);
            HASSTOCKERROR = string.IsNullOrEmpty(STOCK);

            if (HASPRODUCTERROR)
            {
                evtTextFocused?.Invoke("PRODUCT", null);
                return;
            }

            if (HASSTOCKERROR)
            {
                evtTextFocused?.Invoke("STOCK", null);
                return;
            }

            if (_FSProdutoModel == null || _FSProdutoModel.ProdutoPcp == null)
            {
                ShowShortMessage(Resources.ErrorMessagesResource.ProductNotFound);
                evtTextFocused?.Invoke("PRODUCT", null);
                return;
            }

            if (PRODUCTBARCODE.ToUpperInvariant().In(
                    (_FSProdutoModel.codproduto ?? "").ToUpperInvariant(),
                    ((_FSProdutoModel?.ProdutoPcp?.codgtin) ?? "").ToUpperInvariant(),
                    ((_FSProdutoModel?.ProdutoPcp?.codgtinemb) ?? "").ToUpperInvariant()))
            {
                QUANTITY += Convert.ToDouble(_FSProdutoModel.ProdutoPcp.qtdeporemb) * PACKAGESQUANTITY;
                PACKAGESQUANTITY = 1;
            }
            else
            {
                OnMessageRaise(new MessageRaiseEvent(
                    Resources.ValidationMessagesResource.FailToRead,
                    Resources.ErrorMessagesResource.InvalidProduct
                ));
            }
        }

        public void ClearInstance()
        {
            STOCK = "";
            QUANTITY = 0;
            PACKAGESQUANTITY = 1;
            LBLPRODUCTHELPERTEXT = Resources.FieldsNameResource.ProductKey;
            LBLSTOCKHELPERTEXT = Resources.FieldsNameResource.StockOrigin;
            HASPRODUCTERROR = false;
            HASSTOCKERROR = false;
            PRODUCTBARCODE = "";
            HASQUANTITYERROR = false;
            MANUALQUANTITY = false;
        }
    }
}
