﻿using Hino.Estoque.App.Models.Producao;
using Hino.Estoque.App.Services.Application.Estoque;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Hino.Estoque.App.Utils.Extensions;
using XF.Material.Forms.UI.Dialogs;

namespace Hino.Estoque.App.Services.ViewModels.Producao
{
    public class SearchOrdemProdVM : BaseViewModel
    {
        private readonly PDOrdemProdModel _PDOrdemProdModel;
        public string LBLOPPLACEHOLDER { get => Hino.Estoque.App.Resources.FieldsNameResource.OPIdentifier; }
        public string LBLOPHELPERTEXT { get => Hino.Estoque.App.Resources.FieldsNameResource.OPIdentifier; }
        public string LBLOPERRORTEXT { get => "Informe a O.P."; }

        public event EventHandler GoToItemsOP;
        public ICommand CmdGetOPItemsClick { protected set; get; }

        public bool _HasOPError = false;
        public bool HASOPERROR
        {
            get
            {
                return _HasOPError;
            }
            set
            {
                SetProperty(ref _HasOPError, value);
            }
        }

        private string _OPIdentifier;
        public string OPIDENTIFIER
        {
            get
            {
                return _OPIdentifier;
            }
            set
            {
                SetProperty(ref _OPIdentifier, value);
            }
        }

        public SearchOrdemProdVM()
        {
            Title = "Buscar O.P.";
            _PDOrdemProdModel = new PDOrdemProdModel();
            CmdGetOPItemsClick = new Command(GetOPItemsClick);
        }

        public async void GetOPItemsClick()
        {
            if (IsBusy)
                return;

            HASOPERROR = OPIDENTIFIER.IsEmpty();

            if (!HASOPERROR)
            {
                using (await MaterialDialog.Instance.LoadingDialogAsync(message: Resources.ValidationMessagesResource.LoadProcessing))
                {
                    IsBusy = true;
                    await Task.Delay(200);
                    try
                    {
                        var TransferenciaService = new OPTransferenciaService();
                        var ret = await TransferenciaService.GetOPByBarCodeAsync(OPIDENTIFIER);
                        await TransferenciaService.RemoveOpComponentsAsync(ret.codordprod, ret.nivelordprod, null, null);

                        await Task.Delay(200);

                        GoToItemsOP?.Invoke(ret, null);
                    }
                    catch (Exception ex)
                    {
                        ShowShortMessage(ex.Message);
                    }
                    finally
                    {
                        IsBusy = false;
                    }
                }
            }
        }
    }
}
