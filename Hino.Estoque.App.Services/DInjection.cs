﻿using Autofac;
using Hino.Estoque.App.Services.API.Auth;
using Hino.Estoque.App.Services.API.Estoque;
using Hino.Estoque.App.Services.API.Fiscal;
using Hino.Estoque.App.Services.API.Gerais;
using Hino.Estoque.App.Services.API.Interfaces.Auth;
using Hino.Estoque.App.Services.API.Interfaces.Estoque;
using Hino.Estoque.App.Services.API.Interfaces.Fiscal;
using Hino.Estoque.App.Services.API.Interfaces.Gerais;
using Hino.Estoque.App.Services.API.Interfaces.Manutencao;
using Hino.Estoque.App.Services.API.Interfaces.Producao;
using Hino.Estoque.App.Services.API.Interfaces.Producao.Apontamento;
using Hino.Estoque.App.Services.API.Interfaces.Producao.OEE;
using Hino.Estoque.App.Services.API.Interfaces.Producao.Sucata;
using Hino.Estoque.App.Services.API.Manutencao;
using Hino.Estoque.App.Services.API.Producao;
using Hino.Estoque.App.Services.API.Producao.Apontamento;
using Hino.Estoque.App.Services.API.Producao.OEE;
using Hino.Estoque.App.Services.API.Producao.Sucata;
using Hino.Estoque.App.Services.DB.Estoque;
using Hino.Estoque.App.Services.DB.Interfaces.Estoque;
using Hino.Estoque.App.Services.DB.Interfaces.Producao;
using Hino.Estoque.App.Services.DB.Producao;
using System;

namespace Hino.Estoque.App.Services
{
    public static class DInjection
    {
        private static IContainer _container;

        public static void Initialize()
        {
            if (_container == null)
            {
                var builder = new ContainerBuilder();

                builder.RegisterType<AuthAPI>().As<IAuthAPI>();
                builder.RegisterType<InventarioAPI>().As<IInventarioAPI>();
                builder.RegisterType<TransferenciaAPI>().As<ITransferenciaAPI>();
                builder.RegisterType<OrdemProdAPI>().As<IOrdemProdAPI>();
                builder.RegisterType<EstabelecimentosAPI>().As<IEstabelecimentosAPI>();
                builder.RegisterType<ProdutosAPI>().As<IProdutosAPI>();
                builder.RegisterType<SucataAPI>().As<ISucataAPI>();
                builder.RegisterType<PDOPCreateTransfDB>().As<IPDOPCreateTransfDB>();
                builder.RegisterType<PDRelatorioEficienciaProducaoDB>().As<IPDRelatorioEficienciaProducaoDB>();
                builder.RegisterType<PDOPCreateTransfItemDB>().As<IPDOPCreateTransfItemDB>();
                builder.RegisterType<PDOrdemProdCompEstqEndDB>().As<IPDOrdemProdCompEstqEndDB>();

                builder.RegisterType<ESInventDetDB>().As<IESInventDetDB>();
                builder.RegisterType<PDOrdemProdCompDB>().As<IPDOrdemProdCompDB>();
                builder.RegisterType<AptInicioProdAPI>().As<IAptInicioProdAPI>();
                builder.RegisterType<AptTerminoProdAPI>().As<IAptTerminoProdAPI>();
                builder.RegisterType<MotivosAPI>().As<IMotivosAPI>();
                builder.RegisterType<RequisicaoAPI>().As<IRequisicaoAPI>();

                builder.RegisterType<EquipamentoAPI>().As<IEquipamentoAPI>();
                builder.RegisterType<MotivoManutencaoAPI>().As<IMotivoManutencaoAPI>();
                builder.RegisterType<SolicManutAPI>().As<ISolicManutAPI>();
                builder.RegisterType<TipoManutencaoAPI>().As<ITipoManutencaoAPI>();
                builder.RegisterType<PDRelatorioEficienciaProducaoAPI>().As<IPDRelatorioEficienciaProducaoAPI>();
                builder.RegisterType<OEEAPI>().As<IOEEAPI>();                

                _container = builder.Build();
            }
        }

        public static T GetIntance<T>() =>
            _container.Resolve<T>();
    }
}
