using Hino.Estoque.Infra.Cross.Entities.Estoque;
using Hino.Estoque.Domain.Base.Interfaces.Repositories;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Hino.Estoque.Domain.Estoque.Interfaces.Repositories.Estoque
{
    public interface IESLoteSaldoRepository : IBaseRepository<ESLoteSaldo>
    {
        Task<double> SaldoEstoqueLote(short pCodEstab, decimal pLote, string pCodEstoque);
        Task<IEnumerable<esTransfUNLotes>> GetLotesUnidadeNegAsync(int pCodEstab, string pCodProduto, string pCodEstoque);
        Task<IEnumerable<ESSaldoLoteOP>> BuscaLoteSaldoOPAsync(int pCodEstab, string pCodComponente, string pCodEstoqueOri, long pCodOrdProd, string pNivelOrdProd);
    }
}
