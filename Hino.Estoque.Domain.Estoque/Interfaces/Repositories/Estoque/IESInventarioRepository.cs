using Hino.Estoque.Infra.Cross.Entities.Estoque;
using Hino.Estoque.Domain.Base.Interfaces.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.Estoque.Domain.Estoque.Interfaces.Repositories.Estoque
{
    public interface IESInventarioRepository : IBaseRepository<ESInventario>
    {
        Task<ESInventario> GetInventarioAsync(short pCodEstab, decimal pCodInv, decimal pContagem);
        Task<IEnumerable<ESInventario>> GetInventariosPendentes(short pCodEstab);
        Task<string> CreateItemInventarioAsync(ESCreateItemInventario pCreateItemInventario);
        int GetMaxInventCount(short pCodEstab, decimal pCodInv);
    }
}
