﻿using Hino.Estoque.Domain.Base.Interfaces.Repositories;
using Hino.Estoque.Infra.Cross.Entities.Estoque;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Domain.Estoque.Interfaces.Repositories.Estoque
{
    public interface IESProdEnderecRepository : IBaseRepository<ESProdEnderec>
    {
    }
}
