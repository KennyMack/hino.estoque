using Hino.Estoque.Infra.Cross.Entities.Estoque;
using Hino.Estoque.Domain.Base.Interfaces.Repositories;

namespace Hino.Estoque.Domain.Estoque.Interfaces.Repositories.Estoque
{
    public interface IESTransfDetRepository : IBaseRepository<ESTransfDet>
    {
    }
}
