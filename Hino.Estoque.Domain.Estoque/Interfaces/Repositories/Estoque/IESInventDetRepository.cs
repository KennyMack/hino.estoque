using Hino.Estoque.Infra.Cross.Entities.Estoque;
using Hino.Estoque.Domain.Base.Interfaces.Repositories;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Hino.Estoque.Domain.Estoque.Interfaces.Repositories.Estoque
{
    public interface IESInventDetRepository : IBaseRepository<ESInventDet>
    {
        Task<IEnumerable<ESInventDet>> GetItensInventariosPendentes(short pCodEstab);
        Task<IEnumerable<ESInventDet>> GetItensInventario(short pCodEstab, decimal pCodInv, decimal pContagem);
        Task<ESInventDet> GetById(short pCodEstab, decimal pCodInv, decimal pContagem, string pCodProduto, string pCodEstoque);
    }
}
