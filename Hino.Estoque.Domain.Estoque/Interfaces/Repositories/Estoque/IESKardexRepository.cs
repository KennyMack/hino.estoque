using Hino.Estoque.Infra.Cross.Entities.Estoque;
using Hino.Estoque.Domain.Base.Interfaces.Repositories;
using System.Threading.Tasks;
using Hino.Estoque.Aplication.ViewModels.Estoque;
using System;

namespace Hino.Estoque.Domain.Estoque.Interfaces.Repositories.Estoque
{
    public interface IESKardexRepository : IBaseRepository<ESKardex>
    {
        Task<DateTime> GetDataFechamento(short pCodEstab);
        Task<bool> GetValidaSaldoDataLancConsumo(short pCodEstab, string pCodProduto, string pCodEstoque, decimal pQuantidade, DateTime pDtLanc);
        Task<bool> CreateProdTransferenciaAsync(ESCreateProdTransferencia pCreateTransf);
        Task<EPermisEstoque> ValidaPermisMovEstoqueAsync(int pCodEstab, string pCodUsuario, DateTime pData, string pCodEstoque);
        Task<bool> GeraMovimentoRequisicaoAsync(short pCodEstab, long pCodRequisicao, string pCodUsuario, long pLote, decimal pQuantidade);
    }
}
