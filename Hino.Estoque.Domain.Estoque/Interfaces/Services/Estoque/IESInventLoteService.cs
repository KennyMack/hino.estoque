using Hino.Estoque.Infra.Cross.Entities.Estoque;
using Hino.Estoque.Domain.Base.Interfaces.Services;

namespace Hino.Estoque.Domain.Estoque.Interfaces.Services.Estoque
{
    public interface IESInventLoteService : IBaseService<ESInventLote>
    {
    }
}
