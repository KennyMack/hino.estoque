﻿using Hino.Estoque.Domain.Base.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Estoque;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Domain.Estoque.Interfaces.Services.Estoque
{
    public interface IESProdEnderecService : IBaseService<ESProdEnderec>
    {
    }
}
