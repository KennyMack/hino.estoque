using Hino.Estoque.Infra.Cross.Entities.Estoque;
using Hino.Estoque.Domain.Base.Interfaces.Services;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Hino.Estoque.Domain.Estoque.Interfaces.Services.Estoque
{
    public interface IESInventarioService : IBaseService<ESInventario>
    {
        Task<ESInventario> GetInventarioAsync(short pCodEstab, decimal pCodInv, decimal pContagem);
        Task<IEnumerable<ESInventario>> GetInventariosPendentes(short pCodEstab);
        Task<ESCreateItemInventario> CreateItemInventarioAsync(ESCreateItemInventario pCreateItemInventario);
        int GetMaxInventCount(short pCodEstab, decimal pCodInv);
    }
}
