using Hino.Estoque.Infra.Cross.Entities.Estoque;
using Hino.Estoque.Domain.Base.Interfaces.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.Estoque.Domain.Estoque.Interfaces.Services.Estoque
{
    public interface IESInventDetService : IBaseService<ESInventDet>
    {
        Task<IEnumerable<ESInventDet>> GetItensInventariosPendentes(short pCodEstab);
        Task<IEnumerable<ESInventDet>> GetItensInventario(short pCodEstab, decimal pCodInv, decimal pContagem);
        Task<ESInventDet> GetById(short pCodEstab, decimal pCodInv, decimal pContagem, string pCodProduto, string pCodEstoque);
    }
}
