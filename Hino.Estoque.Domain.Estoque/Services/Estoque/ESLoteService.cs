using Hino.Estoque.Domain.Estoque.Interfaces.Repositories.Estoque;
using Hino.Estoque.Domain.Estoque.Interfaces.Services.Estoque;
using Hino.Estoque.Infra.Cross.Entities.Estoque;
using Hino.Estoque.Domain.Base.Services;

namespace Hino.Estoque.Domain.Estoque.Services.Estoque
{
    public class ESLoteService : BaseService<ESLote>, IESLoteService
    {
        private readonly IESLoteRepository _IESLoteRepository;

        public ESLoteService(IESLoteRepository pIESLoteRepository) : 
             base(pIESLoteRepository)
        {
            _IESLoteRepository = pIESLoteRepository;
        }
    }
}
