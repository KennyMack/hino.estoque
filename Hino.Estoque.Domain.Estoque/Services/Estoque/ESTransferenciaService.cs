using Hino.Estoque.Domain.Estoque.Interfaces.Repositories.Estoque;
using Hino.Estoque.Domain.Estoque.Interfaces.Services.Estoque;
using Hino.Estoque.Infra.Cross.Entities.Estoque;
using Hino.Estoque.Domain.Base.Services;

namespace Hino.Estoque.Domain.Estoque.Services.Estoque
{
    public class ESTransferenciaService : BaseService<ESTransferencia>, IESTransferenciaService
    {
        private readonly IESTransferenciaRepository _IESTransferenciaRepository;

        public ESTransferenciaService(IESTransferenciaRepository pIESTransferenciaRepository) : 
             base(pIESTransferenciaRepository)
        {
            _IESTransferenciaRepository = pIESTransferenciaRepository;
        }
    }
}
