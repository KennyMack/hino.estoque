using Hino.Estoque.Domain.Estoque.Interfaces.Repositories.Estoque;
using Hino.Estoque.Domain.Estoque.Interfaces.Services.Estoque;
using Hino.Estoque.Infra.Cross.Entities.Estoque;
using Hino.Estoque.Domain.Base.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.Estoque.Domain.Estoque.Services.Estoque
{
    public class ESInventDetService : BaseService<ESInventDet>, IESInventDetService
    {
        private readonly IESInventDetRepository _IESInventDetRepository;

        public ESInventDetService(IESInventDetRepository pIESInventDetRepository) : 
             base(pIESInventDetRepository)
        {
            _IESInventDetRepository = pIESInventDetRepository;
        }

        public async Task<IEnumerable<ESInventDet>> GetItensInventariosPendentes(short pCodEstab) =>
            await _IESInventDetRepository.GetItensInventariosPendentes(pCodEstab);

        public async Task<IEnumerable<ESInventDet>> GetItensInventario(short pCodEstab, decimal pCodInv, decimal pContagem) =>
            await _IESInventDetRepository.GetItensInventario(pCodEstab, pCodInv, pContagem);

        public async Task<ESInventDet> GetById(short pCodEstab, decimal pCodInv, decimal pContagem, string pCodProduto, string pCodEstoque) =>
            await _IESInventDetRepository.GetById(pCodEstab, pCodInv, pContagem, pCodProduto, pCodEstoque);
    }
}
