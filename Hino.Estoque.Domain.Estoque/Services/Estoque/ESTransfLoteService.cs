using Hino.Estoque.Domain.Estoque.Interfaces.Repositories.Estoque;
using Hino.Estoque.Domain.Estoque.Interfaces.Services.Estoque;
using Hino.Estoque.Infra.Cross.Entities.Estoque;
using Hino.Estoque.Domain.Base.Services;

namespace Hino.Estoque.Domain.Estoque.Services.Estoque
{
    public class ESTransfLoteService : BaseService<ESTransfLote>, IESTransfLoteService
    {
        private readonly IESTransfLoteRepository _IESTransfLoteRepository;

        public ESTransfLoteService(IESTransfLoteRepository pIESTransfLoteRepository) : 
             base(pIESTransfLoteRepository)
        {
            _IESTransfLoteRepository = pIESTransfLoteRepository;
        }
    }
}
