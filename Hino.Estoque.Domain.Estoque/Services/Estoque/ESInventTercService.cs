using Hino.Estoque.Domain.Estoque.Interfaces.Repositories.Estoque;
using Hino.Estoque.Domain.Estoque.Interfaces.Services.Estoque;
using Hino.Estoque.Infra.Cross.Entities.Estoque;
using Hino.Estoque.Domain.Base.Services;

namespace Hino.Estoque.Domain.Estoque.Services.Estoque
{
    public class ESInventTercService : BaseService<ESInventTerc>, IESInventTercService
    {
        private readonly IESInventTercRepository _IESInventTercRepository;

        public ESInventTercService(IESInventTercRepository pIESInventTercRepository) : 
             base(pIESInventTercRepository)
        {
            _IESInventTercRepository = pIESInventTercRepository;
        }
    }
}
