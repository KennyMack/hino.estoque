using Hino.Estoque.Domain.Estoque.Interfaces.Repositories.Estoque;
using Hino.Estoque.Domain.Estoque.Interfaces.Services.Estoque;
using Hino.Estoque.Infra.Cross.Entities.Estoque;
using Hino.Estoque.Domain.Base.Services;
using System.Threading.Tasks;
using System.Collections.Generic;
using System;

namespace Hino.Estoque.Domain.Estoque.Services.Estoque
{
    public class ESLoteSaldoService : BaseService<ESLoteSaldo>, IESLoteSaldoService
    {
        private readonly IESLoteSaldoRepository _IESLoteSaldoRepository;

        public ESLoteSaldoService(IESLoteSaldoRepository pIESLoteSaldoRepository) : 
             base(pIESLoteSaldoRepository)
        {
            _IESLoteSaldoRepository = pIESLoteSaldoRepository;
        }

        public async Task<double> SaldoEstoqueLote(short pCodEstab, decimal pLote, string pCodEstoque) =>
            await _IESLoteSaldoRepository.SaldoEstoqueLote(pCodEstab, pLote, pCodEstoque);

        public async Task<IEnumerable<esTransfUNLotes>> GetLotesUnidadeNegAsync(int pCodEstab, string pCodProduto, string pCodEstoque) =>
            await _IESLoteSaldoRepository.GetLotesUnidadeNegAsync(pCodEstab, pCodProduto, pCodEstoque);

        public async Task<IEnumerable<ESSaldoLoteOP>> BuscaLoteSaldoOPAsync(int pCodEstab, string pCodComponente, string pCodEstoqueOri, decimal pCodOrdProd, string pNivelOrdProd) =>
            await _IESLoteSaldoRepository.BuscaLoteSaldoOPAsync(pCodEstab, pCodComponente, pCodEstoqueOri, Convert.ToInt64(pCodOrdProd), pNivelOrdProd);
        


    }
}
