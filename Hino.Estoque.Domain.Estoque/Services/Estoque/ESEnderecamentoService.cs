﻿using Hino.Estoque.Domain.Base.Services;
using Hino.Estoque.Domain.Estoque.Interfaces.Repositories.Estoque;
using Hino.Estoque.Domain.Estoque.Interfaces.Services.Estoque;
using Hino.Estoque.Infra.Cross.Entities.Estoque;

namespace Hino.Estoque.Domain.Estoque.Services.Estoque
{
    public class ESEnderecamentoService : BaseService<ESEnderecamento>, IESEnderecamentoService
    {
        private readonly IESEnderecamentoRepository _IESEnderecamentoRepository;

        public ESEnderecamentoService(IESEnderecamentoRepository pIESEnderecamentoRepository) :
             base(pIESEnderecamentoRepository)
        {
            _IESEnderecamentoRepository = pIESEnderecamentoRepository;
        }
    }
}
