using Hino.Estoque.Domain.Estoque.Interfaces.Repositories.Estoque;
using Hino.Estoque.Domain.Estoque.Interfaces.Services.Estoque;
using Hino.Estoque.Infra.Cross.Entities.Estoque;
using Hino.Estoque.Domain.Base.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using Hino.Estoque.Infra.Cross.Utils.Exceptions;
using Hino.Estoque.Infra.Cross.Utils;
using System.Linq;
using System.Configuration;

namespace Hino.Estoque.Domain.Estoque.Services.Estoque
{
    public class ESInventarioService : BaseService<ESInventario>, IESInventarioService
    {
        private readonly IESInventarioRepository _IESInventarioRepository;

        public ESInventarioService(IESInventarioRepository pIESInventarioRepository) :
             base(pIESInventarioRepository)
        {
            _IESInventarioRepository = pIESInventarioRepository;
        }

        public int GetMaxInventCount(short pCodEstab, decimal pCodInv) =>
            _IESInventarioRepository.GetMaxInventCount(pCodEstab, pCodInv);

        public async Task<ESCreateItemInventario> CreateItemInventarioAsync(ESCreateItemInventario pCreateItemInventario)
        {
            if (pCreateItemInventario.codproduto.IsEmpty())
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { Infra.Cross.Resources.MessagesResource.ProductNotInformed }
                });
                return null;
            }
            if (pCreateItemInventario.codestoque.IsEmpty())
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { Infra.Cross.Resources.MessagesResource.StockNotInformed }
                });
                return null;
            }
            if (pCreateItemInventario.codinv <= 0)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { Infra.Cross.Resources.MessagesResource.InventoryNotInformed }
                });
                return null;
            }

            var inventario = await _IESInventarioRepository.QueryAsync(r =>
                r.codestab == pCreateItemInventario.codestab &&
                r.codinv == pCreateItemInventario.codinv);

            if (!inventario.Any())
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { Infra.Cross.Resources.MessagesResource.InventoryInvalid }
                });
                return null;
            }

            var OnlyLastList = Convert.ToBoolean(ConfigurationManager.AppSettings.Get("OnlyLastList") ?? "false");
            if (OnlyLastList)
            {
                var LastCount = _IESInventarioRepository.GetMaxInventCount(pCreateItemInventario.codestab, pCreateItemInventario.codinv);
                if (LastCount > pCreateItemInventario.contagem)
                {
                    Errors.Add(new ModelException
                    {
                        ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                        Field = "Body",
                        Value = "",
                        Messages = new string[] { string.Format(Infra.Cross.Resources.MessagesResource.InventCountIs, LastCount) }
                    });
                    return null;
                }
            }

            try
            {
                pCreateItemInventario.codproduto = await _IESInventarioRepository.CreateItemInventarioAsync(pCreateItemInventario);
            }
            catch (Exception ex)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { ex.Message.LimpaMensagemOracle() }
                });
                return null;
            }
            return pCreateItemInventario;
        }

        public async Task<ESInventario> GetInventarioAsync(short pCodEstab, decimal pCodInv, decimal pContagem) =>
            await _IESInventarioRepository.GetInventarioAsync(pCodEstab, pCodInv, pContagem);

        public async Task<IEnumerable<ESInventario>> GetInventariosPendentes(short pCodEstab) =>
            await _IESInventarioRepository.GetInventariosPendentes(pCodEstab);
    }
}
