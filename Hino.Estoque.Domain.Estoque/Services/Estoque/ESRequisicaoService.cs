﻿using Hino.Estoque.Domain.Base.Services;
using Hino.Estoque.Domain.Estoque.Interfaces.Repositories.Estoque;
using Hino.Estoque.Domain.Estoque.Interfaces.Services.Estoque;
using Hino.Estoque.Infra.Cross.Entities.Estoque;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Domain.Estoque.Services.Estoque
{
    public class ESRequisicaoService : BaseService<ESRequisicao>, IESRequisicaoService
    {
        private readonly IESRequisicaoRepository _IESRequisicaoRepository;

        public ESRequisicaoService(IESRequisicaoRepository pIESRequisicaoRepository) :
             base(pIESRequisicaoRepository)
        {
            _IESRequisicaoRepository = pIESRequisicaoRepository;
        }
    }
}
