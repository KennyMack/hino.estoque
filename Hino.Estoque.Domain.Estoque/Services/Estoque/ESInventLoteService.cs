using Hino.Estoque.Domain.Estoque.Interfaces.Repositories.Estoque;
using Hino.Estoque.Domain.Estoque.Interfaces.Services.Estoque;
using Hino.Estoque.Infra.Cross.Entities.Estoque;
using Hino.Estoque.Domain.Base.Services;

namespace Hino.Estoque.Domain.Estoque.Services.Estoque
{
    public class ESInventLoteService : BaseService<ESInventLote>, IESInventLoteService
    {
        private readonly IESInventLoteRepository _IESInventLoteRepository;

        public ESInventLoteService(IESInventLoteRepository pIESInventLoteRepository) : 
             base(pIESInventLoteRepository)
        {
            _IESInventLoteRepository = pIESInventLoteRepository;
        }
    }
}
