using Hino.Estoque.Domain.Estoque.Interfaces.Repositories.Estoque;
using Hino.Estoque.Domain.Estoque.Interfaces.Services.Estoque;
using Hino.Estoque.Infra.Cross.Entities.Estoque;
using Hino.Estoque.Domain.Base.Services;

namespace Hino.Estoque.Domain.Estoque.Services.Estoque
{
    public class ESTransfDetService : BaseService<ESTransfDet>, IESTransfDetService
    {
        private readonly IESTransfDetRepository _IESTransfDetRepository;

        public ESTransfDetService(IESTransfDetRepository pIESTransfDetRepository) : 
             base(pIESTransfDetRepository)
        {
            _IESTransfDetRepository = pIESTransfDetRepository;
        }
    }
}
