using Hino.Estoque.Domain.Estoque.Interfaces.Repositories.Estoque;
using Hino.Estoque.Domain.Estoque.Interfaces.Services.Estoque;
using Hino.Estoque.Infra.Cross.Entities.Estoque;
using Hino.Estoque.Domain.Base.Services;
using System.Threading.Tasks;
using Hino.Estoque.Aplication.ViewModels.Estoque;
using Hino.Estoque.Infra.Cross.Utils.Exceptions;
using System;

namespace Hino.Estoque.Domain.Estoque.Services.Estoque
{
    public class ESKardexService : BaseService<ESKardex>, IESKardexService
    {
        private readonly IESKardexRepository _IESKardexRepository;

        public ESKardexService(IESKardexRepository pIESKardexRepository) : 
             base(pIESKardexRepository)
        {
            _IESKardexRepository = pIESKardexRepository;
        }

        public async Task<bool> CreateProdTransferenciaAsync(ESCreateProdTransferencia pCreateTransf)
        {
            try
            {
                await _IESKardexRepository.CreateProdTransferenciaAsync(pCreateTransf);
            }
            catch (System.Exception ex)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { ex.Message }
                });

                return false;
            }
            return true;
        }

        public async Task<bool> GeraMovimentoRequisicaoAsync(
            short pCodEstab, long pCodRequisicao, string pCodUsuario,
            long pLote, decimal pQuantidade)
        {
            try
            {
                await _IESKardexRepository.GeraMovimentoRequisicaoAsync(
                    pCodEstab, pCodRequisicao, 
                    pCodUsuario, pLote, pQuantidade);
            }
            catch (Exception ex)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { ex.Message }
                });

                return false;
            }
            return true;
        }

        public async Task<DateTime> GetDataFechamento(short pCodEstab)
        {
            try
            {
                return await _IESKardexRepository.GetDataFechamento(pCodEstab);
            }
            catch (Exception ex)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { ex.Message }
                });
            }
            return new DateTime(1900, 01, 01);
        }

        public async Task<bool> GetValidaSaldoDataLancConsumo(short pCodEstab, string pCodProduto, string pCodEstoque, decimal pQuantidade, DateTime pDtLanc)
        {
            try
            {
                await _IESKardexRepository.GetValidaSaldoDataLancConsumo(pCodEstab, pCodProduto, pCodEstoque, pQuantidade, pDtLanc);
            }
            catch (Exception ex)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { ex.Message }
                });

                return false;
            }
            return true;
        }

        public async Task<EPermisEstoque> ValidaPermisMovEstoqueAsync(int pCodEstab, string pCodUsuario, DateTime pData, string pCodEstoque) =>
            await _IESKardexRepository.ValidaPermisMovEstoqueAsync(pCodEstab, pCodUsuario, pData, pCodEstoque);
    }
}
