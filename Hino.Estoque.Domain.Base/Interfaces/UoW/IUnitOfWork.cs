﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Domain.Base.Interfaces.UoW
{
    public interface IUnitOfWork : IDisposable
    {
        long RowsAffected { get; }
        void Commit();
        void Rollback();
    }
}
