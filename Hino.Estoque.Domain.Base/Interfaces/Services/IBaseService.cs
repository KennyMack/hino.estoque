﻿using Hino.Estoque.Infra.Cross.Utils.Paging;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Hino.Estoque.Infra.Cross.Utils.Exceptions;

namespace Hino.Estoque.Domain.Base.Interfaces.Services
{
    public interface IBaseService<T> where T : class
    {
        List<ModelException> Errors { get; set; }
        T Add(T model);
        Task<PagedResult<T>> GetAllPagedAsync(int page, int pageSize, params Expression<Func<T, object>>[] includeProperties);
        Task<IEnumerable<T>> GetAllAsync(params Expression<Func<T, object>>[] includeProperties);
        Task<IEnumerable<T>> QueryAsync(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties);
        Task<PagedResult<T>> QueryPagedAsync(int page, int pageSize, Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties);
        T Update(T model);
        void Remove(T model);
        long NextSequence();
        Task<long> NextSequenceAsync();
        Task<int> SaveChanges();
        void RollBackChanges();
        void Dispose();
    }
}
