﻿namespace Hino.Estoque.Domain.Base.Enums
{
    public enum EMaritalStatus
    {
        Single = 0,
        Married = 1,
        Divorced = 2,
        Widower = 3,
        Separated = 4
    }
}
