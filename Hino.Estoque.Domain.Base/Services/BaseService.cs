﻿using Hino.Estoque.Domain.Base.Exceptions;
using Hino.Estoque.Domain.Base.Interfaces.Repositories;
using Hino.Estoque.Domain.Base.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Utils.Exceptions;
using Hino.Estoque.Infra.Cross.Utils.Paging;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.Estoque.Domain.Base.Services
{
    public class BaseService<T> : IDisposable, IBaseService<T> where T : class
    {
        public List<ModelException> Errors { get; set; }
        private readonly IBaseRepository<T> BaseRepo;

        public BaseService(IBaseRepository<T> repo)
        {
            BaseRepo = repo;
            Errors = new List<ModelException>();
        }

        public virtual T Add(T model)
        {
            try
            {
                BaseRepo.Add(model);
            }
            catch (Exception e)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InsertSQLError,
                    Field = e.HelpLink,
                    Value = e.Source,
                    Messages = new string[] { e.Message,
                                              e?.InnerException?.InnerException?.Message  }
                });
            }
            return model;
        }

        public virtual T Update(T model)
        {
            try
            {
                BaseRepo.Update(model);
            }
            catch (Exception e)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.UpdateSQLError,
                    Field = e.HelpLink,
                    Value = e.Source,
                    Messages = new string[] { e.Message,
                                              e?.InnerException?.InnerException?.Message }
                });
            }
            return model;
        }

        public virtual void Remove(T model)
        {
            try
            {
                BaseRepo.Remove(model);
            }
            catch (Exception e)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.DeleteSQLError,
                    Field = e.HelpLink,
                    Value = e.Source,
                    Messages = new string[] { e.Message,
                                              e?.InnerException?.InnerException?.Message  }
                });
            }
        }

        public virtual async Task<PagedResult<T>> GetAllPagedAsync(int page, int pageSize, params Expression<Func<T, object>>[] includeProperties) =>
            await BaseRepo.GetAllPagedAsync(page, pageSize, includeProperties);        

        public virtual async Task<IEnumerable<T>> GetAllAsync(params Expression<Func<T, object>>[] includeProperties) =>
            await BaseRepo.GetAllAsync(includeProperties);

        public virtual async Task<IEnumerable<T>> QueryAsync(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties) =>
            await BaseRepo.QueryAsync(predicate, includeProperties);        

        public virtual async Task<PagedResult<T>> QueryPagedAsync(int page, int pageSize, Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties) =>
            await BaseRepo.QueryPagedAsync(page, pageSize, predicate, includeProperties);

        public virtual long NextSequence() => BaseRepo.NextSequence();

        public virtual async Task<long> NextSequenceAsync() => await BaseRepo.NextSequenceAsync();

        public async Task<int> SaveChanges()
        {
            try
            {
                return await BaseRepo.SaveChanges();
            }
            catch (Exception e)
            {
                if (e is IModelEntityValidationException)
                {
                    Errors = ((IModelEntityValidationException)e).MException;
                }
                else
                {

                    Errors.Add(new ModelException
                    {
                        ErrorCode = (int)EExceptionErrorCodes.SaveSQLError,
                        Field = e.HelpLink,
                        Value = e.Source,
                        Messages = new string[] { e.Message,
                                              e?.InnerException?.InnerException?.Message }
                    });
                }
            }
            return -1;
        }

        public void RollBackChanges()
        {
            try
            {
                BaseRepo.RollBackChanges();
            }
            catch (Exception e)
            {
                if (e is IModelEntityValidationException)
                {
                    Errors = ((IModelEntityValidationException)e).MException;
                }
                else
                {

                    Errors.Add(new ModelException
                    {
                        ErrorCode = (int)EExceptionErrorCodes.SaveSQLError,
                        Field = e.HelpLink,
                        Value = e.Source,
                        Messages = new string[] { e.Message,
                                              e?.InnerException?.InnerException?.Message }
                    });
                }
            }
        }

        public void Dispose()
        {
            BaseRepo.Dispose();
            //GC.SuppressFinalize(this);
        }

        
    }
}
