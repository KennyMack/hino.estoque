﻿using Hino.Estoque.Infra.Cross.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Domain.Base.Exceptions
{
    public interface IModelEntityValidationException
    {
        string Message { get; }
        List<ModelException> MException { get; }
    }
}
