﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Service.Entities
{
    public class peProduto
    {
        public string CodProduto { get; set; }
        public string Desenho { get; set; }
        public string DesenhoVinculado { get; set; }
        public string RevDesenho { get; set; }
        public string RevDesenhoVinculado { get; set; }
    }
}
