﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Service.Entities
{
    public class geEmpresaEndereco
    {
        public Int32? codempresa { get; set; }
        public Int32? codendereco { get; set; }
        /// <summary>
        /// Tipo do endereço
        /// 0-Comercial 
        /// 1-Entrega 
        /// 2-Cobranca 
        /// 3-Residencial
        /// </summary>
        public SByte? tipo { get; set; }
        public String desctipo { get; set; }
        public String endereco { get; set; }
        public Int32? numero { get; set; }
        public String complemento { get; set; }
        public String bairro { get; set; }
        public String cep { get; set; }
        public String cxpostal { get; set; }
        public String telefone { get; set; }
        public String fax { get; set; }
        public String contato { get; set; }
        public String email { get; set; }
        public String site { get; set; }
        public SByte? tipocnpjcpf { get; set; }
        public String numcnpjcpf { get; set; }
        public String inscestadual { get; set; }
        public String inscmunicipal { get; set; }
        public String inscsuframa { get; set; }
        public String doca { get; set; }
        public String planta { get; set; }
        public String codnatjur { get; set; }
        public Int16? codpais { get; set; }
        public Int16? codestado { get; set; }
        public Int32? codmunicipio { get; set; }
        public String genatjurDesnatjur { get; set; }
        public String gemunicipioNome { get; set; }
        public String geestadoSigla { get; set; }
        public String geestadoNome { get; set; }
        public String gepaisNome { get; set; }
        public Int64? nire { get; set; }
        public String rg { get; set; }
        public String emailnfe { get; set; }
        public String emailfinanceiro { get; set; }
        public String tpfornec { get; set; }
        public sbyte? cobradescarga { get; set; }
        public sbyte? cobraagend { get; set; }
        public sbyte? cobrasuframa { get; set; }
        public sbyte? cobrapalet { get; set; }
        public String nifvat { get; set; }
        public String rntrc { get; set; }

        #region Insert Address
        public async Task<bool> InsertAddressAsync(OracleConnection pConn)
        {
            bool retorno = true;

            // Define a instrução SQL
            string strSql = @"INSERT INTO GEENDERECO
                                (CODENDERECO, TIPO, ENDERECO, NUMERO,
                                 COMPLEMENTO, BAIRRO, CEP, CXPOSTAL,
                                 TELEFONE, FAX, CONTATO, EMAIL,
                                 SITE, TIPOCNPJCPF, NUMCNPJCPF, INSCESTADUAL,
                                 INSCMUNICIPAL, INSCSUFRAMA, DOCA, PLANTA,
                                 CODNATJUR, CODPAIS, CODESTADO, CODMUNICIPIO,
                                 NIRE, RG, EMAILNFE, EMAILFINANCEIRO, TPFORNEC,
                                 COBRADESCARGA, RNTRC, COBRAAGEND, COBRASUFRAMA,
                                 COBRAPALET, NIFVAT)
                              VALUES
                                (:pCODENDERECO, :pTIPO, :pENDERECO, :pNUMERO,
                                 :pCOMPLEMENTO, :pBAIRRO, :pCEP, :pCXPOSTAL,
                                 :pTELEFONE, :pFAX, :pCONTATO, :pEMAIL,
                                 :pSITE, :pTIPOCNPJCPF, :pNUMCNPJCPF, :pINSCESTADUAL,
                                 :pINSCMUNICIPAL, :pINSCSUFRAMA, :pDOCA, :pPLANTA,
                                 :pCODNATJUR, :pCODPAIS, :pCODESTADO, :pCODMUNICIPIO, 
                                 :pNIRE,:pRG, :pEMAILNFE, :pEMAILFINANCEIRO, :pTPFORNEC,
                                 :pCOBRADESCARGA, :pRNTRC, :pCOBRAAGEND, :pCOBRASUFRAMA,
                                 :pCOBRAPALET, :pNIFVAT)";


            // Cria o objeto command para executar a instruçao sql
            using (OracleCommand cmd = new OracleCommand(strSql, pConn))
            {
                // Define o tipo do comando
                cmd.CommandType = CommandType.Text;

                // Parâmetros
                cmd.Parameters.Add("pCODENDERECO", OracleDbType.Int32).Value = codendereco;
                cmd.Parameters.Add("pTIPO", OracleDbType.Int16).Value = tipo;
                cmd.Parameters.Add("pENDERECO", OracleDbType.Varchar2).Value = endereco.Trim();
                cmd.Parameters.Add("pNUMERO", OracleDbType.Int32).Value = numero ?? 0;
                cmd.Parameters.Add("pCOMPLEMENTO", OracleDbType.Varchar2).Value = complemento.Trim();
                cmd.Parameters.Add("pBAIRRO", OracleDbType.Varchar2).Value = bairro.Trim();
                cmd.Parameters.Add("pCEP", OracleDbType.Varchar2).Value = cep;
                cmd.Parameters.Add("pCXPOSTAL", OracleDbType.Varchar2).Value = cxpostal;
                cmd.Parameters.Add("pTELEFONE", OracleDbType.Varchar2).Value = telefone;
                cmd.Parameters.Add("pFAX", OracleDbType.Varchar2).Value = fax;
                cmd.Parameters.Add("pCONTATO", OracleDbType.Varchar2).Value = contato;
                cmd.Parameters.Add("pEMAIL", OracleDbType.Varchar2).Value = email;
                cmd.Parameters.Add("pSITE", OracleDbType.Varchar2).Value = site;
                cmd.Parameters.Add("pTIPOCNPJCPF", OracleDbType.Int16).Value = tipocnpjcpf;
                cmd.Parameters.Add("pNUMCNPJCPF", OracleDbType.Varchar2).Value = numcnpjcpf.Replace(".", "").Replace("/", "").Replace("-", "");
                cmd.Parameters.Add("pINSCESTADUAL", OracleDbType.Varchar2).Value = inscestadual;
                cmd.Parameters.Add("pINSCMUNICIPAL", OracleDbType.Varchar2).Value = inscmunicipal;
                cmd.Parameters.Add("pINSCSUFRAMA", OracleDbType.Varchar2).Value = inscsuframa;
                cmd.Parameters.Add("pDOCA", OracleDbType.Varchar2).Value = doca;
                cmd.Parameters.Add("pPLANTA", OracleDbType.Varchar2).Value = planta;
                cmd.Parameters.Add("pCODNATJUR", OracleDbType.Varchar2).Value = codnatjur;
                cmd.Parameters.Add("pCODPAIS", OracleDbType.Int16).Value = codpais;
                cmd.Parameters.Add("pCODESTADO", OracleDbType.Int16).Value = codestado;
                cmd.Parameters.Add("pCODMUNICIPIO", OracleDbType.Int32).Value = codmunicipio;
                cmd.Parameters.Add("pNIRE", OracleDbType.Int64).Value = nire;
                if (rg != null)
                {
                    if (rg.ToString() != "")
                        cmd.Parameters.Add("pRG", OracleDbType.Varchar2).Value = rg.Replace(".", "").Replace("-", "");
                    else
                        cmd.Parameters.Add("pRG", OracleDbType.Varchar2).Value = rg;
                }
                else
                    cmd.Parameters.Add("pRG", OracleDbType.Varchar2).Value = "";

                cmd.Parameters.Add("pEMAILNFE", OracleDbType.Varchar2).Value = emailnfe;
                cmd.Parameters.Add("pEMAILFINANCEIRO", OracleDbType.Varchar2).Value = emailfinanceiro;
                cmd.Parameters.Add("pTPFORNEC", OracleDbType.Varchar2).Value = tpfornec ?? "P";
                cmd.Parameters.Add("pCOBRADESCARGA", OracleDbType.Int16).Value = cobradescarga ?? 0;
                cmd.Parameters.Add("pRNTRC", OracleDbType.Varchar2).Value = rntrc;
                cmd.Parameters.Add("pCOBRAAGEND", OracleDbType.Int16).Value = cobraagend ?? 0;
                cmd.Parameters.Add("pCOBRASUFRAMA", OracleDbType.Int16).Value = cobrasuframa ?? 0;
                cmd.Parameters.Add("pCOBRAPALET", OracleDbType.Int16).Value = cobrapalet ?? 0;
                cmd.Parameters.Add("pNIFVAT", OracleDbType.Varchar2).Value = nifvat;

                // Executando o comando
                try
                {
                    await cmd.ExecuteNonQueryAsync();
                }
                catch (Exception)
                {
                    retorno = false;
                }
            }
            return retorno;
        }
        #endregion

        #region Insert Enterprise Address
        public async Task<bool> InsertEnterpriseAddressAsync(OracleConnection pConn)
        {
            bool retorno = true;

            // Define a instrução SQL
            string strSql = @"INSERT INTO GEENDEMPRESA
                                (CODENDERECO, CODEMPRESA)
                              VALUES
                                (:pCODENDERECO, :pCODEMPRESA)";


            // Cria o objeto command para executar a instruçao sql
            using (OracleCommand cmd = new OracleCommand(strSql, pConn))
            {
                // Define o tipo do comando
                cmd.CommandType = CommandType.Text;

                // Parâmetros
                cmd.Parameters.Add("pCODENDERECO", OracleDbType.Int32).Value = codendereco;
                cmd.Parameters.Add("pCODEMPRESA", OracleDbType.Int32).Value = codempresa;

                // Executando o comando
                try
                {
                    await cmd.ExecuteNonQueryAsync();
                }
                catch (Exception)
                {
                    retorno = false;
                }
            }
            return retorno;
        }
        #endregion

        #region Retorna o código do município
        public async Task<int?> RetCodMunicipioAsync(OracleConnection pConn, string pCodIbge)
        {
            int? retorno = null;

            // Define a instrução SQL
            string strSql = $@"SELECT MIN(CODMUNICIPIO) CODMUNICIPIO 
                                 FROM GEMUNICIPIO 
                                WHERE CODIBGE = LPAD('{pCodIbge}', 7, '0')";

            using (OracleCommand cmd = new OracleCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = pConn;
                cmd.CommandText = strSql;
                try
                {
                    retorno = Convert.ToInt32(await cmd.ExecuteScalarAsync());
                }
                catch (Exception)
                {
                }
            }

            return retorno;
        }
        #endregion

        #region Busca Cód. Estado pela sigla
        public async Task<short?> BuscaCodEstadoPorSiglaAsync(OracleConnection pConn, string pSigla)
        {
            short? retorno = null;

            // Define a instrução SQL
            string strSql = $@"SELECT MIN(GEESTADO.CODESTADO) CODESTADO
                                FROM GEESTADO
                               WHERE GEESTADO.SIGLA = :pSIGLA";

            using (OracleCommand cmd = new OracleCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = pConn;
                cmd.CommandText = strSql;

                cmd.Parameters.Add("pSIGLA", OracleDbType.Varchar2).Value = pSigla;
                try
                {
                    retorno = Convert.ToInt16(await cmd.ExecuteScalarAsync());
                }
                catch (Exception)
                {
                }
            }

            return retorno;
        }
        #endregion

        #region Busca Cód. Estado pelo IBGE
        public async Task<short?> BuscaCodEstadoPorIBGEAsync(OracleConnection pConn, string pIBGE)
        {
            short? retorno = null;

            // Define a instrução SQL
            string strSql = $@"SELECT MIN(GEESTADO.CODESTADO) CODESTADO
                                FROM GEESTADO
                               WHERE GEESTADO.CODIBGE = :pCODIBGE";

            using (OracleCommand cmd = new OracleCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = pConn;
                cmd.CommandText = strSql;

                cmd.Parameters.Add("pCODIBGE", OracleDbType.Varchar2).Value = pIBGE;
                try
                {
                    retorno = Convert.ToInt16(await cmd.ExecuteScalarAsync());
                }
                catch (Exception)
                {
                }
            }

            return retorno;
        }
        #endregion

        #region Retorna o próximo código do endereço
        public async Task<Int32> NextSequenceAsync(OracleConnection pConn)
        {
            int retorno = 0;

            // Define a instrução SQL
            string strSql = $@"SELECT SEQ_GEENDERECO.NEXTVAL FROM DUAL";

            using (OracleCommand cmd = new OracleCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = pConn;
                cmd.CommandText = strSql;

                try
                {
                    retorno = Convert.ToInt32(await cmd.ExecuteScalarAsync());
                }
                catch (Exception)
                {
                }
            }

            return retorno;
        }
        #endregion
    }
}
