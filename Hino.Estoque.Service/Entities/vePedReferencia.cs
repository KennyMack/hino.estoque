﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Service.Entities
{
    public class vePedReferencia
    {
        public string Pedido { get; set; }
        public long IdIntegracao { get; set; }
    }
}
