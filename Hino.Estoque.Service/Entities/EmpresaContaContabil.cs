﻿using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Service.Entities
{
    public class EmpresaContaContabil
    {
        public string ContaContabilNova { get; set; }
        public int UltimaContaCodestab { get; set; }

        public async Task<bool> GerarContaContabilAsync(OracleConnection pConn,
            string pTipoEmpresa,
            string pRazaoSocial,
            int pEmpNacEst)
        {
            using (OracleCommand cmd = new OracleCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = pConn;
                cmd.CommandText = "PCKG_CUSTOM_HOLEC.GERARCONTACONTABIL";

                cmd.Parameters.Add("pRETURN", OracleDbType.Clob);
                cmd.Parameters["pRETURN"].Direction = ParameterDirection.ReturnValue;

                cmd.Parameters.Add("pCODESTAB", OracleDbType.Int32).Value = Globals.CodEstab;
                cmd.Parameters["pCODESTAB"].Direction = ParameterDirection.Input;

                cmd.Parameters.Add("pTIPOEMPRESA", OracleDbType.Varchar2).Value = pTipoEmpresa;
                cmd.Parameters["pTIPOEMPRESA"].Direction = ParameterDirection.Input;

                cmd.Parameters.Add("pRAZAOSOCIAL", OracleDbType.Varchar2).Value = pRazaoSocial;
                cmd.Parameters["pRAZAOSOCIAL"].Direction = ParameterDirection.Input;

                cmd.Parameters.Add("pEMPNACEST", OracleDbType.Int32).Value = pEmpNacEst;
                cmd.Parameters["pEMPNACEST"].Direction = ParameterDirection.Input;

                try
                {
                    await cmd.ExecuteNonQueryAsync();

                    var ContaEstab = ((OracleClob)(cmd.Parameters["pRETURN"].Value)).Value.Split('|');

                    UltimaContaCodestab = Convert.ToInt32(ContaEstab[0]);
                    ContaContabilNova = ContaEstab[1];
                }
                catch (Exception)
                {
                    return false;
                }
            }

            return true;
        }
    }
}
