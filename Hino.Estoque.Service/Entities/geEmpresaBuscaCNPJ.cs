﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Service.Entities
{
    public class geEmpresaBuscaCNPJ
    {
        public List<geAtividadePrincipal> atividade_principal { get; set; }
        public DateTime? data_situacao { get; set; }
        public string nome { get; set; }
        public string uf { get; set; }
        public List<geAtividadesSecundarias> atividades_secundarias { get; set; }
        public List<geQSA> qsa { get; set; }
        public string situacao { get; set; }
        public string bairro { get; set; }
        public string logradouro { get; set; }
        public string numero { get; set; }
        public string cep { get; set; }
        public string cepLimpo
        {
            get
            {
                return ((cep ?? "").Replace("-", "").Replace(".", "")).Trim();
            }
        }
        public string municipio { get; set; }
        public string porte { get; set; }
        public string abertura { get; set; }
        public string natureza_juridica { get; set; }
        public string fantasia { get; set; }
        public string nomefantasia
        {
            get
            {
                if (string.IsNullOrEmpty(fantasia))
                    return nome;

                return fantasia;
            }
        }

        public string cnpj { get; set; }
        public string cnpjLimpo
        {
            get
            {
                return ((cnpj ?? "").Replace(".", "").Replace("/", "").Replace("-", "")).Trim();
            }
        }
        public DateTime? ultima_atualizacao { get; set; }
        public string status { get; set; }
        public string tipo { get; set; }
        public string complemento { get; set; }
        public string email { get; set; }
        public string telefone { get; set; }
        public string primeirotelefone
        {
            get
            {
                if (!string.IsNullOrEmpty(telefone))
                {
                    if (telefone.IndexOf("/") > 0)
                        return telefone.Split('/')[0].Trim();

                    return telefone;
                }

                return "";
            }
        }
        public string efr { get; set; }
        public string motivo_situacao { get; set; }
        public string situacao_especial { get; set; }
        public DateTime? data_situacao_especial { get; set; }
        public string capital_social { get; set; }
        public geExtra extra { get; set; }

        public geCEPResult Endereco { get; set; }


        public geEmpresaBuscaCNPJ()
        {
            atividade_principal = new List<geAtividadePrincipal>();
            atividades_secundarias = new List<geAtividadesSecundarias>();
            qsa = new List<geQSA>();
        }

    }

    public class geAtividadePrincipal
    {
        public string code { get; set; }
        public string text { get; set; }
    }

    public class geAtividadesSecundarias
    {
        public string code { get; set; }
        public string text { get; set; }
    }

    public class geQSA
    {
        public string qual { get; set; }
        public string nome { get; set; }
    }


    public class geCEPEndereco
    {
        public string cep { get; set; }
        public string logradouro { get; set; }
        public string complemento { get; set; }
        public string bairro { get; set; }
        public string localidade { get; set; }
        public string uf { get; set; }
        public string unidade { get; set; }
        public string ibge { get; set; }
        public string gia { get; set; }
    }


    public class geCEPResult
    {
        public int status { get; set; }
        public bool success { get; set; }
        public geCEPEndereco data { get; set; }
    }

    public class geExtra
    {

    }
}
