﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Service.Entities
{
    public class geEmpresa
    {
        
        public Int32? codempresa { get; set; }
        public String razaosocial { get; set; }
        public String nomefantasia { get; set; }
        public String descstatus { get; set; }
        public String desctipoempresa { get; set; }
        public String descclassifempresa { get; set; }
        public Int32? represinterno { get; set; }
        public String descrepresinterno { get; set; }
        public Decimal? codgrpfisemp { get; set; }
        public String fsgrpfisempdescricao { get; set; }
        public String contacontabil { get; set; }
        public String desccontacontabil { get; set; }
        public String contacontabiladiant { get; set; }
        public String desccontacontabiladiant { get; set; }
        public String contacomissaolib { get; set; }
        public String desccontacomissaolib { get; set; }
        public String contacomissaopag { get; set; }
        public String desccontacomissaopag { get; set; }
        public String motivobloqueio { get; set; }
        public String contasint { get; set; }
        public String desccsint { get; set; }
        public String contaadiantsint { get; set; }
        public String desccadiantsint { get; set; }
        public String contacomlibsint { get; set; }
        public String desccomlibcsint { get; set; }
        public String contacombpagsint { get; set; }
        public String desccompagcsint { get; set; }
        public Int32? codcomissao { get; set; }
        public String desccomissao { get; set; }
        public Int32? simplesnacional { get; set; }
        public String descsimplesnacional { get; set; }
        public DateTime? dtcertifvalid { get; set; }
        public String numerocertif { get; set; }
        public SByte? certificadoobrig { get; set; }
        public Int16? tagpersnfe { get; set; }
        public Int32? codestab { get; set; }
        public Decimal? status { get; set; }
        public String tipoempresa { get; set; }
        public String classifempresa { get; set; }
        public String geestabRazaosocial { get; set; }
        public Int32? codestabcc { get; set; }
        public Int32? codestabccadiant { get; set; }
        public Int32? codestabcccomissaolib { get; set; }
        public Int32? codestabcccomissaopag { get; set; }
        public SByte? semnftransito { get; set; }
        public SByte? descontatit { get; set; }
        public SByte? aprovarepres { get; set; }
        public SByte? tipodocquali { get; set; }
        public String tipofornec { get; set; }
        public String obsquali { get; set; }
        public Decimal? notaavaliacao { get; set; }
        public String codusuasist { get; set; }
        public SByte? travarnfeembdif { get; set; }


        #region Retorna o próximo código da empresa
        public async Task<Int32> NextSequenceAsync(OracleConnection pConn)
        {
            int retorno = 0;

            // Define a instrução SQL
            string strSql = $@"SELECT SEQ_GEEMPRESA.NEXTVAL FROM DUAL";

            using (OracleCommand cmd = new OracleCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = pConn;
                cmd.CommandText = strSql;
                
                try
                {
                    retorno = Convert.ToInt32(await cmd.ExecuteScalarAsync());
                }
                catch (Exception)
                {
                }
            }

            return retorno;
        }
        #endregion

        #region Insert Enterprise Cabec.
        public async Task<bool> InsertEnterpriseCabecAsync(OracleConnection pConn)
        {
            bool retorno = true;

            // Define a instrução SQL
            string strSql = @"INSERT INTO GEEMPRESA
                                (CODEMPRESA, RAZAOSOCIAL, NOMEFANTASIA)
                              VALUES
                                (:pCODEMPRESA, :pRAZAOSOCIAL, :pNOMEFANTASIA)";


            // Cria o objeto command para executar a instruçao sql
            using (OracleCommand cmd = new OracleCommand(strSql, pConn))
            {
                // Define o tipo do comando
                cmd.CommandType = CommandType.Text;

                // Parâmetros
                cmd.Parameters.Add("pCODEMPRESA", OracleDbType.Int32).Value = codempresa;
                cmd.Parameters.Add("pRAZAOSOCIAL", OracleDbType.Varchar2).Value = razaosocial.Trim();
                cmd.Parameters.Add("pNOMEFANTASIA", OracleDbType.Varchar2).Value = nomefantasia.Trim();

                // Executando o comando
                try
                {
                    await cmd.ExecuteNonQueryAsync();
                }
                catch (Exception)
                {
                    retorno = false;
                }
            }
            return retorno;
        }
        #endregion

        #region Insert Enterprise Param.
        public async Task<bool> InsertEnterpriseParamAsync(OracleConnection pConn)
        {
            bool retorno = true;

            // Define a instrução SQL
            string strSql = @"INSERT INTO GEEMPRESAPARAMESTAB
                                (CODEMPRESA, CODESTAB, STATUS, TIPOEMPRESA,
                                 CLASSIFEMPRESA, REPRESINTERNO, CODGRPFISEMP,
                                 CONTACONTABIL, CODESTABCC, CONTACONTABILADIANT,
                                 CODESTABCCADIANT, CONTACOMISSAOLIB, CODESTABCCCOMISSAOLIB,
                                 CONTACOMISSAOPAG, CODESTABCCCOMISSAOPAG, MOTIVOBLOQUEIO,
                                 CODCOMISSAO, SIMPLESNACIONAL, DTCERTIFVALID, 
                                 NUMEROCERTIF, CERTIFICADOOBRIG, TAGPERSNFE, SEMNFTRANSITO,
                                 DESCONTATIT, APROVAREPRES, TIPODOCQUALI, TIPOFORNEC,
                                 OBSQUALI, NOTAAVALIACAO, CODUSUASIST, TRAVARNFEEMBDIF,
                                 DATAMODIFICACAO)
                              VALUES
                                (:pCODEMPRESA, :pCODESTAB, :pSTATUS, :pTIPOEMPRESA,
                                 :pCLASSIFEMPRESA, :pREPRESINTERNO, :pCODGRPFISEMP,
                                 :pCONTACONTABIL, :pCODESTABCC, :pCONTACONTABILADIANT,
                                 :pCODESTABCCADIANT, :pCONTACOMISSAOLIB, :pCODESTABCCCOMISSAOLIB,
                                 :pCONTACOMISSAOPAG, :pCODESTABCCCOMISSAOPAG, :pMOTIVOBLOQUEIO,
                                 :pCODCOMISSAO, :pSIMPLESNACIONAL, :pDTCERTIFVALID, 
                                 :pNUMEROCERTIF, :pCERTIFICADOOBRIG, :pTAGPERSNFE, :pSEMNFTRANSITO,
                                 :pDESCONTATIT, :pAPROVAREPRES, :pTIPODOCQUALI, :pTIPOFORNEC,
                                 :pOBSQUALI, :pNOTAAVALIACAO, :pCODUSUASIST, :pTRAVARNFEEMBDIF,
                                 SYSDATE)";


            // Cria o objeto command para executar a instruçao sql
            using (OracleCommand cmd = new OracleCommand(strSql, pConn))
            {
                // Define o tipo do comando
                cmd.CommandType = CommandType.Text;

                // Parâmetros
                cmd.Parameters.Add("pCODEMPRESA", OracleDbType.Int32).Value = codempresa;
                cmd.Parameters.Add("pCODESTAB", OracleDbType.Int32).Value = codestab;
                cmd.Parameters.Add("pSTATUS", OracleDbType.Decimal).Value = status;
                cmd.Parameters.Add("pTIPOEMPRESA", OracleDbType.Varchar2).Value = tipoempresa;
                cmd.Parameters.Add("pCLASSIFEMPRESA", OracleDbType.Varchar2).Value = classifempresa;
                cmd.Parameters.Add("pREPRESINTERNO", OracleDbType.Decimal).Value = represinterno;
                cmd.Parameters.Add("pCODGRPFISEMP", OracleDbType.Int32).Value = codgrpfisemp;
                cmd.Parameters.Add("pCONTACONTABIL", OracleDbType.Varchar2).Value = contacontabil;
                cmd.Parameters.Add("pCODESTABCC", OracleDbType.Int32).Value = codestabcc;
                cmd.Parameters.Add("pCONTACONTABILADIANT", OracleDbType.Varchar2).Value = contacontabiladiant;
                cmd.Parameters.Add("pCODESTABCCADIANT", OracleDbType.Int32).Value = codestabccadiant;
                cmd.Parameters.Add("pCONTACOMISSAOLIB", OracleDbType.Varchar2).Value = contacomissaolib;
                cmd.Parameters.Add("pCODESTABCCCOMISSAOLIB", OracleDbType.Int32).Value = codestabcccomissaolib;
                cmd.Parameters.Add("pCONTACOMISSAOPAG", OracleDbType.Varchar2).Value = contacomissaopag;
                cmd.Parameters.Add("pCODESTABCCCOMISSAOPAG", OracleDbType.Int32).Value = codestabcccomissaopag;
                cmd.Parameters.Add("pMOTIVOBLOQUEIO", OracleDbType.Varchar2).Value = motivobloqueio;
                cmd.Parameters.Add("pCODCOMISSAO", OracleDbType.Int32).Value = codcomissao;
                cmd.Parameters.Add("pSIMPLESNACIONAL", OracleDbType.Int16).Value = simplesnacional;
                cmd.Parameters.Add("pDTCERTIFVALID", OracleDbType.Date).Value = dtcertifvalid;
                cmd.Parameters.Add("pNUMEROCERTIF", OracleDbType.Varchar2).Value = numerocertif;
                cmd.Parameters.Add("pCERTIFICADOOBRIG", OracleDbType.Int16).Value = certificadoobrig ?? 0;
                cmd.Parameters.Add("pTAGPERSNFE", OracleDbType.Int16).Value = tagpersnfe ?? 0;
                cmd.Parameters.Add("pSEMNFTRANSITO", OracleDbType.Int16).Value = semnftransito ?? 0;
                cmd.Parameters.Add("pDESCONTATIT", OracleDbType.Int16).Value = descontatit ?? 0;
                cmd.Parameters.Add("pAPROVAREPRES", OracleDbType.Int16).Value = aprovarepres ?? 0;
                cmd.Parameters.Add("pTIPODOCQUALI", OracleDbType.Int16).Value = tipodocquali;
                cmd.Parameters.Add("pTIPOFORNEC", OracleDbType.Varchar2).Value = tipofornec;
                cmd.Parameters.Add("pOBSQUALI", OracleDbType.Varchar2).Value = obsquali;
                cmd.Parameters.Add("pNOTAAVALIACAO", OracleDbType.Decimal).Value = notaavaliacao;
                cmd.Parameters.Add("pCODUSUASIST", OracleDbType.Varchar2).Value = codusuasist;
                cmd.Parameters.Add("pTRAVARNFEEMBDIF", OracleDbType.Int16).Value = travarnfeembdif ?? 0;

                // Executando o comando
                try
                {
                    await cmd.ExecuteNonQueryAsync();
                }
                catch (Exception)
                {
                    retorno = false;
                }
            }
            return retorno;
        }
        #endregion

        #region Insert Enterprise Pers
        public async Task<bool> InsertEnterprisePersAsync(OracleConnection pConn)
        {
            bool retorno = true;

            // Define a instrução SQL
            string strSql = @"INSERT INTO PEEMPRESA
                                (CODEMPRESA)
                              VALUES
                                (:pCODEMPRESA)";


            // Cria o objeto command para executar a instruçao sql
            using (OracleCommand cmd = new OracleCommand(strSql, pConn))
            {
                // Define o tipo do comando
                cmd.CommandType = CommandType.Text;

                // Parâmetros
                cmd.Parameters.Add("pCODEMPRESA", OracleDbType.Int32).Value = codempresa;

                // Executando o comando
                try
                {
                    await cmd.ExecuteNonQueryAsync();
                }
                catch (Exception)
                {
                    retorno = false;
                }
            }
            return retorno;
        }
        #endregion
    }
}
