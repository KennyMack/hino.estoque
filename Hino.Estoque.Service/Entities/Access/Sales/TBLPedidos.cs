﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Service.Entities.Access.Sales
{
    [Table("Hino_Pedidos")]
    public class TBLPedidos : BaseAccessEntity
    {
        [ExplicitKey]
        public int OrderID { get; set; }
        public string Pedido { get; set; }
        public DateTime DateOrdered { get; set; }
        public int Delivery { get; set; }
        public string CompanyID { get; set; }
        public string SPID { get; set; }
        public string CNPJCPFVendedor { get; set; }
        public string CarrierID { get; set; }
        public double PercentDiscount { get; set; }
        public double PercentFee { get; set; }
        public string Finalidade { get; set; }
        public string OrderIDClient { get; set; }
        public string Payment { get; set; }
        public string PaymentTerms { get; set; }
        public string Tipo { get; set; }
        public string Comprador { get; set; }
        public string OBS { get; set; }
        public int Antecipar { get; set; }
        public bool Cancelado { get; set; }

        public int? PagtoI { get; set; }
        public decimal? VLI { get; set; }

        public int? PagtoII { get; set; }
        public decimal? VLII { get; set; }

        public int? PagtoIII { get; set; }
        public decimal? VLIII { get; set; }

        public int? PagtoIV { get; set; }
        public decimal? VLIV { get; set; }

        public int? PagtoV { get; set; }
        public decimal? VLV { get; set; }

        public int? PagtoVI { get; set; }
        public decimal? VLVI { get; set; }

        public decimal Frete { get; set; }
        public int TipoPed { get; set; }
        public string RespCancel { get; set; }
        public double Comissao { get; set; }
        public string Proposta { get; set; }
        public bool Parcelado { get; set; }
        public string Montador { get; set; }
        public int TipoFrete { get; set; }
        public double ISS { get; set; }
        public double? CargaMedia { get; set; }
        public string TransportadoraI { get; set; }
        public int TipoFreteI { get; set; }
        public bool Boleto { get; set; }
        public bool Integrado { get; set; }
        public DateTime? DataEntrega { get; set; }
    }
}
