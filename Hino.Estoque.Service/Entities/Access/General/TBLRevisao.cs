﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Service.Entities.Access.General
{
    [Table("Hino_RevisaoDesenhos")]
    public class TBLRevisao : BaseAccessEntity
    {
        [ExplicitKey]
        public string Desenho { get; set; }
        public string Revisao { get; set; }
    }
}
