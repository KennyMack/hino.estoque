﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Service.Entities.Access.General
{
    [Table("Hino_ProdutosDet")]
    public class TBLProdutoDet : BaseAccessEntity
    {
        [ExplicitKey]
        public string PartNumber { get; set; }
        public decimal? UnitPrice { get; set; }
        public decimal? UnitCost { get; set; }
        public string FiscalClass { get; set; }
        public string DiscountClass { get; set; }
        public string LeadtimeClass { get; set; }
        public decimal? NetWeight { get; set; }
        public decimal? GrossWeight { get; set; }
        public decimal? Stock { get; set; }
        public decimal? StockLeast { get; set; }
        public decimal? Reserve { get; set; }
        public string Prevision { get; set; }
        public DateTime? Date { get; set; }
        public bool Apagar { get; set; }
    }
}
