﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Service.Entities.Access.General
{
    [Table("Hino_ClientesTransp")]
    public class TBLEmpresa : BaseAccessEntity
    {
        [ExplicitKey]
        public long ID { get; set; }
        public string CNPJCPF { get; set; }
        public string Inscricao { get; set; }
        public string GrupoEIP { get; set; }
        public string Site { get; set; }
        public string EMailNFe { get; set; }
        public int CodEstado { get; set; }
        public string Fone { get; set; }
        public string Fax { get; set; }
        public string Tipo { get; set; }
        public bool NContribuinte { get; set; }
        public bool Apagar { get; set; }
        public string EnderecoEntrega { get; set; }
        public string BairroEntrega { get; set; }
        public string FoneEntrega { get; set; }
        public string CidadeEntrega { get; set; }
        public string EstadoEntrega { get; set; }
        public string CepEntrega { get; set; }
        public string EnderecoCobranca { get; set; }
        public string BairroCobranca { get; set; }
        public string FoneCobranca { get; set; }
        public string CidadeCobranca { get; set; }
        public string EstadoCobranca { get; set; }
        public string CepCobranca { get; set; }
    }
}
