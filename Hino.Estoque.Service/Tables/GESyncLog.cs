﻿using Hino.Estoque.Service.DataBase.Oracle;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Service.Tables
{
    public class GESyncLog
    {
        #region Propriedades privadas
        private readonly DBOracle _bdCont;
        #endregion

        #region Propriedades públicas
        public int? codestab { get; set; }
        public long? codlog { get; set; }
        public DateTime? datelog { get; set; }
        public int? tipolog { get; set; }
        public string tabelalog { get; set; }
        public string logerro { get; set; }
        public string bodyerro { get; set; }
        public int? tentativas { get; set; }
        public int? concluido { get; set; }
        public string rowid { get; set; }
        #endregion

        #region Construtor
        public GESyncLog(DBOracle bdCont)
        {
            _bdCont = bdCont;
        }
        #endregion

        #region Select
        public String Select()
        {
            return @"SELECT CODESTAB, CODLOG, DATELOG, TIPOLOG, 
                            TABELALOG, LOGERRO, BODYERRO, TENTATIVAS,
                            CONCLUIDO
                       FROM GESYNCLOG";
        }
        #endregion

        #region Select filtrando por estabelecimento
        public String SelectErrosPedido(int pCodestab)
        {
            return string.Concat(this.Select(), string.Format(@" WHERE GESYNCLOG.CODESTAB = {0} 
                                                                   AND GESYNCLOG.CONCLUIDO = 0 
                                                                   AND GESYNCLOG.TABELALOG IN ('VEITEMPEDIDO', 'VEPEDVENDA')", pCodestab));
        }
        #endregion

        #region SelectSearch
        public String SelectSearch()
        {
            return @"SELECT CODESTAB, CODLOG, DATELOG, TIPOLOG, 
                            TABELALOG, LOGERRO, BODYERRO, TENTATIVAS,
                            CONCLUIDO
                       FROM GESYNCLOG";
        }
        #endregion

        #region Insert
        public Boolean Insert(out String mensErro)
        {
            bool retorno = true;
            mensErro = "";

            // Define a instrução SQL
            string strSql = @"INSERT INTO GESYNCLOG (CODESTAB, CODLOG, DATELOG, TIPOLOG, 
                                           TABELALOG, LOGERRO, BODYERRO, TENTATIVAS,
                                           CONCLUIDO)
                                   VALUES (:pCODESTAB, :pCODLOG, :pDATELOG, :pTIPOLOG,
                                           :pTABELALOG, :pLOGERRO, :pBODYERRO, :pTENTATIVAS,
                                           :pCONCLUIDO)";

            // Cria o objeto command para executar a instruçao sql
            using (OracleCommand cmd = new OracleCommand(strSql, _bdCont.AbreConn()))
            {
                // Define o tipo do comando
                cmd.CommandType = CommandType.Text;

                // Parâmetros
                cmd.Parameters.Add("pCODESTAB", OracleDbType.Int32).Value = codestab;
                cmd.Parameters.Add("pCODLOG", OracleDbType.Int64).Value = codlog;
                cmd.Parameters.Add("pDATELOG", OracleDbType.Date).Value = datelog;
                cmd.Parameters.Add("pTIPOLOG", OracleDbType.Int32).Value = tipolog;
                cmd.Parameters.Add("pTABELALOG", OracleDbType.Varchar2).Value = tabelalog;
                cmd.Parameters.Add("pLOGERRO", OracleDbType.Clob).Value = logerro;
                cmd.Parameters.Add("pBODYERRO", OracleDbType.Clob).Value = bodyerro;
                cmd.Parameters.Add("pTENTATIVAS", OracleDbType.Int32).Value = tentativas;
                cmd.Parameters.Add("pCONCLUIDO", OracleDbType.Int32).Value = concluido;

                // Executando o comando
                try
                {
                    cmd.ExecuteNonQuery();
                    this.Carrega();
                }
                catch (Exception e)
                {
                    mensErro = "Problema ao incluir o registro. Motivo: " + e.Message;
                    retorno = false;
                }

            }

            return retorno;
        }
        #endregion

        #region Update
        public Boolean Update(out String mensErro)
        {
            bool retorno = true;
            mensErro = "";

            // Define a instrução SQL
            string strSql = @"UPDATE GESYNCLOG
                                 SET DATELOG = :pDATELOG,
                                     TIPOLOG = :pTIPOLOG,
                                     TABELALOG = :pTABELALOG,
                                     LOGERRO = :pLOGERRO,
                                     BODYERRO = :pBODYERRO,
                                     TENTATIVAS = :pTENTATIVAS,
                                     CONCLUIDO =:pCONCLUIDO 
                               WHERE CODESTAB = :pCODESTAB
                                 AND CODLOG = :pCODLOG";

            // Cria o objeto command para executar a instruçao sql
            using (OracleCommand cmd = new OracleCommand(strSql, _bdCont.AbreConn()))
            {

                // Define o tipo do comando
                cmd.CommandType = CommandType.Text;

                // Parâmetros
                cmd.Parameters.Add("pDATELOG", OracleDbType.Date).Value = datelog;
                cmd.Parameters.Add("pTIPOLOG", OracleDbType.Int32).Value = tipolog;
                cmd.Parameters.Add("pTABELALOG", OracleDbType.Varchar2).Value = tabelalog;
                cmd.Parameters.Add("pLOGERRO", OracleDbType.Clob).Value = logerro;
                cmd.Parameters.Add("pBODYERRO", OracleDbType.Clob).Value = bodyerro;
                cmd.Parameters.Add("pTENTATIVAS", OracleDbType.Int32).Value = tentativas;
                cmd.Parameters.Add("pCONCLUIDO", OracleDbType.Int32).Value = concluido;
                cmd.Parameters.Add("pCODESTAB", OracleDbType.Int32).Value = codestab;
                cmd.Parameters.Add("pCODLOG", OracleDbType.Int64).Value = codlog;

                // Executando o comando
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    mensErro = "Problema ao alterar o registro. Motivo: " + e.Message;
                    retorno = false;
                }
            }

            return retorno;
        }
        #endregion

        #region Update Tentativa
        public Boolean UpdateRetries()
        {
            bool retorno = true;

            // Define a instrução SQL
            string strSql = @"UPDATE GESYNCLOG
                                 SET TENTATIVAS = :pTENTATIVAS
                               WHERE CODESTAB = :pCODESTAB
                                 AND CODLOG = :pCODLOG";

            // Cria o objeto command para executar a instruçao sql
            using (OracleCommand cmd = new OracleCommand(strSql, _bdCont.AbreConn()))
            {

                // Define o tipo do comando
                cmd.CommandType = CommandType.Text;

                // Parâmetros
                cmd.Parameters.Add("pTENTATIVAS", OracleDbType.Int32).Value = tentativas;
                cmd.Parameters.Add("pCODESTAB", OracleDbType.Int32).Value = codestab;
                cmd.Parameters.Add("pCODLOG", OracleDbType.Int64).Value = codlog;

                // Executando o comando
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception)
                {
                    retorno = false;
                }
            }

            return retorno;
        }
        #endregion

        #region Update To Complete
        public Boolean UpdateToComplete()
        {
            bool retorno = true;

            // Define a instrução SQL
            string strSql = @"UPDATE GESYNCLOG
                                 SET CONCLUIDO =:pCONCLUIDO 
                               WHERE CODESTAB = :pCODESTAB
                                 AND CODLOG = :pCODLOG";

            // Cria o objeto command para executar a instruçao sql
            using (OracleCommand cmd = new OracleCommand(strSql, _bdCont.AbreConn()))
            {

                // Define o tipo do comando
                cmd.CommandType = CommandType.Text;

                // Parâmetros
                cmd.Parameters.Add("pCONCLUIDO", OracleDbType.Int32).Value = concluido;
                cmd.Parameters.Add("pCODESTAB", OracleDbType.Int32).Value = codestab;
                cmd.Parameters.Add("pCODLOG", OracleDbType.Int64).Value = codlog;

                // Executando o comando
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception)
                {
                    retorno = false;
                }
            }

            return retorno;
        }
        #endregion

        #region Delete
        public Boolean Delete(out String mensErro)
        {
            bool retorno = true;
            mensErro = "";

            // Define a instrução SQL
            string strSql = @"DELETE
                                FROM GESYNCLOG
                               WHERE CODESTAB = :pCODESTAB
                                 AND CODLOG = :pCODLOG";

            // Cria o objeto command para executar a instruçao sql
            using (OracleCommand cmd = new OracleCommand(strSql, _bdCont.AbreConn()))
            {
                // Define o tipo do comando
                cmd.CommandType = CommandType.Text;

                // Parâmetros
                cmd.Parameters.Add("pCODESTAB", OracleDbType.Int32).Value = codestab;
                cmd.Parameters.Add("pCODLOG", OracleDbType.Int64).Value = codlog;

                // Executando o comando
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    mensErro = "Problema ao excluir o registro. Motivo: " + e.Message;
                    retorno = false;
                }
            }

            return retorno;
        }
        #endregion

        #region Valida
        public Boolean Valida(out String mensErro, out String cmpErro)
        {
            Boolean retorno = true;
            mensErro = "";
            cmpErro = "";

            return retorno;
        }
        #endregion

        #region Existe
        public Boolean Existe()
        {
            Boolean retorno = true;
            String strSql = @"SELECT COUNT(*) TOTREG
                                FROM GESYNCLOG
                               WHERE CODESTAB = :pCODESTAB
                                 AND CODLOG = :pCODLOG";

            // Cria o objeto command para executar a instruçao sql
            using (OracleCommand cmd = new OracleCommand(strSql, _bdCont.AbreConn()))
            {
                // Define o tipo do comando
                cmd.CommandType = CommandType.Text;

                // Parâmetros
                cmd.Parameters.Add("pCODESTAB", OracleDbType.Int32).Value = codestab;
                cmd.Parameters.Add("pCODLOG", OracleDbType.Int64).Value = codlog;

                // Executando o comando
                retorno = (int.Parse(cmd.ExecuteScalar().ToString()) > 0);
            }

            return retorno;
        }
        #endregion

        #region Carrega
        public bool Carrega()
        {
            var retorno = false;
            string strSql = Select();
            strSql = strSql.Replace("SELECT", "SELECT GESYNCLOG.ROWID,");

            if (strSql.IndexOf("WHERE") > 0)
                strSql += " AND ";
            else
                strSql += " WHERE ";

            strSql += @"GESYNCLOG.CODESTAB = :pCODESTAB
                    AND GESYNCLOG.CODLOG = :pCODLOG";


            // Cria o objeto command para executar a instruçao sql
            using (OracleCommand cmd = new OracleCommand(strSql, _bdCont.AbreConn()))
            {
                // Define o tipo do comando
                cmd.CommandType = CommandType.Text;

                // Parâmetros
                cmd.Parameters.Add("pCODESTAB", OracleDbType.Int32).Value = codestab;
                cmd.Parameters.Add("pCODLOG", OracleDbType.Int64).Value = codlog;

                // Cria um objeto reader
                using (OracleDataReader reader = cmd.ExecuteReader())
                {
                    // Carregando dados
                    reader.Read();
                    if (reader.HasRows)
                    {
                        retorno = true;
                        rowid = reader["ROWID"].ToString();
                        codestab = Int32.Parse(reader["CODESTAB"].ToString());
                        codlog = Int32.Parse(reader["CODLOG"].ToString());
                        datelog = DateTime.Parse(reader["DATELOG"].ToString());
                        tipolog = Int32.Parse(reader["TIPOLOG"].ToString());
                        tabelalog = (reader["TABELALOG"].ToString());
                        logerro = (reader["LOGERRO"].ToString());
                        bodyerro = (reader["BODYERRO"].ToString());
                        tentativas = Int32.Parse(reader["TENTATIVAS"].ToString());
                        concluido = Int32.Parse(reader["CONCLUIDO"].ToString());
                    }
                }
            }
            return retorno;
        }
        #endregion

        #region Obtem a descrição para os campos
        public String GetDescCampo(String campo)
        {
            String retorno = "";

            if (campo == "CODESTAB")
                retorno = "Código do estabelecimento";

            return retorno;
        }
        #endregion

        #region Retorna o próximo código
        public Int64 ProximoCod()
        {
            Int64 retorno;
            String strSql = @"SELECT SEQ_GESYNCLOG.NEXTVAL FROM DUAL";


            // Cria o objeto command para executar a instruçao sql
            using (OracleCommand cmd = new OracleCommand(strSql, _bdCont.AbreConn()))
            {
                // Define o tipo do comando
                cmd.CommandType = CommandType.Text;

                // Executando o comando
                retorno = Convert.ToInt64(cmd.ExecuteScalar());

                // Fecha a conexão
                cmd.Dispose();
            }

            return retorno;
        }
        #endregion
    }
}
