﻿using Hino.Estoque.Service.DataBase.Oracle;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Service.Tables
{
    public class GESincConfig
    {
        #region Propriedades privadas
        private readonly DBOracle _bdCont;
        #endregion

        #region Propriedades públicas
        public int? codestab { get; set; }
        public string establishmentkey { get; set; }
        public DateTime? ultimasincronia { get; set; }
        public DateTime? ultimabusca { get; set; }
        public DateTime? sincformapgto { get; set; }
        public DateTime? sinccondpgto { get; set; }
        public DateTime? sincregioes { get; set; }
        public DateTime? sincempresas { get; set; }
        public DateTime? sincprodutos { get; set; }
        public DateTime? sinctabpreco { get; set; }
        public DateTime? sincpedidos { get; set; }
        public DateTime? sincsaldoestoque { get; set; }
        public DateTime? sincpedaccess { get; set; }
        public DateTime? buscaformapgto { get; set; }
        public DateTime? buscacondpgto { get; set; }
        public DateTime? buscaregioes { get; set; }
        public DateTime? buscaempresas { get; set; }
        public DateTime? buscaprodutos { get; set; }
        public DateTime? buscatabpreco { get; set; }
        public DateTime? buscapedidos { get; set; }
        public DateTime? buscasaldoestoque { get; set; }
        public DateTime? buscapedaccess { get; set; }
        public string rowid { get; set; }
        #endregion

        #region Construtor
        public GESincConfig(DBOracle bdCont)
        {
            _bdCont = bdCont;
        }
        #endregion

        #region Select
        public String Select()
        {
            return @"SELECT CODESTAB, ESTABLISHMENTKEY, ULTIMASINCRONIA, ULTIMABUSCA,
                            SINCFORMAPGTO,
                            SINCCONDPGTO,
                            SINCREGIOES,
                            SINCEMPRESAS,
                            SINCPRODUTOS,
                            SINCTABPRECO,
                            SINCPEDIDOS,
                            SINCSALDOESTOQUE,
                            SINCPEDACCESS,
                            BUSCAFORMAPGTO,
                            BUSCACONDPGTO,
                            BUSCAREGIOES,
                            BUSCAEMPRESAS,
                            BUSCAPRODUTOS,
                            BUSCATABPRECO,
                            BUSCAPEDIDOS,
                            BUSCASALDOESTOQUE,
                            BUSCAPEDACCESS
                       FROM GESINCCONFIG";
        }
        #endregion

        #region SelectSearch
        public String SelectSearch()
        {
            return @"SELECT CODESTAB, ESTABLISHMENTKEY, ULTIMASINCRONIA, ULTIMABUSCA
                       FROM GESINCCONFIG";
        }
        #endregion

        #region Insert
        public Boolean Insert(out String mensErro)
        {
            bool retorno = true;
            mensErro = "";

            // Define a instrução SQL
            string strSql = @"INSERT INTO GESINCCONFIG
                                (CODESTAB, ESTABLISHMENTKEY, ULTIMASINCRONIA, ULTIMABUSCA,
                                 SINCFORMAPGTO, SINCCONDPGTO, SINCREGIOES, SINCEMPRESAS, 
                                 SINCPRODUTOS, SINCTABPRECO, SINCPEDIDOS, SINCSALDOESTOQUE, 
                                 SINCPEDACCESS,
                                 BUSCAFORMAPGTO, BUSCACONDPGTO, BUSCAREGIOES, BUSCAEMPRESAS, 
                                 BUSCAPRODUTOS, BUSCATABPRECO, BUSCAPEDIDOS, BUSCASALDOESTOQUE,
                                 BUSCAPEDACCESS)
                              VALUES
                                (:pCODESTAB, :pESTABLISHMENTKEY, :pULTIMASINCRONIA, :pULTIMABUSCA,
                                 :pSINCFORMAPGTO, :pSINCCONDPGTO, :pSINCREGIOES, :pSINCEMPRESAS, 
                                 :pSINCPRODUTOS, :pSINCTABPRECO, :pSINCPEDIDOS, :pSINCSALDOESTOQUE, 
                                 :pSINCPEDACCESS,
                                 :pBUSCAFORMAPGTO, :pBUSCACONDPGTO, :pBUSCAREGIOES, :pBUSCAEMPRESAS, 
                                 :pBUSCAPRODUTOS, :pBUSCATABPRECO, :pBUSCAPEDIDOS, BUSCASALDOESTOQUE,
                                 :pBUSCAPEDACCESS)";



            // Cria o objeto command para executar a instruçao sql
            using (OracleCommand cmd = new OracleCommand(strSql, _bdCont.AbreConn()))
            {
                // Define o tipo do comando
                cmd.CommandType = CommandType.Text;

                // Parâmetros
                cmd.Parameters.Add("pCODESTAB", OracleDbType.Int32).Value = codestab;
                cmd.Parameters.Add("pESTABLISHMENTKEY", OracleDbType.Varchar2).Value = establishmentkey;
                cmd.Parameters.Add("pULTIMASINCRONIA", OracleDbType.Date).Value = ultimasincronia;
                cmd.Parameters.Add("pULTIMABUSCA", OracleDbType.Date).Value = ultimabusca;
                cmd.Parameters.Add("pSINCFORMAPGTO", OracleDbType.Date).Value = sincformapgto;
                cmd.Parameters.Add("pSINCCONDPGTO", OracleDbType.Date).Value = sinccondpgto;
                cmd.Parameters.Add("pSINCREGIOES", OracleDbType.Date).Value = sincregioes;
                cmd.Parameters.Add("pSINCEMPRESAS", OracleDbType.Date).Value = sincempresas;
                cmd.Parameters.Add("pSINCPRODUTOS", OracleDbType.Date).Value = sincprodutos;
                cmd.Parameters.Add("pSINCTABPRECO", OracleDbType.Date).Value = sinctabpreco;
                cmd.Parameters.Add("pSINCPEDIDOS", OracleDbType.Date).Value = sincpedidos;
                cmd.Parameters.Add("pSINCSALDOESTOQUE", OracleDbType.Date).Value = sincsaldoestoque;
                cmd.Parameters.Add("pSINCPEDACCESS", OracleDbType.Date).Value = sincpedaccess;
                cmd.Parameters.Add("pBUSCAFORMAPGTO", OracleDbType.Date).Value = buscaformapgto;
                cmd.Parameters.Add("pBUSCACONDPGTO", OracleDbType.Date).Value = buscacondpgto;
                cmd.Parameters.Add("pBUSCAREGIOES", OracleDbType.Date).Value = buscaregioes;
                cmd.Parameters.Add("pBUSCAEMPRESAS", OracleDbType.Date).Value = buscaempresas;
                cmd.Parameters.Add("pBUSCAPRODUTOS", OracleDbType.Date).Value = buscaprodutos;
                cmd.Parameters.Add("pBUSCATABPRECO", OracleDbType.Date).Value = buscatabpreco;
                cmd.Parameters.Add("pBUSCAPEDIDOS", OracleDbType.Date).Value = buscapedidos;
                cmd.Parameters.Add("pBUSCASALDOESTOQUE", OracleDbType.Date).Value = buscasaldoestoque;
                cmd.Parameters.Add("pBUSCAPEDACCESS", OracleDbType.Date).Value = buscapedaccess;

                // Executando o comando
                try
                {
                    cmd.ExecuteNonQuery();
                    this.Carrega();
                }
                catch (Exception e)
                {
                    mensErro = "Problema ao incluir o registro. Motivo: " + e.Message;
                    retorno = false;
                }

            }

            return retorno;
        }
        #endregion

        #region Update
        public Boolean Update(out String mensErro)
        {
            bool retorno = true;
            mensErro = "";

            // Define a instrução SQL
            string strSql = @"UPDATE GESINCCONFIG
                                 SET ESTABLISHMENTKEY = :pESTABLISHMENTKEY,
                                     ULTIMASINCRONIA = :pULTIMASINCRONIA,
                                     ULTIMABUSCA = :pULTIMABUSCA,
                                     SINCFORMAPGTO = :pSINCFORMAPGTO,
                                     SINCCONDPGTO = :pSINCCONDPGTO,
                                     SINCREGIOES = :pSINCREGIOES,
                                     SINCEMPRESAS = :pSINCEMPRESAS,
                                     SINCPRODUTOS = :pSINCPRODUTOS,
                                     SINCTABPRECO = :pSINCTABPRECO,
                                     SINCPEDIDOS = :pSINCPEDIDOS,
                                     SINCSALDOESTOQUE = :pSINCSALDOESTOQUE,
                                     SINCPEDACCESS = :pSINCPEDACCESS,
                                     BUSCAFORMAPGTO = :pBUSCAFORMAPGTO,
                                     BUSCACONDPGTO = :pBUSCACONDPGTO,
                                     BUSCAREGIOES = :pBUSCAREGIOES,
                                     BUSCAEMPRESAS = :pBUSCAEMPRESAS,
                                     BUSCAPRODUTOS = :pBUSCAPRODUTOS,
                                     BUSCATABPRECO = :pBUSCATABPRECO,
                                     BUSCAPEDIDOS = :pBUSCAPEDIDOS,
                                     BUSCASALDOESTOQUE = :pBUSCASALDOESTOQUE,
                                     BUSCAPEDACCESS = :pBUSCAPEDACCESS
                               WHERE CODESTAB = :pCODESTAB";


            // Cria o objeto command para executar a instruçao sql
            using (OracleCommand cmd = new OracleCommand(strSql, _bdCont.AbreConn()))
            {

                // Define o tipo do comando
                cmd.CommandType = CommandType.Text;

                // Parâmetros
                cmd.Parameters.Add("pESTABLISHMENTKEY", OracleDbType.Varchar2).Value = establishmentkey;
                cmd.Parameters.Add("pULTIMASINCRONIA", OracleDbType.Date).Value = ultimasincronia;
                cmd.Parameters.Add("pULTIMABUSCA", OracleDbType.Date).Value = ultimabusca;
                cmd.Parameters.Add("pSINCFORMAPGTO", OracleDbType.Date).Value = sincformapgto;
                cmd.Parameters.Add("pSINCCONDPGTO", OracleDbType.Date).Value = sinccondpgto;
                cmd.Parameters.Add("pSINCREGIOES", OracleDbType.Date).Value = sincregioes;
                cmd.Parameters.Add("pSINCEMPRESAS", OracleDbType.Date).Value = sincempresas;
                cmd.Parameters.Add("pSINCPRODUTOS", OracleDbType.Date).Value = sincprodutos;
                cmd.Parameters.Add("pSINCTABPRECO", OracleDbType.Date).Value = sinctabpreco;
                cmd.Parameters.Add("pSINCPEDIDOS", OracleDbType.Date).Value = sincpedidos;
                cmd.Parameters.Add("pSINCSALDOESTOQUE", OracleDbType.Date).Value = sincsaldoestoque;
                cmd.Parameters.Add("pSINCPEDACCESS", OracleDbType.Date).Value = sincpedaccess;
                cmd.Parameters.Add("pBUSCAFORMAPGTO", OracleDbType.Date).Value = buscaformapgto;
                cmd.Parameters.Add("pBUSCACONDPGTO", OracleDbType.Date).Value = buscacondpgto;
                cmd.Parameters.Add("pBUSCAREGIOES", OracleDbType.Date).Value = buscaregioes;
                cmd.Parameters.Add("pBUSCAEMPRESAS", OracleDbType.Date).Value = buscaempresas;
                cmd.Parameters.Add("pBUSCAPRODUTOS", OracleDbType.Date).Value = buscaprodutos;
                cmd.Parameters.Add("pBUSCATABPRECO", OracleDbType.Date).Value = buscatabpreco;
                cmd.Parameters.Add("pBUSCAPEDIDOS", OracleDbType.Date).Value = buscapedidos;
                cmd.Parameters.Add("pBUSCASALDOESTOQUE", OracleDbType.Date).Value = buscasaldoestoque;
                cmd.Parameters.Add("pBUSCAPEDACCESS", OracleDbType.Date).Value = buscapedaccess;
                cmd.Parameters.Add("pCODESTAB", OracleDbType.Int32).Value = codestab;

                // Executando o comando
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    mensErro = "Problema ao alterar o registro. Motivo: " + e.Message;
                    retorno = false;
                }
            }

            return retorno;
        }
        #endregion

        #region Update Last Sync
        public Boolean UpdateLastSync(string pLastSync, DateTime pDate)
        {
            bool retorno = true;

            // Define a instrução SQL
            string strSql = $@"UPDATE GESINCCONFIG
                                 SET {pLastSync} = :pDATESYNC
                               WHERE CODESTAB = :pCODESTAB";


            // Cria o objeto command para executar a instruçao sql
            using (OracleCommand cmd = new OracleCommand(strSql, _bdCont.AbreConn()))
            {
                // Define o tipo do comando
                cmd.CommandType = CommandType.Text;

                // Parâmetros
                cmd.Parameters.Add("pDATESYNC", OracleDbType.Date).Value = pDate;
                cmd.Parameters.Add("pCODESTAB", OracleDbType.Int32).Value = codestab;

                // Executando o comando
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception)
                {
                    retorno = false;
                }
            }

            return retorno;
        }
        #endregion

        #region Update Last Push
        public Boolean UpdateLastPush(string pLastSync, DateTime pDate)
        {
            bool retorno = true;

            // Define a instrução SQL
            string strSql = $@"UPDATE GESINCCONFIG
                                 SET {pLastSync} = :pDATESYNC
                               WHERE CODESTAB = :pCODESTAB";


            // Cria o objeto command para executar a instruçao sql
            using (OracleCommand cmd = new OracleCommand(strSql, _bdCont.AbreConn()))
            {
                // Define o tipo do comando
                cmd.CommandType = CommandType.Text;

                // Parâmetros
                cmd.Parameters.Add("pDATESYNC", OracleDbType.Date).Value = pDate;
                cmd.Parameters.Add("pCODESTAB", OracleDbType.Int32).Value = codestab;

                // Executando o comando
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception)
                {
                    retorno = false;
                }
            }

            return retorno;
        }
        #endregion

        #region Delete
        public Boolean Delete(out String mensErro)
        {
            bool retorno = true;
            mensErro = "";

            // Define a instrução SQL
            string strSql = @"DELETE
                                FROM GESINCCONFIG
                               WHERE CODESTAB = :pCODESTAB";

            // Cria o objeto command para executar a instruçao sql
            using (OracleCommand cmd = new OracleCommand(strSql, _bdCont.AbreConn()))
            {
                // Define o tipo do comando
                cmd.CommandType = CommandType.Text;

                // Parâmetros
                cmd.Parameters.Add("pCODESTAB", OracleDbType.Int32).Value = codestab;

                // Executando o comando
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    mensErro = "Problema ao excluir o registro. Motivo: " + e.Message;
                    retorno = false;
                }
            }

            return retorno;
        }
        #endregion

        #region Valida
        public Boolean Valida(out String mensErro, out String cmpErro)
        {
            Boolean retorno = true;
            mensErro = "";
            cmpErro = "";

            if (codestab.ToString() == "")
            {
                mensErro = "Campo " + this.GetDescCampo("CODESTAB") + " é de preenchimento obrigatório.";
                cmpErro = "CODESTAB";
                retorno = false;
            }
            else if (establishmentkey == "")
            {
                mensErro = "Campo " + this.GetDescCampo("ESTABLISHMENTKEY") + " é de preenchimento obrigatório.";
                cmpErro = "ESTABLISHMENTKEY";
                retorno = false;
            }
            else if (ultimasincronia.ToString() == "")
            {
                mensErro = "Campo " + this.GetDescCampo("ULTIMASINCRONIA") + " é de preenchimento obrigatório.";
                cmpErro = "ULTIMASINCRONIA";
                retorno = false;
            }

            return retorno;
        }
        #endregion

        #region Existe
        public Boolean Existe()
        {
            Boolean retorno = true;
            String strSql = @"SELECT COUNT(*) TOTREG
                                FROM GESINCCONFIG
                               WHERE CODESTAB = :pCODESTAB";

            // Cria o objeto command para executar a instruçao sql
            using (OracleCommand cmd = new OracleCommand(strSql, _bdCont.AbreConn()))
            {
                // Define o tipo do comando
                cmd.CommandType = CommandType.Text;

                // Parâmetros
                cmd.Parameters.Add("pCODESTAB", OracleDbType.Int32).Value = codestab;

                // Executando o comando
                retorno = (int.Parse(cmd.ExecuteScalar().ToString()) > 0);
            }

            return retorno;
        }
        #endregion

        #region Carrega
        public bool Carrega()
        {
            var retorno = false;
            string strSql = Select();
            strSql = strSql.Replace("SELECT", "SELECT GESINCCONFIG.ROWID,");

            if (strSql.IndexOf("WHERE") > 0)
                strSql += " AND ";
            else
                strSql += " WHERE ";

            strSql += @"GESINCCONFIG.CODESTAB = :pCODESTAB";


            // Cria o objeto command para executar a instruçao sql
            using (OracleCommand cmd = new OracleCommand(strSql, _bdCont.AbreConn()))
            {
                // Define o tipo do comando
                cmd.CommandType = CommandType.Text;

                // Parâmetros
                cmd.Parameters.Add("pCODESTAB", OracleDbType.Int32).Value = codestab;

                // Cria um objeto reader
                using (OracleDataReader reader = cmd.ExecuteReader())
                {
                    // Carregando dados
                    reader.Read();
                    if (reader.HasRows)
                    {
                        retorno = true;
                        rowid = reader["ROWID"].ToString();
                        codestab = Int32.Parse(reader["CODESTAB"].ToString());
                        establishmentkey = (reader["ESTABLISHMENTKEY"].ToString());
                        ultimasincronia = DateTime.Parse(reader["ULTIMASINCRONIA"].ToString());
                        ultimabusca = DateTime.Parse(reader["ULTIMABUSCA"].ToString());

                        sincformapgto = Convert.ToDateTime(reader["SINCFORMAPGTO"]);
                        sinccondpgto = Convert.ToDateTime(reader["SINCCONDPGTO"]);
                        sincregioes = Convert.ToDateTime(reader["SINCREGIOES"]);
                        sincempresas = Convert.ToDateTime(reader["SINCEMPRESAS"]);
                        sincprodutos = Convert.ToDateTime(reader["SINCPRODUTOS"]);
                        sinctabpreco = Convert.ToDateTime(reader["SINCTABPRECO"]);
                        sincpedidos = Convert.ToDateTime(reader["SINCPEDIDOS"]);
                        sincsaldoestoque = Convert.ToDateTime(reader["SINCSALDOESTOQUE"]);
                        sincpedaccess = Convert.ToDateTime(reader["SINCPEDACCESS"]);
                        buscaformapgto = Convert.ToDateTime(reader["BUSCAFORMAPGTO"]);
                        buscacondpgto = Convert.ToDateTime(reader["BUSCACONDPGTO"]);
                        buscaregioes = Convert.ToDateTime(reader["BUSCAREGIOES"]);
                        buscaempresas = Convert.ToDateTime(reader["BUSCAEMPRESAS"]);
                        buscaprodutos = Convert.ToDateTime(reader["BUSCAPRODUTOS"]);
                        buscatabpreco = Convert.ToDateTime(reader["BUSCATABPRECO"]);
                        buscapedidos = Convert.ToDateTime(reader["BUSCAPEDIDOS"]);
                        buscasaldoestoque = Convert.ToDateTime(reader["BUSCASALDOESTOQUE"]);
                        buscapedaccess = Convert.ToDateTime(reader["BUSCAPEDACCESS"]);
                    }
                }
            }
            return retorno;
        }
        #endregion

        #region Obtem a descrição para os campos
        public String GetDescCampo(String campo)
        {
            String retorno = "";

            if (campo == "CODESTAB")
                retorno = "Código do estabelecimento";
            else if (campo == "ESTABLISHMENTKEY")
                retorno = "Chave do estabelecimento";
            else if (campo == "ULTIMASINCRONIA")
                retorno = "Data da ultima sincronia";
            else if (campo == "ULTIMABUSCA")
                retorno = "Data da ultima busca";

            return retorno;
        }
        #endregion
    }
}
