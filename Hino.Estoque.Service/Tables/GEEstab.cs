﻿using Hino.Estoque.Service.DataBase.Oracle;
using Hino.Estoque.Infra.Cross.Utils.Reader;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Service.Tables
{
    public class GEEstab
    {
        #region Propriedades Públicas
        public short codestab { get; set; }
        public string nomefantasia { get; set; }
        public string razaosocial { get; set; }
        public string tokenbuscacnpj { get; set; }
        #endregion

        #region Propriedades privadas
        private readonly DBOracle _bdCont;
        #endregion

        public GEEstab(DBOracle pbdCont)
        {
            _bdCont = pbdCont;
        }

        #region Select
        public String Select()
        {
            return @"SELECT CODESTAB, GEESTAB.RAZAOSOCIAL, GEESTAB.NOMEFANTASIA, GEESTAB.TOKENBUSCACNPJ 
                       FROM GEESTAB";
        }
        #endregion

        #region Get Establishments
        public List<GEEstab> GetEstablishments()
        {
            var lstGEEstab = new List<GEEstab>();
            String strSql = string.Concat(Select(), " WHERE CODESTAB > 0 ");

            // Cria o objeto command para executar a instruçao sql
            using (OracleCommand cmd = new OracleCommand(strSql, _bdCont.AbreConn()))
            {
                // Define o tipo do comando
                cmd.CommandType = CommandType.Text;
                
                // Executando o comando
                using (var reader = cmd.ExecuteReader())
                {
                    lstGEEstab = reader.Select(r => 
                    new GEEstab(_bdCont)
                    {
                        codestab = Convert.ToInt16(r["CODESTAB"]),
                        razaosocial = r["RAZAOSOCIAL"].ToString(),
                        nomefantasia = r["NOMEFANTASIA"].ToString(),
                        tokenbuscacnpj = r["TOKENBUSCACNPJ"].ToString()
                    }).ToList();
                }
            }

            return lstGEEstab;
        }
        #endregion
    }
}
