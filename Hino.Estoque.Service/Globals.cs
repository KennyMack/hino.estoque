﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Service
{
    public static class Globals
    {
        public static int CodEstab
        {
            set
            {
                Properties.Settings.Default.CodEstab = value;
                Properties.Settings.Default.Save();
            }
            get
            {
                return Properties.Settings.Default.CodEstab;
            }
        }

        public static string ConnStr
        {
            set
            {
                Properties.Settings.Default.ConnStr = value;
                Properties.Settings.Default.Save();
            }
            get
            {
                return Properties.Settings.Default.ConnStr;
            }
        }

        public static string ConnAccessStr
        {
            set
            {
                Properties.Settings.Default.ConnAccessStr = value;
                Properties.Settings.Default.Save();
            }
            get
            {
                return Properties.Settings.Default.ConnAccessStr;
            }
        }

        public static string PathToAccessDataBase
        {
            set
            {
                Properties.Settings.Default.PathToAccessDataBase = value;
                Properties.Settings.Default.Save();
            }
            get
            {
                return Properties.Settings.Default.PathToAccessDataBase;
            }
        }

        public static string PathToHinoConexoes
        {
            set
            {
                Properties.Settings.Default.PathToHinoConexoes = value;
                Properties.Settings.Default.Save();
            }
            get
            {
                return Properties.Settings.Default.PathToHinoConexoes;
            }
        }

        public static string UidAccess
        {
            set
            {
                Properties.Settings.Default.UidAccess = value;
                Properties.Settings.Default.Save();
            }
            get
            {
                return Properties.Settings.Default.UidAccess;
            }
        }

        public static string PwdAccess
        {
            set
            {
                Properties.Settings.Default.PwdAccess = value;
                Properties.Settings.Default.Save();
            }
            get
            {
                return Properties.Settings.Default.PwdAccess;
            }
        }


    }
}
