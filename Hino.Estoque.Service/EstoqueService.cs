﻿using Hino.Estoque.Infra.Cross.Utils.Reader;
using Hino.Estoque.Service.Application;
using Hino.Estoque.Service.DataBase.Access;
using Hino.Estoque.Service.DataBase.Oracle;
using Hino.Estoque.Service.Tables;
using log4net;
using log4net.Config;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Odbc;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Hino.Estoque.Service
{
    public partial class EstoqueService : ServiceBase
    {
        private readonly DepResolver _DepResolver;
        private readonly OracleConnectionStringBuilder ConnStr;
        private readonly OdbcConnectionStringBuilder OdbcStrConn;
        private System.Timers.Timer timer = new System.Timers.Timer();
        private DBOracle _DBOracle;
        private DBAccess _DBAccess;
        private bool ConnectionOK;
        private static readonly ILog Logger = LogManager.GetLogger(typeof(EstoqueService));
        private readonly DataSet _DsLoadConnection;
        private DataTable _DtConnection
        {
            get
            {
                try
                {
                    return _DsLoadConnection.Tables[0];
                }
                catch (Exception)
                {

                }
                return null;
            }
        }

        public EstoqueService(DepResolver pDepResolver)
        {
            InitializeComponent();
            _DepResolver = pDepResolver;
            timer.Interval = 5000;
            timer.Enabled = true;
            ConnectionOK = false;
            XmlConfigurator.Configure();
            _DsLoadConnection = new DataSet();

            var pwd = string.IsNullOrEmpty(Globals.PwdAccess) ? "" : $"Pwd={Globals.PwdAccess};";
            var udi = string.IsNullOrEmpty(Globals.UidAccess) ? "" : $"Pwd={Globals.UidAccess};";

            OdbcStrConn = new OdbcConnectionStringBuilder
            {
                ConnectionString =
                 $"Driver={{Microsoft Access Driver (*.mdb, *.accdb)}};Dbq={Globals.PathToAccessDataBase};{udi}{pwd}"

            };
            ConnStr = new OracleConnectionStringBuilder
            {
                Pooling = true,
                IncrPoolSize = 1,
                MaxPoolSize = 30,
                ConnectionTimeout = 120
            };
        }

        #region Load Data
        private void LoadData()
        {
            try
            {
                _DsLoadConnection.ReadXml(Path.Combine(Globals.PathToHinoConexoes, "Hino.Conexoes.xml"));
            }
            catch (FileNotFoundException)
            {
                ConnectionOK = false;
                Logger.Error("Arquivo 'Hino.Conexoes.xml' não localizado.");
                return;
            }
            catch (Exception)
            {
                ConnectionOK = false;
                Logger.Error($"Não foi possível carregar os dados do banco de dados.");
                Logger.Error($"Local do arquivo: {Path.Combine(Globals.PathToHinoConexoes, "Hino.Conexoes.xml")}.");
                return;
            }

            Utils.cripto cripto = new Utils.cripto();
            try
            {
                var conexao = _DtConnection.Select("Alias = 'SINCRONIA'")
                    .FirstOrDefault();

                if (conexao["DbType"].ToString() != "Oracle")
                {
                    Logger.Error("Este sistema só trabalha com o banco de dados Oracle.");
                    return;
                }
                ConnStr.DataSource = conexao["DbServer"].ToString();
                ConnStr.UserID = cripto.Decrypt(conexao["UserName"].ToString());
                ConnStr.Password = cripto.Decrypt(conexao["Password"].ToString());
                _DBOracle = DBOracle.GetInstance(ConnStr.ConnectionString);

                _DBOracle.AbreConn();
                if (!_DBOracle.ConexaoOk)
                    throw new Exception(_DBOracle.MsgErro)
                    {
                        Source = "BANCO"
                    };

                _DBOracle.FechaConn();
                _DBOracle.FreeInstancia();

                ConnectionOK = true;
                Globals.ConnStr = ConnStr.ConnectionString;
            }
            catch (FileNotFoundException)
            {
                ConnectionOK = false;
                _DBOracle.FechaConn();
                _DBOracle.FreeInstancia();
                Logger.Error("Arquivo 'Hino.Conexoes.xml' não localizado.");
                return;
            }
            catch (Exception ex)
            {
                ConnectionOK = false;
                if (ex.Source == "BANCO")
                    Logger.Error("Problema ao estabelecer conexão com o servidor de banco de dados. Motivo: " + ex.Message);
                else
                    Logger.Error("Não foi possível carregar os dados do banco de dados.");
            }
            finally
            {
                _DBOracle.FechaConn();
                _DBOracle.FreeInstancia();
            }

            Logger.Info("Buscando dados da base de dados Access.");

            if (string.IsNullOrEmpty(Globals.PathToAccessDataBase))
            {
                Logger.Error("Caminho para a base de dados Access não informada.");
                ConnectionOK = false;
                return;
            }

            try
            {
                using (var fl = File.Open(Globals.PathToAccessDataBase, FileMode.Open))
                {

                }
            }
            catch (FileNotFoundException)
            {
                ConnectionOK = false;
                Logger.Error($"Arquivo {Globals.PathToAccessDataBase} não localizado.");
                return;
            }
            catch (Exception ex)
            {
                ConnectionOK = false;
                Logger.Error($"Não foi possível carregar os dados do banco de dados Access. Motivo: {ex.Message}");
                return;
            }
            try
            {
                _DBAccess = DBAccess.GetInstance(OdbcStrConn.ConnectionString);

                _DBAccess.AbreConn();
                if (!_DBAccess.ConexaoOk)
                    throw new Exception(_DBAccess.MsgErro)
                    {
                        Source = "BANCO"
                    };

                _DBAccess.FechaConn();
                _DBAccess.FreeInstancia();

                ConnectionOK = true;
                Globals.ConnAccessStr = OdbcStrConn.ConnectionString;
            }
            catch (FileNotFoundException)
            {
                ConnectionOK = false;
                _DBAccess.FechaConn();
                _DBAccess.FreeInstancia();
                Logger.Error($"Arquivo {Globals.PathToAccessDataBase} não localizado.");
                return;
            }
            catch (Exception ex)
            {
                ConnectionOK = false;
                if (ex.Source == "BANCO")
                    Logger.Error("Problema ao estabelecer conexão com o servidor de banco de dados Access. Motivo: " + ex.Message);
                else
                    Logger.Error("Não foi possível carregar os dados do banco de dados Access.");
            }
            finally
            {
                _DBAccess.FechaConn();
                _DBAccess.FreeInstancia();
            }
        }
        #endregion

        #region Método para iniciar o serviço em modo console
        public void Console(string[] args)
        {
            System.Console.WriteLine("Servico iniciado em modo console.");
            System.Console.WriteLine("Pressione qualquer tecla para sair.");
            this.OnStart(args);
            System.Console.ReadKey();
            this.OnStop();
        }
        #endregion

        protected override void OnStart(string[] args)
        {
            Logger.Info("Servico iniciado.");

            Logger.Info("Realizando conexao com o banco de dados.");
            var retry = 1;
            while (!ConnectionOK)
            {
                try
                {
                    Logger.Info("Tentando realizar conexão.");
                    Logger.Info("Tentativa " + retry + "/20.");

                    LoadData();

                    if (ConnectionOK)
                        Logger.Info("Conexao com o banco de dados realizada com sucesso.");
                    else
                        throw new Exception(_DBOracle.MsgErro);
                }
                catch (Exception ex)
                {
                    Logger.Error($"Nao foi possivel realizar conexao com o banco de dados (verifique a configuracao da conexao). Motivo: {ex.Message}");
                    retry++;
                    Thread.Sleep(10000);
                }

                if (!ConnectionOK && retry > 20)
                    break;
            }

            /*if (ConnectionOK)
                ConnectionOK = await AuthenticateUser();*/

            if (ConnectionOK)
            {
                Logger.Info("Iniciando processos.");

                timer.Elapsed += new System.Timers.ElapsedEventHandler(OnElapsedTime);
                timer.Interval = 30000;
                timer.Enabled = true;
            }
            else
            {
                this.OnStop();
            }
        }

        protected override void OnStop()
        {
            Logger.Info("Servico parado.");
            timer.Stop();
        }

        private async void OnElapsedTime(object source, System.Timers.ElapsedEventArgs e)
        {
            Logger.Info("Processando.");
            timer.Stop();

            var Estab = new GEEstab(_DBOracle);
            Logger.Info("Buscando estabelecimentos.");
            var lstEstabs = Estab.GetEstablishments();

            foreach (var item in lstEstabs)
            {
                Logger.Info($"Percorrendo Estab: {item.codestab} - {item.nomefantasia}.");

                Globals.CodEstab = item.codestab;
                if (Globals.CodEstab == 1)
                {
                    SyncData sync = new SyncData(_DepResolver)
                    {
                        tokenbuscacnpj = item.tokenbuscacnpj
                    };
                    await sync.PushChanges();
                    await sync.Syncronize();
                }
            }

            timer.Start();
        }
    }
}
