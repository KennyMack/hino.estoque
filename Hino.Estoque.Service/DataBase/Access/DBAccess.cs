﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Service.DataBase.Access
{
    public class DBAccess
    {
        public bool ConexaoOk = false;
        public string MsgErro = "";
        private static DBAccess _Instancia = null;
        private string _ConnStr;
        private OdbcConnection _orcConn = null;
        public OdbcTransaction _ocTran = null;

        #region Instancia
        /// <summary>
        /// Instancia a Classe
        /// </summary>
        /// <param name="ConnStr">Connection String</param>
        /// <returns>bdControle</returns>
        public static DBAccess GetInstance(string ConnStr)
        {
            if (_Instancia == null)
                _Instancia = new DBAccess(ConnStr);

            return _Instancia;
        }
        #endregion

        void _OrclConn_StateChange(object sender, System.Data.StateChangeEventArgs e)
        {
            // throw new NotImplementedException();
        }

        #region Free Instancia
        public void FreeInstancia()
        {
            if (_Instancia != null)
                _Instancia = null;
        }
        #endregion

        #region Construtor
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="ConnStr">Connection String</param>
        private DBAccess(string ConnStr)
        {
            this._ConnStr = ConnStr;
        }
        #endregion

        #region Abre a conexão
        public OdbcConnection AbreConn(bool Transaction = false)
        {
            if (this._orcConn == null || 
                string.IsNullOrEmpty(this._orcConn?.ConnectionString) ||
                this._orcConn?.State != ConnectionState.Open)
            {
                this._orcConn = new OdbcConnection(this._ConnStr);
                this._orcConn.StateChange += _OrclConn_StateChange;
            }

            try
            {
                this._orcConn.Open();
                ConexaoOk = true;
                MsgErro = "";
            }
            catch (Exception ex)
            {
                if (this._orcConn.State != ConnectionState.Open)
                {
                    ConexaoOk = false;
                    MsgErro = ex.Message;
                }
            }

            if (this._orcConn.State == ConnectionState.Open)
            {

                if (Transaction)
                    this.IniciaTransacao();
            }

            return this._orcConn;
        }
        #endregion

        #region Fecha a conexão
        public void FechaConn()
        {
            if (this._orcConn != null)
            {
                if (this._orcConn.State != ConnectionState.Closed)
                {
                    this._orcConn.Close();
                    ConexaoOk = false;
                    MsgErro = "";
                }
            }
        }
        #endregion

        #region Inicia a transação
        private void IniciaTransacao()
        {
            if (this._ocTran == null)
                this._ocTran = this._orcConn.BeginTransaction();
        }
        #endregion

        #region Commita transação
        public void CommitTransacao()
        {
            if (this._ocTran != null)
            {
                this._ocTran.Commit();
                this._ocTran = null;
            }
        }
        #endregion

        #region Rollback transação
        public void RollbackTransacao()
        {
            if (this._ocTran != null)
            {
                this._ocTran.Rollback();
                this._ocTran = null;
            }
        }
        #endregion
    }
}
