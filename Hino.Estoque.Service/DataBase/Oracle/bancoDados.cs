﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Service.DataBase.Oracle
{
    public class bancoDados
    {
        #region Propriedades privadas
        private string _connStr;
        private OracleConnection _orclConn;
        #endregion

        #region Construtor
        public bancoDados(string connStr)
        {
            _connStr = connStr;
        }

        public bancoDados(OracleConnection orclConn)
        {
            _orclConn = orclConn;
            _connStr = orclConn.ConnectionString;
        }
        #endregion

        #region ExecuteScalar retornando Object
        public Object ExecScalarRetObject(string strSql)
        {
            Object retorno;

            // Cria a conexão com o banco de dados
            OracleConnection con;

            if (_orclConn == null)
            {
                con = new OracleConnection(_connStr);
                con.Open();
            }
            else
                con = _orclConn;

            // Cria o objeto command para executar a instruçao sql
            using (OracleCommand cmd = new OracleCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                cmd.CommandText = strSql;

                // Executando o comando
                retorno = cmd.ExecuteScalar();

            }

            if (_orclConn == null)
            {
                con.Close();
                con.Dispose();
            }

            return retorno;
        }
        #endregion

        #region ExecuteNonQuery retornando Int32
        public Int32 ExecNonQuery(string strSql)
        {
            Int32 retorno;

            // Cria a conexão com o banco de dados
            OracleConnection con;

            if (_orclConn == null)
            {
                con = new OracleConnection(_connStr);
                con.Open();
            }
            else
                con = _orclConn;

            // Cria o objeto command para executar a instruçao sql
            using (OracleCommand cmd = new OracleCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                cmd.CommandText = strSql;

                // Executando o comando
                retorno = cmd.ExecuteNonQuery();
            }

            // Fecha a conexão
            if (_orclConn == null)
            {
                con.Close();
                con.Dispose();
            }

            return retorno;
        }
        #endregion

        #region ExecuteNonQuery retornando Int32
        public Int32 ExecNonQuery(string strSql, params OracleParameter[] pars)
        {
            Int32 retorno;

            // Cria a conexão com o banco de dados
            OracleConnection con;

            if (_orclConn == null)
            {
                con = new OracleConnection(_connStr);
                con.Open();
            }
            else
                con = _orclConn;

            // Cria o objeto command para executar a instruçao sql
            using (OracleCommand cmd = new OracleCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                cmd.CommandText = strSql;

                // Passagem de parâmetros por nome
                cmd.BindByName = true;

                // Acrescentando os parâmetros
                for (int i = 0; i < pars.Length; i++)
                {
                    cmd.Parameters.Add(pars[i]);
                }

                // Executando o comando
                retorno = cmd.ExecuteNonQuery();
            }

            // Fecha a conexão
            if (_orclConn == null)
            {
                con.Close();
                con.Dispose();
            }

            return retorno;
        }
        #endregion

        #region ExecuteScalar retornando Int16
        public Int16 ExecScalarRetInt16(string strSql)
        {
            Int16 retorno;

            // Cria a conexão com o banco de dados
            OracleConnection con;

            if (_orclConn == null)
            {
                con = new OracleConnection(_connStr);
                con.Open();
            }
            else
                con = _orclConn;

            // Cria o objeto command para executar a instruçao sql
            using (OracleCommand cmd = new OracleCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                cmd.CommandText = strSql;

                // Executando o comando
                retorno = Int16.Parse(cmd.ExecuteScalar().ToString());
            }

            if (_orclConn == null)
            {
                con.Close();
                con.Dispose();
            }

            return retorno;
        }
        #endregion

        #region ExecuteScalar retornando Int16 - Com Parâmetros
        public Int16 ExecScalarRetInt16(string strSql, params OracleParameter[] pars)
        {
            Int16 retorno;

            // Cria a conexão com o banco de dados
            OracleConnection con;

            if (_orclConn == null)
            {
                con = new OracleConnection(_connStr);
                con.Open();
            }
            else
                con = _orclConn;

            // Cria o objeto command para executar a instruçao sql
            using (OracleCommand cmd = new OracleCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                cmd.CommandText = strSql;

                // Passagem de parâmetros por nome
                cmd.BindByName = true;

                // Acrescentando os parâmetros
                for (int i = 0; i < pars.Length; i++)
                {
                    cmd.Parameters.Add(pars[i]);
                }

                // Executando o comando
                retorno = Int16.Parse(cmd.ExecuteScalar().ToString());
            }

            if (_orclConn == null)
            {
                con.Close();
                con.Dispose();
            }

            return retorno;
        }
        #endregion

        #region ExecuteScalar retornando Int32
        public int ExecScalarRetInt(string strSql)
        {
            int retorno;

            // Cria a conexão com o banco de dados
            OracleConnection con;

            if (_orclConn == null)
            {
                con = new OracleConnection(_connStr);
                con.Open();
            }
            else
                con = _orclConn;

            // Cria o objeto command para executar a instruçao sql
            using (OracleCommand cmd = new OracleCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                cmd.CommandText = strSql;

                // Executando o comando
                retorno = int.Parse(cmd.ExecuteScalar().ToString());
            }

            if (_orclConn == null)
            {
                con.Close();
                con.Dispose();
            }

            return retorno;
        }
        #endregion

        #region ExecuteScalar retornando Int32 - com parâmetros
        public int ExecScalarRetInt(string strSql, params OracleParameter[] pars)
        {
            int retorno;

            // Cria a conexão com o banco de dados
            OracleConnection con;

            if (_orclConn == null)
            {
                con = new OracleConnection(_connStr);
                con.Open();
            }
            else
                con = _orclConn;

            // Cria o objeto command para executar a instruçao sql
            using (OracleCommand cmd = new OracleCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                cmd.CommandText = strSql;

                // Passagem de parâmetros por nome
                cmd.BindByName = true;

                // Acrescentando os parâmetros
                for (int i = 0; i < pars.Length; i++)
                {
                    cmd.Parameters.Add(pars[i]);
                }

                // Executando o comando
                retorno = int.Parse(cmd.ExecuteScalar().ToString());
            }

            // Fecha a conexão
            if (_orclConn == null)
            {
                con.Close();
                con.Dispose();
            }

            return retorno;
        }
        #endregion

        #region ExecuteScalar retornando Int64
        public Int64 ExecScalarRetInt64(string strSql)
        {
            Int64 retorno;

            // Cria a conexão com o banco de dados
            OracleConnection con;

            if (_orclConn == null)
            {
                con = new OracleConnection(_connStr);
                con.Open();
            }
            else
                con = _orclConn;

            // Cria o objeto command para executar a instruçao sql
            using (OracleCommand cmd = new OracleCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                cmd.CommandText = strSql;

                // Executando o comando
                retorno = Int64.Parse(cmd.ExecuteScalar().ToString());
            }

            // Fecha a conexão
            if (_orclConn == null)
            {
                con.Close();
                con.Dispose();
            }

            return retorno;
        }
        #endregion

        #region ExecuteScalar retornando Int64 - Com parâmetros
        public Int64 ExecScalarRetInt64(string strSql, params OracleParameter[] pars)
        {
            Int64 retorno;

            // Cria a conexão com o banco de dados
            OracleConnection con;

            if (_orclConn == null)
            {
                con = new OracleConnection(_connStr);
                con.Open();
            }
            else
                con = _orclConn;

            // Cria o objeto command para executar a instruçao sql
            using (OracleCommand cmd = new OracleCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                cmd.CommandText = strSql;

                // Passagem de parâmetros por nome
                cmd.BindByName = true;

                // Acrescentando os parâmetros
                for (int i = 0; i < pars.Length; i++)
                {
                    cmd.Parameters.Add(pars[i]);
                }

                // Executando o comando
                retorno = Int64.Parse(cmd.ExecuteScalar().ToString());
            }


            // Fecha a conexão
            if (_orclConn == null)
            {
                con.Close();
                con.Dispose();
            }

            return retorno;
        }
        #endregion

        #region ExecuteScalar retornando String
        public string ExecScalarRetString(string strSql)
        {
            string retorno;

            // Cria a conexão com o banco de dados
            OracleConnection con;

            if (_orclConn == null)
            {
                con = new OracleConnection(_connStr);
                con.Open();
            }
            else
                con = _orclConn;

            // Cria o objeto command para executar a instruçao sql
            using (OracleCommand cmd = new OracleCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                cmd.CommandText = strSql;

                // Executando o comando
                try
                {
                    retorno = (string)cmd.ExecuteScalar();
                }
                catch
                {
                    retorno = "";
                }
            }

            // Fecha a conexão
            if (_orclConn == null)
            {
                con.Close();
                con.Dispose();
            }

            return retorno;
        }
        #endregion

        #region ExecuteScalar retornando String - Com parâmetros
        public string ExecScalarRetString(string strSql, params OracleParameter[] pars)
        {
            string retorno;

            // Cria a conexão com o banco de dados
            OracleConnection con;

            if (_orclConn == null)
            {
                con = new OracleConnection(_connStr);
                con.Open();
            }
            else
                con = _orclConn;

            // Cria o objeto command para executar a instruçao sql
            using (OracleCommand cmd = new OracleCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                cmd.CommandText = strSql;

                // Passagem de parâmetros por nome
                cmd.BindByName = true;

                // Acrescentando os parâmetros
                for (int i = 0; i < pars.Length; i++)
                {
                    cmd.Parameters.Add(pars[i]);
                }

                // Executando o comando
                try
                {
                    retorno = (string)cmd.ExecuteScalar();
                }
                catch
                {
                    retorno = "";
                }
            }

            // Fecha a conexão
            if (_orclConn == null)
            {
                con.Close();
                con.Dispose();
            }
            return retorno;
        }
        #endregion

        #region ExecuteScalar retornando Decimal
        public Decimal ExecScalarRetDecimal(string strSql)
        {
            Decimal retorno;

            // Cria a conexão com o banco de dados
            OracleConnection con;

            if (_orclConn == null)
            {
                con = new OracleConnection(_connStr);
                con.Open();
            }
            else
                con = _orclConn;

            // Cria o objeto command para executar a instruçao sql
            using (OracleCommand cmd = new OracleCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                cmd.CommandText = strSql;

                // Executando o comando
                retorno = Convert.ToDecimal(cmd.ExecuteScalar());
            }

            // Fecha a conexão
            if (_orclConn == null)
            {
                con.Close();
                con.Dispose();
            }

            return retorno;
        }
        #endregion

        #region ExecuteScalar retornando Decimal - Com parâmetros
        public Decimal ExecScalarRetDecimal(string strSql, params OracleParameter[] pars)
        {
            Decimal retorno;

            // Cria a conexão com o banco de dados
            OracleConnection con;

            if (_orclConn == null)
            {
                con = new OracleConnection(_connStr);
                con.Open();
            }
            else
                con = _orclConn;

            // Cria o objeto command para executar a instruçao sql
            using (OracleCommand cmd = new OracleCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                cmd.CommandText = strSql;

                // Passagem de parâmetros por nome
                cmd.BindByName = true;

                // Acrescentando os parâmetros
                for (int i = 0; i < pars.Length; i++)
                {
                    cmd.Parameters.Add(pars[i]);
                }

                // Executando o comando
                retorno = Decimal.Parse(cmd.ExecuteScalar().ToString());
            }

            // Fecha a conexão
            if (_orclConn == null)
            {
                con.Close();
                con.Dispose();
            }

            return retorno;
        }
        #endregion

        #region ExecuteScalar retornando DateTime
        public DateTime ExecScalarRetDateTime(string strSql)
        {
            DateTime retorno;

            // Cria a conexão com o banco de dados
            OracleConnection con;

            if (_orclConn == null)
            {
                con = new OracleConnection(_connStr);
                con.Open();
            }
            else
                con = _orclConn;

            // Cria o objeto command para executar a instruçao sql
            using (OracleCommand cmd = new OracleCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                cmd.CommandText = strSql;

                // Executando o comando
                retorno = Convert.ToDateTime(cmd.ExecuteScalar());
            }

            // Fecha a conexão
            if (_orclConn == null)
            {
                con.Close();
                con.Dispose();
            }

            return retorno;
        }
        #endregion

        #region ExecuteScalar retornando DateTime - Com parâmetros
        public DateTime ExecScalarRetDateTime(string strSql, params OracleParameter[] pars)
        {
            DateTime retorno;

            // Cria a conexão com o banco de dados
            OracleConnection con;

            if (_orclConn == null)
            {
                con = new OracleConnection(_connStr);
                con.Open();
            }
            else
                con = _orclConn;

            // Cria o objeto command para executar a instruçao sql
            using (OracleCommand cmd = new OracleCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                cmd.CommandText = strSql;

                // Passagem de parâmetros por nome
                cmd.BindByName = true;

                // Acrescentando os parâmetros
                for (int i = 0; i < pars.Length; i++)
                {
                    cmd.Parameters.Add(pars[i]);
                }

                // Executando o comando
                retorno = Convert.ToDateTime(cmd.ExecuteScalar());
            }

            // Fecha a conexão
            if (_orclConn == null)
            {
                con.Close();
                con.Dispose();
            }

            return retorno;
        }
        #endregion

        #region Executa consulta e retorna o datatable
        public DataTable ExecConsRetDataTable(string strSql)
        {
            // Cria a conexão com o banco de dados
            OracleConnection con;

            if (_orclConn == null)
            {
                con = new OracleConnection(_connStr);
                con.Open();
            }
            else
                con = _orclConn;

            // Cria um objeto datatable
            DataTable dt = new DataTable();

            // Cria o objeto command para executar a instruçao sql
            using (OracleCommand cmd = new OracleCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                cmd.CommandText = strSql;

                using (OracleDataAdapter da = new OracleDataAdapter(cmd))
                {
                    // Preenche o datatable via dataadapter
                    da.Fill(dt);
                }
            }

            // Fecha a conexão    
            if (_orclConn == null)
            {
                con.Close();
                con.Dispose();
            }

            return dt;
        }
        #endregion

        #region Executa consulta e retorna o datatable - com lista de parametros 
        public DataTable ExecConsRetDataTable(String strSql, List<OracleParameter> pars)
        {
            // Cria a conexão com o banco de dados
            OracleConnection con;

            if (_orclConn == null)
            {
                con = new OracleConnection(_connStr);
                con.Open();
            }
            else
                con = _orclConn;

            // Cria um objeto datatable
            DataTable dt = new DataTable();

            // Cria o objeto command para executar a instruçao sql
            using (OracleCommand cmd = new OracleCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                cmd.CommandText = strSql;

                // Passagem de parâmetros por nome
                cmd.BindByName = true;

                // Acrescentando os parâmetros
                for (int i = 0; i < pars.Count; i++)
                {
                    cmd.Parameters.Add(pars[i]);
                }

                // Cria um dataadapter
                using (OracleDataAdapter da = new OracleDataAdapter(cmd))
                {
                    // Preenche o datatable via dataadapter
                    da.Fill(dt);
                }
            }

            // Fecha a conexão
            if (_orclConn == null)
            {
                con.Close();
                con.Dispose();
            }

            return dt;
        }
        #endregion

        #region Executa consulta e retorna o datatable - Com parâmetros
        public DataTable ExecConsRetDataTable(String strSql, params OracleParameter[] pars)
        {
            // Cria a conexão com o banco de dados
            OracleConnection con;

            if (_orclConn == null)
            {
                con = new OracleConnection(_connStr);
                con.Open();
            }
            else
                con = _orclConn;

            // Cria um objeto datatable
            DataTable dt = new DataTable();

            // Cria o objeto command para executar a instruçao sql
            using (OracleCommand cmd = new OracleCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                cmd.CommandText = strSql;

                // Passagem de parâmetros por nome
                cmd.BindByName = true;

                // Acrescentando os parâmetros
                for (int i = 0; i < pars.Length; i++)
                {
                    cmd.Parameters.Add(pars[i]);
                }

                // Cria um dataadapter
                using (OracleDataAdapter da = new OracleDataAdapter(cmd))
                {
                    // Preenche o datatable via dataadapter
                    da.Fill(dt);
                }
            }

            // Fecha a conexão            
            if (_orclConn == null)
            {
                con.Close();
                con.Dispose();
            }

            return dt;
        }
        #endregion

        #region Executa consulta e retorna o datatable com conexão
        public DataTable ExecConsRetDataTable(string strSql, OracleConnection _ConnBD)
        {
            // Cria um objeto datatable
            DataTable dt = new DataTable();

            // Cria o objeto command para executar a instruçao sql
            using (OracleCommand cmd = new OracleCommand(strSql, _ConnBD))
            {
                // Define o tipo do comando
                cmd.CommandType = CommandType.Text;

                // Cria um dataadapter
                using (OracleDataAdapter da = new OracleDataAdapter(cmd))
                {
                    // Preenche o datatable via dataadapter
                    da.Fill(dt);
                }
            }

            return dt;
        }
        #endregion

        #region Acrescenta condição ao select - verifica se usa o WHERE ou AND
        /// <summary>
        /// Verifica o select e acescenta a condicão pertinente
        /// WHERE ou AND
        /// * Somente a primeira condição deve ser inibida o WHERE ou o AND
        /// outras condições devem ser adicionadas com AND
        /// </summary>
        /// <param name="select">Select</param>
        /// <param name="condicao">Condição</param>
        /// <returns>String</returns>
        public String AddCondicao(String pSelect, String condicao)
        {
            String clausula;

            string select = "";
            string Group = "";
            string order = "";
            if (condicao != "")
            {

                if (pSelect.LastIndexOf("GROUP BY") > 0)
                {
                    Group = pSelect.Substring(pSelect.LastIndexOf("GROUP BY"), (pSelect.Length - pSelect.LastIndexOf("GROUP BY")));
                    if (Group.IndexOf("WHERE") >= 0)
                    {
                        select = pSelect;
                        Group = "";
                    }
                    else
                        select = pSelect.Remove(pSelect.LastIndexOf("GROUP BY"), (pSelect.Length - pSelect.LastIndexOf("GROUP BY")));
                }
                else if (pSelect.LastIndexOf("ORDER BY") > 0)
                {
                    order = pSelect.Substring(pSelect.LastIndexOf("ORDER BY"), (pSelect.Length - pSelect.LastIndexOf("ORDER BY")));
                    if (Group.IndexOf("WHERE") >= 0)
                    {
                        select = pSelect;
                        order = "";
                    }
                    else
                        select = pSelect.Remove(pSelect.LastIndexOf("ORDER BY"), (pSelect.Length - pSelect.LastIndexOf("ORDER BY")));
                }
                else
                    select = pSelect;

                if (select.IndexOf("WHERE") > 0)
                    clausula = " AND ";
                else
                    clausula = " WHERE ";

                if (condicao.Trim().StartsWith("AND"))
                    return string.Concat(select, ' ', clausula, ' ', condicao.Trim().Substring(3), ' ', Group, ' ', order);

                return string.Concat(select, ' ', clausula, ' ', condicao, ' ', Group, ' ', order);
            }
            else
                return pSelect;
        }
        #endregion

        #region  Acrescenta condição ao select - verifica se usa o WHERE ou AND e adiciona o order by
        /// <summary>
        /// Verifica o select e acescenta a condicão pertinente
        /// WHERE ou AND
        /// e adiciona a Ordenação pelas colunas passadas
        /// * Somente a primeira condição deve ser inibida o WHERE ou o AND
        /// outras condições devem ser adicionadas com AND
        /// 
        /// </summary>
        /// <param name="select">Select</param>
        /// <param name="condicao">Condição</param>
        /// <param name="Orderby">Ordenação das linhas</param>
        /// <returns>String</returns>
        public String AddCondicao(String pSelect, String condicao, String Orderby)
        {
            if (Orderby.IndexOf("ORDER BY") > 0)
                return string.Concat(this.AddCondicao(pSelect, condicao), ' ', Orderby);
            else
                return string.Concat(this.AddCondicao(pSelect, condicao), " ORDER BY ", Orderby);
        }
        #endregion

        #region Select Paginado
        public string SelectPaginado(string pSelect, int pPagina)
        {
            pSelect = pSelect.Substring(6);

            if (pSelect.ToUpper().Substring(0, 9).Trim() == "DISTINCT")
                pSelect = string.Concat(" DISTINCT ABS(CEIL(COUNT(1) OVER (PARTITION BY 1) / 500)) PAGINACAOXNUMXPAGINAS, ", pSelect.Substring(9));
            else
                pSelect = string.Concat(" ABS(CEIL(COUNT(1) OVER (PARTITION BY 1) / 500)) PAGINACAOXNUMXPAGINAS, ", pSelect);



            return string.Format(@"SELECT * 
                                     FROM (SELECT TBDADOS.*, rownum PAGINACAOXROWXFIM
                                              FROM (
                                                    SELECT {1}
                                                    ) TBDADOS
                                             WHERE ROWNUM < (({0} * 500)+ 1)
                                           )
                                    WHERE PAGINACAOXROWXFIM >= ((({0} -1) * 500) + 1)", pPagina, pSelect);
        }
        #endregion

        #region Executa consulta e retorna o DataReader
        public OracleDataReader ExecConsRetDataReader(string strSql, params OracleParameter[] pars)
        {
            // Cria a conexão com o banco de dados
            OracleConnection con;

            if (_orclConn == null)
            {
                con = new OracleConnection(_connStr);
                con.Open();
            }
            else
                con = _orclConn;

            // Cria um objeto DataReader
            OracleDataReader dr;

            // Cria o objeto command para executar a instruçao sql
            using (OracleCommand cmd = new OracleCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                cmd.CommandText = strSql;

                // Passagem de parâmetros por nome
                cmd.BindByName = true;

                // Acrescentando os parâmetros
                for (int i = 0; i < pars.Length; i++)
                {
                    cmd.Parameters.Add(pars[i]);
                }

                // Preenche o datareader via dataadapter
                dr = cmd.ExecuteReader();
            }

            return dr;
        }
        #endregion

        #region Executa consulta e retorna o DataReader
        public OracleDataReader ExecConsRetDataReader(string strSql)
        {
            // Cria a conexão com o banco de dados
            OracleConnection con;

            if (_orclConn == null)
            {
                con = new OracleConnection(_connStr);
                con.Open();
            }
            else
            {
                con = _orclConn;

                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                }
            }

            // Cria um objeto DataReader
            OracleDataReader dr;

            // Cria o objeto command para executar a instruçao sql
            using (OracleCommand cmd = new OracleCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = con;
                cmd.CommandText = strSql;

                // Preenche o datareader via DataReader
                dr = cmd.ExecuteReader();
            }

            return dr;
        }
        #endregion

        #region Executa consulta e retorna o DataReader
        public OracleDataReader ExecConsRetDataReader(string strSql, OracleConnection _ConnBD)
        {
            // Cria um objeto DataReader
            OracleDataReader dr;

            // Cria o objeto command para executar a instruçao sql
            using (OracleCommand cmd = new OracleCommand(strSql, _ConnBD))
            {
                cmd.CommandType = CommandType.Text;

                // Preenche o datareader via DataReader
                dr = cmd.ExecuteReader();
            }

            return dr;
        }
        #endregion

        #region Cria os parâmetros
        public OracleParameter CriaParam(String nome, OracleDbType tipo, object valor)
        {
            OracleParameter op = new OracleParameter(nome, tipo);
            op.Value = valor;
            return op;
        }
        #endregion
    }
}
