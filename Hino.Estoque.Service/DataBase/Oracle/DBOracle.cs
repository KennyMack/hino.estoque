﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;

namespace Hino.Estoque.Service.DataBase.Oracle
{
    public class DBOracle
    {
        public bool ConexaoOk = false;
        public string MsgErro = "";
        private static DBOracle _Instancia = null;
        private string _ConnStr;
        private OracleConnection _orcConn = null;
        public OracleTransaction _ocTran = null;

        #region Instancia
        /// <summary>
        /// Instancia a Classe
        /// </summary>
        /// <param name="ConnStr">Connection String</param>
        /// <returns>bdControle</returns>
        public static DBOracle GetInstance(string ConnStr)
        {
            if (_Instancia == null)
                _Instancia = new DBOracle(ConnStr);

            return _Instancia;
        }
        #endregion

        void _OrclConn_StateChange(object sender, StateChangeEventArgs e)
        {
            // throw new NotImplementedException();
        }

        #region Free Instancia
        public void FreeInstancia()
        {
            if (_Instancia != null)
                _Instancia = null;
        }
        #endregion

        #region Construtor
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="ConnStr">Connection String</param>
        private DBOracle(string ConnStr)
        {
            this._ConnStr = ConnStr;
        }
        #endregion

        #region Abre a conexão
        public OracleConnection AbreConn(bool Transaction = false)
        {
            if (this._orcConn == null)
            {
                this._orcConn = new OracleConnection(this._ConnStr);
                this._orcConn.StateChange += _OrclConn_StateChange;
            }

            try
            {
                this._orcConn.Open();
                ConexaoOk = true;
                MsgErro = "";
            }
            catch (Exception ex)
            {
                if (this._orcConn.State != ConnectionState.Open)
                {
                    ConexaoOk = false;
                    MsgErro = ex.Message;
                }
            }

            if (this._orcConn.State == ConnectionState.Open)
            {

                if (Transaction)
                    this.IniciaTransacao();
            }

            return this._orcConn;
        }
        #endregion

        #region Fecha a conexão
        public void FechaConn()
        {
            if (this._orcConn != null)
            {
                if (this._orcConn.State != ConnectionState.Closed)
                {
                    this._orcConn.Close();
                    ConexaoOk = false;
                    MsgErro = "";
                }
            }
        }
        #endregion

        #region Inicia a transação
        private void IniciaTransacao()
        {
            if (this._ocTran == null)
                this._ocTran = this._orcConn.BeginTransaction();
        }
        #endregion

        #region Commita transação
        public void CommitTransacao()
        {
            if (this._ocTran != null)
            {
                this._ocTran.Commit();
                this._ocTran = null;
            }
        }
        #endregion

        #region Rollback transação
        public void RollbackTransacao()
        {
            if (this._ocTran != null)
            {
                this._ocTran.Rollback();
                this._ocTran = null;
            }
        }
        #endregion
    }
}
