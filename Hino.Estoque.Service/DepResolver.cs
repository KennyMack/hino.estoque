﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Service
{
    public class DepResolver
    {
        protected IServiceProvider _serviceProvider;

        public DepResolver(IServiceProvider serviceProvider)
        {
            this._serviceProvider = serviceProvider;
        }        

        public T GetService<T>(Type serviceType)
        {
            return (T)this._serviceProvider.GetService(serviceType);
        }

        public object GetServices(Type serviceType)
        {
            return this._serviceProvider.GetService(serviceType);
        }
    }
}
