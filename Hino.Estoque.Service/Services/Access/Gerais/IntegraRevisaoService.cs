﻿using Hino.Estoque.Aplication.Interfaces.Fiscal;
using Hino.Estoque.Aplication.Interfaces.Gerais;
using Hino.Estoque.Infra.Cross.Entities.Fiscal;
using Hino.Estoque.Infra.Cross.Entities.Gerais;
using Hino.Estoque.Infra.Cross.Utils.Extensions;
using Hino.Estoque.Infra.Cross.Utils.Reader;
using Hino.Estoque.Service.DataBase.Access;
using Hino.Estoque.Service.DataBase.Oracle;
using Hino.Estoque.Service.Entities;
using Hino.Estoque.Service.Entities.Access.General;
using Hino.Estoque.Service.Repositories.General;
using Hino.Estoque.Service.Repositories.Interfaces.General;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Service.Services.Access.Gerais
{
    public class IntegraRevisaoService
    {
        private readonly DBOracle _DBOracle;
        private readonly IRevisaoRepository _IRevisaoRepository;
        private readonly ICZIntegracaoRevisaoAS _ICZIntegracaoRevisaoAS;
        private readonly DBAccess _DBAccess;

        public IntegraRevisaoService(
            DBOracle dBOracle,
            DBAccess dBAccess,
            ICZIntegracaoRevisaoAS pICZIntegracaoRevisaoAS)
        {
            _DBOracle = dBOracle;
            _DBAccess = dBAccess;
            _ICZIntegracaoRevisaoAS = pICZIntegracaoRevisaoAS;
            _IRevisaoRepository = new RevisaoRepository(_DBAccess);
        }

        private async Task<IEnumerable<TBLRevisao>> GetPushNewAsync() =>
            (await _IRevisaoRepository.GetAllAsync());

        private async Task SaveToIntegrateAsync(IEnumerable<TBLRevisao> pRevisao)
        {
            foreach (var item in pRevisao)
            {
                var Revisao = new CZIntegracaoRevisao();

                Revisao.CopyProperties(item);
                await _ICZIntegracaoRevisaoAS.CreateAsync(Revisao);
            }
        }

        public async Task PushNewDrawsAsync()
        {
            var Products = await GetPushNewAsync();

            await SaveToIntegrateAsync(Products);
        }

        public async Task<IEnumerable<CZIntegracaoRevisao>> GetDispIntegAsync() =>
            await _ICZIntegracaoRevisaoAS.GetDispIntegAsync();
        

        public async Task AtualizaDesenhoRev(OracleConnection pConn)
        {
            using (OracleCommand cmd = new OracleCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = pConn;
                cmd.CommandText = "PCKG_CUSTOM_HOLEC.ATUALIZADESENHOREV";

                await cmd.ExecuteNonQueryAsync();
                
            }
        }
    }
}
