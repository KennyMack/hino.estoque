﻿using Hino.Estoque.Aplication.Interfaces.Gerais;
using Hino.Estoque.Aplication.ViewModels.Fiscal;
using Hino.Estoque.Infra.Cross.Entities.Gerais;
using Hino.Estoque.Infra.Cross.Utils.Extensions;
using Hino.Estoque.Service.DataBase.Access;
using Hino.Estoque.Service.DataBase.Oracle;
using Hino.Estoque.Service.Entities.Access.General;
using Hino.Estoque.Service.Repositories.Interfaces.General;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Service.Services.Access.Gerais
{
    public class IntegraEmpresaService
    {
        private readonly DBOracle _DBOracle;
        private readonly IEmpresaRepository _IEmpresaRepository;
        private readonly ICZIntegrEmpresaAS _ICZIntegrEmpresaAS;
        private readonly DBAccess _DBAccess;
        public IntegraEmpresaService(
            DBOracle dBOracle,
            DBAccess dBAccess,
            ICZIntegrEmpresaAS pICZIntegrEmpresaAS)
        {
            _DBOracle = dBOracle;
            _DBAccess = dBAccess;
            _ICZIntegrEmpresaAS = pICZIntegrEmpresaAS;
            _IEmpresaRepository = new EmpresaRepository(_DBAccess);
        }

        private async Task<IEnumerable<TBLEmpresa>> GetPushNewAsync() =>
            (await _IEmpresaRepository.GetAllAsync()).Where(r => !r.Apagar);

        private async Task SaveToIntegrateAsync(IEnumerable<TBLEmpresa> pEnterprises)
        {
            foreach (var item in pEnterprises)
            {
                var Enterprise = new CZIntegrEmpresa();

                Enterprise.CopyProperties(item);
                await _ICZIntegrEmpresaAS.CreateAsync(Enterprise);
            }
        }

        private async Task CheckIntegratedAccessAsync(IEnumerable<TBLEmpresa> pEnterprises)
        {
            foreach (var item in pEnterprises)
            {
                var ItemAccess = (await _IEmpresaRepository.QueryAsync(r => r.ID == item.ID)).FirstOrDefault();

                if (ItemAccess != null)
                {
                    ItemAccess.Apagar = true;
                    await _IEmpresaRepository.UpdateIntegratedAsync(ItemAccess);
                }
            }
        }

        public async Task PushNewOrdersAsync()
        {
            var Orders = await GetPushNewAsync();

            await SaveToIntegrateAsync(Orders);

            await CheckIntegratedAccessAsync(Orders);
        }

        public async Task<IEnumerable<CZIntegrEmpresa>> GetDispIntegAsync() =>
            await _ICZIntegrEmpresaAS.GetDispIntegAsync();

        public async Task UpdateCodEmpresaAsync(OracleConnection pConn, CZIntegrEmpresa pIntegrEmpresa)
        {
            using (OracleCommand cmd = new OracleCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = pConn;
                cmd.CommandText = @"update CZINTEGREMPRESA
                                       SET CODEMPRESA  = :pCODEMPRESA
                                    WHERE (IDINTEGRACAO = :pIDINTEGRACAO)";

                cmd.Parameters.Add("pCODEMPRESA", OracleDbType.Int64).Value = pIntegrEmpresa.CodEmpresa;
                cmd.Parameters["pCODEMPRESA"].Direction = ParameterDirection.Input;

                cmd.Parameters.Add("pIDINTEGRACAO", OracleDbType.Int64).Value = pIntegrEmpresa.IdIntegracao;
                cmd.Parameters["pIDINTEGRACAO"].Direction = ParameterDirection.Input;

                await cmd.ExecuteNonQueryAsync();
            }
        }

        public async Task UpdateIntegratedAsync(OracleConnection pConn, CZIntegrEmpresa pIntegrEmpresa)
        {
            using (OracleCommand cmd = new OracleCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = pConn;
                cmd.CommandText = @"update CZINTEGREMPRESA
                                       SET CODEMPRESA  = :pCODEMPRESA, 
                                           DATAINTEGRA  = :pDATAINTEGRA,
                                           EMPINTEGRA = :pEMPINTEGRA
                                    WHERE (IDINTEGRACAO = :pIDINTEGRACAO)";

                cmd.Parameters.Add("pCODEMPRESA", OracleDbType.Int64).Value = pIntegrEmpresa.CodEmpresa;
                cmd.Parameters["pCODEMPRESA"].Direction = ParameterDirection.Input;

                cmd.Parameters.Add("pDATAINTEGRA", OracleDbType.Date).Value = pIntegrEmpresa.DataIntegra;
                cmd.Parameters["pDATAINTEGRA"].Direction = ParameterDirection.Input;

                cmd.Parameters.Add("pEMPINTEGRA", OracleDbType.Int32).Value = pIntegrEmpresa.EmpIntegra ? 1 : 0;
                cmd.Parameters["pEMPINTEGRA"].Direction = ParameterDirection.Input;

                cmd.Parameters.Add("pIDINTEGRACAO", OracleDbType.Int64).Value = pIntegrEmpresa.IdIntegracao;
                cmd.Parameters["pIDINTEGRACAO"].Direction = ParameterDirection.Input;

                await cmd.ExecuteNonQueryAsync();
            }
        }

        public async Task<CZIntegrEmpresa> GenerateEnterpriseCabecAsync(OracleConnection pConn, CZIntegrEmpresa pIntegrEmpresa)
        {
            using (OracleCommand cmd = new OracleCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = pConn;
                cmd.CommandText = "PCKG_CUSTOM_HOLEC.INTEGRACABECEMPRESA";

                cmd.Parameters.Add("pRETURN", OracleDbType.Decimal);
                cmd.Parameters["pRETURN"].Direction = ParameterDirection.ReturnValue;

                cmd.Parameters.Add("pCODESTAB", OracleDbType.Int32).Value = Globals.CodEstab;
                cmd.Parameters["pCODESTAB"].Direction = ParameterDirection.Input;

                cmd.Parameters.Add("pIDINTEGRACAO", OracleDbType.Int64).Value = pIntegrEmpresa.IdIntegracao;
                cmd.Parameters["pIDINTEGRACAO"].Direction = ParameterDirection.Input;

                cmd.Parameters.Add("pCODEMPRESA", OracleDbType.Decimal);
                cmd.Parameters["pCODEMPRESA"].Direction = ParameterDirection.Output;

                cmd.Parameters.Add("pMSERRO", OracleDbType.Clob);
                cmd.Parameters["pMSERRO"].Direction = ParameterDirection.Output;

                await cmd.ExecuteNonQueryAsync();

                if (((OracleDecimal)(cmd.Parameters["pRETURN"].Value)).Value == 1)
                    throw new Exception(((OracleClob)(cmd.Parameters["pMSERRO"].Value)).Value);

                pIntegrEmpresa.CodEmpresa = Convert.ToInt64(((OracleDecimal)(cmd.Parameters["pCODEMPRESA"].Value)).Value);

            }


            return pIntegrEmpresa;
        }

        public async Task<CZIntegrEmpresa> GenerateEnterpriseAddressAsync(OracleConnection pConn, CZIntegrEmpresa pIntegrEmpresa)
        {
            using (OracleCommand cmd = new OracleCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = pConn;
                cmd.CommandText = "PCKG_CUSTOM_HOLEC.INTEGRAENDERECOEMPRESA";

                cmd.Parameters.Add("pRETURN", OracleDbType.Decimal);
                cmd.Parameters["pRETURN"].Direction = ParameterDirection.ReturnValue;

                cmd.Parameters.Add("pCODESTAB", OracleDbType.Int32).Value = Globals.CodEstab;
                cmd.Parameters["pCODESTAB"].Direction = ParameterDirection.Input;

                cmd.Parameters.Add("pIDINTEGRACAO", OracleDbType.Int64).Value = pIntegrEmpresa.IdIntegracao;
                cmd.Parameters["pIDINTEGRACAO"].Direction = ParameterDirection.Input;

                cmd.Parameters.Add("pCODEMPRESA", OracleDbType.Int64).Value = pIntegrEmpresa.CodEmpresa;
                cmd.Parameters["pCODEMPRESA"].Direction = ParameterDirection.Input;

                cmd.Parameters.Add("pMSERRO", OracleDbType.Clob);
                cmd.Parameters["pMSERRO"].Direction = ParameterDirection.Output;

                await cmd.ExecuteNonQueryAsync();

                if (((OracleDecimal)(cmd.Parameters["pRETURN"].Value)).Value == 1)
                    throw new Exception(((OracleClob)(cmd.Parameters["pMSERRO"].Value)).Value);
            }


            return pIntegrEmpresa;
        }

        public async Task GenerateEnterpriseWohner()
        {
            using (OracleCommand cmd = new OracleCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = _DBOracle.AbreConn();
                cmd.CommandText = "PCKG_CUSTOM_HOLEC.GERAREMPRESAWOHNER";

                await cmd.ExecuteNonQueryAsync();
            }
        }
    }
}
