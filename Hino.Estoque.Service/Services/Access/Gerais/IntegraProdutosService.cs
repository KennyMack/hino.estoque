﻿using Hino.Estoque.Aplication.Interfaces.Fiscal;
using Hino.Estoque.Infra.Cross.Entities.Fiscal;
using Hino.Estoque.Infra.Cross.Utils.Extensions;
using Hino.Estoque.Service.DataBase.Access;
using Hino.Estoque.Service.DataBase.Oracle;
using Hino.Estoque.Service.Entities.Access.General;
using Hino.Estoque.Service.Repositories.Interfaces.General;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Service.Services.Access.Gerais
{
    public class IntegraProdutosService
    {
        private readonly DBOracle _DBOracle;
        private readonly IProdutosRepository _IProdutosRepository;
        private readonly IProdutoDetRepository _IProdutoDetRepository;
        private readonly ICZIntegProdutosAS _ICZIntegProdutosAS;
        private readonly ICZIntegrProdDetAS _ICZIntegrProdDetAS;
        private readonly DBAccess _DBAccess;

        public IntegraProdutosService(
            DBOracle dBOracle,
            DBAccess dBAccess,
            ICZIntegProdutosAS pICZIntegProdutosAS,
            ICZIntegrProdDetAS pICZIntegrProdDetAS)
        {
            _DBOracle = dBOracle;
            _DBAccess = dBAccess;
            _ICZIntegProdutosAS = pICZIntegProdutosAS;
            _ICZIntegrProdDetAS = pICZIntegrProdDetAS;
            _IProdutoDetRepository = new ProdutoDetRepository(_DBAccess);
            _IProdutosRepository = new ProdutosRepository(_DBAccess);
        }

        private async Task<IEnumerable<TBLProdutos>> GetPushNewAsync() =>
            (await _IProdutosRepository.GetAllAsync()).Where(r => !r.Apagar);
        
        private async Task<IEnumerable<TBLProdutoDet>> GetPushProductDetailAsync(string[] pProdutos) =>
            (await _IProdutoDetRepository.GetAllAsync()).Where(r => pProdutos.Contains(r.PartNumber));

        private async Task SaveToIntegrateAsync(IEnumerable<TBLProdutos> pProducts)
        {
            foreach (var item in pProducts)
            {
                var Product = new CZIntegProdutos();

                Product.CopyProperties(item);
                await _ICZIntegProdutosAS.CreateAsync(Product);
            }
        }

        private async Task SaveToProdDetailIntegrateAsync(IEnumerable<TBLProdutoDet> pProductsDet)
        {
            foreach (var item in pProductsDet)
            {
                var ProductDB = (await _ICZIntegProdutosAS.QueryAsync(r => r.PartNumber == item.PartNumber)).FirstOrDefault();

                if (ProductDB != null)
                {
                    var ProductDet = new CZIntegrProdDet();

                    ProductDet.CopyProperties(item);
                    ProductDet.IdIntegracao = ProductDB.IdIntegracao;

                    await _ICZIntegrProdDetAS.CreateAsync(ProductDet);
                }
            }
        }

        private async Task CheckIntegratedAccessAsync(IEnumerable<TBLProdutos> pEnterprises)
        {
            foreach (var item in pEnterprises)
            {
                var ItemAccess = (await _IProdutosRepository.QueryAsync(r => r.PartNumber == item.PartNumber)).FirstOrDefault();

                if (ItemAccess != null)
                {
                    ItemAccess.Apagar = true;
                    await _IProdutosRepository.UpdateIntegratedAsync(ItemAccess);
                }
            }
        }

        public async Task PushNewProductsAsync()
        {
            var Products = await GetPushNewAsync();

            await SaveToIntegrateAsync(Products);

            await CheckIntegratedAccessAsync(Products);

            var ProductsDetails = await GetPushProductDetailAsync(Products.Select(r=> r.PartNumber).ToArray());

            await SaveToProdDetailIntegrateAsync(ProductsDetails);
        }

        public async Task<IEnumerable<CZIntegProdutos>> GetDispIntegAsync() =>
            await _ICZIntegProdutosAS.GetDispIntegAsync();
        
        public async Task UpdateIntegratedAsync(OracleConnection pConn, CZIntegProdutos pIntegProdutos)
        {
            using (OracleCommand cmd = new OracleCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = pConn;
                cmd.CommandText = @"update CZINTEGPRODUTOS
                                       SET DATAINTEGRA  = :pDATAINTEGRA,
                                           PRODINTEGRA = :pPRODINTEGRA
                                    WHERE (IDINTEGRACAO = :pIDINTEGRACAO)";

                cmd.Parameters.Add("pDATAINTEGRA", OracleDbType.Date).Value = pIntegProdutos.DataIntegra;
                cmd.Parameters["pDATAINTEGRA"].Direction = ParameterDirection.Input;

                cmd.Parameters.Add("pEMPINTEGRA", OracleDbType.Int32).Value = pIntegProdutos.ProdIntegra ? 1 : 0;
                cmd.Parameters["pEMPINTEGRA"].Direction = ParameterDirection.Input;

                cmd.Parameters.Add("pIDINTEGRACAO", OracleDbType.Int64).Value = pIntegProdutos.IdIntegracao;
                cmd.Parameters["pIDINTEGRACAO"].Direction = ParameterDirection.Input;

                await cmd.ExecuteNonQueryAsync();
            }
        }

        public async Task UpdateCodWohnerAsync(OracleConnection pConn, CZIntegProdutos pIntegProdutos)
        {
            using (OracleCommand cmd = new OracleCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = pConn;
                cmd.CommandText = @"UPDATE PEPRODUTO
                                       SET PEPRODUTO.CODIGOANTIGO = :pCODIGOANTIGO
                                     WHERE PEPRODUTO.CODPRODUTO = :pCODPRODUTO";

                cmd.Parameters.Add("pCODIGOANTIGO", OracleDbType.Varchar2).Value = pIntegProdutos.CodWohner;
                cmd.Parameters["pCODIGOANTIGO"].Direction = ParameterDirection.Input;

                cmd.Parameters.Add("pCODPRODUTO", OracleDbType.Varchar2).Value = pIntegProdutos.PartNumber;
                cmd.Parameters["pCODPRODUTO"].Direction = ParameterDirection.Input;

                await cmd.ExecuteNonQueryAsync();
            }
        }

        public async Task<CZIntegProdutos> GenerateProductAsync(OracleConnection pConn, CZIntegProdutos pIntegProdutos)
        {
            using (OracleCommand cmd = new OracleCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = pConn;
                cmd.CommandText = "PCKG_CUSTOM_HOLEC.INTEGRAPRODUTO";

                cmd.Parameters.Add("pRETURN", OracleDbType.Decimal);
                cmd.Parameters["pRETURN"].Direction = ParameterDirection.ReturnValue;

                cmd.Parameters.Add("pCODESTAB", OracleDbType.Int32).Value = Globals.CodEstab;
                cmd.Parameters["pCODESTAB"].Direction = ParameterDirection.Input;

                cmd.Parameters.Add("pIDINTEGRACAO", OracleDbType.Int64).Value = pIntegProdutos.IdIntegracao;
                cmd.Parameters["pIDINTEGRACAO"].Direction = ParameterDirection.Input;

                cmd.Parameters.Add("pMSERRO", OracleDbType.Clob);
                cmd.Parameters["pMSERRO"].Direction = ParameterDirection.Output;

                await cmd.ExecuteNonQueryAsync();

                if (((OracleDecimal)(cmd.Parameters["pRETURN"].Value)).Value == 1)
                    throw new Exception(((OracleClob)(cmd.Parameters["pMSERRO"].Value)).Value);

            }


            return pIntegProdutos;
        }

        public async Task GenerateProductWohner()
        {
            using (OracleCommand cmd = new OracleCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = _DBOracle.AbreConn();
                cmd.CommandText = "PCKG_CUSTOM_HOLEC.GERARPRODUTOWOHNER";

                await cmd.ExecuteNonQueryAsync();

            }
        }
    }
}
