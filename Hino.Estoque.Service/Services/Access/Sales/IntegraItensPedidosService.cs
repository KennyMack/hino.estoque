﻿using Hino.Estoque.Aplication.Interfaces.Vendas;
using Hino.Estoque.Aplication.Services.Vendas;
using Hino.Estoque.Infra.Cross.Entities.Vendas;
using Hino.Estoque.Infra.Cross.Utils.Extensions;
using Hino.Estoque.Infra.Data.DataBase.Context;
using Hino.Estoque.Service.DataBase.Access;
using Hino.Estoque.Service.DataBase.Oracle;
using Hino.Estoque.Service.Entities;
using Hino.Estoque.Service.Entities.Access.Sales;
using Hino.Estoque.Service.Repositories.Interfaces.Sales;
using Hino.Estoque.Service.Repositories.Sales;
using log4net.Core;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Service.Services.Access.Sales
{
    public class IntegraItensPedidosService
    {
        private readonly DBOracle _DBOracle;
        private readonly IItensRepository _IItensRepository;
        private readonly ICZIntegPedItensAS _ICZIntegPedItensAS;
        private readonly DBAccess _DBAccess;
        public IntegraItensPedidosService(
            DBOracle dBOracle,
            DBAccess dBAccess,
            ICZIntegPedItensAS pICZIntegPedItensAS)
        {
            _DBOracle = dBOracle;
            _DBAccess = dBAccess;
            _ICZIntegPedItensAS = pICZIntegPedItensAS;
            _IItensRepository = new ItensRepository(_DBAccess);

        }

        private async Task<IEnumerable<TBLItens>> GetPushNewAsync(string[] pPedidos ) =>
            (await _IItensRepository.GetAllAsync()).Where(r => pPedidos.Contains(r.Pedido));

        private async Task SaveToIntegrateAsync(vePedReferencia[] pReferencia, IEnumerable<TBLItens> pOrderItems)
        {
            foreach (var item in pOrderItems.OrderBy(r => r.Codigo))
            {
                var Pedido = new CZIntegPedItens();
                var IdIntegracao = pReferencia.FirstOrDefault(r => r.Pedido == item.Pedido).IdIntegracao;
                Pedido.CopyProperties(item);
                Pedido.DescPed = item.Desc;
                Pedido.ExtraInfo = item.Extra;
                if (Pedido.Descricao.Length > 500)
                    Pedido.Descricao = Pedido.Descricao.Substring(0, 500);
                await _ICZIntegPedItensAS.CreateAsync(IdIntegracao, Pedido);
            }
        }

        /*private async Task CheckIntegratedAccessAsync(IEnumerable<TBLItens> pOrders)
        {
            foreach (var item in pOrders)
            {
                var ItemAccess = (await _IItensRepository.QueryAsync(r => r.OrderIDItem == item.OrderIDItem)).FirstOrDefault();

                if (ItemAccess != null)
                {
                    ItemAccess.Integrado = true;
                    await _IItensRepository.UpdateIntegratedAsync(ItemAccess);
                }
            }
        }*/

        public async Task PushNewItemsOrdersAsync(vePedReferencia[] pPedidos)
        {
            var Orders = await GetPushNewAsync(pPedidos.Select(r => r.Pedido).ToArray());

            await SaveToIntegrateAsync(pPedidos, Orders);

            // await CheckIntegratedAccessAsync(Orders);
        }
    }
}
