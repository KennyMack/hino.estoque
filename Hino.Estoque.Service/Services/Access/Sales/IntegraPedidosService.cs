﻿using Hino.Estoque.Aplication.Interfaces.Vendas;
using Hino.Estoque.Aplication.Services.Vendas;
using Hino.Estoque.Infra.Cross.Entities.Vendas;
using Hino.Estoque.Infra.Cross.Utils.Extensions;
using Hino.Estoque.Infra.Data.DataBase.Context;
using Hino.Estoque.Service.DataBase.Access;
using Hino.Estoque.Service.DataBase.Oracle;
using Hino.Estoque.Service.Entities.Access.Sales;
using Hino.Estoque.Service.Repositories.Interfaces.Sales;
using Hino.Estoque.Service.Repositories.Sales;
using log4net.Core;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Service.Services.Access.Sales
{
    public class IntegraPedidosService
    {
        private readonly DBOracle _DBOracle;
        private readonly IPedidosRepository _IPedidosRepository;
        private readonly ICZIntegraPedidosAS _ICZIntegraPedidosAS;
        private readonly DBAccess _DBAccess;
        public IntegraPedidosService(
            DBOracle dBOracle,
            DBAccess dBAccess,
            ICZIntegraPedidosAS pICZIntegraPedidosAS)
        {
            _DBOracle = dBOracle;
            _DBAccess = dBAccess;
            _ICZIntegraPedidosAS = pICZIntegraPedidosAS;
            _IPedidosRepository = new PedidosRepository(_DBAccess);
            
        }

        private async Task<IEnumerable<TBLPedidos>> GetPushNewAsync() => 
            (await _IPedidosRepository.GetAllAsync()).Where(r => !r.Integrado);

        private async Task SaveToIntegrateAsync(IEnumerable<TBLPedidos> pOrders)
        {
            foreach (var item in pOrders)
            {
                var Pedido = new CZIntegraPedidos();

                Pedido.CopyProperties(item);
                await _ICZIntegraPedidosAS.CreateAsync(Pedido);
            }
        }

        private async Task CheckIntegratedAccessAsync(IEnumerable<TBLPedidos> pOrders)
        {
            foreach (var item in pOrders)
            {
                var ItemAccess = (await _IPedidosRepository.QueryAsync(r => r.OrderID == item.OrderID)).FirstOrDefault();

                if (ItemAccess != null)
                {
                    ItemAccess.Integrado = true;
                    await _IPedidosRepository.UpdateIntegratedAsync(ItemAccess);
                }
            }
        }

        public async Task PushNewOrdersAsync()
        {
            var Orders = await GetPushNewAsync();

            await SaveToIntegrateAsync(Orders);

            await CheckIntegratedAccessAsync(Orders);
        }

        public async Task<IEnumerable<CZIntegraPedidos>> GetDispIntegAsync() =>
            await _ICZIntegraPedidosAS.GetDispIntegAsync();

        public async Task UpdateCodPedVendaAsync(CZIntegraPedidos pIntegraPedidos)
        {
            using (OracleCommand cmd = new OracleCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = _DBOracle.AbreConn();
                cmd.CommandText = @"update CZINTEGRAPEDIDOS
                                       SET CODPEDVENDA  = :pCODPEDVENDA
                                    WHERE (IDINTEGRACAO = :pIDINTEGRACAO)";

                cmd.Parameters.Add("pCODPEDVENDA", OracleDbType.Int64).Value = pIntegraPedidos.CodPedVenda;
                cmd.Parameters["pCODPEDVENDA"].Direction = ParameterDirection.Input;

                cmd.Parameters.Add("pIDINTEGRACAO", OracleDbType.Int64).Value = pIntegraPedidos.IdIntegracao;
                cmd.Parameters["pIDINTEGRACAO"].Direction = ParameterDirection.Input;

                await cmd.ExecuteNonQueryAsync();
            }
        }

        public async Task UpdateIntegratedAsync(CZIntegraPedidos pIntegraPedidos)
        {
            using (OracleCommand cmd = new OracleCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = _DBOracle.AbreConn();
                cmd.CommandText = @"update CZINTEGRAPEDIDOS
                                       SET CODPEDVENDA  = :pCODPEDVENDA, 
                                           DATAINTEGRA  = :pDATAINTEGRA,
                                           PEDINTEGRADO = :pPEDINTEGRADO
                                    WHERE (IDINTEGRACAO = :pIDINTEGRACAO)";

                cmd.Parameters.Add("pCODPEDVENDA", OracleDbType.Int64).Value = pIntegraPedidos.CodPedVenda;
                cmd.Parameters["pCODPEDVENDA"].Direction = ParameterDirection.Input;

                cmd.Parameters.Add("pDATAINTEGRA", OracleDbType.Date).Value = pIntegraPedidos.DataIntegra;
                cmd.Parameters["pDATAINTEGRA"].Direction = ParameterDirection.Input;

                cmd.Parameters.Add("pPEDINTEGRADO", OracleDbType.Int32).Value = pIntegraPedidos.PedIntegrado ? 1 : 0;
                cmd.Parameters["pPEDINTEGRADO"].Direction = ParameterDirection.Input;

                cmd.Parameters.Add("pIDINTEGRACAO", OracleDbType.Int64).Value = pIntegraPedidos.IdIntegracao;
                cmd.Parameters["pIDINTEGRACAO"].Direction = ParameterDirection.Input;

                await cmd.ExecuteNonQueryAsync();
            }
        }

        public async Task<CZIntegraPedidos> GenerateOrderCabecAsync(OracleConnection pConn, CZIntegraPedidos pIntegraPedidos)
        {
            using (OracleCommand cmd = new OracleCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = pConn;
                cmd.CommandText = "PCKG_CUSTOM_HOLEC.INTEGRACABECPEDIDO";

                cmd.Parameters.Add("pRETURN", OracleDbType.Decimal);
                cmd.Parameters["pRETURN"].Direction = ParameterDirection.ReturnValue;

                cmd.Parameters.Add("pCODESTAB", OracleDbType.Int32).Value = Globals.CodEstab;
                cmd.Parameters["pCODESTAB"].Direction = ParameterDirection.Input;

                cmd.Parameters.Add("pCODESTAB", OracleDbType.Int64).Value = pIntegraPedidos.IdIntegracao;
                cmd.Parameters["pCODESTAB"].Direction = ParameterDirection.Input;

                cmd.Parameters.Add("pCODPEDVENDA", OracleDbType.Decimal);
                cmd.Parameters["pCODPEDVENDA"].Direction = ParameterDirection.Output;

                cmd.Parameters.Add("pMSERRO", OracleDbType.Clob);
                cmd.Parameters["pMSERRO"].Direction = ParameterDirection.Output;

                await cmd.ExecuteNonQueryAsync();

                if (((OracleDecimal)(cmd.Parameters["pRETURN"].Value)).Value == 1)
                    throw new Exception(((OracleClob)(cmd.Parameters["pMSERRO"].Value)).Value);

                pIntegraPedidos.CodPedVenda = Convert.ToInt64(((OracleDecimal)(cmd.Parameters["pCODPEDVENDA"].Value)).Value);
            }

            return pIntegraPedidos;
        }

        public async Task<CZIntegraPedidos> CancelOrderAsync(OracleConnection pConn, CZIntegraPedidos pIntegraPedidos)
        {
            using (OracleCommand cmd = new OracleCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = pConn;
                cmd.CommandText = "PCKG_CUSTOM_HOLEC.CANCELARPEDIDO";

                cmd.Parameters.Add("pRETURN", OracleDbType.Decimal);
                cmd.Parameters["pRETURN"].Direction = ParameterDirection.ReturnValue;

                cmd.Parameters.Add("pCODESTAB", OracleDbType.Int32).Value = Globals.CodEstab;
                cmd.Parameters["pCODESTAB"].Direction = ParameterDirection.Input;

                cmd.Parameters.Add("pCODESTAB", OracleDbType.Int64).Value = pIntegraPedidos.IdIntegracao;
                cmd.Parameters["pCODESTAB"].Direction = ParameterDirection.Input;

                cmd.Parameters.Add("pCODPEDVENDA", OracleDbType.Decimal);
                cmd.Parameters["pCODPEDVENDA"].Direction = ParameterDirection.Output;

                cmd.Parameters.Add("pMSERRO", OracleDbType.Clob);
                cmd.Parameters["pMSERRO"].Direction = ParameterDirection.Output;

                await cmd.ExecuteNonQueryAsync();

                if (((OracleDecimal)(cmd.Parameters["pRETURN"].Value)).Value == 1)
                    throw new Exception(((OracleClob)(cmd.Parameters["pMSERRO"].Value)).Value);

                pIntegraPedidos.CodPedVenda = Convert.ToInt64(((OracleDecimal)(cmd.Parameters["pCODPEDVENDA"].Value)).Value);

            }

            return pIntegraPedidos;
        }

        public async Task<CZIntegraPedidos> GenerateOrderItemsAsync(OracleConnection pConn, CZIntegraPedidos pIntegraPedidos)
        {
            using (OracleCommand cmd = new OracleCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = pConn;
                cmd.CommandText = "PCKG_CUSTOM_HOLEC.INTEGRAITEMPEDIDO";

                cmd.Parameters.Add("pRETURN", OracleDbType.Decimal);
                cmd.Parameters["pRETURN"].Direction = ParameterDirection.ReturnValue;

                cmd.Parameters.Add("pCODESTAB", OracleDbType.Int32).Value = Globals.CodEstab;
                cmd.Parameters["pCODESTAB"].Direction = ParameterDirection.Input;

                cmd.Parameters.Add("pIDINTEGRACAO", OracleDbType.Int64).Value = pIntegraPedidos.IdIntegracao;
                cmd.Parameters["pIDINTEGRACAO"].Direction = ParameterDirection.Input;

                cmd.Parameters.Add("pCODPEDVENDA", OracleDbType.Int64).Value = pIntegraPedidos.CodPedVenda;
                cmd.Parameters["pCODPEDVENDA"].Direction = ParameterDirection.Input;

                cmd.Parameters.Add("pMSERRO", OracleDbType.Clob);
                cmd.Parameters["pMSERRO"].Direction = ParameterDirection.Output;

                await cmd.ExecuteNonQueryAsync();

                if (((OracleDecimal)(cmd.Parameters["pRETURN"].Value)).Value == 1)
                    throw new Exception(((OracleClob)(cmd.Parameters["pMSERRO"].Value)).Value);

            }


            return pIntegraPedidos;
        }

        public async Task<CZIntegraPedidos> TotalizeOrderValuesAsync(OracleConnection pConn, CZIntegraPedidos pIntegraPedidos)
        {
            using (OracleCommand cmd = new OracleCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = pConn;
                cmd.CommandText = "PCKG_CUSTOM_HOLEC.TOTALIZAPEDIDO";
                
                cmd.Parameters.Add("pCODESTAB", OracleDbType.Int32).Value = Globals.CodEstab;
                cmd.Parameters["pCODESTAB"].Direction = ParameterDirection.Input;

                cmd.Parameters.Add("pCODPEDVENDA", OracleDbType.Int64).Value = pIntegraPedidos.CodPedVenda;
                cmd.Parameters["pCODPEDVENDA"].Direction = ParameterDirection.Input;

                await cmd.ExecuteNonQueryAsync();
            }

            return pIntegraPedidos;
        }

        public async Task<CZIntegraPedidos> GenerateTaxValuesAsync(OracleConnection pConn, CZIntegraPedidos pIntegraPedidos)
        {
            using (OracleCommand cmd = new OracleCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = pConn;
                cmd.CommandText = "PCKG_CUSTOM_HOLEC.GERARIMPOSTOSPEDIDO";
                
                cmd.Parameters.Add("pCODESTAB", OracleDbType.Int32).Value = Globals.CodEstab;
                cmd.Parameters["pCODESTAB"].Direction = ParameterDirection.Input;

                cmd.Parameters.Add("pCODPEDVENDA", OracleDbType.Int64).Value = pIntegraPedidos.CodPedVenda;
                cmd.Parameters["pCODPEDVENDA"].Direction = ParameterDirection.Input;

                await cmd.ExecuteNonQueryAsync();
            }

            return pIntegraPedidos;
        }

        public async Task<CZIntegraPedidos> GenerateOrderCommissionAsync(OracleConnection pConn, CZIntegraPedidos pIntegraPedidos)
        {
            using (OracleCommand cmd = new OracleCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = pConn;
                cmd.CommandText = "PCKG_CUSTOM_HOLEC.GERARCOMISSAOPEDIDO";
                
                cmd.Parameters.Add("pCODESTAB", OracleDbType.Int32).Value = Globals.CodEstab;
                cmd.Parameters["pCODESTAB"].Direction = ParameterDirection.Input;

                cmd.Parameters.Add("pCODPEDVENDA", OracleDbType.Int64).Value = pIntegraPedidos.CodPedVenda;
                cmd.Parameters["pCODPEDVENDA"].Direction = ParameterDirection.Input;

                await cmd.ExecuteNonQueryAsync();
            }

            return pIntegraPedidos;
        }
    }
}
