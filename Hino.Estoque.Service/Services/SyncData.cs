﻿using Hino.Estoque.Aplication.Interfaces.Fiscal;
using Hino.Estoque.Aplication.Interfaces.Gerais;
using Hino.Estoque.Aplication.Interfaces.Vendas;
using Hino.Estoque.Aplication.Services.Fiscal;
using Hino.Estoque.Aplication.Services.Gerais;
using Hino.Estoque.Aplication.Services.Vendas;
using Hino.Estoque.Infra.Cross.Entities.Gerais;
using Hino.Estoque.Infra.Cross.Utils;
using Hino.Estoque.Service.DataBase.Access;
using Hino.Estoque.Service.DataBase.Oracle;
using Hino.Estoque.Service.Entities;
using Hino.Estoque.Service.Entities.Access.Sales;
using Hino.Estoque.Service.Repositories;
using Hino.Estoque.Service.Services.Access.Gerais;
using Hino.Estoque.Service.Services.Access.Sales;
using Hino.Estoque.Service.Tables;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Service.Application
{
    public class SyncData
    {
        private readonly DepResolver _DepResolver;
        public bool Synchonizing { get; private set; }
        public readonly JsonSerializerSettings _JsonSerializerSettings;
        public readonly JsonSerializer _JsonSerializer;
        public string tokenbuscacnpj { get; set; }

        private static readonly ILog Logger = LogManager.GetLogger(typeof(EstoqueService));
        private readonly DBOracle _bdCont;
        private readonly DBAccess _DBAccess;

        private readonly GESincConfig _geSincConfig;
        private readonly GESyncLog _geSyncLog;

        public SyncData(DepResolver pDepResolver)
        {
            _DepResolver = pDepResolver;
            _bdCont = DBOracle.GetInstance(Globals.ConnStr);
            _DBAccess = DBAccess.GetInstance(Globals.ConnAccessStr);

            _geSincConfig = new GESincConfig(_bdCont);
            _geSyncLog = new GESyncLog(_bdCont);

            _JsonSerializer = new JsonSerializer
            {
                Culture = new System.Globalization.CultureInfo("pt-BR"),
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                DateFormatString = "dd/MM/yyyy HH:mm:ss"
            };

            _JsonSerializerSettings = new JsonSerializerSettings
            {
                Culture = new System.Globalization.CultureInfo("pt-BR"),
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                DateFormatString = "dd/MM/yyyy HH:mm:ss"
            };
        }

        public async Task Syncronize()
        {
            if (Synchonizing)
                return;

            Synchonizing = true;

            Logger.Info("Sincronizando");

            _geSincConfig.codestab = Globals.CodEstab;

            var ultDate = new DateTime(1900, 1, 1, 1, 1, 1);

            if (_geSincConfig.Carrega())
            {
                await Task.Delay(10);
                // await SyncPaymentType((DateTime)_geSincConfig.sincformapgto);
                // await SyncPaymentCondition((DateTime)_geSincConfig.sinccondpgto);
                // // await SyncCountries(ultDate);
                // // await SyncStates(ultDate);
                // // await SyncCities(ultDate);
                // await SyncRegion((DateTime)_geSincConfig.sincregioes);
                // await SyncEnterprises((DateTime)_geSincConfig.sincempresas);
                // await SyncProducts((DateTime)_geSincConfig.sincprodutos);
                // await SyncSalePrice((DateTime)_geSincConfig.sinctabpreco);
                // await SyncOrders((DateTime)_geSincConfig.sincprodutos);
                // await SyncProductsStockBalance();
            }
            else
                Logger.Info("Não encontrado a configuração de sincronização.");
            Logger.Info("Rotina de sincronização concluída.");

            Synchonizing = false;
        }

        public async Task PushChanges()
        {
            if (Synchonizing)
                return;

            Synchonizing = true;

            Logger.Info("Buscando alterações.");

            _geSincConfig.codestab = Globals.CodEstab;

            var ultDate = new DateTime(1900, 1, 1, 1, 1, 1);
            

            if (_geSincConfig.Carrega())
            {
                await Task.Delay(10);

                await PushEnterprises((DateTime)_geSincConfig.buscapedaccess);
                await PushEnterpriseWohner((DateTime)_geSincConfig.buscapedaccess);
                await PushProducts((DateTime)_geSincConfig.buscapedaccess);
                await PushProductWohner((DateTime)_geSincConfig.buscapedaccess);
                await PushOrders((DateTime)_geSincConfig.buscapedaccess);
                await PushDraws((DateTime)_geSincConfig.buscapedaccess);
                // await TryLoadErrors();

            }
            else
                Logger.Info("Não encontrado a configuração de sincronização.");
            Logger.Info("Rotina de Busca de alterações concluída.");

            Synchonizing = false;
        }

        #region Push Info
        enum TableLog
        {
            VEPEDVENDA = 0,
            VEITEMPEDIDO = 1,
            GEEMPRESA = 2,
            FSPRODUTO = 3
        }

        enum TipoLog
        {
            success = 0,
            error = 1
        }
        
        #region Get Cod Ped. Venda
        private long GetCodPedVenda(string pCodPedIntegracao)
        {
            bancoDados bd = new bancoDados(_bdCont.AbreConn());
            return bd.ExecScalarRetInt64(@"SELECT VEPEDVENDA.CODPEDVENDA
                                            FROM VEPEDVENDA
                                           WHERE VEPEDVENDA.CODPEDINTEGRACAO = :pIDAPI
                                             AND VEPEDVENDA.CODESTAB = :pCODESTAB",
                          bd.CriaParam("pIDAPI", OracleDbType.Varchar2, pCodPedIntegracao),
                          bd.CriaParam("pCODESTAB", OracleDbType.Int32, Globals.CodEstab));
        }
        #endregion

        #region Order is Integrated
        private bool OrderIsIntegrated(string pCodPedIntegracao)
        {
            bancoDados bd = new bancoDados(_bdCont.AbreConn());
            return bd.ExecScalarRetInt(@"SELECT COUNT(1)
                                            FROM VEPEDVENDA
                                           WHERE VEPEDVENDA.CODPEDINTEGRACAO = :pIDAPI
                                             AND VEPEDVENDA.CODESTAB = :pCODESTAB",
                          bd.CriaParam("pIDAPI", OracleDbType.Varchar2, pCodPedIntegracao),
                          bd.CriaParam("pCODESTAB", OracleDbType.Int32, Globals.CodEstab)) > 0;
        }
        #endregion
        
        #region Enterprise is Integrated
        private bool EnterpriseIsIntegrated(string pCPFCNPJ)
        {
            bancoDados bd = new bancoDados(_bdCont.AbreConn());
            return bd.ExecScalarRetInt(@"SELECT COUNT(1)
                                            FROM GEENDERECO
                                           WHERE GEENDERECO.NUMCNPJCPF = :pCPFCNPJ",
                          bd.CriaParam("pCPFCNPJ", OracleDbType.Varchar2, pCPFCNPJ)) > 0;
        }
        #endregion
        
        #region Product is Integrated
        private bool ProductIsIntegrated(string pCodProduto)
        {
            bancoDados bd = new bancoDados(_bdCont.AbreConn());
            return bd.ExecScalarRetInt(@"SELECT COUNT(1)
                                            FROM FSPRODUTOPARAMESTAB
                                           WHERE FSPRODUTOPARAMESTAB.CODPRODUTO = :pCODPRODUTO
                                             AND FSPRODUTOPARAMESTAB.CODESTAB   = :pCODESTAB",
                          bd.CriaParam("pCODPRODUTO", OracleDbType.Varchar2, pCodProduto),
                          bd.CriaParam("pCODESTAB", OracleDbType.Int32, Globals.CodEstab)) > 0;
        }
        #endregion
        
        #region Draw is Integrated
        private bool DrawIsIntegrated(string pDesenho)
        {
            bancoDados bd = new bancoDados(_bdCont.AbreConn());
            return bd.ExecScalarRetInt(@"SELECT COUNT(1)
                                           FROM PEPRODUTO
                                          WHERE PEPRODUTO.DESENHO          = :pDESENHO",
                          bd.CriaParam("pDESENHO", OracleDbType.Varchar2, pDesenho)) > 0;
        }
        #endregion

        #region VincDraw is Integrated
        private bool VincDrawIsIntegrated(string pDesenho)
        {
            bancoDados bd = new bancoDados(_bdCont.AbreConn());
            return bd.ExecScalarRetInt(@"SELECT COUNT(1)
                                           FROM PEPRODUTO
                                          WHERE PEPRODUTO.DESENHOVINCULADO          = :pDESENHOVINCULADO",
                          bd.CriaParam("pDESENHOVINCULADO", OracleDbType.Varchar2, pDesenho)) > 0;
        }
        #endregion

        #region Get Cod Empresa
        private long GetCodEmpresa(string pCPFCNPJ)
        {
            bancoDados bd = new bancoDados(_bdCont.AbreConn());
            return bd.ExecScalarRetInt64(@"SELECT GEENDEMPRESA.CODEMPRESA
                                             FROM GEENDERECO,
                                                  GEENDEMPRESA
                                            WHERE GEENDEMPRESA.CODENDERECO = GEENDERECO.CODENDERECO
                                              AND GEENDERECO.NUMCNPJCPF = :pCPFCNPJ",
                          bd.CriaParam("pCPFCNPJ", OracleDbType.Varchar2, pCPFCNPJ));
        }
        #endregion

        #region Convert Resultado busca para empresa
        private geEmpresaBuscaCNPJ ConvertToBuscaEmpresa(JObject pJson, JsonSerializerSettings pJsonSerializerSettings, JsonSerializer _JsonSerializer)
        {
            var empresa = new geEmpresaBuscaCNPJ();

            var Container = (JContainer)JsonConvert.DeserializeObject(pJson.ToString(), pJsonSerializerSettings);

            empresa = Container.ToObject<geEmpresaBuscaCNPJ>(_JsonSerializer);

            return empresa;
        }
        #endregion

        #region Convert Resultado busca para cep
        private geCEPResult ConvertToBuscaCep(JObject pJson, JsonSerializerSettings pJsonSerializerSettings, JsonSerializer _JsonSerializer)
        {
            var CEPResult = new geCEPResult();

            var Container = (JContainer)JsonConvert.DeserializeObject(pJson.ToString(), pJsonSerializerSettings);

            CEPResult.data = Container.ToObject<geCEPEndereco>(_JsonSerializer);

            return CEPResult;
        }
        #endregion

        #region Buscar dados da empresa CNPJ
        private async Task<int> BuscarDadosEmpresaCNPJAsync(OracleConnection pConn, CZIntegrEmpresa pIntegrEmpresa)
        {
            var CodEmpresa = 0;

            try
            {
                var cnpj = (pIntegrEmpresa.CNPJCPF.Replace(".", "").Replace("/", "").Replace("-", "")).Trim();

                var _JsonSerializerSettings = new JsonSerializerSettings
                {
                    Culture = new System.Globalization.CultureInfo("pt-BR"),
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                    DateFormatString = "dd/MM/yyyy HH:mm:ss"
                };

                var _JsonSerializer = new JsonSerializer
                {
                    Culture = new System.Globalization.CultureInfo("pt-BR"),
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                    DateFormatString = "dd/MM/yyyy HH:mm:ss"
                };

                Logger.Info("Buscando dados da empresa na receita.");
                geEmpresaBuscaCNPJ jsonEmpresa = null;
                try
                {
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(string.Format(@"https://www.receitaws.com.br/v1/cnpj/{0}", cnpj));
                    request.Headers.Add("Authorization", string.Format("Bearer {0}", tokenbuscacnpj));
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    string content = new StreamReader(response.GetResponseStream()).ReadToEnd();
                    jsonEmpresa = ConvertToBuscaEmpresa(JObject.Parse(content), _JsonSerializerSettings, _JsonSerializer);
                }
                catch (Exception ex)
                {
                    jsonEmpresa = null;
                    if (ex.Message.IndexOf("429") > -1)
                        throw new Exception("O servidor da receita não respondeu, tente novamente em alguns minutos.");
                    else
                        throw new Exception($"Não foi possível buscar os dados.\r\nMotivo: {ex.Message}");
                }

                if (string.IsNullOrEmpty(jsonEmpresa.cepLimpo))
                    throw new Exception("Não foi possível localizar os dados de endereço da empresa.");

                if (jsonEmpresa != null && !string.IsNullOrEmpty(jsonEmpresa.cepLimpo))
                {
                    jsonEmpresa.Endereco = null;
                    try
                    {
                        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(
                            // string.Format("http://3.229.108.251:6081/api/cep/{0}",
                            // string.Format("http://localhost:6090/Hino.Salesforce.API/api/cep/{0}",
                            string.Format("http://viacep.com.br/ws/{0}/json/",
                            jsonEmpresa.cepLimpo));
                        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                        string content = new StreamReader(response.GetResponseStream()).ReadToEnd();

                        jsonEmpresa.Endereco = ConvertToBuscaCep(JObject.Parse(JObject.Parse(content).ToString()), _JsonSerializerSettings, _JsonSerializer);
                    }
                    catch (Exception)
                    {
                        jsonEmpresa.Endereco = null;
                        throw new Exception("Não foi possível localizar os dados de endereço da empresa.");
                    }
                }


                if (jsonEmpresa != null)
                {
                    Logger.Info("Salvando cabeçalho da empresa.");
                    var Empresa = new geEmpresa();
                    CodEmpresa = await Empresa.NextSequenceAsync(pConn);
                    Empresa.codempresa = CodEmpresa;
                    Empresa.razaosocial = jsonEmpresa.nome.ToUpper();
                    Empresa.nomefantasia = jsonEmpresa.nomefantasia.ToUpper();
                    if (Empresa.razaosocial.Length > 60)
                        Empresa.razaosocial = Empresa.razaosocial.Substring(0, 60);
                    if (Empresa.nomefantasia.Length > 60)
                        Empresa.nomefantasia = Empresa.nomefantasia.Substring(0, 60);
                    Empresa.codestab = Globals.CodEstab;
                    Empresa.geestabRazaosocial = jsonEmpresa.nomefantasia.ToUpper();
                    Empresa.codusuasist = "";
                    Empresa.status = 0;
                    Empresa.represinterno = 0;
                    if (pIntegrEmpresa.Tipo == "C")
                    {
                        if (pIntegrEmpresa.NContribuinte)
                        {
                            Empresa.codgrpfisemp = 10;
                            Empresa.tipoempresa = "C";
                            Empresa.classifempresa = "C2";
                        }
                        else
                        {
                            Empresa.codgrpfisemp = 8;
                            Empresa.tipoempresa = "C";
                            Empresa.classifempresa = "C1";
                        }
                    }
                    else if (pIntegrEmpresa.Tipo == "T")
                    {
                        Empresa.codgrpfisemp = 3;
                        Empresa.tipoempresa = "T";
                        Empresa.classifempresa = "T1";
                    }
                    Empresa.dtcertifvalid = null;
                    Empresa.certificadoobrig = 0;
                    Empresa.simplesnacional = 0;
                    Empresa.tagpersnfe = 0;
                    Empresa.semnftransito = 0;



                    Logger.Info("Gerando conta contabil da empresa.");
                    EmpresaContaContabil empCC = new EmpresaContaContabil();
                    if (await empCC.GerarContaContabilAsync(pConn,
                        Empresa.tipoempresa,
                        Empresa.razaosocial,
                        0))
                    {
                        if (Empresa.tipoempresa == "C" || Empresa.tipoempresa == "F" ||
                                      Empresa.tipoempresa == "T")
                        {
                            Empresa.contacontabil = empCC.ContaContabilNova;
                            Empresa.codestabcc = empCC.UltimaContaCodestab;
                        }
                        else if (Empresa.tipoempresa == "R" || Empresa.tipoempresa == "V")
                        {
                            Empresa.contacomissaopag = empCC.ContaContabilNova;
                            Empresa.codestabcccomissaopag = empCC.UltimaContaCodestab;
                        }
                    }
                    await Empresa.InsertEnterpriseCabecAsync(pConn);
                    await Empresa.InsertEnterpriseParamAsync(pConn);
                    await Empresa.InsertEnterprisePersAsync(pConn);

                    if (jsonEmpresa.Endereco != null)
                    {
                        Logger.Info("Salvando endereço da empresa.");

                        var Endereco = new geEmpresaEndereco();
                        Endereco.codpais = 1;

                        Endereco.tipocnpjcpf = 0;
                        Endereco.numcnpjcpf = jsonEmpresa.cnpjLimpo;

                        Endereco.cep = jsonEmpresa.cepLimpo;
                        Endereco.endereco = jsonEmpresa.logradouro.ToUpper();
                        Endereco.bairro = jsonEmpresa.bairro.ToUpper();

                        var nro = 0;
                        if (int.TryParse(jsonEmpresa.numero, out nro))
                            Endereco.numero = nro;

                        if (!string.IsNullOrEmpty(jsonEmpresa.Endereco.data.ibge))
                            Endereco.codmunicipio = await Endereco.RetCodMunicipioAsync(pConn, jsonEmpresa.Endereco.data.ibge);

                        if (!string.IsNullOrEmpty(jsonEmpresa.Endereco.data.uf))
                            Endereco.codestado = await Endereco.BuscaCodEstadoPorSiglaAsync(pConn, jsonEmpresa.Endereco.data.uf);

                        Endereco.codpais = 1;

                        Endereco.complemento = jsonEmpresa.complemento.ToUpper();

                        Endereco.codempresa = CodEmpresa;
                        Endereco.tipo = 0;
                        Endereco.doca = "";
                        Endereco.planta = "";
                        Endereco.codnatjur = "";
                        Endereco.tpfornec = "P";
                        Endereco.cobradescarga = 0;
                        Endereco.cobraagend = 0;
                        Endereco.cobrasuframa = 0;
                        Endereco.cobrapalet = 0;
                        Endereco.nifvat = "";
                        Endereco.rntrc = "";
                        Endereco.email = jsonEmpresa.email;
                        Endereco.emailnfe = pIntegrEmpresa.EMailNFe;
                        Endereco.site = pIntegrEmpresa.Site;
                        Endereco.telefone = jsonEmpresa.primeirotelefone;
                        Endereco.codendereco = await Endereco.NextSequenceAsync(pConn);

                        await Endereco.InsertAddressAsync(pConn);
                        await Endereco.InsertEnterpriseAddressAsync(pConn);

                        if (!string.IsNullOrEmpty(pIntegrEmpresa.EnderecoEntrega) &&
                            !string.IsNullOrEmpty(pIntegrEmpresa.BairroEntrega) &&
                            !string.IsNullOrEmpty(pIntegrEmpresa.CepEntrega))
                        {
                            var EnderecoEntrega = new geEmpresaEndereco();
                            EnderecoEntrega.codpais = 1;
                            EnderecoEntrega.tipocnpjcpf = 0;
                            EnderecoEntrega.numcnpjcpf = pIntegrEmpresa.CNPJCPF;

                            EnderecoEntrega.cep = ((pIntegrEmpresa.CepEntrega ?? "").Replace("-", "").Replace(".", "")).Trim();
                            EnderecoEntrega.endereco = pIntegrEmpresa.EnderecoEntrega.ToUpper();
                            EnderecoEntrega.bairro = pIntegrEmpresa.BairroEntrega.ToUpper();

                            if (!string.IsNullOrEmpty(pIntegrEmpresa.CidadeEntrega))
                                EnderecoEntrega.codmunicipio = await EnderecoEntrega.RetCodMunicipioAsync(pConn, pIntegrEmpresa.CidadeEntrega);

                            if (!string.IsNullOrEmpty(pIntegrEmpresa.EstadoEntrega))
                                EnderecoEntrega.codestado = await EnderecoEntrega.BuscaCodEstadoPorIBGEAsync(pConn, pIntegrEmpresa.EstadoEntrega);

                            EnderecoEntrega.codpais = 1;
                            EnderecoEntrega.complemento = "";
                            EnderecoEntrega.codempresa = CodEmpresa;
                            EnderecoEntrega.tipo = 1;
                            EnderecoEntrega.doca = "";
                            EnderecoEntrega.planta = "";
                            EnderecoEntrega.codnatjur = "";
                            EnderecoEntrega.tpfornec = "P";
                            EnderecoEntrega.cobradescarga = 0;
                            EnderecoEntrega.cobraagend = 0;
                            EnderecoEntrega.cobrasuframa = 0;
                            EnderecoEntrega.cobrapalet = 0;
                            EnderecoEntrega.nifvat = "";
                            EnderecoEntrega.rntrc = "";
                            EnderecoEntrega.email = "";
                            EnderecoEntrega.emailnfe = "";
                            EnderecoEntrega.site = "";
                            EnderecoEntrega.telefone = (pIntegrEmpresa.FoneEntrega ?? "").SubStr(0, 20);
                            EnderecoEntrega.codendereco = await EnderecoEntrega.NextSequenceAsync(pConn);

                            await EnderecoEntrega.InsertAddressAsync(pConn);
                            await EnderecoEntrega.InsertEnterpriseAddressAsync(pConn);
                        }


                        if (!string.IsNullOrEmpty(pIntegrEmpresa.EstadoCobranca) &&
                                !string.IsNullOrEmpty(pIntegrEmpresa.BairroCobranca) &&
                                !string.IsNullOrEmpty(pIntegrEmpresa.CepCobranca))
                        {
                            var EnderecoCobranca = new geEmpresaEndereco();
                            EnderecoCobranca.codpais = 1;
                            EnderecoCobranca.tipocnpjcpf = 0;
                            EnderecoCobranca.numcnpjcpf = pIntegrEmpresa.CNPJCPF;

                            EnderecoCobranca.cep = ((pIntegrEmpresa.CepCobranca ?? "").Replace("-", "").Replace(".", "")).Trim();
                            EnderecoCobranca.endereco = pIntegrEmpresa.EnderecoCobranca.ToUpper();
                            EnderecoCobranca.bairro = pIntegrEmpresa.BairroCobranca.ToUpper();

                            if (!string.IsNullOrEmpty(pIntegrEmpresa.CidadeCobranca))
                                EnderecoCobranca.codmunicipio = await EnderecoCobranca.RetCodMunicipioAsync(pConn, pIntegrEmpresa.CidadeCobranca);

                            if (!string.IsNullOrEmpty(pIntegrEmpresa.EstadoCobranca))
                                EnderecoCobranca.codestado = await EnderecoCobranca.BuscaCodEstadoPorIBGEAsync(pConn, pIntegrEmpresa.EstadoCobranca);

                            EnderecoCobranca.codpais = 1;
                            EnderecoCobranca.complemento = "";
                            EnderecoCobranca.codempresa = CodEmpresa;
                            EnderecoCobranca.tipo = 2;
                            EnderecoCobranca.doca = "";
                            EnderecoCobranca.planta = "";
                            EnderecoCobranca.codnatjur = "";
                            EnderecoCobranca.tpfornec = "P";
                            EnderecoCobranca.cobradescarga = 0;
                            EnderecoCobranca.cobraagend = 0;
                            EnderecoCobranca.cobrasuframa = 0;
                            EnderecoCobranca.cobrapalet = 0;
                            EnderecoCobranca.nifvat = "";
                            EnderecoCobranca.rntrc = "";
                            EnderecoCobranca.email = "";
                            EnderecoCobranca.emailnfe = "";
                            EnderecoCobranca.site = "";
                            EnderecoCobranca.telefone = (pIntegrEmpresa.FoneCobranca ?? "").SubStr(0, 20);
                            EnderecoCobranca.codendereco = await EnderecoCobranca.NextSequenceAsync(pConn);

                            await EnderecoCobranca.InsertAddressAsync(pConn);
                            await EnderecoCobranca.InsertEnterpriseAddressAsync(pConn);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CodEmpresa = 0;
                throw new Exception(ex.Message);
            }

            return CodEmpresa;
        }
        #endregion
        
        #region Push Products Wohner
        private async Task PushEnterpriseWohner(DateTime pLastSync)
        {
            try
            {
                Logger.Info("Gerando cadastros de empresa Wohner.");
                var IntegrEmpresaAS = _DepResolver.GetService<CZIntegrEmpresaAS>(typeof(ICZIntegrEmpresaAS));
                var _IntegraEmpresaService = new
                    IntegraEmpresaService(_bdCont, _DBAccess, IntegrEmpresaAS);

                await _IntegraEmpresaService.GenerateEnterpriseWohner();
            }
            catch (Exception e)
            {
                Logger.Error("Problema ao gerar empresa Wohner");
                Logger.Error($"Motivo: {e.Message}");

            }
        }
        #endregion

        #region Push Enterprises
        private async Task PushEnterprises(DateTime pLastSync)
        {
            try
            {
                Logger.Info("Buscando empresas.");
                var codempresa = 0L;

                var IntegrEmpresaAS = _DepResolver.GetService<CZIntegrEmpresaAS>(typeof(ICZIntegrEmpresaAS));
                var _IntegraEmpresaService = new
                    IntegraEmpresaService(_bdCont, _DBAccess, IntegrEmpresaAS);

                Logger.Info("Carregando empresas encontrados.");
                await _IntegraEmpresaService.PushNewOrdersAsync();
                var results = (await _IntegraEmpresaService.GetDispIntegAsync()).ToList();

                Logger.Info($"Empresas encontrados: {results.Count()}.");

                Logger.Info("Percorrendo empresas encontrados.");
                for (int i = 0, length = results.Count(); i < length; i++)
                {
                    codempresa = 0L;
                    Logger.Info($"Integrando empresa: {i + 1} de {results.Count()}.");

                    Logger.Info("Verificando se a empresa já foi integrada.");

                    var EnterpriseActual = results[i];

                    using (var conn = new OracleConnection(Globals.ConnStr))
                    {
                        conn.Open();

                        var transaction = conn.BeginTransaction();

                        if (EnterpriseIsIntegrated(EnterpriseActual.CNPJCPF))
                        {
                            Logger.Info("Empresa já integrada anteriormente.");

                            Logger.Info("Gerando log de sucesso");

                            codempresa = GetCodEmpresa(EnterpriseActual.CNPJCPF);

                            EnterpriseActual.CodEmpresa = codempresa;
                            EnterpriseActual.DataIntegra = DateTime.Now;
                            EnterpriseActual.EmpIntegra = true;
                            
                            await _IntegraEmpresaService.UpdateIntegratedAsync(conn, EnterpriseActual);

                            transaction.Commit();
                            continue;
                        }

                        try
                        {
                            if (EnterpriseActual.CNPJCPF.Length != 14)
                                throw new Exception("Não é possível realizar a busca de consumidor final");

                            codempresa = await BuscarDadosEmpresaCNPJAsync(conn, EnterpriseActual);

                            EnterpriseActual.CodEmpresa = codempresa;

                            Logger.Info("Empresa salva com sucesso.");
                            Logger.Info($"Empresa gerada: {codempresa}.");

                            await _IntegraEmpresaService.UpdateCodEmpresaAsync(conn, EnterpriseActual);
                        }
                        catch (Exception e)
                        {
                            transaction.Rollback();
                            codempresa = 0;
                            Logger.Error($"Problema ao salvar a empresa {EnterpriseActual.CNPJCPF}");
                            Logger.Error($"Motivo: {e.Message}");
                        }

                        if (codempresa > 0)
                        {
                            EnterpriseActual.CodEmpresa = codempresa;
                            EnterpriseActual.DataIntegra = DateTime.Now;
                            EnterpriseActual.EmpIntegra = true;

                            transaction.Commit();
                            await _IntegraEmpresaService.UpdateIntegratedAsync(conn, EnterpriseActual);
                        }
                    }

                }

                // _geSincConfig.UpdateLastPush("BUSCAPEDACCESS", DateTime.Now);
                Logger.Info("Busca de empresa concluída com sucesso");


            }
            catch (Exception e)
            {
                Logger.Error("Problema ao buscar empresa");
                Logger.Error($"Motivo: {e.Message}");
            }
        }
        #endregion
        
        #region Push Products Wohner
        private async Task PushProductWohner(DateTime pLastSync)
        {
            try
            {
                Logger.Info("Gerando cadastros de produto Wohner.");
                var IntegProdutosAS = _DepResolver.GetService<CZIntegProdutosAS>(typeof(ICZIntegProdutosAS));
                var IntegrProdDetAS = _DepResolver.GetService<CZIntegrProdDetAS>(typeof(ICZIntegrProdDetAS));
                var _IntegraProdutosService = new
                    IntegraProdutosService(_bdCont, _DBAccess, IntegProdutosAS, IntegrProdDetAS);

                await _IntegraProdutosService.GenerateProductWohner();
            }
            catch (Exception e)
            {
                Logger.Error("Problema ao gerar produto Wohner");
                Logger.Error($"Motivo: {e.Message}");

            }


        }
        #endregion

        #region Push Products
        private async Task PushProducts(DateTime pLastSync)
        {
            try
            {
                Logger.Info("Buscando produtos.");
                var codproduto = "";

                var IntegProdutosAS = _DepResolver.GetService<CZIntegProdutosAS>(typeof(ICZIntegProdutosAS));
                var IntegrProdDetAS = _DepResolver.GetService<CZIntegrProdDetAS>(typeof(ICZIntegrProdDetAS));
                var _IntegraProdutosService = new
                    IntegraProdutosService(_bdCont, _DBAccess, IntegProdutosAS, IntegrProdDetAS);

                Logger.Info("Carregando produtos encontrados.");
                await _IntegraProdutosService.PushNewProductsAsync();
                var results = (await _IntegraProdutosService.GetDispIntegAsync()).ToList();

                Logger.Info($"Produtos encontrados: {results.Count()}.");

                Logger.Info("Percorrendo produtos encontrados.");
                for (int i = 0, length = results.Count(); i < length; i++)
                {
                    codproduto = "";
                    Logger.Info($"Integrando produto: {i + 1} de {results.Count()}.");


                    var ProductActual = results[i];


                    using (var conn = new OracleConnection(Globals.ConnStr))
                    {
                        conn.Open();

                        var transaction = conn.BeginTransaction();

                        Logger.Info("Verificando se o produto já foi integrado.");

                        if (ProductIsIntegrated(ProductActual.PartNumber))
                        {
                            Logger.Info("Produto já integrado anteriormente.");

                            codproduto = ProductActual.PartNumber;

                            ProductActual.DataIntegra = DateTime.Now;
                            ProductActual.ProdIntegra = true;

                            if (!string.IsNullOrEmpty(ProductActual.CodWohner))
                                await _IntegraProdutosService.UpdateCodWohnerAsync(conn, ProductActual);

                            await _IntegraProdutosService.UpdateIntegratedAsync(conn, ProductActual);

                            // await _IntegraProdutosService.GenerateProductWohner(conn, ProductActual);
                            
                            transaction.Commit();
                            continue;
                        }

                        Logger.Info("Salvando produto.");

                        try
                        {
                            Logger.Info("Tentativa de integrar produto.");
                            await _IntegraProdutosService.GenerateProductAsync(conn, ProductActual);
                            await _IntegraProdutosService.UpdateCodWohnerAsync(conn, ProductActual);
                            // await _IntegraProdutosService.GenerateProductWohner(conn, ProductActual);

                            codproduto = ProductActual.PartNumber;

                            Logger.Info("Produto integrado com sucesso.");
                            Logger.Info($"Produto gerado: {codproduto}.");
                        }
                        catch (Exception e)
                        {
                            transaction.Rollback();
                            codproduto = "";
                            Logger.Error($"Problema ao salvar o produto {ProductActual.PartNumber}");
                            Logger.Error($"Motivo: {e.Message}");
                        }

                        if (!string.IsNullOrEmpty(codproduto))
                        {
                            ProductActual.DataIntegra = DateTime.Now;
                            ProductActual.ProdIntegra = true;

                            await _IntegraProdutosService.UpdateIntegratedAsync(conn, ProductActual);
                            transaction.Commit();
                        }
                    }

                }

                // _geSincConfig.UpdateLastPush("BUSCAPEDACCESS", DateTime.Now);
                Logger.Info("Busca de Produto concluída com sucesso");


            }
            catch (Exception e)
            {
                Logger.Error("Problema ao busca Produto");
                Logger.Error($"Motivo: {e.Message}");
            }
        }
        #endregion

        #region Push Orders
        private async Task PushOrders(DateTime pLastSync)
        {
            try
            {
                Logger.Info("Buscando Pedidos.");
                var codpedvenda = 0L;

                var IntegraPedidosAS = _DepResolver.GetService<CZIntegraPedidosAS>(typeof(ICZIntegraPedidosAS));
                var _IntegraPedidosService = new
                    IntegraPedidosService(_bdCont, _DBAccess, IntegraPedidosAS);

                var IntegPedItensAS = _DepResolver.GetService<CZIntegPedItensAS>(typeof(ICZIntegPedItensAS));
                var _IntegraItensPedidosService = new
                     IntegraItensPedidosService(_bdCont, _DBAccess,
                     IntegPedItensAS);

                Logger.Info("Carregando pedidos encontrados.");
                await _IntegraPedidosService.PushNewOrdersAsync();
                var results = (await _IntegraPedidosService.GetDispIntegAsync()).ToList();

                await _IntegraItensPedidosService.PushNewItemsOrdersAsync( results.Select(r => 
                new vePedReferencia { Pedido = r.Pedido, IdIntegracao = r.IdIntegracao }).ToArray());

                Logger.Info($"Pedidos encontrados: {results.Count()}.");

                Logger.Info("Percorrendo pedidos encontrados.");
                for (int i = 0, length = results.Count(); i < length; i++)
                {
                    codpedvenda = 0L;
                    Logger.Info($"Integrando pedido: {i + 1} de {results.Count()}.");

                    var OrderActual = results[i];

                    if (OrderActual.Cancelado)
                    {
                        try
                        {
                            Logger.Info("Cancelando pedido.");
                            await _IntegraPedidosService.CancelOrderAsync(_bdCont.AbreConn(), OrderActual);
                            codpedvenda = (long)OrderActual.CodPedVenda;

                            Logger.Info("Cabeçalho do pedido integrado com sucesso.");
                            Logger.Info($"Pedido cancelado: {codpedvenda}.");

                            OrderActual.CodPedVenda = codpedvenda;
                            OrderActual.DataIntegra = DateTime.Now;
                            OrderActual.PedIntegrado = true;

                            await _IntegraPedidosService.UpdateIntegratedAsync(OrderActual);
                        }
                        catch (Exception e)
                        {
                            codpedvenda = 0;
                            Logger.Error($"Problema ao salvar o pedido { OrderActual.Pedido }");
                            Logger.Error($"Motivo: {e.Message}");
                        }
                        continue;
                    }

                    Logger.Info("Verificando se o pedido já foi integrado.");
                    if (OrderIsIntegrated(OrderActual.Pedido))
                    {
                        Logger.Info("Pedido já integrado anteriormente.");

                        codpedvenda = GetCodPedVenda(OrderActual.Pedido);

                        OrderActual.CodPedVenda = codpedvenda;
                        OrderActual.DataIntegra = DateTime.Now;
                        OrderActual.PedIntegrado = true;

                        await _IntegraPedidosService.UpdateIntegratedAsync(OrderActual);
                        continue;
                    }
                    using (var conn = new OracleConnection(Globals.ConnStr))
                    {
                        conn.Open();

                        var transaction = conn.BeginTransaction();

                        Logger.Info("Salvando cabeçalho do pedido.");

                        try
                        {
                            Logger.Info("Tentativa de integrar pedido.");
                            await _IntegraPedidosService.GenerateOrderCabecAsync(conn, OrderActual);
                            codpedvenda = (long)OrderActual.CodPedVenda;

                            Logger.Info("Cabeçalho do pedido integrado com sucesso.");
                            Logger.Info($"Pedido gerado: {codpedvenda}.");

                            await _IntegraPedidosService.UpdateCodPedVendaAsync(OrderActual);
                        }
                        catch (Exception e)
                        {
                            transaction.Rollback();
                            codpedvenda = 0;
                            Logger.Error($"Problema ao salvar o pedido {OrderActual.Pedido}");
                            Logger.Error($"Motivo: {e.Message}");
                        }

                        if (codpedvenda > 0)
                        {
                            try
                            {
                                Logger.Info($"Integrando itens do pedido: {codpedvenda}");

                                await _IntegraPedidosService.GenerateOrderItemsAsync(conn, OrderActual);

                                Logger.Info($"Items integrados com sucesso.");

                                Logger.Info("Gerando log de sucesso");
                            }
                            catch (Exception e)
                            {
                                transaction.Rollback();
                                codpedvenda = 0;
                                Logger.Error($"Problema ao salvar items do pedido {OrderActual.Pedido}");
                                Logger.Error($"Motivo: {e.Message}");
                            }
                        }

                        if (codpedvenda > 0)
                        {
                            try
                            {
                                Logger.Info($"Gerando impostos do pedido {codpedvenda}.");

                                await _IntegraPedidosService.GenerateTaxValuesAsync(conn, OrderActual);

                                Logger.Info($"Impostos do pedido {codpedvenda} gerados com sucesso.");
                            }
                            catch (Exception e)
                            {
                                transaction.Rollback();
                                codpedvenda = 0;
                                Logger.Error($"Problema ao gerar o imposto do pedido {codpedvenda}");
                                Logger.Error($"Motivo: {e.Message}");
                            }
                        }

                        if (codpedvenda > 0)
                        {
                            try
                            {
                                Logger.Info($"Totalizando o pedido {codpedvenda}.");

                                await _IntegraPedidosService.TotalizeOrderValuesAsync(conn, OrderActual);

                                Logger.Info($"Pedido {codpedvenda} Totalizado com sucesso.");
                            }
                            catch (Exception e)
                            {
                                transaction.Rollback();
                                codpedvenda = 0;
                                Logger.Error($"Problema ao totalizar o pedido {codpedvenda}");
                                Logger.Error($"Motivo: {e.Message}");
                                
                            }
                        }

                        if (codpedvenda > 0)
                        {
                            try
                            {
                                Logger.Info($"Gerando comissão do pedido {codpedvenda}.");

                                await _IntegraPedidosService.GenerateOrderCommissionAsync(conn, OrderActual);

                                Logger.Info($"Impostos do pedido {codpedvenda} gerados com sucesso.");
                            }
                            catch (Exception e)
                            {
                                transaction.Rollback();
                                codpedvenda = 0;
                                Logger.Error($"Problema ao gerar o imposto do pedido {codpedvenda}");
                                Logger.Error($"Motivo: {e.Message}");
                            }
                        }

                        if (codpedvenda > 0)
                        {
                            OrderActual.CodPedVenda = codpedvenda;
                            OrderActual.DataIntegra = DateTime.Now;
                            OrderActual.PedIntegrado = true;

                            transaction.Commit();
                            await _IntegraPedidosService.UpdateIntegratedAsync(OrderActual);
                        }
                    }

                }

                _geSincConfig.UpdateLastPush("BUSCAPEDACCESS", DateTime.Now);
                Logger.Info("Busca de pedidos concluída com sucesso");


            }
            catch (Exception e)
            {
                Logger.Error("Problema ao buscar pedidos");
                Logger.Error($"Motivo: {e.Message}");
            }
        }
        #endregion

        #region Push Draws
        private async Task PushDraws(DateTime pLastSync)
        {
            try
            {
                Logger.Info("Buscando desenhos.");

                var IntegracaoRevisaoAS = _DepResolver.GetService<CZIntegracaoRevisaoAS>(typeof(ICZIntegracaoRevisaoAS));
                var _IntegracaoRevisaoService = new
                    IntegraRevisaoService(_bdCont, _DBAccess, IntegracaoRevisaoAS);

                Logger.Info("Carregando desenhos encontrados.");
                await _IntegracaoRevisaoService.PushNewDrawsAsync();

                Logger.Info($"Integrando revisões de desenhos");

                using (var conn = new OracleConnection(Globals.ConnStr))
                {
                    conn.Open();
                    try
                    {
                       await _IntegracaoRevisaoService.AtualizaDesenhoRev(conn);
                    }
                    catch (Exception e)
                    {
                        Logger.Info($"Problema ao atualizar as revisões de desenhos");
                        Logger.Info($"Motivo: {e.Message}");
                    }
                }

                Logger.Info("Busca de desenho concluída com sucesso");
            }
            catch (Exception e)
            {
                Logger.Error("Problema ao buscar desenho");
                Logger.Error($"Motivo: {e.Message}");
            }
        }
        #endregion
        
        #endregion
    }
}

