﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Hino.Estoque.Infra.Cross.IoC;
using Hino.Estoque.Aplication.Services.Vendas;
using Hino.Estoque.Aplication.Interfaces.Vendas;

namespace Hino.Estoque.Service
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("pt-BR");
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("pt-BR");

            Globals.PathToAccessDataBase = ConfigurationManager.AppSettings["Path_to_AccessDataBase"];
            Globals.PathToHinoConexoes = ConfigurationManager.AppSettings["Path_to_HinoConexoes"];
            Globals.UidAccess = ConfigurationManager.AppSettings["Uid_AccessDataBase"];
            Globals.PwdAccess = ConfigurationManager.AppSettings["Pwd_AccessDataBase"];

            var services = new ServiceCollection();
            services.RegisterServicesConsole();

            var resolver = new DepResolver(services.BuildServiceProvider());

            var console = 0;
            if ((args.Length > 0) && (args[0].ToLower() == "-console"))
                console = 1;
            if (console == 1)
            {
                EstoqueService srv = new EstoqueService(resolver);
                srv.Console(args);
            }
            else
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
                {
                new EstoqueService(resolver)
                };
                ServiceBase.Run(ServicesToRun);
            }
        }
    }
}
