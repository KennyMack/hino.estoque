﻿using Hino.Estoque.Service.DataBase.Access;
using Hino.Estoque.Service.Entities.Access.Sales;
using Hino.Estoque.Service.Repositories.Interfaces.Sales;
using System.Threading.Tasks;
using Dapper;

namespace Hino.Estoque.Service.Repositories.Sales
{
    public class ItensRepository : BaseAccessRepository<TBLItens>, IItensRepository
    {
        private DBAccess _DBAccess;
        public ItensRepository(DBAccess dBAccess) :
            base(dBAccess)
        {
            _DBAccess = dBAccess;
        }

        public async Task UpdateIntegratedAsync(TBLItens model)
        {
            using (var db = _DBAccess.AbreConn())
            {

                await db.QueryAsync($"UPDATE Hino_PedItens SET Integrado = 1  WHERE OrderIDItem = {model.OrderIDItem}");
            }
        }
    }
}
