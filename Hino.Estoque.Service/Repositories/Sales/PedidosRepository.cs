﻿using Hino.Estoque.Service.DataBase.Access;
using Hino.Estoque.Service.Entities.Access.Sales;
using Hino.Estoque.Service.Repositories.Interfaces.Sales;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Service.Repositories.Sales
{
    public class PedidosRepository : BaseAccessRepository<TBLPedidos>, IPedidosRepository
    {
        private DBAccess _DBAccess;
        public PedidosRepository(DBAccess dBAccess) :
            base (dBAccess)
        {
            _DBAccess = dBAccess;
        }

        public async Task UpdateIntegratedAsync(TBLPedidos model)
        {
            using (var db = _DBAccess.AbreConn())
            {
                await db.QueryAsync($"UPDATE Hino_Pedidos SET Integrado = 1  WHERE OrderID = {model.OrderID}");
            }
        }
    }
}
