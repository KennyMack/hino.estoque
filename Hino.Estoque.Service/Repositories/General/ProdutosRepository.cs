﻿using Hino.Estoque.Service.DataBase.Access;
using Hino.Estoque.Service.Entities.Access.General;
using System.Threading.Tasks;
using Dapper;

namespace Hino.Estoque.Service.Repositories.Interfaces.General
{
    public class ProdutosRepository : BaseAccessRepository<TBLProdutos>, IProdutosRepository
    {
        private DBAccess _DBAccess;
        public ProdutosRepository(DBAccess dBAccess) :
            base(dBAccess)
        {
            _DBAccess = dBAccess;
        }

        public async Task UpdateIntegratedAsync(TBLProdutos model)
        {
            using (var db = _DBAccess.AbreConn())
            {
                await db.QueryAsync($"UPDATE Hino_Produtos SET Apagar = 1  WHERE PartNumber = '{model.PartNumber}'");
            }
        }
    }
}
