﻿using Hino.Estoque.Service.DataBase.Access;
using Hino.Estoque.Service.Entities.Access.General;
using Dapper;
using System.Threading.Tasks;

namespace Hino.Estoque.Service.Repositories.Interfaces.General
{
    public class ProdutoDetRepository : BaseAccessRepository<TBLProdutoDet>, IProdutoDetRepository
    {
        private DBAccess _DBAccess;
        public ProdutoDetRepository(DBAccess dBAccess) :
            base(dBAccess)
        {
            _DBAccess = dBAccess;
        }

        public async Task UpdateIntegratedAsync(TBLProdutoDet model)
        {
            using (var db = _DBAccess.AbreConn())
            {
                await db.QueryAsync($"UPDATE Hino_ProdutosDet SET Apagar = 1  WHERE PartNumber = '{model.PartNumber}'");
            }
        }
    }
}
