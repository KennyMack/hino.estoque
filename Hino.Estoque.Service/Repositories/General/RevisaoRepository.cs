﻿using Hino.Estoque.Service.DataBase.Access;
using Hino.Estoque.Service.Entities.Access.General;
using Hino.Estoque.Service.Repositories.Interfaces.General;

namespace Hino.Estoque.Service.Repositories.General
{
    public class RevisaoRepository : BaseAccessRepository<TBLRevisao>, IRevisaoRepository
    {
        private DBAccess _DBAccess;
        public RevisaoRepository(DBAccess dBAccess) :
            base(dBAccess)
        {
            _DBAccess = dBAccess;
        }
    }
}
