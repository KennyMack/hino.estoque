﻿using Hino.Estoque.Service.DataBase.Access;
using Hino.Estoque.Service.Entities.Access.General;
using System.Threading.Tasks;
using Dapper;

namespace Hino.Estoque.Service.Repositories.Interfaces.General
{
    public class EmpresaRepository : BaseAccessRepository<TBLEmpresa>, IEmpresaRepository
    {
        private DBAccess _DBAccess;
        public EmpresaRepository(DBAccess dBAccess) :
            base(dBAccess)
        {
            _DBAccess = dBAccess;
        }

        public async Task UpdateIntegratedAsync(TBLEmpresa model)
        {
            using (var db = _DBAccess.AbreConn())
            {
                await db.QueryAsync($"UPDATE Hino_ClientesTransp SET Apagar = 1  WHERE ID = {model.ID}");
            }
        }
    }
}
