﻿using Hino.Estoque.Service.Entities.Access.Sales;
using System.Threading.Tasks;

namespace Hino.Estoque.Service.Repositories.Interfaces.Sales
{
    public interface IItensRepository : IBaseAccessRepository<TBLItens>
    {
        Task UpdateIntegratedAsync(TBLItens model);
    }
}
