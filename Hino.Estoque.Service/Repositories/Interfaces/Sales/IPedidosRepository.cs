﻿using Hino.Estoque.Service.Entities.Access.Sales;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Service.Repositories.Interfaces.Sales
{
    public interface IPedidosRepository : IBaseAccessRepository<TBLPedidos>
    {
        Task UpdateIntegratedAsync(TBLPedidos model);
    }
}
