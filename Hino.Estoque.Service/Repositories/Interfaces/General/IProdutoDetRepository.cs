﻿using Hino.Estoque.Service.Entities.Access.General;
using System.Threading.Tasks;

namespace Hino.Estoque.Service.Repositories.Interfaces.General
{
    public interface IProdutoDetRepository : IBaseAccessRepository<TBLProdutoDet>
    {
        Task UpdateIntegratedAsync(TBLProdutoDet model);
    }
}
