﻿using Hino.Estoque.Service.Entities.Access.General;

namespace Hino.Estoque.Service.Repositories.Interfaces.General
{
    public interface IRevisaoRepository : IBaseAccessRepository<TBLRevisao>
    {
    }
}
