﻿using Hino.Estoque.Service.Entities.Access.General;
using System.Threading.Tasks;

namespace Hino.Estoque.Service.Repositories.Interfaces.General
{
    public interface IProdutosRepository : IBaseAccessRepository<TBLProdutos>
    {
        Task UpdateIntegratedAsync(TBLProdutos model);
    }
}
