﻿using Hino.Estoque.Infra.Cross.Utils.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Service.Repositories.Interfaces
{
    public interface IBaseAccessRepository<T> where T : class
    {
        void Add(T model);
        Task<PagedResult<T>> GetAllPagedAsync(int page, int pageSize);
        Task<IEnumerable<T>> GetAllAsync();
        Task<IEnumerable<T>> QueryAsync(Func<T, bool> predicate);
        Task<PagedResult<T>> QueryPagedAsync(int page, int pageSize, Func<T, bool> predicate);
        void Update(T model);
        void Remove(T model);
        long NextSequence();
        Task<long> NextSequenceAsync();
        void SaveChanges();
        void RollBackChanges();
        void Dispose();
    }
}
