﻿using Dapper;
using Dapper.Contrib.Extensions;
using Hino.Estoque.Infra.Cross.Utils.Paging;
using Hino.Estoque.Service.DataBase.Access;
using Hino.Estoque.Service.Entities.Access;
using Hino.Estoque.Service.Entities.Access.Sales;
using Hino.Estoque.Service.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Service.Repositories
{
    public class BaseAccessRepository<T> : IDisposable, IBaseAccessRepository<T> where T : BaseAccessEntity
    {
        private readonly DBAccess _DBAccess;
        public BaseAccessRepository(DBAccess dBAccess)
        {
            _DBAccess = dBAccess;
        }

        protected PagedResult<T> PaginateQuery(IEnumerable<T> query, int page, int pageSize)
        {
            var result = new PagedResult<T>
            {
                CurrentPage = page,
                PageSize = pageSize,
                RowCount = 10
            };

            var pageCount = (double)result.RowCount / pageSize;
            result.PageCount = (int)Math.Ceiling(pageCount);

            var skip = (page - 1) * pageSize;
            result.Results = query.ToList();
            return result;
        }

        public virtual void Add(T model)
        {
            using (var db = _DBAccess.AbreConn())
            {
                db.Insert(model);
            }
        }

        public virtual void Update(T model)
        {
            using (var db = _DBAccess.AbreConn())
            {
                db.Update(model);
            }
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            using (var db = _DBAccess.AbreConn())
            {
                var dt = await db.GetAllAsync<T>();

                return dt;
            }
        }

        public async Task<PagedResult<T>> GetAllPagedAsync(int page, int pageSize)
        {
            using (var db = _DBAccess.AbreConn())
            {
                var dt = await db.GetAllAsync<T>();

                return PaginateQuery(dt, page, pageSize);
            }
        }

        public async Task<IEnumerable<T>> QueryAsync(Func<T, bool> predicate)
        {
            using (var db = _DBAccess.AbreConn())
            {
                var dt = await db.GetAllAsync<T>();

                return dt.Where(predicate);
            }
        }

        public async Task<PagedResult<T>> QueryPagedAsync(int page, int pageSize,Func<T, bool> predicate)
        {
            using (var db = _DBAccess.AbreConn())
            {
                var dt = await db.GetAllAsync<T>();

                return PaginateQuery(dt.Where(predicate), page, pageSize);
            }
        }

        public void Remove(T model)
        {
            throw new NotImplementedException();
        }

        public void RollBackChanges()
        {
            _DBAccess.RollbackTransacao();
        }

        public void SaveChanges()
        {
            _DBAccess.CommitTransacao();
        }

        public long NextSequence()
        {
            throw new NotImplementedException();
        }

        public Task<long> NextSequenceAsync()
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            _DBAccess.FreeInstancia();
        }



        /*
        public List<TBLPedidos> Get()
        {
            var connection = new OdbcConnection(OdbcStrConn.ConnectionString);

            connection.Open();

            var query = "SELECT * FROM Tbl_Pedidos";

            var data = connection.GetAll<TBLPedidos>().Where(r => r.Data >= new DateTime(2019, 01, 01));

            connection.Close();

            return data.ToList();
        }*/

        public Task<PagedResult<T>> GetAllPagedAsync()
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<T>> QueryAsync(Expression<Func<T, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public Task<PagedResult<T>> QueryPagedAsync(int page, int pageSize, Expression<Func<T, bool>> predicate)
        {
            throw new NotImplementedException();
        }
    }
}
