﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Hino.Estoque.API.HttpUtils
{
    public class HttpActionResult : IHttpActionResult
    {
        private readonly string _message;
        private readonly HttpStatusCode _statusCode;

        public HttpActionResult(HttpStatusCode statusCode, string message)
        {
            _statusCode = statusCode;
            _message = message;
        }

        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            HttpResponseMessage response = new HttpResponseMessage(_statusCode)
            {
                Content = new StringContent(_message, System.Text.Encoding.UTF8, "application/json")
            };
            return Task.FromResult(response);
        }
    }

    public class HttpActionErrorResult : IHttpActionResult
    {
        private readonly string _message;
        private readonly HttpStatusCode _statusCode;

        public HttpActionErrorResult(HttpStatusCode statusCode, string message)
        {
            _statusCode = statusCode;
            _message = message;
        }

        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            var result = new System.Web.Mvc.JsonResult
            {
                Data = new
                {
                    status = _statusCode,
                    success = false,
                    data = new JObject(),
                    error = new JArray()
                    {
                        JObject.Parse(_message)
                    }
                },
                JsonRequestBehavior = System.Web.Mvc.JsonRequestBehavior.AllowGet
            };

            var message = new HttpResponseMessage()
            {
                Content = new StringContent(JsonConvert.SerializeObject(result, Formatting.Indented), Encoding.UTF8, "application/json"),
                ReasonPhrase = _statusCode.ToString(),
                StatusCode = _statusCode
            };

            message.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json")
            {
                CharSet = "utf-8"
            };
            return Task.FromResult(message);

        }
    }
}