﻿using Hino.Estoque.Aplication.Interfaces.Estoque;
using Hino.Estoque.Aplication.ViewModels.Estoque;
using Hino.Estoque.Infra.Cross.Utils;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Estoque.API.Controllers.Estoque
{
    [RoutePrefix("api/Estoque/Inventario/Estab/{pCodEstab}")]
    public class InventarioController : BaseController
    {
        private readonly IESInventarioAS _IESInventarioAS;
        private readonly IESInventDetAS _IESInventDetAS;

        public InventarioController(IESInventarioAS pIESInventarioAS,
            IESInventDetAS pIESInventDetAS)
        {
            _IESInventarioAS = pIESInventarioAS;
            _IESInventDetAS = pIESInventDetAS;
        }

        // GET: api/Inventario/Pendentes
        [Route("Pendentes")]
        [HttpGet]
        public async Task<IHttpActionResult> GetPendentes(short pCodEstab)
        {
            var Results = await _IESInventarioAS.GetInventariosPendentes(pCodEstab);

            return RequestOK(Results);
        }

        [Route("Inventario/Produto")]
        [HttpPost]
        public async Task<IHttpActionResult> PostInventarioItem(short pCodEstab, [FromBody]ESInventDetItemVM pDados)
        {
            _IESInventDetAS.Errors.Clear();
            ValidateModelState(pDados);

            if (!ModelState.IsValid)
                return InvalidRequest(pDados, ModelState);

            var Result = await _IESInventDetAS.GetInventarioItemAsync(pCodEstab, pDados);

            if (Result == null)
                return InvalidRequest(null, _IESInventDetAS.Errors);

            return RequestOK(Result);
        }

        [Route("Invent/{pCodInv}/Contagem/{pContagem}/Lista")]
        [HttpGet]
        public async Task<IHttpActionResult> GetInventario(short pCodEstab, decimal pCodInv, decimal pContagem)
        {
            _IESInventarioAS.Errors.Clear();
            var Result = await _IESInventarioAS.GetInventarioAsync(pCodEstab, pCodInv, pContagem);

            if (Result == null)
                return InvalidRequest(null, _IESInventarioAS.Errors);

            return RequestOK(Result);
        }

        // GET: api/Lista/Contagem
        [Route("Lista/{pCodInv}/Contagem/{pContagem}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetItensInventario(short pCodEstab, decimal pCodInv, decimal pContagem)
        {
            var Results = await _IESInventDetAS.GetItensInventario(pCodEstab, pCodInv, pContagem);

            return RequestOK(Results);
        }

        // POST: api/Lista/Contagem/Confirmar
        [Route("Lista/{pCodInv}/Contagem/{pContagem}/Confirmar")]
        [HttpPost]
        public async Task<IHttpActionResult> PostContagem(short pCodEstab, decimal pCodInv, decimal pContagem, [FromBody]ESUpdateInventDetVM[] lstItensDet)
        {
            _IESInventDetAS.Errors.Clear();
            ValidateModelState(lstItensDet);

            if (!ModelState.IsValid)
                return InvalidRequest(lstItensDet, ModelState);

            await _IESInventDetAS.ConfirmaContagemItens(pCodEstab, pCodInv, pContagem, lstItensDet);

            if (_IESInventDetAS.Errors.Count > 0)
                return InvalidRequest(null, _IESInventDetAS.Errors);

            return RequestOK(new { Result = Infra.Cross.Resources.MessagesResource.CountComplete });
        }

        [Route("Lista/{pCodInv}/Contagem/{pContagem}/Create")]
        public async Task<IHttpActionResult> PostCreateItem(short pCodEstab, long pCodInv, int pContagem, [FromBody]ESCreateItemInventarioVM pCreateItemInvent)
        {
            _IESInventarioAS.Errors.Clear();
            ValidateModelState(pCreateItemInvent);

            if (!ModelState.IsValid)
                return InvalidRequest(pCreateItemInvent, ModelState);

            pCreateItemInvent.codestab = pCodEstab;
            pCreateItemInvent.codinv = pCodInv;
            pCreateItemInvent.contagem = pContagem;

            var CreateItemResult = await _IESInventarioAS.CreateItemInventarioAsync(pCreateItemInvent);

            if (!_IESInventDetAS.Errors.Any() && (CreateItemResult?.Result?.IsEmpty() ?? false))
            {
                await _IESInventDetAS.ConfirmaContagemItens(pCodEstab, pCodInv, pContagem, new ESUpdateInventDetVM[] {
                    new ESUpdateInventDetVM
                    {
                        codestab = pCreateItemInvent.codestab,
                        codinv = pCodInv,
                        contagem = pContagem,
                        codestoque = pCreateItemInvent.codestoque,
                        codproduto = pCreateItemInvent.codproduto,
                        saldoinvent = pCreateItemInvent.quantidade,
                        codusuario = pCreateItemInvent.codusuario,
                        datacontagem = DateTime.Now
                    }
                });
            }

            if (_IESInventarioAS.Errors.Any())
                return InvalidRequest(null, _IESInventarioAS.Errors);

            return RequestOK(CreateItemResult);
        }
    }
}
