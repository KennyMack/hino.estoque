﻿using Hino.Estoque.Aplication.Interfaces.Estoque;
using Hino.Estoque.Aplication.ViewModels.Estoque;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Estoque.API.Controllers.Estoque
{
    [RoutePrefix("api/Estoque/Requisicoes/Estab/{pCodEstab}")]
    public class RequisicoesController : BaseController
    {
        private readonly IESRequisicaoAS _IESRequisicaoAS;
        private readonly IESKardexAS _IESKardexAS;

        public RequisicoesController(IESRequisicaoAS pIESRequisicaoAS,
            IESKardexAS pIESKardexAS)
        {
            _IESKardexAS = pIESKardexAS;
            _IESRequisicaoAS = pIESRequisicaoAS;
        }

        // GET: api/Requisicoes/Pendentes
        [Route("Pendentes")]
        [HttpGet]
        public async Task<IHttpActionResult> GetPendentes(short pCodEstab)
        {
            var dt = DateTime.Now.AddDays(-7).Date;
            var Results = await _IESRequisicaoAS.QueryAsync(r => 
            r.codestab == pCodEstab 
            && r.status == 0
            && r.data >= dt, 
            s => s.FSProduto, 
            t => t.FSProdutoparamEstab);

            return RequestOK(Results);
        }

        // GET: api/Requisicoes/Novo
        [Route("Novo")]
        [HttpPost]
        public async Task<IHttpActionResult> PostNovo(short pCodEstab, [FromBody]ESCreateRequisicaoVM CreateReq)
        {
            _IESRequisicaoAS.Errors.Clear();
            ValidateModelState(CreateReq);

            if (!ModelState.IsValid)
                return InvalidRequest(CreateReq, ModelState);

            var Results = await _IESRequisicaoAS.Create(CreateReq);

            if (Results == null || _IESRequisicaoAS.Errors.Any())
                return InvalidRequest(null, _IESRequisicaoAS.Errors);

            return RequestOK(Results);
        }

        // GET: api/Requisicoes/Novo
        [Route("Aprovar")]
        [HttpPost]
        public async Task<IHttpActionResult> PostAprovar(short pCodEstab, [FromBody] ESUpdateRequisicaoVM CreateReq)
        {
            _IESRequisicaoAS.Errors.Clear();
            ValidateModelState(CreateReq);

            if (!ModelState.IsValid)
                return InvalidRequest(CreateReq, ModelState);

            var Results = await _IESKardexAS.AprovarRequisicaoEstoqueAsync(CreateReq);

            if (Results == null || _IESKardexAS.Errors.Any())
                return InvalidRequest(null, _IESKardexAS.Errors);

            return RequestOK(Results);
        }

        // GET: api/Requisicoes/Reprovar
        [Route("Reprovar")]
        [HttpPost]
        public async Task<IHttpActionResult> PostReprovar(short pCodEstab, [FromBody] ESUpdateRequisicaoVM CreateReq)
        {
            _IESRequisicaoAS.Errors.Clear();
            ValidateModelState(CreateReq);

            if (!ModelState.IsValid)
                return InvalidRequest(CreateReq, ModelState);

            var Results = await _IESKardexAS.ReprovarRequisicaoEstoqueAsync(CreateReq);

            if (Results == null || _IESKardexAS.Errors.Any())
                return InvalidRequest(null, _IESKardexAS.Errors);

            return RequestOK(Results);
        }
    }
}