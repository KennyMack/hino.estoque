﻿using Hino.Estoque.Aplication.Interfaces.Estoque;
using Hino.Estoque.Aplication.Interfaces.Fiscal;
using Hino.Estoque.Aplication.Interfaces.Producao;
using Hino.Estoque.Aplication.ViewModels.Estoque;
using Hino.Estoque.Aplication.ViewModels.Producao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Estoque.API.Controllers.Estoque
{
    [RoutePrefix("api/Estoque/Transferencias/Estab/{pCodEstab}")]
    public class TransferenciasController : BaseController
    {
        private readonly IPDOPTransfAS _IPDOPTransfAS;
        private readonly IESKardexAS _IESKardexAS;
        private readonly IFSSaldoEstoqueAS _IFSSaldoEstoqueAS;

        public TransferenciasController(IPDOPTransfAS pIPDOPTransfAS,
            IESKardexAS pIESKardexAS,
            IFSSaldoEstoqueAS pIFSSaldoEstoqueAS)
        {
            _IPDOPTransfAS = pIPDOPTransfAS;
            _IESKardexAS = pIESKardexAS;
            _IFSSaldoEstoqueAS = pIFSSaldoEstoqueAS;
        }

        [Route("Ordem/Producao/Separar")]
        [HttpPost]
        public async Task<IHttpActionResult> PostBarras(short pCodEstab, [FromBody]PDOPCreateTransfVM[] lstTransf)
        {
            _IPDOPTransfAS.Errors.Clear();
            ValidateModelState(lstTransf);

            if (!ModelState.IsValid)
                return InvalidRequest(lstTransf, ModelState);

            var Result = await _IPDOPTransfAS.SeparateOPComponents(pCodEstab, lstTransf);

            Result.Result = (!Result.Success) ?
                Infra.Cross.Resources.MessagesResource.SeparateWithProblem :
                Infra.Cross.Resources.MessagesResource.SeparateSuccess;

            return RequestOK(Result);
        }
        
        [Route("Produto/Transferir")]
        [HttpPost]
        public async Task<IHttpActionResult> PostTransferir(short pCodEstab, [FromBody]ESCreateProdTransferenciaVM pCreateTranf)
        {
            _IPDOPTransfAS.Errors.Clear();
            ValidateModelState(pCreateTranf);

            if (!ModelState.IsValid)
                return InvalidRequest(pCreateTranf, ModelState);

            var Result = await _IESKardexAS.CreateProdTransferenciaAsync(pCreateTranf);

            if (!Result)
                return InvalidRequest(null, _IESKardexAS.Errors);

            return RequestOK(new { Result = Infra.Cross.Resources.MessagesResource.SeparateSuccess });
        }

        [Route("Endereco/Transferir")]
        [HttpPost]
        public async Task<IHttpActionResult> PostEnderecoTransferir(short pCodEstab, [FromBody] ESCreateProdEndTransfVM pCreateTranf)
        {
            _IPDOPTransfAS.Errors.Clear();
            ValidateModelState(pCreateTranf);

            if (!ModelState.IsValid)
                return InvalidRequest(pCreateTranf, ModelState);

            var Result = await _IESKardexAS.CreateProdEndTransferenciaAsync(pCreateTranf);

            if (!Result)
                return InvalidRequest(null, _IESKardexAS.Errors);

            return RequestOK(new { Result = Infra.Cross.Resources.MessagesResource.SeparateSuccess });
        }

        [Route("Produto/Saldo")]
        [HttpPost]
        public async Task<IHttpActionResult> PostSaldoEstoque([FromBody] ESBuscaSaldoEstoqueVM pBuscaSaldo)
        {
            _IPDOPTransfAS.Errors.Clear();
            ValidateModelState(pBuscaSaldo);

            if (!ModelState.IsValid)
                return InvalidRequest(pBuscaSaldo, ModelState);

            var Result = await _IFSSaldoEstoqueAS.QueryAsync(r => r.codestab == pBuscaSaldo.CodEstab && 
                r.codestoque == pBuscaSaldo.CodEstoque && 
                r.codproduto == pBuscaSaldo.CodProduto);

            return RequestOK(Result.FirstOrDefault());
        }
    }
}
 