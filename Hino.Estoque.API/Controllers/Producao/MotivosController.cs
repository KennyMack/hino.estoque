﻿using Hino.Estoque.Aplication.Interfaces.Producao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Hino.Estoque.API.Controllers.Producao
{
    [RoutePrefix("api/Producao/Motivos/{pCodEstab}")]
    public class MotivosController : BaseController
    {
        private readonly IPDMotivosAS _IPDMotivosAS;

        public MotivosController(IPDMotivosAS pIPDMotivosAS)
        {
            _IPDMotivosAS = pIPDMotivosAS;
        }

        [Route("Por/Tipo/{pType}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetMotivosAsync(short pCodEstab, short pType)
        {
            _IPDMotivosAS.Errors.Clear();

            var Results = await _IPDMotivosAS.QueryAsync(r => ((r.tipo == pType && pType > -1) || (pType <= -1)) && r.status);

            if (Results == null)
                return InvalidRequest(null, _IPDMotivosAS.Errors);

            return RequestOK(Results);
        }
    }
}