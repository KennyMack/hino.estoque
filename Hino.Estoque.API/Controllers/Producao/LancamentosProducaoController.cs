﻿using Hino.Estoque.Aplication.Interfaces.Producao;
using Hino.Estoque.Aplication.Interfaces.Producao.Apontamento;
using Hino.Estoque.Aplication.ViewModels.Producao.Apontamento;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Estoque.API.Controllers.Producao
{
    [RoutePrefix("api/Producao/Lancamentos/{pCodEstab}")]
    public class LancamentosProducaoController : BaseController
    {
        private readonly IPDAptInicioProdAS _IPDAptInicioProdAS;
        private readonly IPDAptTerminoProdAS _IPDAptTerminoProdAS;
        private readonly IPDOrdemProdAS _IPDOrdemProdAS;
        private readonly IPDSucataAS _IPDSucataAS;
        public LancamentosProducaoController(IPDAptInicioProdAS pIPDAptInicioProdAS,
            IPDAptTerminoProdAS pIPDAptTerminoProdAS,
            IPDSucataAS pIPDSucataAS,
            IPDOrdemProdAS pIPDOrdemProdAS)
        {
            _IPDOrdemProdAS = pIPDOrdemProdAS;
            _IPDAptInicioProdAS = pIPDAptInicioProdAS;
            _IPDAptTerminoProdAS = pIPDAptTerminoProdAS;
            _IPDSucataAS = pIPDSucataAS;
        }


        [Route("Maquinas")]
        [HttpGet]
        public async Task<IHttpActionResult> GetMachines(short pCodEstab) =>
            RequestOK(await _IPDOrdemProdAS.GetMachinesAsync(pCodEstab));

        [Route("Iniciar/Producao")]
        [HttpPost]
        public async Task<IHttpActionResult> PostIniciar(short pCodEstab, [FromBody] PDAptInicioProdCreateVM pCreateIni )
        {
            _IPDAptInicioProdAS.Errors.Clear();
            ValidateModelState(pCreateIni);

            if (!ModelState.IsValid)
                return InvalidRequest(pCreateIni, ModelState);

            var Results = await _IPDAptInicioProdAS.IniciarProducaoAsync(pCodEstab, pCreateIni);

            if (Results == null)
                return InvalidRequest(null, _IPDAptInicioProdAS.Errors);

            return RequestOK(Results);
        }

        [Route("Terminar/Producao")]
        [HttpPost]
        public async Task<IHttpActionResult> PostTermino(short pCodEstab, [FromBody] PDAptTerminoProdCreateVM pCreateTerm)
        {
            _IPDAptTerminoProdAS.Errors.Clear();
            ValidateModelState(pCreateTerm);

            if (!ModelState.IsValid)
                return InvalidRequest(pCreateTerm, ModelState);

            var Results = await _IPDAptTerminoProdAS.TerminarProducaoAsync(pCodEstab, pCreateTerm);

            if (Results == null)
                return InvalidRequest(null, _IPDAptTerminoProdAS.Errors);

            try
            {
                Results.askstartnew = Convert.ToBoolean(ConfigurationManager.AppSettings.Get("AskStartNew") ?? "false");
            }
            catch (Exception)
            {
            }

            return RequestOK(Results);
        }

        [Route("Terminar/Turno")]
        [HttpPost]
        public async Task<IHttpActionResult> PostTerminoTurno(short pCodEstab, [FromBody] PDAptTerminoProdCreateVM pCreateTerm)
        {
            _IPDAptTerminoProdAS.Errors.Clear();
            ValidateModelState(pCreateTerm);

            if (!ModelState.IsValid)
                return InvalidRequest(pCreateTerm, ModelState);

            var Results = await _IPDAptTerminoProdAS.TerminarTurnoProducaoAsync(pCodEstab, pCreateTerm);

            if (Results == null)
                return InvalidRequest(null, _IPDAptTerminoProdAS.Errors);

            try
            {
                Results.askstartnew = Convert.ToBoolean(ConfigurationManager.AppSettings.Get("AskStartNew") ?? "false");
            }
            catch (Exception)
            {
            }

            return RequestOK(Results);
        }

        [Route("Iniciar/Parada")]
        [HttpPost]
        public async Task<IHttpActionResult> PostIniciarParada(short pCodEstab, [FromBody] PDAptInicioProdCreateVM pCreateIni)
        {
            _IPDAptInicioProdAS.Errors.Clear();
            ValidateModelState(pCreateIni);

            if (!ModelState.IsValid)
                return InvalidRequest(pCreateIni, ModelState);

            var Results = await _IPDAptInicioProdAS.IniciarParadaAsync(pCodEstab, pCreateIni);

            if (Results == null)
                return InvalidRequest(null, _IPDAptInicioProdAS.Errors);

            return RequestOK(Results);
        }

        [Route("Terminar/Parada")]
        [HttpPost]
        public async Task<IHttpActionResult> PostTerminoParada(short pCodEstab, [FromBody] PDAptTerminoProdCreateVM pCreateTerm)
        {
            _IPDAptTerminoProdAS.Errors.Clear();
            ValidateModelState(pCreateTerm);

            if (!ModelState.IsValid)
                return InvalidRequest(pCreateTerm, ModelState);

            var Results = await _IPDAptTerminoProdAS.TerminarParadaAsync(pCodEstab, pCreateTerm);

            if (Results == null)
                return InvalidRequest(null, _IPDAptTerminoProdAS.Errors);

            return RequestOK(Results);
        }

        [Route("Apontar/Sucata")]
        [HttpPost]
        public async Task<IHttpActionResult> PostApontarSucata(short pCodEstab, [FromBody] PDSucataCreateVM pPDSucataCreate)
        {
            _IPDSucataAS.Errors.Clear();
            ValidateModelState(pPDSucataCreate);

            if (!ModelState.IsValid)
                return InvalidRequest(pPDSucataCreate, ModelState);

            var Results = await _IPDSucataAS.PostApontarSucata(pCodEstab, pPDSucataCreate);

            if (Results == null)
                return InvalidRequest(null, _IPDSucataAS.Errors);

            return RequestOK(Results);
        }

        [Route("Apontamentos/{pCodFuncionario}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetApontamentosAsync(short pCodEstab, int pCodFuncionario)
        {
            var Results = await _IPDAptTerminoProdAS.GetListApontamentoAsync(pCodEstab, pCodFuncionario);

            if (Results == null || _IPDAptTerminoProdAS.Errors.Any())
                return InvalidRequest(null, _IPDAptTerminoProdAS.Errors);

            return RequestOK(Results);
        }

        [Route("Buscar/Inicio/{pCodFuncionario}")]
        [HttpPost]
        public async Task<IHttpActionResult> GetApontamentoInicioAsync(short pCodEstab, int pCodFuncionario,
            [FromBody] PDAptBuscarInicioVM pBuscarInicio)
        {
            _IPDAptTerminoProdAS.Errors.Clear();
            ValidateModelState(pBuscarInicio);

            if (!ModelState.IsValid)
                return InvalidRequest(pBuscarInicio, ModelState);

            var Results = await _IPDAptInicioProdAS.GetLastAptStartAsync(
                pCodEstab, pCodFuncionario, pBuscarInicio.CodOrdProd,
                pBuscarInicio.NivelOrdProd, pBuscarInicio.CodEstrutura,
                pBuscarInicio.CodRoteiro, pBuscarInicio.Operacao, pBuscarInicio.Tipo);

            if (Results == null)
                return InvalidRequest(null, _IPDAptInicioProdAS.Errors);

            return RequestOK(Results);
        }

    }
}
