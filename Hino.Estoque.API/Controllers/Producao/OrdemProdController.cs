﻿using Hino.Estoque.Aplication.Interfaces.Producao;
using Hino.Estoque.Aplication.ViewModels.Producao;
using Hino.Estoque.Aplication.ViewModels.Producao.Apontamento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Estoque.API.Controllers.Producao
{
    [RoutePrefix("api/Producao/Ordem/Producao/{pCodEstab}")]
    public class OrdemProdController : BaseController
    {
        private readonly IPDOrdemProdAS _IPDOrdemProdAS;
        public OrdemProdController(IPDOrdemProdAS pIPDOrdemProdAS)
        {
            _IPDOrdemProdAS = pIPDOrdemProdAS;
        }

        // POST: api/Barras
        [Route("Barras/{pBarCode}")]
        [HttpPost]
        public async Task<IHttpActionResult> PostBarras(short pCodEstab, string pBarCode)
        {
            _IPDOrdemProdAS.Errors.Clear();

            var Results = await _IPDOrdemProdAS.GetOPByBarCode(pCodEstab, pBarCode);
            
            if (Results == null || _IPDOrdemProdAS.Errors.Any())
                return InvalidRequest(null, _IPDOrdemProdAS.Errors);

            return RequestOK(Results);
        }

        [Route("Componentes/Sucata")]
        [HttpPost]
        public async Task<IHttpActionResult> PostGetProductsToSuc(short pCodEstab, [FromBody]PDOPProductsSucVM pProductsSuc)
        {
            _IPDOrdemProdAS.Errors.Clear();
            ValidateModelState(pProductsSuc);

            if (!ModelState.IsValid)
                return InvalidRequest(pProductsSuc, ModelState);

            var Results = await _IPDOrdemProdAS.GetCompToSucAsync(pCodEstab, pProductsSuc.CodOrdProd, pProductsSuc.NivelOrdProd);

            if (Results == null || _IPDOrdemProdAS.Errors.Any())
                return InvalidRequest(null, _IPDOrdemProdAS.Errors);

            return RequestOK(Results);
        }

        [Route("Relatorio/Eficiencia")]
        [HttpPost]
        public async Task<IHttpActionResult> PostRelatorioEficienciaAsync(short pCodEstab, [FromBody]PDEficienciaProducaoVM pEficiencia)
        {
            _IPDOrdemProdAS.Errors.Clear();
            ValidateModelState(pEficiencia);

            if (!ModelState.IsValid)
                return InvalidRequest(pEficiencia, ModelState);

            var Results = await _IPDOrdemProdAS.BuscaEficienciaProducaoAsync(pCodEstab, pEficiencia.DataInicio, pEficiencia.DataTermino);

            if (Results == null || _IPDOrdemProdAS.Errors.Any())
                return InvalidRequest(null, _IPDOrdemProdAS.Errors);

            return RequestOK(Results);
        }
    }
}
