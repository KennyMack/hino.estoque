﻿using Hino.Estoque.Aplication.Interfaces.Producao.OEE;
using Hino.Estoque.Aplication.ViewModels.Producao.OEE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Estoque.API.Controllers.Producao
{
    [RoutePrefix("api/Producao/OEE/{pCodEstab}")]
    public class OEEController : BaseController
    {
        private readonly IPDOEEAS _IPDOEEAS;
        private readonly IPDOEEParamAS _IPDOEEParamAS;
        private readonly IPDOEEJustificaAS _IPDOEEJustificaAS;

        public OEEController(IPDOEEAS pIPDOEEAS,
            IPDOEEParamAS pIPDOEEParamAS,
            IPDOEEJustificaAS pIPDOEEJustificaAS)
        {
            _IPDOEEAS = pIPDOEEAS;
            _IPDOEEParamAS = pIPDOEEParamAS;
            _IPDOEEJustificaAS = pIPDOEEJustificaAS;
        }

        [Route("Gerar")]
        [HttpPost]
        public async Task<IHttpActionResult> Gerar(short pCodEstab, [FromBody] PDOEEGerarVM pGerar)
        {
            _IPDOEEAS.Errors.Clear();
            ValidateModelState(pGerar);

            if (!ModelState.IsValid)
                return InvalidRequest(pGerar, ModelState);

            var Results = await _IPDOEEAS.GerarOEEAsync(pGerar);

            if (!Results)
                return InvalidRequest(null, _IPDOEEAS.Errors);

            return RequestOK(new
            {
                result = Results
            },
            Results ? HttpStatusCode.OK : HttpStatusCode.BadRequest);
        }

        [Route("Gerar/Mensal")]
        [HttpPost]
        public async Task<IHttpActionResult> GerarMensal(short pCodEstab, [FromBody] PDOEEGerarVM pGerar)
        {
            _IPDOEEAS.Errors.Clear();
            ValidateModelState(pGerar);

            if (!ModelState.IsValid)
                return InvalidRequest(pGerar, ModelState);

            var Results = await _IPDOEEAS.GerarOEEMensalAsync(pGerar);

            if (!Results)
                return InvalidRequest(null, _IPDOEEAS.Errors);

            return RequestOK(new
            {
                result = Results
            },
            Results ? HttpStatusCode.OK : HttpStatusCode.BadRequest);
        }

        [Route("Listar")]
        [HttpPost]
        public async Task<IHttpActionResult> Listar(short pCodEstab, [FromBody] PDOEEListarVM pUsuario)
        {
            _IPDOEEAS.Errors.Clear();
            ValidateModelState(pUsuario);

            if (!ModelState.IsValid)
                return InvalidRequest(pUsuario, ModelState);

            var Results = await _IPDOEEAS.ListagemAsync(pCodEstab, pUsuario.codusuario);

            if (Results == null)
                return InvalidRequest(null, _IPDOEEAS.Errors);

            var Colors = _IPDOEEParamAS.GetOEEColorsByEstabAsync(pCodEstab);

            var Justificativas = _IPDOEEJustificaAS.GetOEEJustificativas(pCodEstab, pUsuario.codmaquina, pUsuario.dataini);

            return RequestOK(new {
                Results,
                Colors,
                Justificativas
            });
        }

        [Route("Listar/Mensal")]
        [HttpPost]
        public async Task<IHttpActionResult> ListarMensal(short pCodEstab, [FromBody] PDOEEListarVM pUsuario)
        {
            _IPDOEEAS.Errors.Clear();
            ValidateModelState(pUsuario);

            if (!ModelState.IsValid)
                return InvalidRequest(pUsuario, ModelState);

            var Results = await _IPDOEEAS.ListagemMensalAsync(pCodEstab, pUsuario.codusuario);

            if (Results == null)
                return InvalidRequest(null, _IPDOEEAS.Errors);

            var Colors = _IPDOEEParamAS.GetOEEColorsByEstabAsync(pCodEstab);

            return RequestOK(new
            {
                Results,
                Colors
            });
        }

        [Route("Search")]
        [HttpGet]
        public async Task<IHttpActionResult> Search(short pCodEstab) =>
            RequestOK(await _IPDOEEAS.SearchOEEAsync(pCodEstab));

    }
}
