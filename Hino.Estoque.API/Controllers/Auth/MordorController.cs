﻿using Hino.Estoque.Aplication.Interfaces.Gerais;
using Hino.Estoque.Aplication.ViewModels.Gerais;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Estoque.API.Controllers.Auth
{
    [RoutePrefix("api/Auth/Mordor")]
    public class MordorController : BaseController
    {
        private readonly IGEUsuariosAS _IGEUsuariosAS;

        public MordorController(IGEUsuariosAS pIGEUsuariosAS)
        {
            _IGEUsuariosAS = pIGEUsuariosAS;
        }
        
        // POST: api/Mordor/Login
        [Route("Login")]
        [HttpPost]
        public async Task<IHttpActionResult> Post([FromBody]GEUsuarioLoginVM value)
        {
            _IGEUsuariosAS.Errors.Clear();

            if (!ModelState.IsValid)
                return InvalidRequest(value, ModelState);

            var User = await _IGEUsuariosAS.GetByIdentifier((short)value.codestab, value.senha, value.identifier);

            if (_IGEUsuariosAS.Errors.Count > 0)
                return InvalidRequest(value, _IGEUsuariosAS.Errors);

            return RequestOK(User);
        }

        [Route("Test")]
        [HttpGet]
        public IHttpActionResult Test()
        {
            return RequestOK(new { Result = true });
        }
    }
}
