﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Net;
using System.Threading.Tasks;
using Hino.Estoque.Aplication.Interfaces.Manutencao;
using Hino.Estoque.Aplication.ViewModels.Manutencao;

namespace Hino.Estoque.API.Controllers.Manutencao
{
    [RoutePrefix("api/Manutencao/Motivos/{pCodEstab}")]
    public class MotivosManutencaoController : BaseController
    {
        private readonly IMNMotivoManutencaoAS _IMNMotivoManutencaoAS;

        public MotivosManutencaoController(IMNMotivoManutencaoAS pIMNMotivoManutencaoAS)
        {
            _IMNMotivoManutencaoAS = pIMNMotivoManutencaoAS;
        }

        [Route("Barras")]
        [HttpPost]
        public async Task<IHttpActionResult> GetBarrasAsync(short pCodEstab, [FromBody] MNSearchMotivoVM pSearchMotivo) =>
            RequestOK(await _IMNMotivoManutencaoAS.QueryAsync(r =>
                r.codmotivo == pSearchMotivo.codmotivo
            ));

        [Route("All")]
        [HttpGet]
        public async Task<IHttpActionResult> GetAll(short pCodEstab) =>
            RequestOK(await _IMNMotivoManutencaoAS.GetAllAsync());
    }
}