﻿using Hino.Estoque.Aplication.Interfaces.Manutencao;
using Hino.Estoque.Aplication.ViewModels.Manutencao;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Estoque.API.Controllers.Manutencao
{
    [RoutePrefix("api/Manutencao/Tipos/{pCodEstab}")]
    public class TiposManutencaoController : BaseController
    {
        private readonly IMNTipoManutencaoAS _IMNTipoManutencaoAS;

        public TiposManutencaoController(IMNTipoManutencaoAS pIMNTipoManutencaoAS)
        {
            _IMNTipoManutencaoAS = pIMNTipoManutencaoAS;
        }

        [Route("Barras")]
        [HttpPost]
        public async Task<IHttpActionResult> GetBarrasAsync(short pCodEstab, [FromBody] MNSearchTipoManutVM pSearchTipoManut) =>
            RequestOK(await _IMNTipoManutencaoAS.QueryAsync(r =>
                r.codtipomanut == pSearchTipoManut.codtipomanut
            ));

        [Route("All")]
        [HttpGet]
        public async Task<IHttpActionResult> GetAll(short pCodEstab) =>
            RequestOK(await _IMNTipoManutencaoAS.GetAllAsync());
    }
}
 