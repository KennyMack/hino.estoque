﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Net;
using System.Threading.Tasks;
using Hino.Estoque.Aplication.Interfaces.Manutencao;
using Hino.Estoque.Aplication.ViewModels.Manutencao;

namespace Hino.Estoque.API.Controllers.Manutencao
{
    [RoutePrefix("api/Manutencao/Equipamentos/{pCodEstab}")]
    public class EquipamentosController : BaseController
    {
        private readonly IMNEquipamentoAS _IMNEquipamentoAS;

        public EquipamentosController(IMNEquipamentoAS pIMNEquipamentoAS)
        {
            _IMNEquipamentoAS = pIMNEquipamentoAS;
        }

        [Route("Barras")]
        [HttpPost]
        public async Task<IHttpActionResult> GetBarrasAsync(short pCodEstab, [FromBody] MNSearchEquipamentoVM pEquipamento) =>
            RequestOK(await _IMNEquipamentoAS.QueryAsync(r =>
                r.codestab == pCodEstab &&
                r.codequipamento == pEquipamento.equipamento
            ));

        [Route("All")]
        [HttpGet]
        public async Task<IHttpActionResult> GetAll(short pCodEstab) => 
            RequestOK(await _IMNEquipamentoAS.QueryAsync(r =>
                r.codestab == pCodEstab &&
                r.status == 1
            ));
    }
}
