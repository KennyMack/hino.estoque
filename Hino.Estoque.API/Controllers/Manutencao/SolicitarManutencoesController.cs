﻿using Hino.Estoque.Aplication.Interfaces.Manutencao;
using Hino.Estoque.Aplication.ViewModels.Manutencao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Estoque.API.Controllers.Manutencao
{
    [RoutePrefix("api/Manutencao/Solicitar/{pCodEstab}")]
    public class SolicitarManutencoesController : BaseController
    {
        private readonly IMNSolicManutAS _IMNSolicManutAS;
        public SolicitarManutencoesController(IMNSolicManutAS pIMNSolicManutAS)
        {
            _IMNSolicManutAS = pIMNSolicManutAS;
        }

        [Route("Funcionario/{pCodFuncionario}/All")]
        [HttpPost]
        public async Task<IHttpActionResult> PostAllForUser(short pCodEstab, [FromBody] MNSolicForUserVM pUser)
        {
            var dt = DateTime.Now.AddDays(-7).Date;
            return RequestOK(await _IMNSolicManutAS.QueryAsync(r => 
                r.codestab == pCodEstab && 
                (r.datasolic >= dt || r.status == "P") &&
                (r.aprovador == pUser.codusuario || r.solicitante == pUser.codusuario),
                s => s.Equipamento,
                g => g.Motivo,
                t => t.TipoManut));
        }

        [Route("Id/{pCodSolic}")]
        [HttpGet]
        public async Task<IHttpActionResult> PostCreate(short pCodEstab, long pCodSolic) =>
            RequestOK(await _IMNSolicManutAS.QueryAsync(r =>
                r.codestab == pCodEstab &&
                r.codsolic == pCodSolic,
                s => s.Equipamento,
                g => g.Motivo,
                t => t.TipoManut));


        [Route("Novo")]
        [HttpPost]
        public async Task<IHttpActionResult> PostCreate(short pCodEstab, [FromBody] MNCreateSolicManutVM pSolicManut)
        {
            _IMNSolicManutAS.Errors.Clear();
            ValidateModelState(pSolicManut);

            if (!ModelState.IsValid)
                return InvalidRequest(pSolicManut, ModelState);

            var Results = await _IMNSolicManutAS.CreateAsync(pSolicManut);

            if (Results == null || _IMNSolicManutAS.Errors.Any())
                return InvalidRequest(null, _IMNSolicManutAS.Errors);

            return RequestOK(Results);
        }

        [Route("Aprovar")]
        [HttpPost]
        public async Task<IHttpActionResult> PostAprovarAsync(short pCodEstab, [FromBody] MNStatusSolicManutVM pStatus)
        {
            _IMNSolicManutAS.Errors.Clear();
            ValidateModelState(pStatus);

            if (!ModelState.IsValid)
                return InvalidRequest(pStatus, ModelState);

            var Results = await _IMNSolicManutAS.AlterarStatusSolicManutAsync(pStatus);

            if (Results == null || _IMNSolicManutAS.Errors.Any())
                return InvalidRequest(null, _IMNSolicManutAS.Errors);

            return RequestOK(Results);
        }

        [Route("Reprovar")]
        [HttpPost]
        public async Task<IHttpActionResult> PostReprovarAsync(short pCodEstab, [FromBody] MNStatusSolicManutVM pStatus)
        {
            _IMNSolicManutAS.Errors.Clear();
            ValidateModelState(pStatus);

            if (!ModelState.IsValid)
                return InvalidRequest(pStatus, ModelState);

            var Results = await _IMNSolicManutAS.AlterarStatusSolicManutAsync(pStatus);

            if (Results == null || _IMNSolicManutAS.Errors.Any())
                return InvalidRequest(null, _IMNSolicManutAS.Errors);

            return RequestOK(Results);
        }

        [Route("Search")]
        [HttpGet]
        public async Task<IHttpActionResult> GetDataSearchAsync(short pCodEstab) =>
            RequestOK(await _IMNSolicManutAS.SearchManutAsync(pCodEstab));
    }
}
