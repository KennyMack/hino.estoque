﻿using Hino.Estoque.Aplication.Interfaces.Gerais;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Estoque.API.Controllers.Gerais
{
    [RoutePrefix("api/Gerais/Estabelecimento")]
    public class EstabelecimentoController : BaseController
    {
        private readonly IGEEstabAS _IGEEstabAS;
        public EstabelecimentoController(IGEEstabAS pIGEEstabAS)
        {
            _IGEEstabAS = pIGEEstabAS;
        }

        [Route("All")]
        [HttpGet]
        public async Task<IHttpActionResult> GetEstabs() =>
            RequestOK(await _IGEEstabAS.QueryAsync(r => r.codestab > 0));

        [Route("{pCodEstab}/Configuracao")]
        [HttpGet]
        public async Task<IHttpActionResult> GetEstabSettings(short pCodEstab) =>
            RequestOK(await _IGEEstabAS.GetEstabAndSettingsAsync(pCodEstab));

        [Route("Configuracoes")]
        [HttpGet]
        public IHttpActionResult GetSettings() =>
            RequestOK(_IGEEstabAS.GetCurrentSettings());
    }
}
