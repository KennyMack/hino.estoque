﻿using Hino.Estoque.Aplication.Interfaces.Fiscal;
using Hino.Estoque.Aplication.ViewModels.Fiscal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Estoque.API.Controllers.Fiscal
{
    [RoutePrefix("api/Fiscal/Produtos/Estab/{pCodEstab}")]
    public class ProdutosController : BaseController
    {
        private readonly IFSProdutoAS _IFSProdutoAS;
        private readonly IFSSaldoEstoqueAS _IFSSaldoEstoqueAS;

        public ProdutosController(IFSProdutoAS pIFSProdutoAS,
            IFSSaldoEstoqueAS pIFSSaldoEstoqueAS)
        {
            _IFSProdutoAS = pIFSProdutoAS;
            _IFSSaldoEstoqueAS = pIFSSaldoEstoqueAS;
        }
        
        [Route("Barras")]
        [HttpPost]
        public async Task<IHttpActionResult> GetProductData(short pCodEstab, [FromBody]FSProdutoVM pProduto)
        {
            var Results = await _IFSProdutoAS.GetProductByBarCodeAsync(pCodEstab, pProduto.codproduto);

            return RequestOK(Results);
        }

        [Route("Saldos")]
        [HttpPost]
        public async Task<IHttpActionResult> GetProductStockBalance(short pCodEstab, [FromBody] FSProductStockBalanceVM pProduto)
        {
            ValidateModelState(pProduto);

            if (!ModelState.IsValid)
                return InvalidRequest(pProduto, ModelState);

            var Results = await _IFSSaldoEstoqueAS.QueryAsync(r => r.codestab == pCodEstab && r.codproduto == pProduto.CodProduto,
                x => x.FSParamLocEstoqEstab);

            return RequestOK(Results);
        }
    }
}
