﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using Hino.Estoque.API.App_Start;
using Hino.Estoque.API.Controllers.Auth;
using Hino.Estoque.API.Controllers.Estoque;
using Hino.Estoque.API.Controllers.Fiscal;
using Hino.Estoque.API.Controllers.Gerais;
using Hino.Estoque.API.Controllers.Manutencao;
using Hino.Estoque.API.Controllers.Producao;
using Hino.Estoque.API.Filter;
using Hino.Estoque.API.Handlers;
using Hino.Estoque.Infra.Cross.IoC;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Owin;

[assembly: OwinStartup(typeof(Hino.Estoque.API.Startup))]

namespace Hino.Estoque.API
{
    public class Startup
    {
        public static DepResolver DependencyResolver { get; private set; }
        public void Configuration(IAppBuilder app)
        {
            // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=316888

            // Enable cors
            app.UseCors(CorsOptions.AllowAll);

            // config WebApi
            var config = new HttpConfiguration();

            // Web API configuration and services
            config.Services.Replace(typeof(IExceptionLogger), new UnhandledExceptionHandler());

            // config routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApiEstoque",
                routeTemplate: "api/{controller}/{action}"
            );

            var services = new ServiceCollection();
            services.RegisterServices();

            services.AddTransient(typeof(MordorController));
            services.AddTransient(typeof(TransferenciasController));
            services.AddTransient(typeof(OrdemProdController));
            services.AddTransient(typeof(InventarioController));
            services.AddTransient(typeof(EstabelecimentoController));
            services.AddTransient(typeof(ProdutosController));
            services.AddTransient(typeof(LancamentosProducaoController));
            services.AddTransient(typeof(MotivosController));
            services.AddTransient(typeof(RequisicoesController));
            services.AddTransient(typeof(EquipamentosController));
            services.AddTransient(typeof(MotivosManutencaoController));
            services.AddTransient(typeof(SolicitarManutencoesController));
            services.AddTransient(typeof(TiposManutencaoController));
            services.AddTransient(typeof(OEEController));

            config.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling
                = Newtonsoft.Json.ReferenceLoopHandling.Ignore;

            var resolver = new DepResolver(services.BuildServiceProvider());
            config.DependencyResolver = resolver;
            DependencyResolver = resolver;

            config.Filters.Add(new NullModelStateActionFilter());
            config.Filters.Add(new ExceptionHandleFilter());

            // Enable webapi config
            app.UseWebApi(config);
        }
    }
}
