﻿using Hino.Estoque.API.HttpUtils;
using Hino.Estoque.Infra.Cross.Utils.Exceptions;
using System.Web.Http.Filters;

namespace Hino.Estoque.API.Filter
{
    public class ExceptionHandleFilter : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            var Messages = new[] { actionExecutedContext.Exception.Message,
                                   actionExecutedContext.Exception?.InnerException?.InnerException?.Message,
                                   System.Web.HttpUtility.JavaScriptStringEncode(actionExecutedContext.Exception.StackTrace) };


            HttpActionFilterResult.InvalidResult(
                actionExecutedContext.ActionContext,
                new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.UnhandledException,
                    Field = "",
                    Value = "",
                    Messages = Messages
                });

            base.OnException(actionExecutedContext);
        }
    }
}