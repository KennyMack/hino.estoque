﻿using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using Newtonsoft.Json.Linq;
using System.Net.Http.Headers;
using System.Text;
using Hino.Estoque.API.HttpUtils;

namespace Hino.Estoque.API.Filter
{
    public class NullModelStateActionFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (!actionContext.ActionDescriptor.GetCustomAttributes<NullableModelAttribute>().Any() &&
                actionContext.ActionArguments.ContainsValue(null))
            {
                HttpActionFilterResult.InvalidResult(actionContext,
                    new Infra.Cross.Utils.Exceptions.ModelException
                    {
                        ErrorCode = (int)Infra.Cross.Utils.Exceptions.EExceptionErrorCodes.InvalidRequest,
                        Field = "body",
                        Value = "body",
                        Messages = new[] { "Requisição inválida." }
                    }
                );
                return;
            }
        }
    }

    [AttributeUsage(AttributeTargets.Method, Inherited = true)]
    public class NullableModelAttribute : Attribute { }
}