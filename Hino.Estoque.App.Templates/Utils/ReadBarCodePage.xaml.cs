﻿using Hino.Estoque.App.Utils.Extensions;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Estoque.App.Templates.Utils
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ReadBarCodePage : Rg.Plugins.Popup.Pages.PopupPage
    {
        public EventHandler evtValueChanged;
        public EventHandler evtCloseWithoutBar;
        private bool _Iniciando;
        private System.Timers.Timer tmBusca;

        public ReadBarCodePage()
        {
            _Iniciando = true;
            InitializeComponent();
            tmBusca = new System.Timers.Timer(500);

            tmBusca.Enabled = true;
            tmBusca.Elapsed += TmBusca_Elapsed;
            tmBusca.Stop();
        }

        async Task valueChanged()
        {
            if (PopupNavigation.Instance.PopupStack.Any())
                await PopupNavigation.Instance.PopAsync();
            evtValueChanged?.Invoke(txtBarCode.Text, new EventArgs());
        }

        private async void TmBusca_Elapsed(object sender, ElapsedEventArgs e)
        {
            tmBusca.Stop();
            await valueChanged();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            if (txtBarCode.Text.IsEmpty())
                evtCloseWithoutBar?.Invoke("", new EventArgs());
        }

        protected override void OnAppearing()
        {
            txtBarCode.Text = "";
            txtBarCode.Focus();
            base.OnAppearing();
            _Iniciando = false;
        }

        private async void txtBarCode_Completed(object sender, EventArgs e) =>
            await valueChanged();

        private async void btnConfirm_Clicked(object sender, EventArgs e) =>
            await valueChanged();

        private void txtBarCode_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (txtBarCode.Text != "" && !_Iniciando)
            {
                tmBusca.Stop();
                tmBusca.Start();
            }
        }
    }
}