﻿using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XF.Material.Forms.UI;
using XF.Material.Forms.UI.Dialogs;
using XF.Material.Forms.UI.Internals;
using ZXing.Net.Mobile.Forms;

namespace Hino.Estoque.App.Templates.Utils
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BarcodeVCTpl : ContentView
    {
        public event EventHandler BarcodeReadResult;
        public event EventHandler BarcodeClosed;

        public bool ShowBarCode
        {
            get { return (bool)GetValue(ShowBarCodeProperty); }
            set { SetValue(ShowBarCodeProperty, value); }
        }

        public static readonly BindableProperty ShowBarCodeProperty =
            BindableProperty.Create(nameof(ShowBarCode), typeof(bool), typeof(BarcodeVCTpl));

        public BarcodeVCTpl()
        {
            InitializeComponent();
            _content.BindingContext = this;

            txtCamera.Tap(async (s, e) =>
                await ShowCameraView());
            txtBarcode.Tap(async (s, e) =>
                await ShowBarCodeView());
            txtQrCode.Tap(async (s, e) => 
                await ShowQRCodeView());
        }

        public async Task ShowBarCodeView()
        {
            var viewBarCode = new ReadBarCodePage();

            viewBarCode.evtValueChanged += (s, e) =>
            {
                var r = new ZXing.Result(s.ToString(), new byte[1], new ZXing.ResultPoint[1], ZXing.BarcodeFormat.EAN_13);
                BarcodeReadResult?.Invoke(r, null);
            };
            viewBarCode.evtCloseWithoutBar += (s, e) =>
            {
                BarcodeClosed?.Invoke(null, null);
            };

            await PopupNavigation.Instance.PushAsync(viewBarCode);
        }

        public async Task ShowQRCodeView()
        {
            var view = new ZXingScannerPage(new ZXing.Mobile.MobileBarcodeScanningOptions
            {
                TryHarder = true,
                PossibleFormats = new List<ZXing.BarcodeFormat>
                {
                    ZXing.BarcodeFormat.QR_CODE
                }
            });
            await Navigation.PushAsync(view);
            view.OnScanResult += (result) =>
            {
                view.IsScanning = false;
                Device.BeginInvokeOnMainThread(async () =>
                {
                    await Navigation.PopAsync();
                    BarcodeReadResult?.Invoke(result, null);
                });
            };
        }

        public async Task ShowCameraView()
        {
            var view = new ZXingScannerPage(new ZXing.Mobile.MobileBarcodeScanningOptions
            {
                TryHarder = true,
                PossibleFormats = new List<ZXing.BarcodeFormat>
                {
                    ZXing.BarcodeFormat.AZTEC,
                    ZXing.BarcodeFormat.CODABAR,
                    ZXing.BarcodeFormat.CODE_39,
                    ZXing.BarcodeFormat.CODE_93,
                    ZXing.BarcodeFormat.CODE_128,
                    ZXing.BarcodeFormat.DATA_MATRIX,
                    ZXing.BarcodeFormat.EAN_8,
                    ZXing.BarcodeFormat.EAN_13,
                    ZXing.BarcodeFormat.ITF,
                    ZXing.BarcodeFormat.MAXICODE,
                    ZXing.BarcodeFormat.PDF_417,
                    ZXing.BarcodeFormat.RSS_14,
                    ZXing.BarcodeFormat.RSS_EXPANDED,
                    ZXing.BarcodeFormat.UPC_A,
                    ZXing.BarcodeFormat.UPC_E,
                    ZXing.BarcodeFormat.All_1D,
                    ZXing.BarcodeFormat.UPC_EAN_EXTENSION,
                    ZXing.BarcodeFormat.MSI,
                    ZXing.BarcodeFormat.PLESSEY,
                    ZXing.BarcodeFormat.IMB,

                }
            });
            await Navigation.PushAsync(view);
            view.OnScanResult += (result) =>
            {
                view.IsScanning = false;

                Device.BeginInvokeOnMainThread(async () =>
                {
                    await Navigation.PopAsync();
                    BarcodeReadResult?.Invoke(result, null);
                });
            };
        }

        private void BarcodeScanView_OnScanResult(ZXing.Result result)
        {
            var rs = result;
        }
    }
}