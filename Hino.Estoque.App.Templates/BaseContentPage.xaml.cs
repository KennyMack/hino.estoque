﻿using Hino.Estoque.App.Utils.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Estoque.App.Templates
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BaseContentPage : ContentPage
    {
        #region Eventos
        public event EventHandler<AppearingEvent> AfterAppearing;
        protected void AfterAppearingRaise(AppearingEvent e) =>
            AfterAppearing?.Invoke(this, e);
        #endregion

        public BaseContentPage()
        {
            InitializeComponent();
        }
    }
}