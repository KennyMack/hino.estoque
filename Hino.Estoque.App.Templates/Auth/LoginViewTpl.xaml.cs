﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XF.Material.Forms.UI;
using XF.Material.Forms.UI.Dialogs;

namespace Hino.Estoque.App.Templates.Auth
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginViewTpl : ContentView
    {
        public string LBLUserPlaceholder
        {
            get { return (string)GetValue(LBLUserPlaceholderProperty); }
            set { SetValue(LBLUserPlaceholderProperty, value); }
        }

        public static readonly BindableProperty LBLUserPlaceholderProperty =
            BindableProperty.Create(nameof(LBLUserPlaceholder), typeof(string), typeof(LoginViewTpl));

        public string LBLUserHelperText
        {
            get { return (string)GetValue(LBLUserHelperTextProperty); }
            set { SetValue(LBLUserHelperTextProperty, value); }
        }

        public static readonly BindableProperty LBLUserHelperTextProperty =
            BindableProperty.Create(nameof(LBLUserHelperText), typeof(string), typeof(LoginViewTpl));

        public string LBLUserErrorText
        {
            get { return (string)GetValue(LBLUserErrorTextProperty); }
            set { SetValue(LBLUserErrorTextProperty, value); }
        }

        public static readonly BindableProperty LBLUserErrorTextProperty =
            BindableProperty.Create(nameof(LBLUserErrorText), typeof(string), typeof(LoginViewTpl));

        public bool UserHasError
        {
            get { return (bool)GetValue(UserHasErrorProperty); }
            set { SetValue(UserHasErrorProperty, value); }
        }

        public static readonly BindableProperty UserHasErrorProperty =
            BindableProperty.Create(nameof(UserHasError), typeof(bool), typeof(LoginViewTpl));

        public string UserText
        {
            get { return (string)GetValue(UserTextProperty); }
            set { SetValue(UserTextProperty, value); }
        }

        public static BindableProperty UserTextProperty =
                      BindableProperty.Create(nameof(UserText), typeof(string),
                      typeof(LoginViewTpl), defaultBindingMode: BindingMode.TwoWay);

        public string LBLUserPassPlaceholder
        {
            get { return (string)GetValue(LBLUserPassPlaceholderProperty); }
            set { SetValue(LBLUserPassPlaceholderProperty, value); }
        }

        public static readonly BindableProperty LBLUserPassPlaceholderProperty =
            BindableProperty.Create(nameof(LBLUserPassPlaceholder), typeof(string), typeof(LoginViewTpl));

        public string LBLUserPassHelperText
        {
            get { return (string)GetValue(LBLUserPassHelperTextProperty); }
            set { SetValue(LBLUserPassHelperTextProperty, value); }
        }

        public static readonly BindableProperty LBLUserPassHelperTextProperty =
            BindableProperty.Create(nameof(LBLUserPassHelperText), typeof(string), typeof(LoginViewTpl));

        public string LBLUserPassErrorText
        {
            get { return (string)GetValue(LBLUserPassErrorTextProperty); }
            set { SetValue(LBLUserPassErrorTextProperty, value); }
        }

        public static readonly BindableProperty LBLUserPassErrorTextProperty =
            BindableProperty.Create(nameof(LBLUserPassErrorText), typeof(string), typeof(LoginViewTpl));

        public bool UserPassHasError
        {
            get { return (bool)GetValue(UserPassHasErrorProperty); }
            set { SetValue(UserPassHasErrorProperty, value); }
        }

        public static readonly BindableProperty UserPassHasErrorProperty =
            BindableProperty.Create(nameof(UserPassHasError), typeof(bool), typeof(LoginViewTpl));

        public string UserPassText
        {
            get { return (string)GetValue(UserPassTextProperty); }
            set { SetValue(UserPassTextProperty, value); }
        }

        public static BindableProperty UserPassTextProperty =
                      BindableProperty.Create(nameof(UserPassText), typeof(string),
                      typeof(LoginViewTpl), defaultBindingMode: BindingMode.TwoWay);


        public bool ShowSelectEstab
        {
            get { return (bool)GetValue(ShowSelectEstabProperty); }
            set { SetValue(ShowSelectEstabProperty, value); }
        }

        public static readonly BindableProperty ShowSelectEstabProperty =
            BindableProperty.Create(nameof(ShowSelectEstab), typeof(bool), typeof(LoginViewTpl));

        public string EstabSelected
        {
            get { return (string)GetValue(EstabSelectedProperty); }
            set { SetValue(EstabSelectedProperty, value); }
        }

        public static BindableProperty EstabSelectedProperty =
                      BindableProperty.Create(nameof(EstabSelected), typeof(string),
                      typeof(LoginViewTpl));

        public bool ShowPassword
        {
            get { return (bool)GetValue(ShowPasswordProperty); }
            set { SetValue(ShowPasswordProperty, value); }
        }

        public static readonly BindableProperty ShowPasswordProperty =
            BindableProperty.Create(nameof(ShowPassword), typeof(bool), typeof(LoginViewTpl));


        public LoginViewTpl()
        {
            InitializeComponent();
            _content.BindingContext = this;
        }

        public event EventHandler BtnServidorClicked;
        private void BtnServidor_Clicked(object sender, EventArgs e) =>
            BtnServidorClicked?.Invoke(sender, e);

        public event EventHandler BtnLoginClicked;
        private void BtnEnter_Clicked(object sender, EventArgs e) =>
            BtnLoginClicked?.Invoke(sender, e);

        public event EventHandler BtnSelectEstab;
        private void BtnSelectEstab_Clicked(object sender, EventArgs e) =>
            BtnSelectEstab?.Invoke(sender, e);

        public event EventHandler UserLoginCompleted;
        private void TxtUser_Completed(object sender, EventArgs e)
        {
            if (!ShowPassword)
                UserLoginCompleted?.Invoke(sender, e);
            else
                txtUserPass.Focus();
        }

        private void TxtUserPass_Completed(object sender, EventArgs e)
        {
            if (ShowPassword)
                UserLoginCompleted?.Invoke(sender, e);
        }
    }
}