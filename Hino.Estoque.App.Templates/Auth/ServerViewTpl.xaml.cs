﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Estoque.App.Templates.Auth
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ServerViewTpl : ContentView
    {
        public string LBLServerAddressPlaceholder
        {
            get { return (string)GetValue(LBLServerAddressPlaceholderProperty); }
            set { SetValue(LBLServerAddressPlaceholderProperty, value); }
        }

        public static readonly BindableProperty LBLServerAddressPlaceholderProperty =
            BindableProperty.Create(nameof(LBLServerAddressPlaceholder), typeof(string), typeof(LoginViewTpl));

        public string LBLServerAddressHelperText
        {
            get { return (string)GetValue(LBLServerAddressHelperTextProperty); }
            set { SetValue(LBLServerAddressHelperTextProperty, value); }
        }

        public static readonly BindableProperty LBLServerAddressHelperTextProperty =
            BindableProperty.Create(nameof(LBLServerAddressHelperText), typeof(string), typeof(LoginViewTpl));

        public string LBLServerAddressErrorText
        {
            get { return (string)GetValue(LBLServerAddressErrorTextProperty); }
            set { SetValue(LBLServerAddressErrorTextProperty, value); }
        }

        public static readonly BindableProperty LBLServerAddressErrorTextProperty =
            BindableProperty.Create(nameof(LBLServerAddressErrorText), typeof(string), typeof(LoginViewTpl));

        public bool ServerAddressHasError
        {
            get { return (bool)GetValue(ServerAddressHasErrorProperty); }
            set { SetValue(ServerAddressHasErrorProperty, value); }
        }

        public static readonly BindableProperty ServerAddressHasErrorProperty =
            BindableProperty.Create(nameof(ServerAddressHasError), typeof(bool), typeof(LoginViewTpl));

        public string ServerAddressText
        {
            get { return (string)GetValue(ServerAddressTextProperty); }
            set { SetValue(ServerAddressTextProperty, value); }
        }

        public static BindableProperty ServerAddressTextProperty =
                      BindableProperty.Create(nameof(ServerAddressText), typeof(string),
                      typeof(LoginViewTpl), defaultBindingMode: BindingMode.TwoWay);
        
        public ServerViewTpl ()
		{
			InitializeComponent ();
            _content.BindingContext = this;
        }

        public event EventHandler BtnBackClicked;
        private void BtnBack_Clicked(object sender, EventArgs e) =>
            BtnBackClicked?.Invoke(sender, e);

        public event EventHandler BtnSaveClicked;
        private void BtnSave_Clicked(object sender, EventArgs e) =>
            BtnSaveClicked?.Invoke(sender, e);

        public event EventHandler ServerAddressCompleted;
        private void TxtServerAddress_Completed(object sender, EventArgs e) =>
            ServerAddressCompleted?.Invoke(sender, e);
    }
}