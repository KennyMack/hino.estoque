﻿using Hino.Estoque.App.Services.ViewModels.Main;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Estoque.App.Templates
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BaseTabbedPage : TabbedPage
    {
        public event EventHandler TitleChanged;
        private readonly MainTabbedVM _MainTabbedVM;
        protected Stack<Page> TabStack { get; private set; } = new Stack<Page>();

        public BaseTabbedPage()
        {
            InitializeComponent();
            this.BindingContext = _MainTabbedVM = new MainTabbedVM();
        }

        #region Update title
        private void UpdateTitle(Page pPage)
        {
            _MainTabbedVM.Title = pPage.Title;
            TitleChanged?.Invoke(_MainTabbedVM.Title, null);
        }
        #endregion

        protected override void OnCurrentPageChanged()
        {
            var page = CurrentPage;
            UpdateTitle(page);

            if (page != null && TabStack.Where(r => r.Title == page.Title).Count() <= 0)
                TabStack.Push(page);

            base.OnCurrentPageChanged();
        }

        protected override bool OnBackButtonPressed()
        {
            if (TabStack.Any())
                TabStack.Pop();

            if (TabStack.Any())
            {
                CurrentPage = TabStack.Pop();
                UpdateTitle(CurrentPage);

                return true;
            }

            return base.OnBackButtonPressed();
        }
    }
}