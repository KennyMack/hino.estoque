﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XF.Material.Forms.UI;

namespace Hino.Estoque.App.Templates.Producao
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OPComponentsStockVCTpl : ViewCell
    {
        public OPComponentsStockVCTpl()
        {
            InitializeComponent();
        }
    }
}