﻿using Hino.Estoque.App.Models.Producao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Estoque.App.Templates.Producao
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OPComponentsVCTpl : ViewCell
    {
        public OPComponentsVCTpl()
        {
            InitializeComponent();
        }

        public event EventHandler Quantity_Completed;
        private void TxtQuantity_Completed(object sender, EventArgs e)
        {
            decimal.TryParse(txtQuantity.Text, out decimal qtd);

            Quantity_Completed?.Invoke(new OPComponentsResult
            {
                IdMobile = Convert.ToInt64(lblIdMobile.Text),
                qtdseparada = qtd
            }, e);
        }

        private void TxtQuantity_Unfocused(object sender, FocusEventArgs e)
        {
            TxtQuantity_Completed(sender, null);
        }
    }
}