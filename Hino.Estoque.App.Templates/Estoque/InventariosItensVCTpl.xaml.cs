﻿using Hino.Estoque.App.Models.Estoque;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Estoque.App.Templates.Estoque
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class InventariosItensVCTpl : ViewCell
    {
        private decimal oldValue { get; set; }
        public InventariosItensVCTpl()
        {
            InitializeComponent();
        }

        public event EventHandler Quantity_Completed;
        private void TxtQuantity_Completed(object sender, EventArgs e)
        {
            decimal.TryParse(txtQuantity.Text, out decimal qtd);
            //if (oldValue != qtd)
            {
                Quantity_Completed?.Invoke(new InventDetResult
                {
                    IdMobile = Convert.ToInt64(lblIdMobile.Text),
                    codproduto = lblCodProduto.Text,
                    codinv = Convert.ToInt16(lblCodInv.Text),
                    contagem = Convert.ToDecimal(lblContagem.Text),
                    saldoinvent = qtd
                }, e);
            }
        }

        private void TxtQuantity_Unfocused(object sender, FocusEventArgs e)
        {
            // TxtQuantity_Completed(sender, null);
        }

        private void txtQuantity_Focused(object sender, FocusEventArgs e)
        {
            decimal.TryParse(txtQuantity.Text, out decimal qtd);
            oldValue = qtd;

        }
    }
}