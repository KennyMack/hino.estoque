﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Estoque.App.Templates.Estoque
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class InventariosVCTpl : ViewCell
	{
		public InventariosVCTpl ()
		{
			InitializeComponent ();
		}
	}
}