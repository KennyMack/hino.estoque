﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Estoque.App.Templates.Estoque
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RequisicoesItemVCTpl : ViewCell
    {

        public RequisicoesItemVCTpl()
        {
            InitializeComponent();
        }

        public event EventHandler btnAprovarClicked;
        private void btnAprovar_Clicked(object sender, EventArgs e) =>
            btnAprovarClicked?.Invoke(txtCodRequisicao.Text, e);

        public event EventHandler btnReprovarClicked;
        private void btnReprovar_Clicked(object sender, EventArgs e) =>
            btnReprovarClicked?.Invoke(txtCodRequisicao.Text, e);
    }
}