﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Hino.Estoque.App.Templates
{
    public static class ClickedEvent
    {
        public static void Tap (this View pInput, EventHandler pEventClicked)
        {
            if (pInput != null)
            {
                TapGestureRecognizer TapReco = new TapGestureRecognizer();
                TapReco.Tapped += async (s, e) =>
                {
                    await pInput.FadeTo(0.3, 300);
                    await pInput.FadeTo(1, 300);
                    pEventClicked.Invoke(pInput, e);
                };

                pInput.GestureRecognizers.Add(TapReco);
            }
        }
    }
}
