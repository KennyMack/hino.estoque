﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Estoque.App.Templates.Manutencao
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SolicManutItemCTpl : ViewCell
    {
        public SolicManutItemCTpl()
        {
            InitializeComponent();
        }
    }
}