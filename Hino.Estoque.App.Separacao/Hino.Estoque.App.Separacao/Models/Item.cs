﻿using System;

namespace Hino.Estoque.App.Separacao.Models
{
    public class Item
    {
        public string Id { get; set; }
        public string Text { get; set; }
        public string Description { get; set; }
    }
}