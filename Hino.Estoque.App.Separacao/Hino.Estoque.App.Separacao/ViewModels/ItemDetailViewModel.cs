﻿using System;

using Hino.Estoque.App.Separacao.Models;

namespace Hino.Estoque.App.Separacao.ViewModels
{
    public class ItemDetailViewModel : BaseViewModel
    {
        public Item Item { get; set; }
        public ItemDetailViewModel(Item item = null)
        {
            Title = item?.Text;
            Item = item;
        }
    }
}
