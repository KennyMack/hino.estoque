using Hino.Estoque.Domain.Fiscal.Interfaces.Repositories;
using Hino.Estoque.Domain.Fiscal.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Fiscal;
using Hino.Estoque.Domain.Base.Services;
using System.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System;

namespace Hino.Estoque.Domain.Fiscal.Services
{
    public class FSSaldoEstoqueService : BaseService<FSSaldoEstoque>, IFSSaldoEstoqueService
    {
        private readonly IFSSaldoEstoqueRepository _IFSSaldoEstoqueRepository;
        private readonly IFSParamLocEstoqEstabRepository _IFSParamLocEstoqEstabRepository;

        public FSSaldoEstoqueService(IFSSaldoEstoqueRepository pIFSSaldoEstoqueRepository,
            IFSParamLocEstoqEstabRepository pIFSParamLocEstoqEstabRepository) :
             base(pIFSSaldoEstoqueRepository)
        {
            _IFSParamLocEstoqEstabRepository = pIFSParamLocEstoqEstabRepository;
            _IFSSaldoEstoqueRepository = pIFSSaldoEstoqueRepository;
        }

        public async Task<ICollection<FSSaldoEstoque>> GetProdutoSaldos(short pCodEstab, string pCodProduto) =>
            (await _IFSSaldoEstoqueRepository.QueryAsync(r => r.codestab == pCodEstab && r.codproduto == pCodProduto)).ToList();

        public async Task<ICollection<FSSaldoEstoque>> GetProdutoSaldo(short pCodEstab, string pCodProduto, string pCodEstoque) =>
            (await _IFSSaldoEstoqueRepository.QueryAsync(r => r.codestab == pCodEstab && r.codproduto == pCodProduto && r.codestoque == pCodEstoque)).ToList();

        public async Task<double> SaldoEstoqueProduto(short pCodEstab, string pCodProduto, string pCodEstoque) =>
            await _IFSSaldoEstoqueRepository.SaldoEstoqueProduto(pCodEstab, pCodProduto, pCodEstoque);

    }
}
