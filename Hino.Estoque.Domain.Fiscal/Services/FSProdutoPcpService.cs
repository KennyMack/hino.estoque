using Hino.Estoque.Domain.Fiscal.Interfaces.Repositories;
using Hino.Estoque.Domain.Fiscal.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Fiscal;
using Hino.Estoque.Domain.Base.Services;

namespace Hino.Estoque.Domain.Fiscal.Services
{
    public class FSProdutoPcpService : BaseService<FSProdutoPcp>, IFSProdutoPcpService
    {
        private readonly IFSProdutoPcpRepository _IFSProdutoPcpRepository;

        public FSProdutoPcpService(IFSProdutoPcpRepository pIFSProdutoPcpRepository) : 
             base(pIFSProdutoPcpRepository)
        {
            _IFSProdutoPcpRepository = pIFSProdutoPcpRepository;
        }
    }
}
