﻿using Hino.Estoque.Domain.Base.Interfaces.Repositories;
using Hino.Estoque.Domain.Base.Interfaces.Services;
using Hino.Estoque.Domain.Base.Services;
using Hino.Estoque.Domain.Fiscal.Interfaces.Repositories;
using Hino.Estoque.Domain.Fiscal.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Fiscal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Domain.Fiscal.Services
{
    public class CZIntegrProdDetService : BaseService<CZIntegrProdDet>, ICZIntegrProdDetService
    {
        private readonly ICZIntegrProdDetRepository _ICZIntegrProdDetRepository;

        public CZIntegrProdDetService(ICZIntegrProdDetRepository pCZIntegrProdDetRepository) :
             base(pCZIntegrProdDetRepository)
        {
            _ICZIntegrProdDetRepository = pCZIntegrProdDetRepository;
        }
    }
}
