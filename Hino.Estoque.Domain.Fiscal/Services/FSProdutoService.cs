using Hino.Estoque.Domain.Fiscal.Interfaces.Repositories;
using Hino.Estoque.Domain.Fiscal.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Fiscal;
using Hino.Estoque.Domain.Base.Services;
using System.Threading.Tasks;

namespace Hino.Estoque.Domain.Fiscal.Services
{
    public class FSProdutoService : BaseService<FSProduto>, IFSProdutoService
    {
        private readonly IFSProdutoRepository _IFSProdutoRepository;

        public FSProdutoService(IFSProdutoRepository pIFSProdutoRepository) : 
             base(pIFSProdutoRepository)
        {
            _IFSProdutoRepository = pIFSProdutoRepository;
        }
        public async Task<FSProduto> GetProductByBarCodeAsync(short pCodEstab, string pBarCode) =>
            await _IFSProdutoRepository.GetProductByBarCodeAsync(pCodEstab, pBarCode);

    }
}
