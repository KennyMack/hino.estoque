using Hino.Estoque.Domain.Fiscal.Interfaces.Repositories;
using Hino.Estoque.Domain.Fiscal.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Fiscal;
using Hino.Estoque.Domain.Base.Services;
using System.Threading.Tasks;

namespace Hino.Estoque.Domain.Fiscal.Services
{
    public class FSProdutoparamEstabService : BaseService<FSProdutoparamEstab>, IFSProdutoparamEstabService
    {
        private readonly IFSProdutoparamEstabRepository _IFSProdutoparamEstabRepository;

        public FSProdutoparamEstabService(IFSProdutoparamEstabRepository pIFSProdutoparamEstabRepository) : 
             base(pIFSProdutoparamEstabRepository)
        {
            _IFSProdutoparamEstabRepository = pIFSProdutoparamEstabRepository;
        }
    }
}
