using Hino.Estoque.Domain.Fiscal.Interfaces.Repositories;
using Hino.Estoque.Domain.Fiscal.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Fiscal;
using Hino.Estoque.Domain.Base.Services;

namespace Hino.Estoque.Domain.Fiscal.Services
{
    public class FSLocalEstoqueService : BaseService<FSLocalEstoque>, IFSLocalEstoqueService
    {
        private readonly IFSLocalEstoqueRepository _IFSLocalEstoqueRepository;

        public FSLocalEstoqueService(IFSLocalEstoqueRepository pIFSLocalEstoqueRepository) : 
             base(pIFSLocalEstoqueRepository)
        {
            _IFSLocalEstoqueRepository = pIFSLocalEstoqueRepository;
        }
    }
}
