using Hino.Estoque.Infra.Cross.Entities.Fiscal;
using Hino.Estoque.Domain.Base.Interfaces.Repositories;
using System.Threading.Tasks;

namespace Hino.Estoque.Domain.Fiscal.Interfaces.Repositories
{
    public interface IFSSaldoEstoqueRepository : IBaseRepository<FSSaldoEstoque>
    {
        Task<double> SaldoEstoqueProduto(short pCodEstab, string pCodProduto, string pCodEstoque);
    }
}
