﻿using Hino.Estoque.Domain.Base.Interfaces.Repositories;
using Hino.Estoque.Infra.Cross.Entities.Fiscal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Domain.Fiscal.Interfaces.Repositories
{
    public interface ICZIntegProdutosRepository : IBaseRepository<CZIntegProdutos>
    {
    }
}
