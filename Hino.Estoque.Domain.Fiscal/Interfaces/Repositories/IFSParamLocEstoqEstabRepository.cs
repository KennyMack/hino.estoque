﻿using Hino.Estoque.Domain.Base.Interfaces.Repositories;
using Hino.Estoque.Infra.Cross.Entities.Fiscal;

namespace Hino.Estoque.Domain.Fiscal.Interfaces.Repositories
{
    public interface IFSParamLocEstoqEstabRepository : IBaseRepository<FSParamLocEstoqEstab>
    {
    }
}
