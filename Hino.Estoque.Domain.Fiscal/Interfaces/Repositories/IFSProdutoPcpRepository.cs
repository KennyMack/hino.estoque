using Hino.Estoque.Infra.Cross.Entities.Fiscal;
using Hino.Estoque.Domain.Base.Interfaces.Repositories;

namespace Hino.Estoque.Domain.Fiscal.Interfaces.Repositories
{
    public interface IFSProdutoPcpRepository : IBaseRepository<FSProdutoPcp>
    {
    }
}
