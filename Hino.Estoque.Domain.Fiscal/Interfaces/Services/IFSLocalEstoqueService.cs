using Hino.Estoque.Infra.Cross.Entities.Fiscal;
using Hino.Estoque.Domain.Base.Interfaces.Services;

namespace Hino.Estoque.Domain.Fiscal.Interfaces.Services
{
    public interface IFSLocalEstoqueService : IBaseService<FSLocalEstoque>
    {
    }
}
