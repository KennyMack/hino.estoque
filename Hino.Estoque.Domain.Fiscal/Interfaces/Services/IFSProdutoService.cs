using Hino.Estoque.Infra.Cross.Entities.Fiscal;
using Hino.Estoque.Domain.Base.Interfaces.Services;
using System.Threading.Tasks;

namespace Hino.Estoque.Domain.Fiscal.Interfaces.Services
{
    public interface IFSProdutoService : IBaseService<FSProduto>
    {
        Task<FSProduto> GetProductByBarCodeAsync(short pCodEstab, string pBarCode);
    }
}
