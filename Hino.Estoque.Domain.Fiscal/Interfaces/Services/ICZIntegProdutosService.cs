﻿using Hino.Estoque.Domain.Base.Interfaces.Repositories;
using Hino.Estoque.Domain.Base.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Fiscal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Domain.Fiscal.Interfaces.Services
{
    public interface ICZIntegProdutosService : IBaseService<CZIntegProdutos>
    {
    }
}
