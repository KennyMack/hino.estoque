using Hino.Estoque.Infra.Cross.Entities.Fiscal;
using Hino.Estoque.Domain.Base.Interfaces.Services;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Hino.Estoque.Domain.Fiscal.Interfaces.Services
{
    public interface IFSSaldoEstoqueService : IBaseService<FSSaldoEstoque>
    {
        Task<ICollection<FSSaldoEstoque>> GetProdutoSaldos(short pCodEstab, string pCodProduto);
        Task<ICollection<FSSaldoEstoque>> GetProdutoSaldo(short pCodEstab, string pCodProduto, string pCodEstoque);
        Task<double> SaldoEstoqueProduto(short pCodEstab, string pCodProduto, string pCodEstoque);
    }
}
