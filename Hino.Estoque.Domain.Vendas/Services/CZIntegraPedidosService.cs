﻿using Hino.Estoque.Domain.Base.Services;
using Hino.Estoque.Domain.Vendas.Interfaces.Repositories;
using Hino.Estoque.Domain.Vendas.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Vendas;
using System;

namespace Hino.Estoque.Domain.Vendas.Services
{
    public class CZIntegraPedidosService : BaseService<CZIntegraPedidos>, ICZIntegraPedidosService
    {
        private readonly ICZIntegraPedidosRepository _ICZIntegraPedidosRepository;

        public CZIntegraPedidosService(ICZIntegraPedidosRepository repo) : base(repo)
        {
            _ICZIntegraPedidosRepository = repo;
        }
    }
}
