﻿using Hino.Estoque.Domain.Base.Services;
using Hino.Estoque.Domain.Vendas.Interfaces.Repositories;
using Hino.Estoque.Domain.Vendas.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Vendas;
using System;

namespace Hino.Estoque.Domain.Vendas.Services
{
    public class CZIntegPedItensService : BaseService<CZIntegPedItens>, ICZIntegPedItensService
    {
        private readonly ICZIntegPedItensRepository _ICZIntegPedItensRepository;

        public CZIntegPedItensService(ICZIntegPedItensRepository repo) : base(repo)
        {
            _ICZIntegPedItensRepository = repo;
        }
    }
}
