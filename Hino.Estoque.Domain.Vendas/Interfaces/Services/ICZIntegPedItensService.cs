﻿using Hino.Estoque.Domain.Base.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Vendas;
using System;

namespace Hino.Estoque.Domain.Vendas.Interfaces.Services
{
    public interface ICZIntegPedItensService : IBaseService<CZIntegPedItens>
    {
    }
}
