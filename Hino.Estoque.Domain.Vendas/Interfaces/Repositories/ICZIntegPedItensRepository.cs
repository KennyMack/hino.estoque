﻿using Hino.Estoque.Domain.Base.Interfaces.Repositories;
using Hino.Estoque.Infra.Cross.Entities.Vendas;

namespace Hino.Estoque.Domain.Vendas.Interfaces.Repositories
{
    public interface ICZIntegPedItensRepository : IBaseRepository<CZIntegPedItens>
    {
    }
}
