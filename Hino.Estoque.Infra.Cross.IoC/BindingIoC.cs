﻿using Hino.Estoque.Aplication.Interfaces.Engenharia;
using Hino.Estoque.Aplication.Interfaces.Estoque;
using Hino.Estoque.Aplication.Interfaces.Fiscal;
using Hino.Estoque.Aplication.Interfaces.Gerais;
using Hino.Estoque.Aplication.Interfaces.Manutencao;
using Hino.Estoque.Aplication.Interfaces.Producao;
using Hino.Estoque.Aplication.Interfaces.Producao.Apontamento;
using Hino.Estoque.Aplication.Interfaces.Producao.OEE;
using Hino.Estoque.Aplication.Interfaces.Vendas;
using Hino.Estoque.Aplication.Services.Engenharia;
using Hino.Estoque.Aplication.Services.Estoque;
using Hino.Estoque.Aplication.Services.Fiscal;
using Hino.Estoque.Aplication.Services.Gerais;
using Hino.Estoque.Aplication.Services.Manutencao;
using Hino.Estoque.Aplication.Services.Producao;
using Hino.Estoque.Aplication.Services.Producao.Apontamento;
using Hino.Estoque.Aplication.Services.Producao.OEE;
using Hino.Estoque.Aplication.Services.Vendas;
using Hino.Estoque.Domain.Base.Interfaces.UoW;
using Hino.Estoque.Domain.Engenharia.Interfaces.Repositories;
using Hino.Estoque.Domain.Engenharia.Interfaces.Services;
using Hino.Estoque.Domain.Engenharia.Services;
using Hino.Estoque.Domain.Estoque.Interfaces.Repositories.Estoque;
using Hino.Estoque.Domain.Estoque.Interfaces.Services.Estoque;
using Hino.Estoque.Domain.Estoque.Services.Estoque;
using Hino.Estoque.Domain.Fiscal.Interfaces.Repositories;
using Hino.Estoque.Domain.Fiscal.Interfaces.Services;
using Hino.Estoque.Domain.Fiscal.Services;
using Hino.Estoque.Domain.Gerais.Interfaces.Repositories;
using Hino.Estoque.Domain.Gerais.Interfaces.Services;
using Hino.Estoque.Domain.Gerais.Services;
using Hino.Estoque.Domain.Manutencao.Interfaces.Repositories;
using Hino.Estoque.Domain.Manutencao.Interfaces.Services;
using Hino.Estoque.Domain.Manutencao.Services;
using Hino.Estoque.Domain.Producao.Interfaces.Repositories;
using Hino.Estoque.Domain.Producao.Interfaces.Repositories.Apontamento;
using Hino.Estoque.Domain.Producao.Interfaces.Repositories.OEE;
using Hino.Estoque.Domain.Producao.Interfaces.Services;
using Hino.Estoque.Domain.Producao.Interfaces.Services.Apontamento;
using Hino.Estoque.Domain.Producao.Interfaces.Services.OEE;
using Hino.Estoque.Domain.Producao.Services;
using Hino.Estoque.Domain.Producao.Services.Apontamento;
using Hino.Estoque.Domain.Producao.Services.OEE;
using Hino.Estoque.Domain.Vendas.Interfaces.Repositories;
using Hino.Estoque.Domain.Vendas.Interfaces.Services;
using Hino.Estoque.Domain.Vendas.Services;
using Hino.Estoque.Infra.Data.DataBase.Context;
using Hino.Estoque.Infra.Data.DataBase.Repositories.Engenharia;
using Hino.Estoque.Infra.Data.DataBase.Repositories.Estoque;
using Hino.Estoque.Infra.Data.DataBase.Repositories.Fiscal;
using Hino.Estoque.Infra.Data.DataBase.Repositories.Gerais;
using Hino.Estoque.Infra.Data.DataBase.Repositories.Manutencao;
using Hino.Estoque.Infra.Data.DataBase.Repositories.Producao;
using Hino.Estoque.Infra.Data.DataBase.Repositories.Producao.Apontamento;
using Hino.Estoque.Infra.Data.DataBase.Repositories.Producao.OEE;
using Hino.Estoque.Infra.Data.DataBase.Repositories.Vendas;
using Hino.Estoque.Infra.Data.DataBase.UoW;
using Hino.Estoque.Infra.DataBase.Repositories.Estoque;
using Hino.Estoque.Infra.DataBase.Repositories.Fiscal;
using Hino.Estoque.Infra.DataBase.Repositories.Gerais;
using Hino.Estoque.Infra.DataBase.Repositories.Producao;
using Hino.Estoque.Infra.DataBase.Repositories.Producao.Apontamento;
using Microsoft.Extensions.DependencyInjection;

namespace Hino.Estoque.Infra.Cross.IoC
{
    public static class BindingIoC
    {
        
        private static void RegisterContext(IServiceCollection services, bool Api)
        {
            if (Api)
            {
                services.AddScoped<IUnitOfWork, UnitOfWork>();
                services.AddTransient<AppDbContext>();

            }
            else
            {
                services.AddScoped<IUnitOfWork, UnitOfWork>();
                services.AddTransient<AppDbContext>();

            }
        }

        private static void IOCServices(this IServiceCollection services)
        {
            services.AddTransient<IESInventarioRepository, ESInventarioRepository>();
            services.AddTransient<IESInventDetRepository, ESInventDetRepository>();
            services.AddTransient<IESInventLoteRepository, ESInventLoteRepository>();
            services.AddTransient<IESInventTercRepository, ESInventTercRepository>();
            services.AddTransient<IESKardexRepository, ESKardexRepository>();
            services.AddTransient<IESLoteRepository, ESLoteRepository>();
            services.AddTransient<IESLoteSaldoRepository, ESLoteSaldoRepository>();
            services.AddTransient<IESTransfDetRepository, ESTransfDetRepository>();
            services.AddTransient<IESTransferenciaRepository, ESTransferenciaRepository>();
            services.AddTransient<IESTransfLoteRepository, ESTransfLoteRepository>();
            services.AddTransient<IFSLocalEstoqueRepository, FSLocalEstoqueRepository>();
            services.AddTransient<IFSProdutoRepository, FSProdutoRepository>();
            services.AddTransient<IFSProdutoparamEstabRepository, FSProdutoparamEstabRepository>();
            services.AddTransient<IFSProdutoPcpRepository, FSProdutoPcpRepository>();
            services.AddTransient<IFSSaldoEstoqueRepository, FSSaldoEstoqueRepository>();
            services.AddTransient<IGEEstabRepository, GEEstabRepository>();
            services.AddTransient<IGEFuncionariosRepository, GEFuncionariosRepository>();
            services.AddTransient<IGELogRepository, GELogRepository>();
            services.AddTransient<IGELogDetRepository, GELogDetRepository>();
            services.AddTransient<IGEUsuariosRepository, GEUsuariosRepository>();
            services.AddTransient<IPDLancamentosRepository, PDLancamentosRepository>();
            services.AddTransient<IPDOPTransfRepository, PDOPTransfRepository>();
            services.AddTransient<IPDOPTransfLoteRepository, PDOPTransfLoteRepository>();
            services.AddTransient<IPDOrdemProdRepository, PDOrdemProdRepository>();
            services.AddTransient<IPDOrdemProdCompRepository, PDOrdemProdCompRepository>();
            services.AddTransient<IPDOrdemProdRotinasRepository, PDOrdemProdRotinasRepository>();
            services.AddTransient<IPDParamEstabRepository, PDParamEstabRepository>();
            services.AddTransient<ICZIntegPedItensRepository, CZIntegPedItensRepository>();
            services.AddTransient<ICZIntegraPedidosRepository, CZIntegraPedidosRepository>();
            services.AddTransient<ICZIntegProdutosRepository, CZIntegProdutosRepository>();
            services.AddTransient<ICZIntegrProdDetRepository, CZIntegrProdDetRepository>();
            services.AddTransient<ICZIntegrEmpresaRepository, CZIntegrEmpresaRepository>();
            services.AddTransient<ICZIntegracaoRevisaoRepository, CZIntegracaoRevisaoRepository>();
            services.AddTransient<IPDAptInicioProdRepository, PDAptInicioProdRepository>();
            services.AddTransient<IPDAptTerminoProdRepository, PDAptTerminoProdRepository>();
            services.AddTransient<IPDMotivosRepository, PDMotivosRepository>();
            services.AddTransient<IPDAptLancamentoRepository, PDAptLancamentoRepository>();
            services.AddTransient<IPDParadasRepository, PDParadasRepository>();
            services.AddTransient<IPDAptParadasRepository, PDAptParadasRepository>();
            services.AddTransient<IPDSucataRepository, PDSucataRepository>();
            services.AddTransient<IESRequisicaoRepository, ESRequisicaoRepository>();
            services.AddTransient<IPDOEERepository, PDOEERepository>();
            services.AddTransient<IESProdEnderecRepository, ESProdEnderecRepository>();
            services.AddTransient<IESEnderecamentoRepository, ESEnderecamentoRepository>();
            services.AddTransient<IFSParamLocEstoqEstabRepository, FSParamLocEstoqEstabRepository>();

            services.AddTransient<IESProdEnderecService, ESProdEnderecService>();
            services.AddTransient<IESEnderecamentoService, ESEnderecamentoService>();
            services.AddTransient<IESInventarioService, ESInventarioService>();
            services.AddTransient<IESInventDetService, ESInventDetService>();
            services.AddTransient<IESInventLoteService, ESInventLoteService>();
            services.AddTransient<IESInventTercService, ESInventTercService>();
            services.AddTransient<IESKardexService, ESKardexService>();
            services.AddTransient<IESLoteService, ESLoteService>();
            services.AddTransient<IESLoteSaldoService, ESLoteSaldoService>();
            services.AddTransient<IESTransfDetService, ESTransfDetService>();
            services.AddTransient<IESTransferenciaService, ESTransferenciaService>();
            services.AddTransient<IESTransfLoteService, ESTransfLoteService>();
            services.AddTransient<IFSLocalEstoqueService, FSLocalEstoqueService>();
            services.AddTransient<IFSProdutoService, FSProdutoService>();
            services.AddTransient<IFSProdutoparamEstabService, FSProdutoparamEstabService>();
            services.AddTransient<IFSProdutoPcpService, FSProdutoPcpService>();
            services.AddTransient<IFSSaldoEstoqueService, FSSaldoEstoqueService>();
            services.AddTransient<IGEEstabService, GEEstabService>();
            services.AddTransient<IGEFuncionariosService, GEFuncionariosService>();
            services.AddTransient<IGELogService, GELogService>();
            services.AddTransient<IGELogDetService, GELogDetService>();
            services.AddTransient<IGEUsuariosService, GEUsuariosService>();
            services.AddTransient<IPDLancamentosService, PDLancamentosService>();
            services.AddTransient<IPDOPTransfService, PDOPTransfService>();
            services.AddTransient<IPDOPTransfLoteService, PDOPTransfLoteService>();
            services.AddTransient<IPDOrdemProdService, PDOrdemProdService>();
            services.AddTransient<IPDOrdemProdCompService, PDOrdemProdCompService>();
            services.AddTransient<IPDOrdemProdRotinasService, PDOrdemProdRotinasService>();
            services.AddTransient<IPDParamEstabService, PDParamEstabService>();
            services.AddTransient<ICZIntegPedItensService, CZIntegPedItensService>();
            services.AddTransient<ICZIntegraPedidosService, CZIntegraPedidosService>();
            services.AddTransient<ICZIntegProdutosService, CZIntegProdutosService>();
            services.AddTransient<ICZIntegrProdDetService, CZIntegrProdDetService>();
            services.AddTransient<ICZIntegrEmpresaService, CZIntegrEmpresaService>();
            services.AddTransient<ICZIntegracaoRevisaoService, CZIntegracaoRevisaoService>();
            services.AddTransient<IPDAptInicioProdService, PDAptInicioProdService>();
            services.AddTransient<IPDAptTerminoProdService, PDAptTerminoProdService>();
            services.AddTransient<IPDMotivosService, PDMotivosService>();
            services.AddTransient<IPDAptLancamentoService, PDAptLancamentoService>();
            services.AddTransient<IPDParadasService, PDParadasService>();
            services.AddTransient<IPDAptParadasService, PDAptParadasService>();
            services.AddTransient<IPDSucataService, PDSucataService>();
            services.AddTransient<IESRequisicaoService, ESRequisicaoService>();
            services.AddTransient<IPDOEEService, PDOEEService>();

            services.AddTransient<IESInventarioAS, ESInventarioAS>();
            services.AddTransient<IESInventDetAS, ESInventDetAS>();
            services.AddTransient<IESInventLoteAS, ESInventLoteAS>();
            services.AddTransient<IESInventTercAS, ESInventTercAS>();
            services.AddTransient<IESKardexAS, ESKardexAS>();
            services.AddTransient<IESLoteAS, ESLoteAS>();
            services.AddTransient<IESLoteSaldoAS, ESLoteSaldoAS>();
            services.AddTransient<IESTransfDetAS, ESTransfDetAS>();
            services.AddTransient<IESTransferenciaAS, ESTransferenciaAS>();
            services.AddTransient<IESTransfLoteAS, ESTransfLoteAS>();
            services.AddTransient<IFSLocalEstoqueAS, FSLocalEstoqueAS>();
            services.AddTransient<IFSProdutoAS, FSProdutoAS>();
            services.AddTransient<IFSProdutoparamEstabAS, FSProdutoparamEstabAS>();
            services.AddTransient<IFSProdutoPcpAS, FSProdutoPcpAS>();
            services.AddTransient<IFSSaldoEstoqueAS, FSSaldoEstoqueAS>();
            services.AddTransient<IGEEstabAS, GEEstabAS>();
            services.AddTransient<IGEFuncionariosAS, GEFuncionariosAS>();
            services.AddTransient<IGELogAS, GELogAS>();
            services.AddTransient<IGELogDetAS, GELogDetAS>();
            services.AddTransient<IGEUsuariosAS, GEUsuariosAS>();
            services.AddTransient<IPDLancamentosAS, PDLancamentosAS>();
            services.AddTransient<IPDOPTransfAS, PDOPTransfAS>();
            services.AddTransient<IPDOPTransfLoteAS, PDOPTransfLoteAS>();
            services.AddTransient<IPDOrdemProdAS, PDOrdemProdAS>();
            services.AddTransient<IPDOrdemProdCompAS, PDOrdemProdCompAS>();
            services.AddTransient<IPDOrdemProdRotinasAS, PDOrdemProdRotinasAS>();
            services.AddTransient<IPDParamEstabAS, PDParamEstabAS>();
            services.AddTransient<ICZIntegPedItensAS, CZIntegPedItensAS>();
            services.AddTransient<ICZIntegraPedidosAS, CZIntegraPedidosAS>();
            services.AddTransient<ICZIntegProdutosAS, CZIntegProdutosAS>();
            services.AddTransient<ICZIntegrProdDetAS, CZIntegrProdDetAS>();
            services.AddTransient<ICZIntegrEmpresaAS, CZIntegrEmpresaAS>();
            services.AddTransient<ICZIntegracaoRevisaoAS, CZIntegracaoRevisaoAS>();
            services.AddTransient<IPDAptInicioProdAS, PDAptInicioProdAS>();
            services.AddTransient<IPDAptTerminoProdAS, PDAptTerminoProdAS>();
            services.AddTransient<IPDMotivosAS, PDMotivosAS>();
            services.AddTransient<IPDAptLancamentoAS, PDAptLancamentoAS>();
            services.AddTransient<IPDParadasAS, PDParadasAS>();
            services.AddTransient<IPDAptParadasAS, PDAptParadasAS>();
            services.AddTransient<IPDSucataAS, PDSucataAS>();
            services.AddTransient<IESRequisicaoAS, ESRequisicaoAS>();
            services.AddTransient<IPDOEEAS, PDOEEAS>();

            services.AddTransient<IMNEquipamentoAS, MNEquipamentoAS>();
            services.AddTransient<IMNEquipamentoService, MNEquipamentoService>();
            services.AddTransient<IMNEquipamentoRepository, MNEquipamentoRepository>();

            services.AddTransient<IMNMotivoManutencaoAS, MNMotivoManutencaoAS>();
            services.AddTransient<IMNMotivoManutencaoService, MNMotivoManutencaoService>();
            services.AddTransient<IMNMotivoManutencaoRepository, MNMotivoManutencaoRepository>();

            services.AddTransient<IMNSolicManutAS, MNSolicManutAS>();
            services.AddTransient<IMNSolicManutService, MNSolicManutService>();
            services.AddTransient<IMNSolicManutRepository, MNSolicManutRepository>();

            services.AddTransient<IMNTipoManutencaoAS, MNTipoManutencaoAS>();
            services.AddTransient<IMNTipoManutencaoService, MNTipoManutencaoService>();
            services.AddTransient<IMNTipoManutencaoRepository, MNTipoManutencaoRepository>();

            services.AddTransient<IENMaquinasRepository, ENMaquinasRepository>();
            services.AddTransient<IENMaquinasService, ENMaquinasService>();
            services.AddTransient<IENMaquinasAS, ENMaquinasAS>();

            services.AddTransient<IENProcessosRepository, ENProcessosRepository>();
            services.AddTransient<IENProcessosService, ENProcessosService>();
            services.AddTransient<IENProcessosAS, ENProcessosAS>();

            services.AddTransient<IPDOEEParamRepository, PDOEEParamRepository>();
            services.AddTransient<IPDOEEParamService, PDOEEParamService>();
            services.AddTransient<IPDOEEParamAS, PDOEEParamAS>();

            services.AddTransient<IPDOEEJustificaRepository, PDOEEJustificaRepository>();
            services.AddTransient<IPDOEEJustificaService, PDOEEJustificaService>();
            services.AddTransient<IPDOEEJustificaAS, PDOEEJustificaAS>();

            services.AddTransient<IENProcAgrupRepository, ENProcAgrupRepository>();
            services.AddTransient<IENProcAgrupService, ENProcAgrupService>();
            services.AddTransient<IENProcAgrupAS, ENProcAgrupAS>();


            services.AddTransient<ICurrentSettingsAS, CurrentSettingsAS>();
        }

        public static void RegisterServicesConsole(this IServiceCollection services)
        {
            RegisterContext(services, false);
            IOCServices(services);
        }

        public static void RegisterServices(this IServiceCollection services)
        {
            RegisterContext(services, true);
            IOCServices(services);
        }
    }
}
