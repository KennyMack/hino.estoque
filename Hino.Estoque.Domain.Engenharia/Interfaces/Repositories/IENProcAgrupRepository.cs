﻿using Hino.Estoque.Domain.Base.Interfaces.Repositories;
using Hino.Estoque.Infra.Cross.Entities.Engenharia;

namespace Hino.Estoque.Domain.Engenharia.Interfaces.Repositories
{
    public interface IENProcAgrupRepository : IBaseRepository<ENProcAgrup>
    {
    }
}
