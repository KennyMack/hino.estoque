﻿using Hino.Estoque.Domain.Base.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Engenharia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Domain.Engenharia.Interfaces.Services
{
    public interface IENProcessosService : IBaseService<ENProcessos>
    {
    }
}
