﻿using Hino.Estoque.Domain.Base.Services;
using Hino.Estoque.Domain.Engenharia.Interfaces.Repositories;
using Hino.Estoque.Domain.Engenharia.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Engenharia;

namespace Hino.Estoque.Domain.Engenharia.Services
{
    public class ENMaquinasService : BaseService<ENMaquinas>, IENMaquinasService
    {
        private readonly IENMaquinasRepository _IENMaquinasRepository;

        public ENMaquinasService(IENMaquinasRepository pIENMaquinasRepository) :
             base(pIENMaquinasRepository)
        {
            _IENMaquinasRepository = pIENMaquinasRepository;
        }
    }
}
