﻿using Hino.Estoque.Domain.Base.Services;
using Hino.Estoque.Domain.Engenharia.Interfaces.Repositories;
using Hino.Estoque.Domain.Engenharia.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Engenharia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Domain.Engenharia.Services
{
    public class ENProcAgrupService : BaseService<ENProcAgrup>, IENProcAgrupService
    {
        private readonly IENProcAgrupRepository _IENProcAgrupRepository;

        public ENProcAgrupService(IENProcAgrupRepository pENProcessosRepository) :
             base(pENProcessosRepository)
        {
            _IENProcAgrupRepository = pENProcessosRepository;
        }
    }
}
