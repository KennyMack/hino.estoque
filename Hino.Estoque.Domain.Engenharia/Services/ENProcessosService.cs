﻿using Hino.Estoque.Domain.Base.Services;
using Hino.Estoque.Domain.Engenharia.Interfaces.Repositories;
using Hino.Estoque.Domain.Engenharia.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Engenharia;

namespace Hino.Estoque.Domain.Engenharia.Services
{
    public class ENProcessosService : BaseService<ENProcessos>, IENProcessosService
    {
        private readonly IENProcessosRepository _IENProcessosRepository;

        public ENProcessosService(IENProcessosRepository pENProcessosRepository) :
             base(pENProcessosRepository)
        {
            _IENProcessosRepository = pENProcessosRepository;
        }
    }
}
