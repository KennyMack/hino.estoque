using Hino.Estoque.Domain.Producao.Interfaces.Repositories;
using Hino.Estoque.Domain.Producao.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Producao;
using Hino.Estoque.Domain.Base.Services;
using System.Threading.Tasks;
using Hino.Estoque.Infra.Cross.Utils.Exceptions;

namespace Hino.Estoque.Domain.Producao.Services
{
    public class PDOPTransfService : BaseService<PDOPTransf>, IPDOPTransfService
    {
        private readonly IPDOPTransfRepository _IPDOPTransfRepository;

        public PDOPTransfService(IPDOPTransfRepository pIPDOPTransfRepository) : 
             base(pIPDOPTransfRepository)
        {
            _IPDOPTransfRepository = pIPDOPTransfRepository;
        }

        public async Task<bool> AtualizaTransfOP(short pCodEstab, byte pStatus, PDOPTransf pPDOPTransf)
        {
            try
            {
                await _IPDOPTransfRepository.AtualizaTransfOP(pCodEstab, pStatus, pPDOPTransf);
            }
            catch (System.Exception ex)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { ex.Message }
                });

                return false;
            }
            return true;
        }

        public Task<double> QtdCompTransferida(short pCodEstab, decimal pCodOrdProd, string pNivelOrdProd, string pCodComponente) =>
            _IPDOPTransfRepository.QtdCompTransferida(pCodEstab, pCodOrdProd, pNivelOrdProd, pCodComponente);
    }
}
