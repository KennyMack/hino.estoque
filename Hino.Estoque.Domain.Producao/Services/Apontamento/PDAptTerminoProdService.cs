﻿using Hino.Estoque.Domain.Base.Services;
using Hino.Estoque.Domain.Producao.Interfaces.Repositories.Apontamento;
using Hino.Estoque.Domain.Producao.Interfaces.Services.Apontamento;
using Hino.Estoque.Infra.Cross.Entities.Producao.Apontamento;
using Hino.Estoque.Infra.Cross.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Domain.Producao.Services.Apontamento
{
    public class PDAptTerminoProdService : BaseService<PDAptTerminoProd>, IPDAptTerminoProdService
    {
        private readonly IPDAptTerminoProdRepository _IPDAptTerminoProdRepository;

        public PDAptTerminoProdService(IPDAptTerminoProdRepository pIPDAptTerminoProdRepository) :
             base(pIPDAptTerminoProdRepository)
        {
            _IPDAptTerminoProdRepository = pIPDAptTerminoProdRepository;
        }

        public async Task<IEnumerable<PDAptLancList>> GetListApontamentoAsync(short pCodEstab, int pCodFuncionario)
        {
            try
            {
                return await _IPDAptTerminoProdRepository.GetListApontamentoAsync(pCodEstab, pCodFuncionario);
            }
            catch (Exception ex)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { ex.Message }
                });

                return new List<PDAptLancList>();
            }
        }
    }
}
