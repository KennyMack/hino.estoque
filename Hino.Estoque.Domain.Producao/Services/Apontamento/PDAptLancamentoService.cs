﻿using Hino.Estoque.Domain.Base.Services;
using Hino.Estoque.Domain.Producao.Interfaces.Repositories.Apontamento;
using Hino.Estoque.Domain.Producao.Interfaces.Services.Apontamento;
using Hino.Estoque.Infra.Cross.Entities.Producao.Apontamento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Domain.Producao.Services.Apontamento
{
    public class PDAptLancamentoService : BaseService<PDAptLancamento>, IPDAptLancamentoService
    {
        private readonly IPDAptLancamentoRepository _IPDAptLancamentoRepository;

        public PDAptLancamentoService(IPDAptLancamentoRepository pIPDAptLancamentoRepository) :
             base(pIPDAptLancamentoRepository)
        {
            _IPDAptLancamentoRepository = pIPDAptLancamentoRepository;
        }
    }
}
