﻿using Hino.Estoque.Domain.Base.Services;
using Hino.Estoque.Domain.Producao.Interfaces.Repositories.Apontamento;
using Hino.Estoque.Domain.Producao.Interfaces.Services.Apontamento;
using Hino.Estoque.Infra.Cross.Entities.Producao.Apontamento;
using Hino.Estoque.Infra.Cross.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Domain.Producao.Services.Apontamento
{
    public class PDSucataService : BaseService<PDSucata>, IPDSucataService
    {
        private readonly IPDSucataRepository _IPDSucataRepository;

        public PDSucataService(IPDSucataRepository pIPDSucataRepository) :
             base(pIPDSucataRepository)
        {
            _IPDSucataRepository = pIPDSucataRepository;
        }

        public async Task<bool> ValidaDtSuc(short pCodEstab, decimal pCodOrdProd, string pNivelOrdProd, DateTime pDataConsumo) =>
            await _IPDSucataRepository.ValidaDtSuc(pCodEstab, pCodOrdProd, pNivelOrdProd, pDataConsumo);

        public async Task<bool> ApontaSucataAsync(short pCodEstab, long pCodSucata, string pCodUsuario)
        {
            try
            {
                await _IPDSucataRepository.ApontaSucataAsync(pCodEstab, pCodSucata, pCodUsuario);
            }
            catch (Exception ex)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { ex.Message }
                });
                return false;
            }

            return true;
        }
    }
}
