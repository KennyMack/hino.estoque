using Hino.Estoque.Domain.Producao.Interfaces.Repositories;
using Hino.Estoque.Domain.Producao.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Producao;
using Hino.Estoque.Domain.Base.Services;
using Hino.Estoque.Infra.Cross.Entities.Producao.Apontamento;
using Hino.Estoque.Domain.Producao.Interfaces.Services.Apontamento;
using Hino.Estoque.Domain.Producao.Interfaces.Repositories.Apontamento;
using System.Threading.Tasks;
using Hino.Estoque.Infra.Cross.Utils.Exceptions;
using System.Collections.Generic;

namespace Hino.Estoque.Domain.Producao.Services.Apontamento
{
    public class PDLancamentosService : BaseService<PDLancamentos>, IPDLancamentosService
    {
        private readonly IPDLancamentosRepository _IPDLancamentosRepository;

        public PDLancamentosService(IPDLancamentosRepository pIPDLancamentosRepository) : 
             base(pIPDLancamentosRepository)
        {
            _IPDLancamentosRepository = pIPDLancamentosRepository;
        }

        public async Task<bool> ApontamentoProducaoTempAsync(long pCodLancamento, PDLancamentos pLancamento, List<PDLancMotivoRefugo> pRefugos)
        {
            try
            {
                return await _IPDLancamentosRepository.ApontamentoProducaoTempAsync(pCodLancamento, pLancamento, pRefugos);
            }
            catch (System.Exception ex)
            {

                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "body",
                    Value = "",
                    Messages = new string[] { ex.Message }
                });
            }

            return false;
        }

        public async Task<bool> ValidateAppointmentAsync(PDLancamentos pLancamento)
        {
            try
            {
                return await _IPDLancamentosRepository.ValidateAppointmentAsync(pLancamento);
            }
            catch (System.Exception ex)
            {

                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "body",
                    Value = "",
                    Messages = new string[] { ex.Message }
                });
            }

            return false;
        }

        public async Task<bool> VerifyOperations(PDLancamentos pLancamento) =>
            await _IPDLancamentosRepository.VerifyOperations(pLancamento);
    }
}
