﻿using Hino.Estoque.Domain.Base.Services;
using Hino.Estoque.Domain.Producao.Interfaces.Repositories.Apontamento;
using Hino.Estoque.Domain.Producao.Interfaces.Services.Apontamento;
using Hino.Estoque.Infra.Cross.Entities.Producao.Apontamento;
using Hino.Estoque.Infra.Cross.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Domain.Producao.Services.Apontamento
{
    public class PDAptInicioProdService : BaseService<PDAptInicioProd>, IPDAptInicioProdService
    {
        private readonly IPDAptInicioProdRepository _IPDAptInicioProdRepository;

        public PDAptInicioProdService(IPDAptInicioProdRepository pIPDLancamentosRepository) :
             base(pIPDLancamentosRepository)
        {
            _IPDAptInicioProdRepository = pIPDLancamentosRepository;
        }

        public async Task<bool> ExisteInspecaoRotinaAnteriorAsync(short pCodEstab, PDAptInicioProd pPDAptInicioProd)
        {
            try
            {
                return await _IPDAptInicioProdRepository.ExisteInspecaoRotinaAnteriorAsync(pCodEstab, pPDAptInicioProd);
            }
            catch (Exception ex)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { ex.Message }
                });
            }
            return false;
        }

        public async Task<PDAptInicioProd> GetLastAptStartAsync(
            short pCodEstab, int pCodFuncionario,
            long pCodOrdProd, string pNivelOrdProd,
            long pCodEstrutura, int pCodRoteiro,
            int pOperacao, short pTipo)
        {
            try
            {
                return await _IPDAptInicioProdRepository.GetLastAptStartAsync(
                    pCodEstab, pCodFuncionario, pCodOrdProd,
                    pNivelOrdProd, pCodEstrutura, pCodRoteiro,
                    pOperacao, pTipo);
            }
            catch (Exception ex)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { ex.Message }
                });
            }
            return null;
        }

        public async Task<bool> OperacaoJaIniciadaAsync(short pCodEstab, PDAptInicioProd pPDAptInicioProd)
        {
            try
            {
                return await _IPDAptInicioProdRepository.OperacaoJaIniciadaAsync(pCodEstab, pPDAptInicioProd);
            }
            catch (Exception ex)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { ex.Message }
                });
            }
            return false;
        }

    }
}
