﻿using Hino.Estoque.Domain.Base.Services;
using Hino.Estoque.Domain.Producao.Interfaces.Repositories.OEE;
using Hino.Estoque.Domain.Producao.Interfaces.Services.OEE;
using Hino.Estoque.Infra.Cross.Entities.Producao.OEE;
using System;
using System.Collections.Generic;

namespace Hino.Estoque.Domain.Producao.Services.OEE
{
    public class PDOEEJustificaService : BaseService<PDOEEJustifica>, IPDOEEJustificaService
    {
        private readonly IPDOEEJustificaRepository _IPDOEEJustificaRepository;

        public PDOEEJustificaService(IPDOEEJustificaRepository pIPDOEEParamRepository) :
             base(pIPDOEEParamRepository)
        {
            _IPDOEEJustificaRepository = pIPDOEEParamRepository;
        }

        public IEnumerable<PDOEEJustifica> GetOEEJustificativas(short pCodEstab, string pCodMaquina, DateTime pDtPeriodo) =>
            _IPDOEEJustificaRepository.GetOEEJustificativas(pCodEstab, pCodMaquina, pDtPeriodo);
    }
}
