﻿using Hino.Estoque.Domain.Base.Services;
using Hino.Estoque.Domain.Producao.Interfaces.Repositories.OEE;
using Hino.Estoque.Domain.Producao.Interfaces.Services.OEE;
using Hino.Estoque.Infra.Cross.Entities.Producao.OEE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Domain.Producao.Services.OEE
{
    public class PDOEEService : BaseService<PDOEE>, IPDOEEService
    {
        private readonly IPDOEERepository _IPDOEERepository;

        public PDOEEService(IPDOEERepository pIPDOEERepository) :
             base(pIPDOEERepository)
        {
            _IPDOEERepository = pIPDOEERepository;
        }

        public async Task<bool> GerarOEEAsync(PDOEE pOee)
        {
            if (pOee.codprocesso == null)
                pOee.codprocesso = 0;

            if (string.IsNullOrEmpty(pOee.codmaquina))
                pOee.codmaquina = "0";

            pOee.gerardet = 1;

            return await _IPDOEERepository.GerarOEEAsync(pOee);
        }

        public async Task<bool> GerarOEEMensalAsync(PDOEE pOee)
        {
            if (pOee.codprocesso == null)
                pOee.codprocesso = 0;

            if (string.IsNullOrEmpty(pOee.codmaquina))
                pOee.codmaquina = "0";

            pOee.gerardet = 1;

            return await _IPDOEERepository.GerarOEEMensalAsync(pOee);
        }

        public async Task<IEnumerable<PDOEEResultado>> ListagemAsync(short pCodEstab, string pCodUsuario) =>
            await _IPDOEERepository.ListagemAsync(pCodEstab, pCodUsuario);

        public async Task<IEnumerable<PDOEEMensalResultado>> ListagemMensalAsync(short pCodEstab, string pCodUsuario) =>
            await _IPDOEERepository.ListagemMensalAsync(pCodEstab, pCodUsuario);
    }
}
