﻿using Hino.Estoque.Domain.Base.Services;
using Hino.Estoque.Domain.Producao.Interfaces.Repositories.OEE;
using Hino.Estoque.Domain.Producao.Interfaces.Services.OEE;
using Hino.Estoque.Infra.Cross.Entities.Producao.OEE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Domain.Producao.Services.OEE
{
    public class PDOEEParamService : BaseService<PDOEEParam>, IPDOEEParamService
    {
        private readonly IPDOEEParamRepository _IPDOEEParamRepository;

        public PDOEEParamService(IPDOEEParamRepository pIPDOEEParamRepository) :
             base(pIPDOEEParamRepository)
        {
            _IPDOEEParamRepository = pIPDOEEParamRepository;
        }

        public IEnumerable<PDOEEParam> GetOEEColorsByEstabAsync(short pCodEstab) =>
            _IPDOEEParamRepository.GetOEEColorsByEstabAsync(pCodEstab);
    }
}
