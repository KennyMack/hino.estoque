using Hino.Estoque.Domain.Producao.Interfaces.Repositories;
using Hino.Estoque.Domain.Producao.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Producao;
using Hino.Estoque.Domain.Base.Services;

namespace Hino.Estoque.Domain.Producao.Services
{
    public class PDOPTransfLoteService : BaseService<PDOPTransfLote>, IPDOPTransfLoteService
    {
        private readonly IPDOPTransfLoteRepository _IPDOPTransfLoteRepository;

        public PDOPTransfLoteService(IPDOPTransfLoteRepository pIPDOPTransfLoteRepository) : 
             base(pIPDOPTransfLoteRepository)
        {
            _IPDOPTransfLoteRepository = pIPDOPTransfLoteRepository;
        }
    }
}
