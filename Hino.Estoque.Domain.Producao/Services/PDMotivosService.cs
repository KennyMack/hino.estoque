﻿using Hino.Estoque.Domain.Base.Services;
using Hino.Estoque.Domain.Producao.Interfaces.Repositories;
using Hino.Estoque.Domain.Producao.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Producao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Domain.Producao.Services
{
    public class PDMotivosService : BaseService<PDMotivos>, IPDMotivosService
    {
        private readonly IPDMotivosRepository _IPDMotivosRepository;

        public PDMotivosService(IPDMotivosRepository pIPDMotivosRepository) :
             base(pIPDMotivosRepository)
        {
            _IPDMotivosRepository = pIPDMotivosRepository;
        }
    }
}
