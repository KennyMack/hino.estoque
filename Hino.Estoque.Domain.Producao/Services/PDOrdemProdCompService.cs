using Hino.Estoque.Domain.Producao.Interfaces.Repositories;
using Hino.Estoque.Domain.Producao.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Producao;
using Hino.Estoque.Domain.Base.Services;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Hino.Estoque.Domain.Producao.Services
{
    public class PDOrdemProdCompService : BaseService<PDOrdemProdComp>, IPDOrdemProdCompService
    {
        private readonly IPDOrdemProdCompRepository _IPDOrdemProdCompRepository;

        public PDOrdemProdCompService(IPDOrdemProdCompRepository pIPDOrdemProdCompRepository) : 
             base(pIPDOrdemProdCompRepository)
        {
            _IPDOrdemProdCompRepository = pIPDOrdemProdCompRepository;
        }

        public Task<IEnumerable<PDOrdemProdCompEstqEnd>> BuscaListaEstqEndAsync(string pCodProduto, short pCodEstab) =>
            _IPDOrdemProdCompRepository.BuscaListaEstqEndAsync(pCodProduto, pCodEstab);
    }
}
