using Hino.Estoque.Domain.Producao.Interfaces.Repositories;
using Hino.Estoque.Domain.Producao.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Producao;
using Hino.Estoque.Domain.Base.Services;

namespace Hino.Estoque.Domain.Producao.Services
{
    public class PDParamEstabService : BaseService<PDParamEstab>, IPDParamEstabService
    {
        private readonly IPDParamEstabRepository _IPDParamEstabRepository;

        public PDParamEstabService(IPDParamEstabRepository pIPDParamEstabRepository) : 
             base(pIPDParamEstabRepository)
        {
            _IPDParamEstabRepository = pIPDParamEstabRepository;
        }
    }
}
