using Hino.Estoque.Domain.Producao.Interfaces.Repositories;
using Hino.Estoque.Domain.Producao.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Producao;
using Hino.Estoque.Domain.Base.Services;

namespace Hino.Estoque.Domain.Producao.Services
{
    public class PDOrdemProdRotinasService : BaseService<PDOrdemProdRotinas>, IPDOrdemProdRotinasService
    {
        private readonly IPDOrdemProdRotinasRepository _IPDOrdemProdRotinasRepository;

        public PDOrdemProdRotinasService(IPDOrdemProdRotinasRepository pIPDOrdemProdRotinasRepository) : 
             base(pIPDOrdemProdRotinasRepository)
        {
            _IPDOrdemProdRotinasRepository = pIPDOrdemProdRotinasRepository;
        }
    }
}
