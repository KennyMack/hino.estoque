using Hino.Estoque.Domain.Producao.Interfaces.Repositories;
using Hino.Estoque.Domain.Producao.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Producao;
using Hino.Estoque.Domain.Base.Services;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using System;
using Hino.Estoque.Infra.Cross.Utils.Exceptions;

namespace Hino.Estoque.Domain.Producao.Services
{
    public class PDOrdemProdService : BaseService<PDOrdemProd>, IPDOrdemProdService
    {
        private readonly IPDOrdemProdRepository _IPDOrdemProdRepository;

        public PDOrdemProdService(IPDOrdemProdRepository pIPDOrdemProdRepository) : 
             base(pIPDOrdemProdRepository)
        {
            _IPDOrdemProdRepository = pIPDOrdemProdRepository;
        }

        public async Task<IEnumerable<PDEficienciaProducao>> BuscaEficienciaProducaoAsync(short pCodEstab, DateTime pDtInicio, DateTime pDtTermino)
        {
            try
            {
                return await _IPDOrdemProdRepository.BuscaEficienciaProducaoAsync(pCodEstab, pDtInicio, pDtTermino);
            }
            catch (Exception ex)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { ex.Message }
                });
            }
            return null;
        }

        public async Task<PDOrdemProd> GetOPByBarCode(short pCodEstab, string pBarCode)
        {
            var result = await _IPDOrdemProdRepository.GetOPByBarCode(pCodEstab, pBarCode);

            if (result != null && result.pdordemprodcomp.Any())
            {
                result.pdordemprodcomp = result.pdordemprodcomp.OrderByDescending(r =>
                r.fsproduto.FSProdutoparamEstab.FirstOrDefault()
                    ?.ESProdEnderec?.FirstOrDefault()
                    ?.ESEnderecamento?.descricao).ThenByDescending(r =>
                        r.fsproduto.FSProdutoparamEstab.FirstOrDefault()
                           ?.ESProdEnderec.FirstOrDefault()?.ESEnderecamento?.sigla).ToList();
            }
            return result;
        }

        public async Task<PDOrdemProd> GetOPById(short pCodEstab, decimal pCodOrdProd, string pNivelOrdProd)
        {
            var result = await _IPDOrdemProdRepository.GetOPById(pCodEstab, pCodOrdProd, pNivelOrdProd);
            
            if (result != null && result.pdordemprodcomp.Any())
            {
                result.pdordemprodcomp = result.pdordemprodcomp.OrderByDescending(r =>
                    r.fsproduto.FSProdutoparamEstab.FirstOrDefault()
                        ?.ESProdEnderec?.FirstOrDefault()
                        ?.ESEnderecamento.descricao).ThenByDescending(r =>
                            r.fsproduto.FSProdutoparamEstab.FirstOrDefault()
                                ?.ESProdEnderec.FirstOrDefault()?.ESEnderecamento?.sigla).ToList();
            }
            return result;
        }

        public async Task<long?> GetCodEstruturaAsync(string pCodProduto, short pCodEstab) =>
            await _IPDOrdemProdRepository.GetCodEstruturaAsync(pCodProduto, pCodEstab);
    }
}
