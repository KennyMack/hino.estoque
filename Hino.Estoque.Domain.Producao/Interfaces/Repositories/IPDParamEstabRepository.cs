using Hino.Estoque.Infra.Cross.Entities.Producao;
using Hino.Estoque.Domain.Base.Interfaces.Repositories;

namespace Hino.Estoque.Domain.Producao.Interfaces.Repositories
{
    public interface IPDParamEstabRepository : IBaseRepository<PDParamEstab>
    {
    }
}
