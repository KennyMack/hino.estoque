using Hino.Estoque.Domain.Base.Interfaces.Repositories;
using Hino.Estoque.Infra.Cross.Entities.Producao.Apontamento;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.Estoque.Domain.Producao.Interfaces.Repositories.Apontamento
{
    public interface IPDLancamentosRepository : IBaseRepository<PDLancamentos>
    {
        Task<bool> VerifyOperations(PDLancamentos pLancamento);
        Task<bool> ValidateAppointmentAsync(PDLancamentos pLancamento);
        Task<bool> ApontamentoProducaoTempAsync(long pCodLancamento, PDLancamentos pLancamento, List<PDLancMotivoRefugo> pRefugos);
    }
}
