using Hino.Estoque.Infra.Cross.Entities.Producao;
using Hino.Estoque.Domain.Base.Interfaces.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.Estoque.Domain.Producao.Interfaces.Repositories
{
    public interface IPDOrdemProdCompRepository : IBaseRepository<PDOrdemProdComp>
    {
        Task<IEnumerable<PDOrdemProdCompEstqEnd>> BuscaListaEstqEndAsync(string pCodProduto, short pCodEstab);
    }
}
