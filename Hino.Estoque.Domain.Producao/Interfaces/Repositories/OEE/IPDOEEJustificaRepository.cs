﻿using Hino.Estoque.Domain.Base.Interfaces.Repositories;
using Hino.Estoque.Infra.Cross.Entities.Producao.OEE;
using System;
using System.Collections.Generic;

namespace Hino.Estoque.Domain.Producao.Interfaces.Repositories.OEE
{
    public interface IPDOEEJustificaRepository : IBaseRepository<PDOEEJustifica>
    {
        IEnumerable<PDOEEJustifica> GetOEEJustificativas(short pCodEstab, string pCodMaquina, DateTime pDtPeriodo);
    }
}
