﻿using Hino.Estoque.Domain.Base.Interfaces.Repositories;
using Hino.Estoque.Infra.Cross.Entities.Producao.OEE;
using System.Collections.Generic;

namespace Hino.Estoque.Domain.Producao.Interfaces.Repositories.OEE
{
    public interface IPDOEEParamRepository : IBaseRepository<PDOEEParam>
    {
        IEnumerable<PDOEEParam> GetOEEColorsByEstabAsync(short pCodEstab);
    }
}
