using Hino.Estoque.Infra.Cross.Entities.Producao;
using Hino.Estoque.Domain.Base.Interfaces.Repositories;
using System.Threading.Tasks;

namespace Hino.Estoque.Domain.Producao.Interfaces.Repositories
{
    public interface IPDOPTransfRepository : IBaseRepository<PDOPTransf>
    {
        Task<bool> AtualizaTransfOP(short pCodEstab, byte pStatus, PDOPTransf pPDOPTransf);
        Task<double> QtdCompTransferida(short pCodEstab, decimal pCodOrdProd, string pNivelOrdProd, string pCodComponente);
    }
}
