using Hino.Estoque.Infra.Cross.Entities.Producao;
using Hino.Estoque.Domain.Base.Interfaces.Services;

namespace Hino.Estoque.Domain.Producao.Interfaces.Services
{
    public interface IPDOPTransfLoteService : IBaseService<PDOPTransfLote>
    {
    }
}
