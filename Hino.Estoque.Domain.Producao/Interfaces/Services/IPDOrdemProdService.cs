using Hino.Estoque.Infra.Cross.Entities.Producao;
using Hino.Estoque.Domain.Base.Interfaces.Services;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;

namespace Hino.Estoque.Domain.Producao.Interfaces.Services
{
    public interface IPDOrdemProdService : IBaseService<PDOrdemProd>
    {
        Task<PDOrdemProd> GetOPByBarCode(short pCodEstab, string pBarCode);
        Task<PDOrdemProd> GetOPById(short pCodEstab, decimal pCodOrdProd, string pNivelOrdProd);
        Task<IEnumerable<PDEficienciaProducao>> BuscaEficienciaProducaoAsync(short pCodEstab, DateTime pDtInicio, DateTime pDtTermino);
        Task<long?> GetCodEstruturaAsync(string pCodProduto, short pCodEstab);
    }
}
