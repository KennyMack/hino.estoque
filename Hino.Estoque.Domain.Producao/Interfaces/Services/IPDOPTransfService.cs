using Hino.Estoque.Infra.Cross.Entities.Producao;
using Hino.Estoque.Domain.Base.Interfaces.Services;
using System.Threading.Tasks;

namespace Hino.Estoque.Domain.Producao.Interfaces.Services
{
    public interface IPDOPTransfService : IBaseService<PDOPTransf>
    {
        Task<bool> AtualizaTransfOP(short pCodEstab, byte pStatus, PDOPTransf pPDOPTransf);
        Task<double> QtdCompTransferida(short pCodEstab, decimal pCodOrdProd, string pNivelOrdProd, string pCodComponente);
    }
}
