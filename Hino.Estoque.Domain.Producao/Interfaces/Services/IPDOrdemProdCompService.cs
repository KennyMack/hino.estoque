using Hino.Estoque.Infra.Cross.Entities.Producao;
using Hino.Estoque.Domain.Base.Interfaces.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.Estoque.Domain.Producao.Interfaces.Services
{
    public interface IPDOrdemProdCompService : IBaseService<PDOrdemProdComp>
    {
        Task<IEnumerable<PDOrdemProdCompEstqEnd>> BuscaListaEstqEndAsync(string pCodProduto, short pCodEstab);
    }
}
