using Hino.Estoque.Infra.Cross.Entities.Producao;
using Hino.Estoque.Domain.Base.Interfaces.Services;

namespace Hino.Estoque.Domain.Producao.Interfaces.Services
{
    public interface IPDParamEstabService : IBaseService<PDParamEstab>
    {
    }
}
