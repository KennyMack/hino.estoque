﻿using Hino.Estoque.Domain.Base.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Producao.Apontamento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Domain.Producao.Interfaces.Services.Apontamento
{
    public interface IPDSucataService : IBaseService<PDSucata>
    {
        Task<bool> ValidaDtSuc(short pCodEstab, decimal pCodOrdProd, string pNivelOrdProd, DateTime pDataConsumo);
        Task<bool> ApontaSucataAsync(short pCodEstab, long pCodSucata, string pCodUsuario);
    }
}
