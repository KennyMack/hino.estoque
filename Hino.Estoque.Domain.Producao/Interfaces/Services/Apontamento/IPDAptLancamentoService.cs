﻿using Hino.Estoque.Domain.Base.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Producao.Apontamento;

namespace Hino.Estoque.Domain.Producao.Interfaces.Services.Apontamento
{
    public interface IPDAptLancamentoService : IBaseService<PDAptLancamento>
    {
    }
}
