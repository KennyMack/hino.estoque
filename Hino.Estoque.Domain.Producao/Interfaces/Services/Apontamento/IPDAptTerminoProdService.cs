﻿using Hino.Estoque.Domain.Base.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Producao.Apontamento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Domain.Producao.Interfaces.Services.Apontamento
{
    public interface IPDAptTerminoProdService : IBaseService<PDAptTerminoProd>
    {
        Task<IEnumerable<PDAptLancList>> GetListApontamentoAsync(short pCodEstab, int pCodFuncionario);
    }
}
