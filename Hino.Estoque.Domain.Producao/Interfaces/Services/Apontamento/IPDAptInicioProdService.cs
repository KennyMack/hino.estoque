﻿using Hino.Estoque.Domain.Base.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Producao.Apontamento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Domain.Producao.Interfaces.Services.Apontamento
{
    public interface IPDAptInicioProdService : IBaseService<PDAptInicioProd>
    {
        Task<bool> OperacaoJaIniciadaAsync(short pCodEstab, PDAptInicioProd pPDAptInicioProd);
        Task<bool> ExisteInspecaoRotinaAnteriorAsync(short pCodEstab, PDAptInicioProd pPDAptInicioProd);
        Task<PDAptInicioProd> GetLastAptStartAsync(
            short pCodEstab,
            int pCodFuncionario,
            long pCodOrdProd,
            string pNivelOrdProd,
            long pCodEstrutura,
            int pCodRoteiro,
            int pOperacao,
            short pTipo
        );
    }
}
