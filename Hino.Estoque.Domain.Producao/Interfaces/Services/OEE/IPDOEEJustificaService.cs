﻿using Hino.Estoque.Domain.Base.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Producao.OEE;
using System;
using System.Collections.Generic;

namespace Hino.Estoque.Domain.Producao.Interfaces.Services.OEE
{
    public interface IPDOEEJustificaService : IBaseService<PDOEEJustifica>
    {
        IEnumerable<PDOEEJustifica> GetOEEJustificativas(short pCodEstab, string pCodMaquina, DateTime pDtPeriodo);
    }
}
