﻿using Hino.Estoque.Domain.Base.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Producao.OEE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Estoque.Domain.Producao.Interfaces.Services.OEE
{
    public interface IPDOEEService : IBaseService<PDOEE>
    {
        Task<bool> GerarOEEAsync(PDOEE pOee);
        Task<bool> GerarOEEMensalAsync(PDOEE pOee);
        Task<IEnumerable<PDOEEResultado>> ListagemAsync(short pCodEstab, string pCodUsuario);
        Task<IEnumerable<PDOEEMensalResultado>> ListagemMensalAsync(short pCodEstab, string pCodUsuario);
    }
}
