﻿using Hino.Estoque.Domain.Base.Interfaces.Repositories;
using Hino.Estoque.Domain.Base.Interfaces.Services;
using Hino.Estoque.Infra.Cross.Entities.Producao.OEE;
using System.Collections.Generic;

namespace Hino.Estoque.Domain.Producao.Interfaces.Services.OEE
{
    public interface IPDOEEParamService : IBaseService<PDOEEParam>
    {
        IEnumerable<PDOEEParam> GetOEEColorsByEstabAsync(short pCodEstab);
    }
}
